// -----------------------------------------------------------------------
// <copyright file="XmlUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Xml
{
    using System;
    using System.IO;
    using System.Security;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    ///     Utility class providing helper methods to aid with XML.
    /// </summary>
    public static class XmlUtil
    {
        /// <summary>
        ///     Formats the specified XML String so it has indentation
        /// </summary>
        /// <param name="unformattedXml">
        ///     A string representation of some XML
        /// </param>
        /// <returns>
        ///     a formatted version of the input XML.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="unformattedXml"/> is <see langword="null" />.</exception>
        public static string FormatXml(string unformattedXml)
        {
            if (unformattedXml == null)
            {
                throw new ArgumentNullException("unformattedXml");
            }

            XDocument doc = XDocument.Parse(unformattedXml);
            return doc.ToString(SaveOptions.OmitDuplicateNamespaces);
        }

        /// <summary>
        /// Formats the XML.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <exception cref="ArgumentNullException"><paramref name="reader"/> is <see langword="null" />.</exception>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        public static void FormatXml(XmlReader reader, Stream outputStream)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            XmlWriterSettings settings = new XmlWriterSettings { Indent = true, NamespaceHandling = NamespaceHandling.OmitDuplicates, Encoding = new UTF8Encoding(false) };
            using (XmlWriter writer = XmlWriter.Create(outputStream, settings))
            {
                CopyXml(reader, writer);
            }
        }

        /// <summary>
        /// Copies the XML.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="writer">The writer.</param>
        /// <exception cref="ArgumentNullException"><paramref name="reader"/> is <see langword="null" />.</exception>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        public static void CopyXml(XmlReader reader, XmlWriter writer)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            while (reader.Read())
            {
                writer.WriteNode(reader, true);
            }
        }

        /// <summary>
        ///     Gets the root node of the XML file..
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <returns>The root node</returns>
        /// <exception cref="ArgumentNullException"><paramref name="sourceData"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSyntaxException">XML exception.</exception>
        public static string GetRootNode(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            try
            {
                var xmlReaderSettings = new XmlReaderSettings
                                            {
                                                IgnoreComments = true, 
                                                IgnoreProcessingInstructions = true, 
                                                IgnoreWhitespace = true
                                            };
                using (var stream = sourceData.InputStream)
                using (var parser = XmlReader.Create(stream, xmlReaderSettings))
                {
                    while (parser.Read())
                    {
                        switch (parser.NodeType)
                        {
                            case XmlNodeType.Element:
                                {
                                    return parser.LocalName;
                                }
                        }
                    }
                }

                return null;
            }
            catch (XmlException e)
            {
                throw new SdmxSyntaxException(e, ExceptionCode.XmlParseException);
            }
        }

        /// <summary>
        ///     Returns true if the specified ReadableDataLocation contains valid XML.
        /// </summary>
        /// <param name="sourceData">
        ///     The ReadableDataLocation to test
        /// </param>
        /// <returns>
        ///     true if the specified ReadableDataLocation contains valid XML.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="sourceData"/> is <see langword="null" />.</exception>
        /// <exception cref="SecurityException">The <see cref="T:System.Xml.XmlReader" /> does not have sufficient permissions to access the location of the XML data.</exception>
        public static bool IsXML(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            try
            {
                var settings = new XmlReaderSettings();
                settings.IgnoreComments = true;
                settings.IgnoreProcessingInstructions = true;
                settings.IgnoreWhitespace = true;
                settings.ValidationType = ValidationType.None;
                using (XmlReader reader = XmlReader.Create(sourceData.InputStream))
                {
                    return reader.Read();
                }
            }
            catch (XmlException)
            {
                return false;
            }

            ////SAXReader reader = new SAXReader();
            ////reader.SetValidation(false);
            ////try {
            ////  reader.Read(sourceData.GetInputStream());
            ////} catch (Exception th) {
            ////  return false;
            ////}
            ////return true;
        }
    }
}