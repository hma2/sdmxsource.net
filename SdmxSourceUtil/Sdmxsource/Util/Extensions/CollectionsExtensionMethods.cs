﻿// -----------------------------------------------------------------------
// <copyright file="CollectionsExtensionMethods.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     Extension methods
    /// </summary>
    public static class CollectionsExtensionMethods
    {
        /// <summary>
        /// Gets the value associated with <paramref name="key"/> if it exists or default value for <typeparamref name="TValue"/>.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <returns>The value associated with <paramref name="key"/> if it exists or default value for <typeparamref name="TValue"/>.</returns>
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException("dictionary");
            }

            TValue val;
            return dictionary.TryGetValue(key, out val) ? val : default(TValue);
        }

        /// <summary>
        /// Adds only if the <paramref name="item" /> is <typeparamref name="T" />
        /// </summary>
        /// <typeparam name="T">The item type</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="item">The item.</param>
        /// <returns><c>true</c> if <paramref name="item"/> is added, <c>false</c> otherwise.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// collection
        /// or
        /// item
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="collection" /> is <see langword="null" />.</exception>
         public static bool AddIfOfType<T>(this ICollection<T> collection, object item) where T : class
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            T castItem = item as T;
            if (castItem != null)
            {
                collection.Add(castItem);
                return true;
            }

            return false;
        }

        /// <summary>
        ///     The add all.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <param name="items">
        ///     The items.
        /// </param>
        /// <typeparam name="T">
        ///     Generic type param
        /// </typeparam>
        /// <exception cref="ArgumentNullException">
        ///     Throws ArgumentNullException
        /// </exception>
        public static void AddAll<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "New collection cannot be null");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Parameter cannot be null");
            }

            foreach (T item in items)
            {
                collection.Add(item);
            }
        }

        /// <summary>
        ///     Determines whether the specified items contains all.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="items">The items.</param>
        /// <returns>True if all</returns>
        /// <exception cref="ArgumentNullException">
        ///     items;New collection cannot be null
        ///     or
        ///     collection;Parameter cannot be null
        /// </exception>
        public static bool ContainsAll<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "New collection cannot be null");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Parameter cannot be null");
            }

            foreach (T item in items)
            {
                if (!collection.Contains(item))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     The remove item list.
        /// </summary>
        /// <param name="collection">
        ///     The collection.
        /// </param>
        /// <param name="items">
        ///     The items.
        /// </param>
        /// <typeparam name="T">
        ///     Generic type param
        /// </typeparam>
        /// <exception cref="ArgumentNullException">
        ///     Throws ArgumentNullException
        /// </exception>
        public static void RemoveItemList<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items", "New collection cannot be null");
            }

            if (collection == null)
            {
                throw new ArgumentNullException("collection", "Parameter cannot be null");
            }

            foreach (T item in items)
            {
                collection.Remove(item);
            }
        }

        /// <summary>
        /// Gets the duplicates.
        /// </summary>
        /// <typeparam name="T">The type of item</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns>The duplicates</returns>
        public static IEnumerable<T> GetDuplicates<T>(this IEnumerable<T> enumerable, IEqualityComparer<T> comparer)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException("enumerable");
            }

            ISet<T> hashSet = new HashSet<T>(comparer);
            return enumerable.Where(arg => !hashSet.Add(arg));
        }
    }
}
