﻿// -----------------------------------------------------------------------
// <copyright file="ReflectionExtensionMethods.cs" company="EUROSTAT">
//   Date Created : 2013-04-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    ///     Extensions related to Reflection
    /// </summary>
    public static class ReflectionExtensionMethods
    {
        /// <summary>
        ///     Get public instance properties of <paramref name="type" />. Works with interface inheritance
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The public properties of <paramref name="type" /></returns>
        /// <remarks>Credit goes to <a href="http://stackoverflow.com/a/2444090">An answer at SO</a></remarks>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public static PropertyInfo[] GetPublicProperties(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (type.IsInterface)
            {
                var propertyInfos = new HashSet<PropertyInfo>();

                var considered = new HashSet<Type>();
                var queue = new Queue<Type>();
                considered.Add(type);
                queue.Enqueue(type);
                while (queue.Count > 0)
                {
                    var subType = queue.Dequeue();
                    foreach (var subInterface in subType.GetInterfaces())
                    {
                        if (!considered.Contains(subInterface))
                        {
                            considered.Add(subInterface);
                            queue.Enqueue(subInterface);
                        }
                    }

                    var typeProperties =
                        subType.GetProperties(
                            BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance);

                    propertyInfos.UnionWith(typeProperties);
                }

                return propertyInfos.ToArray();
            }

            return type.GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance);
        }
    }
}