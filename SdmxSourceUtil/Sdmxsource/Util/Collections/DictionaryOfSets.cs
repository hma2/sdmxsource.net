﻿// -----------------------------------------------------------------------
// <copyright file="DictionaryOfSets.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    ///     The dictionary of sets.
    /// </summary>
    /// <typeparam name="TKey">
    ///     The key type
    /// </typeparam>
    /// <typeparam name="TValue">
    ///     The <see cref="ISet{T}" /> type
    /// </typeparam>
    [Serializable]
    public class DictionaryOfSets<TKey, TValue> : Dictionary<TKey, ISet<TValue>>, IDictionaryOfSets<TKey, TValue>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        public DictionaryOfSets()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        /// <param name="capacity">
        ///     The capacity.
        /// </param>
        public DictionaryOfSets(int capacity)
            : base(capacity)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        /// <param name="comparer">
        ///     The comparer.
        /// </param>
        public DictionaryOfSets(IEqualityComparer<TKey> comparer)
            : base(comparer)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        /// <param name="capacity">
        ///     The capacity.
        /// </param>
        /// <param name="comparer">
        ///     The comparer.
        /// </param>
        public DictionaryOfSets(int capacity, IEqualityComparer<TKey> comparer)
            : base(capacity, comparer)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        /// <param name="dictionary">
        ///     The dictionary.
        /// </param>
        /// <param name="comparer">
        ///     The comparer.
        /// </param>
        public DictionaryOfSets(IDictionary<TKey, ISet<TValue>> dictionary, IEqualityComparer<TKey> comparer)
            : base(dictionary, comparer)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        /// <param name="dictionary">
        ///     The dictionary.
        /// </param>
        public DictionaryOfSets(IDictionary<TKey, ISet<TValue>> dictionary)
            : base(dictionary)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DictionaryOfSets{TKey,TValue}" /> class.
        /// </summary>
        /// <param name="info">
        ///     The info.
        /// </param>
        /// <param name="context">
        ///     The context.
        /// </param>
        protected DictionaryOfSets(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        ///     Adds the specified value to the <paramref name="value" /> to the set corresponding to the specified
        ///     <paramref name="key" />
        /// </summary>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        public void AddToSet(TKey key, TValue value)
        {
            ISet<TValue> set;
            if (!this.TryGetValue(key, out set))
            {
                set = new HashSet<TValue>();
                this.Add(key, set);
            }

            set.Add(value);
        }
    }
}