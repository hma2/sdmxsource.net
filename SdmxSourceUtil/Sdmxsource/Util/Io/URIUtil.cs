// -----------------------------------------------------------------------
// <copyright file="URIUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.IO;
    using System.Net;
    using System.Security;

    /// <summary>
    ///     The uri util.
    /// </summary>
    public class URIUtil
    {
        /// <summary>
        ///     The temporary uri util.
        /// </summary>
        private static readonly URIUtil _temporaryUriUtil = new URIUtil("tmpFile");

        /// <summary>
        ///     The file base name.
        /// </summary>
        private readonly string _fileBaseName;

        /// <summary>
        ///     Initializes a new instance of the <see cref="URIUtil" /> class.
        /// </summary>
        /// <param name="fileBaseName">
        ///     The file base name.
        /// </param>
        private URIUtil(string fileBaseName)
        {
            ////this._fileBaseName = "tmp_file.";

            // this.deleteFilesOlderThen = deleteFilesOlderThen;
            // if(deleteFilesOlderThen > 0) {
            // task = new ScheduledTimerTask();
            // task.setDelay(deleteFilesOlderThen / 2);
            // task.setPeriod(deleteFilesOlderThen);
            // task.setRunnable(new FileDeleter());
            // } 
            // this.uriDirectory = uriDirectory;
            this._fileBaseName = fileBaseName;

            // this.overwright = overwright;
        }

        /// <summary>
        ///     Gets the URIUtil class which creates temporary URI locations for storing files.
        ///     <p />
        ///     Files which have been created through this which have not been modified for 24 hours, are deleted.
        /// </summary>
        /// <value> </value>
        public static URIUtil TempUriUtil
        {
            get
            {
                return _temporaryUriUtil;
            }
        }

        /// <summary>
        ///     Gets a URI from the specified <paramref name="file" />
        /// </summary>
        /// <param name="file">
        ///     The file.
        /// </param>
        /// <returns>
        ///     The <see cref="Uri" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="file"/> is <see langword="null" />.</exception>
        /// <exception cref="SecurityException">The caller does not have the required permission. </exception>
        public static Uri GetUriFromFile(FileInfo file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }

            return new Uri(file.FullName);
        }

        /// <summary>
        ///     The get uri util.
        /// </summary>
        /// <param name="fileBaseName">
        ///     The file base name.
        /// </param>
        /// <returns>
        ///     The <see cref="URIUtil" />.
        /// </returns>
        public static URIUtil GetUriUtil(string fileBaseName)
        {
            return new URIUtil(fileBaseName);
        }

        /// <summary>
        ///     Gets a new file from <paramref name="stream" />. The <paramref name="stream" /> is closed.
        /// </summary>
        /// <param name="stream">
        ///     The input stream.
        /// </param>
        /// <returns>
        ///     The <see cref="FileInfo" />.
        /// </returns>
        public FileInfo GetFileFromStream(Stream stream)
        {
            FileInfo outputFile = this.GetTempFile();

            StreamUtil.CopyStream(stream, outputFile.OpenWrite());

            return outputFile;
        }

        /// <summary>
        ///     Returns a <see cref="FileInfo" /> pointing to the downloaded <paramref name="sourceUri" />
        /// </summary>
        /// <param name="sourceUri">
        ///     The source uri.
        /// </param>
        /// <returns>
        ///     The <see cref="FileInfo" />.
        /// </returns>
        public FileInfo GetFileFromUri(Uri sourceUri)
        {
            return this.GetFileFromStream(URLUtil.GetInputStream(sourceUri));
        }

        /// <summary>
        ///     Gets a new temporary file
        /// </summary>
        /// <returns>
        ///     The <see cref="FileInfo" />.
        /// </returns>
        public FileInfo GetTempFile()
        {
            FileInfo f = FileUtil.CreateTemporaryFile(this._fileBaseName, "tmp");
            return f;
        }

        // SAME FOR ALL INSTANCES

        // INSTANCE SPECIFIC
    }
}