﻿// -----------------------------------------------------------------------
// <copyright file="URLUtil.cs" company="EUROSTAT">
//   Date Created : 2014-03-26
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Net;
    using System.Net.Sockets;
    using System.Security;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    ///     URLUtil class
    /// </summary>
    internal class URLUtil
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(URLUtil));

        /// <summary>
        ///     Opens an input stream to the URL, accepts GZIP encoding
        ///     Gets a new file from <paramref name="surl" />.
        /// </summary>
        /// <param name="surl">The input url.</param>
        /// <returns>
        ///     The <see cref="Stream" />.
        /// </returns>
        public static Stream GetInputStream(Uri surl)
        {
            _log.Debug("Get Input Stream from URL: " + surl);
            WebResponse connection = GetConnection(surl);
            return GetInputStream(connection, null);
        }

        /// <summary>
        ///     Returns a WebRequest from a String, throws a SdmxException if the URL String is not a valid URL
        ///     <paramref name="surl" />.
        /// </summary>
        /// <param name="surl">
        ///     The url string.
        /// </param>
        /// <returns>
        ///     The <see cref="WebRequest" />.
        /// </returns>
        public static WebRequest GetURL(string surl)
        {
            try
            {
                return WebRequest.Create(surl);
            }
            catch (WebException e)
            {
                throw new SdmxException("Malformed URL: " + surl, e);
            }
        }

        /// <summary>
        ///     Returns true if the HTTP Status is 200 (ok) from the given URL <paramref name="surl" />.
        /// </summary>
        /// <param name="surl">
        ///     The URL string.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="surl"/> is <see langword="null" />.</exception>
        public static bool UrlExists(Uri surl)
        {
            if (surl == null)
            {
                throw new ArgumentNullException("surl");
            }

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(surl);

                req.AllowAutoRedirect = false;
                req.Method = "HEAD";

                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                // Does the HTTP Status start with 2, if so, it is okay
                if (resp.StatusCode.ToString().StartsWith("2", StringComparison.Ordinal))
                {
                    return true;
                }

                _log.Warn("URL " + surl + " returns status code: " + resp.StatusCode);
            }
            catch (SecurityException securityException)
            {
                _log.Error("Security exception for URL" + surl, securityException);
            }
            catch (NotSupportedException notSupportedException)
            {
                _log.Error("Not supported URL" + surl, notSupportedException);
            }
            catch (UriFormatException uriFormatException)
            {
                _log.Error("Format URL" + surl, uriFormatException);
            }
            catch (WebException webException)
            {
                _log.Error("Cannot retrieve URL" + surl, webException);
            }
            catch (ProtocolViolationException protocolViolationException)
            {
                _log.Error("Protocol violation for URL" + surl, protocolViolationException);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                _log.Error("Invalid operation for URL" + surl, invalidOperationException);
            }

            return false;
        }

        /// <summary>
        ///     Gets the input stream from <paramref name="url" />.
        /// </summary>
        /// <param name="url">
        ///     The input url.
        /// </param>
        /// <returns>
        ///     The web response <see cref="WebResponse" />.
        /// </returns>
        private static WebResponse GetConnection(Uri url)
        {
            _log.Debug("Get URLConnection: " + url);

            // Make connection, use post mode, and send query   
            WebRequest req = WebRequest.Create(url);
            HttpWebRequest httpReq = (HttpWebRequest)req;
            httpReq.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate; 
////            httpReq.Headers["Accept-Encoding"] = "gzip";

            WebResponse resp = null;
            try
            {
                resp = req.GetResponse();
            }
            catch (IOException e)
            {
                throw new SdmxServiceUnavailableException(e, ExceptionCode.WebServiceBadConnection, url);
            }

            return resp;
        }

        /// <summary>
        ///     Gets the input stream from <paramref name="resp" />.
        /// </summary>
        /// <param name="resp">The resp.</param>
        /// <param name="payload">The payload object.</param>
        /// <returns>
        ///     The <see cref="Stream" />.
        /// </returns>
        /// <exception cref="SdmxServiceUnavailableException">Service Unavailable Exception</exception>
        /// <exception cref="SdmxException">I/O Exception while trying to unzip stream retrieved from service: + resp.ResponseUri</exception>
        private static Stream GetInputStream(WebResponse resp, object payload)
        {
            Stream stream;
            try
            {
                if (payload != null)
                {
                    // Send Payload
                    StreamWriter sw = new StreamWriter(resp.GetResponseStream());
                    sw.Write(payload);
                    sw.Close();
                }

                stream = GetInputStream(resp);
            }
            catch (IOException e)
            {
                throw new SdmxServiceUnavailableException(e, ExceptionCode.WebServiceBadConnection, e.Message);
            }

            string encoding = ((HttpWebResponse)resp).ContentEncoding;
            if (encoding.Equals("gzip"))
            {
                _log.Debug("Response received as GZIP");
                try
                {
                    stream = new GZipStream(stream, CompressionMode.Compress);
                }
                catch (IOException e)
                {
                    throw new SdmxException(
                        "I/O Ecception while trying to unzip stream retrieved from service:" + resp.ResponseUri, 
                        e);
                }
            }

            return stream;
        }

        /// <summary>
        ///     Gets the input stream from WebResponse <paramref name="resp" />.
        /// </summary>
        /// <param name="resp">
        ///     The web response.
        /// </param>
        /// <returns>
        ///     The <see cref="Stream" />.
        /// </returns>
        private static Stream GetInputStream(WebResponse resp)
        {
            try
            {
                return resp.GetResponseStream();
            }
            catch (WebException c)
            {
                throw new SdmxServiceUnavailableException(
                    c, 
                    ExceptionCode.WebServiceBadConnection, 
                    ((HttpWebResponse)c.Response).StatusDescription);
            }
            catch (SocketException c)
            {
                if (c.SocketErrorCode == SocketError.TimedOut)
                {
                    throw new SdmxServiceUnavailableException(c, ExceptionCode.WebServiceSocketTimeout, c.Message);
                }

                throw new SdmxServiceUnavailableException(c, ExceptionCode.WebServiceBadConnection, c.Message);
            }
            catch (IOException e)
            {
                var response = resp as HttpWebResponse;
                if (response != null)
                {
                    try
                    {
                        if (response.StatusCode == HttpStatusCode.Unauthorized /*401*/)
                        {
                            throw new SdmxUnauthorisedException(e.Message, e);
                        }
                    }
                    catch (IOException e1)
                    {
                        Console.WriteLine(e1.StackTrace);
                    }

                    HttpWebResponse httpConnection = response;
                    Stream inputStream = httpConnection.GetResponseStream();
                    if (inputStream != null)
                    {
                        return inputStream;
                    }
                }

                string message = null;
                if (e.Message.Contains("Server returned HTTP response code:"))
                {
                    string split = e.Message.Split(':')[1];
                    split = split.Trim();
                    split = split.Substring(0, split.IndexOf(' '));
                    int responseCode;
                    if (int.TryParse(split, out responseCode))
                    {
                        switch (responseCode)
                        {
                            case 400:
                                message = "Response Code 400 = The request could not be understood by the server due to malformed syntax";
                                break;
                            case 401:
                                message = "Response Code 401 = Authentication failure";
                                break;
                            case 403:
                                message = "Response Code 403 = The server understood the request, but is refusing to fulfill it";
                                break;
                            case 404:
                                message = "Response Code 404 = Page not found";
                                break;

                            // TODO Do others
                        }
                    }

                    Console.Out.Write(responseCode);
                }

                if (message != null)
                {
                    throw new SdmxServiceUnavailableException(e, ExceptionCode.WebServiceBadConnection, message);
                }

                throw new SdmxServiceUnavailableException(e, ExceptionCode.WebServiceBadConnection, resp.ResponseUri);
            }
        }
    }
}