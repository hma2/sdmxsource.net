﻿// -----------------------------------------------------------------------
// <copyright file="FileReadableDataLocation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.IO;

    /// <summary>
    ///     The readable data location stream.
    /// </summary>
    public class FileReadableDataLocation : BaseReadableDataLocation
    {
        /// <summary>
        ///     The input file.
        /// </summary>
        private readonly FileInfo _file;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FileReadableDataLocation" /> class.
        /// </summary>
        /// <param name="fileName">
        ///     The file name.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="fileName" />
        ///     is null
        /// </exception>
        public FileReadableDataLocation(string fileName)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            this._file = new FileInfo(fileName);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FileReadableDataLocation" /> class.
        /// </summary>
        /// <param name="file">
        ///     The input file.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="file" />
        ///     is null
        /// </exception>
        public FileReadableDataLocation(FileInfo file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }

            this._file = file;
        }

        /// <summary>
        ///     Gets a new input stream on each method call.
        ///     The input stream will be reading the same underlying data source.
        /// </summary>
        public override Stream InputStream
        {
            get
            {
                FileStream fileStream = this._file.OpenRead();
                this.AddDisposable(fileStream);
                return fileStream;
            }
        }
    }
}