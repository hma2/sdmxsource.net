// -----------------------------------------------------------------------
// <copyright file="WriteableDataLocationTmp.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Util;

    /// <summary>
    ///     The writeable data location tmp.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "Disposables are added to AddDisposable")]
    public class WriteableDataLocationTmp : ReadableDataLocationTmp, IWriteableDataLocation
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WriteableDataLocationTmp" /> class.
        /// </summary>
        public WriteableDataLocationTmp()
            : base(URIUtil.TempUriUtil.GetTempFile())
        {
            this.DeleteOnClose = true;
        }

        /// <summary>
        ///     Gets the output stream.
        /// </summary>
        public Stream OutputStream
        {
            get
            {
                var output = this.InstanceFile.OpenWrite();
                this.AddDisposable(output);

                return output;
            }
        }
    }
}