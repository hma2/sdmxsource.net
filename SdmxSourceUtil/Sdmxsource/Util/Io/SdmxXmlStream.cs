﻿// -----------------------------------------------------------------------
// <copyright file="SdmxXmlStream.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Io
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    ///     Implementation of <see cref="ISdmxXmlStream" /> for XML streaming and carrying SDMX-ML message information. Note
    ///     <see cref="IReadableDataLocation.InputStream" /> and <see cref="IWriteableDataLocation.OutputStream" /> are not
    ///     implemented.
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="SdmxXmlStream" />.
    ///     <code source="..\ReUsingExamples\Structure\ReUsingStructureParsingManagerFast.cs" lang="cs" />
    /// </example>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "Disposables are added to AddDisposable")]
    public class SdmxXmlStream : BaseReadableDataLocation, IWriteableDataLocation, ISdmxXmlStream
    {
        /// <summary>
        ///     The _message type.
        /// </summary>
        private readonly MessageEnumType _messageType;

        /// <summary>
        ///     The _query message type
        /// </summary>
        private readonly IList<QueryMessageEnumType> _queryMessageTypes;

        /// <summary>
        ///     The _reader.
        /// </summary>
        private readonly XmlReader _reader;

        /// <summary>
        ///     The _registry type.
        /// </summary>
        private readonly RegistryMessageEnumType _registryType;

        /// <summary>
        ///     The _schema enum type.
        /// </summary>
        private readonly SdmxSchemaEnumType _sdmxVersion;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxXmlStream" /> class.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <param name="messageType">
        ///     The message type.
        /// </param>
        /// <param name="sdmxVersion">
        ///     The schema enum type.
        /// </param>
        /// <param name="registryType">
        ///     The registry type.
        /// </param>
        /// <exception cref="ArgumentException">Input not an SDMX-ML file</exception>
        public SdmxXmlStream(
            XmlReader reader, 
            MessageEnumType messageType, 
            SdmxSchemaEnumType sdmxVersion, 
            RegistryMessageEnumType registryType)
        {
            if (!SdmxSchema.GetFromEnum(sdmxVersion).IsXmlFormat())
            {
                throw new ArgumentException("Input not an SDMX-ML file", "sdmxVersion");
            }

            this._reader = reader;
            this._messageType = messageType;
            this._sdmxVersion = sdmxVersion;
            this._registryType = registryType;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxXmlStream" /> class.
        /// </summary>
        /// <param name="readableDataLocation">
        ///     The readable data location.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     Input not an SDMX-ML file
        /// </exception>
        /// <exception cref="SecurityException">The <see cref="T:System.Xml.XmlReader" /> does not have sufficient permissions to access the location of the XML data.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="readableDataLocation"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSyntaxException">Can not get Scheme Version from SDMX message.  Unable to determine structure type from Namespaces- please ensure this is a valid SDMX document</exception>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        public SdmxXmlStream(IReadableDataLocation readableDataLocation)
        {
            if (readableDataLocation == null)
            {
                throw new ArgumentNullException("readableDataLocation");
            }

            var xmlStream = readableDataLocation as ISdmxXmlStream;
            if (xmlStream != null)
            {
                this._reader = xmlStream.Reader;
                this._messageType = xmlStream.MessageType;
                this._sdmxVersion = xmlStream.SdmxVersion;
                this._registryType = xmlStream.RegistryType;
                this._queryMessageTypes = xmlStream.QueryMessageTypes;
            }
            else
            {
                this._sdmxVersion = SdmxMessageUtil.GetSchemaVersion(readableDataLocation);
                if (!SdmxSchema.GetFromEnum(this._sdmxVersion).IsXmlFormat())
                {
                    throw new ArgumentException("Input not an SDMX-ML file", "readableDataLocation");
                }

                this._messageType = SdmxMessageUtil.GetMessageType(readableDataLocation);
                this._registryType = this._messageType != MessageEnumType.RegistryInterface
                                         ? RegistryMessageEnumType.Null
                                         : SdmxMessageUtil.GetRegistryMessageType(readableDataLocation);
                if (this._messageType == MessageEnumType.Query)
                {
                    this._queryMessageTypes = SdmxMessageUtil.GetQueryMessageTypes(readableDataLocation);
                }

                this._reader = XMLParser.CreateSdmxMlReader(readableDataLocation.InputStream, this._sdmxVersion);
                this.AddDisposable(this._reader);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether has reader.
        /// </summary>
        public bool HasReader
        {
            get
            {
                return this._reader != null;
            }
        }

        /// <summary>
        ///     Gets the input stream.
        /// </summary>
        /// <exception cref="NotSupportedException">
        ///     This implementation does not support a re-Readable Stream. Use Reader instead.
        /// </exception>
        public override Stream InputStream
        {
            get
            {
                throw new NotSupportedException(
                    "This implementation does not support a re-Readable Stream. Use Reader instead.");
            }
        }

        /// <summary>
        ///     Gets the message type.
        /// </summary>
        public MessageEnumType MessageType
        {
            get
            {
                return this._messageType;
            }
        }

        /// <summary>
        ///     Gets the query message type.
        /// </summary>
        public IList<QueryMessageEnumType> QueryMessageTypes
        {
            get
            {
                return this._queryMessageTypes;
            }
        }

        /// <summary>
        ///     Gets the reader.
        /// </summary>
        public XmlReader Reader
        {
            get
            {
                return this._reader;
            }
        }

        /// <summary>
        ///     Gets the registry type.
        /// </summary>
        public RegistryMessageEnumType RegistryType
        {
            get
            {
                return this._registryType;
            }
        }

        /// <summary>
        ///     Gets the schema enum type.
        /// </summary>
        public SdmxSchemaEnumType SdmxVersion
        {
            get
            {
                return this._sdmxVersion;
            }
        }

        /// <summary>
        ///     Gets the output stream.
        /// </summary>
        /// <exception cref="NotSupportedException">
        ///     This implementation does not support a re-Readable Stream. Use Writer instead.
        /// </exception>
        public Stream OutputStream
        {
            get
            {
                throw new NotSupportedException(
                    "This implementation does not support a re-Readable Stream. Use Writer instead.");
            }
        }
    }
}