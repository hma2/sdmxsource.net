// -----------------------------------------------------------------------
// <copyright file="RandomUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.Random
{
    using System;
    using System.Text;
    using System.Threading;

    /// <summary>
    ///     The random util.
    /// </summary>
    public static class RandomUtil
    {
        /// <summary>
        ///     The password set.
        /// </summary>
        private const string PasswordSet = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

        /// <summary>
        ///     The <see cref="RandomGenerator" />, one object per thread.
        /// </summary>
        [ThreadStatic]
        private static Random _random;

        /// <summary>
        ///     Seed counter
        /// </summary>
        private static int _seedCounter = new Random().Next();

        /// <summary>
        ///     Gets the <see cref="RandomGenerator" />, one object per thread.
        /// </summary>
        private static Random RandomGenerator
        {
            get
            {
                if (_random == null)
                {
                    int seed = Interlocked.Increment(ref _seedCounter);
                    _random = new Random(seed);
                }

                return _random;
            }
        }

        /// <summary>
        ///     The generate random password.
        /// </summary>
        /// <param name="length">
        ///     The length.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string GenerateRandomPassword(int length)
        {
            var b = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                b.Append(PasswordSet[RandomGenerator.Next(PasswordSet.Length)]);
            }

            return b.ToString();
        }

        /// <summary>
        ///     The generate random string.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public static string GenerateRandomString()
        {
            int r1 = RandomGenerator.Next();
            int r2 = RandomGenerator.Next();
            string hash1 = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:x}", r1);
            string hash2 = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:x}", r2);
            return hash1 + hash2;
        }
    }
}