﻿// -----------------------------------------------------------------------
// <copyright file="MessageDecoder.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Util.ResourceBundle
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Resources;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     MessageDecoder class
    /// </summary>
    public class MessageDecoder : IMessageResolver
    {
        /// <summary>
        ///     The _loc
        /// </summary>
        private static readonly CultureInfo _loc = new CultureInfo("en");

        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MessageDecoder));

        /// <summary>
        ///     The _message source
        /// </summary>
        private static ResourceManager _messageSource;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MessageDecoder" /> class
        /// </summary>
        public MessageDecoder()
        {
            SdmxException.SetMessageResolver(this);

            _messageSource = ExceptionMessages.ResourceManager;
        }

        /// <summary>
        ///     Gets the message source.
        /// </summary>
        /// <value>
        ///     The message source.
        /// </value>
        public static ResourceManager MessageSource
        {
            get
            {
                return _messageSource;
            }
        }

        /// <summary>
        ///     Decodes the message.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The decoded message</returns>
        public static string DecodeMessage(string id, params object[] args)
        {
            if (_messageSource == null)
            {
                return id;
            }

            return string.Format(CultureInfo.InvariantCulture, GetString(id, _loc), args);
        }

        /// <summary>
        ///     Decodes the message default locale.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The decoded message</returns>
        public static string DecodeMessageDefaultLocale(string id, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, GetString(id, _loc), args);
        }

        /// <summary>
        ///     Decodes the message given locale.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="lang">The language.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The decoded message</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "lang", Justification = "It needs to have the same signature as SdmxSource.Java")]
        public static string DecodeMessageGivenLocale(string id, string lang, params object[] args)
        {
            if (_messageSource == null)
            {
                return id;
            }

            var format = GetString(id, _loc);
            return string.Format(CultureInfo.InvariantCulture, format, args);
        }

        /// <summary>
        ///     Resolves the message against a resource which contains code-&gt;string mappings, inserting any args
        ///     where there are placeholders.  If there is no resource found, or no mapping could be found
        ///     for the messageCode, then the original messageCode will be returned.
        /// </summary>
        /// <param name="messageCode">The message Code.</param>
        /// <param name="locale">- the locale to resolve the message in</param>
        /// <param name="args">The args.</param>
        /// <returns>
        ///     the resolved message, or the original if could not be resolved
        /// </returns>
        public string ResolveMessage(string messageCode, CultureInfo locale, params object[] args)
        {
            string format = null;
            try
            {
                format = GetString(messageCode, locale);
                if (args != null && args.Length > 0)
                {
                    if (format != null)
                    {
                        return string.Format(CultureInfo.InvariantCulture, format, args);
                    }

                    return messageCode + " - " + string.Join(" - ", args.Where(o => o != null).Select(o => o.ToString()));
                }

                if (format != null)
                {
                    return format;
                }

                return messageCode;
            }
            catch (FormatException th)
            {
                _log.ErrorFormat(CultureInfo.InvariantCulture, "{0} - {1} - {2} - {3}", messageCode, format, locale, args);
                _log.Error("While ResolveMessage", th);
                return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} - {1}", messageCode, format ?? string.Empty);
            }
            catch (CultureNotFoundException th)
            {
                _log.ErrorFormat("Unable to find culture {0}", th.InvalidCultureName);
                return HandleException(messageCode, locale, args, format, th);
            }
            catch (MissingManifestResourceException th)
            {
                _log.Error("Missing Manifest. Please consult https://msdn.microsoft.com/query/dev12.query?appId=Dev12IDEF1&l=EN-US&k=k%28System.Resources.ResourceManager%29;k%28TargetFrameworkMoniker-.NETFramework#exception");
                return HandleException(messageCode, locale, args, format, th);
            }
            catch (MissingSatelliteAssemblyException th)
            {
                _log.Error("Missing Manifest. Please consult https://msdn.microsoft.com/query/dev12.query?appId=Dev12IDEF1&l=EN-US&k=k%28System.Resources.ResourceManager%29;k%28TargetFrameworkMoniker-.NETFramework#exception");
                return HandleException(messageCode, locale, args, format, th);
            }
        }

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="locale">The locale.</param>
        /// <param name="args">The arguments.</param>
        /// <param name="format">The format.</param>
        /// <param name="th">The th.</param>
        /// <returns>The <see cref="System.String"/>.</returns>
        private static string HandleException(string messageCode, CultureInfo locale, object[] args, string format, Exception th)
        {
            _log.ErrorFormat(CultureInfo.InvariantCulture, "{0} - {1} - {2} - {3}", messageCode, format, locale, args);
            _log.Error("While ResolveMessage", th);
            return messageCode;
        }

        /// <summary>
        ///     Gets the string.
        /// </summary>
        /// <param name="messageCode">The message code.</param>
        /// <param name="locale">The locale.</param>
        /// <returns>the message for this locale;otherwise null.</returns>
        private static string GetString(string messageCode, CultureInfo locale)
        {
            var name = "ID" + messageCode;
            if (locale == null)
            {
                return ExceptionMessages.ResourceManager.GetString(name);
            }

            return ExceptionMessages.ResourceManager.GetString(name, locale);
        }
    }
}