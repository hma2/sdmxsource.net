﻿// -----------------------------------------------------------------------
// <copyright file="PreferedLanguageTranslator.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Translator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// PreferedLanguageTranslator class.
    /// </summary>
    public class PreferedLanguageTranslator : ITranslator
    {
        /// <summary>
        /// The default language
        /// </summary>
        private readonly CultureInfo _defaultLanguage;

        /// <summary>
        /// Initializes a new instance of the <see cref="PreferedLanguageTranslator"/> class.
        /// </summary>
        /// <param name="preferedLanguages">
        /// The preferred languages.
        /// </param>
        /// <param name="availableLanguages">
        /// The available languages.
        /// </param>
        /// <param name="defaultLanguage">
        /// The default language.
        /// </param>
        public PreferedLanguageTranslator(IList<CultureInfo> preferedLanguages, IList<CultureInfo> availableLanguages, CultureInfo defaultLanguage)
        {
            if (defaultLanguage == null)
            {
                throw new ArgumentNullException("defaultLanguage");
            }

            this._defaultLanguage = defaultLanguage;
            this.SelectedLanguage = this.GetSelectedLanguage(preferedLanguages, availableLanguages) ?? defaultLanguage;
        }

        /// <summary>
        /// Gets the selected language.
        /// </summary>
        public CultureInfo SelectedLanguage { get; private set; }

        /// <summary>
        /// Gets the translation using the selected or default language.
        /// </summary>
        /// <param name="textTypeWrapperList">
        /// The textTypeWrapper list.
        /// </param>
        /// <returns>
        /// The translation.
        /// </returns>
        public string GetTranslation(IList<ITextTypeWrapper> textTypeWrapperList)
        {
            return textTypeWrapperList == null ? string.Empty : this.GetTranslation(textTypeWrapperList.ToDictionary(x => CultureInfo.CreateSpecificCulture(x.Locale), y => y.Value));
        }

        /// <summary>
        /// Gets the translation using the selected or default language.
        /// </summary>
        /// <param name="translationDictionary">
        /// The translation dictionary.
        /// </param>
        /// <returns>
        /// The translation.
        /// </returns>
        public string GetTranslation(IDictionary<CultureInfo, string> translationDictionary)
        {
            if (translationDictionary == null || !translationDictionary.Any())
            {
                return string.Empty;
            }

            var cultureInfo = translationDictionary.Keys.FirstOrDefault(x => x.TwoLetterISOLanguageName.Equals(this.SelectedLanguage.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase))
                              ?? translationDictionary.Keys.FirstOrDefault(x => x.TwoLetterISOLanguageName.Equals(this._defaultLanguage.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase));

            return cultureInfo != null ? translationDictionary[cultureInfo] : string.Empty;
        }

        /// <summary>
        /// Gets the selected language.
        /// </summary>
        /// <param name="preferedLanguages">
        /// The prefered languages.
        /// </param>
        /// <param name="availableLanguages">
        /// The available languages.
        /// </param>
        /// <returns>
        /// The selected language as a <see cref="CultureInfo"/>
        /// </returns>
        private CultureInfo GetSelectedLanguage(IList<CultureInfo> preferedLanguages, IList<CultureInfo> availableLanguages)
        {
            if (preferedLanguages == null || availableLanguages == null)
            {
                return null;
            }

            foreach (var language in preferedLanguages)
            {
                var selectedLanguage = availableLanguages.FirstOrDefault(x => x.TwoLetterISOLanguageName.Equals(language.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase));

                if (selectedLanguage != null)
                {
                    return selectedLanguage;
                }
            }

            return null;
        }
    }
}