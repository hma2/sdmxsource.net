// -----------------------------------------------------------------------
// <copyright file="XMLParser.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.XmlHelper
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Xml;
    using System.Xml.Schema;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     The xml parser.
    /// </summary>
    public static class XMLParser
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(XMLParser));

        /// <summary>
        ///     The _schema locations.
        /// </summary>
        private static readonly IDictionary<SdmxSchemaEnumType, XmlSchemaSet> _schemaLocations =
            new Dictionary<SdmxSchemaEnumType, XmlSchemaSet>();

        /// <summary>
        ///     The enable validation.
        /// </summary>
        private static bool _enableValidation = true;

        /// <summary>
        ///     Initializes static members of the <see cref="XMLParser" /> class.
        /// </summary>
        static XMLParser()
        {
            var schemaSet = new XmlSchemaSet { XmlResolver = new XmlEmbededResourceResolver("Org.xsd._1_0") };
            schemaSet.Add(null, "res:///SDMXMessage.xsd");
            schemaSet.Compile();
            _schemaLocations.Add(SdmxSchemaEnumType.VersionOne, schemaSet);

            schemaSet = new XmlSchemaSet { XmlResolver = new XmlEmbededResourceResolver("Org.xsd._2_0") };
            schemaSet.Add(null, "res:///SDMXMessage.xsd");

            // TODO do we add ESTAT or MT Extensions ?!?
            _schemaLocations.Add(SdmxSchemaEnumType.VersionTwo, schemaSet);

            schemaSet = new XmlSchemaSet { XmlResolver = new XmlEmbededResourceResolver("Org.xsd._2_1") };
            schemaSet.Add(null, "res:///SDMXMessage.xsd");
            _schemaLocations.Add(SdmxSchemaEnumType.VersionTwoPointOne, schemaSet);
        }

        /// <summary>
        /// Sets a value indicating whether enable validation.
        /// </summary>
        /// <param name="value">if set to <c>true</c> validation is enabled (default); otherwise disable.</param>
        public static void ChangeValidationState(bool value)
        {
            _enableValidation = value;
        }

        /// <summary>
        ///     Creates and returns an new instance of an <see cref="XmlReader" /> with the settings from
        ///     <see cref="GetSdmxXmlReaderSettings" />
        ///     .
        /// </summary>
        /// <param name="stream">
        ///     The input stream
        /// </param>
        /// <param name="sdmxSchema">
        ///     The SDMX Schema version
        /// </param>
        /// <returns>
        ///     A new instance of an <see cref="XmlReader" /> with the settings from <see cref="GetSdmxXmlReaderSettings" />
        /// </returns>
        public static XmlReader CreateSdmxMlReader(Stream stream, SdmxSchemaEnumType sdmxSchema)
        {
            return XmlReader.Create(stream, GetSdmxXmlReaderSettings(sdmxSchema));
        }

        /// <summary>
        ///     Creates and returns an new instance of an <see cref="XmlReaderSettings" /> for the specified
        ///     <paramref name="sdmxSchema" />
        /// </summary>
        /// <param name="sdmxSchema">
        ///     The SDMX Schema version
        /// </param>
        /// <returns>
        ///     A new instance of an <see cref="XmlReaderSettings" /> for the specified <paramref name="sdmxSchema" />
        /// </returns>
        public static XmlReaderSettings GetSdmxXmlReaderSettings(SdmxSchemaEnumType sdmxSchema)
        {
            var settings = new XmlReaderSettings
                               {
                                   IgnoreComments = true, 
                                   IgnoreProcessingInstructions = true, 
                                   IgnoreWhitespace = true
                               };

            XmlSchemaSet schemaSet;
            if (_enableValidation && _schemaLocations.TryGetValue(sdmxSchema, out schemaSet))
            {
                settings.Schemas = schemaSet;
                settings.ValidationType = ValidationType.Schema;
            }
            else
            {
                settings.ValidationType = ValidationType.None;
            }

            return settings;
        }

        /// <summary>
        ///     Validate SDMX-ML file.
        /// </summary>
        /// <param name="sourceData">
        ///     The source data.
        /// </param>
        /// <param name="schemaVersion">
        ///     The schema version.
        /// </param>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        public static void ValidateXml(Stream sourceData, SdmxSchemaEnumType schemaVersion)
        {
            XmlReaderSettings settings = GetSdmxXmlReaderSettings(schemaVersion);
            settings.ValidationEventHandler += SettingsOnValidationEventHandler;

            using (XmlReader reader = XmlReader.Create(sourceData, settings))
            {
                while (reader.Read())
                {
                }
            }
        }

        /// <summary>
        ///     Validate SDMX-ML file.
        /// </summary>
        /// <param name="sourceData">
        ///     The source data.
        /// </param>
        /// <param name="schemaVersion">
        ///     The schema version.
        /// </param>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        /// <exception cref="ArgumentNullException"><paramref name="sourceData"/> is <see langword="null" />.</exception>
        public static void ValidateXml(IReadableDataLocation sourceData, SdmxSchemaEnumType schemaVersion)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            ValidateXml(sourceData.InputStream, schemaVersion);
        }

        /// <summary>
        ///     The settings on validation event handler.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="args">
        ///     The validation event parameter.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Schema validation error
        /// </exception>
        private static void SettingsOnValidationEventHandler(object sender, ValidationEventArgs args)
        {
            Trace.WriteLine(args.Message);
            if (args.Exception == null)
            {
                return;
            }

            Trace.WriteLine(args.Exception);
            
            // Workaround for BAMBOO 
            var parameters = BuildParameters(args);
            string message = string.Format(CultureInfo.InvariantCulture, "{0} Line: {1}. Column {2} Error:\n {3}", parameters);
            Trace.WriteLine(message);
            _log.Warn(message, args.Exception);

            throw new SdmxSyntaxException(args.Exception, ExceptionCode.XmlParseException, message);
        }

        /// <summary>
        /// Builds the parameters. This is a workaround, somehow the .NET 4.6 overload is used.
        /// </summary>
        /// <param name="args">The <see cref="ValidationEventArgs"/> instance containing the event data.</param>
        /// <returns>The parameters from the specified </returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        private static object[] BuildParameters(ValidationEventArgs args)
        {
            object[] parameters = { args.Exception.SourceUri, args.Exception.LineNumber, args.Exception.LinePosition, args.Exception.Message };
            return parameters;
        }

        /////*
        //// * This class is a bit of a work-around for a possible bug that exists within the validate routine of the Validator class.
        //// * When validating an XML file against a schema the problem is that if the validation of the file fails, 
        //// * then the file becomes locked until the application terminates. 
        //// * If the input file was valid then the file does not become locked and everything is fine. 
        //// * So, when an error is encountered, it is simply stored in a List and validation continues successfully.
        //// * Once the validation has completed, this class must be asked if there were any errors and act accordingly.
        //// */
        ////protected internal class ErrorHandler : DefaultHandler {
        ////  public ErrorHandler() {
        ////      this.errors = new List<SAXParseException>();
        ////  }

        ////  private IList<SAXParseException> errors;

        ////  public IList<SAXParseException> Errors {
        ////    get {
        ////              return errors;
        ////          }
        ////  }

        ////  public override void Error(SAXParseException parseException) {
        ////      LoggingUtil.Error(_log, parseException);
        ////      ILOG.J2CsMapping.Collections.Generics.Collections.Add(errors,parseException);
        ////  }

        ////  public override void FatalError(SAXParseException parseException) {
        ////      LoggingUtil.Error(_log, parseException);
        ////      ILOG.J2CsMapping.Collections.Generics.Collections.Add(errors,parseException);
        ////  }

        ////  public override void Warning(SAXParseException parseException) {
        ////      LoggingUtil.Error(_log, parseException);
        ////      ILOG.J2CsMapping.Collections.Generics.Collections.Add(errors,parseException);
        ////  }
        ////}
    }
}