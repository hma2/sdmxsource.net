// -----------------------------------------------------------------------
// <copyright file="StaxUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Xml
{
    using System;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    ///     The stax util.
    /// </summary>
    public static class StaxUtil
    {
        /// <summary>
        ///     The jump to node.
        /// </summary>
        /// <param name="parser">
        ///     The parser.
        /// </param>
        /// <param name="findNodeName">
        ///     The find node name.
        /// </param>
        /// <param name="doNotProcessPastNodeName">
        ///     The do not process past node name.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="parser"/> is <see langword="null" />.</exception>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        public static bool JumpToNode(XmlReader parser, string findNodeName, string doNotProcessPastNodeName)
        {
            if (parser == null)
            {
                throw new ArgumentNullException("parser");
            }

            while (parser.Read())
            {
                string nodeName;
                switch (parser.NodeType)
                {
                    case XmlNodeType.Element:
                        nodeName = parser.LocalName;
                        if (nodeName.Equals(findNodeName))
                        {
                            return true;
                        }

                        break;
                    case XmlNodeType.EndElement:
                        nodeName = parser.LocalName;
                        if (!string.IsNullOrEmpty(doNotProcessPastNodeName))
                        {
                            if (nodeName.Equals(doNotProcessPastNodeName))
                            {
                                return false;
                            }
                        }

                        break;
                }
            }

            return false;
        }

        /// <summary>
        ///     Returns the current node, all attributes and children as a string.
        ///     This just calls <see cref="XmlReader.ReadOuterXml" />. Use <see cref="XmlReader.ReadOuterXml" /> instead.
        /// </summary>
        /// <param name="parser">
        ///     The xml reader
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        /// <exception cref="XmlException">The XML was not well-formed, or an error occurred while parsing the XML.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="parser"/> is <see langword="null" />.</exception>
        [Obsolete("This just calls XmlReader.ReadOuterXml(). Use XmlReader.ReadOuterXml() instead.")]
        public static string ParseString(XmlReader parser)
        {
            if (parser == null)
            {
                throw new ArgumentNullException("parser");
            }

            return parser.ReadOuterXml();
        }

        /// <summary>
        ///     Moves the parser position forward to the point after this node and all children of this node
        ///     This just calls <see cref="XmlReader.Skip" />. Use <see cref="XmlReader.Skip" /> instead.
        /// </summary>
        /// <param name="parser">
        ///     The Xml Reader
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="parser"/> is <see langword="null" />.</exception>
        [Obsolete("This just calls XmlReader.Skip(). Use XmlReader.Skip() instead.")]
        public static void SkipNode(XmlReader parser)
        {
            if (parser == null)
            {
                throw new ArgumentNullException("parser");
            }

            parser.Skip();
        }

        /// <summary>
        ///     Moves the parser position forward to the next instance of the end node given name
        /// </summary>
        /// <param name="parser">
        ///     The Xml Reader
        /// </param>
        /// <param name="nodeName">
        ///     The node Name.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        /// <exception cref="ArgumentNullException"><paramref name="parser"/> is <see langword="null" />.</exception>
        public static bool SkipToEndNode(XmlReader parser, string nodeName)
        {
            if (parser == null)
            {
                throw new ArgumentNullException("parser");
            }

            while (parser.Read())
            {
                switch (parser.NodeType)
                {
                    case XmlNodeType.Element:
                        if (parser.LocalName.Equals(nodeName))
                        {
                            if (parser.IsEmptyElement)
                            {
                                return true;
                            }
                        }

                        break;
                    case XmlNodeType.EndElement:
                        if (parser.LocalName.Equals(nodeName))
                        {
                            return true;
                        }

                        break;
                }
            }

            return false;
        }

        /// <summary>
        ///     Moves the parser position forward to the next instance of the node with the given name
        /// </summary>
        /// <param name="parser">
        ///     The xml reader
        /// </param>
        /// <param name="nodeName">
        ///     The node Name.
        /// </param>
        /// <returns>
        ///     True if the <paramref name="nodeName" /> was found; otherwise false
        /// </returns>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        /// <exception cref="ArgumentNullException"><paramref name="parser"/> is <see langword="null" />.</exception>
        public static bool SkipToNode(XmlReader parser, string nodeName)
        {
            if (parser == null)
            {
                throw new ArgumentNullException("parser");
            }

            while (parser.Read())
            {
                switch (parser.NodeType)
                {
                    case XmlNodeType.Element:
                        if (parser.LocalName.Equals(nodeName))
                        {
                            return true;
                        }

                        break;
                }
            }

            return false;
        }

        /// <summary>
        ///     The write header.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="senderId">
        ///     The sender id.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="writer"/> is <see langword="null" />.</exception>
        public static void WriteHeader(XmlWriter writer, string id, string senderId)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            if (id == null)
            {
                throw new ArgumentNullException("id");
            }

            if (senderId == null)
            {
                throw new ArgumentNullException("senderId");
            }

            writer.WriteStartElement("Header");

            // WRITE ID
            writer.WriteStartElement("ID");
            writer.WriteString(id);
            writer.WriteEndElement();

            // WRITE TEST
            writer.WriteStartElement("Test");
            writer.WriteString("false");
            writer.WriteEndElement();

            // WRITE PREPARED
            writer.WriteStartElement("Prepared");
            writer.WriteString(DateUtil.FormatDate(DateTime.Now));
            writer.WriteEndElement();

            // WRITE SENDER
            writer.WriteStartElement("Sender");
            writer.WriteAttributeString("id", senderId);
            writer.WriteEndElement();

            // END HEADER
            writer.WriteEndElement();
        }
    }
}