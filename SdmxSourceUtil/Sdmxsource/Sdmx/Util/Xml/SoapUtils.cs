﻿// -----------------------------------------------------------------------
// <copyright file="SoapUtils.cs" company="EUROSTAT">
//   Date Created : 2013-09-13
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Xml
{
    using System;
    using System.IO;
    using System.Security;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;

    /// <summary>
    ///     The soap utils.
    /// </summary>
    public static class SoapUtils
    {
        /// <summary>
        /// Extract message from soap
        /// </summary>
        /// <param name="input">
        /// The input
        /// </param>
        /// <param name="writer">
        /// The writer
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="input"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        public static void ExtractSdmxMessage(Stream input, XmlWriter writer)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }

            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            using (var reader = XmlReader.Create(input))
            {
                SdmxMessageUtil.FindSdmx(reader);
                if (reader.EOF)
                {
                    return;
                }

                writer.WriteStartDocument();
                writer.WriteNode(reader, false);
            }
        }

        /// <summary>
        /// Extracts the SDMX message with comment.
        /// </summary>
        /// <param name="readableDataLocation">
        /// The readable data location.
        /// </param>
        /// <param name="writer">
        /// The writer.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// input
        /// or
        /// writer
        /// </exception>
        public static void ExtractSdmxMessageWithComment(IReadableDataLocation readableDataLocation, XmlWriter writer)
        {
            if (readableDataLocation == null)
            {
                throw new ArgumentNullException("readableDataLocation");
            }

            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            using (var reader = XmlReader.Create(readableDataLocation.InputStream))
            {
                SdmxMessageUtil.FindSdmx(reader);
                if (reader.EOF)
                {
                    return;
                }

                writer.WriteStartDocument();
                var comment = GetComment(readableDataLocation.InputStream);
                if (!string.IsNullOrEmpty(comment))
                {
                    writer.WriteComment(comment);
                }

                writer.WriteNode(reader, false);
            }
        }

        /// <summary>
        /// Gets the comment.
        /// </summary>
        /// <param name="commentStream">
        /// The comment stream.
        /// </param>
        /// <returns>
        /// The comment from the <paramref name="commentStream"/>
        /// </returns>
        private static string GetComment(Stream commentStream)
        {
            using (var reader = XmlReader.Create(commentStream))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Comment:
                            return reader.Value;
                    }
                }
            }

            return string.Empty;
        }
    }
}