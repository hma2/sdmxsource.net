﻿// -----------------------------------------------------------------------
// <copyright file="StructureExtension.cs" company="EUROSTAT">
//   Date Created : 2013-07-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Extension
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The structure extensions methods.
    /// </summary>
    public static class StructureExtension
    {
        /// <summary>
        ///     Changes the stars <c>*</c>  to null.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <returns>the structure reference</returns>
        public static IStructureReference ChangeStarsToNull(this IStructureReference reference)
        {
            if (reference == null)
            {
                return null;
            }

            if (!reference.HasAgencyId() && !reference.HasMaintainableId() && !reference.HasVersion())
            {
                return reference;
            }

            if (SdmxConstants.RestWildcard.Equals(reference.AgencyId)
                || SdmxConstants.RestWildcard.Equals(reference.MaintainableId)
                || SdmxConstants.RestWildcard.Equals(reference.Version))
            {
                string agencyId = SdmxConstants.RestWildcard.Equals(reference.AgencyId) ? null : reference.AgencyId;
                string version = SdmxConstants.RestWildcard.Equals(reference.Version) ? null : reference.Version;
                string id = SdmxConstants.RestWildcard.Equals(reference.MaintainableId)
                                ? null
                                : reference.MaintainableId;
                if (reference.HasChildReference())
                {
                    return new StructureReferenceImpl(
                        agencyId, 
                        id, 
                        version, 
                        reference.TargetReference, 
                        reference.IdentifiableIds);
                }

                return new StructureReferenceImpl(agencyId, id, version, reference.TargetReference);
            }

            return reference;
        }

        /// <summary>
        ///     Returns the id from the leaf <see cref="IIdentifiableRefObject" />
        /// </summary>
        /// <param name="identifiableRefObject">
        ///     The <see cref="IIdentifiableRefObject" />.
        /// </param>
        /// <returns>
        ///     The the id from the leaf <see cref="IIdentifiableRefObject" /> from the specified
        ///     <paramref name="identifiableRefObject" />; otherwise null.
        /// </returns>
        public static string GetLastId(this IIdentifiableRefObject identifiableRefObject)
        {
            if (identifiableRefObject == null)
            {
                return null;
            }

            while (identifiableRefObject.ChildReference != null)
            {
                identifiableRefObject = identifiableRefObject.ChildReference;
            }

            return identifiableRefObject.Id;
        }
    }
}