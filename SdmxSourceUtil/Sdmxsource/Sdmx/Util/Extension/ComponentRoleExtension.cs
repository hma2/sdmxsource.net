﻿// -----------------------------------------------------------------------
// <copyright file="ComponentRoleExtension.cs" company="EUROSTAT">
//   Date Created : 2016-06-10
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Extension
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Some extension methods to the <see cref="ComponentRole"/>
    /// </summary>
    public static class ComponentRoleExtension
    {
        /// <summary>
        /// The <paramref name="role"/> as a dimension attribute.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>The corresponding dimension attribute if such exists or null.</returns>
        public static string AsDimensionAttribute(this ComponentRole role)
        {
            switch (role)
            {
                case ComponentRole.Count:
                    return "isCountDimension";
                case ComponentRole.NonObservationalTime:
                    return "isNonObservationTimeDimension";
                case ComponentRole.Identity:
                    return "isIdentityDimension";
                case ComponentRole.Entity:
                    return "isEntityDimension";
            }

            return null;
        }

        /// <summary>
        /// The <paramref name="role"/> as a dimension attribute.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>The corresponding dimension attribute if such exists or null.</returns> 
        public static string AsDataAttributeAttribute(this ComponentRole role)
        {
            switch (role)
            {
                case ComponentRole.Count:
                    return "isCountAttribute";
                case ComponentRole.NonObservationalTime:
                    return "isNonObservationalTimeAttribute";
                case ComponentRole.Identity:
                    return "isIdentityAttribute";
                case ComponentRole.Entity:
                    return "isEntityAttribute";
                case ComponentRole.Frequency:
                    return "isFrequencyAttribute";
            }

            return null;
        }
    }
}