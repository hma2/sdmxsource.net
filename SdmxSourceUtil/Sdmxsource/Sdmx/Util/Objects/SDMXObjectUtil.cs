// -----------------------------------------------------------------------
// <copyright file="SDMXObjectUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Objects
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The sdmx object util.
    /// </summary>
    public static class SdmxObjectUtil
    {
        /// <summary>
        ///     The _false value.
        /// </summary>
        private static readonly TertiaryBool _falseValue = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False);

        /// <summary>
        ///     The _true value.
        /// </summary>
        private static readonly TertiaryBool _trueValue = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);

        /// <summary>
        ///     The _unset value.
        /// </summary>
        private static readonly TertiaryBool _unsetValue = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

        /// <summary>
        ///     Create tertiary from the specified <paramref name="isSet" /> and <paramref name="valueRen" />
        /// </summary>
        /// <param name="isSet">
        ///     The is set.
        /// </param>
        /// <param name="valueRen">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="TertiaryBool" />.
        /// </returns>
        public static TertiaryBool CreateTertiary(bool isSet, bool valueRen)
        {
            if (!isSet)
            {
                return _unsetValue;
            }

            return valueRen ? _trueValue : _falseValue;
        }

        /// <summary>
        ///     Create tertiary from the specified  and <paramref name="valueRen" />
        /// </summary>
        /// <param name="valueRen">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The corresponding <see cref="TertiaryBool" /> if <paramref name="valueRen" /> is not null; otherwise
        ///     <see cref="TertiaryBoolEnumType.Unset" />.
        /// </returns>
        public static TertiaryBool CreateTertiary(bool? valueRen)
        {
            if (!valueRen.HasValue)
            {
                return _unsetValue;
            }

            return valueRen.Value ? _trueValue : _falseValue;
        }
    }
}