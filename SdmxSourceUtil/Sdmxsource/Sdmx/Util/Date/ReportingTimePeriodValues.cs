﻿// -----------------------------------------------------------------------
// <copyright file="ReportingTimePeriodValues.cs" company="EUROSTAT">
//   Date Created : 2017-09-21
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Text.RegularExpressions;
    using System.Xml;

    using Api.Constants;

    /// <summary>
    /// This model class represents a Reporting Time Period
    /// </summary>
    public class ReportingTimePeriodValues
    {
        /// <summary>
        /// The reporting period
        /// </summary>
        private readonly string _reportingPeriod;
        
        /// <summary>
        /// The reporting year start date string
        /// </summary>
        private readonly string _reportingYearStartDateString;

        /// <summary>
        /// The frequency
        /// </summary>
        private TimeFormat _frequency;

        /// <summary>
        /// The reporting year start date
        /// </summary>
        private DateTime? _reportingYearStartDate;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingTimePeriodValues"/> class. 
        /// </summary>
        /// <param name="reportingPeriod">
        /// The reporting period.
        /// </param>
        /// <param name="reportingYearStartDateString">
        /// The reporting year start date string.
        /// </param>
        public ReportingTimePeriodValues(string reportingPeriod, string reportingYearStartDateString)
        {
            this._reportingPeriod = reportingPeriod;
            this._reportingYearStartDateString = reportingYearStartDateString;
        }

        /// <summary>
        /// Gets the period value.
        /// </summary>
        /// <value>
        /// The period value.
        /// </value>
        public int PeriodValue
        {
            get { return int.Parse(Regex.Match(this._reportingPeriod, @"(\d+)[^-]*$").Value); }
        }

        /// <summary>
        /// Gets the reporting year start date.
        /// </summary>
        /// <value>
        /// The reporting year start date.
        /// </value>
        public DateTime ReportingYearStartDate
        {
            get
            {
                if (this._reportingYearStartDate == null)
                {
                    var year = int.Parse(Regex.Match(this._reportingPeriod, @".+?(?=-)").Value);
                    var dateTime = XmlConvert.ToDateTime(this._reportingYearStartDateString, XmlDateTimeSerializationMode.Utc);
                    dateTime = dateTime.AddYears(year - dateTime.Year);
                    this._reportingYearStartDate = dateTime;
                }

                return this._reportingYearStartDate.Value;
            }
        }

        /// <summary>
        /// Gets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public TimeFormat Frequency
        {
            get
            {
                var codeId = Regex.Match(this._reportingPeriod, @"[a-zA-Z]").Value;
                if (string.IsNullOrWhiteSpace(codeId))
                {
                    return null;
                }

                return this._frequency ?? (this._frequency = TimeFormat.GetTimeFormatFromCodeId(codeId));
            }
        }
    }
}