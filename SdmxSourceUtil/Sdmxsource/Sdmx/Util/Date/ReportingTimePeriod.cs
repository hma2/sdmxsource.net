﻿// -----------------------------------------------------------------------
// <copyright file="ReportingTimePeriod.cs" company="EUROSTAT">
//   Date Created : 2017-09-18
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// This class allows to convert between Gregorian Time periods and Reporting Time Periods
    /// </summary>
    public class ReportingTimePeriod
    {
        /// <summary>
        /// A reporting quarter represents a period of 3 months (P3M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingQuarterType.
        /// </summary>
        private readonly int P3M = 3;

        /// <summary>
        /// A reporting trimester represents a period of 4 months (P4M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingTrimesterType.
        /// </summary>
        private readonly int P4M = 4;

        /// <summary>
        /// A reporting semester represents a period of 6 months (P6M) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingSemesterType.
        /// </summary>
        private readonly int P6M = 6;

        /// <summary>
        /// A reporting week represents a period of 7 days (P7D) from the start date of the reporting year. This is expressed as using the SDMX specific ReportingWeekType.
        /// </summary>
        private readonly int P7D = 7;

        /// <summary>
        /// The weekly
        /// </summary>
        private readonly Weekly _weekly = new Weekly();

        /// <summary>
        /// The reporting period pattern
        /// </summary>
        private readonly Regex _reportingPeriodPattern = new Regex(@"^\d{4}\-(A1|S[1-2]|T[1-3]|Q[1-4]|M(0[1-9]|1[0-2])|W(0[1-9]|[1-4][0-9]|5[0-3])|D(0[0-9][1-9]|[1-2][0-9][0-9]|3[0-5][0-9]|36[0-6]))$", RegexOptions.Compiled);

        /// <summary>
        /// The reporting start date pattern
        /// </summary>
        private readonly Regex _reportingStartDatePattern = new Regex(@"^--(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$", RegexOptions.Compiled);

        /// <summary>
        /// The gregorian frequencies TODO use Enum
        /// </summary>
        private readonly string[] _gregorianFrequencies = new[] { "Q", "T", "S", "W" };

        /// <summary>
        /// Parse two strings to a gregorian period
        /// </summary>
        /// <param name="reportingPeriod">The reporting period.</param>
        /// <param name="reportingYearStartDateString">The reporting year start date string.</param>
        /// <returns>
        /// The <see cref="SDMXGregorianPeriod" />.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Reporting period is not valid</exception>
        public SDMXGregorianPeriod ToGregorianPeriod(string reportingPeriod, string reportingYearStartDateString)
        {
            if (string.IsNullOrWhiteSpace(reportingYearStartDateString))
            {
                reportingYearStartDateString = "--01-01";
            }

            var reportingTimePeriodValues = new ReportingTimePeriodValues(reportingPeriod, reportingYearStartDateString);
            var periodIndicator = reportingTimePeriodValues.Frequency;

            if (this._gregorianFrequencies.Contains(periodIndicator.FrequencyCode) && !this.CheckReportingPeriod(reportingPeriod, reportingYearStartDateString))
            {
                return new SDMXGregorianPeriod(DateUtil.FormatDate(reportingPeriod, true), periodIndicator);
            }

            if (!this._gregorianFrequencies.Contains(periodIndicator.FrequencyCode) && !this.CheckReportingPeriod(reportingPeriod, reportingYearStartDateString))
            {
                throw new SdmxSemmanticException(string.Format(CultureInfo.InvariantCulture, "reporting period composed from {0} and {1} is not valid", reportingPeriod, reportingYearStartDateString));
            }

            var reportingYearStartDate = reportingTimePeriodValues.ReportingYearStartDate;
            var reportingYearBase = reportingYearStartDate;
            var periodValue = reportingTimePeriodValues.PeriodValue;
            if (periodIndicator.FrequencyCode == this._weekly.TimeFormat.FrequencyCode)
            {
                reportingYearBase = SetDateToStartOfTheWeek(reportingYearStartDate, reportingYearStartDate.DayOfWeek);
            }

            var periodStart = DateTime.MinValue;
            var periodValueForStart = periodValue - 1;

            // TODO reuse MonthsPerPeriod from IPeriodicity
            switch (periodIndicator.EnumType)
            {
                case TimeFormatEnumType.Year:
                    periodStart = reportingYearBase.AddYears(periodValueForStart);
                    break;
                case TimeFormatEnumType.HalfOfYear:
                    periodStart = reportingYearBase.AddMonths(periodValueForStart * this.P6M);
                    break;
                case TimeFormatEnumType.ThirdOfYear:
                    periodStart = reportingYearBase.AddMonths(periodValueForStart * this.P4M);
                    break;
                case TimeFormatEnumType.QuarterOfYear:
                    periodStart = reportingYearBase.AddMonths(periodValueForStart * this.P3M);
                    break;
                case TimeFormatEnumType.Month:
                    periodStart = reportingYearBase.AddMonths(periodValueForStart);
                    break;
                case TimeFormatEnumType.Week:
                    periodStart = reportingYearBase.AddDays(periodValueForStart * this.P7D);
                    break;
                case TimeFormatEnumType.Date:
                    periodStart = reportingYearBase.AddDays(periodValueForStart);
                    break;
            }

            return new SDMXGregorianPeriod(periodStart, periodIndicator);
        }

        /// <summary>
        /// Checks if reporting period strings are valid
        /// </summary>
        /// <param name="reportingPeriod">The reporting period.</param>
        /// <param name="reportingYearStartDateString">The reporting year start date string.</param>
        /// <returns>
        ///   <c>true</c> if <paramref name="reportingPeriod" /> is a reporting period (from SdmxSource point of view); otherwise false.
        /// </returns>
        public bool CheckReportingPeriod(string reportingPeriod, string reportingYearStartDateString)
        {
            var frequency = new ReportingTimePeriodValues(reportingPeriod, reportingYearStartDateString).Frequency;
            if (frequency == null)
            {
                return false;
            }

            if (this._gregorianFrequencies.Contains(frequency.FrequencyCode) && string.IsNullOrWhiteSpace(reportingYearStartDateString))
            {
                return false;
            }

            if (!this._gregorianFrequencies.Contains(frequency.FrequencyCode) && string.IsNullOrWhiteSpace(reportingYearStartDateString))
            {
                return this._reportingPeriodPattern.IsMatch(reportingPeriod);
            }

            return this._reportingPeriodPattern.IsMatch(reportingPeriod) && this._reportingStartDatePattern.IsMatch(reportingYearStartDateString);
        }

        /// <summary>
        /// Convert the gregorian time to a reporting period, using as offset the specified <paramref name="reportingYearStartDate"/>
        /// </summary>
        /// <param name="sdmxDate">The SDMX date.</param>
        /// <param name="reportingYearStartDate">The reporting year start date.</param>
        /// <returns>The reporting period</returns>
        public string ToReportingPeriod(ISdmxDate sdmxDate, string reportingYearStartDate)
        {
            var frequencyCode = sdmxDate.TimeFormatOfDate.FrequencyCode;
            var dateValue = sdmxDate.Date.Value;
            int periodValue;
            var periodString = string.Empty;
            var periodStart = DateTime.MinValue;
            var utcDateTime = XmlConvert.ToDateTime(reportingYearStartDate, XmlDateTimeSerializationMode.Utc);
            var valueToAdd = dateValue.IsMonthAndDayAfter(utcDateTime) ? dateValue.Year - utcDateTime.Year : dateValue.Year - utcDateTime.Year - 1;
            var startDate = utcDateTime.AddYears(valueToAdd);
            switch (sdmxDate.TimeFormatOfDate.EnumType)
            {
                case TimeFormatEnumType.Year:
                    periodString = "1";
                    periodStart = startDate;
                    break;
                case TimeFormatEnumType.HalfOfYear:
                    periodValue = dateValue.MonthDifference(startDate) / this.P6M + 1;
                    periodStart = dateValue.AddMonths((-periodValue + 1) * this.P6M);
                    periodString = periodValue.ToString(CultureInfo.InvariantCulture);
                    break;
                case TimeFormatEnumType.ThirdOfYear:
                    periodValue = dateValue.MonthDifference(startDate) / this.P4M + 1;
                    periodStart = dateValue.AddMonths((-periodValue + 1) * this.P4M);
                    periodString = periodValue.ToString(CultureInfo.InvariantCulture);
                    break;
                case TimeFormatEnumType.QuarterOfYear:
                    periodValue = dateValue.MonthDifference(startDate) / this.P3M + 1;
                    periodStart = dateValue.AddMonths((-periodValue + 1) * this.P3M);
                    periodString = periodValue.ToString(CultureInfo.InvariantCulture);
                    break;
                case TimeFormatEnumType.Month:
                    periodValue = dateValue.MonthDifference(startDate) + 1;
                    periodStart = dateValue.AddMonths(-periodValue + 1);
                    periodString = periodValue.ToString().PadLeft(2, '0');
                    break;
                case TimeFormatEnumType.Week:

                    startDate = SetDateToStartOfTheWeek(startDate, utcDateTime.DayOfWeek);

                    periodValue = dateValue.Subtract(startDate).Days / this.P7D + 1;
                    periodStart = dateValue.AddDays((-periodValue + 1) * this.P7D);
                    periodString = periodValue.ToString().PadLeft(2, '0');
                    break;
                case TimeFormatEnumType.Date:
                    periodValue = dateValue.Subtract(startDate).Days + 1;
                    periodStart = dateValue.AddDays(-periodValue + 1);
                    periodString = periodValue.ToString().PadLeft(3, '0');
                    break;
            }

            return periodStart.Year + "-" + frequencyCode + periodString;
        }

        /// <summary>
        /// Sets the date to start of the week.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns>The date to the start of the week</returns>
        private static DateTime SetDateToStartOfTheWeek(DateTime startDate, DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Friday:
                    startDate = startDate.AddDays(3);
                    break;
                case DayOfWeek.Saturday:
                    startDate = startDate.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    startDate = startDate.AddDays(1);
                    break;
                case DayOfWeek.Tuesday:
                    startDate = startDate.AddDays(-1);
                    break;
                case DayOfWeek.Wednesday:
                    startDate = startDate.AddDays(-2);
                    break;
                case DayOfWeek.Thursday:
                    startDate = startDate.AddDays(-3);
                    break;
            }

            return startDate;
        }
    }
}