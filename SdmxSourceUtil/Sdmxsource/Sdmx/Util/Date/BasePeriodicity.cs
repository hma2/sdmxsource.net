﻿// -----------------------------------------------------------------------
// <copyright file="BasePeriodicity.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The base periodicity class.
    /// </summary>
    public abstract class BasePeriodicity
    {
        /// <summary>
        ///     The default calendar instance
        /// </summary>
        private static readonly Calendar _calendar = CultureInfo.InvariantCulture.Calendar;

        /// <summary>
        ///     The time format
        /// </summary>
        private readonly TimeFormat _timeFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BasePeriodicity" /> class.
        /// </summary>
        /// <param name="timeFormat">
        ///     The time format.
        /// </param>
        protected BasePeriodicity(TimeFormatEnumType timeFormat)
        {
            this._timeFormat = TimeFormat.GetFromEnum(timeFormat);
        }

        /// <summary>
        ///     Gets the time format
        /// </summary>
        public TimeFormat TimeFormat
        {
            get
            {
                return this._timeFormat;
            }
        }

        /// <summary>
        ///     Gets the default calendar instance
        /// </summary>
        protected internal static Calendar Calendar
        {
            get
            {
                return _calendar;
            }
        }
    }
}