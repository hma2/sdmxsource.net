﻿// -----------------------------------------------------------------------
// <copyright file="SDMXGregorianPeriod.cs" company="EUROSTAT">
//   Date Created : 2017-09-27
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtil.
// 
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    /// An object representing a Gregorian Period. This is an internal object. It shouldn't be used outside SdmxSourceUtil
    /// For SdmxSource Time period representation please use <see cref="ISdmxDate"/>
    /// </summary>
    public class SDMXGregorianPeriod
    {
        /// <summary>
        /// The frequency
        /// </summary>
        private readonly TimeFormat _frequency;

        /// <summary>
        /// The period start
        /// </summary>
        private readonly DateTime _periodStart;

        /// <summary>
        /// Initializes a new instance of the <see cref="SDMXGregorianPeriod"/> class.
        /// </summary>
        /// <param name="periodStart">
        /// The period start.
        /// </param>
        /// <param name="frequency">
        /// The frequency.
        /// </param>
        public SDMXGregorianPeriod(DateTime periodStart, TimeFormat frequency)
        {
            this._periodStart = periodStart;
            this._frequency = frequency;
        }

        /// <summary>
        /// Gets the frequency.
        /// </summary>
        /// <value>
        /// The frequency.
        /// </value>
        public TimeFormat Frequency
        {
            get
            {
                return this._frequency;
            }
        }

        /// <summary>
        /// Gets the period start.
        /// </summary>
        /// <value>
        /// The period start.
        /// </value>
        public DateTime PeriodStart
        {
            get
            {
                return this._periodStart;
            }
        }
    }
}