// -----------------------------------------------------------------------
// <copyright file="Weekly.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Util.Date
{
    using System;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Weekly frequency
    /// </summary>
    public class Weekly : BasePeriodicity, IPeriodicity
    {
        /// <summary>
        ///     The SDMX date format
        /// </summary>
        public const string SdmxDateTimeFormat = "{0:yyyy}-W{1}";

        /// <summary>
        ///     When the period digit starts
        /// </summary>
        private const byte DigitStartIndex = 1;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const byte NumberOfMonths = 0;

        /// <summary>
        ///     The number of months in a period
        /// </summary>
        private const short NumberOfPeriods = 52;

        /// <summary>
        ///     The SDMX period only format
        /// </summary>
        private const string SdmxFormat = "\\W0";

        /// <summary>
        ///     GESMES period information
        /// </summary>
        private readonly GesmesPeriod _gesmesPeriod;

        /// <summary>
        /// The calendar
        /// </summary>
        private readonly GregorianCalendar _calendar = new GregorianCalendar();

        /// <summary>
        ///     Initializes a new instance of the <see cref="Weekly" /> class
        /// </summary>
        public Weekly()
            : base(TimeFormatEnumType.Week)
        {
            this._gesmesPeriod = new GesmesPeriod(this)
                                     {
                                         DateFormat = EdiTimeFormat.Week, 
                                         PeriodFormat = "0", 
                                         PeriodMax = NumberOfPeriods + 1
                                     };
        }

        /// <summary>
        ///     Gets when the digit starts inside a period time SDMX Time. (For SDMX TIme Period)
        ///     e.g. in Quarterly the digit x is second character "Qx" so starting from 0 it is 1
        /// </summary>
        public byte DigitStart
        {
            get
            {
                return DigitStartIndex;
            }
        }

        /// <summary>
        ///     Gets the format of the period for using it with int.ToString(string format,NumberFormatInfo)
        ///     <see cref="System.Int32" />
        ///     E.g. for monthly is "00" for quarterly is "\\Q0" (For SDMX TIme Period)
        /// </summary>
        public string Format
        {
            get
            {
                return SdmxFormat;
            }
        }

        /// <summary>
        ///     Gets the <see cref="GesmesPeriod" />
        /// </summary>
        public GesmesPeriod Gesmes
        {
            get
            {
                return _gesmesPeriod;
            }
        }

        /// <summary>
        ///     Gets the number of months in a period e.g. 1 for Monthly, 3 for quarterly (For SDMX TIme Period)
        /// </summary>
        public byte MonthsPerPeriod
        {
            get
            {
                return NumberOfMonths;
            }
        }

        /// <summary>
        ///     Gets the number of periods in a period e.g. 12 for monthly (For SDMX TIme Period)
        /// </summary>
        public short PeriodCount
        {
            get
            {
                return NumberOfPeriods;
            }
        }

        /// <summary>
        ///     Gets the period prefix if any
        /// </summary>
        public char PeriodPrefix
        {
            get
            {
                return FrequencyConstants.Weekly;
            }
        }

        /// <summary>
        ///     Add period to the specified <paramref name="dateTime" /> and returns the result.
        /// </summary>
        /// <param name="dateTime">
        ///     The date time.
        /// </param>
        /// <returns>
        ///     The result of adding period to the specified <paramref name="dateTime" />
        /// </returns>
        public DateTime AddPeriod(DateTime dateTime)
        {
            return Calendar.AddWeeks(dateTime, 1);
        }

        /// <summary>
        ///     Convert an SDMX Time Period type to a System.DateTime object
        /// </summary>
        /// <param name="sdmxPeriod">
        ///     A string with the SDMX Time period
        /// </param>
        /// <param name="start">
        ///     If it is true it will expand to the start of the period else towards the end e.g. if it is true 2001-04 will become
        ///     2001-04-01 else it will become 2001-04-30
        /// </param>
        /// <returns>
        ///     A DateTime object
        /// </returns>
        public DateTime ToDateTime(string sdmxPeriod, bool start)
        {
            var ret = new DateTime();
            if (!string.IsNullOrEmpty(sdmxPeriod))
            {
                string[] dateFields = sdmxPeriod.Split(new[] { '-' }, 3);
                if (dateFields.Length == 2)
                {
                    int period = Convert.ToInt32(dateFields[1].Substring(1), CultureInfo.InvariantCulture);
                    int year = Convert.ToInt32(dateFields[0].Substring(0, 4), CultureInfo.InvariantCulture);
                    ret = new DateTime(year, 1, 1).AddDays(period * 7);
                }
            }

            return ret;
        }

        /// <summary>
        ///     Convert the specified <paramref name="time" /> to SDMX Time Period type representation
        /// </summary>
        /// <param name="time">
        ///     The <see cref="DateTime" /> object to convert
        /// </param>
        /// <returns>
        ///     A string with the SDMX Time Period
        /// </returns>
        public string ToString(DateTime time)
        {
            IFormatProvider fmt = CultureInfo.InvariantCulture;
            return string.Format(
                fmt, 
                SdmxDateTimeFormat, 
                time,
                _calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday));
        }
    }
}