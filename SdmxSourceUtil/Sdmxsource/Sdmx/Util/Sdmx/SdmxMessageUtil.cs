// -----------------------------------------------------------------------
// <copyright file="SdmxMessageUtil.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtil.
//     SdmxSourceUtil is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtil is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtil.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Util.Sdmx
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Xml;
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Util.Xml;

    #endregion

    /// <summary>
    ///     The <c>SDMX</c> message utility class
    /// </summary>
    public static class SdmxMessageUtil
    {
        /// <summary>
        /// Find SDMX message. Moves to the first SDMX element.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="reader"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        public static void FindSdmx(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:

                        // should cover most cases
                        switch (reader.NamespaceURI)
                        {
                            case SdmxConstants.MessageNs10:
                            case SdmxConstants.MessageNs20:
                            case SdmxConstants.MessageNs21:
                                return;
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// Returns the first SDMX element
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        /// <returns>
        /// The <see cref="XElement"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="document"/> is <see langword="null"/>.
        /// </exception>
        public static XElement FindSdmx(XDocument document)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            var sdmxNamespaces = new[] { SdmxConstants.MessageNs10, SdmxConstants.MessageNs20, SdmxConstants.MessageNs21 };
            return document.Descendants().FirstOrDefault(d => sdmxNamespaces.Contains(d.Name.NamespaceName));
        }

        /// <summary>
        /// Returns the dataset action
        /// </summary>
        /// <param name="sourceData">
        /// The source Data.
        /// </param>
        /// <returns>
        /// The <see cref="DatasetActionEnumType"/> .
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="sourceData"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        public static DatasetActionEnumType GetDataSetAction(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            MessageEnumType message = GetMessageType(sourceData);
            bool continueToSubmitStructureRequest = false;
            if (message == MessageEnumType.RegistryInterface)
            {
                RegistryMessageEnumType registryMessageType = GetRegistryMessageType(sourceData);
                if (registryMessageType == RegistryMessageEnumType.SubmitStructureRequest)
                {
                    continueToSubmitStructureRequest = true;
                }
            }

            string action = null;
            using (Stream stream = sourceData.InputStream)
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    while (reader.Read())
                    {
                        string currentNode;
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                currentNode = reader.LocalName;
                                switch (currentNode)
                                {
                                    case "SubmitStructureRequest":
                                        string attribute = reader.GetAttribute("action");
                                        if (attribute != null)
                                        {
                                            return DatasetAction.GetAction(attribute).EnumType;
                                        }

                                        return DatasetActionEnumType.Append;
                                }

                                break;

                            case XmlNodeType.EndElement:
                                currentNode = reader.LocalName;
                                switch (currentNode)
                                {
                                    case "DataSetAction":

                                        switch (action)
                                        {
                                            case "Append":
                                            case "Update":
                                                return DatasetActionEnumType.Append;
                                            case "Replace":
                                                return DatasetActionEnumType.Replace;
                                            case "Delete":
                                                return DatasetActionEnumType.Delete;
                                            case "Information":
                                                return DatasetActionEnumType.Information;
                                        }

                                        break;
                                    case "Header":
                                        if (!continueToSubmitStructureRequest)
                                        {
                                            return DatasetActionEnumType.Append;
                                        }

                                        break;
                                }

                                break;
                            case XmlNodeType.Text:
                                action = reader.Value;
                                break;
                        }
                    }
                }
            }

            return DatasetActionEnumType.Append;
        }

        /// <summary>
        /// Returns MESSAGE_TYPE for the URI.  This is determined from the root node.
        /// </summary>
        /// <param name="sourceData">
        /// The source Data.
        /// </param>
        /// <returns>
        /// The <see cref="MessageEnumType"/> of the <c>SDMX</c> message in
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="sourceData"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        /// <exception cref="SdmxSyntaxException">
        /// No root node found
        /// </exception>
        public static MessageEnumType GetMessageType(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            if (IsEdi(sourceData))
            {
                return MessageEnumType.SdmxEdi;
            }

            using (Stream stream = sourceData.InputStream)
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            string rootNode = reader.LocalName;
                            try
                            {
                                return MessageType.ParseString(rootNode).EnumType;
                            }
                            catch (ArgumentException argumentException)
                            {
                                throw new SdmxSyntaxException(argumentException, ExceptionCode.ParseErrorNotSdmx);
                            }
                        }
                    }
                }
            }

            // TODO should return a value indicating that this is not a recognized format. Which is not exceptional.
            throw new SdmxSyntaxException("No root node found");
        }

        /// <summary>
        /// Determines the query message types for an input SDMX-ML query message
        /// </summary>
        /// <param name="sourceData">
        /// The source data.
        /// </param>
        /// <returns>
        /// For v2.1 it will be always one query message due to schema constraints.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// "Can not determine query type
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="sourceData"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        public static IList<QueryMessageEnumType> GetQueryMessageTypes(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            MessageEnumType messageType = GetMessageType(sourceData);
            if (messageType != MessageEnumType.Query)
            {
                throw new ArgumentException("Can not determine query type, as message is of type : " + messageType);
            }

            IList<QueryMessageEnumType> returnList = new List<QueryMessageEnumType>();

            using (Stream stream = sourceData.InputStream)
            using (XmlReader reader = XmlReader.Create(stream, new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true, IgnoreProcessingInstructions = true }))
            {
                reader.MoveToContent();

                // Determine by the root node if it is a v2.1 DataQuery
                var rootStartEvent = reader.NodeType;
                if (rootStartEvent != XmlNodeType.Element)
                {
                    throw new ArgumentException("Could not get root start element!");
                }

                string rootNode = reader.LocalName;

                if (rootNode.Equals(QueryMessageType.GetFromEnum(QueryMessageEnumType.GenericDataQuery).NodeName) || rootNode.Equals(QueryMessageType.GetFromEnum(QueryMessageEnumType.StructureSpecificDataQuery).NodeName)
                    || rootNode.Equals(QueryMessageType.GetFromEnum(QueryMessageEnumType.GenericTimeseriesDataQuery).NodeName)
                    || rootNode.Equals(QueryMessageType.GetFromEnum(QueryMessageEnumType.StructureSpecificTimeseriesDataQuery).NodeName)
                    || rootNode.Equals(QueryMessageType.GetFromEnum(QueryMessageEnumType.GenericMetadataQuery).NodeName) || rootNode.Equals(QueryMessageType.GetFromEnum(QueryMessageEnumType.StructureSpecificMetadataQuery).NodeName))
                {
                    // don't like but checking with parsingString exception is worse. May be boolean isQueryMessageType() in enum
                    returnList.Add(QueryMessageType.ParseString(rootNode).EnumType);
                    return returnList;
                }

                StaxUtil.SkipToNode(reader, "Query");

                // continue determine type by  Where elements after Query 
                bool getOut = false;
                while (!getOut && reader.Read())
                {
                    string nodeName;
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            nodeName = reader.LocalName;

                            if (nodeName.Equals("ReturnDetails"))
                            {
                                // Skip details of 2.1 queries
                                StaxUtil.SkipToEndNode(reader, "ReturnDetails");
                            }
                            else
                            {
                                var queryMessageType = QueryMessageType.ParseString(nodeName);
                                if (queryMessageType != null)
                                {
                                    returnList.Add(queryMessageType.EnumType);
                                }

                                reader.Skip();
                            }

                            break;
                        case XmlNodeType.EndElement:
                            nodeName = reader.LocalName;
                            getOut = "Query".Equals(nodeName);
                            break;
                    }
                }
            }

            return returnList;
        }

        /// <summary>
        /// Returns the registry message type - this will only work if the XML at the
        ///     URI is a RegistryInterface message.
        ///     The registry message type may be one of the following:
        ///     <ul>
        ///         <li>SUBMIT STRUCTURE REQUEST</li>
        ///         <li>SUBMIT PROVISION REQUEST</li>
        ///         <li>SUBMIT REGISTRATION REQUEST</li>
        ///         <li>SUBMIT SUBSCRIPTION REQUEST</li>
        ///         <li>QUERY STRUCTURE REQUEST</li>
        ///         <li>QUERY PROVISION REQUEST</li>
        ///         <li>QUERY REGISTRATION REQUEST</li>
        ///         <li>SUBMIT STRUCTURE RESPONSE</li>
        ///         <li>SUBMIT PROVISION RESPONSE</li>
        ///         <li>SUBMIT REGISTRATION RESPONSE</li>
        ///         <li>QUERY STRUCTURE RESPONSE</li>
        ///         <li>QUERY PROVISION RESPONSE</li>
        ///         <li>QUERY REGISTRATION RESPONSE</li>
        ///         <li>SUBMIT SUBSCRIPTION RESPONSE</li>
        ///         <li>NOTIFY REGISTRY EVENT</li>
        ///     </ul>
        /// </summary>
        /// <param name="sourceData">
        /// The source Data.
        /// </param>
        /// <returns>
        /// The registry message type
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Header not found
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        public static RegistryMessageEnumType GetRegistryMessageType(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                return RegistryMessageEnumType.Null;
            }

            using (Stream stream = sourceData.InputStream)
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    bool skippedHeader = StaxUtil.SkipToEndNode(reader, "Header");
                    if (!skippedHeader)
                    {
                        throw new ArgumentException("Header not found");
                    }

                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                string rootNode = reader.LocalName;
                                return RegistryMessageType.GetMessageType(rootNode).EnumType;
                        }
                    }
                }
            }

            return RegistryMessageEnumType.Null;
        }

        /// <summary>
        /// Returns the version of the schema that the document stored at this URI points to.
        ///     <br/>
        ///     The URI is inferred by the namespaces declared in the root element of the document.
        ///     <br/>
        ///     This will throw an error if there is no way of determining the schema version
        /// </summary>
        /// <param name="sourceData">
        /// The source data
        /// </param>
        /// <returns>
        /// The <see cref="SdmxSchemaEnumType"/> .
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="sourceData"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        /// <exception cref="SdmxSyntaxException">
        /// Can not get Scheme Version from SDMX message.  Unable to determine structure type from Namespaces- please ensure this is a valid SDMX document
        /// </exception>
        public static SdmxSchemaEnumType GetSchemaVersion(IReadableDataLocation sourceData)
        {
            if (sourceData == null)
            {
                throw new ArgumentNullException("sourceData");
            }

            if (IsEdi(sourceData))
            {
                return SdmxSchemaEnumType.Edi;
            }

            if (IsEcv(sourceData))
            {
                return SdmxSchemaEnumType.Ecv;
            }

            if (IsJson(sourceData))
            {
                return SdmxSchemaEnumType.Json;
            }

            using (Stream stream = sourceData.InputStream)
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(stream))
                    {
                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.Element:

                                    // should cover most cases
                                    switch (reader.NamespaceURI)
                                    {
                                        case SdmxConstants.MessageNs10:
                                            return SdmxSchemaEnumType.VersionOne;
                                        case SdmxConstants.MessageNs20:
                                            return SdmxSchemaEnumType.VersionTwo;
                                        case SdmxConstants.MessageNs21:
                                            return SdmxSchemaEnumType.VersionTwoPointOne;
                                    }

                                    for (int i = 0; i < reader.AttributeCount; i++)
                                    {
                                        string ns = reader.GetAttribute(i);
                                        if (SdmxConstants.NamespacesV1.Contains(ns))
                                        {
                                            return SdmxSchemaEnumType.VersionOne;
                                        }

                                        if (SdmxConstants.NamespacesV2.Contains(ns))
                                        {
                                            return SdmxSchemaEnumType.VersionTwo;
                                        }

                                        if (SdmxConstants.NamespacesV21.Contains(ns))
                                        {
                                            return SdmxSchemaEnumType.VersionTwoPointOne;
                                        }
                                    }

                                    throw new SdmxSyntaxException("Can not get Scheme Version from SDMX message.  Unable to determine structure type from Namespaces- please ensure this is a valid SDMX document");
                            }
                        }
                    }

                    throw new SdmxSyntaxException(ExceptionCode.XmlParseException, "No root node found");
                }
                catch (XmlException e)
                {
                    throw new SdmxSyntaxException(e, ExceptionCode.XmlParseException, e.Message);
                }
            }
        }

        /// <summary>
        /// The is ecv delete.
        /// </summary>
        /// <param name="dataLocation">
        /// The data location.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="dataLocation"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="IOException">
        /// An I/O error occurs. 
        /// </exception>
        public static bool IsEcvDelete(IReadableDataLocation dataLocation)
        {
            if (dataLocation == null)
            {
                throw new ArgumentNullException("dataLocation");
            }

            char[] firstPortion;
            using (Stream stream = dataLocation.InputStream)
            using (TextReader br = new StreamReader(stream))
            {
                firstPortion = new char[100];
                br.Read(firstPortion, 0, 10);
            }

            var str = new string(firstPortion);
            return str.StartsWith("ECV-", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Parses the SDMX error message.
        /// </summary>
        /// <param name="dataLocation">
        /// The data location.
        /// </param>
        /// <exception cref="SdmxNoResultsException">
        /// No results exception
        /// </exception>
        /// <exception cref="SdmxNotImplementedException">
        /// Not implemented exception
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        /// Semantic exception
        /// </exception>
        /// <exception cref="SdmxUnauthorisedException">
        /// Unauthorized exception
        /// </exception>
        /// <exception cref="SdmxServiceUnavailableException">
        /// service unavailable exeption
        /// </exception>
        /// <exception cref="SdmxSyntaxException">
        /// Sintax exception
        /// </exception>
        /// <exception cref="SdmxResponseSizeExceedsLimitException">
        /// Response Size Exceeds Limit Exception
        /// </exception>
        /// <exception cref="SdmxResponseTooLargeException">
        /// Response Too Large Exception
        /// </exception>
        /// <exception cref="SdmxInternalServerException">
        /// Internal Server Exception
        /// </exception>
        /// <exception cref="SdmxException">
        /// General exception
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="dataLocation"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SecurityException">
        /// The <see cref="T:System.Xml.XmlReader"/> does not have sufficient permissions to access the location of the XML data.
        /// </exception>
        /// <exception cref="XmlException">
        /// An error occurred while parsing the XML. 
        /// </exception>
        public static void ParseSdmxErrorMessage(IReadableDataLocation dataLocation)
        {
            if (dataLocation == null)
            {
                throw new ArgumentNullException("dataLocation");
            }

            using (var stream = dataLocation.InputStream)
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    SdmxErrorCode errorCode = null;
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                string nodeName = reader.LocalName;
                                if (nodeName.Equals("ErrorMessage"))
                                {
                                    var code = reader.GetAttribute("code");
                                    int clientCode;
                                    if (int.TryParse(code, out clientCode))
                                    {
                                        errorCode = SdmxErrorCode.ParseClientCode(clientCode);
                                    }
                                }
                                else if (nodeName.Equals("Text"))
                                {
                                    if (errorCode == null)
                                    {
                                        errorCode = SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError);
                                    }

                                    switch (errorCode.EnumType)
                                    {
                                        case SdmxErrorCodeEnumType.NoResultsFound:
                                            throw new SdmxNoResultsException(reader.Value);
                                        case SdmxErrorCodeEnumType.NotImplemented:
                                            throw new SdmxNotImplementedException(reader.Value);
                                        case SdmxErrorCodeEnumType.SemanticError:
                                            throw new SdmxSemmanticException(reader.Value);
                                        case SdmxErrorCodeEnumType.Unauthorised:
                                            throw new SdmxUnauthorisedException(reader.Value);
                                        case SdmxErrorCodeEnumType.ServiceUnavailable:
                                            throw new SdmxServiceUnavailableException(reader.Value);
                                        case SdmxErrorCodeEnumType.SyntaxError:
                                            throw new SdmxSyntaxException(reader.Value);
                                        case SdmxErrorCodeEnumType.ResponseSizeExceedsServiceLimit:
                                            throw new SdmxResponseSizeExceedsLimitException(reader.Value);
                                        case SdmxErrorCodeEnumType.ResponseTooLarge:
                                            throw new SdmxResponseTooLargeException(reader.Value);
                                        case SdmxErrorCodeEnumType.InternalServerError:
                                            throw new SdmxInternalServerException(reader.Value);
                                        case SdmxErrorCodeEnumType.Conflict:
                                            throw new SdmxConflictException(reader.Value);
                                        default:
                                            throw new SdmxException(reader.Value, errorCode);
                                    }
                                }

                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The is ecv.
        /// </summary>
        /// <param name="sourceData">
        /// The source data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Throws ArgumentException
        /// </exception>
        private static bool IsEcv(IReadableDataLocation sourceData)
        {
            using (Stream stream = sourceData.InputStream)
            {
                char[] firstPortion;
                using (TextReader br = new StreamReader(stream))
                {
                    firstPortion = new char[100];
                    br.Read(firstPortion, 0, 100);
                }

                var str = new string(firstPortion); // $$$ UTF encoding
                return str.ToUpperInvariant().StartsWith("ECV", StringComparison.Ordinal);
            }
        }

        /// <summary>
        /// The is edi.
        /// </summary>
        /// <param name="sourceData">
        /// The source data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> .
        /// </returns>
        private static bool IsEdi(IReadableDataLocation sourceData)
        {
            char[] firstPortion;
            using (Stream stream = sourceData.InputStream)
            using (TextReader br = new StreamReader(stream))
            {
                firstPortion = new char[100];
                br.Read(firstPortion, 0, 100);
            }

            var str = new string(firstPortion);
            return str.ToUpperInvariant().StartsWith("UNA", StringComparison.Ordinal);
        }

        /// <summary>
        /// The is json.
        /// </summary>
        /// <param name="sourceData">
        /// The source data.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsJson(IReadableDataLocation sourceData)
        {
            char[] firstPortion;
            using (Stream stream = sourceData.InputStream)
            using (TextReader br = new StreamReader(stream))
            {
                firstPortion = new char[100];
                br.Read(firstPortion, 0, 100);
            }

            var str = new string(firstPortion);
            return str.StartsWith("{", StringComparison.Ordinal) || str.StartsWith("[", StringComparison.Ordinal);
        }
    }
}