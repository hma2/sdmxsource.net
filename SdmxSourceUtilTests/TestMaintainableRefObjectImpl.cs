// -----------------------------------------------------------------------
// <copyright file="TestMaintainableRefObjectImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using Moq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="MaintainableRefObjectImpl"/>
    /// </summary>
    [TestFixture]
    public class TestMaintainableRefObjectImpl
    {

        /// <summary>
        /// Test unit for <see cref="MaintainableRefObjectImpl.Equals(object)"/> 
        /// </summary>
        /// <param name="agencyId">
        /// The agency Id.
        /// </param>
        /// <param name="hasAgency">
        /// The has Agency.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="hasId">
        /// The has Id.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="hasVersion">
        /// The has Version.
        /// </param>
        /// <param name="equals">
        /// The equals.
        /// </param>
        [TestCase(null, false, null, false, null, false, false)]
        [TestCase(null, false, null, false, "", false, false)]
        [TestCase(null, false, null, false, " ", false, false)]
        [TestCase(null, false, "", false, null, false, false)]
        [TestCase(null, false, " ", false, null, false, false)]
        [TestCase(null, false, "", false, "", false, false)]
        [TestCase(null, false, "", false, " ", false, false)]
        [TestCase(null, false, " ", false, "", false, false)]
        [TestCase(null, false, " ", false, " ", false, false)]
        [TestCase("", false, null, false, null, false, false)]
        [TestCase("", false, null, false, null, false, false)]
        [TestCase("", false, null, false, "", false, false)]
        [TestCase("", false, null, false, " ", false, false)]
        [TestCase("", false, "", false, null, false, false)]
        [TestCase("", false, " ", false, null, false, false)]
        [TestCase("", false, "", false, "", false, false)]
        [TestCase("", false, "", false, " ", false, false)]
        [TestCase("", false, " ", false, "", false, false)]
        [TestCase("", false, " ", false, " ", false, false)]
        [TestCase(" ", false, null, false, null, false, false)]
        [TestCase(" ", false, null, false, null, false, false)]
        [TestCase(" ", false, null, false, "", false, false)]
        [TestCase(" ", false, null, false, " ", false, false)]
        [TestCase(" ", false, "", false, null, false, false)]
        [TestCase(" ", false, " ", false, null, false, false)]
        [TestCase(" ", false, "", false, "", false, false)]
        [TestCase(" ", false, "", false, " ", false, false)]
        [TestCase(" ", false, " ", false, "", false, false)]
        [TestCase(" ", false, " ", false, " ", false, false)]
        [TestCase(null, false, null, false, "1.0", true, false)]
        [TestCase("", false, "", false, "1.0", true, false)]
        [TestCase(" ", false, " ", false, "1.0", true, false)]
        [TestCase(null, false, "TEST", true, null, false, false)]
        [TestCase("", false, "TEST", true, null, false, false)]
        [TestCase("", false, "TEST", true, "", false, false)]
        [TestCase(null, false, "TEST", true, "", false, false)]
        [TestCase(null, false, "TEST", true, "1.0", true, false)]
        [TestCase("AGENCY", true, "TEST", true, "", false, false)]
        [TestCase("AGENCY", true, "TEST", true, "1.0", true, false)]
        [TestCase("AGENCY", true, "TST", true, "2.0", true, false)]
        [TestCase("AGNCY", true, "TEST", true, "2.0", true, false)]
        [TestCase("AGENCY", true, "TEST", true, "2.0", true, true)]
        public void TestEquals(string agencyId, bool hasAgency, string id, bool hasId, string version, bool hasVersion, bool equals)
        {
            var maintRef = new MaintainableRefObjectImpl(agencyId, id, version);
            Assert.AreEqual(hasAgency, maintRef.HasAgencyId());
            Assert.AreEqual(hasVersion, maintRef.HasVersion());
            Assert.AreEqual(hasId, maintRef.HasMaintainableId());
            var moq = new Mock<IMaintainableRefObject>();
            moq.Setup(o => o.MaintainableId).Returns("TEST");
            moq.Setup(o => o.AgencyId).Returns("AGENCY");
            moq.Setup(o => o.Version).Returns("2.0");
            moq.Setup(o => o.HasAgencyId()).Returns(true);
            moq.Setup(o => o.HasVersion()).Returns(true);
            moq.Setup(o => o.HasMaintainableId()).Returns(true);
            Assert.AreEqual(equals, maintRef.Equals(moq.Object));
        }
    }
}