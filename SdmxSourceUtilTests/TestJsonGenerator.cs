﻿// -----------------------------------------------------------------------
// <copyright file="TestJsonGenerator.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxSourceUtilTests.
// 
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Json;

    /// <summary>
    /// The test json generator.
    /// </summary>
    [TestFixture]
    public class TestJsonGenerator
    {
        /// <summary>
        /// Tests the write array field start.
        /// </summary>
        [Test]
        public void TestWriteArrayFieldStart()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteArrayFieldStart("test");

                    Assert.AreEqual("\"test\":[", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write boolean.
        /// </summary>
        [Test]
        public void TestWriteBoolean()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteBoolean(true);

                    StringAssert.StartsWith("true", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write boolean field.
        /// </summary>
        [Test]
        public void TestWriteBooleanField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteBooleanField("testField", true);

                    StringAssert.StartsWith("\"testField\":true", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write decimal.
        /// </summary>
        [Test]
        public void TestWriteDecimal()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumber((decimal)0);

                    StringAssert.StartsWith("0.0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write decimal field.
        /// </summary>
        [Test]
        public void TestWriteDecimalField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumberField("testField", (decimal)0);

                    StringAssert.StartsWith("\"testField\":0.0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write double.
        /// </summary>
        [Test]
        public void TestWriteDouble()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumber((double)0);

                    StringAssert.StartsWith("0.0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write double field.
        /// </summary>
        [Test]
        public void TestWriteDoubleField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumberField("testField", (double)0);

                    StringAssert.StartsWith("\"testField\":0.0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write float.
        /// </summary>
        [Test]
        public void TestWriteFloat()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumber((float)0);

                    StringAssert.StartsWith("0.0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write float field.
        /// </summary>
        [Test]
        public void TestWriteFloatField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumberField("testField", (float)0);

                    StringAssert.StartsWith("\"testField\":0.0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write integer.
        /// </summary>
        [Test]
        public void TestWriteInt()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumber(0);

                    StringAssert.StartsWith("0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write integer field.
        /// </summary>
        [Test]
        public void TestWriteIntField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumberField("testField", 0);

                    StringAssert.StartsWith("\"testField\":0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write long.
        /// </summary>
        [Test]
        public void TestWriteLong()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumber((long)0);

                    StringAssert.StartsWith("0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write long field.
        /// </summary>
        [Test]
        public void TestWriteLongField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNumberField("testField", (long)0);

                    StringAssert.StartsWith("\"testField\":0", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write null field.
        /// </summary>
        [Test]
        public void TestWriteNullField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteNullField("testField");

                    StringAssert.StartsWith("\"testField\":null", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write object field start.
        /// </summary>
        [Test]
        public void TestWriteObjectFieldStart()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteObjectFieldStart("test");

                    Assert.AreEqual("\"test\":{", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write string.
        /// </summary>
        [Test]
        public void TestWriteString()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteString("test");

                    Assert.AreEqual("\"test\"", stringWriter.ToString());
                }
            }
        }

        /// <summary>
        /// Tests the write string field.
        /// </summary>
        [Test]
        public void TestWriteStringField()
        {
            using (var stringWriter = new StringWriter())
            {
                using (var jsonGenerator = new JsonGenerator(stringWriter))
                {
                    jsonGenerator.WriteStringField("testField", "test");

                    Assert.AreEqual("\"testField\":\"test\"", stringWriter.ToString());
                }
            }
        }
    }
}