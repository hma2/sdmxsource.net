﻿// -----------------------------------------------------------------------
// <copyright file="TestScanner.cs" company="EUROSTAT">
//   Date Created : 2014-07-28
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System;
    using System.Globalization;
    using System.IO;

    using NUnit.Framework;

    using Org.Sdmxsource.Util.Text;

    /// <summary>
    /// Test unit for <see cref="Scanner"/>
    /// </summary>
    [TestFixture]
    public class TestScanner
    {
        /// <summary>
        /// Test unit for <see cref="Scanner" />
        /// </summary>
        /// <param name="delimiter">The delimiter.</param>
        /// <param name="input">The input.</param>
        [TestCase("'", "L0TEST'\nL1'L2'L3'L4'\nL5+dldldldlldl'")]
        [TestCase("'", "L0TEST'\nL1'L2'L3'L4'\nL5+dldldldlldl'\n")]
        [TestCase("'", "L0TEST'\nL1'L2'L3'L4'\nL5+dldldldlldl'L6\n")]
        public void Test(string delimiter, string input)
        {
            using (var reader = new StringReader(input))
            using (var scanner = new Scanner(reader))
            {
                scanner.UseDelimiter(delimiter);
                int index = 0;
                while (scanner.HasNext())
                {
                    string token = scanner.Next().Trim('\n');
                    if (token.Length > 0)
                    {
                        string expected = string.Format(CultureInfo.InvariantCulture, "L{0}", index++);
                        Assert.IsTrue(token.StartsWith(expected, StringComparison.Ordinal), "Expected to start with '{0}' got token '{1}'", expected, token);
                    }
                }
            }
        }
    }
}