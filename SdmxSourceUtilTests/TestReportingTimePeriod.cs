﻿using System;

namespace SdmxSourceUtilTests
{
    using System.Xml;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    public class TestReportingTimePeriod
    {
        [Test]
        public void TestQuarterly()
        {
            var reportingPeriod = "2010-Q2";
            var reportingYearStartDate = "--07-01";

            var startDate = new DateTime(2010,10,01);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod,reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart,Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency,Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.QuarterOfYear)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.QuarterOfYear), reportingYearStartDate);
            Assert.That(period,Is.EqualTo(reportingPeriod));
        }


        [Test]
        public void TestWeekly()
        {
            var reportingPeriod = "2011-W36";
            var reportingYearStartDate = "--07-01";

            var startDate = new DateTime(2012,03,05);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            var sdmxDateCore = new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.Week);
            Assert.That(timePeriod.PeriodStart,Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency,Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Week)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.Week), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }


        [Test]
        public void TestMonthly()
        {
            var reportingPeriod = "2001-M01";
            var reportingYearStartDate = "--07-01";

            var startDate = new DateTime(2001, 7, 1);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Month)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.Month), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));

        }


        [Test]
        public void TestDaily()
        {
            var reportingPeriod = "2016-D005";
            var reportingYearStartDate = "--02-29";
           
            var startDate = new DateTime(2016, 3, 4);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Date)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.Date), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));

        }

        [Test]
        public void TestReportingStartDate()
        {
            var reportingPeriod = "2016-S2";
            var reportingYearStartDate = "--11-01-04:00";

            var startDate = new DateTime(2017, 5, 1, 4, 0, 0);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.HalfOfYear)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.HalfOfYear), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }


        [Test]
        public void TestReportingStartDateZ()
        {
            var reportingPeriod = "2016-T2";
            var reportingYearStartDate = "--11-01Z";

            var startDate = new DateTime(2017, 3, 1);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.ThirdOfYear)));

            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.ThirdOfYear), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }


        [Test]
        public void TestAnnualy()
        {
            var reportingPeriod = "2016-A1";
            var reportingYearStartDate = "--11-15";

            var startDate = new DateTime(2016, 11, 15);
            var reportingTimePeriod = new ReportingTimePeriod();
            var timePeriod = reportingTimePeriod.ToGregorianPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(timePeriod.PeriodStart, Is.EqualTo(startDate));
            Assert.That(timePeriod.Frequency, Is.EqualTo(TimeFormat.GetFromEnum(TimeFormatEnumType.Year)));


            var period = reportingTimePeriod.ToReportingPeriod(new SdmxDateCore(timePeriod.PeriodStart, TimeFormatEnumType.Year), reportingYearStartDate);
            Assert.That(period, Is.EqualTo(reportingPeriod));
        }

        [TestCase("2005", "--01-01", "2005-A1")]
        [TestCase("2005", "--12-01", "2004-A1")]
        [TestCase("2005-03", "--01-01", "2005-M03")]
        [TestCase("2005-03", "--04-01", "2004-M12")]
        [TestCase("2005-01", "--04-01", "2004-M10")]
        [TestCase("2005-02", "--04-01", "2004-M11")]
        [TestCase("2005-03", "--04-01", "2004-M12")]
        [TestCase("2016-Q1", "--01-01", "2016-Q1")]
        [TestCase("2016-Q1", "--03-01", "2015-Q4")]
        [TestCase("2016-Q1", "--09-01", "2015-Q2")]
        [TestCase("2016-Q2", "--09-01", "2015-Q3")]
        public void TestGreogorianToReportingRoundTrip(string gregorian, string reportingStartYearDay, string expectedReportingPeriod)
        {
            var reportingTimePeriod = new ReportingTimePeriod();
            var sdmxDate = new SdmxDateCore(gregorian);
            var reportingPeriod = reportingTimePeriod.ToReportingPeriod(sdmxDate, reportingStartYearDay);
            Assert.That(reportingPeriod, Is.Not.Null);
            Assert.That(reportingPeriod, Is.EqualTo(expectedReportingPeriod));
            
            var gregorianRounndTrip = reportingTimePeriod.ToGregorianPeriod(expectedReportingPeriod, reportingStartYearDay);
            var sdmxDateRoundTrip = new SdmxDateCore(gregorianRounndTrip.PeriodStart, gregorianRounndTrip.Frequency);
            Assert.That(sdmxDateRoundTrip.DateInSdmxFormat, Is.EqualTo(gregorian));
        }
        
        [TestCase("2001-Q1",null,false)]
        [TestCase("2001-Q1","--07-11",true)]
        [TestCase("2001-M1", "--07-11",false)]
        [TestCase("2001-M01", "--07-11",true)]
        [TestCase("2001-A1", null,true)]
        [TestCase("2001-T1", null,false)]
        [TestCase("2001-T1", "--07-11", true)]
        [TestCase("2001-W10", "--07-11", true)]
        [TestCase("2001-W1", null, false)]
        [TestCase("2001-01", "--07-11", false)]
        [TestCase("2001-S1", "--07-11", true)]
        [TestCase("2001-S1", null, false)]
        public void ValidateReportingPeriod(string reportingPeriod, string reportingYearStartDate,bool expectedValidity)
        {
            var isValid = new ReportingTimePeriod().CheckReportingPeriod(reportingPeriod, reportingYearStartDate);
            Assert.That(isValid,Is.EqualTo(expectedValidity));
        }
    }
}
