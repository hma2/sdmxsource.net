// -----------------------------------------------------------------------
// <copyright file="TestEmailValidation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Util.Email;

    /// <summary>
    ///     Test unit class for <see cref="EmailValidation" />
    /// </summary>
    [TestFixture]
    public class TestEmailValidation
    {
        #region Public Methods and Operators

        /// <summary>
        /// Test method for <see cref="EmailValidation.ValidateEmail"/>
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="expectedResult">
        /// The expected Result.
        /// </param>
        [TestCase("soemone@domain.com", true)]
        [TestCase("soemone@domain.foo.com", true)]
        [TestCase("soemone@domain.foo.bar.com", true)]
        [TestCase("name.soemone@domain.com", true)]
        [TestCase("n1ame.soem4one@domain.foo.com", true)]
        [TestCase("name.soemone@domain.foo.bar.com", true)]
        [TestCase("name.soemone@domai3n.foo.bar.com", true)]
        [TestCase("name.soe4mone@domain.eu", true)]
        [TestCase("name.soemo2ne@domain.int", true)]
        [TestCase("name.s1232oemone@do3main.org", true)]
        [TestCase("NAME.S1232OEMONE@DO3MAIn.org", true)]
        [TestCase("name.soemone-nodomain", false)]
        [TestCase("namesoemonenodomain", false)]
        [TestCase("name@192.168.1.1", true)]
        public void Test(string email, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, EmailValidation.ValidateEmail(email));
        }

        #endregion
    }
}