// -----------------------------------------------------------------------
// <copyright file="TestSdmxXmlStream.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxSourceUtilTests.
//     SdmxSourceUtilTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxSourceUtilTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxSourceUtilTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxSourceUtilTests
{
    using System.IO;
    using System.Xml;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Util.Io;

    /// <summary> Test unit class for <see cref="XmlDocReadableDataLocation"/> </summary>
    [TestFixture]
    public class TestSdmxXmlStream
    {
               /// <summary>
        /// Test method for <see cref="XmlDocReadableDataLocation"/> 
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        public void TestXmlReaderCtor(string file)
        {
            using (var doc = XmlReader.Create(file))
            using (var readable = new SdmxXmlStream(doc, MessageEnumType.Structure, SdmxSchemaEnumType.VersionTwo, RegistryMessageEnumType.Null))
            {
                Assert.IsTrue(readable.HasReader);
                Assert.IsNotNull(readable.Reader);
                Assert.AreEqual(MessageEnumType.Structure, readable.MessageType);
                Assert.AreEqual(SdmxSchemaEnumType.VersionTwo, readable.SdmxVersion);
                Assert.AreEqual(RegistryMessageEnumType.Null, readable.RegistryType);
            }
        }


        /// <summary>
        /// Test method for <see cref="XmlDocReadableDataLocation"/> 
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        public void TestReadableDataLocationCtor(string file)
        {
            using (var readable = new FileReadableDataLocation(file))
            {
                using (var sdmxXmlStream = new SdmxXmlStream(readable))
                {
                    Assert.IsTrue(sdmxXmlStream.HasReader);
                    Assert.IsNotNull(sdmxXmlStream.Reader);
                    Assert.AreEqual(MessageEnumType.Structure, sdmxXmlStream.MessageType);
                    Assert.AreEqual(SdmxSchemaEnumType.VersionTwo, sdmxXmlStream.SdmxVersion);
                    Assert.AreEqual(RegistryMessageEnumType.Null, sdmxXmlStream.RegistryType);
                }
            }
        }

        /// <summary>
        /// Test method for <see cref="XmlDocReadableDataLocation"/> 
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        public void TestReadableDataLocationCtor2(string file)
        {
            using (var readable = new FileReadableDataLocation(file))
            {
                using (var xmlStream = new SdmxXmlStream(readable))
                using (var sdmxXmlStream = new SdmxXmlStream(xmlStream))
                {
                    Assert.IsTrue(sdmxXmlStream.HasReader);
                    Assert.IsNotNull(sdmxXmlStream.Reader);
                    Assert.AreEqual(MessageEnumType.Structure, sdmxXmlStream.MessageType);
                    Assert.AreEqual(SdmxSchemaEnumType.VersionTwo, sdmxXmlStream.SdmxVersion);
                    Assert.AreEqual(RegistryMessageEnumType.Null, sdmxXmlStream.RegistryType);
                }
            }
        }
    }
}