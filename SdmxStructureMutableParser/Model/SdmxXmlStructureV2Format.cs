﻿// -----------------------------------------------------------------------
// <copyright file="SdmxXmlStructureV2Format.cs" company="EUROSTAT">
//   Date Created : 2016-07-11
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjects.
// 
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Model
{
    using System;
    using System.Globalization;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// The SDMX XML structure format.
    /// </summary>
    public class SdmxXmlStructureV2Format : IStructureFormat
    {
        /// <summary>
        /// The _SDMX output format
        /// </summary>
        private readonly StructureOutputFormat _sdmxOutputFormat;

        /// <summary>
        /// The _XML writer
        /// </summary>
        private readonly XmlWriter _xmlWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxXmlStructureV2Format"/> class.
        /// </summary>
        /// <param name="sdmxOutputFormat">The SDMX output format.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sdmxOutputFormat
        /// or
        /// xmlWriter
        /// </exception>
        public SdmxXmlStructureV2Format(StructureOutputFormat sdmxOutputFormat, XmlWriter xmlWriter)
        {
            if (sdmxOutputFormat == null)
            {
                throw new ArgumentNullException("sdmxOutputFormat");
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException("xmlWriter");
            }

            if (!sdmxOutputFormat.OutputVersion.IsXmlFormat())
            {
                throw new ArgumentException("This format should be used only for XML output");
            }

            this._sdmxOutputFormat = sdmxOutputFormat;
            this._xmlWriter = xmlWriter;
        }

        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        /// <p />
        /// This is expected to return a not null response.
        /// </summary>
        public string FormatAsString
        {
            get
            {
                return this.ToString();
            }
        }

        /// <summary>
        /// Gets the SDMX Structure Output Type that this interface is describing.
        /// If this is not describing an SDMX message then this will return null and the implementation class will be expected
        /// to expose additional methods
        /// to describe the output format
        /// </summary>
        public StructureOutputFormat SdmxOutputFormat
        {
            get
            {
                return this._sdmxOutputFormat;
            }
        }

        /// <summary>
        /// Gets the writer.
        /// </summary>
        /// <value>
        /// The writer.
        /// </value>
        public XmlWriter Writer
        {
            get
            {
                return this._xmlWriter;
            }
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "SdmxXmlStructureV2Format-{0}", this._sdmxOutputFormat.EnumType);
        }
    }
}