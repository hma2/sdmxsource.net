﻿// -----------------------------------------------------------------------
// <copyright file="StructureOutputFormat.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Model
{
    using System;
    using System.Globalization;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    ///     SdmxStructureV2Format class
    /// </summary>
    public class SdmxStructureV2Format : IStructureFormat
    {
        /// <summary>
        ///     The _structure format
        /// </summary>
        private readonly StructureOutputFormat _structureFormat;

        /// <summary>
        /// The output Encoding
        /// </summary>
        private readonly Encoding _outputEncoding;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxStructureV2Format" /> class.
        /// </summary>
        /// <param name="structureFormat">The structure format.</param>
        /// <exception cref="ArgumentException">STRUCTURE_OUTPUT_FORMAT can not be null</exception>
        public SdmxStructureV2Format(StructureOutputFormat structureFormat)
            : this(structureFormat, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxStructureV2Format" /> class.
        /// </summary>
        /// <param name="structureFormat">The structure format.</param>
        /// <param name="encoding">The encoding.</param>
        /// <exception cref="System.ArgumentException">STRUCTURE_OUTPUT_FORMAT can not be null</exception>
        /// <exception cref="ArgumentException">STRUCTURE_OUTPUT_FORMAT can not be null</exception>
        public SdmxStructureV2Format(StructureOutputFormat structureFormat, Encoding encoding)
        {
            if (structureFormat == null)
            {
                throw new ArgumentException("STRUCTURE_OUTPUT_FORMAT can not be null");
            }

            this._structureFormat = structureFormat;
            this._outputEncoding = encoding ?? new UTF8Encoding(false);
        }

        /// <summary>
        ///     Gets a string representation of the format, that can be used for auditing and debugging purposes.
        ///     <p />
        ///     This is expected to return a not null response.
        /// </summary>
        public string FormatAsString
        {
            get
            {
                return this.ToString();
            }
        }

        /// <summary>
        ///     Gets the SDMX Structure Output Type that this interface is describing.
        ///     If this is not describing an SDMX message then this will return null and the implementation class will be expected
        ///     to expose additional methods
        ///     to describe the output format
        /// </summary>
        public StructureOutputFormat SdmxOutputFormat
        {
            get
            {
                return this._structureFormat;
            }
        }

        /// <summary>
        /// Gets the output encoding.
        /// </summary>
        /// <value>
        /// The output encoding.
        /// </value>
        public Encoding OutputEncoding
        {
            get
            {
                return this._outputEncoding;
            }
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "SdmxStructureV2Format-{0};charSet={1}", this._structureFormat.EnumType, this._outputEncoding.WebName);
        }
    }
}