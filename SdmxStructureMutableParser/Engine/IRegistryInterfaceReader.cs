﻿// -----------------------------------------------------------------------
// <copyright file="IRegistryInterfaceReader.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureMutableParser.
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Engine
{
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Model;

    using Org.Sdmxsource.Sdmx.Util.Exception;

    /// <summary>
    ///     The RegistryInterfaceReader interface.
    /// </summary>
    public interface IRegistryInterfaceReader
    {
        /// <summary>
        ///     Parses the reader opened against the stream containing the contents of a SDMX-ML Registry message or
        ///     RegistryInterface structure contents and populates the given <see cref="IRegistryInfo" /> object.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="reader" /> is null
        /// </exception>
        /// <exception cref="ParseException">
        ///     SDMX structure message parsing error
        /// </exception>
        /// <param name="registry">
        ///     The <see cref="IRegistryInfo" /> object to populate
        /// </param>
        /// <param name="reader">
        ///     The xml reader opened against the stream containing the structure contents
        /// </param>
        void Read(IRegistryInfo registry, XmlReader reader);
    }
}