// -----------------------------------------------------------------------
// <copyright file="QueryStructureResponseReaderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureMutableParser.
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Engine.V2
{
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     The query structure response reader v 2.
    /// </summary>
    internal class QueryStructureResponseReaderV2 : RegistryInterfaceReaderBaseV2
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryStructureResponseReaderV2" /> class.
        /// </summary>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        public QueryStructureResponseReaderV2(SdmxNamespaces namespaces)
            : base(namespaces)
        {
        }

        /// <summary>
        ///     Read contents.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <param name="rootObject">
        ///     The root object.
        /// </param>
        protected override void ReadContents(XmlReader reader, IRegistryInfo rootObject)
        {
            base.ReadContents(reader, o => this.HandleChildElements(this.RegistryInterface.QueryStructureResponse, o));
        }

        /// <summary>
        ///     Handle QueryStructureResponse Child elements
        /// </summary>
        /// <param name="parent">
        ///     The parent QueryStructureResponseBean object
        /// </param>
        /// <param name="localName">
        ///     The name of the current xml element
        /// </param>
        /// <returns>
        ///     The <see cref="StructureReaderBaseV20.ElementActions" />.
        /// </returns>
        private ElementActions HandleChildElements(IQueryStructureResponseInfo parent, object localName)
        {
            ElementActions actions = null;
            if (NameTableCache.IsElement(localName, ElementNameTable.StatusMessage))
            {
                IStatusMessageInfo status = this.CreateStatusMessage();
                parent.StatusMessage = status;
                actions = this.AddSimpleAction(status, this.HandleTextChildElement);
            }
            else
            {
                if (parent.Structure == null)
                {
                    var structure = new MutableObjectsImpl();
                    parent.Structure = structure;
                }

                this.HandleTopLevelBase(parent.Structure, localName);
            }

            return actions;
        }

        /////// <summary>
        ///////     The initialize type switch.

        /////// </summary>
        ////private void InitializeTypeSwitch()
        ////{
        ////    // add text only element handlers
        ////    this.AddHandleText<IStatusMessageInfo>(this.HandleTextChildElement);

        ////    // add element handlers
        ////    this.AddHandleElement<IQueryStructureResponseInfo>(this.HandleChildElements);
        ////}
    }
}