// -----------------------------------------------------------------------
// <copyright file="QueryStructureRequestWriterV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureMutableParser.
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxStructureMutableParser.Engine.V2
{
    using System;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxStructureMutableParser.Model;
    using Estat.Sri.SdmxXmlConstants;

    /// <summary>
    ///     The query structure request reader v 2.
    /// </summary>
    internal class QueryStructureRequestWriterV2 : RegistryInterfaceWriterBaseV2, IRegistryInterfaceWriter
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="QueryStructureRequestWriterV2" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        public QueryStructureRequestWriterV2(XmlWriter writer, SdmxNamespaces namespaces)
            : base(writer, namespaces)
        {
        }

        /// <summary>
        ///     Write the specified <paramref name="registry" />
        /// </summary>
        /// <param name="registry">
        ///     The <see cref="IRegistryInfo" /> object
        /// </param>
        public void Write(IRegistryInfo registry)
        {
            if (registry == null)
            {
                throw new ArgumentNullException("registry");
            }

            if (registry.QueryStructureRequest != null)
            {
                this.WriteQueryStructureRequest(registry.QueryStructureRequest);
            }
        }

        /// <summary>
        ///     Write query structure request
        /// </summary>
        /// <param name="request">
        ///     THe <see cref="IQueryStructureRequestInfo" /> to write
        /// </param>
        private void WriteQueryStructureRequest(IQueryStructureRequestInfo request)
        {
            this.WriteStartElement(ElementNameTable.QueryStructureRequest);
            this.WriteAttribute(AttributeNameTable.resolveReferences, request.ResolveReferences);
            if (!request.ReturnDetails)
            {
                // defaults to true. It is an extension to SDMX.
                this.TryWriteAttribute(AttributeNameTable.returnDetails, request.ReturnDetails);
            }

            foreach (IReferenceInfo refBean in request.References)
            {
                this.WriteRef(refBean);
            }

            this.WriteEndElement(); // </QueryStructureRequest>
        }
    }
}