// -----------------------------------------------------------------------
// <copyright file="StructureWriterBaseV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureMutableParser.
//     SdmxStructureMutableParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureMutableParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureMutableParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.SdmxStructureMutableParser.Engine.V2
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Engine;
    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The structure writer base v 2.
    /// </summary>
    public abstract class StructureWriterBaseV2 : Writer
    {
        #region Static Fields

        /// <summary>
        ///     The SDMX schema version that this class supports
        /// </summary>
        private static readonly SdmxSchema _versionTwo = SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo);

        #endregion

        #region Fields

        /// <summary>
        ///     The default ns.
        /// </summary>
        private readonly NamespacePrefixPair _defaultNs;

        /// <summary>
        ///     The root namespace, e.g. for <c>CodeLists</c>
        /// </summary>
        private NamespacePrefixPair _rootNamespace;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureWriterBaseV2" /> class.
        ///     Initializes a new instance of the <see cref="StructureWriterV2" /> class.
        ///     Constructor that initialize the internal fields
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     writer is null
        /// </exception>
        /// <param name="writer">
        ///     The XmlWriter object use to actually perform the writing
        /// </param>
        /// <param name="namespaces">
        ///     The namespaces
        /// </param>
        protected StructureWriterBaseV2([ValidatedNotNull]XmlWriter writer, SdmxNamespaces namespaces)
            : base(writer, _versionTwo, namespaces)
        {
            this._defaultNs = this.Namespaces.Structure;
            this._rootNamespace = this.Namespaces.Message;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the tool indicator.
        /// </summary>
        /// <value>
        /// The tool indicator.
        /// </value>
        public IToolIndicator ToolIndicator { get; set; }

        /// <summary>
        /// Gets or sets the component role builder.
        /// </summary>
        /// <value>
        /// The component role builder.
        /// </value>
        public IBuilder<ComponentRole, IStructureReference> ComponentRoleBuilder { get; set; }

        /// <summary>
        ///     Gets the root namespace, e.g. for <c>CodeLists</c>
        /// </summary>
        protected internal NamespacePrefixPair RootNamespace
        {
            get
            {
                return this._rootNamespace;
            }
        }

        /// <summary>
        ///     Gets the default ns.
        /// </summary>
        protected override NamespacePrefixPair DefaultNS
        {
            get
            {
                return this._defaultNs;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        ///     Set the top element namespace prefix
        /// </summary>
        /// <param name="namespaceUri">
        ///     The namespace URI
        /// </param>
        internal void SetTopElementsNS(NamespacePrefixPair namespaceUri)
        {
            this._rootNamespace = namespaceUri;
        }

        /// <summary>
        /// Creates the XML writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="prettify">if set to <c>true</c> then indent the output.</param>
        /// <returns>
        /// The <see cref="XmlWriter" />.
        /// </returns>
        protected static XmlWriter CreateXmlWriter(Stream writer, Encoding encoding, bool prettify)
        {
            return XmlWriter.Create(writer, new XmlWriterSettings() { Indent = prettify, Encoding = encoding ?? new UTF8Encoding(false) });
        }

        /// <summary>
        ///     Write the xml attributes of the given IIdentifiableMutableObject type object
        /// </summary>
        /// <param name="artefact">
        ///     The IIdentifiableMutableObject type object to write
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="artefact"/> is <see langword="null" />.</exception>
        protected void WriteIdentifiableArtefactAttributes(IIdentifiableMutableObject artefact)
        {
            if (artefact == null)
            {
                throw new ArgumentNullException("artefact");
            }

            this.WriteAttributeString(AttributeNameTable.id, artefact.Id);
            this.TryWriteAttribute(AttributeNameTable.uri, artefact.Uri);
            this.TryWriteAttribute(AttributeNameTable.urn, artefact.Urn);
        }

        /// <summary>
        ///     Write the Name(s) and Description(s) of the given IIdentifiableMutableObject type object
        /// </summary>
        /// <param name="artefact">
        ///     The IIdentifiableMutableObject type object to write
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="artefact"/> is <see langword="null" />.</exception>
        protected void WriteIdentifiableArtefactContent(INameableMutableObject artefact)
        {
            if (artefact == null)
            {
                throw new ArgumentNullException("artefact");
            }

            foreach (ITextTypeWrapperMutableObject textTypeBean in artefact.Names)
            {
                this.WriteTextType(this._defaultNs, textTypeBean, ElementNameTable.Name);
            }

            foreach (ITextTypeWrapperMutableObject textTypeBean in artefact.Descriptions)
            {
                this.WriteTextType(this._defaultNs, textTypeBean, ElementNameTable.Description);
            }
        }

        /// <summary>
        ///     Write the given IItemMutableObject type object to the given element.
        /// </summary>
        /// <param name="element">
        ///     The xml element
        /// </param>
        /// <param name="item">
        ///     The IItemMutableObject type object to write
        /// </param>
        protected void WriteItem(ElementNameTable element, IItemMutableObject item)
        {
            // ReSharper restore SuggestBaseTypeForParameter
            this.WriteStartElement(this.DefaultPrefix, element);
            this.WriteIdentifiableArtefactAttributes(item);

            //// TODO NOTE not supported by common api 
            ////TryWriteAttribute(AttributeNameTable.version, item.Version);
            ////TryWriteAttribute(AttributeNameTable.validFrom, item.ValidFrom);
            ////TryWriteAttribute(AttributeNameTable.validTo, item.ValidTo);
            this.WriteIdentifiableArtefactContent(item);
        }

        /// <summary>
        ///     Write the given IMaintainableMutableObject type object to the given element.
        /// </summary>
        /// <param name="element">
        ///     The xml element
        /// </param>
        /// <param name="artefact">
        ///     The IMaintainableMutableObject type object to write
        /// </param>
        protected void WriteMaintainableArtefact(ElementNameTable element, IMaintainableMutableObject artefact)
        {
            this.WriteStartElement(this.DefaultPrefix, element);
            this.WriteMaintainableArtefactAttributes(artefact);
            this.WriteIdentifiableArtefactContent(artefact);
        }

        /// <summary>
        ///     Write the TextFormat Element from the given ITextFormatMutableObject
        /// </summary>
        /// <param name="textFormat">
        ///     The ITextFormatMutableObject to write
        /// </param>
        protected void WriteTextFormat(ITextFormatMutableObject textFormat)
        {
            this.WriteTextFormat(ElementNameTable.TextFormat, textFormat);
        }

        /// <summary>
        ///     Write the specified Element from the given ITextFormatMutableObject
        /// </summary>
        /// <param name="element">
        ///     The TextFormatType Element
        /// </param>
        /// <param name="textFormat">
        ///     The ITextFormatMutableObject to write
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textFormat"/> is <see langword="null" />.</exception>
        protected void WriteTextFormat(ElementNameTable element, ITextFormatMutableObject textFormat)
        {
            if (textFormat == null)
            {
                throw new ArgumentNullException("textFormat");
            }

            this.WriteStartElement(this.DefaultPrefix, element);
            if (textFormat.TextType != null)
            {
                this.TryWriteAttribute(AttributeNameTable.textType, textFormat.TextType.EnumType.ToString());
            }

            if (textFormat.Decimals > -1)
            {
                this.TryWriteAttribute(AttributeNameTable.decimals, textFormat.Decimals);
            }

            if (textFormat.StartValue < textFormat.EndValue)
            {
                this.TryWriteAttribute(AttributeNameTable.startValue, textFormat.StartValue);
                this.TryWriteAttribute(AttributeNameTable.endValue, textFormat.EndValue);
            }

            if (textFormat.Interval > -1)
            {
                this.TryWriteAttribute(AttributeNameTable.interval, textFormat.Interval);
            }

            this.TryWriteAttribute(AttributeNameTable.isSequence, textFormat.Sequence);

            if (textFormat.MaxLength > -1)
            {
                this.TryWriteAttribute(AttributeNameTable.maxLength, textFormat.MaxLength);
            }

            if (textFormat.MinLength > -1)
            {
                this.TryWriteAttribute(AttributeNameTable.minLength, textFormat.MinLength);
            }

            this.TryWriteAttribute(AttributeNameTable.pattern, textFormat.Pattern);
            this.WriteEndElement();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
        }

        /// <summary>
        ///     Write the xml attributes of the given IMaintainableMutableObject type object
        /// </summary>
        /// <param name="artefact">
        ///     The IMaintainableMutableObject type object to write
        /// </param>
        private void WriteMaintainableArtefactAttributes(IMaintainableMutableObject artefact)
        {
            this.WriteIdentifiableArtefactAttributes(artefact);
            this.TryWriteAttribute(AttributeNameTable.version, artefact.Version);
            this.TryWriteAttribute(AttributeNameTable.validFrom, artefact.StartDate);
            this.TryWriteAttribute(AttributeNameTable.validTo, artefact.EndDate);
            this.TryWriteAttribute(AttributeNameTable.agencyID, artefact.AgencyId);
            this.TryWriteAttribute(AttributeNameTable.isFinal, artefact.FinalStructure);
            this.TryWriteAttribute(AttributeNameTable.isExternalReference, artefact.ExternalReference);
        }

        #endregion
    }
}