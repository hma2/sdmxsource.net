// -----------------------------------------------------------------------
// <copyright file="AbstractRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureRetrieval.
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;

    /// <summary>
    ///     The abstract retrieval manager.
    /// </summary>
    public abstract class AbstractRetrievalManager
    {
        /// <summary>
        ///     The _sdmx object retrieval manager.
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AbstractRetrievalManager" /> class.
        /// </summary>
        /// <param name="sdmxObjectRetrievalManager">
        ///     The sdmx object retrieval manager.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="sdmxObjectRetrievalManager" /> is null
        /// </exception>
        protected AbstractRetrievalManager(ISdmxObjectRetrievalManager sdmxObjectRetrievalManager)
        {
            if (sdmxObjectRetrievalManager == null)
            {
                throw new ArgumentNullException("sdmxObjectRetrievalManager");
            }

            this._sdmxObjectRetrievalManager = sdmxObjectRetrievalManager;
        }

        /// <summary>
        ///     Gets the sdmx object retrieval manager.
        /// </summary>
        public ISdmxObjectRetrievalManager SdmxObjectRetrievalManager
        {
            get
            {
                return this._sdmxObjectRetrievalManager;
            }
        }
    }
}