﻿// -----------------------------------------------------------------------
// <copyright file="BaseSdmxObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2014-04-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureRetrieval.
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.CrossReference;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Engine;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.CrossReference;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The base SDMX object retrieval manager.
    /// </summary>
    public abstract class BaseSdmxObjectRetrievalManager : IdentifiableRetrievalManagerCore, ISdmxObjectRetrievalManager
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(BaseSdmxObjectRetrievalManager));

        /// <summary>
        ///     The cross referenced retrieval manager
        /// </summary>
        private ICrossReferencedRetrievalManager _crossReferenceRetrievalManager;

        /// <summary>
        ///     The cross referencing retrieval manager
        /// </summary>
        private ICrossReferencingRetrievalManager _crossReferencingRetrievalManager;

        /// <summary>
        ///     The external service retrieval manager
        /// </summary>
        private IExternalReferenceRetrievalManager _externalReferenceRetrievalManager;

        /// <summary>
        ///     The header retrieval manager
        /// </summary>
        private HeaderRetrievalManager _headerRetrievalManager;

        /// <summary>
        ///     The  retrieval manager
        /// </summary>
        private ISdmxObjectRetrievalManager _proxy;

        /// <summary>
        ///     The service retrieval manager
        /// </summary>
        private IServiceRetrievalManager _serviceRetrievalManager;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseSdmxObjectRetrievalManager" /> class.
        ///     Default constructor
        /// </summary>
        protected BaseSdmxObjectRetrievalManager()
        {
            this.RetrievalManager = this;
            this._crossReferenceRetrievalManager = new CrossReferencedRetrievalManager(this);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseSdmxObjectRetrievalManager" /> class.
        /// </summary>
        /// <param name="retrievalManager">
        ///     The <see cref="ISdmxObjectRetrievalManager" /> retrieval manager.
        /// </param>
        protected BaseSdmxObjectRetrievalManager(ISdmxObjectRetrievalManager retrievalManager)
            : base(null, retrievalManager)
        {
            this._crossReferenceRetrievalManager = new CrossReferencedRetrievalManager(this.RetrievalManager);
        }

        /// <summary>
        ///     Gets or sets the cross referenced retrieval manager
        /// </summary>
        public ICrossReferencedRetrievalManager CrossReferenceRetrievalManager
        {
            get
            {
                return this._crossReferenceRetrievalManager;
            }

            set
            {
                this._crossReferenceRetrievalManager = value;
            }
        }

        /// <summary>
        ///     Gets or sets the cross referencing retrieval manager
        /// </summary>
        public ICrossReferencingRetrievalManager CrossReferencingRetrievalManager
        {
            get
            {
                return this._crossReferencingRetrievalManager;
            }

            set
            {
                this._crossReferencingRetrievalManager = value;
            }
        }

        /// <summary>
        ///     Gets or sets the external service retrieval manager
        /// </summary>
        public IExternalReferenceRetrievalManager ExternalReferenceRetrievalManager
        {
            get
            {
                return this._externalReferenceRetrievalManager;
            }

            set
            {
                this._externalReferenceRetrievalManager = value;
            }
        }

        /// <summary>
        ///     Gets or sets the header retrieval manager
        /// </summary>
        public HeaderRetrievalManager HeaderRetrievalManager
        {
            get
            {
                return this._headerRetrievalManager;
            }

            set
            {
                this._headerRetrievalManager = value;
            }
        }

        /// <summary>
        ///     Gets or sets the retrieval manager
        /// </summary>
        public ISdmxObjectRetrievalManager Proxy
        {
            get
            {
                return this._proxy;
            }

            set
            {
                this._proxy = value;
            }
        }

        /// <summary>
        ///     Gets or sets the service retrieval manager
        /// </summary>
        public IServiceRetrievalManager ServiceRetrievalManager
        {
            get
            {
                return this._serviceRetrievalManager;
            }

            set
            {
                this._serviceRetrievalManager = value;
            }
        }

        /// <summary>
        ///     Returns AgencySchemeObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all AgencySchemeObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IAgencyScheme> GetAgencySchemeObjects(IMaintainableRefObject xref, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IAgencyScheme>(xref, returnStub, false);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetAgencySchemeObjects");
        }

        /// <summary>
        ///     Returns AttachmentConstraintBeans that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IAttachmentConstraintObject> GetAttachmentConstraints(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IAttachmentConstraintObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetAttachmentConstraints");
        }

        /// <summary>
        ///     Returns CategorisationObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<ICategorisationObject> GetCategorisationObjects(IMaintainableRefObject xref, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<ICategorisationObject>(xref, returnStub, false);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetCategorisationObjects");
        }

        /// <summary>
        ///     Returns CategorySchemeObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CategorySchemeObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<ICategorySchemeObject> GetCategorySchemeObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<ICategorySchemeObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetCategorySchemeObjects");
        }

        /// <summary>
        ///     Returns CodelistObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<ICodelistObject> GetCodelistObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<ICodelistObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetCodelistObjects");
        }

        /// <summary>
        ///     Returns ConceptSchemeObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ConceptSchemeObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IConceptSchemeObject> GetConceptSchemeObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IConceptSchemeObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetConceptSchemeObjects");
        }

        /// <summary>
        ///     Returns ContentConstraintObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all CodelistObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IContentConstraintObject> GetContentConstraints(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IContentConstraintObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetContentConstraints");
        }

        /// <summary>
        ///     Returns DataConsumerSchemeObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataConsumerSchemeObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IDataConsumerScheme> GetDataConsumerSchemeObjects(IMaintainableRefObject xref, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IDataConsumerScheme>(xref, returnStub, false);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetDataConsumerSchemeObjects");
        }

        /// <summary>
        ///     Returns DataflowObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataflowObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IDataflowObject> GetDataflowObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IDataflowObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetDataflowObjects");
        }

        /// <summary>
        ///     Returns DataProviderSchemeObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataProviderSchemeObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IDataProviderScheme> GetDataProviderSchemeObjects(IMaintainableRefObject xref, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IDataProviderScheme>(xref, returnStub, false);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetDataProviderSchemeObjects");
        }

        /// <summary>
        ///     Returns DataStructureObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all DataStructureObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IDataStructureObject> GetDataStructureObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IDataStructureObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetDataStructureObjects");
        }

        /// <summary>
        ///     Returns HierarchicalCodelistObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all HierarchicalCodelistObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported, proxy is null.</exception>
        public virtual ISet<IHierarchicalCodelistObject> GetHierarchicCodeListObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IHierarchicalCodelistObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetHierarchicCodeListObjects");
        }

        /// <summary>
        /// Gets a maintainable that is of the given type, determined by T, and matches the reference parameters in the
        /// IMaintainableRefObject.
        /// <p />
        /// Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <typeparam name="T">
        /// The maintainable type.
        /// </typeparam>
        /// <param name="maintainableReference">
        /// The reference object that must match on the returned structure. If version information is missing, then latest
        ///     version is assumed
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableObject" /> .
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="InvalidCastException">An artefact cannot be cast to type <typeparamref name="T"/>.</exception>
        public virtual T GetMaintainableObject<T>(IMaintainableRefObject maintainableReference) where T : IMaintainableObject
        {
            return this.GetMaintainableObject<T>(maintainableReference, false, false);
        }

        /// <summary>
        ///     Gets a maintainable defined by the StructureQueryObject parameter.
        ///     <p />
        ///     Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="structureReference">
        ///     The reference object defining the search parameters, this is expected to uniquely identify one MaintainableObject
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableObject" /> .
        /// </returns>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="ArgumentException">GetMaintainableObject was passed a null StructureReferenceBean this is not allowed</exception>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefacts.</exception>
        public virtual IMaintainableObject GetMaintainableObject(IStructureReference structureReference)
        {
            return this.GetMaintainableObject(structureReference, false, false);
        }

        /// <summary>
        /// Gets a maintainable defined by the StructureQueryObject parameter.
        /// <p />
        /// Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="structureReference">The reference object defining the search parameters, this is expected to uniquely identify one MaintainableObject</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <returns>The <see cref="IMaintainableObject" /> .</returns>
        /// <exception cref="System.ArgumentException">GetMaintainableObject was passed a null StructureReferenceBean this is not allowed</exception>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefacts.</exception>
        public virtual IMaintainableObject GetMaintainableObject(IStructureReference structureReference, bool returnStub, bool returnLatest)
        {
            if (structureReference == null)
            {
                throw new ArgumentException("GetMaintainableObject was passed a null StructureReferenceBean this is not allowed");
            }

            return
                ExtractFromSet(
                    this.GetMaintainableObjects<IMaintainableObject>(
                        structureReference.MaintainableStructureEnumType.MaintainableInterface, 
                        structureReference.MaintainableReference, 
                        returnStub, 
                        returnLatest));
        }

        /// <summary>
        /// Gets a maintainable that is of the given type, determined by T, and matches the reference parameters in the
        /// IMaintainableRefObject.
        /// <p />
        /// Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable
        /// </typeparam>
        /// <param name="maintainableReference">
        /// The reference object that must match on the returned structure. If version information is missing, then latest
        ///     version is assumed
        /// </param>
        /// <param name="returnStub">
        /// If true then a stub object will be returned
        /// </param>
        /// <param name="returnLatest">
        /// If true then the latest version is returned, regardless of whether version information is supplied
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableObject"/> .
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="maintainableReference"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="InvalidCastException">An artefact cannot be cast to type <typeparamref name="T"/>.</exception>
        public virtual T GetMaintainableObject<T>(IMaintainableRefObject maintainableReference, bool returnStub, bool returnLatest) where T : IMaintainableObject
        {
            if (maintainableReference == null)
            {
                throw new ArgumentNullException("maintainableReference");
            }

            if (!returnLatest)
            {
                returnLatest = !ObjectUtil.ValidObject(maintainableReference.Version);
            }

            return ExtractFromSet(this.GetMaintainableObjects<T>(maintainableReference, returnStub, returnLatest));
        }

        /// <summary>
        ///     Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject
        ///     argument.
        ///     An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable. It is constraint  </typeparam>
        /// <param name="maintainableInterface">The maintainable interface.</param>
        /// <param name="maintainableReference">
        ///     Contains the identifiers of the structures to returns, can include wild-carded
        ///     values (null indicates a wild-card).
        /// </param>
        /// <returns>
        ///     The set of <see cref="IMaintainableObject" /> .
        /// </returns>
        /// <remarks>
        ///     This method exists only for compatibility reasons with Java implementation of this interface which uses raw
        ///     types and unchecked generics.
        /// </remarks>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="InvalidCastException">An artefact cannot be cast to type .</exception>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefacts.</exception>
        public virtual ISet<T> GetMaintainableObjects<T>(Type maintainableInterface, IMaintainableRefObject maintainableReference) where T : ISdmxObject
        {
            SdmxStructureType type = SdmxStructureType.ParseClass(maintainableInterface);
            return new HashSet<T>(this.GetMaintainablesOfType<IMaintainableObject>(type, maintainableReference, false, false).Cast<T>());
        }

        /// <summary>
        ///     Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject
        ///     argument.
        ///     An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable. It is constraint</typeparam>
        /// <param name="maintainableInterface">The maintainable interface.</param>
        /// <param name="maintainableReference">
        ///     Contains the identifiers of the structures to returns, can include wild-carded
        ///     values (null indicates a wild-card).
        /// </param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <returns>
        ///     The set of <see cref="IMaintainableObject" /> .
        /// </returns>
        /// <remarks>
        ///     This method exists only for compatibility reasons with Java implementation of this interface which uses raw types
        ///     and unchecked generics.
        /// </remarks>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefacts.</exception>
        public virtual ISet<T> GetMaintainableObjects<T>(Type maintainableInterface, IMaintainableRefObject maintainableReference, bool returnStub, bool returnLatest) where T : IMaintainableObject
        {
            SdmxStructureType type = SdmxStructureType.ParseClass(maintainableInterface);
            return this.GetMaintainablesOfType<T>(type, maintainableReference, returnStub, returnLatest);
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T
        /// <p />
        /// An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The generic type.</typeparam>
        /// <returns>The set of <see cref="IMaintainableObject" /> .</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="InvalidCastException">An artefact cannot be cast to type <typeparamref name="T"/>.</exception>
        public virtual ISet<T> GetMaintainableObjects<T>() where T : IMaintainableObject
        {
            return this.GetMaintainableObjects<T>(null);
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject
        /// argument.
        /// An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable.</typeparam>
        /// <param name="maintainableReference">Contains the identifiers of the structures to returns, can include wildcarded values (null indicates a wildcard).</param>
        /// <returns>The set of <see cref="IMaintainableObject" /> .</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="InvalidCastException">An artefact cannot be cast to type <typeparamref name="T"/>.</exception>
        public virtual ISet<T> GetMaintainableObjects<T>(IMaintainableRefObject maintainableReference) where T : IMaintainableObject
        {
            return this.GetMaintainableObjects<T>(maintainableReference, false, false);
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject
        /// argument.
        /// An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable.</typeparam>
        /// <param name="maintainableReference">Contains the identifiers of the structures to returns, can include wildcarded values (null indicates a wildcard).</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <param name="returnLatest">If true then the latest version is returned, regardless of whether version information is supplied</param>
        /// <returns>The set of <see cref="IMaintainableObject" /> .</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="maintainableReference" /> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="InvalidCastException">An artefact cannot be cast to type <typeparamref name="T"/>.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "It is OK. Big switch statement.")]
        public virtual ISet<T> GetMaintainableObjects<T>(IMaintainableRefObject maintainableReference, bool returnStub, bool returnLatest) where T : IMaintainableObject
        {
            ISet<T> returnSet;
            if (maintainableReference == null)
            {
                maintainableReference = new MaintainableRefObjectImpl();
            }

            if (returnLatest)
            {
                maintainableReference = new MaintainableRefObjectImpl(maintainableReference.AgencyId, maintainableReference.MaintainableId, null);
            }

            SdmxStructureType type = SdmxStructureType.ParseClass(typeof(T));
            IStructureReference structureReference = new StructureReferenceImpl(maintainableReference, type);
            switch (structureReference.TargetReference.EnumType)
            {
                case SdmxStructureEnumType.AgencyScheme:
                    returnSet = new HashSet<T>(this.GetAgencySchemeObjects(maintainableReference, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.DataConsumerScheme:
                    returnSet = new HashSet<T>(this.GetDataConsumerSchemeObjects(maintainableReference, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.AttachmentConstraint:
                    returnSet = new HashSet<T>(this.GetAttachmentConstraints(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.ContentConstraint:
                    returnSet = new HashSet<T>(this.GetContentConstraints(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.DataProviderScheme:
                    returnSet = new HashSet<T>(this.GetDataProviderSchemeObjects(maintainableReference, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.Categorisation:
                    returnSet = new HashSet<T>(this.GetCategorisationObjects(maintainableReference, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    returnSet = new HashSet<T>(this.GetCategorySchemeObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.CodeList:
                    returnSet = new HashSet<T>(this.GetCodelistObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    returnSet = new HashSet<T>(this.GetConceptSchemeObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.Dataflow:
                    returnSet = new HashSet<T>(this.GetDataflowObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    returnSet = new HashSet<T>(this.GetHierarchicCodeListObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.Dsd:
                    returnSet = new HashSet<T>(this.GetDataStructureObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.MetadataFlow:
                    returnSet = new HashSet<T>(this.GetMetadataflowObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.Msd:
                    returnSet = new HashSet<T>(this.GetMetadataStructureObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    returnSet = new HashSet<T>(this.GetOrganisationUnitSchemeObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.Process:
                    returnSet = new HashSet<T>(this.GetProcessObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.ReportingTaxonomy:
                    returnSet = new HashSet<T>(this.GetReportingTaxonomyObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.StructureSet:
                    returnSet = new HashSet<T>(this.GetStructureSetObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                case SdmxStructureEnumType.ProvisionAgreement:
                    returnSet = new HashSet<T>(this.GetProvisionAgreementObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, structureReference.TargetReference);
            }

            return this.HandleReturnDetails(returnStub, returnSet);
        }

        /// <summary>
        ///     Get all the maintainable that match the <paramref name="restquery" />
        /// </summary>
        /// <param name="restquery">The REST structure query.</param>
        /// <returns>the maintainable that match the <paramref name="restquery" /></returns>
        /// <exception cref="ArgumentNullException"><paramref name="restquery"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxNoResultsException">if the maintainable could not be resolved from the given endpoint</exception>
        /// <exception cref="SdmxNotImplementedException">Cannot return stubs since no ServiceRetrievalManager has been supplied!</exception>
        public virtual ISdmxObjects GetMaintainables(IRestStructureQuery restquery)
        {
            if (restquery == null)
            {
                throw new ArgumentNullException("restquery");
            }

            _log.Info("Query for maintainables: " + restquery);

            bool isAllStubs = restquery.StructureQueryMetadata.StructureQueryDetail == StructureQueryDetailEnumType.AllStubs;
            bool isRefStubs = isAllStubs || restquery.StructureQueryMetadata.StructureQueryDetail == StructureQueryDetailEnumType.ReferencedStubs;
            bool isLatest = restquery.StructureQueryMetadata.IsReturnLatest;

            ////bool retrieveStubs = false;
            SdmxStructureType type = restquery.StructureReference.MaintainableStructureEnumType;

            // if the type is NULL we'll ask for ANY SdmxStructureType
            if (type == null)
            {
                type = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            IMaintainableRefObject xref = restquery.StructureReference.ChangeStarsToNull().MaintainableReference;

            var queryResultMaintainables = this.GetQueryResultMaintainables(type, xref, isLatest, false);

            ISet<IMaintainableObject> resolvedStubs = new HashSet<IMaintainableObject>();
            if (this.ExternalReferenceRetrievalManager != null && !isAllStubs)
            {
                foreach (IMaintainableObject currentMaint in queryResultMaintainables)
                {
                    if (currentMaint.IsExternalReference.IsTrue)
                    {
                        resolvedStubs.Add(this.ExternalReferenceRetrievalManager.ResolveFullStructure(currentMaint));
                    }
                }
            }

            foreach (IMaintainableObject qrm in queryResultMaintainables)
            {
                resolvedStubs.Add(qrm);
            }

            queryResultMaintainables = resolvedStubs;
            _log.Info("Returned " + queryResultMaintainables.Count + " results");

            IHeader header = null;
            if (this.HeaderRetrievalManager != null)
            {
                header = this.HeaderRetrievalManager.Header;
            }

            ISdmxObjects referencedBeans = new SdmxObjectsImpl(header);
            ISdmxObjects referenceMerge = new SdmxObjectsImpl();

            switch (restquery.StructureQueryMetadata.StructureReferenceDetail.EnumType)
            {
                case StructureReferenceDetailEnumType.None:
                    _log.Info("Reference detail NONE");
                    break;
                case StructureReferenceDetailEnumType.Parents:
                    _log.Info("Reference detail PARENTS");
                    this.ResolveParents(queryResultMaintainables, referencedBeans, isRefStubs);
                    break;
                case StructureReferenceDetailEnumType.ParentsSiblings:
                    _log.Info("Reference detail PARENTS_SIBLINGS");
                    this.ResolveParents(queryResultMaintainables, referencedBeans, isRefStubs);
                    this.ResolveChildren(referencedBeans.GetAllMaintainables(), referenceMerge, isRefStubs);
                    referencedBeans.Merge(referenceMerge);
                    break;
                case StructureReferenceDetailEnumType.Children:
                    _log.Info("Reference detail CHILDREN");
                    this.ResolveChildren(queryResultMaintainables, referencedBeans, isRefStubs);
                    break;
                case StructureReferenceDetailEnumType.Descendants:
                    _log.Info("Reference detail DESCENDANTS");
                    this.ResolveDescendants(queryResultMaintainables, referencedBeans, isRefStubs);
                    break;
                case StructureReferenceDetailEnumType.All:
                    _log.Info("Reference detail ALL");
                    this.ResolveParents(queryResultMaintainables, referencedBeans, isRefStubs);
                    this.ResolveDescendants(queryResultMaintainables, referenceMerge, isRefStubs);
                    referencedBeans.Merge(referenceMerge);
                    break;
                case StructureReferenceDetailEnumType.Specific:
                    _log.Info("Reference detail Children");
                    this.ResolveSpecific(queryResultMaintainables, referencedBeans, restquery.StructureQueryMetadata.SpecificStructureReference, isRefStubs);
                    break;
            }

            referencedBeans.AddIdentifiables(queryResultMaintainables);

            // Determine if we are returning stubs or not
            if (isAllStubs)
            {
                // It is not necessary to specify a serviceRetrievalManager so it may be null at this stage
                if (this._serviceRetrievalManager == null)
                {
                    throw new SdmxNotImplementedException("Cannot return stubs since no ServiceRetrievalManager has been supplied!");
                }

                ISet<IMaintainableObject> stubSet = new HashSet<IMaintainableObject>();

                foreach (IMaintainableObject currentMaint in referencedBeans.GetAllMaintainables(null))
                {
                    stubSet.Add(this._serviceRetrievalManager.CreateStub(currentMaint));
                }

                referencedBeans = new SdmxObjectsImpl(header);
                referencedBeans.AddIdentifiables(stubSet);
            }

            _log.Info("Result Size : " + referencedBeans.GetAllMaintainables().Count);
            return referencedBeans;
        }

        /// <summary>
        /// Returns MetadataFlowObjects that match the parameters in the ref object.  If the ref object is null or
        /// has no attributes set, then this will be interpreted as a search for all MetadataFlowObjects
        /// </summary>
        /// <param name="xref">The reference object defining the search parameters, can be empty or null</param>
        /// <param name="returnLatest">If true then the latest versions of the structures that match the query will be returned.  If version information
        /// is supplied
        /// then it will be ignored</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <returns>Set of sdmxObjects that match the search criteria</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact</exception>
        public virtual ISet<IMetadataFlow> GetMetadataflowObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IMetadataFlow>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetMetadataflowObjects");
        }

        /// <summary>
        ///     Returns MetadataStructureObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all MetadataStructureObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<IMetadataStructureDefinitionObject> GetMetadataStructureObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IMetadataStructureDefinitionObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetMetadataStructureObjects");
        }

        /// <summary>
        /// Returns OrganisationUnitSchemeObjects that match the parameters in the ref object.  If the ref object is null or
        /// has no attributes set, then this will be interpreted as a search for all OrganisationUnitSchemeObjects
        /// </summary>
        /// <param name="xref">The reference object defining the search parameters, can be empty or null</param>
        /// <param name="returnLatest">If true then the latest versions of the structures that match the query will be returned.  If version information
        /// is supplied
        /// then it will be ignored</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <returns>Set of sdmxObjects that match the search criteria</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<IOrganisationUnitSchemeObject> GetOrganisationUnitSchemeObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IOrganisationUnitSchemeObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetOrganisationUnitSchemeObjects");
        }

        /// <summary>
        /// Returns ProcessObjects that match the parameters in the ref object.  If the ref object is null or
        /// has no attributes set, then this will be interpreted as a search for all ProcessObjects
        /// </summary>
        /// <param name="xref">The reference object defining the search parameters, can be empty or null</param>
        /// <param name="returnLatest">If true then the latest versions of the structures that match the query will be returned.  If version information
        /// is supplied
        /// then it will be ignored</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <returns>Set of sdmxObjects that match the search criteria</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<IProcessObject> GetProcessObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IProcessObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetProcessObjects");
        }

        /// <summary>
        ///     Returns ProvisionAgreementObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ProvisionAgreementObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<IProvisionAgreementObject> GetProvisionAgreementObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IProvisionAgreementObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetProvisionAgreementObjects");
        }

        /// <summary>
        ///     Returns ReportingTaxonomyObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all ReportingTaxonomyObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<IReportingTaxonomyObject> GetReportingTaxonomyObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IReportingTaxonomyObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetReportingTaxonomyObjects");
        }

        /// <summary>
        ///     Gets the SDMX objects.
        /// </summary>
        /// <param name="structureReference">The <see cref="IStructureReference" /> which must not be null.</param>
        /// <param name="resolveCrossReferences">
        ///     either 'do not resolve', 'resolve all' or 'resolve all excluding agencies'. If not
        ///     set to 'do not resolve' then all the structures that are referenced by the resulting structures are also returned
        ///     (and also their children).  This will be equivalent to descendants for a <c>RESTful</c> query..
        /// </param>
        /// <returns>
        ///     Returns a <see cref="ISdmxObjects" /> container containing all the Maintainable Objects that match the query
        ///     parameters as defined by the <paramref name="structureReference" />.
        /// </returns>
        /// <exception cref="SdmxSyntaxException">Unknown Maintainable Type</exception>
        /// <exception cref="ArgumentNullException"><paramref name="structureReference"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxNotImplementedException">Unknown condition encountered.</exception>
        public virtual ISdmxObjects GetSdmxObjects(IStructureReference structureReference, ResolveCrossReferences resolveCrossReferences)
        {
            if (structureReference == null)
            {
                throw new ArgumentNullException("structureReference");
            }

            ISet<IMaintainableObject> maintainables =
                this.GetMaintainablesOfType<IMaintainableObject>(
                    SdmxStructureType.ParseClass(structureReference.MaintainableStructureEnumType.MaintainableInterface), 
                    structureReference.MaintainableReference, 
                    false, 
                    false);
            ISdmxObjects beans = new SdmxObjectsImpl();
            beans.AddIdentifiables(maintainables);

            switch (resolveCrossReferences)
            {
                case ResolveCrossReferences.DoNotResolve:
                    break;
                case ResolveCrossReferences.ResolveAll:
                    this.ResolveReferences(beans, true);
                    break;
                case ResolveCrossReferences.ResolveExcludeAgencies:
                    this.ResolveReferences(beans, false);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Unknown condition encountered for resolveCrossReferences. Value: " + resolveCrossReferences);
            }

            return beans;
        }

        /// <summary>
        ///     Returns StructureSetObjects that match the parameters in the ref object.  If the ref object is null or
        ///     has no attributes set, then this will be interpreted as a search for all StructureSetObjects
        /// </summary>
        /// <param name="xref">
        ///     The reference object defining the search parameters, can be empty or null
        /// </param>
        /// <param name="returnLatest">
        ///     If true then the latest versions of the structures that match the query will be returned.  If version information
        ///     is supplied
        ///     then it will be ignored
        /// </param>
        /// <param name="returnStub">
        ///     If true then a stub object will be returned
        /// </param>
        /// <returns>
        ///     Set of sdmxObjects that match the search criteria
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<IStructureSetObject> GetStructureSetObjects(IMaintainableRefObject xref, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<IStructureSetObject>(xref, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetStructureSetObjects");
        }

        /// <summary>
        /// Gets the subscription objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <returns>The <see cref="ISet{ISubscriptionObject}" />.</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefact.</exception>
        public virtual ISet<ISubscriptionObject> GetSubscriptionObjects(IMaintainableRefObject maintainableRef, bool returnLatest, bool returnStub)
        {
            if (this.Proxy != null)
            {
                return this.Proxy.GetMaintainableObjects<ISubscriptionObject>(maintainableRef, returnStub, returnLatest);
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "GetSubscriptionObjects");
        }

        /// <summary>
        /// Gets all the maintainable objects in this container, returns an empty set if no maintainable objects exist in this
        /// container
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <param name="returnLatest">if set to <c>true</c> [return latest].</param>
        /// <param name="returnStubs">if set to <c>true</c> [return stubs].</param>
        /// <returns>set of all maintainable objects, minus any which match the optional exclude parameters</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefacts.</exception>
        protected virtual ISet<IMaintainableObject> GetAllMaintainables(IMaintainableRefObject maintainableRef, bool returnLatest, bool returnStubs)
        {
            ISet<IMaintainableObject> q = new HashSet<IMaintainableObject>();
            foreach (SdmxStructureType currentMaintainable in SdmxStructureType.MaintainableStructureTypes)
            {
                if (currentMaintainable != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Registration) && currentMaintainable != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Subscription))
                {
                    foreach (IMaintainableObject mr in
                        this.GetMaintainablesOfType<IMaintainableObject>(currentMaintainable.EnumType, maintainableRef, returnStubs, returnLatest))
                    {
                        q.Add(mr);
                    }
                }
            }

            return q;
        }

        /// <summary>
        ///     If the set is of size 1, then returns the element in the set.
        ///     Returns null if the set is null or of size 0
        /// </summary>
        /// <typeparam name="T">
        ///     The maintainable type
        /// </typeparam>
        /// <param name="set">
        ///     set of elements
        /// </param>
        /// <returns>
        ///     The first element of the set.
        ///     Throws SdmxException if the set contains more then 1 element
        /// </returns>
        private static T ExtractFromSet<T>(ICollection<T> set) where T : IMaintainableObject
        {
            if (!ObjectUtil.ValidCollection(set))
            {
                return default(T);
            }

            if (set.Count == 1)
            {
                return set.First();
            }

            throw new SdmxException("Did not expect more then 1 structure from query, got " + set.Count + " structures.");
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type structureType that match the reference parameters in the
        /// IMaintainableRefObject argument.
        /// An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The maintainable type</typeparam>
        /// <param name="structureType">Contains sdmx structure type</param>
        /// <param name="maintainableReference">Contains the identifiers of the structures to returns, can include wildcarded values (null indicates a wildcard).</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <param name="returnLatest">If true then the latest version is returned, regardless of whether version information is supplied</param>
        /// <returns>The set of <see cref="IMaintainableObject" /> .</returns>
        /// <exception cref="SdmxNotImplementedException">Unsupported artefacts.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "It is OK. Big switch statement.")]
        private ISet<T> GetMaintainablesOfType<T>(SdmxStructureEnumType structureType, IMaintainableRefObject maintainableReference, bool returnStub, bool returnLatest) where T : IMaintainableObject
        {
            ISet<T> returnSet;

            if (returnLatest)
            {
                maintainableReference = new MaintainableRefObjectImpl(maintainableReference.AgencyId, maintainableReference.MaintainableId, null);
            }

            switch (structureType)
            {
                case SdmxStructureEnumType.AgencyScheme:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IAgencyScheme>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.DataConsumerScheme:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IDataConsumerScheme>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.AttachmentConstraint:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IAttachmentConstraintObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.ContentConstraint:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IContentConstraintObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.DataProviderScheme:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IDataProviderScheme>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.Categorisation:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<ICategorisationObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<ICategorySchemeObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.CodeList:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<ICodelistObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IConceptSchemeObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.Dataflow:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IDataflowObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IHierarchicalCodelistObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.Dsd:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IDataStructureObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.MetadataFlow:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IMetadataFlow>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.Msd:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IMetadataStructureDefinitionObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IOrganisationUnitSchemeObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.Process:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IProcessObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.ReportingTaxonomy:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IReportingTaxonomyObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.StructureSet:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IStructureSetObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                case SdmxStructureEnumType.ProvisionAgreement:
                    returnSet = new HashSet<T>(this.GetMaintainableObjects<IProvisionAgreementObject>(maintainableReference, returnStub, returnLatest).Cast<T>());
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, structureType);
            }

            return this.HandleReturnDetail(returnStub, returnSet);
        }

        /// <summary>
        /// Gets the query result for maintainable.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <param name="isLatest">if set to <c>true</c> [is latest].</param>
        /// <param name="retrieveStubs">if set to <c>true</c> [retrieve stubs].</param>
        /// <returns>The <see cref="ISet{IMaintainableObject}" />.</returns>
        private ISet<IMaintainableObject> GetQueryResultMaintainables(SdmxStructureType type, IMaintainableRefObject maintainableRef, bool isLatest, bool retrieveStubs)
        {
            ISet<IMaintainableObject> queryResultMaintainables;

            if (type.EnumType == SdmxStructureEnumType.Any)
            {
                queryResultMaintainables = this.GetAllMaintainables(maintainableRef, isLatest, retrieveStubs);
            }
            else if (type.EnumType == SdmxStructureEnumType.OrganisationScheme)
            {
                ISdmxObjects beans = new SdmxObjectsImpl();
                beans.AddIdentifiables(this.GetMaintainableObjects<IAgencyScheme>(maintainableRef, retrieveStubs, isLatest));
                beans.AddIdentifiables(this.GetMaintainableObjects<IDataProviderScheme>(maintainableRef, retrieveStubs, isLatest));
                beans.AddIdentifiables(this.GetMaintainableObjects<IOrganisationUnitSchemeObject>(maintainableRef, retrieveStubs, isLatest));
                beans.AddIdentifiables(this.GetMaintainableObjects<IDataConsumerScheme>(maintainableRef, retrieveStubs, isLatest));
                queryResultMaintainables = beans.GetAllMaintainables();
            }
            else
            {
                queryResultMaintainables = this.GetMaintainablesOfType<IMaintainableObject>(SdmxStructureType.ParseClass(type.MaintainableInterface), maintainableRef, retrieveStubs, isLatest);
            }

            return queryResultMaintainables;
        }

        /// <summary>
        /// Handles the return detail.
        /// </summary>
        /// <typeparam name="T">The maintainable type</typeparam>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <param name="returnSet">The return set.</param>
        /// <returns>The <see cref="ISet{T}" />.</returns>
        private ISet<T> HandleReturnDetail<T>(bool returnStub, ISet<T> returnSet) where T : IMaintainableObject
        {
            if (returnStub && this._serviceRetrievalManager != null)
            {
                ISet<T> stubSet = new HashSet<T>();
                foreach (T returnItm in returnSet)
                {
                    if (returnItm.IsExternalReference.IsTrue)
                    {
                        stubSet.Add(returnItm);
                    }
                    else
                    {
                        stubSet.Add((T)this._serviceRetrievalManager.CreateStub(returnItm));
                    }
                }

                returnSet = stubSet;
            }

            return returnSet;
        }

        /// <summary>
        /// Handles the return details.
        /// </summary>
        /// <typeparam name="T">The maintainable type</typeparam>
        /// <param name="returnStub">if set to <c>true</c> return stub artefacts.</param>
        /// <param name="returnSet">The return set.</param>
        /// <returns>The <see cref="ISet{T}"/>.</returns>
        private ISet<T> HandleReturnDetails<T>(bool returnStub, ISet<T> returnSet) where T : IMaintainableObject
        {
            if (returnStub && this._serviceRetrievalManager != null)
            {
                ISet<T> stubSet = new HashSet<T>();
                foreach (T returnItm in returnSet)
                {
                    if (returnItm.IsExternalReference.IsTrue)
                    {
                        stubSet.Add(returnItm);
                    }
                    else
                    {
                        stubSet.Add((T)this._serviceRetrievalManager.CreateStub(returnItm));
                    }
                }

                returnSet = stubSet;
            }

            return returnSet;
        }

        /// <summary>
        /// The resolve children references.
        /// </summary>
        /// <param name="resolveFor">The set of references.</param>
        /// <param name="referencedBeans">The referenced objects.</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <exception cref="SdmxNotImplementedException">Cannot resolve children because CrossReferenceRetrievalManager is not set</exception>
        private void ResolveChildren(IEnumerable<IMaintainableObject> resolveFor, ISdmxObjects referencedBeans, bool returnStub)
        {
            _log.Info("Resolving Child Structures");
            if (this._crossReferenceRetrievalManager == null)
            {
                throw new SdmxNotImplementedException("Cannot resolve children because CrossReferenceRetrievalManager is not set");
            }

            foreach (var currentMaintainable in resolveFor)
            {
                _log.Debug("Resolving Children of " + currentMaintainable.Urn);
                referencedBeans.AddIdentifiables(this._crossReferenceRetrievalManager.GetCrossReferencedStructures(currentMaintainable, returnStub));
            }

            _log.Info(referencedBeans.GetAllMaintainables().Count + " children found");
        }

        /// <summary>
        /// The resolve descendants references.
        /// </summary>
        /// <param name="resolveFor">The set of references.</param>
        /// <param name="referencedBeans">The referenced objects.</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        private void ResolveDescendants(ISet<IMaintainableObject> resolveFor, ISdmxObjects referencedBeans, bool returnStub)
        {
            // WARNING The following code was altered to fix an issue, present in the Java version. The issue was it was resolving the references twice in some cases.
            // This had as result querying a database twice.
            var resolveStack = new Stack<IMaintainableObject>(resolveFor);
            while (resolveStack.Count > 0)
            {
                var currentPossibleParent = new HashSet<IMaintainableObject> { resolveStack.Pop() };
                ISdmxObjects resolvedObjects = new SdmxObjectsImpl();
                this.ResolveChildren(currentPossibleParent, resolvedObjects, returnStub);
                foreach (var maintainableObject in resolvedObjects.GetAllMaintainables())
                {
                    if (!referencedBeans.GetMaintainables(maintainableObject.StructureType).Contains(maintainableObject))
                    {
                        resolveStack.Push(maintainableObject);
                        referencedBeans.AddIdentifiable(maintainableObject);
                    }
                }
            }

            _log.Info(referencedBeans.GetAllMaintainables().Count + " descendants found");
        }

        /// <summary>
        /// The resolve parents references.
        /// </summary>
        /// <typeparam name="T">The maintainable type</typeparam>
        /// <param name="resolveFor">The set of references.</param>
        /// <param name="referencedBeans">The referenced objects.</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <exception cref="SdmxNotImplementedException">Resolve parents not supported</exception>
        private void ResolveParents<T>(IEnumerable<T> resolveFor, ISdmxObjects referencedBeans, bool returnStub) where T : IMaintainableObject
        {
            _log.Info("Resolving Parents Structures");
            foreach (T currentMaintainable in resolveFor)
            {
                _log.Debug("Resolving Parents of " + currentMaintainable.Urn);
                if (this._crossReferencingRetrievalManager == null)
                {
                    throw new SdmxNotImplementedException("Resolve parents not supported");
                }

                referencedBeans.AddIdentifiables(this._crossReferencingRetrievalManager.GetCrossReferencingStructures(currentMaintainable, returnStub));
            }

            _log.Info(referencedBeans.GetAllMaintainables().Count + " parents found");
        }

        /// <summary>
        ///     The resolve references.
        /// </summary>
        /// <param name="beans0">
        ///     The beans 0.
        /// </param>
        /// <param name="resolveAgencies">
        ///     If true the agencies will be included
        /// </param>
        private void ResolveReferences(ISdmxObjects beans0, bool resolveAgencies)
        {
            _log.Debug("resolve references for beans create new cross reference resolver engine");
            ICrossReferenceResolverEngine resolver = new CrossReferenceResolverEngineCore();
            IDictionary<IIdentifiableObject, ISet<IIdentifiableObject>> crossReferenceMap = resolver.ResolveReferences(beans0, resolveAgencies, 0, this);

            foreach (var keyValuePair in crossReferenceMap)
            {
                beans0.AddIdentifiable(keyValuePair.Key);

                foreach (IIdentifiableObject valueren in keyValuePair.Value)
                {
                    beans0.AddIdentifiable(valueren);
                }
            }

            _log.Debug("resolve references complete");
        }

        /// <summary>
        /// The resolve specific references.
        /// </summary>
        /// <typeparam name="T">The maintainable type</typeparam>
        /// <param name="resolveFor">The set of references.</param>
        /// <param name="referencedBeans">The referenced objects.</param>
        /// <param name="specificType">The specific type.</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        private void ResolveSpecific<T>(ISet<T> resolveFor, ISdmxObjects referencedBeans, SdmxStructureType specificType, bool returnStub) where T : IMaintainableObject
        {
            _log.Info("Resolving Child Structures");
            foreach (T currentMaintainable in resolveFor)
            {
                _log.Debug("Resolving Children of " + currentMaintainable.Urn);
                referencedBeans.AddIdentifiables(this._crossReferenceRetrievalManager.GetCrossReferencedStructures(currentMaintainable, returnStub, specificType));
                if (this._crossReferencingRetrievalManager != null)
                {
                    referencedBeans.AddIdentifiables(this._crossReferencingRetrievalManager.GetCrossReferencingStructures(currentMaintainable, returnStub, specificType));
                }
            }

            _log.Info(referencedBeans.GetAllMaintainables().Count + " children found");
        }
    }
}