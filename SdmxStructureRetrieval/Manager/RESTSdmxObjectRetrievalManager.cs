﻿// -----------------------------------------------------------------------
// <copyright file="RESTSdmxObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2014-03-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureRetrieval.
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     The rest SDMX object retrieval manager.
    /// </summary>
    public class RESTSdmxObjectRetrievalManager : BaseSdmxObjectRetrievalManager
    {
        /// <summary>
        ///     The structure query builder
        /// </summary>
        private readonly IStructureQueryBuilder<string> _restQueryBuilder;

        /// <summary>
        ///     The log.
        /// </summary>
        private readonly string _restUrl;

        /// <summary>
        ///     The data location factory
        /// </summary>
        private readonly IReadableDataLocationFactory _rdlFactory;

        /// <summary>
        ///     The structure parsing manager
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTSdmxObjectRetrievalManager" /> class.
        /// </summary>
        /// <param name="restUrl">The REST URL string.</param>
        /// <param name="restQueryBuilder">The rest query builder.</param>
        /// <param name="structureParsingManager">The structure parsing manager.</param>
        /// <param name="rdlFactory">The readable-data location factory.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     restQueryBuilder
        ///     or
        ///     <paramref name="structureParsingManager" />
        /// </exception>
        public RESTSdmxObjectRetrievalManager(
            string restUrl, 
            IStructureQueryBuilder<string> restQueryBuilder, 
            IStructureParsingManager structureParsingManager, 
            IReadableDataLocationFactory rdlFactory)
        {
            if (restQueryBuilder == null)
            {
                throw new ArgumentNullException("restQueryBuilder");
            }

            if (structureParsingManager == null)
            {
                throw new ArgumentNullException("structureParsingManager");
            }

            this._restUrl = restUrl;
            this._restQueryBuilder = restQueryBuilder;
            this._structureParsingManager = structureParsingManager;
            this._rdlFactory = rdlFactory ?? new ReadableDataLocationFactory();
        }

        /// <summary>
        /// Gets the rest URL.
        /// </summary>
        /// <value>The rest URL.</value>
        public string RestUrl
        {
            get
            {
                return this._restUrl;
            }
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject
        /// argument.
        /// An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The maintainable type</typeparam>
        /// <param name="maintainableReference">Contains the identifiers of the structures to returns, can include wildcarded values (null indicates a wildcard).</param>
        /// <param name="returnStub">If true then a stub object will be returned</param>
        /// <param name="returnLatest">If true then the latest version is returned, regardless of whether version information is supplied</param>
        /// <returns>The set of <see cref="IMaintainableObject" /> .</returns>
        /// <exception cref="CrossReferenceException">- If the structure document references structures that can not be resolved</exception>
        /// <exception cref="SdmxSemmanticException">- If the structure message is syntactically correct, but the content is illegal</exception>
        /// <exception cref="SdmxSyntaxException">- If the structure message is syntactically invalid</exception>
        /// <exception cref="InvalidCastException">An element in the sequence cannot be cast to type .</exception>
        public override ISet<T> GetMaintainableObjects<T>(
            IMaintainableRefObject maintainableReference, 
            bool returnStub, 
            bool returnLatest)
        {
            SdmxStructureType type = SdmxStructureType.ParseClass(typeof(T));

            IStructureReference structureReference = new StructureReferenceImpl(maintainableReference, type);
            StructureReferenceDetail refDetail =
                StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);
            StructureQueryDetail queryDetail = returnStub
                                                   ? StructureQueryDetail.GetFromEnum(
                                                       StructureQueryDetailEnumType.AllStubs)
                                                   : StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
            IRestStructureQuery query = new RESTStructureQueryCore(queryDetail, refDetail, null, structureReference, returnLatest);
            return new HashSet<T>(this.GetMaintainables(query).GetMaintainables(structureReference.MaintainableStructureEnumType).Cast<T>());
        }

        /// <summary>
        ///     Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject
        ///     argument.
        ///     An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable. It is constraint  </typeparam>
        /// <param name="maintainableInterface">The maintainable interface.</param>
        /// <param name="maintainableReference">
        ///     Contains the identifiers of the structures to returns, can include wild-carded
        ///     values (null indicates a wild-card).
        /// </param>
        /// <returns>
        ///     The set of <see cref="IMaintainableObject" /> .
        /// </returns>
        /// <remarks>
        ///     This method exists only for compatibility reasons with Java implementation of this interface which uses raw
        ///     types and unchecked generics.
        /// </remarks>
        /// <exception cref="CrossReferenceException">
        ///     - If the structure document references structures that can not be resolved
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     - If the structure message is syntactically correct, but the content is illegal
        /// </exception>
        /// <exception cref="SdmxSyntaxException">
        ///     - If the structure message is syntactically invalid
        /// </exception>
        public override ISet<T> GetMaintainableObjects<T>(
            Type maintainableInterface, 
            IMaintainableRefObject maintainableReference)
        {
            // TODO implement.
            SdmxStructureType type = SdmxStructureType.ParseClass(maintainableInterface);

            IStructureReference structureReference = new StructureReferenceImpl(maintainableReference, type);
            StructureReferenceDetail refDetail =
                StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);
            StructureQueryDetail queryDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
            IRestStructureQuery query = new RESTStructureQueryCore(queryDetail, refDetail, null, structureReference, false);
            return new HashSet<T>(this.GetMaintainables(query).GetMaintainables(structureReference.MaintainableStructureEnumType).Cast<T>());
        }

        /// <summary>
        /// Get all the maintainable that match the <paramref name="restquery" />
        /// </summary>
        /// <param name="restquery">The REST structure query.</param>
        /// <returns>the maintainable that match the <paramref name="restquery" /></returns>
        /// <exception cref="SdmxSyntaxException">- If the structure message is syntactically invalid</exception>
        /// <exception cref="CrossReferenceException">- If the structure document references structures that can not be resolved</exception>
        /// <exception cref="SdmxSemmanticException">- If the structure message is syntactically correct, but the content is illegal</exception>
        public override ISdmxObjects GetMaintainables(IRestStructureQuery restquery)
        {
            string restQuery = this._restUrl + "/" + this._restQueryBuilder.BuildStructureQuery(restquery);

            using (IReadableDataLocation rdl = this._rdlFactory.GetReadableDataLocation(restQuery))
            {
                return this._structureParsingManager.ParseStructures(rdl).GetStructureObjects(false);
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        /// <param name="resolveCrossReferences">either 'do not resolve', 'resolve all' or 'resolve all excluding agencies'. If not
        /// set to 'do not resolve' then all the structures that are referenced by the resulting structures are also returned
        /// (and also their children).  This will be equivalent to descendants for a <c>RESTful</c> query..</param>
        /// <returns>
        /// Returns a <see cref="ISdmxObjects" /> container containing all the Maintainable Objects that match the query
        /// parameters as defined by the <paramref name="structureReference" />.
        /// </returns>
        /// <exception cref="SdmxSyntaxException">
        ///     - If the structure message is syntactically invalid
        /// </exception>
        /// <exception cref="CrossReferenceException">
        ///     - If the structure document references structures that can not be resolved
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     - If the structure message is syntactically correct, but the content is illegal
        /// </exception>
        public override ISdmxObjects GetSdmxObjects(
            IStructureReference structureReference, 
            ResolveCrossReferences resolveCrossReferences)
        {
            StructureReferenceDetail refDetail;
            switch (resolveCrossReferences)
            {
                case ResolveCrossReferences.DoNotResolve:
                    refDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);
                    break;
                default:
                    refDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Descendants);
                    break;
            }

            StructureQueryDetail queryDetail = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
            IRestStructureQuery query = new RESTStructureQueryCore(queryDetail, refDetail, null, structureReference, false);
            return this.GetMaintainables(query);
        }
    }
}