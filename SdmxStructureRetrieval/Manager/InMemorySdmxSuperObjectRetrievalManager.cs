﻿// -----------------------------------------------------------------------
// <copyright file="InMemorySdmxSuperObjectRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureRetrieval.
//     SdmxStructureRetrieval is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureRetrieval is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureRetrieval.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.StructureRetrieval.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// In Memory Sdmx Super Object Retrieval Manager.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.StructureRetrieval.Manager.BaseSdmxObjectRetrievalManager" />
    public class InMemorySdmxSuperObjectRetrievalManager : ISdmxSuperObjectRetrievalManager
    {
        /// <summary>
        /// The _super objects
        /// </summary>
        private readonly ISuperObjects _superObjects;

        /// <summary>
        /// Initializes a new instance of the <see cref="InMemorySdmxSuperObjectRetrievalManager" /> class.
        /// </summary>
        /// <param name="superObjects">The super objects.</param>
        public InMemorySdmxSuperObjectRetrievalManager(ISuperObjects superObjects)
        {
            this._superObjects = superObjects ?? new SuperObjects();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InMemorySdmxSuperObjectRetrievalManager" /> class.
        /// </summary>
        /// <param name="superObjectsBuilder">The super object builder.</param>
        /// <param name="sdmxObjects">The sdmx objects.</param>
        public InMemorySdmxSuperObjectRetrievalManager(ISuperObjectsBuilder superObjectsBuilder, ISdmxObjects sdmxObjects)
        {
            if (superObjectsBuilder == null)
            {
                throw new ArgumentNullException("superObjectsBuilder");
            }

            if (sdmxObjects == null)
            {
                throw new ArgumentNullException("sdmxObjects");
            }

            this._superObjects = superObjectsBuilder.Build(sdmxObjects);
        }

        /// <summary>
        /// Gets the category scheme super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ICategorySchemeSuperObject" /></returns>
        /// <exception cref="ArgumentException">Ref is null
        /// or
        /// Ref is missing AgencyId
        /// or
        /// Ref is missing Id</exception>
        public ICategorySchemeSuperObject GetCategorySchemeSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (ICategorySchemeSuperObject)SuperObjectRefUtil<ICategorySchemeSuperObject>.ResolveReference(this._superObjects.CategorySchemes, maintainableRef);
        }

        /// <summary>
        /// Gets the category scheme super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{ICategorySchemeSuperObject}" /></returns>
        public ISet<ICategorySchemeSuperObject> GetCategorySchemeSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<ICategorySchemeSuperObject>().ResolveReferences(this._superObjects.CategorySchemes, maintainableRef);
        }

        /// <summary>
        /// Gets the codelist super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ICodelistSuperObject" /></returns>
        public ICodelistSuperObject GetCodelistSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (ICodelistSuperObject)SuperObjectRefUtil<ICodelistSuperObject>.ResolveReference(this._superObjects.Codelists, maintainableRef);
        }

        /// <summary>
        /// Gets the codelist super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{ICodelistSuperObject}" />.</returns>
        public ISet<ICodelistSuperObject> GetCodelistSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<ICodelistSuperObject>().ResolveReferences(this._superObjects.Codelists, maintainableRef);
        }

        /// <summary>
        /// Gets the concept scheme super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IConceptSchemeSuperObject" />.</returns>
        public IConceptSchemeSuperObject GetConceptSchemeSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (IConceptSchemeSuperObject)SuperObjectRefUtil<ICodelistSuperObject>.ResolveReference(this._superObjects.ConceptSchemes, maintainableRef);
        }

        /// <summary>
        /// Gets the concept scheme super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IConceptSchemeSuperObject}" />.</returns>
        public ISet<IConceptSchemeSuperObject> GetConceptSchemeSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<IConceptSchemeSuperObject>().ResolveReferences(this._superObjects.ConceptSchemes, maintainableRef);
        }

        /// <summary>
        /// Gets the dataflow super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IDataflowSuperObject" />.</returns>
        public IDataflowSuperObject GetDataflowSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (IDataflowSuperObject)SuperObjectRefUtil<IDataflowSuperObject>.ResolveReference(this._superObjects.Dataflows, maintainableRef);
        }

        /// <summary>
        /// Gets the dataflow super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IDataflowSuperObject}" />.</returns>
        public ISet<IDataflowSuperObject> GetDataflowSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<IDataflowSuperObject>().ResolveReferences(this._superObjects.Dataflows, maintainableRef);
        }

        /// <summary>
        /// Gets the data structure super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IDataStructureSuperObject" />.</returns>
        public IDataStructureSuperObject GetDataStructureSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (IDataStructureSuperObject)SuperObjectRefUtil<IDataStructureSuperObject>.ResolveReference(this._superObjects.DataStructures, maintainableRef);
        }

        /// <summary>
        /// Gets the data structure super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IDataStructureSuperObject}" />.</returns>
        public ISet<IDataStructureSuperObject> GetDataStructureSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<IDataStructureSuperObject>().ResolveReferences(this._superObjects.DataStructures, maintainableRef);
        }

        /// <summary>
        /// Gets the hierarchic code list super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IHierarchicalCodelistSuperObject" />.</returns>
        public IHierarchicalCodelistSuperObject GetHierarchicCodeListSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (IHierarchicalCodelistSuperObject)SuperObjectRefUtil<IHierarchicalCodelistSuperObject>.ResolveReference(this._superObjects.HierarchicalCodelists, maintainableRef);
        }

        /// <summary>
        /// Gets the hierarchic code list super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IHierarchicalCodelistSuperObject}" />.</returns>
        public ISet<IHierarchicalCodelistSuperObject> GetHierarchicCodeListSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<IHierarchicalCodelistSuperObject>().ResolveReferences(this._superObjects.HierarchicalCodelists, maintainableRef);
        }

        /// <summary>
        /// Gets the provision agreement super object.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="IProvisionAgreementSuperObject" />.</returns>
        public IProvisionAgreementSuperObject GetProvisionAgreementSuperObject(IMaintainableRefObject maintainableRef)
        {
            return (IProvisionAgreementSuperObject)SuperObjectRefUtil<IProvisionAgreementSuperObject>.ResolveReference(this._superObjects.Provisions, maintainableRef);
        }

        /// <summary>
        /// Gets the provision agreement super objects.
        /// </summary>
        /// <param name="maintainableRef">The maintainable reference.</param>
        /// <returns>The <see cref="ISet{IProvisionAgreementSuperObject}" />.</returns>
        public ISet<IProvisionAgreementSuperObject> GetProvisionAgreementSuperObjects(IMaintainableRefObject maintainableRef)
        {
            return new SuperObjectRefUtil<IProvisionAgreementSuperObject>().ResolveReferences(this._superObjects.Provisions, maintainableRef);
        }
    }
}