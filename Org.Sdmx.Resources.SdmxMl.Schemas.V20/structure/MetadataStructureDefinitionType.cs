﻿// -----------------------------------------------------------------------
// <copyright file="MetadataStructureDefinitionType.cs" company="EUROSTAT">
//   Date Created : 2016-03-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V20.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V20 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V20.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure
{
    /// <summary>
    /// Class MetadataStructureDefinitionType.
    /// </summary>
    /// <seealso cref="Xml.Schema.Linq.XTypedElement" />
    /// <seealso cref="Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.IMaintainableType" />
    public partial class MetadataStructureDefinitionType : IMaintainableType
    {
    }
}