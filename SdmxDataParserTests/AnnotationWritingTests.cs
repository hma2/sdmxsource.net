﻿// -----------------------------------------------------------------------
// <copyright file="AnnotationWritingTests.cs" company="EUROSTAT">
//   Date Created : 2018-01-05
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParserTests.
// 
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
using NUnit.Framework;

namespace SdmxDataParserTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    [TestFixture]
    public class AnnotationWritingTests
    {
        private readonly IDataReaderManager _dataReaderManager = new DataReaderManager();
        private readonly IDataWriterManager _dataWriterManager = new DataWriterManager();
        private readonly IReadableDataLocationFactory _dataLocationFactory = new ReadableDataLocationFactory();

        private readonly IStructureParsingManager _structureParsingManager = new StructureParsingManager(SdmxSchemaEnumType.Null);

        /// <summary>
        /// Test unit for <see cref="CompactDataWriterEngine"/>
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        [TestCase(DataEnumType.Compact20)]
        [TestCase(DataEnumType.Compact21)]
        [TestCase(DataEnumType.Generic20)]
        [TestCase(DataEnumType.Generic21)]
        public void ShouldWriteAnnotations(DataEnumType format)
        {
            string outfile = string.Format(System.Globalization.CultureInfo.InvariantCulture, "Annotations-{0}.xml", format);
            DataType fromEnum = DataType.GetFromEnum(format);
            var objects = GetSdmxObjects("tests/v21/Structure/test-sdmxv2.1-ESTAT+STS+2.0.xml");
            IDataStructureObject dataStructureObject = objects.DataStructures.First();
            dynamic series = BuildSeries(objects);
            var startTime = new DateTime(2005, 1, 1);
            var annotation = new AnnotationMutableCore() { Title = "TEST_TITLE", Type = "TEST_TYPE" };
            annotation.AddText("en", "TEST_TEXT");
            var a = new AnnotationObjectCore(annotation, null);
            using (Stream writer = File.Create(outfile))
            {
                Stopwatch sw;
                using (IDataWriterEngine dataWriter = this._dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(fromEnum), writer))
                {
                    var header = GetHeader(dataStructureObject);
                    dataWriter.WriteHeader(header);

                    dataWriter.StartDataset(null, dataStructureObject, null, a);
                    sw = new Stopwatch();
                    sw.Start();

                    sw.Stop();
                    Trace.WriteLine(sw.Elapsed);
                    sw.Reset();

                    sw.Start();
                    foreach (var key in series)
                    {
                        dataWriter.StartSeries(a);
                        dataWriter.WriteSeriesKeyValue("FREQ", key.f.Id);
                        dataWriter.WriteSeriesKeyValue("REF_AREA", "DE");
                        dataWriter.WriteSeriesKeyValue("ADJUSTMENT", key.ad.Id);
                        dataWriter.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
                        dataWriter.WriteSeriesKeyValue("STS_ACTIVITY", key.ac.Id);
                        dataWriter.WriteSeriesKeyValue("STS_INSTITUTION", "1");
                        dataWriter.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");
                        var getPeriod = GetPeriodFunc(key.f.Id, startTime);
                        dataWriter.WriteAttributeValue("TIME_FORMAT", GetTimeFormat(key.f.Id));

                        for (int i = 0; i < 10; i++)
                        {
                            string period = getPeriod(i);
                            dataWriter.WriteObservation(period, i.ToString(CultureInfo.InvariantCulture), a);
                            dataWriter.WriteAttributeValue("OBS_STATUS", "A");
                        }
                    }

                    dataWriter.Close();
                }

                sw.Stop();
                Trace.WriteLine(sw.Elapsed);
            }

            BasicFormatValidation(fromEnum, outfile, dataStructureObject, series, startTime);
            var levelCount = new Dictionary<DatasetPosition, int[]>();
            levelCount.Add(DatasetPosition.Dataset, new[] { 0, 0 });
            levelCount.Add(DatasetPosition.Group, new[] { 0, 0 });
            levelCount.Add(DatasetPosition.Series, new[] { 0, 0 });
            levelCount.Add(DatasetPosition.Observation, new[] { 0, 0 });
            var stack = new Stack<DatasetPosition>();
            DatasetPosition index = DatasetPosition.Null;
            
            using (var reader = XmlReader.Create(outfile))
            {
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    switch (nodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                var localName = reader.LocalName;
                                switch (localName)
                                {
                                    case "DataSet":
                                        index = DatasetPosition.Dataset;
                                        stack.Push(index);
                                        levelCount[index][0]++;
                                        break;
                                    case "Series":
                                        index = DatasetPosition.Series;
                                        stack.Push(index);

                                        levelCount[index][0]++;
                                        break;
                                    case "Group":
                                        index = DatasetPosition.Group;
                                        stack.Push(index);
                                        levelCount[index][0]++;
                                        break;
                                    case "Obs":
                                        index = DatasetPosition.Observation;
                                        stack.Push(index);
                                        levelCount[index][0]++;
                                        break;
                                    case "Annotation":
                                        index = stack.Peek();
                                        levelCount[index][1]++;
                                        break;
                                    default:
                                        {
                                            var sdmxGroup = dataStructureObject.GetGroup(localName);
                                            if (sdmxGroup != null)
                                            {
                                                index = DatasetPosition.Group;
                                                stack.Push(index);
                                                levelCount[index][0]++;
                                            }
                                        }
                                        break;
                                }
                            }
                            break;
                        case XmlNodeType.EndElement:
                            {
                                var localName = reader.LocalName;
                                switch (localName)
                                {
                                    case "DataSet":
                                    case "Series":
                                    case "Group":
                                    case "Obs":
                                        stack.Pop();
                                        break;
                                    default:
                                        {
                                            var sdmxGroup = dataStructureObject.GetGroup(localName);
                                            if (sdmxGroup != null)
                                            {
                                                stack.Pop();
                                            }
                                        }
                                        break;
                                }
                            }
                            break;
                    }
                }
            }

            foreach (var intse in levelCount)
            {
                var count = intse.Value[0];
                var levelAnnotationCount = intse.Value[1];
                Assert.That(levelAnnotationCount, Is.EqualTo(count), "Count doesn't match for level : {0}", intse.Key);
            }
        }

        private  void BasicFormatValidation(DataType fromEnum, string outfile, IDataStructureObject dataStructureObject, dynamic series, DateTime startTime)
        {
            if (fromEnum.BaseDataFormat.EnumType == BaseDataFormatEnumType.Generic)
            {
                var fileReadableDataLocation = new FileReadableDataLocation(outfile);

                XMLParser.ValidateXml(fileReadableDataLocation, fromEnum.SchemaVersion);
            }

            int dataSetCount = 0;
            using (var location = new FileReadableDataLocation(outfile))
            using (var reader = this._dataReaderManager.GetDataReaderEngine(location, dataStructureObject, null))
            {
                while (reader.MoveNextDataset())
                {
                    Assert.IsEmpty(reader.DatasetAttributes);
                    dataSetCount++;
                    int seriesCount = 0;
                    while (reader.MoveNextKeyable())
                    {
                        var keyValues = reader.CurrentKey;
                        if (keyValues.Series)
                        {
                            dynamic expectedSeries = series[seriesCount];
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("FREQ")).Code, expectedSeries.f.Id);
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("ADJUSTMENT")).Code, expectedSeries.ad.Id);
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("STS_ACTIVITY")).Code, expectedSeries.ac.Id);
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("REF_AREA")).Code, "DE");
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("STS_INDICATOR")).Code, "PROD");
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("STS_INSTITUTION")).Code, "1");
                            Assert.AreEqual(keyValues.Key.First(k => k.Concept.Equals("STS_BASE_YEAR")).Code, "2000");
                            if (fromEnum.SchemaVersion != SdmxSchemaEnumType.Edi)
                            {
                                Assert.AreEqual(keyValues.Attributes.First(k => k.Concept.Equals("TIME_FORMAT")).Code, GetTimeFormat(expectedSeries.f.Id));
                            }

                            seriesCount++;
                            var getPeriod = GetPeriodFunc(expectedSeries.f.Id, startTime);
                            int obsCount = 0;
                            while (reader.MoveNextObservation())
                            {
                                var currentObservation = reader.CurrentObservation;
                                Assert.AreEqual(obsCount.ToString(CultureInfo.InvariantCulture), currentObservation.ObservationValue);
                                Assert.AreEqual(getPeriod(obsCount), currentObservation.ObsTime);
                                obsCount++;
                            }

                            Assert.AreEqual(10, obsCount);
                        }
                    }

                    Assert.AreEqual(series.Length, seriesCount);
                }

                Assert.AreEqual(1, dataSetCount);
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        private ISdmxObjects GetSdmxObjects(string fileName)
        {
            return new FileInfo(fileName).GetSdmxObjects(this._structureParsingManager);
        }


        /// <summary>
        /// Builds the series.
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <returns>The <see cref="System.Object"/>.</returns>
        private static dynamic BuildSeries(ISdmxObjects objects)
        {
            ICodelistObject freqCl = objects.GetCodelists(new MaintainableRefObjectImpl(null, "CL_FREQ", null)).First();
            ICodelistObject adjCl = objects.GetCodelists(new MaintainableRefObjectImpl(null, "CL_ADJUSTMENT", null)).First();
            ICodelistObject actCl = objects.GetCodelists(new MaintainableRefObjectImpl(null, "CL_STS_ACTIVITY", null)).First();
            var freqCodes = new[] { "Q", "A", "M" };
            var validFreqCodes = freqCl.Items.Where(code => freqCodes.Contains(code.Id));
            var series = (from f in validFreqCodes from ad in adjCl.Items from ac in actCl.Items.Where(code => code.Id.StartsWith("NS0")) select new { f, ad, ac }).ToArray();
            return series;
        }

        /// <summary>
        /// Writes the time format.
        /// </summary>
        /// <param name="key">The frequency code.</param>
        /// <returns>The TIme format.</returns>
        private static string GetTimeFormat(string key)
        {
            switch (key)
            {
                case "Q":

                    return "P3M";
                case "A":

                    return "P1Y";
                case "M":

                    return "P1M";
                default:
                    Assert.Fail("Test bug. Check CL_FREQ codes");
                    break;
            }

            return null;
        }

        /// <summary>
        /// Gets the period function.
        /// </summary>
        /// <param name="key">
        /// The frequency code.
        /// </param>
        /// <param name="startTime">
        /// The start time.
        /// </param>
        /// <returns>
        /// The method that returns the period.
        /// </returns>
        private static Func<int, string> GetPeriodFunc(string key, DateTime startTime)
        {
            Func<int, string> getPeriod = null;
            switch (key)
            {
                case "Q":
                    getPeriod = i =>
                        {
                            DateTime months = startTime.AddMonths(3 * i);
                            return DateUtil.FormatDate(months, TimeFormatEnumType.QuarterOfYear);
                        };
                    break;
                case "A":
                    getPeriod = i =>
                        {
                            DateTime months = startTime.AddMonths(12 * i);
                            return DateUtil.FormatDate(months, TimeFormatEnumType.Year);
                        };
                    break;
                case "M":
                    getPeriod = i =>
                        {
                            DateTime months = startTime.AddMonths(i + 1);
                            return DateUtil.FormatDate(months, TimeFormatEnumType.Month);
                        };
                    break;
                default:
                    Assert.Fail("Test bug. Check CL_FREQ codes");
                    break;
            }

            return getPeriod;
        }

        /// <summary>
        /// Gets the header.
        /// </summary>
        /// <param name="dataStructureObject">The data structure object.</param>
        /// <returns>The <see cref="IHeader"/>.</returns>
        private static IHeader GetHeader(IDataStructureObject dataStructureObject)
        {
            IList<IDatasetStructureReference> structures = new List<IDatasetStructureReference> { new DatasetStructureReferenceCore(dataStructureObject.AsReference) };
            IList<IParty> receiver = new List<IParty> { new PartyCore(new List<ITextTypeWrapper>(), "ZZ9", new List<IContact>(), null) };
            var sender = new PartyCore(new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "TEST SENDER", null) }, "ZZ1", null, null);
            IHeader header = new HeaderImpl(
                null,
                structures,
                null,
                DatasetAction.GetFromEnum(DatasetActionEnumType.Information),
                "TEST_DATAFLOW",
                "DATASET_ID",
                null,
                DateTime.Now,
                DateTime.Now,
                null,
                null,
                new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "test header name", null) },
                new List<ITextTypeWrapper> { new TextTypeWrapperImpl("en", "source 1", null) },
                receiver,
                sender,
                true);
            return header;
        }


    }
}