﻿// -----------------------------------------------------------------------
// <copyright file="DataParseManagerTests.cs" company="EUROSTAT">
//   Date Created : 2016-09-08
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParserTests.
// 
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxDataParserTests
{
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.DataParser.Transform;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable"), TestFixture]
    public class DataParseManagerTests
    {
        [SetUp]
        public void SetUp()
        {
            _dataReaderManager = new DataReaderManager();
            _dataWriterManager = new DataWriterManager(new DataWriterFactory());
            _structureParsingManager = new StructureParsingManager();
            _writeableDataLocationFactory = new WriteableDataLocationFactory();
            _sourceData = new FileReadableDataLocation("tests/Data/Compact-VersionTwoPointOne-ESTAT.xml");
            _dataFormat = new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21));
            using (var readable = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0-for_transform.xml"))
            {
                var structureWorkspace = _structureParsingManager.ParseStructures(readable);
                _dsd = structureWorkspace.GetStructureObjects(false).DataStructures.First();
            }

            using (
                var readable =
                    new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+SSTSCONS_PROD_M+2.0.xml"))
            {
                var structureWorkspace = _structureParsingManager.ParseStructures(readable);
                _flow = structureWorkspace.GetStructureObjects(false).Dataflows.First();
            }

            _dataReaderWriterTransform = new DataReaderWriterTransform();
            var sdmxObjectsImpl = new SdmxObjectsImpl();
            sdmxObjectsImpl.AddDataflow(_flow);
            sdmxObjectsImpl.AddDataStructure(_dsd);
            _beanRetrievalManager = new InMemoryRetrievalManager(sdmxObjectsImpl);
            _tempFileName = Path.GetTempFileName();
            _stream = File.Create(_tempFileName);
        }

        private IDataReaderManager _dataReaderManager;
        private IDataReaderWriterTransform _dataReaderWriterTransform;
        private IDataWriterManager _dataWriterManager;
        private IStructureParsingManager _structureParsingManager;
        private IWriteableDataLocationFactory _writeableDataLocationFactory;
        private IReadableDataLocation _sourceData;
        private IDataFormat _dataFormat;
        private IDataStructureObject _dsd;
        private IDataflowObject _flow;
        private ISdmxObjectRetrievalManager _beanRetrievalManager;
        private Stream _stream;
        private string _tempFileName;

        public DataParseManager CreateSut()
        {
            return new DataParseManager(_dataReaderManager, _dataReaderWriterTransform, _dataWriterManager, _structureParsingManager, _writeableDataLocationFactory);
        }

        [Test]
        public void ShouldSplitAndTransformWithDsdLocation()
        {
            var dataParseManager = CreateSut();
            IReadableDataLocation dsdLocation = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0-for_transform.xml");
            _sourceData = new FileReadableDataLocation("tests/Data/Compact-VersionTwoPointOne-ESTAT-two-datasets.xml");
            var performTransformAndSplit = dataParseManager.PerformTransformAndSplit(_sourceData, dsdLocation, _dataFormat);

            foreach (var readableDataLocation in performTransformAndSplit)
            {
                XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
                readableDataLocation.Close();
            }

            Assert.That(performTransformAndSplit.Count, Is.EqualTo(2));
        }

        [Test]
        public void ShouldSplitAndTransformWithRetriver()
        {
            var dataParseManager = CreateSut();
            _sourceData = new FileReadableDataLocation("tests/Data/Compact-VersionTwoPointOne-ESTAT-two-datasets.xml");
            var performTransformAndSplit = dataParseManager.PerformTransformAndSplit(_sourceData, _dataFormat, _beanRetrievalManager);

            foreach (var readableDataLocation in performTransformAndSplit)
            {
                XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
                readableDataLocation.Close();
            }

            Assert.That(performTransformAndSplit.Count, Is.EqualTo(2));
        }

        [Test]
        public void ShouldTransform()
        {
            var dataParseManager = CreateSut();
            var readableDataLocation = dataParseManager.PerformTransform(_sourceData, _dataFormat, _dsd, _flow);
            XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            readableDataLocation.Close();
            File.Delete(Path.GetTempPath() + readableDataLocation.Name);
        }


        [Test]
        public void ShouldTransformDsdLocationAndStream()
        {
            var dataParseManager = CreateSut();
            var fileReadableDataLocation = new FileReadableDataLocation("tests/V21/Structure/test-sdmxv2.1-ESTAT+STS+2.0-for_transform.xml");
            dataParseManager.PerformTransform(_sourceData, fileReadableDataLocation, _stream, _dataFormat);
            var readableDataLocation = new FileReadableDataLocation(_tempFileName);
            XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            readableDataLocation.Close();
            File.Delete(_tempFileName);
        }

        [Test]
        public void ShouldTransformWithDsdAndFlow()
        {
            var dataParseManager = CreateSut();
            dataParseManager.PerformTransform(_sourceData, _stream, _dataFormat, _dsd, _flow);
            var readableDataLocation = new FileReadableDataLocation(_tempFileName);
            XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            readableDataLocation.Close();
            File.Delete(_tempFileName);
        }

        [Test]
        public void ShouldTransformWithRetriever()
        {
            var dataParseManager = CreateSut();
            var readableDataLocation = dataParseManager.PerformTransform(_sourceData, _dataFormat, _beanRetrievalManager);
            XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            readableDataLocation.Close();
            File.Delete(Path.GetTempPath() + readableDataLocation.Name);
        }

        [Test]
        public void ShouldTransformWithRetrieverAndStream()
        {
           
            var dataParseManager = CreateSut();
            dataParseManager.PerformTransform(_sourceData, _stream, _dataFormat, _beanRetrievalManager);
            var readableDataLocation = new FileReadableDataLocation(_tempFileName);
            XMLParser.ValidateXml(readableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            readableDataLocation.Close();
            File.Delete(_tempFileName);
        }
    }
}