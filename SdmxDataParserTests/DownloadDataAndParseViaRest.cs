﻿using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReUsingExamples
{
    using System.IO;
    using System.Net;

    [TestFixture("http://localhost/ws/rest/datastructure/ESTAT/STS/3.1/", "http://localhost/ws/rest/data/SSTSCONS_PROD_A/?lastNObservations=1&detail=full")]
    class DownloadDataAndParseViaRest
    {
        private readonly string _dsdUrl;

        private readonly string _dataUrl;

        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _structureParsingManager = new StructureParsingManager();
        private readonly IDataReaderManager _drm = new DataReaderManager();

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadDataAndParseViaRest"/> class.
        /// </summary>
        /// <param name="dsdUrl">The DSD URL.</param>
        /// <param name="dataUrl">The data URL.</param>
        public DownloadDataAndParseViaRest(string dsdUrl, string dataUrl)
        {
            this._dsdUrl = dsdUrl;
            this._dataUrl = dataUrl;
        }

        [Test, Ignore("Requires internet")]
        [Category("RequiresInternet")]
        public void DownloadDataAndParseWithHttpRequestAndMemoryReadableLocation()
        {
            var dsdUrl = _dsdUrl;
            var dataUrl = _dataUrl;
            IDataStructureObject dataDSD = null;
            using (var dsdLocal = this._readableDataLocationFactory.GetReadableDataLocation(dsdUrl))
            {
                var sdmxObjects = this._structureParsingManager.ParseStructures(dsdLocal);
                dataDSD = sdmxObjects.GetStructureObjects(false).DataStructures.First();
            }

            byte[] inMemoryBytes;
            WebRequest req = WebRequest.Create(dataUrl);
            HttpWebRequest httpReq = (HttpWebRequest)req;
            httpReq.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (var webResponse = httpReq.GetResponse())
            {
                using (var responseStream = webResponse.GetResponseStream())
                {
                    inMemoryBytes = StreamUtil.ToByteArray(responseStream);
                }
            }

            using (var dataLocal = new MemoryReadableLocation(inMemoryBytes))
            {
                using (IDataReaderEngine dre = this._drm.GetDataReaderEngine(dataLocal, dataDSD, null))
                {
                    Assert.That(dre.MoveNextDataset());
                    Assert.That(dre.MoveNextKeyable());
                }
            }
        }

        [Test, Ignore("Requires internet")]
        [Category("RequiresInternet")]
        public void DownloadDataAndParseWithMemoryReadableLocation()
        {
            var dsdUrl = _dsdUrl;
            var dataUrl = _dataUrl;
            IDataStructureObject dataDSD = null;
            using(var dsdLocal = this._readableDataLocationFactory.GetReadableDataLocation(dsdUrl))
            {
                var sdmxObjects = this._structureParsingManager.ParseStructures(dsdLocal);
                dataDSD = sdmxObjects.GetStructureObjects(false).DataStructures.First();
            }

            // expects dataUrl to be a string, if it is not the dataUrl.uri.AbsolutePath can be used
            using (var dataLocal = new MemoryReadableLocation(dataUrl))
            {
                using (IDataReaderEngine dre = this._drm.GetDataReaderEngine(dataLocal, dataDSD, null))
                {
                    Assert.That(dre.MoveNextDataset());
                    Assert.That(dre.MoveNextKeyable());
                }
            }
        }
    }
}
