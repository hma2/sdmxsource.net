﻿// -----------------------------------------------------------------------
// <copyright file="TestDataReaderEngine.cs" company="EUROSTAT">
//   Date Created : 2014-05-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParserTests.
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxDataParserTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
    using Org.Sdmxsource.Sdmx.DataParser.Engine.Reader;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Util.Io;

    using Is = NUnit.Framework.Is;

    /// <summary>
    /// Test unit for <see cref="IDataReaderEngine"/>
    /// </summary>
    [TestFixture]
    public class TestDataReaderEngine
    {
        /// <summary>
        /// The _factory
        /// </summary>
        private readonly IReadableDataLocationFactory _factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDataReaderEngine"/> class.
        /// </summary>
        public TestDataReaderEngine()
        {
            this._factory = new ReadableDataLocationFactory();
        }


        /// <summary>
        /// Tests the csv data reader.
        /// </summary>
        /// <param name="dsdFile"></param>
        /// <param name="dataFile"></param>
        [TestCase("tests/v21/264D_264_SALDI+2.1.xml", "tests/Data/264D_264_SALD_data.csv")]
        [TestCase("tests/v21/264D_264_SALDI+2.1.xml", "tests/Data/264D_264_SALD_data_with_labels.csv")]
        [TestCase("tests/v21/264D_264_SALDI+2.1.xml", "tests/Data/264D_264_SALD_data_mixed.csv")]
        public void TestCsvDataReaderCount(string dsdFile, string dataFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects(dsdFile));

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                var seriesCount = 0;
                var observationCount = 0;

                using (var dataReaderEngine = new CsvDataReaderEngine(sourceData, retrievalManager, null, null))
                {
                    if (dataReaderEngine.MoveNextDataset())
                    {
                        while (dataReaderEngine.MoveNextKeyable())
                        {
                            seriesCount++;

                            while (dataReaderEngine.MoveNextObservation())
                            {
                                observationCount++;
                            }
                        }
                    }
                }

                Assert.AreEqual(2, seriesCount);
                Assert.AreEqual(8, observationCount);
            }
        }

        /// <summary>
        /// Tests the csv data reader.
        /// </summary>
        /// <param name="dsdFile"></param>
        /// <param name="dataFile"></param>
        [TestCase("tests/v21/264D_264_SALDI+2.1.xml", "tests/Data/264D_264_SALD_data.csv")]
        [TestCase("tests/v21/264D_264_SALDI+2.1.xml", "tests/Data/264D_264_SALD_data_with_labels.csv")]
        [TestCase("tests/v21/264D_264_SALDI+2.1.xml", "tests/Data/264D_264_SALD_data_mixed.csv")]
        public void TestCsvDataReader(string dsdFile, string dataFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            retrievalManager.SaveStructures(GetSdmxObjects(dsdFile));

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(dataFile)))
            {
                var dataReaderEngine = new CsvDataReaderEngine(sourceData, retrievalManager, null, null);

                Assert.IsNull(dataReaderEngine.Header);
                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsNotNull(dataReaderEngine.DatasetAttributes);
                Assert.AreEqual("Test csv", dataReaderEngine.DatasetAttributes.First(a => a.Concept == "TITLE").Code);
                Assert.AreEqual("dataset level attribute", dataReaderEngine.DatasetAttributes.First(a=>a.Concept == "METADATA_EN").Code);
                Assert.AreEqual("", dataReaderEngine.DatasetAttributes.First(a=>a.Concept == "METADATA_IT").Code);

                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                Assert.AreEqual("A", dataReaderEngine.CurrentKey.GetKeyValue("FREQ"));
                Assert.AreEqual("ITF11", dataReaderEngine.CurrentKey.GetKeyValue("REF_AREA"));
                Assert.AreEqual("COF", dataReaderEngine.CurrentKey.GetKeyValue("IND_TYPE"));
                Assert.IsTrue(dataReaderEngine.MoveNextObservation());
                Assert.AreEqual("2012", dataReaderEngine.CurrentObservation.ObsTime);

                dataReaderEngine.Close();
            }
        }

        /// <summary>
        /// Tests the compact data reader.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase(@"tests\Data\Compact-VersionTwo.xml")]
        [TestCase(@"tests\Data\Compact-VersionTwoPointOne.xml")]
        public void TestCompactDataReader(string file)
        {
            IDataStructureObject dsd = BuildDsd();
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            {
                var dataReaderEngine = new CompactDataReaderEngine(sourceData, null, dsd);
                Assert.NotNull(dataReaderEngine.Header);
                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                Assert.AreEqual("Q", dataReaderEngine.CurrentKey.GetKeyValue("FREQ"));
                Assert.AreEqual("N", dataReaderEngine.CurrentKey.GetKeyValue("ADJUSTMENT"));
                Assert.AreEqual("A", dataReaderEngine.CurrentKey.GetKeyValue("STS_ACTIVITY"));
                Assert.IsTrue(dataReaderEngine.MoveNextObservation());
                Assert.AreEqual("2005-Q1", dataReaderEngine.CurrentObservation.ObsTime);
            }
        }

        /// <summary>
        /// Tests the generic data reader.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase(@"tests\Data\Generic-VersionTwo.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwoPointOne.xml")]
        public void TestGenericDataReader(string file)
        {
            IDataStructureObject dsd = BuildDsd();
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            {
                var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                Assert.NotNull(dataReaderEngine.Header);
                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                Assert.AreEqual("Q", dataReaderEngine.CurrentKey.GetKeyValue("FREQ"));
                Assert.AreEqual("N", dataReaderEngine.CurrentKey.GetKeyValue("ADJUSTMENT"));
                Assert.AreEqual("A", dataReaderEngine.CurrentKey.GetKeyValue("STS_ACTIVITY"));
                Assert.IsTrue(dataReaderEngine.MoveNextObservation());
                Assert.AreEqual("2005-Q1", dataReaderEngine.CurrentObservation.ObsTime);
            }
        }

        /// <summary>
        /// Test unit for <see cref="IDataReaderEngine"/> 
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase(@"tests\Data\Compact-VersionTwo.xml")]
        [TestCase(@"tests\Data\Compact21-alldim.xml")]
        [TestCase(@"tests\Data\Compact-VersionTwoPointOne-ts.xml")]
        [TestCase(@"tests\Data\Compact-VersionTwoPointOne.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwo.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwoPointOne.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwoPointOne-ts.xml")]
        public void TestDataReaderManager(string file)
        {
            IDataStructureObject dsd = BuildDsd();
            IDataReaderManager manager = new DataReaderManager();
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData, dsd, null))
            {
                Assert.NotNull(dataReaderEngine);
                Assert.NotNull(dataReaderEngine.Header);
                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                Assert.AreEqual("Q", dataReaderEngine.CurrentKey.GetKeyValue("FREQ"));
                Assert.AreEqual("N", dataReaderEngine.CurrentKey.GetKeyValue("ADJUSTMENT"));
                Assert.AreEqual("A", dataReaderEngine.CurrentKey.GetKeyValue("STS_ACTIVITY"));
                Assert.IsTrue(dataReaderEngine.MoveNextObservation());
                Assert.AreEqual("2005-Q1", dataReaderEngine.CurrentObservation.ObsTime);
            }
        }

        /// <summary>
        /// Test unit for <see cref="IDataReaderEngine"/> 
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase(@"tests\Data\Compact-VersionTwo.xml")]
        [TestCase(@"tests\Data\Compact21-alldim.xml")]
        [TestCase(@"tests\Data\Compact-VersionTwoPointOne.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwo.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwoPointOne.xml")]
        public void Test(string file)
        {
            IDataStructureObject dsd = BuildDsd();
            IReportedDateEngine reportedDateEngine = new ReportedDateEngine();
            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), reportedDateEngine), null);
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
            {
                Assert.NotNull(dataReaderEngine);
                Assert.NotNull(dataReaderEngine.Header);
                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                Assert.AreEqual("Q", dataReaderEngine.CurrentKey.GetKeyValue("FREQ"));
                Assert.AreEqual("N", dataReaderEngine.CurrentKey.GetKeyValue("ADJUSTMENT"));
                Assert.AreEqual("A", dataReaderEngine.CurrentKey.GetKeyValue("STS_ACTIVITY"));
                Assert.IsTrue(dataReaderEngine.MoveNextObservation());
                Assert.AreEqual("2005-Q1", dataReaderEngine.CurrentObservation.ObsTime);
            }
        }

        /// <summary>
        /// Tests the bop.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dsd">The data structure definition.</param>
        [TestCase(@"tests\Data\SDMX-BOP-BdE-sample-corrected.xml", @"tests\v21\Structure\DataStructure-IMF.BOP(1.0).xml")]
        [TestCase(@"tests\Data\SDMX-BOP-BdE-sample.xml", @"tests\v21\Structure\DataStructure-IMF.BOP(1.0).xml")]
        [TestCase(@"tests\Data\compact_demo.xml", @"tests\v20\demo_xs_dsd.xml")]
        [TestCase(@"tests\Data\ESTAT_NA_MAIN_1_0.xml", @"tests\v21\Structure\ESTAT+NA_MAIN+1.0.xml")]
        [TestCase(@"tests\Data\MessageGroupInput.xml", @"tests\v20\MessageGroupDSD.xml")]
        public void TestBop(string file, string dsd)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsd)))
            {
               IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                retrievalManager.SaveStructures(structureWorkspace.GetStructureObjects(false));

            }

              IReportedDateEngine reportedDateEngine = new ReportedDateEngine();
            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), reportedDateEngine), null);
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, retrievalManager))
            {
                Assert.NotNull(dataReaderEngine);
                Assert.NotNull(dataReaderEngine.Header);

                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                IKeyable currentKey = dataReaderEngine.CurrentKey;

                while (!currentKey.Series && dataReaderEngine.MoveNextKeyable())
                {
                    currentKey = dataReaderEngine.CurrentKey;
                }
            }
        }

        /// <summary>
        /// Tests the bop.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dsd">The data structure definition.</param>
        [TestCase(@"tests\Data\SDMX-BOP-BdE-sample-corrected.xml", @"tests\v21\Structure\DataStructure-IMF.BOP(1.0).xml")]
        [TestCase(@"tests\Data\SDMX-BOP-BdE-sample.xml", @"tests\v21\Structure\DataStructure-IMF.BOP(1.0).xml")]
        [TestCase(@"tests\Data\compact_demo.xml", @"tests\v20\demo_xs_dsd.xml")]
        [TestCase(@"tests\Data\ESTAT_NA_MAIN_1_0.xml", @"tests\v21\Structure\ESTAT+NA_MAIN+1.0.xml")]
        [TestCase(@"tests\Data\MessageGroupInput.xml", @"tests\v20\MessageGroupDSD.xml", Ignore = true, IgnoreReason = "Fix pending")]
        [TestCase(@"tests\Data\DK003037.xml", @"tests\v21\DataStructure-IMF.BOP(1.4).xml")]
        public void TestFull(string file, string dsd)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsd)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                retrievalManager.SaveStructures(structureWorkspace.GetStructureObjects(false));

            }

            IReportedDateEngine reportedDateEngine = new ReportedDateEngine();
            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), reportedDateEngine), null);
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, retrievalManager))
            {
                while (dataReaderEngine.MoveNextDataset())
                {
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        IKeyable currentKey = dataReaderEngine.CurrentKey;
                        if (currentKey.Series)
                        {
                            while (dataReaderEngine.MoveNextObservation())
                            {
                                
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="IDataReaderEngine" />
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dimAtObs">The dim at OBS.</param>
        [TestCase(@"tests\Data\Compact21-ADJUSTMENT.xml", "ADJUSTMENT")]
        [TestCase(@"tests\Data\Compact21-FREQ.xml", "FREQ")]
        [TestCase(@"tests\Data\Compact21-STS_ACTIVITY.xml", "STS_ACTIVITY")]
        [TestCase(@"tests\Data\Generic21-ADJUSTMENT.xml", "ADJUSTMENT")]
        [TestCase(@"tests\Data\Generic21-FREQ.xml", "FREQ")]
        [TestCase(@"tests\Data\Generic21-STS_ACTIVITY.xml", "STS_ACTIVITY")]
        public void TestDimAtObs(string file, string dimAtObs)
        {
            IDataStructureObject dsd = BuildDsd();
            IReportedDateEngine reportedDateEngine = new ReportedDateEngine();
            var sdmxDataReaderFactory = new SdmxDataReaderFactory(new DataInformationManager(new FixedConceptEngine(), reportedDateEngine), null);
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = sdmxDataReaderFactory.GetDataReaderEngine(sourceData, dsd, null))
            {
                Assert.NotNull(dataReaderEngine);
                Assert.NotNull(dataReaderEngine.Header);
                Assert.IsTrue(dataReaderEngine.MoveNextDataset());
                Assert.IsTrue(dataReaderEngine.MoveNextKeyable());
                IKeyable currentKey = dataReaderEngine.CurrentKey;

                Assert.IsTrue(dataReaderEngine.MoveNextObservation());
                var currentObs = dataReaderEngine.CurrentObservation;
                if (!dimAtObs.Equals("FREQ"))
                {
                    Assert.AreEqual("A", currentKey.GetKeyValue("FREQ"));
                }
                else
                {
                    Assert.AreEqual("A", currentObs.CrossSectionalValue.Code);
                }

                if (!dimAtObs.Equals("ADJUSTMENT"))
                {
                    Assert.AreEqual("N", currentKey.GetKeyValue("ADJUSTMENT"));
                }
                else
                {
                    Assert.AreEqual("N", currentObs.CrossSectionalValue.Code);
                }

                if (!dimAtObs.Equals("STS_ACTIVITY"))
                {
                    Assert.AreEqual("A", currentKey.GetKeyValue("STS_ACTIVITY"));
                }
                else
                {
                    Assert.AreEqual("A", currentObs.CrossSectionalValue.Code);
                }

                Assert.AreEqual("2005", dataReaderEngine.CurrentObservation.ObsTime);
            }
        }

        /// <summary>
        /// Tests the like NSIWC.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase(@"tests\Data\Compact-VersionTwo.xml")]
        [TestCase(@"tests\Data\Compact-VersionTwoPointOne.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwo.xml")]
        [TestCase(@"tests\Data\Generic-VersionTwoPointOne.xml")]
        public void TestLikeNsiWc(string file)
        {
            IDataReaderManager manager = new DataReaderManager();
            IDataStructureObject dsd = BuildDsd();
            IList<IDictionary<string, string>> dataSetStoreList = new List<IDictionary<string, string>>();
            int obscount = 0;
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var compact = manager.GetDataReaderEngine(sourceData, dsd, null))
            {
                while (compact.MoveNextKeyable())
                {
                    if (compact.CurrentKey.Series)
                    {
                        IList<IKeyValue> keyValues = compact.CurrentKey.Key;

                        int index = 0;
                        while (compact.MoveNextObservation())
                        {
                            var dataSetStore = new Dictionary<string, string>(StringComparer.Ordinal);
                            foreach (var key in keyValues)
                            {
                                dataSetStore.Add(key.Concept, key.Code);
                            }

                            IObservation currentObservation = compact.CurrentObservation;
                            Assert.IsNotNullOrEmpty(currentObservation.ObservationValue);
                            Assert.IsNotNullOrEmpty(currentObservation.ObsTime);
                            if (currentObservation.CrossSection)
                            {
                                Assert.IsNotNull(currentObservation.CrossSectionalValue);
                                dataSetStore.Add(currentObservation.CrossSectionalValue.Concept, currentObservation.CrossSectionalValue.Code);
                            }

                            dataSetStore.Add(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime);
                            ISdmxDate sdmxDate = new SdmxDateCore(currentObservation.ObsTime);
                            Assert.AreEqual(sdmxDate.TimeFormatOfDate, currentObservation.ObsTimeFormat);
                            dataSetStore.Add(PrimaryMeasure.FixedId, currentObservation.ObservationValue);
                            int i = int.Parse(currentObservation.ObservationValue, NumberStyles.Any, CultureInfo.InvariantCulture);
                            Assert.AreEqual(index, i, "Expected {0}\nBut was {1} At OBS {2}", index, i, obscount);
                            
                            index++;
                            obscount++;
                            dataSetStoreList.Add(dataSetStore);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tests the like NSIWC.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        [TestCase(@"tests\Data\Compact21-ADJUSTMENT.xml", "ADJUSTMENT")]
        [TestCase(@"tests\Data\Compact21-FREQ.xml", "FREQ")]
        [TestCase(@"tests\Data\Compact21-STS_ACTIVITY.xml", "STS_ACTIVITY")]
        [TestCase(@"tests\Data\Generic21-ADJUSTMENT.xml", "ADJUSTMENT")]
        [TestCase(@"tests\Data\Generic21-FREQ.xml", "FREQ")]
        [TestCase(@"tests\Data\Generic21-STS_ACTIVITY.xml", "STS_ACTIVITY")]
        public void TestLikeNsiWcDimAtObs(string file, string dimensionAtObservation)
        {
            IDataReaderManager manager = new DataReaderManager();
            IDataStructureObject dsd = BuildDsd();
            IList<IDictionary<string, string>> dataSetStoreList = new List<IDictionary<string, string>>();
            int obscount = 0;
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var compact = manager.GetDataReaderEngine(sourceData, dsd, null))
            {
                while (compact.MoveNextKeyable())
                {
                    if (compact.CurrentKey.Series)
                    {
                        IList<IKeyValue> keyValues = compact.CurrentKey.Key;

                        int index = 0;
                        while (compact.MoveNextObservation())
                        {
                            var dataSetStore = new Dictionary<string, string>(StringComparer.Ordinal);
                            foreach (var key in keyValues)
                            {
                                dataSetStore.Add(key.Concept, key.Code);
                            }

                            IObservation currentObservation = compact.CurrentObservation;
                            Assert.IsNotNullOrEmpty(currentObservation.ObservationValue);
                            Assert.IsNotNullOrEmpty(currentObservation.ObsTime);
                            if (currentObservation.CrossSection)
                            {
                                Assert.IsNotNull(currentObservation.CrossSectionalValue);
                                dataSetStore.Add(currentObservation.CrossSectionalValue.Concept, currentObservation.CrossSectionalValue.Code);
                                Assert.AreEqual(dimensionAtObservation, currentObservation.CrossSectionalValue.Concept);
                            }

                            dataSetStore.Add(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime);
                            ISdmxDate sdmxDate = new SdmxDateCore(currentObservation.ObsTime);
                            Assert.AreEqual(sdmxDate.TimeFormatOfDate, currentObservation.ObsTimeFormat);
                            dataSetStore.Add(PrimaryMeasure.FixedId, currentObservation.ObservationValue);
                            int i = int.Parse(currentObservation.ObservationValue, NumberStyles.Any, CultureInfo.InvariantCulture);
                            Assert.AreEqual(index, i, "Expected {0}\nBut was {1} At OBS {2}", index, i, obscount);

                            index++;
                            obscount++;
                            dataSetStoreList.Add(dataSetStore);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tests the like NSIWC.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        [TestCase(@"tests\Data\Compact21-alldim.xml")]
        public void TestLikeNsiWcAll(string file)
        {
            IDataReaderManager manager = new DataReaderManager();
            IDataStructureObject dsd = BuildDsd();
            IList<IDictionary<string, string>> dataSetStoreList = new List<IDictionary<string, string>>();
            int obscount = 0;
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var compact = manager.GetDataReaderEngine(sourceData, dsd, null))
            {
                int index = 0;
                while (compact.MoveNextKeyable())
                {
                    if (compact.CurrentKey.Series)
                    {
                        IList<IKeyValue> keyValues = compact.CurrentKey.Key;

                        if (index >= 100)
                        {
                            index = 0;
                        }

                        while (compact.MoveNextObservation())
                        {
                            var dataSetStore = new Dictionary<string, string>(StringComparer.Ordinal);
                            foreach (var key in keyValues)
                            {
                                dataSetStore.Add(key.Concept, key.Code);
                            }

                            IObservation currentObservation = compact.CurrentObservation;
                            Assert.IsNotNullOrEmpty(currentObservation.ObservationValue);
                            Assert.IsNotNullOrEmpty(currentObservation.ObsTime);
                            if (currentObservation.CrossSection)
                            {
                                Assert.IsNotNull(currentObservation.CrossSectionalValue);
                                dataSetStore.Add(currentObservation.CrossSectionalValue.Concept, currentObservation.CrossSectionalValue.Code);
                            }

                            dataSetStore.Add(DimensionObject.TimeDimensionFixedId, currentObservation.ObsTime);
                            ISdmxDate sdmxDate = new SdmxDateCore(currentObservation.ObsTime);
                            Assert.AreEqual(sdmxDate.TimeFormatOfDate, currentObservation.ObsTimeFormat);
                            dataSetStore.Add(PrimaryMeasure.FixedId, currentObservation.ObservationValue);
                            int i = int.Parse(currentObservation.ObservationValue, NumberStyles.Any, CultureInfo.InvariantCulture);
                            Assert.AreEqual(index, i, "Expected {0}\nBut was {1} At OBS {2}", index, i, obscount);

                            index++;
                            obscount++;
                            dataSetStoreList.Add(dataSetStore);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the DSD.
        /// </summary>
        /// <returns>
        /// The <see cref="IDataStructureObject" />.
        /// </returns>
        private static IDataStructureObject BuildDsd()
        {
            IDataStructureMutableObject dsdMutableObject = new DataStructureMutableCore { AgencyId = "TEST", Id = "TEST_DSD", Version = "1.0" };
            dsdMutableObject.AddName("en", "Test data");

            // FREQ="Q" ADJUSTMENT="N" STS_ACTIVITY="A" 
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"), 
                new StructureReferenceImpl("SDMX", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "ADJUSTMENT"), 
                new StructureReferenceImpl("SDMX", "CL_ADJUSTMENT", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "STS_ACTIVITY"), 
                new StructureReferenceImpl("STS", "CL_STS_ACTIVITY", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new DimensionMutableCore { ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD"), TimeDimension = true });

            dsdMutableObject.AddPrimaryMeasure(new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));

            var attributeMutableObject = dsdMutableObject.AddAttribute(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "DECIMALS"), 
                new StructureReferenceImpl("STS", "CL_DECIMALS", "1.0", SdmxStructureEnumType.CodeList));
            attributeMutableObject.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
            attributeMutableObject.DimensionReferences.AddAll(new[] { "FREQ", "ADJUSTMENT", "STS_ACTIVITY" });
            attributeMutableObject.AssignmentStatus = "Mandatory";
            return dsdMutableObject.ImmutableInstance;
        }

        [TestCase(@"tests\Data\structure-specific-reporting-period-annual-no-yearstartend.xml", @"tests\v21\ESTAT_STS_3.1.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-quarterly-no-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.2.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-quarterly-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.2.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-flat-no-yearstartend.xml", @"tests\v21\ESTAT_STS_3.1.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-monthly-dimensionAtObservation-no-yearstartend.xml", @"tests\v21\ESTAT_STS_2.2.xml")]
        public void TestThatReportingPeriodIsConverted(string file, string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                retrievalManager.SaveStructures(structureWorkspace.GetStructureObjects(false));
            }

            ReportingTimePeriod e = new ReportingTimePeriod();
            var originalReportingPeriods = new Queue<string>();
            string reportinStartYearDate = null;

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            {
                using (var xmlReader = XmlReader.Create(sourceData.InputStream, new XmlReaderSettings() {CloseInput = true, IgnoreWhitespace = true}))
                {
                    while (xmlReader.Read())
                    {
                        switch (xmlReader.NodeType)
                        {
                            case XmlNodeType.Element:
                                var timePeriod = xmlReader.GetAttribute(DimensionObject.TimeDimensionFixedId);
                                if (!string.IsNullOrEmpty(timePeriod))
                                {
                                    originalReportingPeriods.Enqueue(timePeriod);
                                }
                                else
                                {
                                    var value = xmlReader.GetAttribute("REPORTING_YEAR_START_DAY");
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        reportinStartYearDate = value;
                                    }
                                }

                                break;
                        }
                    }
                }

                using (var dataReaderEngine = new CompactDataReaderEngine(sourceData, retrievalManager, null, null))
                {
                    while (dataReaderEngine.MoveNextDataset())
                    {
                        while (dataReaderEngine.MoveNextKeyable())
                        {
                            var currentKey = dataReaderEngine.CurrentKey;
                            if (currentKey.Series)
                            {
                                if (!currentKey.TimeSeries)
                                {
                                    var originalValue = originalReportingPeriods.Dequeue();
                                    var normalizedValue = e.ToGregorianPeriod(originalValue, reportinStartYearDate);
                                    var sdmxDate = new SdmxDateCore(normalizedValue.PeriodStart, normalizedValue.Frequency);
                                    Assert.That(currentKey.ObsTime, Is.EqualTo(sdmxDate.DateInSdmxFormat));
                                }
                                else
                                {
                                    while (dataReaderEngine.MoveNextObservation())
                                    {
                                        var currentObservation = dataReaderEngine.CurrentObservation;
                                        var extractedValue = currentObservation.ObsTime;
                                        var originalValue = originalReportingPeriods.Dequeue();
                                        var normalizedValue = e.ToGregorianPeriod(originalValue, reportinStartYearDate);
                                        var sdmxDate = new SdmxDateCore(normalizedValue.PeriodStart, normalizedValue.Frequency);
                                        Assert.That(extractedValue, Is.EqualTo(sdmxDate.DateInSdmxFormat));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [TestCase(@"tests\Data\generic-data-reporting-period-monthly-dimAtObs-no-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.0.xml")]
        [TestCase(@"tests\Data\generic-data-reporting-period-monthly-no-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.0.xml")]
        [TestCase(@"tests\Data\generic-data-reporting-period-monthly-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.0.xml")]
        public void TestThatReportingPeriodIsConvertedGenericData(string file, string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                retrievalManager.SaveStructures(structureWorkspace.GetStructureObjects(false));
            }

            ReportingTimePeriod e = new ReportingTimePeriod();
            var originalReportingPeriods = new Queue<string>();
            string reportinStartYearDate = null;

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            {
                using (var xmlReader = XmlReader.Create(sourceData.InputStream, new XmlReaderSettings() { CloseInput = true, IgnoreWhitespace = true }))
                {
                    while (xmlReader.Read())
                    {
                        switch (xmlReader.NodeType)
                        {
                            case XmlNodeType.Element:
                                var attributeId = xmlReader.GetAttribute("id");
                                if (!string.IsNullOrEmpty(attributeId))
                                {
                                    var value = xmlReader.GetAttribute("value");
                                    switch (attributeId)
                                    {
                                        case DimensionObject.TimeDimensionFixedId:
                                            originalReportingPeriods.Enqueue(value);
                                            break;
                                        case "REPORTING_YEAR_START_DAY":
                                            reportinStartYearDate = value;
                                            break;
                                    }
                               }

                                break;
                        }
                    }
                }

                using (var dataReaderEngine = new GenericDataReaderEngine(sourceData, retrievalManager, null, null))
                {
                    while (dataReaderEngine.MoveNextDataset())
                    {
                        while (dataReaderEngine.MoveNextKeyable())
                        {
                            var currentKey = dataReaderEngine.CurrentKey;
                            if (currentKey.Series)
                            {
                                if (!currentKey.TimeSeries)
                                {
                                    var originalValue = originalReportingPeriods.Dequeue();
                                    var normalizedValue = e.ToGregorianPeriod(originalValue, reportinStartYearDate);
                                    var sdmxDate = new SdmxDateCore(normalizedValue.PeriodStart, normalizedValue.Frequency);
                                    Assert.That(currentKey.ObsTime, Is.EqualTo(sdmxDate.DateInSdmxFormat));
                                }
                                else
                                {
                                    while (dataReaderEngine.MoveNextObservation())
                                    {
                                        var currentObservation = dataReaderEngine.CurrentObservation;
                                        var extractedValue = currentObservation.ObsTime;
                                        var originalValue = originalReportingPeriods.Dequeue();
                                        var normalizedValue = e.ToGregorianPeriod(originalValue, reportinStartYearDate);
                                        var sdmxDate = new SdmxDateCore(normalizedValue.PeriodStart, normalizedValue.Frequency);
                                        Assert.That(extractedValue, Is.EqualTo(sdmxDate.DateInSdmxFormat));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [TestCase(@"tests\Data\structure-specific-reporting-period-annual-no-yearstartend.xml", @"tests\v21\ESTAT_STS_3.1.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-quarterly-no-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.2.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-quarterly-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.2.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-flat-no-yearstartend.xml", @"tests\v21\ESTAT_STS_3.1.xml")]
        [TestCase(@"tests\Data\structure-specific-reporting-period-monthly-dimensionAtObservation-no-yearstartend.xml", @"tests\v21\ESTAT_STS_2.2.xml")]
        public void TestGregorianPeriodCompactDataReader(string file,string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                retrievalManager.SaveStructures(structureWorkspace.GetStructureObjects(false));
            }
          
            ReportingTimePeriod e = new ReportingTimePeriod();
            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = new CompactDataReaderEngine(sourceData, retrievalManager, null, null))
            {
                Assert.NotNull(dataReaderEngine.Header);

                while (dataReaderEngine.MoveNextDataset())
                {
                    Assert.That(!dataReaderEngine.DatasetAttributes.Any(value => value.Concept.Equals("REPORTING_YEAR_START_DAY")));
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        var currentKey = dataReaderEngine.CurrentKey;
                        if (currentKey.Series)
                        {
                            if (!currentKey.TimeSeries)
                            {
                                Assert.NotNull(currentKey.ObsTime);
                                Assert.NotNull(currentKey.TimeFormat);
                                Assert.NotNull(currentKey.ObsAsTimeDate);
                                Assert.That(e.CheckReportingPeriod(currentKey.ObsTime, null), Is.False);
                                Assert.That(currentKey.ObsTime.Contains("A1"), Is.False);
                                Assert.That(currentKey.ObsTime.Contains("M"), Is.False);
                            }

                            while (dataReaderEngine.MoveNextObservation())
                            {
                                var currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.NotNull(currentObservation.ObsTime);
                                Assert.NotNull(currentObservation.ObsTimeFormat);
                                Assert.NotNull(currentObservation.ObsAsTimeDate);
                                Assert.That(e.CheckReportingPeriod(currentObservation.ObsTime, null), Is.False);
                                Assert.That(currentObservation.ObsTime.Contains("A1"), Is.False);
                                Assert.That(currentObservation.ObsTime.Contains("M"), Is.False);
                            }
                        }
                    }
                }
            }
        }


        [TestCase(@"tests\Data\generic-data-reporting-period-monthly-no-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.0.xml")]
        [TestCase(@"tests\Data\generic-data-reporting-period-monthly-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.0.xml")]
        [TestCase(@"tests\Data\generic-data-reporting-period-monthly-dimAtObs-no-yearstartdate.xml", @"tests\v21\ESTAT_STS_2.0.xml")]
        public void TestGregorianPeriodGenericDataReader(string file, string dsdFile)
        {
            var retrievalManager = new InMemoryRetrievalManager();
            using (IReadableDataLocation dataLocation = _factory.GetReadableDataLocation(new FileInfo(dsdFile)))
            {
                IStructureParsingManager manager = new StructureParsingManager();
                var structureWorkspace = manager.ParseStructures(dataLocation);
                retrievalManager.SaveStructures(structureWorkspace.GetStructureObjects(false));

            }

            ReportingTimePeriod e = new ReportingTimePeriod();

            using (var sourceData = this._factory.GetReadableDataLocation(new FileInfo(file)))
            using (var dataReaderEngine = new GenericDataReaderEngine(sourceData, retrievalManager, null, null))
            {
                Assert.NotNull(dataReaderEngine.Header);
                while (dataReaderEngine.MoveNextDataset())
                {
                    Assert.That(!dataReaderEngine.DatasetAttributes.Any(value => value.Concept.Equals("REPORTING_YEAR_START_DAY")));
                    while (dataReaderEngine.MoveNextKeyable())
                    {
                        IKeyable currentKey = dataReaderEngine.CurrentKey;
                        if (currentKey.Series)
                        {
                            if (!currentKey.TimeSeries)
                            {
                                Assert.NotNull(currentKey.ObsTime);
                                Assert.NotNull(currentKey.TimeFormat);
                                Assert.NotNull(currentKey.ObsAsTimeDate);
                                Assert.That(e.CheckReportingPeriod(currentKey.ObsTime, null), Is.False);
                                Assert.That(currentKey.ObsTime.Contains("A1"), Is.False);
                                Assert.That(currentKey.ObsTime.Contains("M"), Is.False);
                            }

                            while (dataReaderEngine.MoveNextObservation())
                            {
                                var currentObservation = dataReaderEngine.CurrentObservation;
                                Assert.NotNull(currentObservation.ObsTime);
                                Assert.NotNull(currentObservation.ObsTimeFormat);
                                Assert.NotNull(currentObservation.ObsAsTimeDate);
                                Assert.That(e.CheckReportingPeriod(currentObservation.ObsTime, null), Is.False);
                                Assert.That(currentObservation.ObsTime.Contains("A1"), Is.False);
                                Assert.That(currentObservation.ObsTime.Contains("M"), Is.False);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <returns>The <see cref="ISdmxObjects"/>.</returns>
        private static ISdmxObjects GetSdmxObjects(string fileName)
        {
            ISdmxObjects objects;
            var file = new FileInfo(fileName);
            IStructureParsingManager manager = new StructureParsingManager(SdmxSchemaEnumType.Null);
            using (var readable = new FileReadableDataLocation(file))
            {
                IStructureWorkspace structureWorkspace = manager.ParseStructures(readable);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            return objects;
        }
    }
}