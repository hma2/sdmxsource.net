﻿// -----------------------------------------------------------------------
// <copyright file="DataReaderWriterTransformTests.cs" company="EUROSTAT">
//   Date Created : 2016-09-02
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParserTests.
// 
//     SdmxDataParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxDataParserTests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine.Reader;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.DataParser.Transform;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    using Rhino.Mocks;

    [TestFixture]
    internal class DataReaderWriterTransformTests
    {
        public IDataReaderWriterTransform CreateSUT()
        {
            return new DataReaderWriterTransform();
        }


        /// <summary>
        /// Builds the DSD.
        /// </summary>
        /// <returns>
        /// The <see cref="IDataStructureObject" />.
        /// </returns>
        private static IDataStructureObject BuildDsd()
        {
            IDataStructureMutableObject dsdMutableObject = new DataStructureMutableCore {AgencyId = "TEST", Id = "TEST_DSD", Version = "1.0"};
            dsdMutableObject.AddName("en", "Test data");

            // FREQ="Q" ADJUSTMENT="N" STS_ACTIVITY="A" 
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "FREQ"),
                new StructureReferenceImpl("SDMX", "CL_FREQ", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "ADJUSTMENT"),
                new StructureReferenceImpl("SDMX", "CL_ADJUSTMENT", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "STS_ACTIVITY"),
                new StructureReferenceImpl("STS", "CL_STS_ACTIVITY", "1.0", SdmxStructureEnumType.CodeList));
            dsdMutableObject.AddDimension(
                new DimensionMutableCore {ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD"), TimeDimension = true});

            dsdMutableObject.AddPrimaryMeasure(new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));

            var attributeMutableObject = dsdMutableObject.AddAttribute(
                new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "DECIMALS"),
                new StructureReferenceImpl("STS", "CL_DECIMALS", "1.0", SdmxStructureEnumType.CodeList));
            attributeMutableObject.AttachmentLevel = AttributeAttachmentLevel.DimensionGroup;
            attributeMutableObject.DimensionReferences.AddAll(new[] {"FREQ", "ADJUSTMENT", "STS_ACTIVITY"});
            attributeMutableObject.AssignmentStatus = "Mandatory";
            return dsdMutableObject.ImmutableInstance;
        }

        [Test]
        public void CopyDatasetToWriterForNonTimePeriod()
        {
            var dsd = BuildDsd();
            var outfile = Path.GetTempFileName();
            using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
            using (Stream writer = File.Create(outfile))
            using (var dataWriter = new DataWriterManager().GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21)), writer))
            {
                var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                dataReaderEngine.MoveNextDataset();
                var dataReaderWriterTransform = CreateSUT();
                dataReaderWriterTransform.CopyDatasetToWriter(dataReaderEngine, dataWriter, "FREQ", true, 10, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(1), true, true);
            }

            using (var fileReadableDataLocation = new FileReadableDataLocation(outfile))
            {
                XMLParser.ValidateXml(fileReadableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            }

            File.Delete(outfile);
        }

        [Test]
        public void CopyDatasetToWriterForTimePeriod()
        {
            var dsd = BuildDsd();
            var outfile = Path.GetTempFileName();
            using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
            using (Stream writer = File.Create(outfile))
            using (var dataWriter = new DataWriterManager().GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21)), writer))
            {
                var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                dataReaderEngine.MoveNextDataset();
                var dataReaderWriterTransform = CreateSUT();
                dataReaderWriterTransform.CopyDatasetToWriter(dataReaderEngine, dataWriter, "TIME_PERIOD", true, 10, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(1), true, true);
            }


            using (var fileReadableDataLocation = new FileReadableDataLocation(outfile))
            {
                XMLParser.ValidateXml(fileReadableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            }
        }


        [Test]
        public void CopyToWriterForTimePeriod()
        {
            var dsd = BuildDsd();
            var outfile = Path.GetTempFileName();
            using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
            using (Stream writer = File.Create(outfile))
            using (var dataWriter = new DataWriterManager().GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21)), writer))
            {
                var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                dataReaderEngine.MoveNextDataset();
                var dataReaderWriterTransform = CreateSUT();
                dataReaderWriterTransform.CopyToWriter(dataReaderEngine, dataWriter, "TIME_PERIOD", true);
            }

            using (var fileReadableDataLocation = new FileReadableDataLocation(outfile))
            {
                XMLParser.ValidateXml(fileReadableDataLocation, SdmxSchemaEnumType.VersionTwoPointOne);
            }
        }


        [Test]
        public void CopyToWriterWithMockForTimePeriod()
        {
            var mock = new MockRepository();
            var engine = mock.DynamicMock<IDataWriterEngine>();
            using (mock.Record())
            {
                Expect.Call(delegate { engine.WriteObservation("TIME_PERIOD", "2005-Q1","0",null); });
                Expect.Call(delegate { engine.WriteSeriesKeyValue("FREQ", "Q"); });
            }
            using (mock.Playback())
            {
                var dsd = BuildDsd();
                using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
                {
                    var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                    dataReaderEngine.MoveNextDataset();
                    var dataReaderWriterTransform = CreateSUT();
                    dataReaderWriterTransform.CopyToWriter(dataReaderEngine, engine, "TIME_PERIOD", true);
                }    
            }
            
        }

        [Test]
        public void CopyToWriterWithMockWithoutTimePeriod()
        {
            var mock = new MockRepository();
            var engine = mock.DynamicMock<IDataWriterEngine>();
            using (mock.Record())
            {
                Expect.Call(delegate { engine.WriteObservation("TIME_PERIOD", "Q", "0", null); });
                Expect.Call(delegate { engine.WriteSeriesKeyValue("ADJUSTMENT", "N"); });
            }
            using (mock.Playback())
            {
                var dsd = BuildDsd();
                using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
                {
                    var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                    dataReaderEngine.MoveNextDataset();
                    var dataReaderWriterTransform = CreateSUT();
                    dataReaderWriterTransform.CopyToWriter(dataReaderEngine, engine, "FREQ", true);
                }
            }

        }


        [Test]
        public void CopyToWriterForTIME_PERIOD()
        {
            var dsd = BuildDsd();
            var outfile = Path.GetTempFileName();
            using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
            using (Stream writer = File.Create(outfile))
            using (var dataWriter = new DataWriterManager().GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21)), writer))
            {
                var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                dataReaderEngine.MoveNextDataset();
                var dataReaderWriterTransform = CreateSUT();
                dataReaderWriterTransform.CopyToWriter(dataReaderEngine, dataWriter, "TIME_PERIOD", true);
            }
                var xDocument = XDocument.Load(outfile);
                var xElement = xDocument.Descendants().First(x => x.Name.LocalName == "SeriesKey");
                var seriesKey = xElement.Descendants();
                Assert.That(seriesKey.ElementAt(0).FirstAttribute.Value, Is.EqualTo("FREQ"));
                Assert.That(seriesKey.ElementAt(0).LastAttribute.Value, Is.EqualTo("Q"));
                Assert.That(seriesKey.ElementAt(1).FirstAttribute.Value, Is.EqualTo("ADJUSTMENT"));
                Assert.That(seriesKey.ElementAt(1).LastAttribute.Value, Is.EqualTo("N"));
                Assert.That(seriesKey.ElementAt(2).FirstAttribute.Value, Is.EqualTo("STS_ACTIVITY"));
                Assert.That(seriesKey.ElementAt(2).LastAttribute.Value, Is.EqualTo("A"));

                var xElement2 = xDocument.Descendants().First(x => x.Name.LocalName == "Obs");
                var obs = xElement2.Descendants();

                Assert.That(obs.ElementAt(0).FirstAttribute.Value, Is.EqualTo("TIME_PERIOD"));
                Assert.That(obs.ElementAt(0).LastAttribute.Value, Is.EqualTo("2005-Q1"));
                Assert.That(obs.ElementAt(1).FirstAttribute.Value, Is.EqualTo("0"));
        }


        [Test]
        public void CopyToWriterForFREQ()
        {
            var dsd = BuildDsd();
            var outfile = Path.GetTempFileName();
            using (var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileInfo(@"tests\Data\Generic-VersionTwoPointOne.xml")))
            using (Stream writer = File.Create(outfile))
            using (var dataWriter = new DataWriterManager().GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21)), writer))
            {
                var dataReaderEngine = new GenericDataReaderEngine(sourceData, null, dsd);
                dataReaderEngine.MoveNextDataset();
                var dataReaderWriterTransform = CreateSUT();
                dataReaderWriterTransform.CopyToWriter(dataReaderEngine, dataWriter, "FREQ", true);
            }
            var xDocument = XDocument.Load(outfile);
            var xElement = xDocument.Descendants().First(x => x.Name.LocalName == "SeriesKey");
            var seriesKey = xElement.Descendants();
            Assert.That(seriesKey.ElementAt(0).FirstAttribute.Value, Is.EqualTo("ADJUSTMENT"));
            Assert.That(seriesKey.ElementAt(0).LastAttribute.Value, Is.EqualTo("N"));
            Assert.That(seriesKey.ElementAt(1).FirstAttribute.Value, Is.EqualTo("STS_ACTIVITY"));
            Assert.That(seriesKey.ElementAt(1).LastAttribute.Value, Is.EqualTo("A"));
            Assert.That(seriesKey.ElementAt(2).FirstAttribute.Value, Is.EqualTo("TIME_PERIOD"));
            Assert.That(seriesKey.ElementAt(2).LastAttribute.Value, Is.EqualTo("2005-Q1"));

            var xElement2 = xDocument.Descendants().First(x => x.Name.LocalName == "Obs");
            var obs = xElement2.Descendants();

            Assert.That(obs.ElementAt(0).FirstAttribute.Value, Is.EqualTo("TIME_PERIOD"));
            Assert.That(obs.ElementAt(0).LastAttribute.Value, Is.EqualTo("Q"));
            Assert.That(obs.ElementAt(1).FirstAttribute.Value, Is.EqualTo("0"));

            File.Delete(outfile);
        }
    }
}