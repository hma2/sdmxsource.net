﻿// -----------------------------------------------------------------------
// <copyright file="DataStructureSuperObjectBuilderTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using FluentAssertions;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;

namespace SdmxStructureParserTests
{
    [TestFixture]
    class DataStructureSuperObjectBuilderTests
    {
        public DataStructureSuperObjectBuilder CreateSUT()
        {
            return new DataStructureSuperObjectBuilder();
        }


        public class When_building_a_DataStructureSuperObject : DataStructureSuperObjectBuilderTests
        {
            private ISdmxObjectRetrievalManager retrievalManager;

            [SetUp]
            public void SetUp()
            {
                retrievalManager = new InMemoryRetrievalManager(Utils.Structures);
            }

            [Test]
            public void Should_create_a_instance_of_DataStructureSuperObject()
            {
                var sut = CreateSUT();
                var dataflowSuperObject = sut.Build(Utils.DataStructure, retrievalManager, null);
                dataflowSuperObject.Should().BeOfType<DataStructureSuperObject>();
            }
        }
    }
}