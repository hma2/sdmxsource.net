﻿// -----------------------------------------------------------------------
// <copyright file="TestComponentRoles.cs" company="EUROSTAT">
//   Date Created : 2016-06-10
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParserTests.
// 
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxStructureParserTests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Util.ResourceBundle;

    /// <summary>
    /// The test component roles.
    /// </summary>
    [TestFixture(StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)]
    [TestFixture(StructureOutputFormatEnumType.SdmxV2RegistrySubmitDocument, IgnoreReason = "Output format not implemented yet.")]
    [TestFixture(StructureOutputFormatEnumType.SdmxV2StructureDocument)]
    public class TestComponentRoles
    {
        /// <summary>
        /// The _component xml attribute builder.
        /// </summary>
        private readonly IBuilder<ComponentRole, IStructureReference> _componentXmlAttributeBuilder = new Sdmxv2ConceptRoleBuilder();

        /// <summary>
        /// The _concept role builder.
        /// </summary>
        private readonly IBuilder<IStructureReference, ComponentRole> _conceptRoleBuilder = new Sdmxv2ConceptRoleBuilder();

        /// <summary>
        /// The _structure writer.
        /// </summary>
        private readonly IStructureWriterManager _structureWriter = new StructureWriterManager();

        /// <summary>
        /// The _structure output format
        /// </summary>
        private readonly StructureOutputFormat _structureOutputFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestComponentRoles" /> class.
        /// </summary>
        /// <param name="structureOutputFormatEnumType">Type of the structure output format enum.</param>
        public TestComponentRoles(StructureOutputFormatEnumType structureOutputFormatEnumType)
        {
            SdmxException.SetMessageResolver(new MessageDecoder());
            this._structureOutputFormat = StructureOutputFormat.GetFromEnum(structureOutputFormatEnumType);
        }

        /// <summary>
        /// Test writing dimension component roles.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        [TestCase(ComponentRole.Count)]
        [TestCase(ComponentRole.Entity)]
        [TestCase(ComponentRole.Identity)]
        [TestCase(ComponentRole.NonObservationalTime)]
        public void TestWritingDimensionComponentRoles(ComponentRole role)
        {
            IDimensionMutableObject dimension = BuildDimension();
            dimension.ConceptRole.Add(this._conceptRoleBuilder.Build(role));
            var dsd = BuildDataStructureObject(dimension);
            var outFile = new FileInfo("TestWritingDimensionComponentRoles" + role + "-" + this._structureOutputFormat.ToEnumType() + ".xml");
            using (var stream = outFile.Create())
            {
                this._structureWriter.WriteStructure(dsd, new HeaderImpl("IDREF001", "ZZ9"), new SdmxStructureFormat(this._structureOutputFormat), stream);
                stream.Flush();
            }

            Func<ComponentRole, string> getXmlAttribute = componentRole => componentRole.AsDimensionAttribute();
            CheckIfXmlAttributeExistsInXml(role, outFile, "Dimension", getXmlAttribute);
        }

        /// <summary>
        /// Test writing dimension component roles.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        [TestCase(ComponentRole.Count)]
        [TestCase(ComponentRole.Entity)]
        [TestCase(ComponentRole.Identity)]
        [TestCase(ComponentRole.NonObservationalTime)]
        public void TestWritingAttributeComponentRoles(ComponentRole role)
        {
            IDimensionMutableObject dimension = BuildDimension();
            IAttributeMutableObject attributeMutable = BuildAttributeMutable(dimension);
            attributeMutable.ConceptRoles.Add(this._conceptRoleBuilder.Build(role));
            var dsd = BuildDataStructureObject(dimension, attributeMutable);
            var outFile = new FileInfo("TestWritingAttributeComponentRoles" + role + "-" + this._structureOutputFormat.ToEnumType() + ".xml");
            using (var stream = outFile.Create())
            {
                this._structureWriter.WriteStructure(dsd, new HeaderImpl("IDREF001", "ZZ9"), new SdmxStructureFormat(this._structureOutputFormat), stream);
                stream.Flush();
            }

            Func<ComponentRole, string> getXmlAttribute = componentRole => componentRole.AsDataAttributeAttribute();
            CheckIfXmlAttributeExistsInXml(role, outFile, "Attribute", getXmlAttribute);
        }

        /// <summary>
        /// Tests the reading dimension component roles.
        /// </summary>
        /// <param name="role">The role.</param>
        [TestCase(ComponentRole.Count)]
        [TestCase(ComponentRole.Entity)]
        [TestCase(ComponentRole.Identity)]
        [TestCase(ComponentRole.NonObservationalTime)]
        public void TestReadingDimensionComponentRoles(ComponentRole role)
        {
            IDimensionMutableObject dimension = BuildDimension();
            dimension.ConceptRole.Add(this._conceptRoleBuilder.Build(role));
            var dsd = BuildDataStructureObject(dimension);
            var outFile = new FileInfo("TestReadingDimensionComponentRoles" + role + "-" + this._structureOutputFormat.ToEnumType() + ".xml");
            using (var stream = outFile.Create())
            {
                this._structureWriter.WriteStructure(dsd, new HeaderImpl("IDREF001", "ZZ9"), new SdmxStructureFormat(this._structureOutputFormat), stream);
                stream.Flush();
            }

            var sdmxObjects = outFile.GetSdmxObjects(new StructureParsingManager());
            var readDsd = sdmxObjects.GetDataStructures(dsd.AsReference).First();
            var crossReference = readDsd.GetDimension(dimension.Id).ConceptRole.First();
            var extractedRole = this._componentXmlAttributeBuilder.Build(crossReference);
            Assert.AreEqual(role, extractedRole);
        }

        /// <summary>
        /// Tests the reading dimension component roles.
        /// </summary>
        /// <param name="role">The role.</param>
        [TestCase(ComponentRole.Count)]
        [TestCase(ComponentRole.Entity)]
        [TestCase(ComponentRole.Identity)]
        [TestCase(ComponentRole.NonObservationalTime)]
        public void TestReadingAttributeComponentRoles(ComponentRole role)
        {
            IDimensionMutableObject dimension = BuildDimension();
            IAttributeMutableObject attributeMutable = BuildAttributeMutable(dimension);
            attributeMutable.ConceptRoles.Add(this._conceptRoleBuilder.Build(role));
            var dsd = BuildDataStructureObject(dimension, attributeMutable);
            var outFile = new FileInfo("TestReadingAttributeComponentRoles" + role + "-" + this._structureOutputFormat.ToEnumType() + ".xml");
            using (var stream = outFile.Create())
            {
                this._structureWriter.WriteStructure(dsd, new HeaderImpl("IDREF001", "ZZ9"), new SdmxStructureFormat(this._structureOutputFormat), stream);
                stream.Flush();
            }

            var sdmxObjects = outFile.GetSdmxObjects(new StructureParsingManager());
            var readDsd = sdmxObjects.GetDataStructures(dsd.AsReference).First();
            var crossReference = readDsd.GetAttribute(attributeMutable.Id).ConceptRoles.First();
            var extractedRole = this._componentXmlAttributeBuilder.Build(crossReference);
            Assert.AreEqual(role, extractedRole);
        }

        /// <summary>
        /// Builds the attribute.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <returns>The <see cref="IAttributeMutableObject"/></returns>
        private static AttributeMutableCore BuildAttributeMutable(IDimensionMutableObject dimension)
        {
            return new AttributeMutableCore()
                       {
                           Id = "TEST_ATTR",
                           AssignmentStatus = "Mandatory",
                           AttachmentLevel = AttributeAttachmentLevel.DimensionGroup,
                           DimensionReferences = { dimension.Id },
                           ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "TEST_ATTR"),
                           Representation = new RepresentationMutableCore() { Representation = new StructureReferenceImpl("TEST", "CL_TEST_ATTR", "1.0", SdmxStructureEnumType.CodeList) }
                       };
        }

        /// <summary>
        /// Builds the dimension.
        /// </summary>
        /// <returns>
        /// The <see cref="DimensionMutableCore"/>.
        /// </returns>
        private static DimensionMutableCore BuildDimension()
        {
            return new DimensionMutableCore()
                       {
                           Id = "TEST_DIM",
                           ConceptRef = new StructureReferenceImpl("TEST", "TEST_CS", "1.0", SdmxStructureEnumType.Concept, "TEST_DIM"),
                           Representation = new RepresentationMutableCore() { Representation = new StructureReferenceImpl("TEST", "CL_TEST_DIM", "1.0", SdmxStructureEnumType.CodeList) }
                       };
        }

        /// <summary>
        /// Checks if XML attribute exists in <paramref name="xmlFile"/>.
        /// </summary>
        /// <param name="role">
        /// The role.
        /// </param>
        /// <param name="xmlFile">
        /// The XML file.
        /// </param>
        /// <param name="tagName">
        /// Name of the tag.
        /// </param>
        /// <param name="getXmlAttribute">
        /// The get XML attribute.
        /// </param>
        private static void CheckIfXmlAttributeExistsInXml(ComponentRole role, FileInfo xmlFile, string tagName, Func<ComponentRole, string> getXmlAttribute)
        {
            using (var reader = XmlReader.Create(xmlFile.OpenText()))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.LocalName.Equals(tagName))
                            {
                                var val = reader.GetAttribute(getXmlAttribute(role));
                                Assert.AreEqual("true", val);
                                return;
                            }

                            break;
                    }
                }
            }

            Assert.Fail("Role {0} not found at a tag {1}", role, tagName);
        }

        /// <summary>
        /// Builds the data structure object.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <param name="attributes">The attributes.</param>
        /// <returns>
        /// The <see cref="IDataStructureObject" />
        /// </returns>
        private static IDataStructureObject BuildDataStructureObject(IDimensionMutableObject dimension, params IAttributeMutableObject[] attributes)
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore() { Id = "TEST_DSD", AgencyId = "TEST", Version = "1.0" };
            dsd.AddName("en", "TEST_DSD");
            dsd.AddPrimaryMeasure(new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));
            dsd.AddDimension(dimension);
            if (attributes != null)
            {
                dsd.AttributeList = new AttributeListMutableCore();
                dsd.Attributes.AddAll(attributes);
            }

            return dsd.ImmutableInstance;
        }
    }
}