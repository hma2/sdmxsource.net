﻿// -----------------------------------------------------------------------
// <copyright file="TestJsonStructureWritingMessage.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureParserTests
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Unit Test for Json Structure message.
    /// </summary>
    [TestFixture]
    public class TestJsonStructureMessage
    {

        [Test]
        public void TestWriteDataflow_JsonFormat()
        {
            ISdmxObjects objects = new SdmxObjectsImpl(new HeaderImpl("Test_case_NSI_WS", "OECD"));

            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("it"),
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                new List<CultureInfo>()
                {
                    CultureInfo.GetCultureInfo("fr"),
                    CultureInfo.GetCultureInfo("en")
                },
                CultureInfo.GetCultureInfo("en"));

            SdmxStructureJsonFormat sdmxStructureFormat = new SdmxStructureJsonFormat(SdmxStructureEnumType.Dataflow, System.Text.Encoding.UTF8, preferedLanguageTranslator);
            
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.AgencyId = "OECD";
            dataflow.Id = "DKI_DPS";
            dataflow.AddName("en", "Truly the best dataflow");
            dataflow.AddDescription("en", "What a beautiful description!");
            dataflow.DataStructureRef = new StructureReferenceImpl("OECD", "TEST_DSD", "1.0", SdmxStructureEnumType.Dsd);
            /*
                var categoryRefType = new CategoryRefType();
                    builtObj.CategoryRef.Add(categoryRefType);
            */


            IAnnotationMutableObject dfannotation = new AnnotationMutableCore();
            dfannotation.Type = "OECD_OPEN_READY_FLAG";
            dfannotation.Title = "Status of the open ready flag";
            dfannotation.AddText("en", "true");
            dataflow.Annotations.Add(dfannotation);

            objects.AddDataflow(dataflow.ImmutableInstance);

            IAgencySchemeMutableObject asmc = new AgencySchemeMutableCore();
            asmc.AgencyId = "OECD";//NB: it is replaced by "AGENCIES"
            asmc.Id = "The OECD"; 
            asmc.AddName("en", "Organisation for Economic Co - operation and Development");

            asmc.CreateItem("DKI", "Digital Knowledge and Information");

            objects.AddAgencyScheme(asmc.ImmutableInstance);

            ICategorySchemeMutableObject csmc = new CategorySchemeMutableCore();
            csmc.AgencyId = "DKI";
            csmc.Id = "GenStats";
            csmc.AddName("en", "General Statistics");

            ICategoryMutableObject cat = csmc.CreateItem("CouStaPro", "Country statistical profiles");

            objects.AddCategoryScheme(csmc.ImmutableInstance);
            
            ICategorisationMutableObject cm = new CategorisationMutableCore();
            cm.AgencyId = asmc.AgencyId;

            ICategoryObject Cat = csmc.ImmutableInstance.GetCategory(cat.Id);

            cm.CategoryReference = new StructureReferenceImpl(Cat.Urn);
            cm.StructureReference = new StructureReferenceImpl(dataflow.ImmutableInstance.Urn);
            cm.Id = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", cm.CategoryReference.GetHashCode(), cm.StructureReference.GetHashCode());
            string categorisationName = string.Format(
                    CultureInfo.InvariantCulture,
                    "Categorisation between: {0} and {1} {2}",
                    cm.CategoryReference,
                    cm.StructureReference.TargetReference.StructureType,
                    cm.StructureReference.MaintainableReference);

            cm.AddName("en", categorisationName);

            objects.AddCategorisation(cm.ImmutableInstance);

            if (!Directory.Exists("tests/Json"))
                Directory.CreateDirectory("tests/Json");

            var outputFileName = string.Format(CultureInfo.InvariantCulture, "{0}-output.json", "tests/Json/TestWriteDataflow_JsonFormat");
            if (File.Exists(outputFileName))
                File.Delete(outputFileName);

            using (FileStream stream = File.Create(outputFileName))
            {
                IStructureWriterManager structureWritingManager = new StructureWriterManager(new SdmxJsonStructureWriterFactory());
                structureWritingManager.WriteStructures(objects, sdmxStructureFormat, stream);
            }

            SdmxStructureJsonFormat sdmxStructureJsonFormat = new SdmxStructureJsonFormat(SdmxStructureEnumType.Dataflow, System.Text.Encoding.UTF8, new PreferedLanguageTranslator(null, null, CultureInfo.GetCultureInfo("en")));
            IStructureParsingManager parsingManager = new StructureParsingJsonManager(sdmxStructureJsonFormat);
            ISdmxObjects CompareObjects = new SdmxObjectsImpl();

            using (IReadableDataLocation location = new FileReadableDataLocation(outputFileName))
            {
                CompareObjects.Merge(parsingManager.ParseStructures(location).GetStructureObjects(false));
            }

            CollectionAssert.IsNotEmpty(CompareObjects.Dataflows);
            CollectionAssert.IsNotEmpty(CompareObjects.AgenciesSchemes);
            CollectionAssert.IsNotEmpty(CompareObjects.CategorySchemes);
            CollectionAssert.IsNotEmpty(CompareObjects.Categorisations);

            Assert.AreEqual(CompareObjects.Dataflows.Count, objects.Dataflows.Count);
            Assert.AreEqual(CompareObjects.AgenciesSchemes.Count, objects.AgenciesSchemes.Count);
            Assert.AreEqual(CompareObjects.CategorySchemes.Count, objects.CategorySchemes.Count);
            Assert.AreEqual(CompareObjects.Categorisations.Count, objects.Categorisations.Count);

            IDataflowObject compareDataflow = null;

            foreach (IDataflowObject df in CompareObjects.Dataflows)
                if (df.Id.Equals(dataflow.Id))
                    compareDataflow = df;

            if (compareDataflow != null)
                Assert.IsTrue(dataflow.ImmutableInstance.DeepEquals(compareDataflow, true), compareDataflow.AsReference.MaintainableReference.ToString());

            IAgencyScheme compareAgengyScheme = CompareObjects.GetAgenciesScheme("OECD");
            Assert.IsTrue(asmc.ImmutableInstance.DeepEquals(compareAgengyScheme, true), compareAgengyScheme.AsReference.MaintainableReference.ToString());

            ICategorySchemeObject compareCategoryScheme = null;

            foreach (ICategorySchemeObject cs in CompareObjects.CategorySchemes)
                if (cs.Id.Equals(csmc.Id))
                    compareCategoryScheme = cs;

            if (compareCategoryScheme != null)
                Assert.IsTrue(csmc.ImmutableInstance.DeepEquals(compareCategoryScheme, true), compareCategoryScheme.AsReference.MaintainableReference.ToString());

            ICategorisationObject compareCategorisation = null;

            foreach (ICategorisationObject ct in CompareObjects.Categorisations)
                if (ct.Id.Equals(cm.Id))
                    compareCategorisation = ct;

            if (compareCategorisation != null)
                Assert.IsTrue(cm.ImmutableInstance.DeepEquals(compareCategorisation, true), compareCategorisation.AsReference.MaintainableReference.ToString());


        }
    }
}