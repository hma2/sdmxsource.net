﻿// -----------------------------------------------------------------------
// <copyright file="Utils.cs" company="EUROSTAT">
//   Date Created : 2016-02-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;

namespace SdmxStructureParserTests
{
    public class Utils
    {
        public static ICodelistObject Codelist
        {
            get { return GetStructures("Structures.xml").Codelists.FirstOrDefault(); }
        }

        public static IHierarchicalCodelistObject HierarchicalCodelist
        {
            get { return GetStructures("Structures.xml").HierarchicalCodelists.FirstOrDefault(); }
        }

        public static List<ICodelistObject> CodelistsForHierarchical
        {
            get { return GetStructures("CodelistsForHierarchical.xml").Codelists.ToList(); }
        }

        public static IDataStructureObject DataStructure
        {
            get { return GetStructures("Structures.xml").DataStructures.FirstOrDefault(); }
        }


        public static IConceptSchemeObject ConceptScheme
        {
            get { return GetStructures("Structures.xml").ConceptSchemes.FirstOrDefault(); }
        }

        public static IDataflowObject DataFlow
        {
            get { return GetStructures("Structures.xml").Dataflows.FirstOrDefault(); }
        }

        public static IGroup Group
        {
            get { return GetStructures("Structures.xml").DataStructures.First().Groups.FirstOrDefault(); }
        }

        public static ICategorisationObject Categorisation
        {
            get { return GetStructures("Structures.xml").Categorisations.FirstOrDefault(); }
        }

        public static ICategoryObject Category
        {
            get { return GetStructures("Structures.xml").CategorySchemes.FirstOrDefault().Items.FirstOrDefault(); }
        }

        public static ICategorySchemeObject CategoryScheme
        {
            get { return GetStructures("Structures.xml").CategorySchemes.FirstOrDefault(); }
        }

        public static IProvisionAgreementObject ProvisionAgreement
        {
            get { return GetStructures("ProvisionAgreement.xml").ProvisionAgreements.FirstOrDefault(); }
        }

        public static IDataProvider DataProvider
        {
            get { return GetStructures("Structures.xml").DataProviderSchemes.First().Items.FirstOrDefault(); }
        }

        public static ISdmxObjects Structures
        {
            get { return GetStructures("Structures.xml"); }
        }

        public static IRegistrationObject Registration
        {
            get { return GetStructures("Registration.xml").Registrations.FirstOrDefault(); }
        }

        public static IProcessObject Process
        {
            get { return GetStructures("Process.xml").Processes.FirstOrDefault(); }
        }

        public static List<IProcessStepObject> ProcessSteps
        {
            get { return GetStructures("Process.xml").Processes.FirstOrDefault().ProcessSteps.ToList(); }
        }

        public static ISdmxObjects StructuresForProvisionAgreement
        {
            get
            {
                return GetStructures("ProvisionAgreement.xml");
            }
        }

        private static ISdmxObjects GetStructures(string fileName)
        {
            IStructureParsingManager manager = new StructureParsingManager();
            using (
                var readable = new FileReadableDataLocation("../../StructureReferences/" + fileName)
                )
            {
                var structureWorkspace = manager.ParseStructures(readable);
                return structureWorkspace.GetStructureObjects(true);
            }
        }
    }
}