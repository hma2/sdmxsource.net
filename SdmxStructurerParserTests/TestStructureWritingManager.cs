﻿// -----------------------------------------------------------------------
// <copyright file="TestStructureWritingManager.cs" company="EUROSTAT">
//   Date Created : 2013-07-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParserTests.
//     SdmxStructureParserTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParserTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParserTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxStructureParserTests
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using Estat.Sri.SdmxStructureMutableParser.Factory;
    using Estat.Sri.SdmxStructureMutableParser.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.XmlHelper;

    /// <summary>
    /// Test unit for <see cref="StructureWriterManager"/>
    /// </summary>
    [TestFixture]
    public class TestStructureWritingManager
    {
        /// <summary>
        /// The _writer manager
        /// </summary>
        private IStructureWriterManager _writerManager;

        /// <summary>
        /// The _parsing manager
        /// </summary>
        private IStructureParsingManager _parsingManager;

        /// <summary>
        /// Setups this instance.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this._writerManager = new StructureWriterManager();
            this._parsingManager = new StructureParsingManager();
        }

        /// <summary>
        /// Tests the write coded time dimension V20.
        /// </summary>
        [Test]
        public void TestWriteCodedTimeDimensionV20()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.TimeDimension = true;
            dimension.Id = DimensionObject.TimeDimensionFixedId;
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD");
            var structureReference = new StructureReferenceImpl("TEST_AGENCY", "CL_TIME_PERIOD", "1.0", SdmxStructureEnumType.CodeList);
            dimension.Representation = new RepresentationMutableCore() { Representation = structureReference };
            var immutable = BuildDataStructureObject(dimension);
            using (var stream = new MemoryStream())
            {
                this._writerManager.WriteStructure(immutable, new HeaderImpl("TEST", "TEST"), new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument)), stream);

                stream.Position = 0;
                using (var location = new MemoryReadableLocation(stream.ToArray()))
                {
                    var workspace = _parsingManager.ParseStructures(location);
                    var dsd = workspace.GetStructureObjects(false).DataStructures.First();
                    var timeDimension = dsd.TimeDimension;
                    Assert.IsTrue(timeDimension.HasCodedRepresentation());
                    Assert.AreEqual(timeDimension.Representation.Representation.AgencyId, structureReference.AgencyId);
                    Assert.AreEqual(timeDimension.Representation.Representation.MaintainableId, structureReference.MaintainableId);
                    Assert.AreEqual(timeDimension.Representation.Representation.Version, structureReference.Version);
                }
            }
        }

        [Test]
        public void TestWriteProvisionAgreement()
        {
            // load DSD and deps
            var sdmxObjects = new FileInfo(@"tests\v21\Structure\test-sdmxv2.1-ESTAT+STS+2.0.xml").GetSdmxObjects(this._parsingManager);

            // build PA
            IProvisionAgreementMutableObject provisionAgreement = new ProvisionAgreementMutableCore();
            provisionAgreement.Id = "TEST_PA";
            provisionAgreement.AgencyId = "TEST";
            provisionAgreement.Version = "1.0";
            provisionAgreement.AddName("en", "This is a test PA");
            provisionAgreement.DataproviderRef = new StructureReferenceImpl("TEST", DataProviderScheme.FixedId, DataProviderScheme.FixedVersion, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider), "DP_TEST");
            provisionAgreement.StructureUsage = new StructureReferenceImpl("TEST", "DATAFLOW_TEST", "1.2", SdmxStructureEnumType.Dataflow);

            sdmxObjects.AddProvisionAgreement(provisionAgreement.ImmutableInstance);

            // build DP
            IDataProviderSchemeMutableObject dataProviderScheme = new DataProviderSchemeMutableCore();
            dataProviderScheme.AgencyId = "TEST";
            dataProviderScheme.AddName("en", "Test name");

            IDataProviderMutableObject dataProvider = new DataProviderMutableCore();
            dataProvider.Id = "DP_TEST";
            dataProvider.AddName("en", "Name for test DP");
            dataProviderScheme.AddItem(dataProvider);

            sdmxObjects.AddDataProviderScheme(dataProviderScheme.ImmutableInstance);

            // build DF
            IDataflowMutableObject dataflow = new DataflowMutableCore();
            dataflow.Id = "DATAFLOW_TEST";
            dataflow.AgencyId = "TEST";
            dataflow.Version = "1.2";
            dataflow.AddName("en", "Name for test dataflow");

            dataflow.DataStructureRef = sdmxObjects.DataStructures.First().AsReference;

            sdmxObjects.AddDataflow(dataflow.ImmutableInstance);

            // write PA and deps
            using (var stream = File.Create("test-pa.xml"))
            {
                this._writerManager.WriteStructures(sdmxObjects, new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), stream);
            }

            ISdmxObjects loaded;
            using (var fileReadableDataLocation = new FileReadableDataLocation("test-pa.xml"))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(fileReadableDataLocation);
                loaded = structureWorkspace.GetStructureObjects(false);
            }

            Assert.IsNotEmpty(loaded.ProvisionAgreements);
            Assert.That(provisionAgreement.ImmutableInstance.DeepEquals(loaded.ProvisionAgreements.First(), false));
        }

        /// <summary>
        /// Tests the write of a organisation unit scheme with parent.
        /// </summary>
        [Test]
        public void TestWriteOrganisationUnitSchemeWithParent()
        {
            IOrganisationUnitSchemeMutableObject organisation = new OrganisationUnitSchemeMutableCore();
            organisation.Id = "TEST_ID";
            organisation.AgencyId = "TEST_AGENCY";
            organisation.AddName("en", "TEST name");
            var item = new OrganisationUnitMutableCore() {Id = "TEST_ORG1"};
            item.AddName("en", "Test ITEM 1");

            organisation.AddItem(item);

            var item2 = new OrganisationUnitMutableCore() {Id = "TEST_ORG2", ParentUnit = "TEST_ORG1"};
            item2.AddName("en", "TEST Unit 2");

            organisation.AddItem(item2);
            using (var stream = new MemoryStream())
            {
                this._writerManager.WriteStructure(organisation.ImmutableInstance, new HeaderImpl("TEST", "TEST"), new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), stream);

                stream.Position = 0;
                using (var location = new MemoryReadableLocation(stream.ToArray()))
                {
                    var workspace = _parsingManager.ParseStructures(location);
                    var structureObjects = workspace.GetStructureObjects(false);
                    Assert.That(structureObjects.OrganisationUnitSchemes, Is.Not.Empty);
                    Assert.That(structureObjects.OrganisationUnitSchemes.First().Id, Is.EqualTo(organisation.Id));
                    Assert.That(structureObjects.OrganisationUnitSchemes.First().Items, Is.Not.Empty);
                    Assert.That(structureObjects.OrganisationUnitSchemes.First().Items.Where(unit => unit.HasParentUnit), Is.Not.Empty);
                    Assert.That(structureObjects.OrganisationUnitSchemes.First().Items.Where(unit => !unit.HasParentUnit), Is.Not.Empty);
                }
            }
        }

        [Test]
        public void TestWritingRegistration()
        {

            // load PA and deps
            var sdmxObjects = new FileInfo(@"tests\v21\Structure\test-pa.xml").GetSdmxObjects(this._parsingManager);
            IRegistrationMutableObject registration = new RegistrationMutableCore();
            registration.DataSource = new DataSourceMutableCore() { DataUrl = new Uri("http://webservice.org/ws/rest/data/DF_TEST"), RESTDatasource = true };
            registration.ProvisionAgreementRef = sdmxObjects.ProvisionAgreements.First().AsReference;
            registration.AgencyId = "NA";

            ISdmxObjects registrationContainer = new SdmxObjectsImpl();
            registrationContainer.AddRegistration(registration.ImmutableInstance);

            // write PA and deps
            using (var stream = File.Create("test-registration.xml"))
            {
                this._writerManager.WriteStructures(registrationContainer, new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), stream);
            }

            ISdmxObjects loaded;
            using (var fileReadableDataLocation = new FileReadableDataLocation("test-registration.xml"))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(fileReadableDataLocation);
                loaded = structureWorkspace.GetStructureObjects(false);
            }

            Assert.IsNotEmpty(loaded.Registrations);

        }


        [Test]
        public void TestWritingProcess()
        {
            IProcessMutableObject process = new ProcessMutableCore();
            process.Id = "TEST_PROCESS";
            process.AgencyId = "TEST";
            process.Version = "1.0";
            process.AddName("en", "Test name");
            
            var processStep = new ProcessStepMutableCore() {Id = "TEST_STEP1"};
            processStep.AddName("en", "The first step");
            
            InputOutputMutableCore inputOutput = new InputOutputMutableCore();
            inputOutput.LocalId = "TEST_IO";
            inputOutput.StructureReference = new StructureReferenceImpl("TEST", "DF_TEST", "1.2", SdmxStructureEnumType.Dataflow);
            processStep.Input.Add(inputOutput);
            var transitionMutableObject = new TransitionMutableCore() { Id = "T_ONE", TargetStep = "TEST_STEP3"};
            transitionMutableObject.Conditions.Add(new TextTypeWrapperMutableCore("en", "obs != null"));
            processStep.AddTransition(transitionMutableObject);
            var processStepNext = new ProcessStepMutableCore() {Id = "TEST_STEP2"};

            processStepNext.Computation = new ComputationMutableCore()
                                              {
                                                  LocalId = "TestComputation", 
                                                  SoftwareLanguage = "csharp", 
                                                  SoftwarePackage = "SDMX-RI", 
                                                  SoftwareVersion = "0.7.4"
                                              };
            processStepNext.AddName("en", "The second step");
            processStepNext.Computation.Descriptions.Add(new TextTypeWrapperMutableCore("en", "Disseminate data"));
            processStep.AddProcessStep(processStepNext);
            
            var processStep3 = new ProcessStepMutableCore() {Id = "TEST_STEP3"};
            processStep3.AddName("en", "Test step 3");
            processStep3.Computation = new ComputationMutableCore()
                                           {
                                               LocalId = "Test3", 
                                               SoftwarePackage = "TestPackage"
                                           };
            processStep3.Computation.Descriptions.Add(new TextTypeWrapperMutableCore("en", "Test Description"));
            process.AddProcessStep(processStep);
            process.AddProcessStep(processStep3);

            ISdmxObjects container = new SdmxObjectsImpl();
            container.AddProcess(process.ImmutableInstance);
            using (var stream = File.Create("test-process.xml"))
            {
                this._writerManager.WriteStructures(container, new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), stream);
            }

            ISdmxObjects loaded;
            using (var fileReadableDataLocation = new FileReadableDataLocation("test-process.xml"))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(fileReadableDataLocation);
                loaded = structureWorkspace.GetStructureObjects(false);
            }

            Assert.IsNotEmpty(loaded.Processes);
            Assert.IsTrue(process.ImmutableInstance.DeepEquals(loaded.Processes.First(), false));
        }

        /// <summary>
        /// Tests the write coded time dimension V21.
        /// </summary>
        [Test]
        public void TestWriteCodedTimeDimensionV21()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.TimeDimension = true;
            dimension.Id = DimensionObject.TimeDimensionFixedId;
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD");
            var structureReference = new StructureReferenceImpl("TEST_AGENCY", "CL_TIME_PERIOD", "1.0", SdmxStructureEnumType.CodeList);
            dimension.Representation = new RepresentationMutableCore() { Representation = structureReference };
            var immutable = BuildDataStructureObject(dimension);
            using (var stream = new MemoryStream())
            {
                this._writerManager.WriteStructure(immutable, new HeaderImpl("TEST", "TEST"), new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument)), stream);

                stream.Position = 0;
                using (var location = new MemoryReadableLocation(stream.ToArray()))
                {
                    var workspace = _parsingManager.ParseStructures(location);
                    var dsd = workspace.GetStructureObjects(false).DataStructures.First();

                    var timeDimension = dsd.TimeDimension;
                    Assert.IsFalse(timeDimension.HasCodedRepresentation());
                    Assert.IsTrue(ObjectUtil.ValidCollection(timeDimension.GetAnnotationsByTitle("CODED_TIME_DIMENSION")));
                }
            }
        }

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME_annotations.xml")]
        public void TestWriteStructures(string file)
        {
            ISdmxObjects objects;
            var fileInfo = new FileInfo(file);
            using (IReadableDataLocation location = new FileReadableDataLocation(fileInfo))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            StructureOutputFormat format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument);
            var sdmxStructureFormat = new SdmxStructureFormat(format);
            var outputFileName = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}-output.xml", fileInfo.Name);
            using (var stream = File.Create(outputFileName))
            {
                this._writerManager.WriteStructures(objects, sdmxStructureFormat, stream);    
            }

            using (var stream = File.OpenRead(outputFileName))
            {
                XMLParser.ValidateXml(stream, SdmxSchemaEnumType.VersionTwo);
            }
        }

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME_annotations.xml")]
        [TestCase("tests/v20/QUESTIONNAIRE_MSD_v0-correct.xml")]
        [TestCase("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml")]
        [TestCase("tests/v20/EGR_1_TS+ESTAT+1.4.xml")]
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        [TestCase("tests/v20/ESTAT+TESTLEVELS+1.0.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategories.xml")]
        [TestCase("tests/v20/QueryStructureRequest.xml")]
        [TestCase("tests/v20/QueryStructureRequestDataflowCodelist.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        [TestCase("tests/v20/SubmitStructureRequest.xml")]
        [TestCase("tests/v20/response.xml")]
        [TestCase("tests/v20/ESTAT+ESMS_MSD+2.2.xml")]
        public void TestWriteStructuresV2(string file)
        {
            ISdmxObjects objects;
            var fileInfo = new FileInfo(file);
            using (IReadableDataLocation location = new FileReadableDataLocation(fileInfo))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            StructureOutputFormat format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument);
            IStructureWriterManager structureWritingManager = new StructureWriterManager(new SdmxStructureWriterV2Factory(), new SdmxStructureWriterFactory());
            var outputFileName = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}-output.xml", fileInfo.Name);
            using (var stream = File.Create(outputFileName))
            {
                var settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    var sdmxStructureFormat = new SdmxXmlStructureV2Format(format, writer);
                    structureWritingManager.WriteStructures(objects, sdmxStructureFormat, null);
                    writer.Flush();
                }
            }

            using (var stream = File.OpenRead(outputFileName))
            {
                XMLParser.ValidateXml(stream, SdmxSchemaEnumType.VersionTwo);
            }
        }

        /// <summary>
        /// Builds the data structure object.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <returns>
        /// The <see cref="IDataStructureObject"/>
        /// </returns>
        private static IDataStructureObject BuildDataStructureObject(IDimensionMutableObject dimension)
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore() { Id = "TEST_DSD", AgencyId = "TEST", Version = "1.0" };
            dsd.AddName("en", "TEST_DSD");
            dsd.AddPrimaryMeasure(new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));
            dsd.AddDimension(dimension);

            var immutable = dsd.ImmutableInstance;
            return immutable;
        }

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME_annotations.xml")]
        [TestCase("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml")]
        [TestCase("tests/v20/EGR_1_TS+ESTAT+1.4.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        [TestCase("tests/v20/ESTAT+TESTLEVELS+1.0.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategoriesNoMDF.xml")]
        [TestCase("tests/v20/QueryStructureRequest.xml")]
        [TestCase("tests/v20/QueryStructureRequestDataflowCodelist.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        [TestCase("tests/v20/SubmitStructureRequest.xml")]
        [TestCase("tests/v20/response.xml")]
        public void TestWriteStructuresV2ObjectCount(string file)
        {
            ISdmxObjects objects;
            var fileInfo = new FileInfo(file);
            using (IReadableDataLocation location = new FileReadableDataLocation(fileInfo))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            StructureOutputFormat format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument);
            IStructureWriterManager structureWritingManager = new StructureWriterManager(new SdmxStructureWriterV2Factory(), new SdmxStructureWriterFactory());
            var outputFileName = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}-output.xml", fileInfo.Name);
            using (var stream = File.Create(outputFileName))
            {
                var settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    var sdmxStructureFormat = new SdmxXmlStructureFormat(format, writer);
                    structureWritingManager.WriteStructures(objects, sdmxStructureFormat, null);
                    writer.Flush();
                }
            }

            ISdmxObjects objects2;
            using (IReadableDataLocation location = new FileReadableDataLocation(outputFileName))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects2 = structureWorkspace.GetStructureObjects(false);
            }

            Assert.AreEqual(objects.GetAllMaintainables().Count, objects2.GetAllMaintainables().Count);
        }

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME_annotations.xml")]
        [TestCase("tests/v20/QUESTIONNAIRE_MSD_v0-correct.xml")]
        [TestCase("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml")]
        [TestCase("tests/v20/EGR_1_TS+ESTAT+1.4.xml")]
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        [TestCase("tests/v20/ESTAT+TESTLEVELS+1.0.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/QueryProvisioningRequest.xml")]
        [TestCase("tests/v20/QueryProvisioningResponse.xml")]
        [TestCase("tests/v20/QueryRegistrationRequest.xml")]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategories.xml")]
        [TestCase("tests/v20/QueryStructureRequest.xml")]
        [TestCase("tests/v20/QueryStructureRequestDataflowCodelist.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        [TestCase("tests/v20/SubmitProvisioningRequest.xml")]
        [TestCase("tests/v20/SubmitProvisioningResponse.xml")]
        [TestCase("tests/v20/SubmitRegistrationResponse.xml")]
        [TestCase("tests/v20/SubmitStructureRequest.xml")]
        [TestCase("tests/v20/SubmitStructureResponse.xml")]
        [TestCase("tests/v20/response.xml")]
        [TestCase("tests/v20/ESTAT+ESMS_MSD+2.2.xml")]
        public void TestWriteStructuresV2Default(string file)
        {
            ISdmxObjects objects;
            var fileInfo = new FileInfo(file);
            using (IReadableDataLocation location = new FileReadableDataLocation(fileInfo))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            StructureOutputFormat format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument);
            IStructureWriterManager structureWritingManager = new StructureWriterManager(new SdmxStructureWriterFactory());
            var outputFileName = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}-output.xml", fileInfo.Name);
            using (var stream = File.Create(outputFileName))
            {
                var settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    var sdmxStructureFormat = new SdmxXmlStructureFormat(format, writer);
                    structureWritingManager.WriteStructures(objects, sdmxStructureFormat, null);
                    writer.Flush();
                }
            }

            using (var stream = File.OpenRead(outputFileName))
            {
                XMLParser.ValidateXml(stream, SdmxSchemaEnumType.VersionTwo);
            }
        }

        /// <summary>
        /// Test unit for <see cref="StructureWriterManager.WriteStructures" />
        /// </summary>
        /// <param name="file">The file.</param>
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME_annotations.xml")]
        [TestCase("tests/v20/QUESTIONNAIRE_MSD_v0-correct.xml")]
        [TestCase("tests/v20/CENSAGR_CAPOAZ_GEN+IT1+1.3.xml")]
        [TestCase("tests/v20/EGR_1_TS+ESTAT+1.4.xml")]
        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        [TestCase("tests/v20/CENSUSHUB+ESTAT+1.1_alllevels.xml")]
        [TestCase("tests/v20/CL_SEX_v1.1.xml")]
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE+2.0.xml")]
        [TestCase("tests/v20/ESTAT+HCL_SAMPLE_NZ+2.1.xml")]
        [TestCase("tests/v20/ESTAT+SSTSCONS_PROD_M+2.0.xml")]
        [TestCase("tests/v20/ESTAT+STS+2.0.xml")]
        [TestCase("tests/v20/ESTAT+TESTLEVELS+1.0.xml")]
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml")]
        [TestCase("tests/v20/QueryRegistrationRequest.xml")]
        [TestCase("tests/v20/queryResponse-estat-sts.xml")]
        [TestCase("tests/v20/QueryResponseDataflowCategories.xml")]
        [TestCase("tests/v20/QueryStructureRequest.xml")]
        [TestCase("tests/v20/QueryStructureRequestDataflowCodelist.xml")]
        [TestCase("tests/v20/QueryStructureResponse.xml")]
        [TestCase("tests/v20/SubmitRegistrationResponse.xml")]
        [TestCase("tests/v20/SubmitStructureRequest.xml")]
        [TestCase("tests/v20/SubmitStructureResponse.xml")]
        [TestCase("tests/v20/response.xml")]
        [TestCase("tests/v20/ESTAT+ESMS_MSD+2.2.xml")]
        public void TestWriteStructuresV2DefaultObjectCount(string file)
        {
            ISdmxObjects objects;
            var fileInfo = new FileInfo(file);
            using (IReadableDataLocation location = new FileReadableDataLocation(fileInfo))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            StructureOutputFormat format = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2StructureDocument);
            IStructureWriterManager structureWritingManager = new StructureWriterManager(new SdmxStructureWriterFactory());
            var outputFileName = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}-output.xml", fileInfo.Name);
            using (var stream = File.Create(outputFileName))
            {
                var settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    var sdmxStructureFormat = new SdmxXmlStructureFormat(format, writer);
                    structureWritingManager.WriteStructures(objects, sdmxStructureFormat, null);
                    writer.Flush();
                }
            }

            ISdmxObjects objects2;
            using (IReadableDataLocation location = new FileReadableDataLocation(outputFileName))
            {
                var structureWorkspace = this._parsingManager.ParseStructures(location);
                objects2 = structureWorkspace.GetStructureObjects(false);
            }

            Assert.AreEqual(objects.GetAllMaintainables().Count, objects2.GetAllMaintainables().Count);
        }
    }
}