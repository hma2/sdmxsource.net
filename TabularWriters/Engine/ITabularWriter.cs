// -----------------------------------------------------------------------
// <copyright file="ITabularWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of TabularWriters.
//     TabularWriters is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     TabularWriters is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with TabularWriters.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.TabularWriters.Engine
{
    /// <summary>
    ///     The interface for tabular Data Writers, such as flat files like CSV or a database table.
    /// </summary>
    /// <example>
    ///     A sample in C# for <see cref="ITabularWriter" />
    ///     <code source="..\ReUsingExamples\DataWriting\ReUsingBinaryTabularWriter.cs" lang="cs" />
    /// </example>
    public interface ITabularWriter
    {
        /// <summary>
        ///     Gets the Total number of rows written
        /// </summary>
        long TotalRecordsWritten { get; }

        /// <summary>
        ///     Close this writer and commit the changes.
        /// </summary>
        void Close();

        /// <summary>
        ///     Start of the column definition. This could be the CSV header or a database table.
        /// </summary>
        void StartColumns();

        /// <summary>
        ///     Start of a record. This could be a new line in a CSV file or a new record in a database table.
        /// </summary>
        void StartRecord();

        /// <summary>
        ///     Write cell <paramref name="value" />.
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order <see cref="WriteCellAttributeValue" /> was
        ///     called
        /// </remarks>
        void WriteCellAttributeValue(string value);

        /// <summary>
        ///     Write cell <paramref name="value" />.
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order <see cref="WriteCellKeyValue" /> was called
        /// </remarks>
        void WriteCellKeyValue(string value);

        /// <summary>
        ///     Write cell <paramref name="value" />.
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order <see cref="WriteCellMeasureValue" /> was called
        /// </remarks>
        void WriteCellMeasureValue(string value);

        /// <summary>
        ///     Write the specified <paramref name="attribute" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="attribute">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="attribute" /> depends on the order <see cref="WriteColumnAttribute" /> was
        ///     called
        /// </remarks>
        void WriteColumnAttribute(string attribute);

        /// <summary>
        ///     Write the specified <paramref name="key" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="key">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="key" /> depends on the order <see cref="WriteColumnKey" /> was called
        /// </remarks>
        void WriteColumnKey(string key);

        /// <summary>
        ///     Write the specified <paramref name="measure" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="measure">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="measure" /> depends on the order <see cref="WriteColumnMeasure" /> was called
        /// </remarks>
        void WriteColumnMeasure(string measure);
    }
}