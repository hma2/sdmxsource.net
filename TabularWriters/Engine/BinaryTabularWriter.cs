// -----------------------------------------------------------------------
// <copyright file="BinaryTabularWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of TabularWriters.
//     TabularWriters is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     TabularWriters is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with TabularWriters.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.TabularWriters.Engine
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    ///     A fast binary tabular writer.
    /// </summary>
    /// <example>
    ///     A sample in C# for <see cref="BinaryTabularWriter" />
    ///     <code source="..\ReUsingExamples\DataWriting\ReUsingBinaryTabularWriter.cs" lang="cs" />
    /// </example>
    public class BinaryTabularWriter : ITabularWriter, IDisposable
    {
        /// <summary>
        ///     The column list
        /// </summary>
        private readonly List<string> _columns = new List<string>();

        /// <summary>
        ///     The data file
        /// </summary>
        private readonly BinaryWriter _dataFile;

        /// <summary>
        ///     The index of the <see cref="_dataFile" /> . It holds the start position of each 1024 record page.
        /// </summary>
        private readonly IList<long> _index;

        /// <summary>
        ///     A value indicating whether it is closed
        /// </summary>
        private bool _closed;

        /// <summary>
        ///     A value indicating whether a <see cref="StartRecord" /> has been called
        /// </summary>
        private bool _dataStarted;

        /// <summary>
        ///     The number of records written
        /// </summary>
        private long _totalRecordsWritten;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BinaryTabularWriter" /> class.
        /// </summary>
        /// <param name="dataFile">
        ///     The data file
        /// </param>
        /// <param name="index">
        ///     The <paramref name="dataFile" /> index. It holds the start position of each 1024 record page.
        /// </param>
        public BinaryTabularWriter(BinaryWriter dataFile, IList<long> index)
        {
            if (dataFile == null)
            {
                throw new ArgumentNullException("dataFile");
            }

            if (index == null)
            {
                throw new ArgumentNullException("index");
            }

            this._dataFile = dataFile;
            this._index = index;
        }

        /// <summary>
        ///     Gets the Total number of rows written
        /// </summary>
        public long TotalRecordsWritten
        {
            get
            {
                return this._totalRecordsWritten;
            }
        }

        /// <summary>
        ///     Close this writer and commit the changes.
        /// </summary>
        public void Close()
        {
            if (!this._closed)
            {
                this._closed = true;
                this._dataFile.Flush();
                this._index.Add(this._dataFile.BaseStream.Position);
                this._dataFile.Close();
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Start of the column definition. This could be the CSV header or a database table.
        /// </summary>
        public void StartColumns()
        {
            if (this._closed)
            {
                throw new InvalidOperationException("Writer was closed");
            }

            if (this._dataStarted)
            {
                throw new InvalidOperationException("StartRecord was called");
            }
        }

        /// <summary>
        ///     Start of a record. This could be a new line in a CSV file or a new record in a database table.
        /// </summary>
        public void StartRecord()
        {
            if (this._closed)
            {
                throw new InvalidOperationException("Writer was closed");
            }

            if (this._totalRecordsWritten % 1024 == 0)
            {
                this._index.Add(this._dataFile.BaseStream.Position);
                this._dataStarted = true;
            }

            this._totalRecordsWritten++;
        }

        /// <summary>
        ///     Write cell <paramref name="value" /> .
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteCellAttributeValue(System.String)" />
        ///     was called
        /// </remarks>
        public void WriteCellAttributeValue(string value)
        {
            this.WriteCell(value);
        }

        /// <summary>
        ///     Write cell <paramref name="value" /> .
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteCellKeyValue(System.String)" />
        ///     was called
        /// </remarks>
        public void WriteCellKeyValue(string value)
        {
            this.WriteCell(value);
        }

        /// <summary>
        ///     Write cell <paramref name="value" /> .
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteCellMeasureValue(System.String)" />
        ///     was called
        /// </remarks>
        public void WriteCellMeasureValue(string value)
        {
            this.WriteCell(value);
        }

        /// <summary>
        ///     Write the specified <paramref name="attribute" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="attribute">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="attribute" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteColumnAttribute(System.String)" />
        ///     was called
        /// </remarks>
        public void WriteColumnAttribute(string attribute)
        {
            this.WriteColumn(attribute);
        }

        /// <summary>
        ///     Write the specified <paramref name="key" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="key">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="key" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteColumnKey(System.String)" />
        ///     was called
        /// </remarks>
        public void WriteColumnKey(string key)
        {
            this.WriteColumn(key);
        }

        /// <summary>
        ///     Write the specified <paramref name="measure" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="measure">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="measure" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteColumnMeasure(System.String)" />
        ///     was called
        /// </remarks>
        public void WriteColumnMeasure(string measure)
        {
            this.WriteColumn(measure);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources and
        ///     optionally managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     A value indicating whether to dispose managed resources
        /// </param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Close();
            }
        }

        /// <summary>
        ///     Write cell <paramref name="value" /> .
        /// </summary>
        /// <param name="value">
        ///     The cell value
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="value" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteCellAttributeValue(System.String)" />
        ///     was called
        /// </remarks>
        private void WriteCell(string value)
        {
            this._dataFile.Write(value);
        }

        /// <summary>
        ///     Write the specified <paramref name="key" /> column. This could be the CSV header value or a database field.
        /// </summary>
        /// <param name="key">
        ///     The column name. i.e. the component name
        /// </param>
        /// <remarks>
        ///     The ordinal of each <paramref name="key" /> depends on the order
        ///     <see cref="M:Estat.Sri.TabularWriters.Engine.ITabularWriter.WriteColumnKey(System.String)" />
        ///     was called
        /// </remarks>
        private void WriteColumn(string key)
        {
            this._columns.Add(key);
            this._dataFile.Write(key);
        }
    }
}