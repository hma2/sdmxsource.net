// -----------------------------------------------------------------------
// <copyright file="StructureOutputFormat.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     The structure output format enumeration type.
    /// </summary>
    public enum StructureOutputFormatEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     The sdmx v 1 structure document.
        /// </summary>
        SdmxV1StructureDocument, 

        /// <summary>
        ///     The sdmx v 2 structure document.
        /// </summary>
        SdmxV2StructureDocument, 

        /// <summary>
        ///     The sdmx v 2 registry submit document.
        /// </summary>
        SdmxV2RegistrySubmitDocument, 

        /// <summary>
        ///     The sdmx v 2 registry query response document.
        /// </summary>
        SdmxV2RegistryQueryResponseDocument, 

        /// <summary>
        ///     The sdmx v 21 structure document.
        /// </summary>
        SdmxV21StructureDocument, 

        /// <summary>
        ///     The sdmx v 21 registry submit document.
        /// </summary>
        SdmxV21RegistrySubmitDocument, 

        /// <summary>
        ///     The sdmx v 21 query response document.
        /// </summary>
        SdmxV21QueryResponseDocument, 

        /// <summary>
        ///     The edi.
        /// </summary>
        Edi, 

        /// <summary>
        ///     The CSV.
        /// </summary>
        Csv, 

        /// <summary>
        ///     The XLSX
        /// </summary>
        Xlsx,

        /// <summary>
        ///     The Json
        /// </summary>
        Json
    }

    /// <summary>
    ///     The structure output format.
    /// </summary>
    public class StructureOutputFormat : BaseConstantType<StructureOutputFormatEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<StructureOutputFormatEnumType, StructureOutputFormat> Instances =
            new Dictionary<StructureOutputFormatEnumType, StructureOutputFormat>
                {
                    {
                        StructureOutputFormatEnumType
                        .SdmxV1StructureDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV1StructureDocument, 
                        SdmxSchemaEnumType.VersionOne, 
                        false, 
                        false)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .SdmxV2StructureDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV2StructureDocument, 
                        SdmxSchemaEnumType.VersionTwo, 
                        false, 
                        false)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .SdmxV2RegistrySubmitDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV2RegistrySubmitDocument, 
                        SdmxSchemaEnumType.VersionTwo, 
                        false, 
                        true)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .SdmxV2RegistryQueryResponseDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV2RegistryQueryResponseDocument, 
                        SdmxSchemaEnumType.VersionTwo, 
                        true, 
                        true)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .SdmxV21StructureDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV21StructureDocument, 
                        SdmxSchemaEnumType.VersionTwoPointOne, 
                        false, 
                        false)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .SdmxV21RegistrySubmitDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV21RegistrySubmitDocument, 
                        SdmxSchemaEnumType.VersionTwoPointOne, 
                        false, 
                        true)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .SdmxV21QueryResponseDocument, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .SdmxV21QueryResponseDocument, 
                        SdmxSchemaEnumType.VersionTwoPointOne, 
                        true, 
                        false)
                    }, 
                    {
                        StructureOutputFormatEnumType.Edi, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .Edi, 
                        SdmxSchemaEnumType.Edi, 
                        false, 
                        false)
                    }, 
                    {
                        StructureOutputFormatEnumType.Csv, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .Csv, 
                        SdmxSchemaEnumType.Csv, 
                        false, 
                        false)
                    }, 
                    {
                        StructureOutputFormatEnumType
                        .Xlsx, 
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .Xlsx, 
                        SdmxSchemaEnumType.Xlsx, 
                        false, 
                        false)
                    },
                    {
                        StructureOutputFormatEnumType
                        .Json,
                        new StructureOutputFormat(
                        StructureOutputFormatEnumType
                        .Json,
                        SdmxSchemaEnumType.Json,
                        false,
                        false)
                    }
                };

        /// <summary>
        ///     The _is query response.
        /// </summary>
        private readonly bool _isQueryResponse;

        /// <summary>
        ///     The _is registry document.
        /// </summary>
        private readonly bool _isRegistryDocument;

        /// <summary>
        ///     The _output version.
        /// </summary>
        private readonly SdmxSchemaEnumType _outputVersion;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureOutputFormat" /> class.
        /// </summary>
        /// <param name="enumType">
        ///     The enumeration type.
        /// </param>
        /// <param name="version">
        ///     The version.
        /// </param>
        /// <param name="isQueryResponse">
        ///     The is query response.
        /// </param>
        /// <param name="isRegistryDocument">
        ///     The is registry document.
        /// </param>
        private StructureOutputFormat(
            StructureOutputFormatEnumType enumType, 
            SdmxSchemaEnumType version, 
            bool isQueryResponse, 
            bool isRegistryDocument)
            : base(enumType)
        {
            this._outputVersion = version;
            this._isQueryResponse = isQueryResponse;
            this._isRegistryDocument = isRegistryDocument;
        }

        /// <summary>
        ///     Gets all instances
        /// </summary>
        public static IEnumerable<StructureOutputFormat> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is query response.
        /// </summary>
        public bool IsQueryResponse
        {
            get
            {
                return this._isQueryResponse;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is registry document.
        /// </summary>
        public bool IsRegistryDocument
        {
            get
            {
                return this._isRegistryDocument;
            }
        }

        /// <summary>
        ///     Gets the output version.
        /// </summary>
        public SdmxSchema OutputVersion
        {
            get
            {
                return SdmxSchema.GetFromEnum(this._outputVersion);
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="StructureOutputFormat" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="StructureOutputFormat" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static StructureOutputFormat GetFromEnum(StructureOutputFormatEnumType enumType)
        {
            StructureOutputFormat output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }
    }
}