// -----------------------------------------------------------------------
// <copyright file="StructureType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    // JAVADOC missing

    /// <summary>
    ///     The structure type.
    /// </summary>
    public enum StructureType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     The structure.
        /// </summary>
        Structure, 

        /// <summary>
        ///     The metadata report.
        /// </summary>
        MetadataReport, 

        /// <summary>
        ///     The registry interface.
        /// </summary>
        RegistryInterface, 

        /// <summary>
        ///     The query message.
        /// </summary>
        QueryMessage
    }

    /*
     * The only method is the ToString() and it matches the output of The enumeration. So no reason to do this.
    public class StructureType : BaseConstantType<StructureType>
    {
        public static readonly StructureType Structure = new StructureType("Structure");
        public static readonly StructureType MetadataReport = new StructureType("MetadataReport");
        public static readonly StructureType RegistryInterface = new StructureType("RegistryInterface");
        public static readonly StructureType QueryMessage = new StructureType("QueryMessage");

        public static IEnumerable<StructureType> Values
        {
            get
            {
                yield return Structure;
                yield return MetadataReport;
                yield return RegistryInterface;
                yield return QueryMessage;
            }
        }

        private string _nodeName;

        private StructureType(string nodeName)
        {
            this._nodeName = nodeName;
        }

        public override string ToString()
        {
            return this._nodeName;
        }
    }
     */
}