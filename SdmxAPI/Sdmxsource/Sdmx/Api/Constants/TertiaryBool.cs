// -----------------------------------------------------------------------
// <copyright file="TertiaryBool.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     An alternative to Boolean which supplements the true and false values with a third value of "UNSET".
    /// </summary>
    public enum TertiaryBoolEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     The true.
        /// </summary>
        True, 

        /// <summary>
        ///     The false.
        /// </summary>
        False, 

        /// <summary>
        ///     The unset.
        /// </summary>
        Unset
    }

    /// <summary>
    ///     An alternative to Boolean which supplements the true and false values with a third value of "UNSET".
    /// </summary>
    public class TertiaryBool : BaseConstantType<TertiaryBoolEnumType>
    {
        /// <summary>
        ///     The instances.
        /// </summary>
        private static readonly Dictionary<TertiaryBoolEnumType, TertiaryBool> Instances =
            new Dictionary<TertiaryBoolEnumType, TertiaryBool>
                {
                    {
                        TertiaryBoolEnumType.True, 
                        new TertiaryBool(TertiaryBoolEnumType.True, true)
                    }, 
                    {
                        TertiaryBoolEnumType.False, 
                        new TertiaryBool(TertiaryBoolEnumType.False, false)
                    }, 
                    {
                        TertiaryBoolEnumType.Unset, 
                        new TertiaryBool(TertiaryBoolEnumType.Unset, false)
                    }
                };

        /// <summary>
        ///     The _is true.
        /// </summary>
        private readonly bool _isTrue;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TertiaryBool" /> class.
        /// </summary>
        /// <param name="enumType">
        ///     The enumeration type.
        /// </param>
        /// <param name="isTrue">
        ///     The is true.
        /// </param>
        private TertiaryBool(TertiaryBoolEnumType enumType, bool isTrue)
            : base(enumType)
        {
            this._isTrue = isTrue;
        }

        /// <summary>
        ///     Gets all instances for this type
        /// </summary>
        public static IEnumerable<TertiaryBool> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is true.
        /// </summary>
        public bool IsTrue
        {
            get
            {
                return this._isTrue;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="TertiaryBool" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="TertiaryBool" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static TertiaryBool GetFromEnum(TertiaryBoolEnumType enumType)
        {
            TertiaryBool output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     The parse boolean.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="TertiaryBool" /> .
        /// </returns>
        public static TertiaryBool ParseBoolean(bool? value)
        {
            /* $$$   boolean null? */
            if (value == null)
            {
                return GetFromEnum(TertiaryBoolEnumType.Unset);
            }

            if (value == true)
            {
                return GetFromEnum(TertiaryBoolEnumType.True);
            }

            return GetFromEnum(TertiaryBoolEnumType.False);
        }

        /// <summary>
        ///     The is set.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public bool IsSet()
        {
            return this.EnumType != TertiaryBoolEnumType.Unset;
        }

        /// <summary>
        ///     The is unset.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public bool IsUnset()
        {
            return this.EnumType == TertiaryBoolEnumType.Unset;
        }
    }
}