﻿// -----------------------------------------------------------------------
// <copyright file="ComplexMaintainableQueryDetail.cs" company="EUROSTAT">
//   Date Created : 2013-05-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    #endregion

    /// <summary>
    ///     For a 2.1 REST data query, this enumeration contains a list of the parameters.
    ///     <p />
    ///     Detail of complex maintainable query; possible options are:
    ///     <ul>
    ///         <li>full - Return everything</li>
    ///         <li>stub - Return the stub</li>
    ///         <li>complete stub - Return the complete stub</li>
    ///     </ul>
    /// </summary>
    public enum ComplexMaintainableQueryDetailEnumType
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     Return everything
        /// </summary>
        Full, 

        /// <summary>
        ///     The stub
        /// </summary>
        Stub, 

        /// <summary>
        ///     The complete stub
        /// </summary>
        CompleteStub
    }

    /// <summary>
    ///     The complex maintainable query detail
    /// </summary>
    public class ComplexMaintainableQueryDetail : BaseConstantType<ComplexMaintainableQueryDetailEnumType>
    {
        /// <summary>
        ///     The _instances.
        /// </summary>
        private static readonly Dictionary<ComplexMaintainableQueryDetailEnumType, ComplexMaintainableQueryDetail> Instances = new Dictionary<ComplexMaintainableQueryDetailEnumType, ComplexMaintainableQueryDetail>
                            {
                                {
                                    ComplexMaintainableQueryDetailEnumType
                                    .Full, 
                                    new ComplexMaintainableQueryDetail(ComplexMaintainableQueryDetailEnumType.Full, "full")
                                }, 
                                {
                                    ComplexMaintainableQueryDetailEnumType.Stub, 
                                    new ComplexMaintainableQueryDetail(ComplexMaintainableQueryDetailEnumType.Stub, "stub")
                                }, 
                                {
                                    ComplexMaintainableQueryDetailEnumType
                                    .CompleteStub, 
                                    new ComplexMaintainableQueryDetail(ComplexMaintainableQueryDetailEnumType.CompleteStub, "completestub")
                                }
                            };

        /// <summary>
        ///     The _value.
        /// </summary>
        private readonly string _value;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexMaintainableQueryDetail" /> class.
        /// </summary>
        /// <param name="enumType">
        ///     The enumeration type
        /// </param>
        /// <param name="value">
        ///     The value
        /// </param>
        private ComplexMaintainableQueryDetail(ComplexMaintainableQueryDetailEnumType enumType, string value)
            : base(enumType)
        {
            this._value = value;
        }

        /// <summary>
        ///     Gets the instances of <see cref="ComplexMaintainableQueryDetail" />
        /// </summary>
        public static IEnumerable<ComplexMaintainableQueryDetail> Values
        {
            get
            {
                return Instances.Values;
            }
        }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this._value;
            }
        }

        /// <summary>
        ///     Gets the instance of <see cref="ComplexMaintainableQueryDetail" /> mapped to <paramref name="enumType" />
        /// </summary>
        /// <param name="enumType">
        ///     The <c>enum</c> type
        /// </param>
        /// <returns>
        ///     the instance of <see cref="ComplexMaintainableQueryDetail" /> mapped to <paramref name="enumType" />
        /// </returns>
        public static ComplexMaintainableQueryDetail GetFromEnum(ComplexMaintainableQueryDetailEnumType enumType)
        {
            ComplexMaintainableQueryDetail output;
            if (Instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns>
        ///     The <see cref="ComplexMaintainableQueryDetail" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Throws Validation Exception</exception>
        public static ComplexMaintainableQueryDetail ParseString(string value)
        {
            foreach (ComplexMaintainableQueryDetail currentQueryDetail in Values)
            {
                if (currentQueryDetail.Value.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    return currentQueryDetail;
                }
            }

            var sb = new StringBuilder();
            string concat = string.Empty;

            foreach (ComplexMaintainableQueryDetail currentQueryDetail in Values)
            {
                sb.Append(concat + currentQueryDetail.Value);
                concat = ", ";
            }

            throw new SdmxSemmanticException("Unknown Parameter " + value + " allowed parameters: " + sb);
        }
    }
}