// -----------------------------------------------------------------------
// <copyright file="TextFormat.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Constants
{
    /// <summary>
    ///     Contains the SDMX Text Formats
    /// </summary>
    public enum TextFormat
    {
        /// <summary>
        ///     Null value; Can be used to check if the value is not set;
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     The text type.
        /// </summary>
        TextType, 

        /// <summary>
        ///     The is sequence.
        /// </summary>
        IsSequence, 

        /// <summary>
        ///     The min length.
        /// </summary>
        MinLength, 

        /// <summary>
        ///     The max length.
        /// </summary>
        MaxLength, 

        /// <summary>
        ///     The start value.
        /// </summary>
        StartValue, 

        /// <summary>
        ///     The end value.
        /// </summary>
        EndValue, 

        /// <summary>
        ///     The interval.
        /// </summary>
        Interval, 

        /// <summary>
        ///     The time interval.
        /// </summary>
        TimeInterval, 

        /// <summary>
        ///     The decimals.
        /// </summary>
        Decimals, 

        /// <summary>
        ///     The pattern.
        /// </summary>
        Pattern
    }
}