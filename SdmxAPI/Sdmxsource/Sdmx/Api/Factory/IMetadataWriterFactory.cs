﻿// -----------------------------------------------------------------------
// <copyright file="IMetadataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Factory
{
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    ///     A MetadataWriterFactory operates as a plug-in to a Manager which can request a MetadataWriterEngine, to which the
    ///     implementation will
    ///     respond with an appropriate MetadataWriterEngine if it is able, otherwise it will return null
    /// </summary>
    public interface IMetadataWriterFactory
    {
        /// <summary>
        ///     Gets the metadata writer engine.
        /// </summary>
        /// <param name="outputFormat">The output format.</param>
        /// <returns> a MetadataWriterEngine if the output format is understood, if it is not known then NULL is returned</returns>
        IMetadataWriterEngine GetMetadataWriterEngine(IMetadataFormat outputFormat);
    }
}