// -----------------------------------------------------------------------
// <copyright file="ICategorisationRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;

    #endregion

    /// <summary>
    ///     Responsible for retrieving categorisations for particular structures
    /// </summary>
    public interface ICategorisationRetrievalManager
    {
        /// <summary>
        ///     Gets categorisations for a given identifiable
        /// </summary>
        /// <param name="identifiable">
        ///     Identifiable Object
        /// </param>
        /// <returns>
        ///     The <see cref="ISet{ICategorisationObject}" /> .
        /// </returns>
        ISet<ICategorisationObject> GetCategorisations(IIdentifiableObject identifiable);

        /// <summary>
        ///     Gets the categorisations for category.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns>all categorisations for the given category</returns>
        ISet<ICategorisationObject> GetCategorisationsForCategory(ICategoryObject category);

        /// <summary>
        ///     Gets the categorizations for category scheme.
        /// </summary>
        /// <param name="scheme">The scheme.</param>
        /// <returns>all categorizations for the given category scheme</returns>
        ISet<ICategorisationObject> GetCategorisationsForCategoryScheme(ICategorySchemeObject scheme);

        /// <summary>
        ///     Gets the categorisation base objects for a given identifiable
        /// </summary>
        /// <param name="identifiable">Identifiable Object </param>
        /// <returns>
        ///     The <see cref="ISet{ICategorisationSuperObject}" /> .
        /// </returns>
        ISet<ICategorisationSuperObject> GetCategorisationSuperObjectForCategory(IIdentifiableObject identifiable);

        /// <summary>
        ///     Gets the categorisation base objects for a given category, optionally a filter of allowable categorised
        ///     structure types can be returned
        /// </summary>
        /// <param name="category">
        ///     The category.
        /// </param>
        /// <param name="includeTypes">
        ///     The include Types.
        /// </param>
        /// <returns>
        ///     The <see cref="ISet{ICategorisationSuperObject}" /> .
        /// </returns>
        ISet<ICategorisationSuperObject> GetCategorisationSuperObjectForCategory(
            ICategoryObject category, 
            params SdmxStructureType[] includeTypes);
    }
}