// -----------------------------------------------------------------------
// <copyright file="ICrossReferenceMutableRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-02-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The CrossReferenceRetrievalMutableManager is used to retrieve mutable structures that cross reference or are cross
    ///     referenced by the
    ///     given structures.
    /// </summary>
    public interface ICrossReferenceMutableRetrievalManager
    {
        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">
        ///     - What Do I Reference?
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IStructureReference structureReference, 
            bool returnStub, 
            params SdmxStructureType[] structures);

        /// <summary>
        ///     Returns a list of MaintainableObject that are cross referenced by the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        ///     the identifiable bean to retrieve the references for - What Do I Reference?
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        IList<IMaintainableMutableObject> GetCrossReferencedStructures(
            IIdentifiableMutableObject identifiable, 
            bool returnStub, 
            params SdmxStructureType[] structures);

        /// <summary>
        ///     Returns a tree structure representing the <paramref name="maintainableObject" /> and all the structures that cross
        ///     reference it, and the structures that reference them, and so on.
        /// </summary>
        /// <param name="maintainableObject">
        ///     The maintainable object to build the tree for.
        /// </param>
        /// <returns>
        ///     The <see cref="ICrossReferencingTree" />.
        /// </returns>
        IMutableCrossReferencingTree GetCrossReferenceTree(IMaintainableMutableObject maintainableObject);

        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the structure(s) that match the reference parameter
        /// </summary>
        /// <param name="structureReference">
        ///     Who References Me?
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        IList<IMaintainableMutableObject> GetCrossReferencingStructures(
            IStructureReference structureReference, 
            bool returnStub, 
            params SdmxStructureType[] structures);

        /// <summary>
        ///     Returns a list of MaintainableObject that cross reference the given identifiable structure
        /// </summary>
        /// <param name="identifiable">
        ///     the identifiable bean to retrieve the references for - Who References Me?
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="structures">
        ///     an optional parameter to further filter the list by structure type
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IMaintainableMutableObject}" />.
        /// </returns>
        IList<IMaintainableMutableObject> GetCrossReferencingStructures(
            IIdentifiableMutableObject identifiable, 
            bool returnStub, 
            params SdmxStructureType[] structures);
    }
}