// -----------------------------------------------------------------------
// <copyright file="IMutableStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2013-02-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    ///     The Mutable Structure Search Manager interface.
    /// </summary>
    public interface IMutableStructureSearchManager
    {
        /// <summary>
        ///     Returns the latest version of the maintainable for the given maintainable input
        /// </summary>
        /// <param name="maintainableObject">
        ///     The maintainable Object.
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableMutableObject" />.
        /// </returns>
        IMaintainableMutableObject GetLatest(IMaintainableMutableObject maintainableObject);

        /// <summary>
        ///     Returns a set of maintainable that match the given query parameters
        /// </summary>
        /// <param name="structureQuery">
        ///     The structure Query.
        /// </param>
        /// <returns>
        ///     The <see cref="IMutableObjects" />.
        /// </returns>
        IMutableObjects GetMaintainables(IRestStructureQuery structureQuery);

        /// <summary>
        ///     Retrieves all structures that match the given query parameters in the list of query objects.  The list
        ///     must contain at least one StructureQueryObject.
        /// </summary>
        /// <param name="queries">
        ///     The queries.
        /// </param>
        /// <param name="resolveReferences">
        ///     - if set to true then any cross referenced structures will also be available in the SdmxObjects container
        /// </param>
        /// <param name="returnStub">
        ///     - if set to true then only stubs of the returned objects will be returned.
        /// </param>
        /// <returns>
        ///     The <see cref="IMutableObjects" />.
        /// </returns>
        IMutableObjects RetrieveStructures(IList<IStructureReference> queries, bool resolveReferences, bool returnStub);
    }
}