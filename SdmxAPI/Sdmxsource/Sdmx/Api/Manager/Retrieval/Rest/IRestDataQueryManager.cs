﻿// -----------------------------------------------------------------------
// <copyright file="IRestDataQueryManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest
{
    #region Using directives

    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    #endregion

    /// <summary>
    ///     Executes the data query, and writes the response to the output stream in the format specified
    /// </summary>
    public interface IRestDataQueryManager
    {
        /// <summary>
        ///     Execute the rest query, write the response out to the output stream based on the data format
        /// </summary>
        /// <param name="dataQuery">
        ///     The data query
        /// </param>
        /// <param name="dataFormat">
        ///     The data format
        /// </param>
        /// <param name="outPutStream">
        ///     The output stream
        /// </param>
        void ExecuteQuery(IRestDataQuery dataQuery, IDataFormat dataFormat, Stream outPutStream);
    }
}