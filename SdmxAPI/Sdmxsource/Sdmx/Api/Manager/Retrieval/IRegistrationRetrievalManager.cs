// -----------------------------------------------------------------------
// <copyright file="IRegistrationRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     Manages the retrieval of registrations by using simple structures that directly reference a registration
    /// </summary>
    public interface IRegistrationRetrievalManager
    {
        /// <summary>
        ///     Gets all the registrations.  Gets an empty set if no registrations exist.
        /// </summary>
        /// <value> </value>
        ISet<IRegistrationObject> AllRegistrations { get; }

        /// <summary>
        ///     Gets the registration that matches the maintainable reference.  Gets null if no registrations match the criteria.
        /// </summary>
        /// <param name="xref">The reference.</param>
        /// <returns>
        ///     The <see cref="IRegistrationObject" /> .
        /// </returns>
        /// <exception cref="System.ArgumentException">
        ///     if more then one match is found for the reference (this is only possible if the reference does not uniquely
        ///     identify a registration, this is only possible if the reference does not have all three parameters populated
        ///     (agencyId, id and version)
        /// </exception>
        IRegistrationObject GetRegistration(IMaintainableRefObject xref);

        /// <summary>
        ///     Gets all the registrations that match the maintainable reference.
        ///     Gets an empty set if no registrations exist that match the criteria.
        /// </summary>
        /// <param name="xref"> The reference object. </param>
        /// <returns> A set containing the registration objects.</returns>
        ISet<IRegistrationObject> GetRegistrations(IMaintainableRefObject xref);

        /// <summary>
        ///     Gets all the registrations against the provision references
        ///     <p />
        ///     The structure reference can either be referencing a Provision structure, a Data or MetadataFlow, or a DataProvider.
        /// </summary>
        /// <param name="structureRef">
        ///     The provision references.
        /// </param>
        /// <returns>  A set containing the registration objects.</returns>
        ISet<IRegistrationObject> GetRegistrations(IStructureReference structureRef);

        /// <summary>
        ///     Gets all the registrations against the provision
        /// </summary>
        /// <param name="provision">
        ///     - the provision to return the registration for
        /// </param>
        /// <returns>
        ///     A set containing the registration objects.
        /// </returns>
        ISet<IRegistrationObject> GetRegistrations(IProvisionAgreementObject provision);
    }
}