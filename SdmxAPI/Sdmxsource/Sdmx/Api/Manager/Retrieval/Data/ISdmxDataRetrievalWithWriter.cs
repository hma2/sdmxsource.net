﻿// -----------------------------------------------------------------------
// <copyright file="ISdmxDataRetrievalWithWriter.cs" company="EUROSTAT">
//   Date Created : 2013-04-03
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data
{
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    ///     The SdmxDataRetrievalWithWriter is capable of executing a DataQuery and writing the response to
    ///     the DataWriterEngine.
    ///     This SdmxDataRetrievalWithWriter does not need to concern itself with the response format -
    ///     as this is handled by the DataWriterEngine
    /// </summary>
    public interface ISdmxDataRetrievalWithWriter
    {
        /// <summary>
        ///     Queries for data conforming to the parameters defined by the DataQuery,
        ///     the response is written to the DataWriterEngine
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        void GetData(IDataQuery dataQuery, IDataWriterEngine dataWriter);
    }
}