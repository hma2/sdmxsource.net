﻿// -----------------------------------------------------------------------
// <copyright file="IAdvancedSdmxDataRetrievalWithWriter.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    #endregion

    /// <summary>
    ///     The AdvancedSdmxDataRetrievalWithWriter is capable of executing ComplexDataQuery and writing the
    ///     response to DataWriterEngine.
    ///     This AdvancedSdmxDataRetrievalWithWriter does not need to concern itself with the response format -
    ///     as this is handled by the DataWriterEngine
    /// </summary>
    public interface IAdvancedSdmxDataRetrievalWithWriter
    {
        /// <summary>
        ///     get data
        /// </summary>
        /// <param name="dataQuery">The data query</param>
        /// <param name="dataWriter">The data writer</param>
        void GetData(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter);
    }
}