﻿// -----------------------------------------------------------------------
// <copyright file="IDictionaryOfSets.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Util
{
    using System.Collections.Generic;

    /// <summary>
    ///     A <see cref="IDictionary{TKey,TValue}" /> between <typeparamref name="TKey" /> and a <see cref="ISet{T}" /> of
    ///     <typeparamref name="TSetValue" />
    /// </summary>
    /// <typeparam name="TKey"> The key type</typeparam>
    /// <typeparam name="TSetValue">
    ///     The type of the <see cref="ISet{T}" />
    /// </typeparam>
    public interface IDictionaryOfSets<TKey, TSetValue> : IDictionary<TKey, ISet<TSetValue>>
    {
        /// <summary>
        ///     Adds the specified value to the <paramref name="value" /> to the set corresponding to the specified
        ///     <paramref name="key" />
        /// </summary>
        /// <param name="key">
        ///     The key.
        /// </param>
        /// <param name="value">
        ///     The value.
        /// </param>
        void AddToSet(TKey key, TSetValue value);
    }
}