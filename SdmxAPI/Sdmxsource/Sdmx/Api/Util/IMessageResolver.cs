// -----------------------------------------------------------------------
// <copyright file="IMessageResolver.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Util
{
    #region Using directives

    using System.Globalization;

    #endregion

    /// <summary>
    ///     Used to resolve message codes to text strings based in the given locale
    /// </summary>
    public interface IMessageResolver
    {
        /// <summary>
        ///     Resolves the message against a resource which contains code-&gt;string mappings, inserting any args
        ///     where there are placeholders.  If there is no resource found, or no mapping could be found
        ///     for the messageCode, then the original messageCode will be returned.
        /// </summary>
        /// <param name="messageCode">
        ///     The message Code.
        /// </param>
        /// <param name="locale">
        ///     - the locale to resolve the message in
        /// </param>
        /// <param name="args">
        ///     The args.
        /// </param>
        /// <returns>
        ///     the resolved message, or the original if could not be resolved
        /// </returns>
        string ResolveMessage(string messageCode, CultureInfo locale, params object[] args);
    }
}