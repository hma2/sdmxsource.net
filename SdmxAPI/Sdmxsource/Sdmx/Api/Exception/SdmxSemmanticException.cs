﻿// -----------------------------------------------------------------------
// <copyright file="SdmxSemmanticException.cs" company="EUROSTAT">
//   Date Created : 2013-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Exception
{
    #region Using Directives

    using System;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    ///     Access exception is a specialized form of SdmxException which
    ///     has a fixed Sdmx Exception type of SemanticError
    /// </summary>
    [Serializable]
    public class SdmxSemmanticException : SdmxException
    {
        /// <summary>
        ///     The SDMX error code
        /// </summary>
        private static readonly SdmxErrorCode _sdmxErrorCode =
            SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError);

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSemmanticException" /> class.
        /// </summary>
        public SdmxSemmanticException()
            : base(_sdmxErrorCode.ErrorString, _sdmxErrorCode)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSemmanticException" /> class.
        ///     Creates Exception from an exception and an error Message
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message
        /// </param>
        /// <param name="exception">
        ///     The exception
        /// </param>
        public SdmxSemmanticException(string errorMessage, Exception exception)
            : base(exception, _sdmxErrorCode, errorMessage)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSemmanticException" /> class.
        ///     Creates Exception from an exception, an exception code and some arguments
        /// </summary>
        /// <param name="exception">
        ///     The exception
        /// </param>
        /// <param name="code">
        ///     The exception code
        /// </param>
        /// <param name="args">
        ///     The arguments
        /// </param>
        public SdmxSemmanticException(Exception exception, ExceptionCode code, params object[] args)
            : base(exception, _sdmxErrorCode, code, args)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSemmanticException" /> class.
        ///     Creates Exception from an exception code and some arguments
        /// </summary>
        /// <param name="code">
        ///     The exception code
        /// </param>
        /// <param name="args">
        ///     The arguments
        /// </param>
        public SdmxSemmanticException(ExceptionCode code, params object[] args)
            : base(null, _sdmxErrorCode, code, args)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSemmanticException" /> class.
        ///     Creates Exception from an error Message
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message
        /// </param>
        public SdmxSemmanticException(string errorMessage)
            : base(errorMessage, _sdmxErrorCode)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxSemmanticException" /> class.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the
        ///     exception being thrown.
        /// </param>
        /// <param name="context">
        ///     The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the
        ///     source or destination.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">
        ///     The <paramref name="info" /> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        ///     The class name is null or <see cref="P:System.Exception.HResult" /> is zero (0).
        /// </exception>
        protected SdmxSemmanticException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}