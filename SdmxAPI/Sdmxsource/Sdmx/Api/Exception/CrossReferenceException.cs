﻿// -----------------------------------------------------------------------
// <copyright file="CrossReferenceException.cs" company="EUROSTAT">
//   Date Created : 2013-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Exception
{
    #region Using Directives

    using System;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Creates a reference exception from a cross reference that could not be resolved in the system
    /// </summary>
    [Serializable]
    public class CrossReferenceException : SdmxSemmanticException
    {
        /// <summary>
        ///     The _cross reference
        /// </summary>
        private readonly ICrossReference _crossReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossReferenceException" /> class.
        /// </summary>
        public CrossReferenceException()
            : base(ExceptionCode.ReferenceErrorUnresolvable)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossReferenceException" /> class.
        /// </summary>
        /// <param name="crossReference">The cross reference</param>
        public CrossReferenceException(ICrossReference crossReference)
            : base(
                ExceptionCode.ReferenceErrorUnresolvable, 
                GetReferenceFromIdentifier(crossReference), 
                crossReference != null ? crossReference.TargetUrn : null)
        {
            if (crossReference == null)
            {
                throw new ArgumentNullException("crossReference");
            }

            this._crossReference = crossReference;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossReferenceException" /> class.
        ///     Creates Exception from an exception and an error Message
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message
        /// </param>
        /// <param name="exception">
        ///     The exception
        /// </param>
        public CrossReferenceException(string errorMessage, Exception exception)
            : base(exception, ExceptionCode.ReferenceErrorUnresolvable, errorMessage)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossReferenceException" /> class.
        ///     Creates Exception from an error Message
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message
        /// </param>
        public CrossReferenceException(string errorMessage)
            : base(ExceptionCode.ReferenceErrorUnresolvable, errorMessage)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossReferenceException" /> class.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the
        ///     exception being thrown.
        /// </param>
        /// <param name="context">
        ///     The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the
        ///     source or destination.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">
        ///     The <paramref name="info" /> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        ///     The class name is null or <see cref="P:System.Exception.HResult" /> is zero (0).
        /// </exception>
        protected CrossReferenceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._crossReference = (ICrossReference)info.GetValue("_crossReference", typeof(ICrossReference));
        }

        /// <summary>
        ///     Gets the cross reference that could not be resolved.
        /// </summary>
        /// <value>
        ///     The cross reference.
        /// </value>
        public ICrossReference CrossReference
        {
            get
            {
                return this._crossReference;
            }
        }

        /// <summary>
        ///     Gets the error type.
        /// </summary>
        public override string ErrorType
        {
            get
            {
                return "Reference Exception";
            }
        }

        /// <summary>
        ///     When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with
        ///     information about the exception.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object
        ///     data about the exception being thrown.
        /// </param>
        /// <param name="context">
        ///     The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual
        ///     information about the source or destination.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">
        ///     The <paramref name="info" /> parameter is a null reference (Nothing in
        ///     Visual Basic).
        /// </exception>
        /// <PermissionSet>
        ///     <IPermission
        ///         class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
        ///         version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
        ///     <IPermission
        ///         class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
        ///         version="1" Flags="SerializationFormatter" />
        /// </PermissionSet>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("_crossReference", this._crossReference);
        }

        /// <summary>
        ///     Gets the reference from identifier.
        /// </summary>
        /// <param name="crossReference">The cross reference.</param>
        /// <returns>A string containing the URN or the <see cref="ISdmxObject" /> it referenced</returns>
        private static string GetReferenceFromIdentifier(ICrossReference crossReference)
        {
            if (crossReference == null)
            {
                throw new ArgumentNullException("crossReference");
            }

            var parentIdentifiable = crossReference.ReferencedFrom.GetParent<IIdentifiableObject>(true);
            if (parentIdentifiable != null)
            {
                return parentIdentifiable.Urn.ToString();
            }

            return crossReference.ReferencedFrom.ToString();
        }
    }
}