﻿// -----------------------------------------------------------------------
// <copyright file="SdmxUnauthorisedException.cs" company="EUROSTAT">
//   Date Created : 2013-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Exception
{
    #region Using Directives

    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Access exception is a specialized form of SdmxException which
    ///     has a fixed sDMX Exception type of Unauthorized
    /// </summary>
    [Serializable]
    public class SdmxConflictException : SdmxException
    {
        /// <summary>
        ///     The SDMX error code
        /// </summary>
        private static readonly SdmxErrorCode _sdmxErrorCode =
            SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.Conflict);

        /// <summary>
        /// The structure reference
        /// </summary>
        private readonly IList<IStructureReference> _structureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        ///     Creates Exception from an error Message
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message
        /// </param>
        public SdmxConflictException(string errorMessage)
            : base(errorMessage, _sdmxErrorCode)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        /// </summary>
        /// <param name="code">
        ///     The Exception code
        /// </param>
        /// <param name="args">
        ///     The arguments
        /// </param>
        public SdmxConflictException(ExceptionCode code, params object[] args)
            : base(_sdmxErrorCode, code, args)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        ///     If the <paramref name="innerException" /> is a SdmxException - then the
        ///     error code will be used, if it is not, then InternalServerError will be used
        /// </summary>
        /// <param name="errorMessage">
        ///     The error message that explains the reason for the exception.
        /// </param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner
        ///     exception is specified.
        /// </param>
        public SdmxConflictException(string errorMessage, Exception innerException)
            : base(innerException, _sdmxErrorCode, errorMessage)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        /// </summary>
        public SdmxConflictException()
            : base(_sdmxErrorCode.ErrorString, _sdmxErrorCode)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="structureReference">The structure reference.</param>
        public SdmxConflictException(string errorMessage, IList<IStructureReference> structureReference)
            : base(errorMessage, _sdmxErrorCode)
        {
            _structureReference = structureReference;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxConflictException" /> class.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the
        ///     exception being thrown.
        /// </param>
        /// <param name="context">
        ///     The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the
        ///     source or destination.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">
        ///     The <paramref name="info" /> parameter is null.
        /// </exception>
        /// <exception cref="T:System.Runtime.Serialization.SerializationException">
        ///     The class name is null or <see cref="P:System.Exception.HResult" /> is zero (0).
        /// </exception>
        protected SdmxConflictException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._structureReference = (IList<IStructureReference>)info.GetValue("_structureReference", typeof(IList<IStructureReference>));
        }

        /// <summary>
        ///  Gets the error type.
        /// </summary>
        public override string ErrorType
        {
            get
            {
                return "Conflict Exception";
            }
        }

        /// <summary>
        /// Gets the structure reference.
        /// </summary>
        /// <value>
        /// The structure reference.
        /// </value>
        public IList<IStructureReference> StructureReference
        {
            get
            {
                return _structureReference;
            }
        }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with
        /// information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object
        /// data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual
        /// information about the source or destination.</param>
        /// <PermissionSet>
        ///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
        ///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="SerializationFormatter" />
        /// </PermissionSet>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("_structureReference", this._structureReference);
        }
    }
}