// -----------------------------------------------------------------------
// <copyright file="IDatasetStructureReference.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Header
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Structure is used in a Dataset header, where there is a one to one mapping between a Structure (which defines the
    ///     DSD/Flow/or Provision and the Dimensions at the observation level)
    /// </summary>
    public interface IDatasetStructureReference
    {
        /// <summary>
        ///     Gets the dimensionAtObservation is used to reference the dimension at the observation level for data messages.
        ///     <p />
        ///     Structure.ALL_DIMENSIONS denotes that the cross sectional data is in the flat format.
        ///     <p />
        ///     IPrimaryMeasure.FIXED_ID denotes that the data is in time series format
        /// </summary>
        string DimensionAtObservation { get; }

        /// <summary>
        ///     Gets the id of this structure, this
        /// </summary>
        /// <value> </value>
        string Id { get; }

        /// <summary>
        ///     Gets the serviceURL attribute indicates the Uri of an SDMX SOAP web service from which the
        ///     details of the object can be retrieved.
        ///     <p />
        ///     Note that this can be a registry or and SDMX structural metadata repository, as they both implement that same web
        ///     service interface.
        /// </summary>
        /// <value> </value>
        Uri ServiceUrl { get; }

        /// <summary>
        ///     Gets the structure reference, either a provision agreement, dataflow, a data structure definition (DSD), or
        ///     metadata structure definition (MSD)
        /// </summary>
        /// <value> </value>
        IStructureReference StructureReference { get; }

        /// <summary>
        ///     Gets the structureURL attribute indicates the Uri of a SDMX-ML structure message
        ///     (in the same version as the source document) in which the externally referenced object is contained.
        ///     <p />
        ///     Note that this may be a Uri of an SDMX <c>RESTful</c> web service which will return the referenced object.
        /// </summary>
        /// <value> </value>
        Uri StructureUrl { get; }

        /// <summary>
        ///     Gets a value indicating whether the getDimensionAtObservation() returns IPrimaryMeasure.FIXED_ID
        /// </summary>
        /// <value> </value>
        bool Timeseries { get; }
    }
}