// -----------------------------------------------------------------------
// <copyright file="IHierarchical.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     A IHierarchical object is one which can contain a single hierarchy of objects,
    ///     the type of which is defined by generics.
    /// </summary>
    /// <typeparam name="T">
    ///     - the type of object in the hierarchy
    /// </typeparam>
    public interface IHierarchical<T>
    {
        /// <summary>
        ///     Gets any children of this object.  If there are no children
        ///     then a <c>null</c> will be returned.
        /// </summary>
        /// <value> child categories </value>
        IList<T> Children { get; }

        /// <summary>
        ///     Gets the parent Object of this object, a null object reference will be
        ///     returned if there is no parent
        /// </summary>
        /// <value> </value>
        T Parent { get; }

        /// <summary>
        ///     Gets <c>true</c> if this Object contains children,
        ///     if this is the case the method call <c>getChildren()</c> is
        ///     guaranteed to return a Set with length greater then 0.
        /// </summary>
        /// <returns> true if this IHierarchical Object contains children </returns>
        bool HasChildren();

        /// <summary>
        ///     Gets <c>true</c> if this Object has a parent, false otherwise.
        /// </summary>
        /// <returns> true if this Object has a parent </returns>
        bool HasParent();
    }
}