// -----------------------------------------------------------------------
// <copyright file="IMaintainableMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base
{
    /// <summary>
    ///     A Maintainable Object is one that is maintainable by a maintenance agency
    /// </summary>
    public interface IMaintainableMutableSuperObject : INameableMutableSuperObject
    {
        /// <summary>
        ///     Gets or sets the agency id of the agency maintaining this object
        /// </summary>
        /// <returns> </returns>
        string AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this maintainable is final
        /// </summary>
        /// <returns> </returns>
        bool IsFinal { get; set; }

        /// <summary>
        ///     Gets or sets the version of this maintainable object
        /// </summary>
        /// <returns> </returns>
        string Version { get; set; }
    }
}