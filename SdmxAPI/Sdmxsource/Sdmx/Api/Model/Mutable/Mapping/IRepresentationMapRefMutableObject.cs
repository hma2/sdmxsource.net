// -----------------------------------------------------------------------
// <copyright file="IRepresentationMapRefMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     The RepresentationMapRefMutableObject interface.
    /// </summary>
    public interface IRepresentationMapRefMutableObject : IMutableObject
    {
        /// <summary>
        ///     Gets or sets the codelist map.
        /// </summary>
        IStructureReference CodelistMap { get; set; }

        /// <summary>
        ///     Gets or sets the to text format.
        /// </summary>
        ITextFormatMutableObject ToTextFormat { get; set; }

        /// <summary>
        ///     Gets or sets the to value type.
        /// </summary>
        ToValue ToValueType { get; set; }

        /// <summary>
        ///     Gets the value mappings.
        /// </summary>
        IDictionaryOfSets<string, string> ValueMappings { get; }

        /// <summary>
        ///     Maps the component id to the component with the given value
        /// </summary>
        /// <param name="componentId">The component unique identifier.</param>
        /// <param name="componentValue">The component value.</param>
        void AddMapping(string componentId, string componentValue);
    }
}