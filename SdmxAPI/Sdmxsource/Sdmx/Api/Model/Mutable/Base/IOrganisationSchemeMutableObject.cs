﻿// -----------------------------------------------------------------------
// <copyright file="IOrganisationSchemeMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The Organisation Scheme Mutable Object interface
    /// </summary>
    /// <typeparam name="TImmutable">The type of the immutable.</typeparam>
    /// <typeparam name="TMutable">The type of the mutable.</typeparam>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IItemSchemeMutableObject{TMutable}" />
    public interface IOrganisationSchemeMutableObject<out TImmutable, TMutable> : IItemSchemeMutableObject<TMutable>
        where TImmutable : IMaintainableObject where TMutable : IItemMutableObject
    {
        /// <summary>
        ///     Gets a representation of itself in a bean which can not be modified, modifications to the mutable bean
        ///     after this point will not be reflected in the returned instance of the MaintainableObject.
        /// </summary>
        new TImmutable ImmutableInstance { get; }
    }
}