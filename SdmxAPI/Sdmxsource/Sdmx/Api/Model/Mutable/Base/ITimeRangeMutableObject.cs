// -----------------------------------------------------------------------
// <copyright file="ITimeRangeMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     The TimeRangeMutableObject interface.
    /// </summary>
    public interface ITimeRangeMutableObject : IMutableObject
    {
        /// <summary>
        ///     Gets or sets the end date.
        /// </summary>
        DateTime? EndDate { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the date is included in the range.
        /// </summary>
        bool IsEndInclusive { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the start date and end date both have a value, and the range is between the
        ///     start and end dates.
        ///     <p />
        ///     If false, then only the start date or end date will be populated, if the start date is populated then it this
        ///     period refers
        ///     to dates before the start date. If the end date is populated then it refers to dates after the end date.
        /// </summary>
        /// <value> </value>
        bool IsRange { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether is start inclusive.
        /// </summary>
        bool IsStartInclusive { get; set; }

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        DateTime? StartDate { get; set; }

        /// <summary>
        ///     The create immutable instance.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <returns>
        ///     The <see cref="ITimeRange" /> .
        /// </returns>
        ITimeRange CreateImmutableInstance(ISdmxStructure parent);
    }
}