// -----------------------------------------------------------------------
// <copyright file="IItemSchemeMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     The ItemSchemeMutableObject interface.
    /// </summary>
    /// <typeparam name="T">
    ///     Generic type parameter.
    /// </typeparam>
    public interface IItemSchemeMutableObject<T> : IMaintainableMutableObject
        where T : IItemMutableObject
    {
        /// <summary>
        ///     Gets or sets a value indicating whether it is partial.
        /// </summary>
        bool IsPartial { get; set; }

        /// <summary>
        ///     Gets the list of items, this can be null.
        /// </summary>
        IList<T> Items { get; }

        /// <summary>
        ///     Adds the item to the list of items, creates a list if it does not already exist.
        /// </summary>
        /// <param name="item">The item.</param>
        void AddItem(T item);

        /// <summary>
        ///     Creates an item and adds it to the scheme
        /// </summary>
        /// <param name="id">The id to set</param>
        /// <param name="name">The name to set in the <c>en</c> language</param>
        /// <returns>The created item</returns>
        T CreateItem(string id, string name);

        /// <summary>
        ///     Removes the item with the given id, if it exists.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns>true if the item was successfully removed, false otherwise</returns>
        bool RemoveItem(string id);
    }
}