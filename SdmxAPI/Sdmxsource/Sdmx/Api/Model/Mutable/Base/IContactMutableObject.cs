﻿// -----------------------------------------------------------------------
// <copyright file="IContactMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    using System.Collections.Generic;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public interface IContactMutableObject : IMutableObject
    {
        /// <summary>
        ///     Gets the departments of the contact
        /// </summary>
        IList<ITextTypeWrapperMutableObject> Departments { get; }

        /// <summary>
        ///     Gets the email of the contact
        /// </summary>
        IList<string> Email { get; }

        /// <summary>
        ///     Gets the fax of the contact
        /// </summary>
        IList<string> Fax { get; }

        /// <summary>
        ///     Gets or sets the id of the contact
        /// </summary>
        string Id { get; set; }

        /// <summary>
        ///     Gets the names of the contact.
        /// </summary>
        /// <value>
        ///     list of names, or an empty list if none exist
        /// </value>
        IList<ITextTypeWrapperMutableObject> Names { get; }

        /// <summary>
        ///     Gets the roles of the contact
        /// </summary>
        /// <value>
        ///     The roles.
        /// </value>
        IList<ITextTypeWrapperMutableObject> Roles { get; }

        /// <summary>
        ///     Gets the telephone of the contact
        /// </summary>
        IList<string> Telephone { get; }

        /// <summary>
        ///     Gets the uris of the contact = list of uris, or an empty list if none exist
        /// </summary>
        IList<string> Uri { get; }

        /// <summary>
        ///     Gets the x400 of the contact
        /// </summary>
        IList<string> X400 { get; }

        /// <summary>
        ///     Adds a new department to the list, creates a new list if it is null
        /// </summary>
        /// <param name="dept">The department.</param>
        void AddDepartment(ITextTypeWrapperMutableObject dept);

        /// <summary>
        ///     Adds a new email to the list, creates a new list if it is null
        /// </summary>
        /// <param name="email">The email.</param>
        void AddEmail(string email);

        /// <summary>
        ///     Adds a new fax to the list, creates a new list if it is null
        /// </summary>
        /// <param name="fax">The fax.</param>
        void AddFax(string fax);

        /// <summary>
        ///     Adds a new name to the list, creates a new list if it is null
        /// </summary>
        /// <param name="name">The name.</param>
        void AddName(ITextTypeWrapperMutableObject name);

        /// <summary>
        ///     Adds a new role to the list, creates a new list if it is null
        /// </summary>
        /// <param name="role">The role.</param>
        void AddRole(ITextTypeWrapperMutableObject role);

        /// <summary>
        ///     Adds a new telephone to the list, creates a new list if it is null
        /// </summary>
        /// <param name="telephone">The telephone.</param>
        void AddTelephone(string telephone);

        /// <summary>
        ///     Adds a new uri to the list, creates a new list if it is null
        /// </summary>
        /// <param name="uri">The URI.</param>
        void AddUri(string uri);

        /// <summary>
        ///     Adds a new X400 to the list, creates a new list if it is null
        /// </summary>
        /// <param name="x400">The X400.</param>
        void AddX400(string x400);
    }
}