// -----------------------------------------------------------------------
// <copyright file="IAnnotationMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     The AnnotationMutableObject interface.
    /// </summary>
    public interface IAnnotationMutableObject : IMutableObject
    {
        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        ///     Gets the text.
        /// </summary>
        IList<ITextTypeWrapperMutableObject> Text { get; }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        string Title { get; set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        string Type { get; set; }

        /// <summary>
        ///     Gets or sets the url.
        /// </summary>
        Uri Uri { get; set; }

        /// <summary>
        ///     The add text.
        /// </summary>
        /// <param name="text">
        ///     The text.
        /// </param>
        void AddText(ITextTypeWrapperMutableObject text);

        /// <summary>
        ///     The add text.
        /// </summary>
        /// <param name="locale">
        ///     The locale.
        /// </param>
        /// <param name="text">
        ///     The text.
        /// </param>
        void AddText(string locale, string text);
    }
}