// -----------------------------------------------------------------------
// <copyright file="IMaintainableMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     The MaintainableMutableObject interface.
    /// </summary>
    public interface IMaintainableMutableObject : INameableMutableObject
    {
        /// <summary>
        ///     Gets or sets the agency id.
        /// </summary>
        string AgencyId { get; set; }

        /// <summary>
        ///     Gets or sets the end date.
        /// </summary>
        DateTime? EndDate { get; set; }

        /// <summary>
        ///     Gets or sets the external reference.
        /// </summary>
        TertiaryBool ExternalReference { get; set; }

        /// <summary>
        ///     Gets or sets the final structure.
        /// </summary>
        TertiaryBool FinalStructure { get; set; }

        /// <summary>
        ///     Gets a representation of itself in a Object which can not be modified, modifications to the mutable Object
        ///     are not reflected in the returned instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        IMaintainableObject ImmutableInstance { get; }

        /// <summary>
        ///     Gets or sets the service url.
        /// </summary>
        Uri ServiceURL { get; set; }

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        DateTime? StartDate { get; set; }

        /// <summary>
        ///     Gets or sets the structure url.
        /// </summary>
        Uri StructureURL { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether stub.
        /// </summary>
        bool Stub { get; set; }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        string Version { get; set; }
    }
}