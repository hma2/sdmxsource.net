// -----------------------------------------------------------------------
// <copyright file="IHierarchicalCodelistMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    #endregion

    /// <summary>
    ///     The HierarchicalCodelistMutableObject interface.
    /// </summary>
    public interface IHierarchicalCodelistMutableObject : IMaintainableMutableObject
    {
        /// <summary>
        ///     Gets the codelist ref.
        /// </summary>
        IList<ICodelistRefMutableObject> CodelistRef { get; }

        /// <summary>
        ///     Gets the hierarchies.
        /// </summary>
        IList<IHierarchyMutableObject> Hierarchies { get; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can not be modified, modifications to the mutable @object
        ///     are not reflected in the returned instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new IHierarchicalCodelistObject ImmutableInstance { get; }

        /// <summary>
        ///     The add codelist ref.
        /// </summary>
        /// <param name="codelistRef">
        ///     The codelist ref.
        /// </param>
        void AddCodelistRef(ICodelistRefMutableObject codelistRef);

        /// <summary>
        ///     The add hierarchies.
        /// </summary>
        /// <param name="hierarchy">
        ///     The hierarchy.
        /// </param>
        void AddHierarchies(IHierarchyMutableObject hierarchy);
    }
}