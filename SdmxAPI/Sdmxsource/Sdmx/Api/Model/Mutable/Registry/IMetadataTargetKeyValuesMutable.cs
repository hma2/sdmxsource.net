﻿// -----------------------------------------------------------------------
// <copyright file="IMetadataTargetKeyValuesMutable.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry.IKeyValuesMutable" />
    public interface IMetadataTargetKeyValuesMutable : IKeyValuesMutable
    {
        /// <summary>
        ///     Gets the dataset references.
        /// </summary>
        /// <value>
        ///     The dataset references.
        /// </value>
        IList<IDataSetReferenceMutableObject> DatasetReferences { get; }

        /// <summary>
        ///     Gets the object references.
        /// </summary>
        /// <value>
        ///     The object references.
        /// </value>
        IList<IStructureReference> ObjectReferences { get; }

        /// <summary>
        ///     Adds the dataset reference.
        /// </summary>
        /// <param name="reference">The reference.</param>
        void AddDatasetReference(IDataSetReferenceMutableObject reference);

        /// <summary>
        ///     Adds the object reference.
        /// </summary>
        /// <param name="sref">The arguments preference.</param>
        void AddObjectReference(IStructureReference sref);
    }
}