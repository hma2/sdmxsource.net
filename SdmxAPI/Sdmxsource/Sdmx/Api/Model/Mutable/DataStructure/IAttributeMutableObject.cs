// -----------------------------------------------------------------------
// <copyright file="IAttributeMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The AttributeMutableObject interface.
    /// </summary>
    public interface IAttributeMutableObject : IComponentMutableObject
    {
        /// <summary>
        ///     Gets or sets the assignment status.
        ///     The assignmentStatus attribute indicates whether a value must be provided for the attribute when sending
        ///     documentation along with the data.
        /// </summary>
        string AssignmentStatus { get; set; }

        /// <summary>
        ///     Gets or sets the attachment group.
        /// </summary>
        string AttachmentGroup { get; set; }

        /// <summary>
        ///     Gets or sets the attachment level.
        ///     Attributes with an attachment level of Group are only available if the data is organized in groups,
        ///     and should be used appropriately, as the values may not be communicated if the data is not grouped.
        /// </summary>
        AttributeAttachmentLevel AttachmentLevel { get; set; }

        /// <summary>
        ///     Gets the concept roles.
        /// </summary>
        IList<IStructureReference> ConceptRoles { get; }

        /// <summary>
        ///     Gets the dimension reference.
        /// </summary>
        IList<string> DimensionReferences { get; }

        /// <summary>
        ///     Gets or sets the primary measure reference.
        /// </summary>
        string PrimaryMeasureReference { get; set; }

        /// <summary>
        ///     Adds a concept Role to the existing list of concept roles
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        void AddConceptRole(IStructureReference structureReference);
    }
}