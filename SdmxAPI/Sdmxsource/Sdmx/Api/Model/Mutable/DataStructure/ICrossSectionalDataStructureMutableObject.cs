// -----------------------------------------------------------------------
// <copyright file="ICrossSectionalDataStructureMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     The CrossSectionalDataStructureMutableObject interface.
    /// </summary>
    public interface ICrossSectionalDataStructureMutableObject : IDataStructureMutableObject
    {
        /// <summary>
        ///     Gets a map of attribute id, mapping to the cross sectional measure ids it is attached to
        /// </summary>
        /// <value> </value>
        IDictionaryOfLists<string, string> AttributeToMeasureMap { get; }

        /// <summary>
        ///     Gets the cross sectional attach data set.
        /// </summary>
        IList<string> CrossSectionalAttachDataSet { get; }

        /// <summary>
        ///     Gets the cross sectional attach group.
        /// </summary>
        IList<string> CrossSectionalAttachGroup { get; }

        /// <summary>
        ///     Gets the cross sectional attach observation.
        /// </summary>
        IList<string> CrossSectionalAttachObservation { get; }

        /// <summary>
        ///     Gets the cross sectional attach section.
        /// </summary>
        IList<string> CrossSectionalAttachSection { get; }

        /// <summary>
        ///     Gets the cross sectional measures.
        /// </summary>
        IList<ICrossSectionalMeasureMutableObject> CrossSectionalMeasures { get; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can not be modified, modifications to the mutable @object
        ///     are not reflected in the returned instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new ICrossSectionalDataStructureObject ImmutableInstance { get; }

        /// <summary>
        ///     Gets the measure dimension codelist mapping.
        /// </summary>
        IDictionary<string, IStructureReference> MeasureDimensionCodelistMapping { get; }

        /// <summary>
        ///     The add cross sectional attach data set.
        /// </summary>
        /// <param name="dimensionReference">
        ///     The dimension reference.
        /// </param>
        void AddCrossSectionalAttachDataSet(string dimensionReference);

        /// <summary>
        ///     The add cross sectional attach group.
        /// </summary>
        /// <param name="dimensionReference">
        ///     The dimension reference.
        /// </param>
        void AddCrossSectionalAttachGroup(string dimensionReference);

        /// <summary>
        ///     The add cross sectional attach observation.
        /// </summary>
        /// <param name="dimensionReference">
        ///     The dimension reference.
        /// </param>
        void AddCrossSectionalAttachObservation(string dimensionReference);

        /// <summary>
        ///     The add cross sectional attach section.
        /// </summary>
        /// <param name="dimensionReference">
        ///     The dimension reference.
        /// </param>
        void AddCrossSectionalAttachSection(string dimensionReference);

        /// <summary>
        ///     The add cross sectional measures.
        /// </summary>
        /// <param name="crossSectionalMeasure">
        ///     The cross sectional measure.
        /// </param>
        void AddCrossSectionalMeasures(ICrossSectionalMeasureMutableObject crossSectionalMeasure);
    }
}