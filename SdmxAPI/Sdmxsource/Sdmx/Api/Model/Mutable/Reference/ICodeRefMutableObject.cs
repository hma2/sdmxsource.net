// -----------------------------------------------------------------------
// <copyright file="ICodeRefMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The CodeRefMutableObject interface.
    /// </summary>
    public interface ICodeRefMutableObject : IIdentifiableMutableObject
    {
        /// <summary>
        ///     Gets or sets the code id.
        /// </summary>
        string CodeId { get; set; }

        /// <summary>
        ///     Gets or sets the codelist alias ref.
        /// </summary>
        string CodelistAliasRef { get; set; }

        /// <summary>
        ///     Gets or sets the code reference.
        /// </summary>
        IStructureReference CodeReference { get; set; }

        /// <summary>
        ///     Gets the code refs.
        /// </summary>
        IList<ICodeRefMutableObject> CodeRefs { get; }

        /// <summary>
        ///     Gets or sets the level reference.
        /// </summary>
        string LevelReference { get; set; }

        /// <summary>
        ///     Gets or sets the valid from.
        /// </summary>
        DateTime? ValidFrom { get; set; }

        /// <summary>
        ///     Gets or sets the valid to.
        /// </summary>
        DateTime? ValidTo { get; set; }

        /// <summary>
        ///     The add code ref.
        /// </summary>
        /// <param name="codeRef">
        ///     The code ref.
        /// </param>
        void AddCodeRef(ICodeRefMutableObject codeRef);
    }
}