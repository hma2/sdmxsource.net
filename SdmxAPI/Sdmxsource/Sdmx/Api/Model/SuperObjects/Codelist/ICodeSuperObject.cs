// -----------------------------------------------------------------------
// <copyright file="ICodeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    #endregion

    /// <summary>
    ///     A code is a id/value pair object, the id is typically what data references where the values is typically
    ///     the semantic meaning of the code
    ///     human readable form.
    ///     <p />
    ///     The SuperObjects representation of a code forms simple hierarchies where they exist, so a code within a codelist
    ///     can have
    ///     a single parent, and many children.
    /// </summary>
    public interface ICodeSuperObject : IItemSuperObject<ICodelistSuperObject>, IHierarchical<ICodeSuperObject>
    {
        /// <summary>
        ///     Gets the code built from object.
        /// </summary>
        new ICode BuiltFrom { get; }
    }
}