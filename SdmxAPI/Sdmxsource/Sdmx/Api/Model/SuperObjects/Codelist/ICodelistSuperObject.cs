// -----------------------------------------------------------------------
// <copyright file="ICodelistSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    #endregion

    /// <summary>
    ///     A ICodelistSuperObject is a container for codes.
    /// </summary>
    public interface ICodelistSuperObject : IMaintainableSuperObject
    {
        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value> The ICodelistObject that this ICodelistSuperObject was built from. </value>
        new ICodelistObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the codes in this codelist. As codes are hierarchical only the top level codes, with no parents, will be
        ///     returned.
        /// </summary>
        /// <value> the codes in this codelist. </value>
        IList<ICodeSuperObject> Codes { get; }

        /// <summary>
        ///     Iterates through the code hierarchy and returns the ICodeSuperObject that has the same id as that supplied.
        /// </summary>
        /// <param name="id">
        ///     the id of a ICodeSuperObject to search for.
        /// </param>
        /// <returns>
        ///     the matching ICodeSuperObject or null if there was no match.
        /// </returns>
        ICodeSuperObject GetCodeByValue(string id);
    }
}