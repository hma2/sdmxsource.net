// -----------------------------------------------------------------------
// <copyright file="IAttributeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    #endregion

    /// <summary>
    ///     An Attribute is a item of information used to add additional metadata to a key or observation or
    ///     group.
    /// </summary>
    public interface IAttributeSuperObject : IComponentSuperObject
    {
        /// <summary>
        ///     Gets the assignmentStatus attribute indicates whether a
        ///     value must be provided for the attribute when sending documentation along with the data.
        /// </summary>
        /// <value> </value>
        string AssignmentStatus { get; }

        /// <summary>
        ///     Gets the attachment group.
        /// </summary>
        string AttachmentGroup { get; }

        /// <summary>
        ///     Gets the ATTRIBUTE_ATTACHMENT_LEVEL attribute indicating the level to which the attribute is attached in
        ///     time-series formats
        ///     (generic, compact, utility data formats).
        ///     Attributes with an attachment level of Group are only available if the data is organized in groups,
        ///     and should be used appropriately, as the values may not be communicated if the data is not grouped.
        /// </summary>
        /// <value> </value>
        AttributeAttachmentLevel AttachmentLevel { get; }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        new IAttributeObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the dimension reference.
        /// </summary>
        IList<string> DimensionReferences { get; }

        /// <summary>
        ///     Gets a value indicating whether the attribute is mandatory
        /// </summary>
        /// <value> </value>
        bool Mandatory { get; }

        /// <summary>
        ///     Gets the primary measure reference.
        /// </summary>
        string PrimaryMeasureReference { get; }
    }
}