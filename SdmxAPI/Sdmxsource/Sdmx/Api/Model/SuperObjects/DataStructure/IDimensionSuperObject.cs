// -----------------------------------------------------------------------
// <copyright file="IDimensionSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    #endregion

    /// <summary>
    ///     A dimension is one component of a key that together with other dimensions of the key
    ///     uniquely identify the observation.
    /// </summary>
    public interface IDimensionSuperObject : IComponentSuperObject
    {
        /// <summary>
        ///     Gets the built from.
        /// </summary>
        new IDimension BuiltFrom { get; }

        /// <summary>
        ///     Gets a value indicating whether the dimension is the frequency dimension, false otherwise.
        /// </summary>
        /// <value> </value>
        bool FrequencyDimension { get; }

        /// <summary>
        ///     Gets a value indicating whether the dimension is the measure dimension, false otherwise.
        /// </summary>
        /// <value> </value>
        bool MeasureDimension { get; }

        /// <summary>
        ///     Gets a value indicating whether time dimension.
        /// </summary>
        bool TimeDimension { get; }
    }
}