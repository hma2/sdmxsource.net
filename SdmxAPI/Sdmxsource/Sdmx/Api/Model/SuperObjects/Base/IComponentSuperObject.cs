// -----------------------------------------------------------------------
// <copyright file="IComponentSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;

    #endregion

    /// <summary>
    ///     A component is something that can be conceptualized and where the values of the component
    ///     can also be taken from a Codelist, an example is a Dimensions
    /// </summary>
    public interface IComponentSuperObject : IIdentifiableSuperObject
    {
        /// <summary>
        ///     Gets the built from.
        /// </summary>
        new IComponent BuiltFrom { get; }

        /// <summary>
        ///     Gets the concept, this is mandatory and will always return a value
        /// </summary>
        /// <value> </value>
        IConceptSuperObject Concept { get; }

        /// <summary>
        ///     Gets the text format, this may be null if there is none
        /// </summary>
        /// <value> </value>
        ITextFormat TextFormat { get; }

        /// <summary>
        ///     Gets the codelist, this may be null if there is none
        /// </summary>
        /// <param name="useConceptIfRequired">
        ///     if the representation is uncoded, but the concept has default representation, then the codelist for the concept
        ///     will be returned if this parameter is set to true
        /// </param>
        /// <returns>
        ///     The <see cref="ICodelistSuperObject" /> .
        /// </returns>
        ICodelistSuperObject GetCodelist(bool useConceptIfRequired);
    }
}