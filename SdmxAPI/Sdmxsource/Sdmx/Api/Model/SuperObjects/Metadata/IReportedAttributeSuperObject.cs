// -----------------------------------------------------------------------
// <copyright file="IReportedAttributeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Metadata
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    #endregion

    /// <summary>
    ///     The ReportedAttributeBase interface.
    /// </summary>
    public interface IReportedAttributeSuperObject : ISuperObject
    {
        /// <summary>
        ///     Gets the IReferenceValue that was used to build this Base object - Override from parent
        /// </summary>
        new IReportedAttributeObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the codelist.
        /// </summary>
        ICodelistObject Codelist { get; }

        /// <summary>
        ///     Gets the concept.
        /// </summary>
        IConceptObject Concept { get; }

        /// <summary>
        ///     Gets a value indicating whether this is the coded representation.
        /// </summary>
        bool HasCodedRepresentation { get; }

        /// <summary>
        ///     Gets a value indicating whether the IReportedAttributeObject has a simple value, in which case getSimpleValue will
        ///     return a not null value
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has simple value; otherwise, <c>false</c>.
        /// </value>
        bool HasSimpleValue { get; }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        string Id { get; }

        /// <summary>
        ///     Gets the metadata text.
        /// </summary>
        /// <returns>a list of structured texts for this component - will return an empty list if no Texts exist.</returns>
        IList<ITextTypeWrapper> MetadataText { get; }

        /// <summary>
        ///     Gets a value indicating whether the getMetadataText returns an empty list
        /// </summary>
        /// <value> </value>
        bool Presentational { get; }

        /// <summary>
        ///     Gets the reported attributes.
        /// </summary>
        /// <returns>child attributes</returns>
        IList<IReportedAttributeSuperObject> ReportedAttributes { get; }

        /// <summary>
        ///     Gets a simple value for this attribute, returns null if there is no simple value
        /// </summary>
        /// <value> </value>
        string SimpleValue { get; }
    }
}