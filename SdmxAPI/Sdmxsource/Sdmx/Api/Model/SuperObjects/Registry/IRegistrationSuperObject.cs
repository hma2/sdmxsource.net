// -----------------------------------------------------------------------
// <copyright file="IRegistrationSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    #endregion

    /// <summary>
    ///     The RegistrationObjectBase interface.
    /// </summary>
    public interface IRegistrationSuperObject : IMaintainableSuperObject
    {
        /// <summary>
        ///     Gets the Build From object
        /// </summary>
        new IRegistrationObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the data source.
        /// </summary>
        IDataSource DataSource { get; }

        /// <summary>
        ///     Gets the index attributes.
        /// </summary>
        TertiaryBool IndexAttribtues { get; }

        /// <summary>
        ///     Gets the index dataset.
        /// </summary>
        TertiaryBool IndexDataset { get; }

        /// <summary>
        ///     Gets the index reporting period.
        /// </summary>
        TertiaryBool IndexReportingPeriod { get; }

        /// <summary>
        ///     Gets the index time series.
        /// </summary>
        TertiaryBool IndexTimeSeries { get; }

        /// <summary>
        ///     Gets the last updated.
        /// </summary>
        ISdmxDate LastUpdated { get; }

        /// <summary>
        ///     Gets the provision agreement that this registration is referencing
        /// </summary>
        /// <value> </value>
        IProvisionAgreementSuperObject ProvisionAgreement { get; }

        /// <summary>
        ///     Gets the valid from.
        /// </summary>
        ISdmxDate ValidFrom { get; }

        /// <summary>
        ///     Gets the valid to.
        /// </summary>
        ISdmxDate ValidTo { get; }
    }
}