// -----------------------------------------------------------------------
// <copyright file="IProvisionAgreementSuperObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;

    #endregion

    /// <summary>
    ///     The ProvisionAgreementObjectBase interface.
    /// </summary>
    public interface IProvisionAgreementSuperObject : IMaintainableSuperObject
    {
        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value> the IProvisionAgreementObject that was the constructor of this Super @object. </value>
        new IProvisionAgreementObject BuiltFrom { get; }

        /// <summary>
        ///     Gets the dataflow super @object that this provision agreement references
        /// </summary>
        /// <value> </value>
        IDataflowSuperObject DataflowSuperObject { get; }

        /// <summary>
        ///     Gets the data provider @object that this provision agreement references
        /// </summary>
        /// <value> </value>
        IDataProvider DataProvider { get; }
    }
}