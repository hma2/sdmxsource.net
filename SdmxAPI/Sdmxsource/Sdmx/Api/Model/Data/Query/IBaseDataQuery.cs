﻿// -----------------------------------------------------------------------
// <copyright file="IBaseDataQuery.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    #endregion

    /// <summary>
    ///     Base interface for both standard and complex data queries
    /// </summary>
    public interface IBaseDataQuery
    {
        /// <summary>
        ///     Gets the Dataflow that the query is returning data for.
        /// </summary>
        IDataflowObject Dataflow { get; }

        /// <summary>
        ///     Gets the data provider(s) that the query is for, an empty list represents ALL
        /// </summary>
        ISet<IDataProvider> DataProvider { get; }

        /// <summary>
        ///     Gets the detail of the query.  The detail specifies whether to return just the series keys, just the data or
        ///     everything.
        ///     <p />
        ///     Defaults to FULL (everything)
        /// </summary>
        DataQueryDetail DataQueryDetail { get; }

        /// <summary>
        ///     Gets the Key Family (Data Structure Definition) that this query is returning data for.
        /// </summary>
        IDataStructureObject DataStructure { get; }

        /// <summary>
        ///     Gets the id of the dimension to be attached at the observation level,
        ///     if not specified the returned data will be time series.
        /// </summary>
        /// <value>
        ///     The dimension
        /// </value>
        string DimensionAtObservation { get; }

        /// <summary>
        ///     Gets the first 'n' observations, for each series key,  to return as a result of a data query.
        /// </summary>
        int? FirstNObservations { get; }

        /// <summary>
        ///     Gets the last 'n' observations, for each series key, to return as a result of a data query.
        /// </summary>
        int? LastNObservations { get; }

        /// <summary>
        ///     Gets a value indicating whether this query has one or more selection groups on it, false means the query is a query
        ///     for all.
        /// </summary>
        /// <returns>
        ///     The boolean
        /// </returns>
        bool HasSelections();
    }
}