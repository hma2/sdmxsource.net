// -----------------------------------------------------------------------
// <copyright file="IDataQuery.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    #endregion

    /// <summary>
    ///     A DataQuery is a Schema independent representation of a DataWhere/REST query
    /// </summary>
    /// <example>
    ///     How to build using existing data queries <see cref="IDataQuery" />
    ///     <code source="..\ReUsingExamples\DataQuery\ReUsingDataQueryParsingManager.cs" lang="cs" />
    ///     How to build programmatic:
    ///     <code source="..\ReUsingExamples\DataQuery\BuildDataQuery.cs" lang="cs" />
    /// </example>
    public interface IDataQuery : IBaseDataQuery
    {
        /// <summary>
        ///     Gets last updated date is a filter on the data meaning 'only return data that was updated after this time'
        ///     <p />
        ///     If this attribute is used, the returned message should
        ///     only include the latest version of what has changed in the database since that point in time (updates and
        ///     revisions).
        ///     <p />
        ///     This should include:
        ///     <ul>
        ///         <li>Observations that have been added since the last time the query was performed (INSERT)</li>
        ///         <li>Observations that have been revised since the last time the query was performed (UPDATE)</li>
        ///         <li>Observations that have been revised since the last time the query was performed (UPDATE)</li>
        ///         <li>Observations that have been deleted since the last time the query was performed (DELETE)</li>
        ///     </ul>
        ///     If no offset is specified, default to local
        ///     <p />
        ///     Gets null if unspecified
        /// </summary>
        /// <value> </value>
        ISdmxDate LastUpdatedDate { get; }

        /// <summary>
        ///     Gets a copy of all the selection groups
        /// </summary>
        /// <value> </value>
        IList<IDataQuerySelectionGroup> SelectionGroups { get; }
    }
}