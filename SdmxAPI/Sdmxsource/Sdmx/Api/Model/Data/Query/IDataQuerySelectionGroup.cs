// -----------------------------------------------------------------------
// <copyright file="IDataQuerySelectionGroup.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data.Query
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    #endregion

    /// <summary>
    ///     A Data Query selection group contains a set of DataQuerySelection's implicit ANDED each DataQuerySelection
    ///     contains a concept along with one or more selection values.  For example DataQuerySelection may contain FREQ=A,B
    ///     which would
    ///     equate to FREQ = A OR B.
    ///     Example :
    ///     DataQuerySelection FREQ=A,B
    ///     DataQuerySelection COUNTRY=UK
    ///     Equates to (FREQ = A OR B) AND (COUNTRY = UK)
    /// </summary>
    /// <example>
    ///     A sample implementation in C# of <see cref="IDataQuerySelectionGroup" />
    ///     <code source="..\ReUsingExamples\DataQuery\ReUsingDataQueryParsingManager.cs" lang="cs" />
    /// </example>
    public interface IDataQuerySelectionGroup
    {
        /// <summary>
        ///     Gets the date from in this selection group
        /// </summary>
        /// <value> </value>
        ISdmxDate DateFrom { get; }

        /// <summary>
        ///     Gets the date to in this selection group
        /// </summary>
        /// <value> </value>
        ISdmxDate DateTo { get; }

        /// <summary>
        ///     Gets the set of selections for this group - these are implicitly ANDED
        /// </summary>
        /// <value> </value>
        ISet<IDataQuerySelection> Selections { get; }

        /// <summary>
        ///     Gets the selection(s) for the given dimension id, returns null if no selection exist for the dimension id
        /// </summary>
        /// <param name="componentId">The component Id. </param>
        /// <returns>
        ///     The <see cref="IDataQuerySelection" /> .
        /// </returns>
        IDataQuerySelection GetSelectionsForConcept(string componentId);

        /// <summary>
        ///     Gets a value indicating whether the selections exist for this dimension Id
        /// </summary>
        /// <param name="dimensionId">
        ///     The dimension Id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasSelectionForConcept(string dimensionId);
    }
}