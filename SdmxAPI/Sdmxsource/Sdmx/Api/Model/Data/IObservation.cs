// -----------------------------------------------------------------------
// <copyright file="IObservation.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     An Observation is a data element that defines a time and value, an Observation may also have attributes attached to
    ///     it.
    ///     <p />
    ///     An Observation can be compared to another, with regards to time.  An observation with a later time period, will
    ///     return a value > 1
    /// </summary>
    public interface IObservation : IAttributable, IComparable<IObservation>
    {
        /// <summary>
        ///     Gets a copy of the annotations.
        /// </summary>
        /// <value>
        ///     The annotations.
        /// </value>
        IList<IAnnotation> Annotations { get; }

        /// <summary>
        ///     Gets a value indicating whether the is a cross sectional observation,
        ///     the call to get the observation time will still return the observation time of the cross section, also
        ///     available on the series key, however there is an additional cross sectional attribute available
        /// </summary>
        /// <value> </value>
        bool CrossSection { get; }

        /// <summary>
        ///     Gets the cross sectional key value
        /// </summary>
        /// <value> </value>
        IKeyValue CrossSectionalValue { get; }

        /// <summary>
        ///     Gets the observation time as a Date Object
        /// </summary>
        /// <value> </value>
        DateTime? ObsAsTimeDate { get; }

        /// <summary>
        ///     Gets the value of the observation
        /// </summary>
        /// <value> </value>
        string ObservationValue { get; }

        /// <summary>
        ///     Gets the observation time in a SDMX time format
        /// </summary>
        /// <value> </value>
        string ObsTime { get; }

        /// <summary>
        ///     Gets the time format the observation value is in
        /// </summary>
        /// <value> </value>
        TimeFormat ObsTimeFormat { get; }

        /// <summary>
        ///     Gets the parent series key for this observation.  The returned object can not be null.
        /// </summary>
        IKeyable SeriesKey { get; }
    }
}