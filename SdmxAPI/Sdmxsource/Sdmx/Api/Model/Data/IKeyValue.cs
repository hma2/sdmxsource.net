// -----------------------------------------------------------------------
// <copyright file="IKeyValue.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Data
{
    #region Using directives

    using System;

    #endregion

    /// <summary>
    ///     A Dimensions point ties a code and concept together to represent an identifier
    ///     for an item of data
    /// </summary>
    public interface IKeyValue : IComparable<IKeyValue>
    {
        /// <summary>
        ///     Gets the code that the value is for
        /// </summary>
        /// <value> </value>
        string Code { get; }

        /// <summary>
        ///     Gets the key (concept/dimension) that this KeyValue is for
        /// </summary>
        /// <value> </value>
        string Concept { get; }
    }
}