// -----------------------------------------------------------------------
// <copyright file="IMetadataSetMutableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Metadata.Mutable
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The MetadataSetMutableObject interface.
    /// </summary>
    public interface IMetadataSetMutableObject : IMutableObject
    {
        /// <summary>
        ///     Gets or sets reference to the DataProvider
        /// </summary>
        /// <value> </value>
        IStructureReference DataProviderReference { get; set; }

        /// <summary>
        ///     Gets or sets the publication period
        /// </summary>
        /// <value></value>
        string PublicationPeriod { get; set; }

        /// <summary>
        ///     Gets or sets the four digit ISo 8601 year of publication
        /// </summary>
        /// <value> </value>
        DateTime PublicationYear { get; set; }

        /// <summary>
        ///     Gets or sets the reportingBeginDate the inclusive start time of the data reported in the metadata set
        /// </summary>
        DateTime ReportingBeginDate { get; set; }

        /// <summary>
        ///     Gets or sets the inclusive end time of the data reported in the metadata set
        /// </summary>
        /// <value>  </value>
        DateTime ReportingEndDate { get; set; }

        /// <summary>
        ///     Gets the reports.
        /// </summary>
        IList<IReportMutableObject> Reports { get; }

        /// <summary>
        ///     Gets or sets the reference to the structure in the Header above. Mandatory
        /// </summary>
        /// <value>  </value>
        string StructureRef { get; set; }

        /// <summary>
        ///     Gets or sets the inclusive start time of the validity of the info in the metadata set
        /// </summary>
        /// <value>  </value>
        DateTime ValidFromDate { get; set; }

        /// <summary>
        ///     Gets or sets the inclusive end time of the validity of the info in the metadata set
        /// </summary>
        /// <value>  </value>
        DateTime ValidToDate { get; set; }

        /// <summary>
        ///     The add report.
        /// </summary>
        /// <param name="report">
        ///     The report.
        /// </param>
        void AddReport(IReportMutableObject report);

        // FUNC footer messages
    }
}