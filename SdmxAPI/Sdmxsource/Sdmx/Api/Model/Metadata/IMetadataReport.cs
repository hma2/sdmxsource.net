// -----------------------------------------------------------------------
// <copyright file="IMetadataReport.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Metadata
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     The MetadataReport interface.
    /// </summary>
    public interface IMetadataReport : ISdmxObject
    {
        /// <summary>
        ///     Gets the id.
        /// </summary>
        string Id { get; }

        /// <summary>
        ///     Gets a copy of the list of the ReportedAttributes
        /// </summary>
        IList<IReportedAttributeObject> ReportedAttributes { get; }

        /// <summary>
        ///     Gets the target, defining what this metadata report attaches to.  The same information can be found
        ///     using the Target...properties on this interface.
        /// </summary>
        ITarget Target { get; }

        /// <summary>
        ///     Gets the reference to the content constraint, if there is one
        ///     This will search the Target.IReportedAttribute for any that contain a constraint reference, and will return null if
        ///     none do.
        /// </summary>
        ICrossReference TargetContentConstraintReference { get; }

        /// <summary>
        ///     Gets a list of data keys, will return an empty list if IsDatasetReference is false
        ///     This will search the Target.IReportedAttribute for any that contain data keys, and will return null if none do.
        /// </summary>
        IList<IDataKey> TargetDataKeys { get; }

        /// <summary>
        ///     Gets the id of the dataset this bean is referencing, returns null if this is not a dataset reference.
        ///     This will search the Target.ReportedAttribute for any that contain a dataset id, and will return null if none do.
        ///     <remarks>
        ///         This is to be used in conjunction with the IdentifiableReference which will return the data provider reference
        ///     </remarks>
        /// </summary>
        string TargetDatasetId { get; }

        /// <summary>
        ///     Gets null if there is no reference to an identifiable structure
        ///     This will search the Target.IReportedAttribute for any that contain a reportPeriod, and will return null if none
        ///     do.
        /// </summary>
        ICrossReference TargetIdentifiableReference { get; }

        /// <summary>
        ///     Gets the date for which this report is relevant
        ///     This will search the Target.IReportedAttribute for any that contain a report Period, and will return null if none
        ///     do.
        /// </summary>
        ISdmxDate TargetReportPeriod { get; }

        /// <summary>
        ///     Gets the targets that exist in the target object
        /// </summary>
        ISet<TargetType> Targets { get; }
    }
}