﻿// -----------------------------------------------------------------------
// <copyright file="IRestStructureQuery.cs" company="EUROSTAT">
//   Date Created : 2013-05-10
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Query
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Represents a query for Maintainable Structure(s) and maps to the SDMX REST query
    /// </summary>
    public interface IRestStructureQuery
    {
        /// <summary>
        ///     Gets information about the response detail, and which referenced artefacts should be  referenced in the response
        /// </summary>
        IStructureQueryMetadata StructureQueryMetadata { get; }

        /// <summary>
        ///     Gets the structure reference, which defines the agency, id, version and structure type of the structure being
        ///     queried
        /// </summary>
        IStructureReference StructureReference { get; }
    }
}