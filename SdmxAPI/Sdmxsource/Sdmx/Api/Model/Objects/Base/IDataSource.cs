// -----------------------------------------------------------------------
// <copyright file="IDataSource.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;

    #endregion

    /// <summary>
    ///     Describes the location and type of Data source
    /// </summary>
    public interface IDataSource : ISdmxStructure
    {
        /// <summary>
        ///     Gets the Uri of the data source, this can never be null
        /// </summary>
        /// <value>
        ///     The data URL.
        /// </value>
        Uri DataUrl { get; }

        /// <summary>
        ///     Gets a value indicating whether the the getDataUrl() is pointing at a REST web service
        /// </summary>
        /// <value>
        ///     <c>true</c> if [rest data source]; otherwise, <c>false</c>.
        /// </value>
        bool RESTDatasource { get; }

        /// <summary>
        ///     Gets a value indicating whether the the getDataUrl() is pointing at a file location
        /// </summary>
        /// <value> </value>
        bool SimpleDatasource { get; }

        /// <summary>
        ///     Gets the WADL Uri - will return null if no WADL location has been defined
        /// </summary>
        /// <value> </value>
        Uri WadlUrl { get; }

        /// <summary>
        ///     Gets a value indicating whether the the getDataUrl() is pointing at a SOAP web service
        /// </summary>
        /// <value> </value>
        bool WebServiceDatasource { get; }

        /// <summary>
        ///     Gets the Uri of the WSDL - will return null if no WSDL location has been defined
        /// </summary>
        /// <value> </value>
        Uri WsdlUrl { get; }

        /// <summary>
        ///     Gets a mutable version of this agencySchemeMutableObject instance
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataSourceMutableObject" /> .
        /// </returns>
        IDataSourceMutableObject CreateMutableInstance();
    }
}