// -----------------------------------------------------------------------
// <copyright file="IIdentifiableObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     An Identifiable Object describes a Object which contains a unique identifier (urn) and can therefore
    ///     be identified uniquely.
    /// </summary>
    public interface IIdentifiableObject : IAnnotableObject
    {
        /// <summary>
        ///     Gets a list of all the underlying text types for this identifiable (does not scroll down to children).
        ///     <p />
        ///     Gets an empty list if there are no text types for this identifiable
        /// </summary>
        /// <value>
        ///     All text types.
        /// </value>
        IList<ITextTypeWrapper> AllTextTypes { get; }

        /// <summary>
        ///     Gets a IStructureReference that is a representation of this IIdentifiableObject as a reference.  The
        ///     returned IStructureReference can be used to uniquely identify this identifiable @object.
        /// </summary>
        /// <value>
        ///     The structure reference.
        /// </value>
        IStructureReference AsReference { get; }

        /// <summary>
        ///     Gets the id for this component, this is a mandatory field and will never be null
        /// </summary>
        /// <value> </value>
        string Id { get; }

        /// <summary>
        ///     Gets the URI for this component, returns null if there is no URI.
        ///     <p />
        ///     URI describes where additional information can be found for this component, this is guaranteed to return
        ///     a value if the structure is a <c>IMaintainableObject</c> and isExternalReference is true
        /// </summary>
        /// <value> </value>
        Uri Uri { get; }

        /// <summary>
        ///     Gets the URN for this component.  The URN is unique to this instance and is a computable generated value based on
        ///     other attributes set within the component.
        /// </summary>
        /// <value> </value>
        Uri Urn { get; }

        /// <summary>
        ///     Gets a period separated id of this identifiable, starting from the non-maintainable top level ancestor to this
        ///     identifiable.
        ///     <p />
        ///     For example, if this is Category A living as a child of Category AA, then this method will return AA.A (not the
        ///     category scheme id is not present in this identifier)
        ///     <p />
        ///     Gets null if this is a maintainable Object
        /// </summary>
        /// <param name="includeDifferentTypes">
        ///     Include different types.
        /// </param>
        /// <returns>
        ///     The <see cref="string" /> .
        /// </returns>
        string GetFullIdPath(bool includeDifferentTypes);
    }
}