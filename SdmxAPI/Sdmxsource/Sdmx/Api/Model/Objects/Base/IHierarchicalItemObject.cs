// -----------------------------------------------------------------------
// <copyright file="IHierarchicalItemObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Base
{
    #region Using directives

    using System.Collections.Generic;

    #endregion

    /// <summary>
    ///     IHierarchicalItemObject are ItemObjects that can contain child ItemObjects
    /// </summary>
    /// <typeparam name="T">
    ///     The item type. Which is a <see cref="IItemObject" /> based class
    /// </typeparam>
    public interface IHierarchicalItemObject<T> : IItemObject
        where T : IItemObject
    {
        /// <summary>
        ///     Gets any child items, if no children exist then an empty list is returned
        ///     <p />
        ///     <b>NOTE</b>The list is a copy so modify the returned set will not
        ///     be reflected in this instance
        /// </summary>
        //// TODO Use ReadOnlyCollection<T> to avoid copying the list.
        IList<T> Items { get; }
    }
}