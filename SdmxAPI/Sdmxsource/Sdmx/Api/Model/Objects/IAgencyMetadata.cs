// -----------------------------------------------------------------------
// <copyright file="IAgencyMetadata.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Constants;

    #endregion

    /// <summary>
    ///     The AgencyMetadata interface.
    /// </summary>
    public interface IAgencyMetadata
    {
        /// <summary>
        ///     Gets the agency id.
        /// </summary>
        string AgencyId { get; }

        /// <summary>
        ///     Gets the number agency schemes.
        /// </summary>
        int NumberAgencySchemes { get; }

        /// <summary>
        ///     Gets the number attachment constraint.
        /// </summary>
        int NumberAttachmentConstraint { get; }

        /// <summary>
        ///     Gets the number categorisations.
        /// </summary>
        int NumberCategorisations { get; }

        /// <summary>
        ///     Gets the number category schemes.
        /// </summary>
        int NumberCategorySchemes { get; }

        /// <summary>
        ///     Gets the number codelists.
        /// </summary>
        int NumberCodelists { get; }

        /// <summary>
        ///     Gets the number concept schemes.
        /// </summary>
        int NumberConceptSchemes { get; }

        /// <summary>
        ///     Gets the number content constraint Object.
        /// </summary>
        int NumberContentConstraintObject { get; }

        /// <summary>
        ///     Gets the number data consumer schemes.
        /// </summary>
        int NumberDataConsumerSchemes { get; }

        /// <summary>
        ///     Gets the number dataflows.
        /// </summary>
        int NumberDataflows { get; }

        /// <summary>
        ///     Gets the number data provider schemes.
        /// </summary>
        int NumberDataProviderSchemes { get; }

        /// <summary>
        ///     Gets the number data structures.
        /// </summary>
        int NumberDataStructures { get; }

        /// <summary>
        ///     Gets the number hierarchical codelists.
        /// </summary>
        int NumberHierarchicalCodelists { get; }

        /// <summary>
        ///     Gets the number maintainable.
        /// </summary>
        int NumberMaintainables { get; }

        /// <summary>
        ///     Gets the number metadataflows.
        /// </summary>
        int NumberMetadataflows { get; }

        /// <summary>
        ///     Gets the number metadata structure definitions.
        /// </summary>
        int NumberMetadataStructureDefinitions { get; }

        /// <summary>
        ///     Gets the number organisation unit schemes.
        /// </summary>
        int NumberOrganisationUnitSchemes { get; }

        /// <summary>
        ///     Gets the number processes.
        /// </summary>
        int NumberProcesses { get; }

        /// <summary>
        ///     Gets the number provisions.
        /// </summary>
        int NumberProvisions { get; }

        /// <summary>
        ///     Gets the number reporting taxonomies.
        /// </summary>
        int NumberReportingTaxonomies { get; }

        /// <summary>
        ///     Gets the number structure sets.
        /// </summary>
        int NumberStructureSets { get; }

        /// <summary>
        ///     The get number of maintainable.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <returns>
        ///     The <see cref="int" /> .
        /// </returns>
        int GetNumberOfMaintainables(SdmxStructureEnumType structureType);
    }
}