// -----------------------------------------------------------------------
// <copyright file="IMetadataAttributeObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     A IMetadataAttributeObject is used in a <c>IReportStructure</c> and is similar to a <c>IDimension</c>
    ///     in that it contains reference to a semantic (<c>IConceptObject</c>) and representation (codelists/free text)
    /// </summary>
    public interface IMetadataAttributeObject : IComponent
    {
        /// <summary>
        ///     Gets the max occurs.
        /// </summary>
        int? MaxOccurs { get; }

        /// <summary>
        ///     Gets any child metadata attributes
        ///     <p />
        ///     <b>NOTE</b>The list is a copy so modifying the returned list will not
        ///     be reflected in the IMetadataAttributeObject instance
        /// </summary>
        IList<IMetadataAttributeObject> MetadataAttributes { get; }

        /// <summary>
        ///     Gets the min occurs.
        /// </summary>
        int? MinOccurs { get; }

        /// <summary>
        ///     Gets the presentational.
        /// </summary>
        TertiaryBool Presentational { get; }
    }
}