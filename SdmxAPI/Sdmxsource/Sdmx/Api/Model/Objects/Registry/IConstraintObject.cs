// -----------------------------------------------------------------------
// <copyright file="IConstraintObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     The ConstraintObject interface.
    /// </summary>
    public interface IConstraintObject : IMaintainableObject
    {
        /// <summary>
        ///     Gets the structure(s) that this constraint is constraining
        /// </summary>
        /// <value> </value>
        IConstraintAttachment ConstraintAttachment { get; }

        /// <summary>
        ///     Gets the Metadata keys that this constraint defines as ones that either
        ///     do not have data, or are not allowed to have data (depending on isDefiningActualDataPresent() value)
        /// </summary>
        IConstraintDataKeySet ExcludedMetadataKeys { get; }

        /// <summary>
        ///     Gets the series keys that this constraint defines at ones that either do not have data, or are not allowed to have
        ///     data (depending on isDefiningActualDataPresent() value)
        /// </summary>
        /// <value> </value>
        IConstraintDataKeySet ExcludedSeriesKeys { get; }

        /// <summary>
        ///     Gets the Metadata keys that this constraint defines as ones that either
        ///     have data, or are allowed to have data (depending on isDefiningActualDataPresent() value)
        /// </summary>
        IConstraintDataKeySet IncludedMetadataKeys { get; }

        /// <summary>
        ///     Gets the series keys that this constraint defines at ones that either have data, or are allowed to have data
        ///     (depending on isDefiningActualDataPresent() value)
        /// </summary>
        /// <value> </value>
        IConstraintDataKeySet IncludedSeriesKeys { get; }
    }
}