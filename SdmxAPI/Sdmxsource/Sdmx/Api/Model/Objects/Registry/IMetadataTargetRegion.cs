﻿// -----------------------------------------------------------------------
// <copyright file="IMetadataTargetRegion.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public interface IMetadataTargetRegion : ISdmxStructure
    {
        /// <summary>
        ///     Gets the attributes restrictions for the metadata target region
        /// </summary>
        IList<IKeyValues> Attributes { get; }

        /// <summary>
        ///     Gets a value indicating whether the information reported is to be included or excluded
        /// </summary>
        bool IsInclude { get; }

        /// <summary>
        ///     Gets the key values restrictions for the metadata target region
        /// </summary>
        IList<IMetadataTargetKeyValues> Key { get; }

        /// <summary>
        ///     Gets the metadata target
        /// </summary>
        string MetadataTarget { get; }

        /// <summary>
        ///     Gets the report
        /// </summary>
        string Report { get; }
    }
}