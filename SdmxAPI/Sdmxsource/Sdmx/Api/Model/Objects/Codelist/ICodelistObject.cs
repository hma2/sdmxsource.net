// -----------------------------------------------------------------------
// <copyright file="ICodelistObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX Code List
    /// </summary>
    public interface ICodelistObject : IItemSchemeObject<ICode>
    {
        /// <summary>
        ///     Gets a representation of itself in a @object which can be modified, modifications to the mutable @object
        ///     are not reflected in the instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new ICodelistMutableObject MutableInstance { get; }

        /// <summary>
        ///     Gets a value indicating whether the codelist is only reporting a subset of the codes.
        ///     <p />
        ///     Partial codelists are used for dissemination purposes only not for reporting updates.
        /// </summary>
        /// <value> </value>
        new bool Partial { get; }

        /// <summary>
        ///     Gets the code with the given id, returns null if there is no code with the id provided
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ICode" /> .
        /// </returns>
        ICode GetCodeById(string id);

        /// <summary>
        ///     Gets a stub reference of itself.
        ///     <p />
        ///     A stub @object only contains Maintainable and Identifiable Attributes, not the composite Objects that are
        ///     contained within the Maintainable
        /// </summary>
        /// <returns>
        ///     The <see cref="ICodelistObject" /> .
        /// </returns>
        /// <param name="actualLocation">
        ///     the Uri indicating where the full structure can be returned from
        /// </param>
        /// <param name="isServiceUrl">
        ///     if true this Uri will be present on the serviceURL attribute, otherwise it will be treated as a structureURL
        ///     attribute
        /// </param>
        new ICodelistObject GetStub(Uri actualLocation, bool isServiceUrl);
    }
}