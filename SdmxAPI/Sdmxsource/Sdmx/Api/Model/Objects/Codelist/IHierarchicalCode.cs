// -----------------------------------------------------------------------
// <copyright file="IHierarchicalCode.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Represents an SDMX IHierarchical Code
    /// </summary>
    public interface IHierarchicalCode : IIdentifiableObject
    {
        /// <summary>
        ///     Gets the code id of the code being referenced, to be used in conjunction with the codelist alias ref
        /// </summary>
        /// <value> </value>
        string CodeId { get; }

        /// <summary>
        ///     Gets the codelist alias used to resolve this codelist reference, returns null if the reference is achieved by using
        ///     the
        ///     ICrossReference (getCodeReference() returns a value)
        /// </summary>
        /// <value> </value>
        string CodelistAliasRef { get; }

        /// <summary>
        ///     Gets the code referenced by this CodeRef, this will never be null as it will be resolved from the codelist alias
        ///     and code id
        /// </summary>
        /// <value> </value>
        ICrossReference CodeReference { get; }

        /// <summary>
        ///     Gets any child IHierarchicalCode Objects as a copy of the underlying list.
        ///     <p />
        ///     Gets an empty list if there are no child Objects
        /// </summary>
        IList<IHierarchicalCode> CodeRefs { get; }

        /// <summary>
        ///     Gets the level of this code ref @object in the hierarchy, 0 indexed
        /// </summary>
        /// <value> </value>
        int LevelInHierarchy { get; }

        /// <summary>
        ///     Gets the valid from.
        /// </summary>
        ISdmxDate ValidFrom { get; }

        /// <summary>
        ///     Gets the valid to.
        /// </summary>
        ISdmxDate ValidTo { get; }

        /// <summary>
        ///     The get level.
        /// </summary>
        /// <param name="acceptDefault">
        ///     level was not explicitly set on creation, if false, then will return the level if it was explicitly set, and will
        ///     return null if it wasn't.
        /// </param>
        /// <returns>
        ///     the level associated with this code
        /// </returns>
        ILevelObject GetLevel(bool acceptDefault);
    }
}