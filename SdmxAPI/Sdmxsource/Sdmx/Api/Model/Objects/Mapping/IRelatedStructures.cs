// -----------------------------------------------------------------------
// <copyright file="IRelatedStructures.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Represents an SDMX Related Structures Map
    /// </summary>
    public interface IRelatedStructures : ISdmxStructure
    {
        /// <summary>
        ///     Gets the category scheme ref.
        /// </summary>
        IList<ICrossReference> CategorySchemeRef { get; }

        /// <summary>
        ///     Gets the concept scheme ref.
        /// </summary>
        IList<ICrossReference> ConceptSchemeRef { get; }

        /// <summary>
        ///     Gets the data structure ref.
        /// </summary>
        IList<ICrossReference> DataStructureRef { get; }

        /// <summary>
        ///     Gets the hierarchical codelist ref.
        /// </summary>
        IList<ICrossReference> HierCodelistRef { get; }

        /// <summary>
        ///     Gets the metadata structure ref.
        /// </summary>
        IList<ICrossReference> MetadataStructureRef { get; }

        /// <summary>
        ///     Gets the org scheme ref.
        /// </summary>
        IList<ICrossReference> OrgSchemeRef { get; }
    }
}