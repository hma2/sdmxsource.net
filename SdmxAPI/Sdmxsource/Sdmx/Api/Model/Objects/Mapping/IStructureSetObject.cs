// -----------------------------------------------------------------------
// <copyright file="IStructureSetObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     Represents an SDMX Structure Set
    /// </summary>
    public interface IStructureSetObject : IMaintainableObject
    {
        /// <summary>
        ///     Gets the category scheme map list.
        /// </summary>
        IList<ICategorySchemeMapObject> CategorySchemeMapList { get; }

        /// <summary>
        ///     Gets the CodelistMap links a source and target codes from different
        ///     lists where there is a semantic equivalence between them.
        /// </summary>
        /// <value> </value>
        IList<ICodelistMapObject> CodelistMapList { get; }

        /// <summary>
        ///     Gets the ConceptSchemeMap links a source and target concepts from different schemes where there is a semantic
        ///     equivalence between them.
        /// </summary>
        /// <value> </value>
        IList<IConceptSchemeMapObject> ConceptSchemeMapList { get; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can be modified, modifications to the mutable @object
        ///     are not reflected in the instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new IStructureSetMutableObject MutableInstance { get; }

        /// <summary>
        ///     Gets the organisation scheme map list.
        /// </summary>
        IList<IOrganisationSchemeMapObject> OrganisationSchemeMapList { get; }

        /// <summary>
        ///     Gets the relatedStructures contains references to structures (key families and metadata structure definitions) and
        ///     structure usages (data flows and metadata flows) to indicate that a semantic relationship exist between them.
        ///     The details of these relationships can be found in the structure maps.
        /// </summary>
        /// <value> </value>
        IRelatedStructures RelatedStructures { get; }

        /// <summary>
        ///     Gets the StructureMap maps components from one structure to components to another structure,
        ///     and can describe how the value of the components are related.
        /// </summary>
        /// <value> </value>
        IList<IStructureMapObject> StructureMapList { get; }

        /// <summary>
        ///     Gets a stub reference of itself.
        ///     <p />
        ///     A stub @object only contains Maintainable and Identifiable Attributes, not the composite Objects that are
        ///     contained within the Maintainable
        /// </summary>
        /// <returns>
        ///     The <see cref="IStructureSetObject" /> .
        /// </returns>
        /// <param name="actualLocation">
        ///     the Uri indicating where the full structure can be returned from
        /// </param>
        /// <param name="isServiceUrl">
        ///     if true this Uri will be present on the serviceURL attribute, otherwise it will be treated as a structureURL
        ///     attribute
        /// </param>
        new IStructureSetObject GetStub(Uri actualLocation, bool isServiceUrl);
    }
}