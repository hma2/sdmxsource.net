// -----------------------------------------------------------------------
// <copyright file="IGroup.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     A Group is responsible for grouping a subset of the Dimensions in a Data Structure and giving the group an id.
    ///     <p />
    ///     A group then contains its own unique id, which can then be referenced when reporting metadata attributes.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Objects.Base.IIdentifiableObject" />
    public interface IGroup : IIdentifiableObject
    {
        /// <summary>
        ///     Gets reference to an attachment constraint which defines the cube to which the metadata can attach
        /// </summary>
        /// <value> </value>
        ICrossReference AttachmentConstraintRef { get; }

        /// <summary>
        ///     Gets the list of dimensions that this group is referencing - the list is in the order that the dimensions appear in
        ///     the <c>KeyFamlyObject</c>.
        ///     The list is mutually exclusive with getAttachmentConstraintRef(), and will have a size of 1 or more only if
        ///     getAttachmentConstraintRef()
        ///     is null, if getAttachmentConstraintRef() is not null then this method will return an empty list
        ///     <p />
        ///     <b>NOTE</b>The list is a copy so modify the returned set will not
        ///     be reflected in the IGroup instance
        /// </summary>
        /// <value> </value>
        IList<string> DimensionRefs { get; }
    }
}