// -----------------------------------------------------------------------
// <copyright file="IAttributeObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Attribute describes the definition of a data attribute, which is defined as a characteristic of an object or
    ///     entity.
    ///     <p />
    ///     This is an immutable Object - this Object can not be modified
    /// </summary>
    public interface IAttributeObject : IComponent
    {
        /// <summary>
        ///     Gets the assignmentStatus attribute indicates whether a
        ///     value must be provided for the attribute when sending documentation along with the data.
        /// </summary>
        /// <value> </value>
        string AssignmentStatus { get; }

        /// <summary>
        ///     Gets the AttachmentGroup value;
        ///     ATTRIBUTE_ATTACHMENT_LEVEL is GROUP then returns a reference to the Group id that this attribute is attached to.
        ///     Returns
        ///     null if ATTRIBUTE_ATTACHMENT_LEVEL is not GROUP
        /// </summary>
        /// <value> </value>
        string AttachmentGroup { get; }

        /// <summary>
        ///     Gets the ATTRIBUTE_ATTACHMENT_LEVEL attribute indicating the level to which the attribute is attached in
        ///     time-series formats
        ///     (generic, compact, utility data formats).
        ///     Attributes with an attachment level of Group are only available if the data is organized in groups,
        ///     and should be used appropriately, as the values may not be communicated if the data is not grouped.
        /// </summary>
        /// <value>
        ///     The attachment level.
        /// </value>
        AttributeAttachmentLevel AttachmentLevel { get; }

        /// <summary>
        ///     Gets a list of cross references to concepts that are used to define the role(s) of this attribute.
        ///     The returned list is a copy of the underlying list
        ///     <p />
        ///     Gets an empty list if this attribute does not reference any concept roles
        /// </summary>
        /// <value> </value>
        IList<ICrossReference> ConceptRoles { get; }

        /// <summary>
        ///     Gets the list of dimension ids that this attribute references, this is only relevant if ATTRIBUTE_ATTACHMENT_LEVEL
        ///     is
        ///     DIMENSION_GROUP.  The returned list is a copy of the underlying list
        ///     <p />
        ///     Gets an empty list if this attribute does not reference any dimensions
        /// </summary>
        /// <value> </value>
        IList<string> DimensionReferences { get; }

        /// <summary>
        ///     Gets a value indicating whether the getAssignmentStatus()==MANDATORY
        /// </summary>
        /// <value> </value>
        bool Mandatory { get; }

        /// <summary>
        ///     Gets the PrimaryMeasureReference value
        ///     ATTRIBUTE_ATTACHMENT_LEVEL is OBSERVATION then returns a reference to the Primary Measure id that this attribute is
        ///     attached to.  Returns
        ///     null if ATTRIBUTE_ATTACHMENT_LEVEL is not GROUP
        /// </summary>
        /// <value> </value>
        string PrimaryMeasureReference { get; }

        /// <summary>
        ///     Gets a value indicating whether this Id = TIME_FORMAT
        /// </summary>
        /// <value> </value>
        bool TimeFormat { get; }
    }
}