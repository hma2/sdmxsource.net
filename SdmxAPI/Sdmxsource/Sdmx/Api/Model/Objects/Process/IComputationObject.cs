// -----------------------------------------------------------------------
// <copyright file="IComputationObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Process
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     ComputationType describes a computation in a process.
    /// </summary>
    public interface IComputationObject : IAnnotableObject
    {
        /// <summary>
        ///     Gets the description describe the computation in any form desired by the user
        ///     (these are informational rather than machine-actionable),
        ///     and so may be supplied in multiple, parallel-language versions,
        /// </summary>
        /// <value> </value>
        IList<ITextTypeWrapper> Description { get; }

        /// <summary>
        ///     Gets the localID attribute is an optional identification for the computation within the process.
        /// </summary>
        /// <value> </value>
        string LocalId { get; }

        /// <summary>
        ///     Gets the softwareLanguage attribute holds the coding language that the software package used to perform the
        ///     computation is written in.
        /// </summary>
        /// <value> </value>
        string SoftwareLanguage { get; }

        /// <summary>
        ///     Gets the softwarePackage attribute holds the name of the software package that is used to perform the computation.
        /// </summary>
        /// <value> </value>
        string SoftwarePackage { get; }

        /// <summary>
        ///     Gets the softwareVersion attribute hold the version of the software package that is used to perform that
        ///     computation.
        /// </summary>
        /// <value> </value>
        string SoftwareVersion { get; }
    }
}