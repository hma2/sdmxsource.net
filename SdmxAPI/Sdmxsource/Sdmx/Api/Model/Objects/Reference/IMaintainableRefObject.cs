// -----------------------------------------------------------------------
// <copyright file="IMaintainableRefObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference
{
    /// <summary>
    ///     The MaintainableRefObject interface.
    /// </summary>
    public interface IMaintainableRefObject
    {
        /// <summary>
        ///     Gets the maintainable id attribute
        /// </summary>
        /// <value> </value>
        string AgencyId { get; }

        /// <summary>
        ///     Gets the maintainable id attribute
        /// </summary>
        /// <value> </value>
        string MaintainableId { get; }

        /// <summary>
        ///     Gets the version attribute
        /// </summary>
        /// <value> </value>
        string Version { get; }

        /// <summary>
        ///     Gets a value indicating whether the there is an agency Id set
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasAgencyId();

        /// <summary>
        ///     Gets a value indicating whether the there is a maintainable id set
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasMaintainableId();

        /// <summary>
        ///     Gets a value indicating whether the there is a value for version set
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasVersion();
    }
}