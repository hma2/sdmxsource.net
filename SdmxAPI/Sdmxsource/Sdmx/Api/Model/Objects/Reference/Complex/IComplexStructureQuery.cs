﻿// -----------------------------------------------------------------------
// <copyright file="IComplexStructureQuery.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex
{
    /// <summary>
    ///     A ComplexStructureQuery defines very complex search rules for finding structures
    /// </summary>
    public interface IComplexStructureQuery
    {
        /// <summary>
        ///     Gets the information about the response detail, and which referenced artefacts should be referenced in the response
        /// </summary>
        IComplexStructureQueryMetadata StructureQueryMetadata { get; }

        /// <summary>
        ///     Gets the query parameters used to identify the structure(s) begin queried for - this can not be null
        /// </summary>
        IComplexStructureReferenceObject StructureReference { get; }
    }
}