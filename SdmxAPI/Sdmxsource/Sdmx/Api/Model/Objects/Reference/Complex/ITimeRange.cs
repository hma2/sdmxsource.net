﻿// -----------------------------------------------------------------------
// <copyright file="ITimeRange.cs" company="EUROSTAT">
//   Date Created : 2013-03-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex
{
    using Org.Sdmxsource.Sdmx.Api.Model.Base;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public interface ITimeRange
    {
        /// <summary>
        ///     Gets the end date, f range is true, or the After date if range is false
        /// </summary>
        /// <value>
        ///     The end date.
        /// </value>
        ISdmxDate EndDate { get; }

        /// <summary>
        ///     Gets a value indicating whether this instance is end inclusive.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is end inclusive; otherwise, <c>false</c>.
        /// </value>
        bool IsEndInclusive { get; }

        /// <summary>
        ///     Gets a value indicating whether the start date is in range
        ///     If true then start date and end date both have a value, and the range is between the start and end dates.
        ///     If false, then only the start date or end date will be populated, if the start date is populated then it this
        ///     period refers
        ///     to dates before the start date. If the end date is populated then it refers to dates after the end date.
        /// </summary>
        bool IsRange { get; }

        /// <summary>
        ///     Gets a value indicating whether this instance is start inclusive.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is start inclusive; otherwise, <c>false</c>.
        /// </value>
        bool IsStartInclusive { get; }

        /// <summary>
        ///     Gets the Start Date - if range is true, or the Before date if range is false
        /// </summary>
        /// <value>
        ///     The start date.
        /// </value>
        ISdmxDate StartDate { get; }
    }
}