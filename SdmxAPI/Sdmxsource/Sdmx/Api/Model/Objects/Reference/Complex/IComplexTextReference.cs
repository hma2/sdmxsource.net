﻿// -----------------------------------------------------------------------
// <copyright file="IComplexTextReference.cs" company="EUROSTAT">
//   Date Created : 2013-02-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex
{
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Used to search for items with certain text fields
    /// </summary>
    public interface IComplexTextReference
    {
        /// <summary>
        ///     Gets the language to search, this can be null
        /// </summary>
        string Language { get; }

        /// <summary>
        ///     Gets the operator to apply, can not be null, defaults to EQUAL
        /// </summary>
        TextSearch Operator { get; }

        /// <summary>
        ///     Gets the search String, can not be null or empty
        /// </summary>
        string SearchParameter { get; }
    }
}