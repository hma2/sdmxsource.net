// -----------------------------------------------------------------------
// <copyright file="IRegistryObjects.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    #endregion

    /// <summary>
    ///     IRegistryObjects is a container for all registry content including the following; SdmxObjects which is a container
    ///     for all structural metadata,
    ///     RegistrationObjects and SubscriptionObjects.
    /// </summary>
    public interface IRegistryObjects
    {
        /// <summary>
        ///     Gets all the registrations in this container, returns an empty list if there are none
        /// </summary>
        /// <value> </value>
        IList<IRegistrationObject> Registrations { get; }

        /// <summary>
        ///     Gets the Sdmx objects contained in this container, returns <c>null</c> if there are none
        /// </summary>
        /// <value> </value>
        ISdmxObjects SdmxObjects { get; }

        /// <summary>
        ///     Gets all the subscriptions in this container, returns an empty list if there are none
        /// </summary>
        IList<ISubscriptionObject> Subscriptions { get; }

        /// <summary>
        ///     Gets a value indicating whether the there are IRegistrationObject contained in this container
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasRegistrationObject();

        /// <summary>
        ///     Gets a value indicating whether the there are SdmxObjects contained in this container
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasSdmxObject();

        /// <summary>
        ///     Gets a value indicating whether the there are ISubscriptionObject contained in this container
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        bool HasSubscriptionObject();
    }
}