// -----------------------------------------------------------------------
// <copyright file="ICategorisationObject.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    #endregion

    /// <summary>
    ///     Represents an SDMX Categorisation
    /// </summary>
    public interface ICategorisationObject : IMaintainableObject
    {
        /// <summary>
        ///     Gets a reference to the category that is categorising the structure - this can not be null
        /// </summary>
        ICrossReference CategoryReference { get; }

        /// <summary>
        ///     Gets a representation of itself in a @object which can be modified, modifications to the mutable @object
        ///     are not reflected in the instance of the IMaintainableObject.
        /// </summary>
        /// <value> </value>
        new ICategorisationMutableObject MutableInstance { get; }

        /// <summary>
        ///     Gets a reference to the structure that this is categorising - this can not be null
        /// </summary>
        /// <value> </value>
        ICrossReference StructureReference { get; }

        /// <summary>
        ///     Gets a stub reference of itself.
        ///     <p />
        ///     A stub @object only contains Maintainable and Identifiable Attributes, not the composite Objects that are
        ///     contained within the Maintainable
        /// </summary>
        /// <param name="actualLocation">
        ///     the URL indicating where the full structure can be returned from
        /// </param>
        /// <param name="isServiceUrl">
        ///     if true this Uri will be present on the serviceURL attribute, otherwise it will be treated as a structureURL
        ///     attribute
        /// </param>
        /// <returns>
        ///     The <see cref="ICategorisationObject" /> .
        /// </returns>
        new ICategorisationObject GetStub(Uri actualLocation, bool isServiceUrl);
    }
}