﻿// -----------------------------------------------------------------------
// <copyright file="RestService.ashx.cs" company="EUROSTAT">
//   Date Created : 2015-05-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.SdmxWebService.
//     GuideTests.SdmxWebService is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests.SdmxWebService is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.SdmxWebService.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxWebService
{
    using System;
    using System.Collections.Generic;
    using System.Net.Mime;
    using System.Text.RegularExpressions;
    using System.Web;

    using GuideTests.SdmxWebService;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Rest;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Factory;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.DataParser.Rest;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Rest;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    using DataType = Org.Sdmxsource.Sdmx.Api.Constants.DataType;

    public class RestService : IHttpHandler 
    {
        /// <summary>
        /// The map that contains the known HTTP Accept header values and the corresponding <see cref="IDataFormat"/>
        /// </summary>
        private readonly IDictionary<string, IDataFormat> _acceptToDataFormats;

        /// <summary>
        /// The map that contains the known HTTP Accept header values and the corresponding <see cref="IStructureFormat"/>
        /// </summary>
        private readonly IDictionary<string, IStructureFormat> _acceptToStructureFormats;
            
        // managers
        private readonly IRestDataQueryManager _dataQueryManager;
        private readonly IRestStructureQueryManager _restStructureQueryManager;

        // data formats
        private readonly SdmxDataFormatCore _genericV21Format = new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21));
        private readonly SdmxDataFormatCore _structureSpecificFormat =  new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21));

        /// <summary>
        /// Structure format for SDMX v2.1
        /// </summary>
        private readonly IStructureFormat _structureFormatv21 = new SdmxStructureFormat(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument));

        public RestService()
        {
            // 1.a To support additional data formats pass additional IDataWriterFactory implementations to the DataWriterManager constructor  
            IDataWriterFactory dataWriterFactory = new DataWriterFactory();
            IDataWriterManager dataWriterManager = new DataWriterManager(dataWriterFactory/*, add your factories here) */);

            // 1.b To support additional data formats pass additional IStructureWriterFactory implementations to the DataWriterManager constructor  
            IStructureWriterManager structureWriterManager = new StructureWriterManager(new SdmxStructureWriterFactory() /*, add your factories here */);

            // a map between the HTTP Accept header value and the IDataFormat
            this._acceptToDataFormats = new Dictionary<string, IDataFormat>(StringComparer.Ordinal)
                                            {
                                                { "application/vnd.sdmx.genericdata+xml", this._genericV21Format }, 
                                                { "application/xml", this._genericV21Format }, 
                                                { "application/vnd.sdmx.structurespecificdata+xml", this._structureSpecificFormat }, 
                                                /* 2. add new data formats here */
                                            };

            // a map between the HTTP Accept header value and the IStructureFormat
            this._acceptToStructureFormats = new Dictionary<string, IStructureFormat>(StringComparer.Ordinal)
                                                 {
                                                     { "application/vnd.sdmx.structure+xml", this._structureFormatv21 }, 
                                                     { "application/xml", this._structureFormatv21 }
                                                    
                                                     /* 2. add new structure formats here */
                                                 };
            
            // load the structures
            ISdmxObjects objects;
            IReadableDataLocationFactory dataLocationFactory = new ReadableDataLocationFactory();
            IStructureParsingManager structureParsingManager = new StructureParsingManager();

            using (IReadableDataLocation structureLocation = dataLocationFactory.GetReadableDataLocation(HttpContext.Current.Server.MapPath("~/App_Data/structures.xml")))
            {
                IStructureWorkspace structureWorkspace = structureParsingManager.ParseStructures(structureLocation);
                objects = structureWorkspace.GetStructureObjects(false);
            }

            ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(objects);

            _dataQueryManager = new RestDataQueryManager(new SampleDataRetrieval(), dataWriterManager, retrievalManager);
            _restStructureQueryManager = new RestStructureQueryManager(structureWriterManager, retrievalManager);
        }

        public void ProcessRequest(HttpContext context) {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            string restUrl = Regex.Replace(request.Url.PathAndQuery, @"^\/RestService.ashx", string.Empty);
            if (restUrl.StartsWith("/data/"))
            {
                IRestDataQuery query = new RESTDataQueryCore(restUrl);

                // get the first known Accept Header value and IDataFormat from the request
                KeyValuePair<string, IDataFormat> dataFormat = GetDataFormat(request);

                // for simplicity use the first known accept header
                response.ContentType = dataFormat.Key;
                response.AddHeader("Content-Disposition", new ContentDisposition { FileName = "data.xml", Inline = true }.ToString());
                this._dataQueryManager.ExecuteQuery(query, dataFormat.Value, response.OutputStream);

            }
            else
            {
                IRestStructureQuery query = new RESTStructureQueryCore(restUrl);
                KeyValuePair<string, IStructureFormat> format = GetStructureFormat(request);

                // for simplicity use the first known accept header
                response.ContentType = format.Key;
                response.AddHeader("Content-Disposition", new ContentDisposition { FileName = "structure.xml", Inline = true }.ToString());
                this._restStructureQueryManager.GetStructures(query, response.OutputStream, format.Value);
            }

            response.End();
        }

        public bool IsReusable { get { return true; } }

        private KeyValuePair<string, IStructureFormat> GetStructureFormat(HttpRequest request)
        {
            string[] accept = request.AcceptTypes;
            if (accept != null)
            {
                for (int i = 0; i < accept.Length; i++)
                {
                    var acceptHeader = accept [i];
                    IStructureFormat format;
                    if (this._acceptToStructureFormats.TryGetValue(acceptHeader, out format))
                    {
                        return new KeyValuePair<string, IStructureFormat>(acceptHeader, format);
                    }
                }
            }

            return new KeyValuePair<string, IStructureFormat>("application/vnd.sdmx.structure+xml", this._structureFormatv21);
        }

        private KeyValuePair<string, IDataFormat> GetDataFormat(HttpRequest request) 
        {
            string[] accept = request.AcceptTypes;
            if (accept != null) {
                for (int i = 0; i < accept.Length; i++) 
                {
                    var acceptHeader = accept [i];
                    IDataFormat format;
                    if (this._acceptToDataFormats.TryGetValue(acceptHeader, out format))
                    {
                        return new KeyValuePair<string, IDataFormat>(acceptHeader, format);
                    }
                }
            }

            return new KeyValuePair<string, IDataFormat>("application/vnd.sdmx.genericdata+xml", this._genericV21Format);
        }
    }
}