﻿// -----------------------------------------------------------------------
// <copyright file="MessagePosition.cs" company="EUROSTAT">
//   Date Created : 2014-07-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxEdiParser.
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Constants
{
    /// <summary>
    ///     The message position.
    /// </summary>
    public enum MessagePosition
    {
        /// <summary>
        ///     The message identification.
        /// </summary>
        MessageIdentification, 

        /// <summary>
        ///     The message function.
        /// </summary>
        MessageFunction, 

        /// <summary>
        ///     The codelist maintenance agency.
        /// </summary>
        CodelistMaintenanceAgency, 

        /// <summary>
        ///     The receiver identification.
        /// </summary>
        ReceiverIdentification, 

        /// <summary>
        ///     The sender identification.
        /// </summary>
        SenderIdentification, 

        /// <summary>
        ///     The concept identifier.
        /// </summary>
        ConceptIdentifier, 

        /// <summary>
        ///     The concept name.
        /// </summary>
        ConceptName, 

        /// <summary>
        ///     The codelist identifier.
        /// </summary>
        CodelistIdentifier, 

        /// <summary>
        ///     The code value.
        /// </summary>
        CodeValue, 

        /// <summary>
        ///     The code description.
        /// </summary>
        CodeDescription, 

        /// <summary>
        ///     The key family identifier.
        /// </summary>
        KeyFamilyIdentifier, 

        /// <summary>
        ///     The key family name.
        /// </summary>
        KeyFamilyName, 

        /// <summary>
        ///     The dimension.
        /// </summary>
        Dimension, 

        /// <summary>
        ///     The attribute.
        /// </summary>
        Attribute, 

        /// <summary>
        ///     The codelist reference.
        /// </summary>
        CodelistReference
    }
}