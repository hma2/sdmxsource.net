﻿// -----------------------------------------------------------------------
// <copyright file="IEdiStructureDocument.cs" company="EUROSTAT">
//   Date Created : 2014-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxEdiParser.
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Document
{
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    ///     An EDI Structure Document provides a view on the structural part of an EDI Document
    /// </summary>
    public interface IEdiStructureDocument
    {
        /// <summary>
        ///     Gets the SDMX objects from the document.
        /// </summary>
        /// <value>
        ///     The SDMX objects.
        /// </value>
        ISdmxObjects SdmxObjects { get; }
    }
}