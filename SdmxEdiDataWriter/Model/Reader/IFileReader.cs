// -----------------------------------------------------------------------
// <copyright file="IFileReader.cs" company="EUROSTAT">
//   Date Created : 2014-07-23
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxEdiParser.
//     SdmxEdiParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxEdiParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxEdiParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.EdiParser.Model.Reader
{
    using System;
    using System.IO;

    /// <summary>
    ///     The FileReader interface.
    /// </summary>
    public interface IFileReader : IDisposable
    {
        /// <summary>
        ///     Gets the current line.  minus the prefix, if the prefix is unknown then an exception is thrown
        /// </summary>
        /// <value>
        ///     The current line.
        /// </value>
        string CurrentLine { get; }

        /// <summary>
        ///     Gets a value indicating whether the reader has flagged this to move back a line, as moving back a line does not
        ///     actually change the current line
        /// </summary>
        /// <value>
        ///     <c>true</c> if the reader has flagged this to move back a line, as moving back a line does not actually change the
        ///     current line; otherwise, <c>false</c>.
        /// </value>
        bool IsBackLine { get; }

        /// <summary>
        ///     Gets the line number of the current line, the first line being '1'.
        /// </summary>
        /// <value>
        ///     The line number.
        /// </value>
        int LineNumber { get; }

        /// <summary>
        ///     Gets the next line. Move the file pointer to the next line and returns that line.
        ///     Null is returned if there is no next line.
        ///     This method is almost the same as calling <see cref="MoveNext" /> followed by <see cref="CurrentLine" /> the
        ///     difference is if there is no next line,
        ///     the call to <see cref="CurrentLine" /> will the same result as it did prior to this call
        /// </summary>
        /// <value>
        ///     The next line.
        /// </value>
        string NextLine { get; }

        /// <summary>
        ///     Closes this instance. Close the reader and any resources associated with the reader
        /// </summary>
        void Close();

        /// <summary>
        ///     Copies the EDI file to the specified <paramref name="output" />
        /// </summary>
        /// <param name="output">
        ///     The output.
        /// </param>
        void CopyToStream(Stream output);

        /// <summary>
        ///     Moves the reader back a single line.  This can not be called to iterate backwards - it will only move back one line
        /// </summary>
        void MoveBackLine();

        /// <summary>
        ///     Moves the file pointer to the next line.
        /// </summary>
        /// <returns>false if there is no next line.</returns>
        bool MoveNext();

        /// <summary>
        ///     Move the reader back to the start of the document.
        /// </summary>
        void ResetReader();
    }
}