﻿// -----------------------------------------------------------------------
// <copyright file="TestSdmxException.cs" company="EUROSTAT">
//   Date Created : 2014-04-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxApiTests.
//     SdmxApiTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxApiTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxApiTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxApiTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Util.ResourceBundle;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Test unit for <see cref="SdmxException"/>
    /// </summary>
    [TestFixture]
    public class TestSdmxDateConverter
    {

        /// <summary>
        /// Test unit for <see cref="SdmxException.Message"/> 
        /// </summary>
        [Test]
        public void TestSdmxDateConverterGregorian()
        {
            RESTDataQueryCore Q = new RESTDataQueryCore("data/", new Dictionary<string, string>() {
                {"startPeriod","2000-Q1" },
                {"endPeriod","2000-Q4" }
            });
            RESTDataQueryCore M = new RESTDataQueryCore("data/", new Dictionary<string, string>() {
                {"startPeriod","2123-M03" },
                {"endPeriod","2123-M06" }
            });
            RESTDataQueryCore A = new RESTDataQueryCore("data/", new Dictionary<string, string>() {
                {"startPeriod","2001-A1" },
                {"endPeriod","2023-A1" }
            });


            Assert.That(Q.StartPeriod.Date == new DateTime(2000, 01, 01));
            Assert.That(Q.EndPeriod.Date == new DateTime(2000, 10, 01));

            Assert.That(M.StartPeriod.Date == new DateTime(2123, 03, 01));
            Assert.That(M.EndPeriod.Date == new DateTime(2123, 06, 01));

            Assert.That(A.StartPeriod.Date == new DateTime(2001, 01, 01));
            Assert.That(A.EndPeriod.Date == new DateTime(2023, 01, 01));
        }
    }
}