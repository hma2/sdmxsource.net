﻿// -----------------------------------------------------------------------
// <copyright file="IReportingPeriodManager.cs" company="EUROSTAT">
//   Date Created : 2018-02-21
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    using Api.Constants;
    using Api.Engine;

    /// <summary>
    /// Reporting period manager
    /// </summary>
    public interface IReportingPeriodManager
    {
        /// <summary>
        /// Gets the engine.
        /// </summary>
        /// <param name="isReportingPeriod">if set to <c>true</c> [is reporting period].</param>
        /// <param name="sdmxSchemaVersion">The SDMX schema version.</param>
        /// <param name="hasTranscoding">if set to <c>true</c> [has transcoding].</param>
        /// <param name="engine">The engine.</param>
        /// <returns></returns>
        IDataWriterEngine GetEngine(bool isReportingPeriod, SdmxSchemaEnumType sdmxSchemaVersion, bool hasTranscoding, IDataWriterEngine engine);
    }
}