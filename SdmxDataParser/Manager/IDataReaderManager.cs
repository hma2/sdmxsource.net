﻿// -----------------------------------------------------------------------
// <copyright file="IDataReaderManager.cs" company="EUROSTAT">
//   Date Created : 2013-05-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    #region Using directives

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;

    #endregion

    /// <summary>
    ///     Used to Obtain a DataReaderEngine capable of reading the information contained in a ReadableDataLocation
    /// </summary>
    public interface IDataReaderManager
    {
        /// <summary>
        ///     Obtains a DataReaderEngine that is capable of reading the data which is exposed via the ReadableDataLocation
        /// </summary>
        /// <param name="sourceData">
        ///     SourceData - giving access to an InputStream of the data
        /// </param>
        /// <param name="dsd">
        ///     Describes the data in terms of the dimensionality
        /// </param>
        /// <param name="dataflowObject">
        ///     The data flow object (optional)
        /// </param>
        /// <returns>
        ///     The data reader engine
        /// </returns>
        /// throws SdmxNotImplementedException if ReadableDataLocation or DataStructureBean is null, also if additioanlInformation is not of the expected type
        IDataReaderEngine GetDataReaderEngine(
            IReadableDataLocation sourceData, 
            IDataStructureObject dsd, 
            IDataflowObject dataflowObject);

        /// <summary>
        ///     Obtains a DataReaderEngine that is capable of reading the data which is exposed via the ReadableDataLocation
        /// </summary>
        /// <param name="sourceData">
        ///     SourceData - giving access to an InputStream of the data
        /// </param>
        /// <param name="retrievalManager">
        ///     RetrievalManager - used to obtain the DataStructure(s) that describe the data
        /// </param>
        /// <returns>
        ///     The data reader engine
        /// </returns>
        /// throws SdmxNotImplementedException if ReadableDataLocation or DataStructureBean is null, also if additioanlInformation is not of the expected type
        IDataReaderEngine GetDataReaderEngine(
            IReadableDataLocation sourceData, 
            ISdmxObjectRetrievalManager retrievalManager);
    }
}