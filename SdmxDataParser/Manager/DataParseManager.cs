﻿// -----------------------------------------------------------------------
// <copyright file="DataParseManager.cs" company="EUROSTAT">
//   Date Created : 2016-09-07
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.DataParser.Manager
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Transform;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Log;

    /// <summary>
    /// The default implementation of <see cref="IDataParseManager"/>
    /// </summary>
    public class DataParseManager : IDataParseManager
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataParseManager));

        /// <summary>
        /// The _data reader manager.
        /// </summary>
        private readonly IDataReaderManager _dataReaderManager;

        /// <summary>
        /// The _data reader writer transform.
        /// </summary>
        private readonly IDataReaderWriterTransform _dataReaderWriterTransform;

        /// <summary>
        /// The _data writer manager.
        /// </summary>
        private readonly IDataWriterManager _dataWriterManager;

        /// <summary>
        /// The _structure parsing manager.
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager;

        /// <summary>
        /// The _writeable data location factory.
        /// </summary>
        private readonly IWriteableDataLocationFactory _writeableDataLocationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataParseManager" /> class.
        /// </summary>
        /// <param name="dataReaderManager">The data reader manager.</param>
        /// <param name="dataReaderWriterTransform">The data reader writer transform.</param>
        /// <param name="dataWriterManager">The data writer manager.</param>
        /// <param name="structureParsingManager">The structure parsing manager.</param>
        /// <param name="writeableDataLocationFactory">The writeable data location factory.</param>
        public DataParseManager(
            IDataReaderManager dataReaderManager,
            IDataReaderWriterTransform dataReaderWriterTransform,
            IDataWriterManager dataWriterManager,
            IStructureParsingManager structureParsingManager,
            IWriteableDataLocationFactory writeableDataLocationFactory)
        {
            this._dataReaderManager = dataReaderManager;
            this._dataReaderWriterTransform = dataReaderWriterTransform;
            this._dataWriterManager = dataWriterManager;
            this._structureParsingManager = structureParsingManager;
            this._writeableDataLocationFactory = writeableDataLocationFactory;
        }

        /// <summary>
        /// Performs the transform.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="dsdLocation">The DSD location.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="dataFormat">The data format.</param>
        public void PerformTransform(IReadableDataLocation sourceData, IReadableDataLocation dsdLocation, Stream outputStream, IDataFormat dataFormat)
        {
            var beans = this._structureParsingManager.ParseStructures(dsdLocation).GetStructureObjects(false);
            this.PerformTransform(sourceData, outputStream, dataFormat, new InMemoryRetrievalManager(beans));
        }

        /// <summary>
        /// Performs the transform.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="dsd">The DSD.</param>
        /// <param name="flow">The flow.</param>
        public void PerformTransform(IReadableDataLocation sourceData, Stream outputStream, IDataFormat dataFormat, IDataStructureObject dsd, IDataflowObject flow)
        {
            LoggingUtil.Debug(_log, "Perform transform request");
            using (outputStream)
            using (var dre = this._dataReaderManager.GetDataReaderEngine(sourceData, dsd, flow))
            using (var dwe = this._dataWriterManager.GetDataWriterEngine(dataFormat, outputStream))
            {
                this._dataReaderWriterTransform.CopyToWriter(dre, dwe, true, true);
            }
        }

        /// <summary>
        /// Performs the transform.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="beanRetrievalManager">The bean retrieval manager.</param>
        public void PerformTransform(IReadableDataLocation sourceData, Stream outputStream, IDataFormat dataFormat, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            using (outputStream)
            {
                LoggingUtil.Debug(_log, "Perform transform request");

                using (var dwe = this._dataWriterManager.GetDataWriterEngine(dataFormat, outputStream))
                using (var dre = this._dataReaderManager.GetDataReaderEngine(sourceData, beanRetrievalManager))
                {
                    this._dataReaderWriterTransform.CopyToWriter(dre, dwe, true, true);
                }
            }
        }

        /// <summary>
        /// Performs the transform.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="beanRetrievalManager">The bean retrieval manager.</param>
        /// <returns>
        /// The <see cref="IReadableDataLocation" />.
        /// </returns>
        public IReadableDataLocation PerformTransform(IReadableDataLocation sourceData, IDataFormat dataFormat, ISdmxObjectRetrievalManager beanRetrievalManager)
        {
            IWriteableDataLocation tmpDataLocation = null;

            try
            {
                tmpDataLocation = this._writeableDataLocationFactory.GetTemporaryWriteableDataLocation();
                LoggingUtil.Debug(_log, "URI generated to write transformed dataset to :" + tmpDataLocation);
                using (var outputStream = tmpDataLocation.OutputStream)
                {
                    this.PerformTransform(sourceData, outputStream, dataFormat, beanRetrievalManager);
                }

                return tmpDataLocation;
            }
            catch (Exception)
            {
                if (tmpDataLocation != null)
                {
                    tmpDataLocation.Close();
                }

                throw;
            }
        }

        /// <summary>
        /// Performs the transform.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="dsd">The DSD.</param>
        /// <param name="flow">The flow.</param>
        /// <returns>
        /// The <see cref="IReadableDataLocation" />.
        /// </returns>
        public IReadableDataLocation PerformTransform(IReadableDataLocation sourceData, IDataFormat dataFormat, IDataStructureObject dsd, IDataflowObject flow)
        {
            IWriteableDataLocation tmpBuffer = null;
            try
            {
                tmpBuffer = this._writeableDataLocationFactory.GetTemporaryWriteableDataLocation();

                LoggingUtil.Debug(_log, "URI generated to write transformed dataset to :" + tmpBuffer);
                using (var outputStream = tmpBuffer.OutputStream)
                {
                    this.PerformTransform(sourceData, outputStream, dataFormat, dsd, flow);
                }
            }
            catch (Exception)
            {
                if (tmpBuffer != null)
                {
                    tmpBuffer.Close();
                }

                throw;
            }

            return tmpBuffer;
        }

        /// <summary>
        /// Performs the transform and split.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="dsdLocation">The DSD location.</param>
        /// <param name="dataFormat">The data format.</param>
        /// <returns>
        /// The <see cref="IList{IReadableDataLocation}" />.
        /// </returns>
        public IList<IReadableDataLocation> PerformTransformAndSplit(IReadableDataLocation sourceData, IReadableDataLocation dsdLocation, IDataFormat dataFormat)
        {
            var beans = this._structureParsingManager.ParseStructures(dsdLocation).GetStructureObjects(false);
            ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(beans);
            return this.PerformTransformAndSplit(sourceData, dataFormat, retrievalManager);
        }

        /// <summary>
        /// Performs the transform and split.
        /// </summary>
        /// <param name="sourceData">The source data.</param>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>
        /// The <see cref="IList{IReadableDataLocation}" />.
        /// </returns>
        public IList<IReadableDataLocation> PerformTransformAndSplit(IReadableDataLocation sourceData, IDataFormat dataFormat, ISdmxObjectRetrievalManager retrievalManager)
        {
            List<IReadableDataLocation> returnList = new List<IReadableDataLocation>();
            using (var dre = this._dataReaderManager.GetDataReaderEngine(sourceData, retrievalManager))
            {
                while (dre.MoveNextDataset())
                {
                    var tmpDataLocation = this._writeableDataLocationFactory.GetTemporaryWriteableDataLocation();
                    using (var outPutStream = tmpDataLocation.OutputStream)
                    {
                        var dwe = this._dataWriterManager.GetDataWriterEngine(dataFormat, outPutStream);
                        this._dataReaderWriterTransform.CopyDatasetToWriter(dre, dwe, null, true, int.MinValue, DateTime.MinValue, DateTime.MinValue, true, true);
                    }

                    returnList.Add(tmpDataLocation);
                }
            }

            return returnList;
        }
    }
}