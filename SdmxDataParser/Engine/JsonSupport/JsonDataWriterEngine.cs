// -----------------------------------------------------------------------
// <copyright file="JsoncDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.IO;
    using System.Text;

    using log4net;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// This class is responsible for writing SDMX-JSON data messages.
    /// </summary>
    public class JsonDataWriterEngine : IDataWriterEngine
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The _json generator.
        /// </summary>
        private readonly JsonGenerator _jsonGenerator;

        /// <summary>
        /// The _super object retrieval manager.
        /// </summary>
        private readonly ISdmxSuperObjectRetrievalManager _superObjectRetrievalManager;

        /// <summary>
        /// The _translator.
        /// </summary>
        private readonly ITranslator _translator;

        /// <summary>
        /// The _disposed.
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// The _header.
        /// </summary>
        private IHeader _header;

        /// <summary>
        /// The _header written.
        /// </summary>
        private bool _headerWritten;

        /// <summary>
        /// The _internal writer.
        /// </summary>
        private AbstractJsonDataWriter _internalWriter;

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonDataWriterEngine"/> class.
        /// </summary>
        /// <param name="outStream">
        /// The output stream.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        /// <param name="encoding">
        /// The encoding.
        /// </param>
        public JsonDataWriterEngine(Stream outStream, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, ITranslator translator, Encoding encoding)
        {
            if (outStream == null)
            {
                throw new ArgumentNullException("outStream");
            }

            if (superObjectRetrievalManager == null)
            {
                throw new ArgumentNullException("superObjectRetrievalManager");
            }

            if (translator == null)
            {
                throw new ArgumentNullException("translator");
            }

            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }

            this._jsonGenerator = new JsonGenerator(new StreamWriter(outStream, encoding));
            this._jsonGenerator.WriteStartObject();

            this._superObjectRetrievalManager = superObjectRetrievalManager;
            this._translator = translator;
        }

        /// <summary>
        /// Writes the closing tags along with the structure.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public void Close(params IFooterMessage[] footer)
        {
            this.Dispose();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Starts the dataset.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartDataset(IDataflowObject dataflow, IDataStructureObject dataStructure, IDatasetHeader header, params IAnnotation[] annotations)
        {
            if (!this._headerWritten)
            {
                // Create the appropriate internal writer.
                string dimensionAtObservation = null;

                if (header != null && header.DataStructureReference != null)
                {
                    dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
                }

                if (!ObjectUtil.ValidString(dimensionAtObservation))
                {
                    dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
                }

                if (dimensionAtObservation == DatasetStructureReference.AllDimensions)
                {
                    this._internalWriter = new FlatDataWriter(this._jsonGenerator, this._superObjectRetrievalManager, this._translator);
                }
                else
                {
                    this._internalWriter = new SeriesDataWriter(this._jsonGenerator, this._superObjectRetrievalManager, this._translator);
                }

                this.WriteDocumentHeader();
                this._headerWritten = true;
            }
            else
            {
                // End previous dataset.
                this._jsonGenerator.WriteEndObject();
            }

            this._internalWriter.StartDataset(dataflow, dataStructure, header, annotations);
        }

        /// <summary>
        /// Start a group.
        /// </summary>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this._internalWriter.StartGroup(groupId, annotations);
        }

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartSeries(params IAnnotation[] annotations)
        {
            this._internalWriter.StartSeries(annotations);
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteAttributeValue(string id, string valueRen)
        {
            this._internalWriter.WriteAttributeValue(id, valueRen);
        }

        /// <summary>
        /// Writes the group key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            this._internalWriter.WriteGroupKeyValue(id, valueRen);
        }

        /// <summary>
        /// Writes the header.
        /// </summary>
        /// <param name="header">
        /// The header.
        /// </param>
        public void WriteHeader(IHeader header)
        {
            this._header = header;

            // Don't attempt to write the header yet, since the internalWriter object has not been set up.
            // The header will be written by the JSON DataWriterEngines before the first Dataset is written.
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this._internalWriter.WriteObservation(obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observationConceptId">
        /// The observation concept id.
        /// </param>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this._internalWriter.WriteObservation(observationConceptId, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsTime">
        /// The observation time.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="sdmxSwTimeFormat">
        /// The SDMX time format.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            this._internalWriter.WriteObservation(obsTime, obsValue, sdmxSwTimeFormat, annotations);
        }

        /// <summary>
        /// Writes the series key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteSeriesKeyValue(string id, string valueRen)
        {
            this._internalWriter.WriteSeriesKeyValue(id, valueRen);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposable">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposable)
        {
            if (!this._disposed)
            {
                if (disposable)
                {
                    if (this._internalWriter != null)
                    {
                        this._internalWriter.Close();
                    }

                    if (this._jsonGenerator != null)
                    {
                        this._jsonGenerator.Close();
                    }
                }

                this._disposed = true;
            }
        }

        /// <summary>
        /// The write contact details.
        /// </summary>
        /// <param name="partyObject">
        /// The party object.
        /// </param>
        private void WriteContactDetails(IParty partyObject)
        {
            string senderId = "unknown";
            string senderName = "unknown";

            if (partyObject != null)
            {
                senderId = partyObject.Id;

                if (ObjectUtil.ValidCollection(partyObject.Name))
                {
                    senderName = this._internalWriter.Translator.GetTranslation(partyObject.Name);
                }
            }

            this._jsonGenerator.WriteStringField("id", senderId);
            this._jsonGenerator.WriteStringField("name", senderName);
        }

        /// <summary>
        /// The write document header.
        /// </summary>
        private void WriteDocumentHeader()
        {
            string id;
            DateTime prepared;
            bool isTest;

            if (this._header != null)
            {
                id = this._header.Id;
                prepared = this._header.Prepared.GetValueOrDefault(DateTime.Now);
                isTest = this._header.Test;
            }
            else
            {
                id = Guid.NewGuid().ToString();
                prepared = DateTime.Now;
                isTest = false;
            }

            _log.Debug("{Header}");
            this._jsonGenerator.WriteObjectFieldStart("header");
            this._jsonGenerator.WriteStringField("id", id);
            this._jsonGenerator.WriteStringField("prepared", DateUtil.FormatDate(prepared, TimeFormatEnumType.DateTime));
            this._jsonGenerator.WriteBooleanField("test", isTest);

            this.WriteSender();
            this.WriteReceiver();

            this._jsonGenerator.WriteEndObject();
            _log.Debug("{/Header}");

            this._headerWritten = true;
        }

        /// <summary>
        /// The write receiver.
        /// </summary>
        private void WriteReceiver()
        {
            if (this._header == null || !ObjectUtil.ValidCollection(this._header.Receiver))
            {
                // Do not write receiver since it's empty.
                return;
            }

            _log.Debug("{receiver}");

            this._jsonGenerator.WriteObjectFieldStart("receiver");

            // Simply use the first receiver.
            IParty receiver = this._header.Receiver[0];
            this.WriteContactDetails(receiver);

            _log.Debug("{/receiver}");
            this._jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// The write sender.
        /// </summary>
        private void WriteSender()
        {
            _log.Debug("{sender}");
            this._jsonGenerator.WriteObjectFieldStart("sender");

            IParty sender = null;
            if (this._header != null)
            {
                sender = this._header.Sender;
            }

            this.WriteContactDetails(sender);

            _log.Debug("{/sender}");
            this._jsonGenerator.WriteEndObject();
        }
    }
}