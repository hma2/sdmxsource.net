﻿// -----------------------------------------------------------------------
// <copyright file="DatasetInfoDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2016-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;

    /// <summary>
    /// DatasetInfoDataWriterEngine class.
    /// </summary>
    public class DatasetInfoDataWriterEngine : DecoratedDataWriterEngine
    {
        /// <summary>
        /// The _codes for component map.
        /// </summary>
        private readonly Dictionary<string, IList<string>> _codesForComponentMap = new Dictionary<string, IList<string>>();
        protected Dictionary<string, int> _dimensionOrder;
        /// <summary>
        /// Initializes a new instance of the <see cref="DatasetInfoDataWriterEngine"/> class. 
        /// Initializes a new instance of <see cref="DatasetInfoDataWriterEngine"/> class.
        /// </summary>
        /// <param name="superObjectRetrievalManager">
        /// The SuperObjectRetrievalManager.
        /// </param>
        /// <exception cref="ArgumentException">
        /// <paramref name="superObjectRetrievalManager"/> is null
        /// </exception>
        public DatasetInfoDataWriterEngine(ISdmxSuperObjectRetrievalManager superObjectRetrievalManager)
            : base(null)
        {
            if (superObjectRetrievalManager == null)
            {
                throw new ArgumentException("Cannot construct DatasetInfoDataWriterEngine, missing required argument: ISdmxSuperObjectRetrievalManager");
            }

            this.SuperObjectRetrievalManager = superObjectRetrievalManager;
        }

        /// <summary>
        /// Gets the current data structure that the writer engine is outputting data for.
        /// </summary>
        protected IDataStructureSuperObject CurrentDsdSuperObject { get; private set; }

        /// <summary>
        /// Gets the SuperObjectRetrievalManager.
        /// </summary>
        protected ISdmxSuperObjectRetrievalManager SuperObjectRetrievalManager { get; private set; }

        /// <summary>
        /// Gets the reported index.
        /// </summary>
        /// <param name="concept">
        /// The concept.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// The reported index.
        /// </returns>
        public int GetReportedIndex(string concept, string code)
        {
            return this._codesForComponentMap[concept].IndexOf(code);
        }

        /// <summary>
        /// Gets the reported values.
        /// </summary>
        /// <param name="componentId">
        /// The component id.
        /// </param>
        /// <returns>
        /// The reported values.
        /// </returns>
        public IList<string> GetReportedValues(string componentId)
        {
            return this._codesForComponentMap[componentId];
        }

        /// <summary>
        /// Returns dimension collection, time dimension last
        /// </summary>
        /// <returns>Returns dimension collection, time dimension last</returns>
        protected IEnumerable<IDimensionSuperObject> DimensionsOrdered()
        {
            IDimensionSuperObject timeDimension = null;

            foreach (var dimension in this.CurrentDsdSuperObject.Dimensions)
            {
                // Always write the Time Dimension at the end.
                if (dimension.TimeDimension)
                {
                    timeDimension = dimension;
                    continue;
                }

                yield return dimension;
            }

            if (timeDimension != null)
                yield return timeDimension;
        }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dsd">
        /// The data structure definition.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartDataset(IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header, params IAnnotation[] annotations)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException("dsd");
            }

            base.StartDataset(dataflow, dsd, header, null);

            this.CurrentDsdSuperObject  = this.SuperObjectRetrievalManager.GetDataStructureSuperObject(dsd.AsReference.MaintainableReference);
            this._dimensionOrder        = this.DimensionsOrdered()
                                            .Select((dim, index) => new KeyValuePair<string, int>(dim.Id, index))
                                            .ToDictionary(kvp=>kvp.Key, kvp=>kvp.Value);

            this.Dataflow = dataflow;

            foreach (var currentComponent in this.CurrentDsdSuperObject.Components)
            {
                this._codesForComponentMap.Add(currentComponent.Id, new List<string>());
            }

            if (!this._codesForComponentMap.ContainsKey(DimensionObject.TimeDimensionFixedId))
            {
                this._codesForComponentMap.Add(DimensionObject.TimeDimensionFixedId, new List<string>());
            }
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="attributeId">
        /// The attribute id.
        /// </param>
        /// <param name="attributeValue">
        /// The attribute value.
        /// </param>
        public override void WriteAttributeValue(string attributeId, string attributeValue)
        {
            base.WriteAttributeValue(attributeId, attributeValue);
            this.StoreComponentValue(attributeId, attributeValue);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observationConceptId">
        /// The observationConceptId.
        /// </param>
        /// <param name="obsIdValue">
        /// The obsIdValue.
        /// </param>
        /// <param name="obsValue">
        /// The obsValue.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void WriteObservation(string observationConceptId, string obsIdValue, string obsValue, params IAnnotation[] annotations)
        {
            this.StoreComponentValue(observationConceptId, obsIdValue);
            base.WriteObservation(observationConceptId, obsIdValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="date">
        /// The date.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void WriteObservation(string date, string value, params IAnnotation[] annotations)
        {
            base.WriteObservation(date, value, annotations);
        }

        /// <summary>
        /// Writes the series key value.
        /// </summary>
        /// <param name="dimensionId">
        /// The dimension id.
        /// </param>
        /// <param name="dimensionValue">
        /// The dimension value.
        /// </param>
        public override void WriteSeriesKeyValue(string dimensionId, string dimensionValue)
        {
            base.WriteSeriesKeyValue(dimensionId, dimensionValue);
            this.StoreComponentValue(dimensionId, dimensionValue);
        }

        /// <summary>
        /// The store component value.
        /// </summary>
        /// <param name="componentId">
        /// The component id.
        /// </param>
        /// <param name="componentValue">
        /// The component value.
        /// </param>
        private void StoreComponentValue(string componentId, string componentValue)
        {
            if (this._codesForComponentMap.ContainsKey(componentId) && !this._codesForComponentMap[componentId].Contains(componentValue))
            {
                this._codesForComponentMap[componentId].Add(componentValue);
            }
        }
    }
}