﻿// -----------------------------------------------------------------------
// <copyright file="SeriesDataWriter.cs" company="EUROSTAT">
//   Date Created : 2016-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using log4net;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Model;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// SeriesDataWriter class.
    /// </summary>
    public class SeriesDataWriter : AbstractJsonDataWriter
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The _series array started.
        /// </summary>
        private bool _seriesArrayStarted;

        /// <summary>
        /// Initializes a new instance of the <see cref="SeriesDataWriter"/> class.
        /// </summary>
        /// <param name="jsonGenerator">
        /// The jsonGenerator.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        public SeriesDataWriter(JsonGenerator jsonGenerator, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, ITranslator translator)
            : base(jsonGenerator, superObjectRetrievalManager, translator)
        {
        }

        /// <summary>
        /// Writes the closing tags (only if needed) for the
        /// - observations object in the current series object
        /// - current series object
        /// - series object in the current dataSet object
        /// - current dataSet object
        /// - dataSets array
        /// along with the structure.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public override void Close(params IFooterMessage[] footer)
        {
            base.Close(footer);

            try
            {
                this.JsonGenerator.WriteEnd();
                this.JsonGenerator.WriteEnd();

                if (this.CurrentPosition == DataPosition.SeriesKey || this.CurrentPosition == DataPosition.SeriesKeyAttribute || this.CurrentPosition == DataPosition.Observation
                    || this.CurrentPosition == DataPosition.ObservationAttribute)
                {
                    this.JsonGenerator.WriteEnd();

                    if (this.CurrentPosition == DataPosition.Observation || this.CurrentPosition == DataPosition.ObservationAttribute)
                    {
                        this.JsonGenerator.WriteEnd();
                        this.JsonGenerator.WriteEnd();
                    }
                }

                this.WriteStructure();
            }
            catch (IOException e)
            {
                throw new ArgumentException("Illegal argument", e);
            }
            finally
            {
                this.JsonGenerator.Close();
            }
        }

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">The annotations.</param>
        public override void StartSeries(params IAnnotation[] annotations)
        {
            base.StartSeries(annotations);

            if (!this._seriesArrayStarted)
            {
                _log.Debug("{series}");
                this.JsonGenerator.WriteObjectFieldStart("series");

                this._seriesArrayStarted = true;
            }
        }

        /// <summary>
        /// Writes the attributes object.
        /// </summary>
        protected override void WriteAttributes()
        {
            _log.Debug("{attributes}");
            this.JsonGenerator.WriteObjectFieldStart("attributes");
            this.JsonGenerator.WriteArrayFieldStart("dataSet");

            foreach (var attr in this.DataStructureDefinition.DatasetAttributes)
            {
                var superAttr = (IAttributeSuperObject)this.CurrentDsdSuperObject.GetComponentById(attr.Id);

                if (superAttr != null)
                {
                    this.WriteComponent(superAttr, -1);
                }
            }

            this.JsonGenerator.WriteEndArray();

            this.JsonGenerator.WriteArrayFieldStart("series");

            foreach (var dimension in this.DataStructureDefinition.GetSeriesAttributes(this.DimensionAtObservation))
            {
                var seriesAttributes = this.CurrentDsdSuperObject.SeriesAttributes;

                foreach (var attributeSuperObject in seriesAttributes)
                {
                    if (attributeSuperObject.Id.Equals(dimension.Id))
                    {
                        this.WriteComponent(attributeSuperObject, -1);
                        break;
                    }
                }
            }

            this.JsonGenerator.WriteEndArray();

            _log.Debug("[observation]");
            this.JsonGenerator.WriteArrayFieldStart("observation");

            foreach (var attr in this.DataStructureDefinition.GetObservationAttributes(this.DimensionAtObservation))
            {
                var superAttr = (IAttributeSuperObject)this.CurrentDsdSuperObject.GetComponentById(attr.Id);

                if (superAttr != null)
                {
                    this.WriteComponent(superAttr, -1);
                }
            }

            _log.Debug("[/observation]");
            this.JsonGenerator.WriteEndArray();

            _log.Debug("{/attributes}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the dataset array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsDataset()
        {
            this.JsonGenerator.WriteArrayFieldStart("dataset");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the observation array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsObservation()
        {
            _log.Debug("[observation]");
            this.JsonGenerator.WriteArrayFieldStart("observation");

            var dimension = this.DataStructureDefinition.GetDimension(this.DimensionAtObservation);
            var superObject = this.CurrentDsdSuperObject.GetDimensionById(this.DimensionAtObservation);

            // Note that a Dimensions' position is 1 indexed, but we need it 0-indexed, so subtract 1
            this.WriteComponent(dimension, superObject, dimension.Position - 1);

            _log.Debug("[/observation]");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the series array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsSeries()
        {
            _log.Debug("[series]");
            this.JsonGenerator.WriteArrayFieldStart("series");

            var position = 0;
            IDimensionSuperObject timeDimension = null;

            foreach (var dimension in this.CurrentDsdSuperObject.Dimensions)
            {
                if (dimension.Id.Equals(this.DimensionAtObservation))
                {
                    continue;
                }

                if (dimension.TimeDimension)
                {
                    // Always write the time dimension at the end.
                    timeDimension = dimension;
                }
                else
                {
                    this.WriteComponent(dimension, position);
                    position++;
                }
            }

            if (timeDimension != null)
            {
                this.WriteComponent(timeDimension, position);
            }

            _log.Debug("[/series]");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obs">
        /// The observation.
        /// </param>
        protected override void WriteObs(IObservation obs)
        {
            this.WriteJsonObs(this.CurrentKey, this.PrevKey, obs);
            this.PrevKey = this.CurrentKey;
        }

        /// <summary>
        /// Writes the component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="superObject">The super object.</param>
        /// <param name="position">The position.</param>
        private void WriteComponent(IComponent component, IComponentSuperObject superObject, int position)
        {
            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();
            this.JsonGenerator.WriteStringField("id", component.Id);
            this.JsonGenerator.WriteStringField("name", this.Translator.GetTranslation(superObject.Concept.Names));

            if (position >= 0)
            {
                this.JsonGenerator.WriteNumberField("keyPosition", position);
            }

            var isTime = component.Id.Equals(DimensionObject.TimeDimensionFixedId);
            if (isTime)
            {
                this.JsonGenerator.WriteStringField("role", "time");
            }
            else
            {
                this.JsonGenerator.WriteNullField("role");
            }

            _log.Debug("[values]");
            this.JsonGenerator.WriteArrayFieldStart("values");
            var allCodes = this.GetReportedValues(component.Id);

            foreach (var currentCode in allCodes)
            {
                _log.Debug("{}");
                this.JsonGenerator.WriteStartObject();
                var id = currentCode;
                var name = currentCode;

                if (isTime)
                {
                    var start = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, true), TimeFormatEnumType.DateTime);
                    var end = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, false), TimeFormatEnumType.DateTime);
                    this.JsonGenerator.WriteStringField("start", start);
                    this.JsonGenerator.WriteStringField("end", end);
                }
                else
                {
                    var codelist = superObject.GetCodelist(true);

                    if (codelist != null)
                    {
                        var code = codelist.GetCodeByValue(currentCode);

                        if (code != null)
                        {
                            id = code.Id;
                            name = this.Translator.GetTranslation(code.Names);
                        }
                    }
                }

                this.JsonGenerator.WriteStringField("id", id);
                this.JsonGenerator.WriteStringField("name", name);
                _log.Debug("{/}");
                this.JsonGenerator.WriteEndObject();
            }

            _log.Debug("[/values]");
            this.JsonGenerator.WriteEndArray();
            _log.Debug("{/}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the json obs.
        /// </summary>
        /// <param name="currentKey">
        /// The current key.
        /// </param>
        /// <param name="prevKey">
        /// The previous key.
        /// </param>
        /// <param name="obs">
        /// The obs.
        /// </param>
        private void WriteJsonObs(string currentKey, string prevKey, IObservation obs)
        {
            var idx = this.GetReportedIndex(this.DimensionAtObservation, obs.ObsTime);

            if (currentKey.Equals(prevKey))
            {
                this.WriteJsonObs(obs, idx);
            }
            else
            {
                // Close the previous key if there was one open.
                if (prevKey != null)
                {
                    // End current object.
                    _log.Debug("{/}");
                    this.JsonGenerator.WriteEndObject();

                    // End Observations.
                    _log.Debug("/observations]");
                    this.JsonGenerator.WriteEndObject();
                }

                var seriesIndex = currentKey;

                // Start Series.
                _log.Debug("{series}");
                this.JsonGenerator.WriteObjectFieldStart(seriesIndex);

                _log.Debug("[attributes]");
                this.JsonGenerator.WriteArrayFieldStart("attributes");

                var seriesAttributes = this.CurrentDsdSuperObject.SeriesAttributes;

                foreach (var sBean in seriesAttributes)
                {
                    var id = sBean.Concept.Id;

                    // If this is Time Series query against the Series Key.
                    var timeSeries = this.DimensionAtObservation.Equals(DimensionObject.TimeDimensionFixedId);
                    string value = null;

                    if (timeSeries)
                    {
                        if (obs.SeriesKey.GetAttribute(id) != null)
                        {
                            value = obs.SeriesKey.GetAttribute(id).Code;
                        }
                    }
                    else if (obs.GetAttribute(id) != null)
                    {
                        value = obs.GetAttribute(id).Code;
                    }

                    if (value == null)
                    {
                        this.JsonGenerator.WriteNull();
                    }
                    else
                    {
                        var reportedIndex = this.GetReportedIndex(id, value);
                        this.JsonGenerator.WriteNumber(reportedIndex);
                    }
                }

                _log.Debug("[/attributes]");
                this.JsonGenerator.WriteEndArray();

                _log.Debug("[annotations]");
                this.JsonGenerator.WriteArrayFieldStart("annotations");

                // Write the annotation values.
                foreach (var currentAnnotation in obs.SeriesKey.Annotations)
                {
                    if (!this.Annotations.Contains(currentAnnotation))
                    {
                        this.Annotations.Add(currentAnnotation);
                    }

                    var index = this.Annotations.IndexOf(currentAnnotation);
                    this.JsonGenerator.WriteNumber(index);
                }

                _log.Debug("[/annotations]");
                this.JsonGenerator.WriteEndArray();
                _log.Debug("{observations}");
                this.JsonGenerator.WriteObjectFieldStart("observations");

                this.WriteJsonObs(obs, idx);
            }
        }

        /// <summary>
        /// Writes the json obs.
        /// </summary>
        /// <param name="obs">The obs.</param>
        /// <param name="idx">The index.</param>
        private void WriteJsonObs(IObservation obs, int idx)
        {
            var seriesKey = idx.ToString(CultureInfo.InvariantCulture);
            _log.Debug("['variable series key']");
            this.JsonGenerator.WriteArrayFieldStart(seriesKey);

            // Write the observation value as the first array field.
            this.JsonGenerator.WriteRawValue(this.GetObservationValueRawJsonString(obs.ObservationValue));

            // Write the attribute values.
            IList<int?> obsAttrs = new List<int?>();

            foreach (var attr in this.CurrentDsdSuperObject.ObservationAttributes)
            {
                var kv = obs.GetAttribute(attr.Id) ?? obs.SeriesKey.GetAttribute(attr.Id);
                var attrValue = (kv != null) ? kv.Code : null;

                if (attrValue == null)
                {
                    obsAttrs.Add(null);
                }
                else
                {
                    var index = this.GetReportedIndex(attr.Id, attrValue);
                    obsAttrs.Add(index);
                }
            }

            // Only write out the attributes, if they are not all null and there are no annotations following.
            if (ObjectUtil.ValidCollection(obsAttrs))
            {
                if (obsAttrs.Any(a => a.HasValue) || obs.Annotations.Count > 0)
                {
                    foreach (var attrIdx in obsAttrs)
                    {
                        if (!attrIdx.HasValue)
                        {
                            this.JsonGenerator.WriteNull();
                        }
                        else
                        {
                            this.JsonGenerator.WriteNumber(attrIdx.Value);
                        }
                    }
                }
            }

            foreach (var currentAnnotation in obs.Annotations)
            {
                if (!this.Annotations.Contains(currentAnnotation))
                {
                    this.Annotations.Add(currentAnnotation);
                }

                var index = this.Annotations.IndexOf(currentAnnotation);
                this.JsonGenerator.WriteNumber(index);
            }

            _log.Debug("[/'variable series key']");
            this.JsonGenerator.WriteEndArray();
        }
    }
}