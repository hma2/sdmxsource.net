﻿// -----------------------------------------------------------------------
// <copyright file="FlatDataWriter.cs" company="EUROSTAT">
//   Date Created : 2016-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using log4net;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Model;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// FlatDataWriter class.
    /// </summary>
    public class FlatDataWriter : AbstractJsonDataWriter
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The observations array started
        /// </summary>
        private bool _observationsArrayStarted;

        /// <summary>
        /// Initializes a new instance of the <see cref="FlatDataWriter"/> class.
        /// </summary>
        /// <param name="jsonGenerator">
        /// The jsonGenerator.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        public FlatDataWriter(JsonGenerator jsonGenerator, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, ITranslator translator)
            : base(jsonGenerator, superObjectRetrievalManager, translator)
        {
        }

        /// <summary>
        /// Writes the closing tags (only if needed) for the
        /// - observations object in the current dataSet object
        /// - current dataSet object
        /// - dataSets array
        /// along with the structure.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public override void Close(params IFooterMessage[] footer)
        {
            base.Close(footer);

            try
            {
                this.JsonGenerator.WriteEnd();

                if (this.CurrentPosition == DataPosition.SeriesKey || this.CurrentPosition == DataPosition.SeriesKeyAttribute || this.CurrentPosition == DataPosition.Observation
                    || this.CurrentPosition == DataPosition.ObservationAttribute)
                {
                    this.JsonGenerator.WriteEnd();

                    if (this.CurrentPosition == DataPosition.Observation || this.CurrentPosition == DataPosition.ObservationAttribute)
                    {
                        this.JsonGenerator.WriteEnd();
                    }
                }

                this.WriteStructure();
            }
            catch (IOException e)
            {
                throw new ArgumentException("Illegal argument", e);
            }
            finally
            {
                this.JsonGenerator.Close();
            }
        }

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartSeries(params IAnnotation[] annotations)
        {
            base.StartSeries(annotations);

            if (!this._observationsArrayStarted)
            {
                _log.Debug("{observations}");
                this.JsonGenerator.WriteObjectFieldStart("observations");

                this._observationsArrayStarted = true;
            }
        }

        /// <summary>
        /// Writes the attributes.
        /// </summary>
        protected override void WriteAttributes()
        {
            _log.Debug("{attributes}");
            this.JsonGenerator.WriteObjectFieldStart("attributes");
            this.JsonGenerator.WriteArrayFieldStart("dataSet");

            foreach (var attr in this.DataStructureDefinition.DatasetAttributes)
            {
                var superAttr = (IAttributeSuperObject)this.CurrentDsdSuperObject.GetComponentById(attr.Id);

                if (superAttr != null)
                {
                    this.WriteComponent(superAttr, -1);
                }
            }

            this.JsonGenerator.WriteEndArray();
            this.JsonGenerator.WriteArrayFieldStart("series");
            this.JsonGenerator.WriteEndArray();

            _log.Debug("[observation]");
            this.JsonGenerator.WriteArrayFieldStart("observation");

            foreach (var attribute in this.CurrentDsdSuperObject.Attributes.Where(a => a.AttachmentLevel != AttributeAttachmentLevel.DataSet))
            {
                this.WriteComponent(attribute, -1);
            }

            _log.Debug("[/observation]");
            this.JsonGenerator.WriteEndArray();
            _log.Debug("{/attributes}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the dataset array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsDataset()
        {
            this.JsonGenerator.WriteArrayFieldStart("dataset");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the observation array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsObservation()
        {
            _log.Debug("[observation]");
            this.JsonGenerator.WriteArrayFieldStart("observation");

            var position = 0;

            foreach (var dimension in this.DimensionsOrdered())
            {
                this.WriteComponent(dimension, position++);
            }

            this.JsonGenerator.WriteEndArray();
            _log.Debug("[/observation]");
        }

        /// <summary>
        /// Writes the series array into root -> structure -> dimensions.
        /// </summary>
        protected override void WriteDimensionsSeries()
        {
            this.JsonGenerator.WriteArrayFieldStart("series");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obs">
        /// The observation.
        /// </param>
        protected override void WriteObs(IObservation obs)
        {
            this.WriteJsonObs(this.CurrentKey, obs);
        }

        /// <summary>
        /// The write json obs.
        /// </summary>
        /// <param name="currentKey">
        /// The current key.
        /// </param>
        /// <param name="obs">
        /// The obs.
        /// </param>
        private void WriteJsonObs(string currentKey, IObservation obs)
        {
            var seriesKey = this.CurrentDsdSuperObject.TimeDimension != null ? currentKey + ":" + this.GetReportedIndex(DimensionObject.TimeDimensionFixedId, obs.ObsTime) : currentKey;

            this.WriteJsonObs(obs, seriesKey);
        }

        /// <summary>
        /// The write json obs.
        /// </summary>
        /// <param name="obs">
        /// The obs.
        /// </param>
        /// <param name="seriesKey">
        /// The series key.
        /// </param>
        private void WriteJsonObs(IObservation obs, string seriesKey)
        {
            _log.Debug("['variable series key']");
            this.JsonGenerator.WriteArrayFieldStart(seriesKey);

            // Write the observation value as the first array field.
            this.JsonGenerator.WriteRawValue(this.GetObservationValueRawJsonString(obs.ObservationValue).Replace(',', '.'));

            // Write the attribute values (or null).
            foreach (var attr in this.CurrentDsdSuperObject.Attributes.Where(a => a.AttachmentLevel != AttributeAttachmentLevel.DataSet))
            {
                var kv = obs.GetAttribute(attr.Id) ?? obs.SeriesKey.GetAttribute(attr.Id);

                var attrValue = (kv != null) ? kv.Code : null;

                if (attrValue == null)
                {
                    this.JsonGenerator.WriteNull();
                }
                else
                {
                    var idx = this.GetReportedIndex(attr.Id, attrValue);
                    this.JsonGenerator.WriteNumber(idx);
                }
            }

            // Write the annotation values.
            foreach (var currentAnnotation in obs.SeriesKey.Annotations)
            {
                if (!this.Annotations.Contains(currentAnnotation))
                {
                    this.Annotations.Add(currentAnnotation);
                }

                var idx = this.Annotations.IndexOf(currentAnnotation);
                this.JsonGenerator.WriteNumber(idx);
            }

            foreach (var currentAnnotation in obs.Annotations)
            {
                if (!this.Annotations.Contains(currentAnnotation))
                {
                    this.Annotations.Add(currentAnnotation);
                }

                var idx = this.Annotations.IndexOf(currentAnnotation);
                this.JsonGenerator.WriteNumber(idx);
            }

            _log.Debug("[/'variable series key']");
            this.JsonGenerator.WriteEndArray();
        }
    }
}