﻿// -----------------------------------------------------------------------
// <copyright file="AbstractJsonDataWriter.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    using log4net;

    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Translator;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Abstract JsonDataWriter class.
    /// </summary>
    public abstract class AbstractJsonDataWriter : DatasetInfoDataWriterEngine
    {
        /// <summary>
        /// The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractJsonDataWriter"/> class.
        /// </summary>
        /// <param name="jsonGenerator">
        /// The json generator.
        /// </param>
        /// <param name="superObjectRetrievalManager">
        /// The superObjectRetrievalManager.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        protected AbstractJsonDataWriter(JsonGenerator jsonGenerator, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, ITranslator translator)
            : base(superObjectRetrievalManager)
        {
            this.JsonGenerator = jsonGenerator;
            this.Translator = translator;
        }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        public ITranslator Translator { get; private set; }

        /// <summary>
        /// Gets the annotations.
        /// </summary>
        protected IList<IAnnotation> Annotations { get; private set; }

        /// <summary>
        /// Gets the current key.
        /// </summary>
        protected string CurrentKey { get; private set; }

        /// <summary>
        /// Gets the json generator.
        /// </summary>
        protected JsonGenerator JsonGenerator { get; private set; }

        /// <summary>
        /// Gets or sets the previous key.
        /// </summary>
        protected string PrevKey { get; set; }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dsd">
        /// The data structure definition.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public override void StartDataset(IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header, params IAnnotation[] annotations)
        {
            base.StartDataset(dataflow, dsd, header, annotations);

            if (annotations != null)
            {
                this.Annotations = new List<IAnnotation>(annotations);
            }

            this.WriteJsonDataSet(header);
        }

        /// <summary>
        /// Writes the attributes.
        /// </summary>
        protected abstract void WriteAttributes();

        /// <summary>
        /// Writes a component.
        /// </summary>
        /// <param name="component">
        /// The component.
        /// </param>
        /// <param name="position">
        /// The position.
        /// </param>
        protected void WriteComponent(IComponentSuperObject component, int position)
        {
            if (component == null)
            {
                throw new ArgumentNullException("component");
            }

            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();
            this.JsonGenerator.WriteStringField("id", component.Id);
            this.JsonGenerator.WriteStringField("name", this.Translator.GetTranslation(component.Concept.Names));

            if (position >= 0)
            {
                this.JsonGenerator.WriteNumberField("keyPosition", position);
            }

            var isTime = component.Id.Equals(DimensionObject.TimeDimensionFixedId);

            if (isTime)
            {
                this.JsonGenerator.WriteStringField("role", "time");
            }
            else
            {
                this.JsonGenerator.WriteNullField("role");
            }

            _log.Debug("[values]");
            this.JsonGenerator.WriteArrayFieldStart("values");
            var allCodes = this.GetReportedValues(component.Id);

            foreach (var currentCode in allCodes)
            {
                _log.Debug("{}");
                this.JsonGenerator.WriteStartObject();
                var id = currentCode;
                var name = currentCode;

                if (isTime)
                {
                    var start = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, true), TimeFormatEnumType.DateTime);
                    var end = DateUtil.FormatDate(DateUtil.FormatDate(currentCode, false), TimeFormatEnumType.DateTime);
                    this.JsonGenerator.WriteStringField("start", start);
                    this.JsonGenerator.WriteStringField("end", end);
                }
                else
                {
                    var codelist = component.GetCodelist(true);

                    var code = (codelist != null) ? codelist.GetCodeByValue(currentCode) : null;

                    if (code != null)
                    {
                        id = code.Id;
                        name = this.Translator.GetTranslation(code.Names);
                    }
                }

                this.JsonGenerator.WriteStringField("id", id);
                this.JsonGenerator.WriteStringField("name", name);
                _log.Debug("{/}");
                this.JsonGenerator.WriteEndObject();
            }

            _log.Debug("[/values]");
            this.JsonGenerator.WriteEndArray();

            _log.Debug("{/}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes dataset attributes.
        /// </summary>
        /// <param name="datasetAttributes">
        /// The dataset attributes.
        /// </param>
        protected override void WriteDatasetAttributes(IList<IKeyValue> datasetAttributes)
        {
            base.WriteDatasetAttributes(datasetAttributes);

            _log.Debug("[attributes]");
            this.JsonGenerator.WriteArrayFieldStart("attributes");

            foreach (var attr in this.CurrentDsdSuperObject.DatasetAttributes)
            {
                var currentKeyValue = datasetAttributes.FirstOrDefault(a => attr.Concept.Id == a.Concept);

                if (currentKeyValue == null || string.IsNullOrWhiteSpace(currentKeyValue.Code))
                {
                    this.JsonGenerator.WriteNull();
                }
                else
                {
                    var reportedIndex = this.GetReportedIndex(currentKeyValue.Concept, currentKeyValue.Code);
                    this.JsonGenerator.WriteNumber(reportedIndex);
                }
            }

            _log.Debug("[/attributes]");
            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Writes the dimensions.
        /// </summary>
        protected abstract void WriteDimensionsDataset();

        /// <summary>
        /// Returns string that will be used as raw value in output Json
        /// </summary>
        /// <param name="observationValue"></param>
        /// <returns></returns>
        protected string GetObservationValueRawJsonString(string observationValue)
        {
            if (observationValue == null)
            {
                return "null";
            }

            if (observationValue.Length == 0)
            {
                return "\"\"";
            }

            return observationValue;
        }

        /// <summary>
        /// Writes the observations.
        /// </summary>
        protected abstract void WriteDimensionsObservation();

        /// <summary>
        /// Writes the series.
        /// </summary>
        protected abstract void WriteDimensionsSeries();

        /// <summary>
        /// Writes the key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        protected override void WriteKey(IKeyable key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (!key.Series)
            {
                return;
            }

            base.WriteKey(key);

            var sb = new StringBuilder();
            var delimiter = string.Empty;

            foreach (var kv in key.Key.OrderBy(x=>this._dimensionOrder[x.Concept]))
            {
                var currentIndex = this.GetReportedIndex(kv.Concept, kv.Code);
                sb.Append(delimiter + currentIndex);
                delimiter = ":";
            }

            if (this.CurrentKey != null)
            {
                this.PrevKey = this.CurrentKey;
            }

            this.CurrentKey = sb.ToString();
        }

        /// <summary>
        /// Writes the structure.
        /// </summary>
        protected void WriteStructure()
        {
            _log.Debug("{structure}");
            this.JsonGenerator.WriteObjectFieldStart("structure");

            var maint = this.Dataflow ?? (IMaintainableObject)this.CurrentDsdSuperObject.BuiltFrom;

            this.JsonGenerator.WriteStringField("name", this.Translator.GetTranslation(maint.Names));
            this.JsonGenerator.WriteStringField("description", this.Translator.GetTranslation(maint.Descriptions));

            this.WriteStructureDimensions();
            this.WriteAttributes();
            this.WriteAnnotations();

            _log.Debug("{/structure}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Writes the structure.
        /// </summary>
        protected void WriteStructureDimensions()
        {
            _log.Debug("{dimensions}");
            this.JsonGenerator.WriteObjectFieldStart("dimensions");

            this.WriteDimensionsDataset();
            this.WriteDimensionsSeries();
            this.WriteDimensionsObservation();

            _log.Debug("{/dimensions}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// The write annotation.
        /// </summary>
        /// <param name="annotation">
        /// The annotation.
        /// </param>
        private void WriteAnnotation(IAnnotation annotation)
        {
            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();

            if (annotation.Id != null)
            {
                this.JsonGenerator.WriteStringField("id", annotation.Id);
            }

            if (annotation.Title != null)
            {
                this.JsonGenerator.WriteStringField("title", annotation.Title);
            }

            if (annotation.Type != null)
            {
                this.JsonGenerator.WriteStringField("type", annotation.Type);
            }

            if (annotation.Uri != null)
            {
                this.JsonGenerator.WriteStringField("uri", annotation.Uri.ToString());
            }

            if (annotation.Text.Count > 0)
            {
                var text = annotation.Text;
                var defaultLocale = TextTypeUtil.GetDefaultLocale(text);
                this.JsonGenerator.WriteStringField("text", defaultLocale.Value);
            }

            _log.Debug("{/}");
            this.JsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// The write annotations.
        /// </summary>
        private void WriteAnnotations()
        {
            this.JsonGenerator.WriteArrayFieldStart("annotations");

            if (this.Annotations != null)
            {
                foreach (var annotation in this.Annotations)
                {
                    this.WriteAnnotation(annotation);
                }
            }

            this.JsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// The write json data set.
        /// </summary>
        /// <param name="datasetHeader">
        /// The dataset header.
        /// </param>
        private void WriteJsonDataSet(IDatasetHeader datasetHeader)
        {
            _log.Debug("[dataSets]");
            this.JsonGenerator.WriteArrayFieldStart("dataSets");

            _log.Debug("{}");
            this.JsonGenerator.WriteStartObject();
            DatasetAction action = DatasetActionEnumType.Information;

            if (datasetHeader != null)
            {
                action = datasetHeader.Action;

                if (datasetHeader.ReportingBeginDate != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("reportingBegin", DateUtil.FormatDate(datasetHeader.ReportingBeginDate));
                }

                if (datasetHeader.ReportingEndDate != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("reportingEnd", DateUtil.FormatDate(datasetHeader.ReportingEndDate));
                }

                if (datasetHeader.ValidFrom != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("validFrom", DateUtil.FormatDate(datasetHeader.ValidFrom));
                }

                if (datasetHeader.ValidTo != default(DateTime))
                {
                    this.JsonGenerator.WriteStringField("validTo", DateUtil.FormatDate(datasetHeader.ValidTo));
                }

                if (datasetHeader.PublicationYear > 0)
                {
                    this.JsonGenerator.WriteNumberField("publicationYear", datasetHeader.PublicationYear);
                }

                if (ObjectUtil.ValidString(datasetHeader.PublicationPeriod))
                {
                    this.JsonGenerator.WriteStringField("publicationPeriod", datasetHeader.PublicationPeriod);
                }
            }

            this.JsonGenerator.WriteStringField("action", action.Action);

            if (this.Annotations != null)
            {
                _log.Debug("[annotations]");
                this.JsonGenerator.WriteArrayFieldStart("annotations");

                foreach (var currentAnnotation in this.Annotations)
                {
                    if (!this.Annotations.Contains(currentAnnotation))
                    {
                        this.Annotations.Add(currentAnnotation);
                    }

                    var idx = this.Annotations.IndexOf(currentAnnotation);
                    this.JsonGenerator.WriteNumber(idx);
                }

                _log.Debug("[/annotations]");
                this.JsonGenerator.WriteEndArray();
            }
        }
    }
}