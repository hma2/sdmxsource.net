﻿// -----------------------------------------------------------------------
// <copyright file="DecoratedDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2016-08-17
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// DecoratedDataWriterEngine class.
    /// </summary>
    public class DecoratedDataWriterEngine : IDataWriterEngine
    {
        /// <summary>
        /// The _attribute list.
        /// </summary>
        private IList<IKeyValue> _attributeList;

        /// <summary>
        /// The _current annotations.
        /// </summary>
        private IAnnotation[] _currentAnnotations;

        /// <summary>
        /// The _current key.
        /// </summary>
        private IKeyable _currentKey;

        /// <summary>
        /// The _flush dataset attributes required.
        /// </summary>
        private bool _flushDatasetAttributesRequired;

        /// <summary>
        /// The _flush key required.
        /// </summary>
        private bool _flushKeyRequired;

        /// <summary>
        /// The _flush obs required.
        /// </summary>
        private bool _flushObsRequired;

        /// <summary>
        /// The _group name.
        /// </summary>
        private string _groupName;

        /// <summary>
        /// The _is closed.
        /// </summary>
        private bool _isClosed;

        /// <summary>
        /// The _key list.
        /// </summary>
        private IList<IKeyValue> _keyList;

        /// <summary>
        /// The _obs concept id.
        /// </summary>
        private string _obsConceptId;

        /// <summary>
        /// The _obs concept value.
        /// </summary>
        private string _obsConceptValue;

        /// <summary>
        /// The _obs value.
        /// </summary>
        private string _obsValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="DecoratedDataWriterEngine"/> class. 
        /// Initializes a new instance of <see cref="DecoratedDataWriterEngine"/> class.
        /// </summary>
        /// <param name="dwe">
        /// The dataWriterEngine.
        /// </param>
        public DecoratedDataWriterEngine(IDataWriterEngine dwe)
        {
            this.DataWriterEngine = dwe;
            this.CurrentPosition = DataPosition.Dataset;

            this.ClearCurrentKeys();
        }

        /// <summary>
        /// Gets or sets the current position.
        /// </summary>
        protected DataPosition CurrentPosition { get; set; }

        /// <summary>
        /// Gets or sets the dataflow.
        /// </summary>
        protected IDataflowObject Dataflow { get; set; }

        /// <summary>
        /// Gets or sets the DSD.
        /// </summary>
        protected IDataStructureObject DataStructureDefinition { get; set; }

        /// <summary>
        /// Gets or sets the data type.
        /// </summary>
        protected DataTypeRepresentation DataType { get; set; }

        /// <summary>
        /// Gets or sets the data writer engine.
        /// </summary>
        protected IDataWriterEngine DataWriterEngine { get; set; }

        /// <summary>
        /// Gets or sets the dimension at observation.
        /// </summary>
        protected string DimensionAtObservation { get; set; }

        /// <summary>
        /// Writes the footer.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public virtual void Close(params IFooterMessage[] footer)
        {
            if (this._isClosed)
            {
                return;
            }

            this._isClosed = true;
            this.FlushDatasetAttributes();
            this.FlushKey();
            this.FlushObs();

            if (this.DataWriterEngine != null)
            {
                this.DataWriterEngine.Close();
            }
        }

        /// <summary>
        /// Disposes the object.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Starts a dataset with the data conforming to the DSD.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dsd">
        /// The data structure definition.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public virtual void StartDataset(IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header, params IAnnotation[] annotations)
        {
            this.CheckClosed();

            this.FlushDatasetAttributes();
            this.FlushKey();
            this.FlushObs();
            this.ClearCurrentKeys();

            this.Dataflow = dataflow;
            this.DataStructureDefinition = dsd;

            this.CurrentPosition = DataPosition.Dataset;

            if (this.DataWriterEngine != null)
            {
                this.DataWriterEngine.StartDataset(dataflow, dsd, header, annotations);
            }

            if (header != null && header.DataStructureReference != null)
            {
                this.DimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            if (!ObjectUtil.ValidString(this.DimensionAtObservation))
            {
                this.DimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            }

            this.DataType = DataTypeRepresentation.GetDataType(this.DimensionAtObservation);
        }

        /// <summary>
        /// Starts a group.
        /// </summary>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.FlushDatasetAttributes();
            this.FlushKey();
            this.FlushObs();
            this.CurrentPosition = DataPosition.Group;
            this._groupName = groupId;
            this._currentAnnotations = annotations;
        }

        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public virtual void StartSeries(params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.FlushDatasetAttributes();
            this.FlushKey();
            this.FlushObs();
            this.CurrentPosition = DataPosition.SeriesKey;
            this._currentAnnotations = annotations;
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public virtual void WriteAttributeValue(string id, string valueRen)
        {
            this.CheckClosed();

            if (this.CurrentPosition == DataPosition.SeriesKey)
            {
                this.CurrentPosition = DataPosition.SeriesKeyAttribute;
            }
            else if (this.CurrentPosition == DataPosition.Observation)
            {
                this.CurrentPosition = DataPosition.ObservationAttribute;
            }
            else if (this.CurrentPosition == DataPosition.GroupKey)
            {
                this.CurrentPosition = DataPosition.GroupKeyAttribute;
            }
            else if (this.CurrentPosition == DataPosition.Dataset)
            {
                this.CurrentPosition = DataPosition.DatasetAttribute;
                this._flushDatasetAttributesRequired = true;
            }

            this._attributeList.Add(new KeyValueImpl(valueRen, id));
        }

        /// <summary>
        /// Writes the group key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CurrentPosition = DataPosition.GroupKeyAttribute;
            this._flushKeyRequired = true;
            this._keyList.Add(new KeyValueImpl(valueRen, id));
        }

        /// <summary>
        /// Writes the header.
        /// </summary>
        /// <param name="header">
        /// The header.
        /// </param>
        public virtual void WriteHeader(IHeader header)
        {
            this.CheckClosed();

            if (this.DataWriterEngine != null)
            {
                this.DataWriterEngine.WriteHeader(header);
            }
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public virtual void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (this.DimensionAtObservation.Equals(Api.Constants.DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.All).Value))
            {
                throw new ArgumentException(
                    "Cannot write observation, as no observation concept id was given, and this is writing a flat dataset. "
                    + "Please use the method: writeObservation(String obsConceptId, String obsIdValue, String obsValue, AnnotationBean... annotations)");
            }

            this.WriteObservation(this.DimensionAtObservation, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsConceptId">
        /// The observation concept id.
        /// </param>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public virtual void WriteObservation(string obsConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CurrentPosition = DataPosition.Observation;
            this.FlushDatasetAttributes();
            this.FlushKey();
            this.FlushObs();
            this._obsConceptId = obsConceptId;
            this._obsConceptValue = obsConceptValue;
            this._obsValue = obsValue;
            this._currentAnnotations = annotations;
            this._flushObsRequired = true;
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsTime">
        /// The observation time.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="sdmxSwTimeFormat">
        /// The time format.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            this.CheckClosed();

            this.CurrentPosition = DataPosition.Observation;
            this.WriteObservation(DateUtil.FormatDate(obsTime, sdmxSwTimeFormat), obsValue, annotations);
        }

        /// <summary>
        /// Writes the series key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public virtual void WriteSeriesKeyValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CurrentPosition = DataPosition.SeriesKey;
            if (!this._flushKeyRequired)
            {
                this.FlushObs();
            }

            this._flushKeyRequired = true;
            this._keyList.Add(new KeyValueImpl(valueRen, id));
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposable">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposable)
        {
        }

        /// <summary>
        /// Flushes the dataset attributes.
        /// </summary>
        protected void FlushDatasetAttributes()
        {
            if (!this._flushDatasetAttributesRequired)
            {
                return;
            }

            this.WriteDatasetAttributes(this._attributeList);
            this._attributeList = new List<IKeyValue>();
            this._flushDatasetAttributesRequired = false;
        }

        /// <summary>
        /// Creates a Keyable (currentKey) and converts it to the mapped key, writing the new key to the writer.
        /// </summary>
        protected void FlushKey()
        {
            if (!this._flushKeyRequired)
            {
                return;
            }

            this._currentKey = new KeyableImpl(this.Dataflow, this.DataStructureDefinition, this._keyList, this._attributeList, this._groupName, this._currentAnnotations);
            this.WriteKey(this._currentKey);
            this.ClearCurrentKeys();
        }

        /// <summary>
        /// Flushes the observation.
        /// </summary>
        protected void FlushObs()
        {
            if (!this._flushObsRequired)
            {
                return;
            }

            IObservation obs;

            if (this.DataType.Equals(DataTypeRepresentationEnumType.TimeSeries))
            {
                obs = new ObservationImpl(this._currentKey, this._obsConceptValue, this._obsValue, this._attributeList, this._currentAnnotations);
            }
            else
            {
                IKeyValue kv = new KeyValueImpl(this._obsConceptValue, this._obsConceptId);
                obs = new ObservationImpl(this._currentKey, this._obsConceptValue, this._obsValue, this._attributeList, kv, this._currentAnnotations);
            }

            this.WriteObs(obs);
            this.ClearCurrentKeys();
        }

        /// <summary>
        /// Writes the dataset attributes.
        /// </summary>
        /// <param name="datasetAttributes">
        /// The dataset attributes.
        /// </param>
        protected virtual void WriteDatasetAttributes(IList<IKeyValue> datasetAttributes)
        {
            if (this.DataWriterEngine == null)
            {
                return;
            }

            if (datasetAttributes == null)
            {
                throw new ArgumentNullException("datasetAttributes");
            }

            foreach (var attr in datasetAttributes)
            {
                this.DataWriterEngine.WriteAttributeValue(attr.Concept, attr.Code);
            }
        }

        /// <summary>
        /// Writes the key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        protected virtual void WriteKey(IKeyable key)
        {
            if (this.DataWriterEngine == null)
            {
                return;
            }

            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (key.Series)
            {
                this.DataWriterEngine.StartSeries();

                foreach (var kv in key.Key)
                {
                    this.DataWriterEngine.WriteSeriesKeyValue(kv.Concept, kv.Code);
                }
            }
            else
            {
                this.DataWriterEngine.StartGroup(key.GroupName);

                foreach (var kv in key.Key)
                {
                    this.DataWriterEngine.WriteGroupKeyValue(kv.Concept, kv.Code);
                }
            }

            foreach (var kv in key.Attributes)
            {
                this.DataWriterEngine.WriteAttributeValue(kv.Concept, kv.Code);
            }
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observation">
        /// The observation.
        /// </param>
        protected virtual void WriteObs(IObservation observation)
        {
            if (this.DataWriterEngine == null)
            {
                return;
            }

            if (observation == null)
            {
                throw new ArgumentNullException("observation");
            }

            this.DataWriterEngine.WriteObservation(observation.ObsTime, observation.ObservationValue);

            foreach (var kv in observation.Attributes)
            {
                this.DataWriterEngine.WriteAttributeValue(kv.Concept, kv.Code);
            }
        }

        /// <summary>
        /// The check closed.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Data Writer has already been closed and cannot have any more information written to it!</exception>
        private void CheckClosed()
        {
            if (this._isClosed)
            {
                throw new InvalidOperationException("Data Writer has already been closed and cannot have any more information written to it!");
            }
        }

        /// <summary>
        /// The clear current keys.
        /// </summary>
        private void ClearCurrentKeys()
        {
            this._keyList = new List<IKeyValue>();
            this._attributeList = new List<IKeyValue>();
            this._groupName = null;
            this._obsConceptValue = null;
            this._obsValue = null;
            this._flushDatasetAttributesRequired = false;
            this._flushObsRequired = false;
            this._flushKeyRequired = false;
            this._currentAnnotations = null;
        }
    }
}