﻿// -----------------------------------------------------------------------
// <copyright file="ReportingPeriodDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2017-10-02
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;

    using Api.Constants;
    using Api.Constants.InterfaceConstant;
    using Api.Engine;
    using Api.Model;
    using Api.Model.Header;
    using Api.Model.Objects.Base;
    using Api.Model.Objects.DataStructure;

    using Manager;

    using SdmxObjects.Model.Objects.Base;

    using Sdmxsource.Util;

    using Util.Date;

    /// <inheritdoc />
    /// <summary>
    /// ReportingPeriodDataWriterEngine
    /// </summary>
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine" />
    public class ReportingPeriodDataWriterEngine : ReportingPeriodDataWriterEngineBase
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.ReportingPeriodDataWriterEngine" /> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        public ReportingPeriodDataWriterEngine(IDataWriterEngine decoratedEngine):base (decoratedEngine)
        {
        }

        /// <inheritdoc />
        public override void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (DimensionObject.TimeDimensionFixedId.Equals(observationConceptId))
            {
                var newValue = this.ReportingPeriodConverter.ToReportingPeriod(new SdmxDateCore(obsConceptValue), this.ReportingStartYearDate);
                this.DecoratedEngine.WriteObservation(observationConceptId, newValue, obsValue, annotations);
            }
            else
            {
                this.DecoratedEngine.WriteObservation(observationConceptId, obsConceptValue, obsValue, annotations);
            }
        }

        /// <inheritdoc />
        public override void WriteSeriesKeyValue(string id, string value)
        {
            if (DimensionObject.TimeDimensionFixedId.Equals(id))
            {
                var newValue = this.ReportingPeriodConverter.ToReportingPeriod(new SdmxDateCore(value), this.ReportingStartYearDate);
                this.DecoratedEngine.WriteSeriesKeyValue(id, newValue);
            }
            else
            {
                this.DecoratedEngine.WriteSeriesKeyValue(id, value);
            }
        }
    }
}