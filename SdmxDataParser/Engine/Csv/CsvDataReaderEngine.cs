﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    using System.IO;
    using System.Text.RegularExpressions;

    using Api.Constants;
    using Api.Engine;
    using Api.Exception;
    using Api.Manager.Retrieval;
    using Api.Model.Data;
    using Api.Model.Header;
    using Api.Model.Objects.DataStructure;
    using Api.Util;

    using CsvHelper;
    using CsvHelper.Configuration;

    using Reader;

    using SdmxObjects.Model.Data;
    using SdmxObjects.Model.Header;
    using Util.Objects.Reference;

    /// <summary>
    /// 
    /// </summary>
    public sealed class CsvDataReaderEngine : AbstractDataReaderEngine
    {
        private CsvReader _csvReader;

        private Dictionary<string, int> _csvHeaders;
        private int _valueIndex = -1;
        private int _timeIndex = -1;
        private bool _readNextObservation = false;
        private List<KeyValuePair<string, int>> _dimensions;
        private List<KeyValuePair<string, int>> _obsAttributes;
        private List<KeyValuePair<string, int>> _seriesAttributes;
        private List<IKeyValue> _datasetAttributes;

        private string[] _buffer;
        private string _seriesKey;
        private static readonly Regex urnRegex = new Regex(@"(.+):(.+)\(((\d\.){1,2}\d)\)", RegexOptions.Compiled);

        /// <summary>
        ///     Initializes a new instance of the <see cref="CsvDataReaderEngine" /> class.
        ///     Initializes a new instance of the <see cref="AbstractSdmxDataReaderEngine" /> class.
        /// </summary>
        /// <param name="dataLocation">
        ///     The data Location.
        /// </param>
        /// <param name="objectRetrieval">
        ///     The SDMX Object Retrieval. giving the ability to retrieve DSDs for the datasets this
        ///     reader engine is reading.  This can be null if there is only one relevant DSD - in which case the
        ///     <paramref name="defaultDsd" /> should be provided.
        /// </param>
        /// <param name="defaultDataflow">
        ///     The default Dataflow. (Optional)
        /// </param>
        /// <param name="defaultDsd">
        ///     The default DSD. The default DSD to use if the <paramref name="objectRetrieval" /> is null, or
        ///     if the bean retrieval does not return the DSD for the given dataset.
        /// </param>
        /// <exception cref="System.ArgumentException">
        ///     AbstractDataReaderEngine expects either a ISdmxObjectRetrievalManager or a
        ///     IDataStructureObject to be able to interpret the structures
        /// </exception>
        public CsvDataReaderEngine(IReadableDataLocation dataLocation, ISdmxObjectRetrievalManager objectRetrieval, IDataflowObject defaultDataflow, IDataStructureObject defaultDsd) : base(dataLocation, objectRetrieval, defaultDataflow, defaultDsd)
        {
            this.Reset();
        }

        /// <summary>
        ///     Gets the attributes available for the current dataset
        /// </summary>
        /// <value> a copy of the list, returns an empty list if there are no dataset attributes </value>
        public override IList<IKeyValue> DatasetAttributes
        {
            get { return this._datasetAttributes; }
        }

        /// <summary>
        ///     Creates a copy of this data reader engine, the copy is another iterator over the same source data
        /// </summary>
        /// <returns>
        ///     The <see cref="IDataReaderEngine" /> .
        /// </returns>
        public override IDataReaderEngine CreateCopy()
        {
            return new CsvDataReaderEngine(this.DataLocation, this.ObjectRetrieval, this.DefaultDataflow, this.DefaultDsd);
        }

        /// <summary>
        ///     The lazy load key.
        /// </summary>
        /// <returns>
        ///     The <see cref="IKeyable" />.
        /// </returns>
        protected override IKeyable LazyLoadKey()
        {
            var key         = this.BuildKey(this._dimensions);
            var attributes  = this.BuildKey(this._seriesAttributes);

            return this.CreateKeyable(key, attributes, TimeFormat.GetFromEnum(TimeFormatEnumType.Null)); ;
        }

        /// <summary>
        ///     The lazy load observation.
        /// </summary>
        /// <returns>
        ///     The <see cref="IObservation" />.
        /// </returns>
        protected override IObservation LazyLoadObservation()
        {
            var attributes = this.BuildKey(this._obsAttributes);
            var obsValue = this._buffer[this._valueIndex];
            var obsTime = this._buffer[this._timeIndex];

            return new ObservationImpl(this.CurrentKeyValue, obsTime, obsValue, attributes);
        }

        /// <summary>
        ///     Moves the next dataset internal.
        /// </summary>
        /// <returns>
        ///     True if there is another <c>Dataset</c>; otherwise false.
        /// </returns>
        protected override bool MoveNextDatasetInternal()
        {
            if (this.DatasetPosition > -1)
                return false;

            // read header row
            if (!this.Read())
                return false;

            this._csvHeaders = this.ReadHeaders(this._csvReader.CurrentRecord);

            // read 1-st data row to obtain dataflow ID
            if (!this.Read())
                return false;

            this.DatasetHeader = this.GetHeader(this._csvReader[0]);

            return true;
        }

        /// <summary>
        ///     Moves the next key-able (internal).
        /// </summary>
        /// <returns>
        ///     True if there is another <c>Key-able</c>; otherwise false.
        /// </returns>
        protected override bool MoveNextKeyableInternal()
        {
            do
            {
                this._buffer = this._csvReader.CurrentRecord;

                if (this.IsNewSeriesKey())
                {
                    this._readNextObservation = false;
                    this._seriesKey = this.GetKey();
                    return true;
                }
            }
            while (this.Read());

            return false;
        }

        /// <summary>
        ///     Moves the next observation internal.
        /// </summary>
        /// <returns>
        ///     True if there is another <c>observation</c>; otherwise false.
        /// </returns>
        protected override bool MoveNextObservationInternal()
        {
            if (this._readNextObservation && !this.Read())
                return false;

            this._buffer = this._csvReader.CurrentRecord;

            return this._readNextObservation = !this.IsNewSeriesKey();
        }


        /// <summary>
        ///     Sets the current DSD.
        /// </summary>
        /// <param name="currentDsd">
        ///     The current DSD.
        /// </param>
        /// <exception cref="SdmxNotImplementedException">
        ///     Time series without time dimension
        /// </exception>
        protected override void SetCurrentDsd(IDataStructureObject currentDsd)
        {
            base.SetCurrentDsd(currentDsd);

            // reset

            this._buffer = this._csvReader.CurrentRecord;
            this._dimensions = new List<KeyValuePair<string, int>>();
            this._obsAttributes = new List<KeyValuePair<string, int>>();
            this._seriesAttributes = new List<KeyValuePair<string, int>>();
            var datasetAttribues = new List<KeyValuePair<string, int>>();


            // map headers

            if (currentDsd.TimeDimension == null)
            {
                throw new SdmxNotImplementedException(string.Format("The DSD: {0} has no time dimension. This is unsupported!", currentDsd.Id));
            }

            foreach (var dim in currentDsd.DimensionList.Dimensions)
            {
                var index = this.GetIndex(dim);

                if (dim.TimeDimension)
                {
                    this._timeIndex = index;
                }
                else
                {
                    this._dimensions.Add(new KeyValuePair<string, int>(dim.Id, index));
                }
            }

            foreach (var attr in currentDsd.Attributes)
            {
                List<KeyValuePair<string, int>> attrList;

                switch (attr.AttachmentLevel)
                {
                    case AttributeAttachmentLevel.DataSet:
                    {
                        attrList = datasetAttribues;
                        break;
                    }
                    case AttributeAttachmentLevel.DimensionGroup:
                    {
                        attrList = this._seriesAttributes;
                        break;
                    }
                    case AttributeAttachmentLevel.Observation:
                    {
                        attrList = this._obsAttributes;
                        break;
                    }
                    default:
                    {
                        continue;
                    }
                }

                attrList.Add(new KeyValuePair<string, int>(attr.Id, this.GetIndex(attr)));
            }

            this._datasetAttributes = this.BuildKey(datasetAttribues);
        }

        /// <summary>
        ///     Moves the read position back to the start of the Data Set (<see cref="IDataReaderEngine.KeyablePosition" /> moved
        ///     back to -1)
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.CloseStreams();

            var buffer = new char[9];

            using(var sr = new StreamReader(this.DataLocation.InputStream))
                sr.Read(buffer, 0, 9);

            this.DataLocation.InputStream.Position = 0;
           
            var marker = new string(buffer);

            if(!marker.Substring(0,8).Equals("DATAFLOW"))
                throw new InvalidOperationException("Wrong sdmx-csv input sream");

            this._csvReader = new CsvReader(new StreamReader(this.DataLocation.InputStream), new CsvConfiguration()
            {
                Delimiter = marker.Substring(8),
                HasHeaderRecord = false
            });
        }

        private void CloseStreams()
        {
            if (this._csvReader != null)
            {
                this._csvReader.Dispose();
            }
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected override void Dispose(bool managed)
        {
            if (managed)
            {
                this.CloseStreams();

                if (this.DataLocation != null)
                {
                    this.DataLocation.Close();
                    this.DataLocation = null;
                }
            }

            base.Dispose(managed);
        }

        private Dictionary<string, int> ReadHeaders(string[] input)
        {
            var dict = new Dictionary<string, int>();

            var prefix = "DIM:";

            for (var i = 1; i < input.Length; i++)
            {
                if (input[i].Equals("OBS_VALUE"))
                {
                    this._valueIndex = i;
                    prefix = "ATTR:";
                    continue;
                }

                var header = this.SanitizeValue(input[i]);

                dict.Add(prefix + header, i);
            }

            return dict;
        } 

        private string SanitizeValue(string str)
        {
            return string.IsNullOrEmpty(str) 
                ? str 
                : str.Split(new [] {": "}, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
        }

        private IDatasetHeader GetHeader(string urn)
        {
            urn = this.SanitizeValue(urn);
            var match = urnRegex.Match(urn);

            if(!match.Success)
                throw new InvalidOperationException(string.Format("Dataflow id in a wrong format: [{0}]", urn));

            var dataflowRef = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow))
            {
                MaintainableId = match.Groups[2].Value,
                AgencyId = match.Groups[1].Value,
                Version = match.Groups[3].Value
            };

            return new DatasetHeaderCore(null, null, new DatasetStructureReferenceCore(dataflowRef));
        }

        private bool IsNewSeriesKey()
        {
            return this._seriesKey != this.GetKey();
        }

        private string GetKey()
        {
            return string.Join("_", this._dimensions.Select(kvp => this._buffer[kvp.Value]));
        }

        private bool Read()
        {
            return this.HasNext = this._csvReader.Read();
        }

        private int GetIndex(IDimension dim)
        {
            int index;

            if (!this._csvHeaders.TryGetValue("DIM:" + dim.Id, out index))
                throw new SdmxSemmanticException(String.Format("Dimension with ID [{0}] is missing in a source", dim.Id));

            return index;
        }

        private int GetIndex(IAttributeObject attr)
        {
            int index;

            if (!this._csvHeaders.TryGetValue("ATTR:" + attr.Id, out index))
                throw new SdmxSemmanticException(String.Format("Attribute with ID [{0}] is missing in a source", attr.Id));

            return index;
        }

        private List<IKeyValue> BuildKey(List<KeyValuePair<string, int>> map)
        {
            return new List<IKeyValue>(map.Select(kvp => new KeyValueImpl(this.SanitizeValue(this._buffer[kvp.Value]), kvp.Key)));
        }

    }
}
