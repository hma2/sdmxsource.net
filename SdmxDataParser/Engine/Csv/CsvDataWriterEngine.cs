﻿using System;
using System.Collections.Generic;

namespace Org.Sdmxsource.Sdmx.DataParser.Engine.Csv
{
    using System.IO;

    using Api.Constants;
    using Api.Constants.InterfaceConstant;
    using Api.Engine;
    using Api.Manager.Retrieval;
    using Api.Model;
    using Api.Model.Header;
    using Api.Model.Objects.Base;
    using Api.Model.Objects.DataStructure;
    using Api.Model.SuperObjects.Codelist;
    using Api.Model.SuperObjects.DataStructure;

    using CsvHelper;
    using CsvHelper.Configuration;

    using Model;

    using SdmxObjects.Model.Data;

    using Sdmxsource.Util;

    using Translator;

    using Util.Date;
    using Util.Objects;

    /// <summary>
    /// This class is responsible for writing SDMX-Csv data messages.
    /// https://github.com/sdmx-twg/sdmx-csv/blob/develop/data-message/docs/sdmx-csv-field-guide.md
    /// </summary>
    public class CsvDataWriterEngine : IDataWriterEngine
    {
        private const string DATAFLOW_COLUMN_ID = "DATAFLOW";
        private const string VALUE_COLUMN_ID = "OBS_VALUE";

        private readonly ISdmxSuperObjectRetrievalManager _superObjectRetrievalManager;
        private IDataStructureSuperObject _currentDsdSuperObject;
        private SdmxCsvOptions _csvOptions;
        private ITranslator _translator;
        private DataPosition _currentPosition;
        private Dictionary<string, Column> columnMap;
        private string _dimensionAtObservation;

        private bool _datasetStarted = false;
        private bool _flushObsRequired = false;
        private int _obsevationValueIndex = -1;
        private bool _isClosed;

        private string[] _datasetBuffer;
        private string[] _seriesBuffer;
        private string[] _currentObsBuffer;
        private readonly CsvWriter csvWriter;
        

        /// <summary>
        /// 
        /// </summary>
        public CsvDataWriterEngine(Stream outStream, ISdmxSuperObjectRetrievalManager superObjectRetrievalManager, SdmxCsvOptions options, ITranslator translator)
        {
            this._currentPosition = DataPosition.Dataset;
            this._superObjectRetrievalManager = superObjectRetrievalManager;
            this._csvOptions = options;
            this._translator = translator;

            this.csvWriter = new CsvWriter(new StreamWriter(outStream), new CsvConfiguration()
            {
                Encoding = options.Encoding,
                Delimiter = new string(new [] {options.Delimiter})
            });
        }

        /// <summary>
        /// Writes the closing tags along with the structure.
        /// </summary>
        /// <param name="footer">
        /// The footer.
        /// </param>
        public void Close(params IFooterMessage[] footer)
        {
            this.Dispose();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this._isClosed)
                return;

            this.FlushRow();

            if (this.csvWriter != null)
                this.csvWriter.Dispose();

            this._isClosed = true;
        }

        /// <summary>
        /// This method has no effect
        /// </summary>
        /// <param name="header">
        /// The header.
        /// </param>
        public void WriteHeader(IHeader header)
        { }

        /// <summary>
        /// Starts the dataset. Writes the header
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartDataset(IDataflowObject dataflow, IDataStructureObject dataStructure, IDatasetHeader header, params IAnnotation[] annotations)
        {
            this.CheckClosed();

            if(dataflow == null)
                throw new ArgumentNullException("dataflow");

            if (dataStructure == null)
                throw new ArgumentNullException("dataStructure");

            if (this._datasetStarted)
                throw new InvalidOperationException("Dataset has been already started");

            if (header != null && header.DataStructureReference != null)
            {
                this._dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            if (!ObjectUtil.ValidString(this._dimensionAtObservation))
            {
                this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            }

            this._currentDsdSuperObject = this._superObjectRetrievalManager.GetDataStructureSuperObject(dataStructure.AsReference.MaintainableReference);

            this.WriteCsvHeader(dataflow, dataStructure);
            this._datasetStarted = true;
        }



        /// <summary>
        /// Starts the series.
        /// </summary>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartSeries(params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();
            this.ResetSeriesKey();

            this._currentPosition = DataPosition.SeriesKey;
        }

        /// <summary>
        /// Writes the series key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteSeriesKeyValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();

            if(this._currentPosition >= DataPosition.Observation)
                this.ResetSeriesKey();

            this._currentPosition = DataPosition.SeriesKey;

            Column column;

            if (!this.columnMap.TryGetValue(this.GetDimId(id), out column))
                throw new InvalidOperationException("Invalid Dimension: " + id);

            this._seriesBuffer[column.Index] = this.GetValue(valueRen, column);
        }

        /// <summary>
        /// Writes the attribute value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteAttributeValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();

            Column column;

            if (!this.columnMap.TryGetValue(this.GetAttrId(id), out column))
                throw new InvalidOperationException("Invalid Attribute: " + id);

            var arr = this._seriesBuffer;

            if (this._currentPosition == DataPosition.Observation)
            {
                arr = this._currentObsBuffer;
            }
            else if (this._currentPosition == DataPosition.Dataset)
            {
                arr = this._datasetBuffer;
            }

            arr[column.Index] = this.GetValue(valueRen, column);
        }


        /// <summary>
        /// Start a group.
        /// </summary>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();

            this._currentPosition = DataPosition.Group;
        }

        /// <summary>
        /// Writes the group key value.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="valueRen">
        /// The value.
        /// </param>
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
        }



        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (this._dimensionAtObservation.Equals(DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.All).Value))
            {
                throw new ArgumentException(
                    "Cannot write observation, as no observation concept id was given, and this is writing a flat dataset. "
                    + "Please use the method: writeObservation(String obsConceptId, String obsIdValue, String obsValue, AnnotationBean... annotations)");
            }

            this.WriteObservation(this._dimensionAtObservation, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="observationConceptId">
        /// The observation concept id.
        /// </param>
        /// <param name="obsConceptValue">
        /// The observation concept value.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this.CheckClosed();
            this.CheckDatasetStarted();
            this.FlushRow();

            Column column;

            if (!this.columnMap.TryGetValue(this.GetDimId(observationConceptId), out column))
                throw new InvalidOperationException("Invalid Dimension: " + observationConceptId);

            this._currentPosition = DataPosition.Observation;
            this._currentObsBuffer = new string[this._currentObsBuffer.Length];
            this._currentObsBuffer[column.Index] = obsConceptValue;
            this._currentObsBuffer[this._obsevationValueIndex] = obsValue;
            this._flushObsRequired = true;
        }

        /// <summary>
        /// Writes the observation.
        /// </summary>
        /// <param name="obsTime">
        /// The observation time.
        /// </param>
        /// <param name="obsValue">
        /// The observation value.
        /// </param>
        /// <param name="sdmxSwTimeFormat">
        /// The SDMX time format.
        /// </param>
        /// <param name="annotations">
        /// The annotations.
        /// </param>
        public void WriteObservation(DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            this.WriteObservation(DateUtil.FormatDate(obsTime, sdmxSwTimeFormat), obsValue, annotations);
        }

        #region Private methods

        private void CheckDatasetStarted()
        {
            if (!this._datasetStarted)
            {
                throw new InvalidOperationException("Dataset should be started first");
            }
        }

        /// <summary>
        /// The check closed.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Data Writer has already been closed and cannot have any more information written to it!</exception>
        private void CheckClosed()
        {
            if (this._isClosed)
            {
                throw new InvalidOperationException("Data Writer has already been closed and cannot have any more information written to it!");
            }
        }

        private void WriteCsvHeader(IDataflowObject dataflow, IDataStructureObject dataStructure)
        {
            this.columnMap = new Dictionary<string, Column>();
            var index = 0;

            // DATAFLOW
            this.csvWriter.WriteField(DATAFLOW_COLUMN_ID);
            index++;

            // Dimensions
            foreach (var dim in dataStructure.DimensionList.Dimensions)
            {
                this.columnMap[this.GetDimId(dim.Id)] = new Column(dim.Id, index++);
                this.csvWriter.WriteField(this.GetColumn(dim));
            }

            // VALUE
            this.csvWriter.WriteField(VALUE_COLUMN_ID);
            this._obsevationValueIndex = index++;

            // Attributes
            foreach (var attr in dataStructure.AttributeList.Attributes)
            {
                this.columnMap[this.GetAttrId(attr.Id)] = new Column(attr.Id, index++);
                this.csvWriter.WriteField(this.GetColumn(attr));
            }

            this._datasetBuffer = new string[index];
            this._seriesBuffer = new string[index];
            this._currentObsBuffer = new string[index];
            this._datasetBuffer[0] = UrnUtil.GetUrnPostfix(dataflow.AgencyId, dataflow.Id, dataflow.Version);

            this.csvWriter.NextRecord();
        }

        private string GetAttrId(string id)
        {
            return "ATTR::" + id;
        }

        private string GetDimId(string id)
        {
            return "DIM::" + id;
        }

        private string GetColumn(IComponent component)
        {
            if (!this._csvOptions.UseLabels)
                return component.Id;

            var superObj = this._currentDsdSuperObject.GetComponentById(component.Id);

            return component.Id + ": " + this._translator.GetTranslation(superObj.Concept.Names);
        }

        private string GetValue(string value, Column column)
        {
            if (!this._csvOptions.UseLabels)
                return value;

            return column.GetCodeLabel(this._currentDsdSuperObject, this._translator, value);
        }

        private void FlushRow()
        {
            if (!this._flushObsRequired)
                return;

            this._flushObsRequired = false;

            for (var i = 0; i < this._currentObsBuffer.Length; i++)
            {
                this.csvWriter.WriteField(this._currentObsBuffer[i] ?? this._seriesBuffer[i] ?? this._datasetBuffer[i]);
            }

            this.csvWriter.NextRecord();
        }

        private void ResetSeriesKey()
        {
            this._seriesBuffer = new string[this._seriesBuffer.Length];
        }

        #endregion

        private class Column
        {
            private bool _codelistInited;
            private ICodelistSuperObject _codelist;
            private string _id;
            public int Index { get; private set; }

            public Column(string id, int index)
            {
                this._id = id;
                this.Index = index;
            }

            public string GetCodeLabel(IDataStructureSuperObject currentDsdSuperObject, ITranslator translator, string value)
            {
                if (!this._codelistInited)
                {
                    var superObj = currentDsdSuperObject.GetComponentById(this._id);
                    this._codelist = superObj.GetCodelist(true);
                    this._codelistInited = true;
                }

                if (this._codelist == null)
                    return value;

                var code = this._codelist.GetCodeByValue(value);

                if (code == null)
                    return value;

                return value + ": " + translator.GetTranslation(code.Names);
            }
        }
    }
}
