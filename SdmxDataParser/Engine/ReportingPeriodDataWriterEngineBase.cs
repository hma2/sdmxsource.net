﻿// -----------------------------------------------------------------------
// <copyright file="ReportingPeriodDataWriterEngineBase.cs" company="EUROSTAT">
//   Date Created : 2018-02-21
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;

    using Api.Constants;
    using Api.Constants.InterfaceConstant;
    using Api.Engine;
    using Api.Model;
    using Api.Model.Header;
    using Api.Model.Objects.Base;
    using Api.Model.Objects.DataStructure;

    using Sdmxsource.Util;

    using Util.Date;

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.Api.Engine.IDataWriterEngine" />
    public class ReportingPeriodDataWriterEngineBase : IDataWriterEngine
    {
        /// <summary>
        /// The decorated engine
        /// </summary>
        protected readonly IDataWriterEngine DecoratedEngine;

        /// <summary>
        /// The reporting period converter
        /// </summary>
        protected readonly ReportingTimePeriod ReportingPeriodConverter;

        /// <summary>
        /// The reporting start year date
        /// </summary>
        protected string ReportingStartYearDate = "--01-01";

        /// <summary>
        /// The dimension at observation
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingPeriodDataWriterEngine"/> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        protected ReportingPeriodDataWriterEngineBase(IDataWriterEngine decoratedEngine)
        {
            this.DecoratedEngine = decoratedEngine;
            this.ReportingPeriodConverter = new ReportingTimePeriod();
        }

        /// <inheritdoc />
        public void Close(params IFooterMessage[] footer)
        {
            this.DecoratedEngine.Close(footer);
        }

        /// <inheritdoc />
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.DecoratedEngine.Dispose();
        }

        /// <inheritdoc />
        public virtual void StartDataset(
            IDataflowObject dataflow, IDataStructureObject dsd, IDatasetHeader header,
            params IAnnotation[] annotations)
        {
            this.DecoratedEngine.StartDataset(dataflow, dsd, header, annotations);

            if (header != null && header.DataStructureReference != null)
            {
                this._dimensionAtObservation = header.DataStructureReference.DimensionAtObservation;
            }

            if (!ObjectUtil.ValidString(this._dimensionAtObservation))
            {
                this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            }
        }

        /// <inheritdoc />
        public void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            this.DecoratedEngine.StartGroup(groupId, annotations);
        }

        /// <inheritdoc />
        public void StartSeries(params IAnnotation[] annotations)
        {
            this.DecoratedEngine.StartSeries();
        }

        /// <inheritdoc />
        public virtual void WriteAttributeValue(string id, string valueRen)
        {
            if (id == AttributeObject.Repyearstart)
            {
                this.ReportingStartYearDate = valueRen;
            }
            this.DecoratedEngine.WriteAttributeValue(id, valueRen);
        }

        /// <inheritdoc />
        public void WriteGroupKeyValue(string id, string valueRen)
        {
            this.DecoratedEngine.WriteGroupKeyValue(id, valueRen);
        }

        /// <inheritdoc />
        public void WriteHeader(IHeader header)
        {
            this.DecoratedEngine.WriteHeader(header);
        }

        /// <inheritdoc />
        public void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this.WriteObservation(this._dimensionAtObservation, obsConceptValue, obsValue, annotations);
        }

        /// <inheritdoc />
        public virtual void WriteObservation(
            string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            this.DecoratedEngine.WriteObservation(observationConceptId, obsConceptValue, obsValue, annotations);
        }

        /// <inheritdoc />
        public void WriteObservation(
            DateTime obsTime, string obsValue, TimeFormat sdmxSwTimeFormat, params IAnnotation[] annotations)
        {
            this.WriteObservation(DateUtil.FormatDate(obsTime, sdmxSwTimeFormat), obsValue, annotations);
        }

        /// <inheritdoc />
        public virtual void WriteSeriesKeyValue(string id, string value)
        {
            this.DecoratedEngine.WriteSeriesKeyValue(id, value);
        }
    }
}