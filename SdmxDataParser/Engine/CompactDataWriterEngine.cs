// -----------------------------------------------------------------------
// <copyright file="CompactDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Engine;
    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Properties;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///     This class is responsible for writing Compact SDMX-ML data messages
    /// </summary>
    /// <example>
    ///     A sample in C# for <see cref="CompactDataWriterEngine" />
    ///     <code source="..\ReUsingExamples\DataWriting\ReUsingCompactWriter.cs" lang="cs" />
    /// </example>
    public class CompactDataWriterEngine : DataWriterEngineBase
    {
        /// <summary>
        ///     A flag indicating whether the writer should be closed.
        /// </summary>
        private readonly bool _closeXmlWriter;

        /// <summary>
        ///     The component values
        /// </summary>
        private readonly List<KeyValuePair<string, string>> _componentVals = new List<KeyValuePair<string, string>>();

        /// <summary>
        ///     The namespace used in <c>DataSet</c>, <c>Series</c> and <c>Obs</c> elements. It depends on
        ///     <see cref="IoBase.TargetSchema" />
        /// </summary>
        private NamespacePrefixPair _compactNs;

        /// <summary>
        ///     The dataset annotations
        /// </summary>
        private IAnnotation[] _datasetAnnotations;

        /// <summary>
        ///     The _disposing
        /// </summary>
        private bool _disposed;

        /// <summary>
        ///     The group annotations
        /// </summary>
        private IAnnotation[] _groupAnnotations;

        /// <summary>
        ///     The OBS annotations
        /// </summary>
        private IAnnotation[] _obsAnnotations;

        /// <summary>
        ///     The observation concept. It is affected by SDMX version and the
        ///     <see cref="DataWriterEngineBase.IsCrossSectional" />
        /// </summary>
        private string _obsConcept;

        /// <summary>
        ///     The primary measure concept
        /// </summary>
        private string _primaryMeasureConcept;

        /// <summary>
        ///     The series annotations
        /// </summary>
        private IAnnotation[] _seriesAnnotations;

        /// <summary>
        ///     This field holds a value that indicates whether the dataset element is open.
        /// </summary>
        private bool _startedDataSet;

        /// <summary>
        ///     This field holds a value that indicates whether a group element is open.
        /// </summary>
        private bool _startedGroup;

        /// <summary>
        ///     This field holds a value that indicates whether a Observation element is open.
        /// </summary>
        private bool _startedObservation;

        /// <summary>
        ///     This field holds a value that indicates whether a series element is open.
        /// </summary>
        private bool _startedSeries;

        /// <summary>
        ///     This field holds the total number of groups written to <see cref="Writer.SdmxMLWriter" />
        /// </summary>
        private int _totalGroupsWritten;

        /// <summary>
        ///     This field holds the total number of observations written to <see cref="Writer.SdmxMLWriter" />
        /// </summary>
        private int _totalObservationsWritten;

        /// <summary>
        ///     This field holds the total number of series written to <see cref="Writer.SdmxMLWriter" />
        /// </summary>
        private int _totalSeriesWritten;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CompactDataWriterEngine" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        public CompactDataWriterEngine(XmlWriter writer, SdmxSchema schema)
            : base(writer, schema)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CompactDataWriterEngine" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        public CompactDataWriterEngine(XmlWriter writer, SdmxNamespaces namespaces, SdmxSchema schema)
            : base(writer, namespaces, schema)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CompactDataWriterEngine" /> class.
        /// </summary>
        /// <param name="outStream">The out stream.</param>
        /// <param name="schemaVersion">The schema version.</param>
        public CompactDataWriterEngine(Stream outStream, SdmxSchema schemaVersion)
            : this(outStream, schemaVersion, Encoding.UTF8)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CompactDataWriterEngine" /> class.
        /// </summary>
        /// <param name="outStream">The out stream.</param>
        /// <param name="schemaVersion">The schema version.</param>
        /// <param name="encoding">The encoding.</param>
        public CompactDataWriterEngine(Stream outStream, SdmxSchema schemaVersion, Encoding encoding)
            : this(XmlWriter.Create(outStream, new XmlWriterSettings { Encoding = encoding }), schemaVersion)
        {
            this._closeXmlWriter = true;
        }

        /// <summary>
        ///     Gets the total groups written
        /// </summary>
        public int TotalGroupsWritten
        {
            get
            {
                return this._totalGroupsWritten;
            }
        }

        /// <summary>
        ///     Gets the total number of observation written
        /// </summary>
        public int TotalObservationsWritten
        {
            get
            {
                return this._totalObservationsWritten;
            }
        }

        /// <summary>
        ///     Gets the total Series Written.
        /// </summary>
        public int TotalSeriesWritten
        {
            get
            {
                return this._totalSeriesWritten;
            }
        }

        /// <summary>
        ///     Gets the data format namespace Suffix e.g. compact
        /// </summary>
        protected override BaseDataFormatEnumType DataFormatType
        {
            get
            {
                return BaseDataFormatEnumType.Compact;
            }
        }

        /// <summary>
        ///     Gets the default namespace
        /// </summary>
        protected override NamespacePrefixPair DefaultNS
        {
            get
            {
                return this.Namespaces.DataSetStructureSpecific;
            }
        }

        /// <summary>
        ///     Gets the Message element tag
        /// </summary>
        protected override string MessageElement
        {
            get
            {
                return
                    NameTableCache.GetElementName(
                        this.IsTwoPointOne ? ElementNameTable.StructureSpecificData : ElementNameTable.CompactData);
            }
        }

        /// <summary>
        ///     Starts a dataset with the data conforming to the DSD
        /// </summary>
        /// <param name="dataflow">Optional. The dataflow can be provided to give extra information about the dataset.</param>
        /// <param name="dsd">The <see cref="IDataStructureObject" /> for which the dataset will be created</param>
        /// <param name="header">The <see cref="IHeader" /> of the dataset</param>
        /// <param name="annotations">
        ///     Any additional annotations that are attached to the dataset, can be null if no annotations
        ///     exist
        /// </param>
        /// <exception cref="System.ArgumentNullException">if the <paramref name="dsd" /> is null</exception>
        public override void StartDataset(
            IDataflowObject dataflow, 
            IDataStructureObject dsd, 
            IDatasetHeader header, 
            params IAnnotation[] annotations)
        {
            base.StartDataset(dataflow, dsd, header, annotations);
            this._primaryMeasureConcept = this.GetComponentId(dsd.PrimaryMeasure);
            this._compactNs = this.TargetSchema.EnumType != SdmxSchemaEnumType.VersionTwoPointOne ? this.Namespaces.DataSetStructureSpecific : NamespacePrefixPair.Empty;
            this._obsConcept = this.IsTwoPointOne ? this.DimensionAtObservation : ConceptRefUtil.GetConceptId(this.KeyFamily.TimeDimension.ConceptRef);

            // we must store the annotations because the element still open and attributes may be written.
            this._datasetAnnotations = annotations;
        }

        /// <summary>
        ///     Start a group with <paramref name="groupId" />
        /// </summary>
        /// <param name="groupId">The group id and the element name</param>
        /// <param name="annotations">
        ///     Annotations any additional annotations that are attached to the group, can be null if no
        ///     annotations exist
        /// </param>
        /// <exception cref="System.InvalidOperationException">Started a group after starting series.</exception>
        /// <exception cref="InvalidOperationException">Cannot call a StartGroup after StartSeries</exception>
        public override void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            base.StartGroup(groupId, annotations);
            if (this._startedSeries)
            {
                throw new InvalidOperationException(Resources.ErrorStartGroupAfterStartSeries);
            }

            this.FlushDataSetAnnotations21();
            this.EndGroup();

            // Store annotations for both 2.0 and 2.1 as a group doesn't have any other child elements
            this._groupAnnotations = annotations;

            // TODO check java for namespace in 2.0 and 2.1
            if (this.TargetSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne)
            {
                this.WriteStartElement(ElementNameTable.Group);
                string format = string.Format(
                    CultureInfo.InvariantCulture, 
                    "{0}:{1}", 
                    this.Namespaces.DataSetStructureSpecific.Prefix, 
                    groupId);
                this.WriteAttributeString(this.Namespaces.Xsi, AttributeNameTable.type, format);
            }
            else
            {
                this.WriteStartElement(this.Namespaces.DataSetStructureSpecific, groupId);
            }

            this._startedGroup = true;
            this._totalGroupsWritten++;
        }

        /// <summary>
        ///     Start a series
        /// </summary>
        /// <param name="annotations">
        ///     Any additional annotations that are attached to the series, can be null if no annotations
        ///     exist
        /// </param>
        public override void StartSeries(params IAnnotation[] annotations)
        {
            this.FlushDataSetAnnotations21();
            this.EndGroup();

            // check if there is an already open series element
            this.EndSeries();
            base.StartSeries(annotations);
            this._startedSeries = true;
            if (this.IsFlat)
            {
                this._componentVals.Clear();
            }
            else
            {
                // write the series element
                this.WriteStartElement(this._compactNs, ElementNameTable.Series);

                this._totalSeriesWritten++;
            }

            this._seriesAnnotations = annotations;
        }

        /// <summary>
        ///     Write an <paramref name="attribute" /> and the <paramref name="valueRen" />
        /// </summary>
        /// <param name="attribute">
        ///     The attribute concept id
        /// </param>
        /// <param name="valueRen">
        ///     The value
        /// </param>
        public override void WriteAttributeValue(string attribute, string valueRen)
        {
            attribute = this.GetComponentId(attribute);
            base.WriteAttributeValue(attribute, valueRen);
            if (this.IsFlat && this._startedSeries && !this._startedObservation)
            {
                this._componentVals.Add(new KeyValuePair<string, string>(attribute, valueRen));
            }
            else
            {
                this.WriteAttributeString(attribute, valueRen);
            }
        }

        /// <summary>
        ///     Write a group <paramref name="key" /> and the <paramref name="valueRen" />
        /// </summary>
        /// <param name="key">
        ///     The key. i.e. the dimension
        /// </param>
        /// <param name="valueRen">
        ///     The value
        /// </param>
        /// <exception cref="InvalidOperationException">Start Group has not be called or Start series started..</exception>
        public override void WriteGroupKeyValue(string key, string valueRen)
        {
            key = this.GetComponentId(key);
            base.WriteGroupKeyValue(key, valueRen);
            if (this._startedGroup)
            {
                this.WriteAttributeString(key, valueRen);
            }
            else
            {
                throw new InvalidOperationException(Resources.ErrorStartGroupNotCalledOrStartSeriesStarted);
            }
        }

        /// <summary>
        ///     Writes an observation, the observation concept is assumed to be that which has been defined to be at the
        ///     observation level (as declared in the start dataset method DatasetHeaderObject).
        /// </summary>
        /// <param name="obsConceptValue">May be the observation time, or the cross section value</param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">
        ///     Any additional annotations that are attached to the observation, can be null if no
        ///     annotations exist
        /// </param>
        /// <exception cref="System.ArgumentException">
        ///     Can not write observation, as no observation concept id was given, and this is writing a flat dataset.  +
        ///     Please use the method:
        ///     <c>
        ///         WriteObservation(string observationConceptId, string obsConceptValue, string primaryMeasureValue, params
        ///         IAnnotation[] annotations)
        ///     </c>
        /// </exception>
        public override void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (this.IsFlat)
            {
                throw new ArgumentException(
                    "Can not write observation, as no observation concept id was given, and this is writing a flat dataset. "
                    + "Please use the method: WriteObservation(string observationConceptId, string obsConceptValue, string primaryMeasureValue, params IAnnotation[] annotations)");
            }

            this.WriteObservation(this._obsConcept, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        ///     Write the <paramref name="observationConceptId" /> and the <paramref name="primaryMeasureValue" />
        /// </summary>
        /// <param name="observationConceptId">The observation component ID</param>
        /// <param name="obsConceptValue">The co</param>
        /// <param name="primaryMeasureValue">The primary measure value</param>
        /// <param name="annotations">The annotations.</param>
        /// <exception cref="System.InvalidOperationException"><see cref="StartSeries" /> not called for SDMX v2.0 dataset</exception>
        public override void WriteObservation(
            string observationConceptId, 
            string obsConceptValue, 
            string primaryMeasureValue, 
            params IAnnotation[] annotations)
        {
            this.FlushSeriesAnnotations21();

            observationConceptId = this.GetComponentId(observationConceptId);
            base.WriteObservation(observationConceptId, obsConceptValue, primaryMeasureValue, annotations);

            if (!this._startedSeries && this.TargetSchema.EnumType != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                throw new InvalidOperationException(Resources.ErrorStartSeriesNotCalled);
            }

            this.EndObservation();

            this.WriteStartElement(this._compactNs, ElementNameTable.Obs);
            if (this.IsFlat)
            {
                foreach (var keyValuePair in this._componentVals)
                {
                    this.WriteAttributeString(keyValuePair.Key, keyValuePair.Value);
                }
            }

            this.WriteAttributeString(observationConceptId, obsConceptValue);
            string obsValue = string.IsNullOrEmpty(primaryMeasureValue) ? this.DefaultObs : primaryMeasureValue;
            this.TryWriteAttribute(this._primaryMeasureConcept, obsValue);
            this._startedObservation = true;
            this._totalObservationsWritten++;

            // Store annotations for both 2.0 and 2.1 since Observation doesn't have any child elements.
            if (!IsFlat)
            {
                this._obsAnnotations = annotations; // STORE THE ANNOTATIONS
            }
            else
            {
                var mergedSeriesObsAnnotations = new List<IAnnotation>();
                if (this._seriesAnnotations != null)
                {
                    mergedSeriesObsAnnotations.AddRange(this._seriesAnnotations);
                }

                if (annotations != null)
                {
                    mergedSeriesObsAnnotations.AddRange(annotations);
                }

                this._obsAnnotations = mergedSeriesObsAnnotations.ToArray();
            }
        }

        /// <summary>
        ///     Write a series <paramref name="key" /> and the <paramref name="value" />
        /// </summary>
        /// <param name="key">
        ///     The key. i.e. the dimension
        /// </param>
        /// <param name="value">
        ///     The value
        /// </param>
        public override void WriteSeriesKeyValue(string key, string value)
        {
            key = this.GetComponentId(key);
            base.WriteSeriesKeyValue(key, value);
            if (this.IsFlat)
            {
                this._componentVals.Add(new KeyValuePair<string, string>(key, value));
            }
            else
            {
                if (!this._startedSeries)
                {
                    this.StartSeries();
                }

                this.WriteAttributeString(key, value);
            }
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposable">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected override void Dispose(bool disposable)
        {
            if (!this._disposed)
            {
                if (disposable)
                {
                    this.EndGroup();
                    this.EndObservation();
                    this.EndSeries();
                    this.EndDataSet();
                    if (this.IsTwoPointOne)
                    {
                        this.WriteFooter(this.FooterMessage);
                    }

                    switch (this.SdmxMLWriter.WriteState)
                    {
                        case WriteState.Start:
                        case WriteState.Closed:
                        case WriteState.Error:
                            break;
                        default:
                            this.CloseMessageTag();
                            break;
                    }

                    if (this._closeXmlWriter)
                    {
                        this.SdmxMLWriter.Close();
                    }
                }

                this._disposed = true;
            }

            base.Dispose(disposable);
        }

        /// <summary>
        ///     Conditionally end DataSet element
        /// </summary>
        protected override void EndDataSet()
        {
            if (!this.IsTwoPointOne)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, this._datasetAnnotations);
                this._datasetAnnotations = null;
            }

            base.EndDataSet();
            if (this._startedDataSet)
            {
                this._startedDataSet = false;
            }
        }

        /// <summary>
        ///     Conditionally start the DataSet if <see cref="_startedDataSet" /> is false
        /// </summary>
        /// <param name="header">
        ///     The dataset header
        /// </param>
        protected override void WriteFormatDataSet(IDatasetHeader header)
        {
            if (this._startedDataSet)
            {
                this.EndGroup();
                this.EndObservation();
                this.EndSeries();
                this.EndDataSet();
            }

            NamespacePrefixPair dataSetNS = this.TargetSchema.EnumType != SdmxSchemaEnumType.VersionTwoPointOne
                                                ? this.Namespaces.DataSetStructureSpecific
                                                : this.Namespaces.Message;
            this.WriteStartElement(dataSetNS, ElementNameTable.DataSet);
            this.WriteDataSetHeader(header);
            this._startedDataSet = true;
        }

        /// <summary>
        /// Flushes the data set annotations for SDMX v21.
        /// </summary>
        private void FlushDataSetAnnotations21()
        {
            if (this.IsTwoPointOne && this._datasetAnnotations != null)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, this._datasetAnnotations);
                this._datasetAnnotations = null;
            }
        }

        /// <summary>
        /// Flushes the series annotations for SDMX v2.1
        /// </summary>
        private void FlushSeriesAnnotations21()
        {
            if (this._seriesAnnotations != null && this.IsTwoPointOne && !this.IsFlat)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, this._seriesAnnotations);
                this._seriesAnnotations = null;
            }
        }

        /// <summary>
        ///     Conditionally end group element
        /// </summary>
        private void EndGroup()
        {
            if (this._startedGroup)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, this._groupAnnotations);
                this._groupAnnotations = null;

                this.WriteEndElement();
                this._startedGroup = false;
            }
        }

        /// <summary>
        ///     Conditionally end observation
        /// </summary>
        private void EndObservation()
        {
            if (this._startedObservation)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, this._obsAnnotations);
                this._obsAnnotations = null;

                this.WriteEndElement();
                this._startedObservation = false;
            }
        }

        /// <summary>
        ///     Conditionally end series
        /// </summary>
        private void EndSeries()
        {
            // save it before it gets reset by EndObservation. 
            var hasObservation = this._startedObservation;

            this.EndObservation();

            if (this._startedSeries)
            {
                if (!this.IsTwoPointOne)
                {
                    this.WriteAnnotations(ElementNameTable.Annotations, this._seriesAnnotations);
                    this._seriesAnnotations = null;
                }
                else if (!hasObservation)
                {
                    // we need this in case we need to write only series. e.g. details=nodata or serieskeyonly
                    // if we have written an observation then we don't need to call this here as annotations go before observations in 2.1
                    // but if we didn't we need to call this
                    this.FlushSeriesAnnotations21();
                }

                if (!this.IsFlat)
                {
                    // in which case close it
                    this.WriteEndElement();
                }

                this._startedSeries = false;
            }
        }
    }
}