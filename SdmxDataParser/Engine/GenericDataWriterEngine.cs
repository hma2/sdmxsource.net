// -----------------------------------------------------------------------
// <copyright file="GenericDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Estat.Sri.SdmxParseBase.Engine;
    using Estat.Sri.SdmxParseBase.Model;
    using Estat.Sri.SdmxXmlConstants;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Properties;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///     The generic stream data writer.
    /// </summary>
    /// <example>
    ///     A sample in C# for <see cref="GenericDataWriterEngine" />
    ///     <code source="..\ReUsingExamples\DataWriting\ReUsingGenericWriter.cs" lang="cs" />
    /// </example>
    public class GenericDataWriterEngine : DataWriterEngineBase
    {
        /// <summary>
        ///     A flag indicating whether the writer should be closed.
        /// </summary>
        private readonly bool _closeXmlWriter;

        /// <summary>
        ///     The component values
        /// </summary>
        private readonly List<KeyValuePair<string, string>> _componentVals = new List<KeyValuePair<string, string>>();

        /// <summary>
        ///     The concept attribute
        /// </summary>
        private readonly AttributeNameTable _conceptAttribute;
        
        /// <summary>
        /// The attributes
        /// </summary>
        private readonly List<KeyValuePair<string, string>> _attributes = new List<KeyValuePair<string, string>>();

        /// <summary>
        ///     The dataset annotations
        /// </summary>
        private IAnnotation[] _datasetAnnotations;

        /// <summary>
        ///     The _dimension at observation
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        ///     The _disposed
        /// </summary>
        private bool _disposed;

        /// <summary>
        ///     The group annotations
        /// </summary>
        private IAnnotation[] _groupAnnotations;

        /// <summary>
        ///     The OBS annotations
        /// </summary>
        private IAnnotation[] _obsAnnotations;

        /// <summary>
        ///     The series annotations
        /// </summary>
        private IAnnotation[] _seriesAnnotations;

        /// <summary>
        ///     This field holds a value that indicates whether the attribute element is open.
        /// </summary>
        private bool _startedAttributes;

        /// <summary>
        ///     This field holds a value that indicates whether the dataset element is open.
        /// </summary>
        private bool _startedDataSet;

        /// <summary>
        ///     This field holds a value that indicates whether a group element is open.
        /// </summary>
        private bool _startedGroup;

        /// <summary>
        ///     This field holds a value that indicates whether the *key element is open.
        /// </summary>
        private bool _startedKey;

        /// <summary>
        ///     This field holds a value that indicates whether a Observation element is open.
        /// </summary>
        private bool _startedObservation;

        /// <summary>
        ///     This field holds a value that indicates whether a series element is open.
        /// </summary>
        private bool _startedSeries;

        /// <summary>
        ///     This field holds the total number of groups written to <see cref="Writer.SdmxMLWriter" />
        /// </summary>
        private int _totalGroupsWritten;

        /// <summary>
        ///     This field holds the total number of observations written to <see cref="Writer.SdmxMLWriter" />
        /// </summary>
        private int _totalObservationsWritten;

        /// <summary>
        ///     This field holds the total number of series written to <see cref="Writer.SdmxMLWriter" />
        /// </summary>
        private int _totalSeriesWritten;

        /// <summary>
        ///     The write observation method.
        /// </summary>
        private Action<string, string> _writeObservationMethod;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GenericDataWriterEngine" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="schema">The schema.</param>
        public GenericDataWriterEngine(Stream writer, SdmxSchema schema)
            : this(writer, schema, Encoding.UTF8)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GenericDataWriterEngine" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="schema">The schema.</param>
        /// <param name="encoding">The encoding.</param>
        public GenericDataWriterEngine(Stream writer, SdmxSchema schema, Encoding encoding)
            : this(XmlWriter.Create(writer, new XmlWriterSettings { Encoding = encoding }), schema)
        {
            this._closeXmlWriter = true;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GenericDataWriterEngine" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        public GenericDataWriterEngine(XmlWriter writer, SdmxSchema schema)
            : base(writer, schema)
        {
            if (this.IsTwoPointOne)
            {
                this._conceptAttribute = AttributeNameTable.id;
                this._writeObservationMethod = this.WriteObservation21;
            }
            else
            {
                this._conceptAttribute = AttributeNameTable.concept;
                this._writeObservationMethod = this.WriteObservation20;
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GenericDataWriterEngine" /> class.
        /// </summary>
        /// <param name="writer">
        ///     The writer.
        /// </param>
        /// <param name="namespaces">
        ///     The namespaces.
        /// </param>
        /// <param name="schema">
        ///     The schema.
        /// </param>
        public GenericDataWriterEngine(XmlWriter writer, SdmxNamespaces namespaces, SdmxSchema schema)
            : base(writer, namespaces, schema)
        {
            if (this.IsTwoPointOne)
            {
                this._conceptAttribute = AttributeNameTable.id;
                this._writeObservationMethod = this.WriteObservation21;
            }
            else
            {
                this._conceptAttribute = AttributeNameTable.concept;
                this._writeObservationMethod = this.WriteObservation20;
            }
        }

        /// <summary>
        ///     Gets the total groups written
        /// </summary>
        public int TotalGroupsWritten
        {
            get
            {
                return this._totalGroupsWritten;
            }
        }

        /// <summary>
        ///     Gets the total number of observation written
        /// </summary>
        public int TotalObservationsWritten
        {
            get
            {
                return this._totalObservationsWritten;
            }
        }

        /// <summary>
        ///     Gets the total Series Written.
        /// </summary>
        public int TotalSeriesWritten
        {
            get
            {
                return this._totalSeriesWritten;
            }
        }

        /// <summary>
        ///     Gets DataFormatType.
        /// </summary>
        protected override BaseDataFormatEnumType DataFormatType
        {
            get
            {
                return BaseDataFormatEnumType.Generic;
            }
        }

        /// <summary>
        ///     Gets the default namespace
        /// </summary>
        protected override NamespacePrefixPair DefaultNS
        {
            get
            {
                return this.Namespaces.Generic;
            }
        }

        /// <summary>
        ///     Gets MessageElement.
        /// </summary>
        protected override string MessageElement
        {
            get
            {
                return NameTableCache.GetElementName(ElementNameTable.GenericData);
            }
        }

        /// <summary>
        ///     Starts a dataset with the data conforming to the DSD
        /// </summary>
        /// <param name="dataflow">Optional. The dataflow can be provided to give extra information about the dataset.</param>
        /// <param name="dsd">The <see cref="IDataStructureObject" /> for which the dataset will be created</param>
        /// <param name="header">The <see cref="IHeader" /> of the dataset</param>
        /// <param name="annotations">
        ///     Any additional annotations that are attached to the dataset, can be null if no annotations
        ///     exist
        /// </param>
        /// <exception cref="System.ArgumentNullException">if the <paramref name="dsd" /> is null</exception>
        public override void StartDataset(
            IDataflowObject dataflow, 
            IDataStructureObject dsd, 
            IDatasetHeader header, 
            params IAnnotation[] annotations)
        {
            base.StartDataset(dataflow, dsd, header, annotations);
            if (this.IsTwoPointOne)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, annotations);
            }
            else
            {
                this._datasetAnnotations = annotations;
            }

            if (this.IsFlat)
            {
                this._writeObservationMethod = this.WriteObservation21Flat;
            }
        }

        /// <summary>
        /// Starts a group with the given id, the subsequent calls to <c>writeGroupKeyValue</c> will write the id/values to
        /// this group.  After
        /// the group key is complete <c>writeAttributeValue</c> may be called to add attributes to this group.
        /// <p /><b>Example Usage</b>
        /// A group 'demo' is made up of 3 concepts (Country/Sex/Status), and has an attribute (Collection).
        /// <code>
        /// DataWriterEngine dre = //Create instance
        /// dre.StartGroup("demo");
        /// dre.WriteGroupKeyValue("Country", "FR");
        /// dre.WriteGroupKeyValue("Sex", "M");
        /// dre.WriteGroupKeyValue("Status", "U");
        /// dre.WriteAttributeValue("Collection", "A");
        /// </code>
        /// Any subsequent calls, for example to start a series, or to start a new group, will close off this exiting group.
        /// </summary>
        /// <param name="groupId">The Group ID</param>
        /// <param name="annotations">Annotations any additional annotations that are attached to the group, can be null if no
        /// annotations exist</param>
        /// <exception cref="InvalidOperationException">The exception</exception>
        public override void StartGroup(string groupId, params IAnnotation[] annotations)
        {
            base.StartGroup(groupId, annotations);
            if (this._startedSeries)
            {
                throw new InvalidOperationException(Resources.ErrorStartGroupAfterStartSeries);
            }

            this.EndAttribute();
            this.EndKey();
            this.EndGroup();

            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Group);
            string type = !string.IsNullOrEmpty(groupId) ? groupId : "Sibling";
            this.WriteAttributeString(AttributeNameTable.type, type);
            this._totalGroupsWritten++;
            this._startedGroup = true;

            if (this.IsTwoPointOne)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, annotations);
            }
            else
            {
                this._groupAnnotations = annotations;
            }
        }

        /// <summary>
        ///     Start a series
        /// </summary>
        /// <param name="annotations">
        ///     Any additional annotations that are attached to the series, can be null if no annotations
        ///     exist
        /// </param>
        public override void StartSeries(params IAnnotation[] annotations)
        {
            base.StartSeries(annotations);
            this.EndAttribute();
            this.EndKey();
            this.EndGroup();
            this.EndObservation();
            this.EndSeries();
            this._seriesAnnotations = null;
            if (this.IsFlat)
            {
                this._componentVals.Clear();
                this._attributes.Clear();
                this._seriesAnnotations = annotations;
            }
            else
            {
                // <generic:Series>
                this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Series);
                this._totalSeriesWritten++;
                if (this.IsTwoPointOne)
                {
                    this.WriteAnnotations(ElementNameTable.Annotations, annotations);
                }
                else
                {
                    this._seriesAnnotations = annotations;
                }
            }

            this._startedSeries = true;
        }

        /// <summary>
        ///     Write an <paramref name="attribute" /> and the <paramref name="valueRen" />
        /// </summary>
        /// <param name="attribute">
        ///     The attribute concept id
        /// </param>
        /// <param name="valueRen">
        ///     The value
        /// </param>
        public override void WriteAttributeValue(string attribute, string valueRen)
        {
            attribute = this.GetComponentId(attribute);
            base.WriteAttributeValue(attribute, valueRen);
            this.EndKey();
            if (this.IsFlat && this._startedSeries && !this._startedObservation)
            {
                this._attributes.Add(new KeyValuePair<string, string>(attribute, valueRen));
            }
            else
            {
                this.StartAttribute();
                this.WriteConceptValue(attribute, valueRen);
            }
        }

        /// <summary>
        ///     Write a group <paramref name="key" /> and the <paramref name="valueRen" />
        /// </summary>
        /// <param name="key">
        ///     The key. i.e. the dimension
        /// </param>
        /// <param name="valueRen">
        ///     The value
        /// </param>
        public override void WriteGroupKeyValue(string key, string valueRen)
        {
            key = this.GetComponentId(key);
            base.WriteGroupKeyValue(key, valueRen);
            if (this._startedGroup)
            {
                this.StartKey(ElementNameTable.GroupKey);
                this.WriteConceptValue(key, valueRen);
            }
            else
            {
                throw new InvalidOperationException(Resources.ErrorStartGroupNotCalledOrStartSeriesStarted);
            }
        }

        /// <summary>
        ///     Writes an observation, the observation concept is assumed to be that which has been defined to be at the
        ///     observation level (as declared in the start dataset method DatasetHeaderObject).
        /// </summary>
        /// <param name="obsConceptValue">May be the observation time, or the cross section value </param>
        /// <param name="obsValue">The observation value - can be numerical</param>
        /// <param name="annotations">
        ///     Any additional annotations that are attached to the observation, can be null if no
        ///     annotations exist
        /// </param>
        /// <exception cref="ArgumentException">Can not write observation, as no observation concept id was given, and this is writing a flat dataset. Please use the method: <c>WriteObservation(string observationConceptId, string obsConceptValue, string primaryMeasureValue, params IAnnotation[] annotations)</c></exception>
        public override void WriteObservation(string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (this.IsFlat)
            {
                throw new ArgumentException(
                    "Can not write observation, as no observation concept id was given, and this is writing a flat dataset. Please use the method: WriteObservation(string observationConceptId, string obsConceptValue, string primaryMeasureValue, params IAnnotation[] annotations)");
            }

            string obsConcept = this.IsTwoPointOne
                                    ? this._dimensionAtObservation
                                    : ConceptRefUtil.GetConceptId(this.KeyFamily.TimeDimension.ConceptRef);

            this.WriteObservation(obsConcept, obsConceptValue, obsValue, annotations);
        }

        /// <summary>
        ///     Writes an Observation value against the current series.
        ///     <p />
        ///     The current series is determined by the latest writeKeyValue call,
        ///     If this is a cross sectional dataset, then the <paramref name="observationConceptId" /> is expected to be the cross
        ///     sectional concept value - for example if this is cross sectional on Country the id may be "FR"
        ///     If this is a time series dataset then the <paramref name="observationConceptId" /> is expected to be the
        ///     observation time - for example 2006-12-12
        ///     <p />
        /// </summary>
        /// <param name="observationConceptId"> the observation dimension id </param>
        /// <param name="obsConceptValue"> the observation dimension value </param>
        /// <param name="primaryMeasureValue">
        ///     the observation value - can be numerical
        /// </param>
        /// <param name="annotations">The observation annotations </param>
        public override void WriteObservation(
            string observationConceptId, 
            string obsConceptValue, 
            string primaryMeasureValue, 
            params IAnnotation[] annotations)
        {
            observationConceptId = this.GetComponentId(observationConceptId);
            if (observationConceptId == null)
            {
                throw new ArgumentNullException("observationConceptId");
            }

            base.WriteObservation(observationConceptId, obsConceptValue, primaryMeasureValue, annotations);

            this.EndAttribute();
            this.EndKey();
            this.EndObservation();

            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Obs);
            string obsValue = string.IsNullOrEmpty(primaryMeasureValue) ? this.DefaultObs : primaryMeasureValue;
            if (this.IsTwoPointOne)
            {
                var mergedSeriesObsAnnotations = new List<IAnnotation>();
                if (this._seriesAnnotations != null && this.IsFlat)
                {
                    mergedSeriesObsAnnotations.AddRange(this._seriesAnnotations);
                }

                if (annotations != null)
                {
                    mergedSeriesObsAnnotations.AddRange(annotations);
                }

                this.WriteAnnotations(ElementNameTable.Annotations, mergedSeriesObsAnnotations.ToArray());
            }
            else
            {
                this._obsAnnotations = annotations;
            }

            this._writeObservationMethod(observationConceptId, obsConceptValue);

            if (!string.IsNullOrEmpty(obsValue))
            {
                // <generic:ObsValue value="3.14"/>
                this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.ObsValue);
                this.WriteAttributeString(AttributeNameTable.value, obsValue);
                this.WriteEndElement();
            }

            this.WriteAttributes();
            this._startedObservation = true;
            this._totalObservationsWritten++;
        }

        /// <summary>
        ///     Write a series <paramref name="key" /> and the <paramref name="value" />
        /// </summary>
        /// <param name="key">
        ///     The key. i.e. the dimension
        /// </param>
        /// <param name="value">
        ///     The value
        /// </param>
        public override void WriteSeriesKeyValue(string key, string value)
        {
            key = this.GetComponentId(key);
            base.WriteSeriesKeyValue(key, value);
            if (this.IsFlat)
            {
                this._componentVals.Add(new KeyValuePair<string, string>(key, value));
            }
            else
            {
                if (!this._startedSeries)
                {
                    this.StartSeries();
                }

                this.StartKey(ElementNameTable.SeriesKey);
                this.WriteConceptValue(key, value);
            }
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposable">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected override void Dispose(bool disposable)
        {
            if (!this._disposed)
            {
                if (disposable)
                {
                    this.EndKey();
                    this.EndAttribute();
                    this.EndGroup();
                    this.EndObservation();
                    this.EndSeries();
                    this.EndDataSet();
                    if (this.IsTwoPointOne)
                    {
                        this.WriteFooter(this.FooterMessage);
                    }

                    this.CloseMessageTag();

                    if (this._closeXmlWriter)
                    {
                        this.SdmxMLWriter.Close();
                    }
                }

                this._disposed = true;
            }

            base.Dispose(disposable);
        }

        /// <summary>
        ///     Conditionally end DataSet element
        /// </summary>
        protected override void EndDataSet()
        {
            if (!this.IsTwoPointOne)
            {
                this.WriteAnnotations(ElementNameTable.Annotations, this._datasetAnnotations);
                this._datasetAnnotations = null;
            }

            base.EndDataSet();
            if (this._startedDataSet)
            {
                this._startedDataSet = false;
            }
        }

        /// <summary>
        ///     Conditionally start the DataSet if <see cref="_startedDataSet" /> is false
        /// </summary>
        /// <param name="header">
        ///     The dataset header
        /// </param>
        protected override void WriteFormatDataSet(IDatasetHeader header)
        {
            if (this._startedDataSet)
            {
                this.EndKey();
                this.EndAttribute();
                this.EndGroup();
                this.EndObservation();
                this.EndSeries();
                this.EndDataSet();
            }

            this._dimensionAtObservation = this.GetDimensionAtObservation(header);

            this.WriteStartElement(this.Namespaces.Message, ElementNameTable.DataSet);
            this.WriteDataSetHeader(header);

            if (this.TargetSchema.EnumType != SdmxSchemaEnumType.VersionTwoPointOne)
            {
                this.TryToWriteElement(this.Namespaces.Generic, ElementNameTable.KeyFamilyRef, this.KeyFamily.Id);
            }

            this._startedDataSet = true;
        }

        /// <summary>
        ///     End a <see cref="ElementNameTable.Attributes" /> is <see cref="_startedAttributes" /> is true
        /// </summary>
        private void EndAttribute()
        {
            if (!this._startedAttributes)
            {
                return;
            }

            this.WriteEndElement();
            this._startedAttributes = false;
        }
        
        /// <summary>
        /// Writes the attributes.
        /// </summary>
        private void WriteAttributes()
        {
            foreach (var attribute in this._attributes)
            {
                this.StartAttribute();
                this.WriteConceptValue(attribute.Key, attribute.Value);
            }
        }

        /// <summary>
        ///     Conditionally end group element
        /// </summary>
        private void EndGroup()
        {
            if (this._startedGroup)
            {
                if (!this.IsTwoPointOne)
                {
                    this.WriteAnnotations(ElementNameTable.Annotations, this._groupAnnotations);
                    this._groupAnnotations = null;
                }

                this.WriteEndElement();
                this._startedGroup = false;
            }
        }

        /// <summary>
        ///     End element if <see cref="_startedKey" /> is true
        /// </summary>
        private void EndKey()
        {
            if (!this._startedKey)
            {
                return;
            }

            this.WriteEndElement();
            this._startedKey = false;
        }

        /// <summary>
        ///     Conditionally end observation
        /// </summary>
        private void EndObservation()
        {
            if (this._startedObservation)
            {
                if (!this.IsTwoPointOne)
                {
                    this.WriteAnnotations(ElementNameTable.Annotations, this._obsAnnotations);
                    this._obsAnnotations = null;
                }

                this.WriteEndElement();
                this._startedObservation = false;
            }
        }

        /// <summary>
        ///     Conditionally end series
        /// </summary>
        private void EndSeries()
        {
            if (IsFlat)
            {
                return;
            }

            if (this._startedSeries)
            {
                if (!this.IsTwoPointOne)
                {
                    this.WriteAnnotations(ElementNameTable.Annotations, this._seriesAnnotations);
                    this._seriesAnnotations = null;
                }

                // in which case close it
                this.WriteEndElement();
                this._startedSeries = false;
            }
        }

        /// <summary>
        ///     Start a <see cref="ElementNameTable.Attributes" /> is <see cref="_startedAttributes" /> is false
        /// </summary>
        private void StartAttribute()
        {
            if (this._startedAttributes)
            {
                return;
            }

            // <generic:GroupKey>
            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Attributes);
            this._startedAttributes = true;
        }

        /// <summary>
        ///     Start a <see cref="ElementNameTable.SeriesKey" /> or a <see cref="ElementNameTable.GroupKey" /> is
        ///     <see cref="_startedKey" />
        ///     is false
        /// </summary>
        /// <param name="keyTag">
        ///     The  a <see cref="ElementNameTable.SeriesKey" /> or a <see cref="ElementNameTable.GroupKey" />
        /// </param>
        private void StartKey(ElementNameTable keyTag)
        {
            if (this._startedKey)
            {
                return;
            }

            // <generic:GroupKey>
            this.WriteStartElement(this.Namespaces.Generic, keyTag);
            this._startedKey = true;
        }

        /// <summary>
        ///     Write <see cref="ElementNameTable.Value" /> with <paramref name="concept" /> and <paramref name="value" />
        /// </summary>
        /// <param name="concept">
        ///     The concept id
        /// </param>
        /// <param name="value">
        ///     The value
        /// </param>
        private void WriteConceptValue(string concept, string value)
        {
            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Value);

            this.WriteAttributeString(this._conceptAttribute, concept);
            this.WriteAttributeString(AttributeNameTable.value, value);

            this.WriteEndElement();
        }

        /// <summary>
        ///     Write the SDMX 2.0 part of the observation .
        /// </summary>
        /// <param name="obsConcept">
        ///     The observation concept.
        /// </param>
        /// <param name="obsConceptValue">The observation concept value.</param>
        private void WriteObservation20(string obsConcept, string obsConceptValue)
        {
            if (!this._startedSeries)
            {
                throw new InvalidOperationException(Resources.ErrorStartSeriesNotCalled);
            }

            // <generic:Time>2000-01</generic:Time>
            this.WriteElement(this.Namespaces.Generic, ElementNameTable.Time, obsConceptValue);
        }

        /// <summary>
        ///     Write the SDMX 2.1 part of the observation .
        /// </summary>
        /// <param name="obsConcept">The observation concept.</param>
        /// <param name="obsConceptValue">The observation concept value.</param>
        private void WriteObservation21(string obsConcept, string obsConceptValue)
        {
            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.ObsDimension);

            //// if (this.IsCrossSectional)
            //// {
            this.WriteAttributeString(AttributeNameTable.id, obsConcept);

            //// }
            this.WriteAttributeString(AttributeNameTable.value, obsConceptValue);
            this.WriteEndElement();
        }

        /// <summary>
        ///     Write the SDMX 2.1 (flat) part of the observation .
        /// </summary>
        /// <param name="obsConcept">
        ///     this.DimensionAtObservation
        ///     The observation concept.
        /// </param>
        /// <param name="obsConceptValue">The observation concept value.</param>
        private void WriteObservation21Flat(string obsConcept, string obsConceptValue)
        {
            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.ObsKey);

            this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Value);
            this.WriteAttributeString(AttributeNameTable.id, obsConcept);
            this.WriteAttributeString(AttributeNameTable.value, obsConceptValue);

            this.WriteEndElement(); // END Value

            foreach (var componentId in this._componentVals)
            {
                this.WriteStartElement(this.Namespaces.Generic, ElementNameTable.Value);
                this.WriteAttributeString(AttributeNameTable.id, componentId.Key);
                this.WriteAttributeString(AttributeNameTable.value, componentId.Value);
                this.WriteEndElement(); // END Value
            }

            this.WriteEndElement(); // END ObsKey
        }
    }
}