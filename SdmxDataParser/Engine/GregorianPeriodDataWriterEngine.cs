﻿// -----------------------------------------------------------------------
// <copyright file="GregorianPeriodDataWriterEngine.cs" company="EUROSTAT">
//   Date Created : 2018-02-21
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Engine
{
    using Api.Constants.InterfaceConstant;
    using Api.Engine;
    using Api.Model.Objects.Base;

    using SdmxObjects.Model.Objects.Base;

    /// <inheritdoc />
    /// <summary>
    /// Gregorian period data writer engine
    /// </summary>
    /// <seealso cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.ReportingPeriodDataWriterEngineBase" />
    public class GregorianPeriodDataWriterEngine : ReportingPeriodDataWriterEngineBase
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.GregorianPeriodDataWriterEngine" /> class.
        /// </summary>
        /// <param name="decoratedEngine">The decorated engine.</param>
        public GregorianPeriodDataWriterEngine(IDataWriterEngine decoratedEngine) : base(decoratedEngine)
        {
        }

        /// <inheritdoc />
        public override void WriteAttributeValue(string id, string valueRen)
        {
            if (id != AttributeObject.Repyearstart)
                this.DecoratedEngine.WriteAttributeValue(id, valueRen);
            else
                this.ReportingStartYearDate = valueRen;
        }

        /// <summary>
        /// </summary>
        /// <param name="observationConceptId"></param>
        /// <param name="obsConceptValue"></param>
        /// <param name="obsValue"></param>
        /// <param name="annotations"></param>
        /// <inheritdoc />
        public override void WriteObservation(string observationConceptId, string obsConceptValue, string obsValue, params IAnnotation[] annotations)
        {
            if (DimensionObject.TimeDimensionFixedId.Equals(observationConceptId) && this.ReportingPeriodConverter.CheckReportingPeriod(obsConceptValue, this.ReportingStartYearDate))
            {
                var gregorianPeriod = this.ReportingPeriodConverter.ToGregorianPeriod(obsConceptValue, this.ReportingStartYearDate);
                var gregorianPeriodDate = new SdmxDateCore(gregorianPeriod.PeriodStart, gregorianPeriod.Frequency);
                this.DecoratedEngine.WriteObservation(observationConceptId, gregorianPeriodDate.DateInSdmxFormat, obsValue, annotations);
            }
            else
            {
                this.DecoratedEngine.WriteObservation(observationConceptId, obsConceptValue, obsValue, annotations);
            }
        }

        /// <inheritdoc />
        public override void WriteSeriesKeyValue(string id, string value)
        {
            if (DimensionObject.TimeDimensionFixedId.Equals(id) && this.ReportingPeriodConverter.CheckReportingPeriod(value, this.ReportingStartYearDate))
            {
                var gregorianPeriod = this.ReportingPeriodConverter.ToGregorianPeriod(value, this.ReportingStartYearDate);
                var gregorianPeriodDate = new SdmxDateCore(gregorianPeriod.PeriodStart, gregorianPeriod.Frequency);
                this.DecoratedEngine.WriteSeriesKeyValue(id, gregorianPeriodDate.DateInSdmxFormat);
            }
            else
            {
                this.DecoratedEngine.WriteSeriesKeyValue(id, value);
            }
        }
    }
}
