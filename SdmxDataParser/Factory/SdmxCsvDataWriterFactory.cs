﻿// -----------------------------------------------------------------------
// <copyright file="SdmxCsvDataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2018-01-09
//    All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxDataParser.
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Factory
{
    using System;
    using System.IO;

    using Engine.Csv;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine.JsonSupport;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

    /// <summary>
    /// The data writer factory.
    /// </summary>
    public class SdmxCsvDataWriterFactory : IDataWriterFactory
    {
        /// <summary>
        /// Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="outStream">The output stream.</param>
        /// <returns>The <see cref="IDataWriterEngine" /> class.</returns>
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            if (dataFormat == null)
            {
                throw new ArgumentNullException("dataFormat");
            }

            if (outStream == null)
            {
                throw new ArgumentNullException("outStream");
            }

            if (dataFormat.SdmxDataFormat != null &&
                dataFormat.SdmxDataFormat.BaseDataFormat != null &&
                dataFormat.SdmxDataFormat.BaseDataFormat.EnumType == BaseDataFormatEnumType.Csv)
            {
                var sdmxJsonDataFormat = dataFormat as SdmxCsvDataFormat;

                return sdmxJsonDataFormat != null
                    ? new CsvDataWriterEngine(
                            outStream,
                            sdmxJsonDataFormat.SdmxSuperObjectRetrievalManager,
                            sdmxJsonDataFormat.Options,
                            sdmxJsonDataFormat.Translator
                        )
                    : null;
            }

            return null;
        }
    }
}