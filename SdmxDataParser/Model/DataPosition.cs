﻿// -----------------------------------------------------------------------
// <copyright file="DataPosition.cs" company="EUROSTAT">
//   Date Created : 2016-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxDataParser.
// 
//     SdmxDataParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxDataParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxDataParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.DataParser.Model
{
    /// <summary>
    /// The data position enumeration.
    /// </summary>
    public enum DataPosition
    {
        /// <summary>
        /// The dataset
        /// </summary>
        Dataset = 0,

        /// <summary>
        /// The dataset attribute
        /// </summary>
        DatasetAttribute = 1,

        /// <summary>
        /// The series key
        /// </summary>
        SeriesKey = 2,

        /// <summary>
        /// The series key attribute
        /// </summary>
        SeriesKeyAttribute = 3,

        /// <summary>
        /// The group
        /// </summary>
        Group = 4,

        /// <summary>
        /// The group key
        /// </summary>
        GroupKey = 5,

        /// <summary>
        /// The group key attribute
        /// </summary>
        GroupKeyAttribute = 6,

        /// <summary>
        /// The group series key
        /// </summary>
        GroupSeriesKey = 7,

        /// <summary>
        /// The group series key attribute
        /// </summary>
        GroupSeriesKeyAttribute = 8,

        /// <summary>
        /// The observation
        /// </summary>
        Observation = 9,

        /// <summary>
        /// The observation attribute
        /// </summary>
        ObservationAttribute = 10
    }
}