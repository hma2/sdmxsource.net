﻿// -----------------------------------------------------------------------
// <copyright file="DatabaseRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SkeletonApplication
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// The database retrieval manager.
    /// </summary>
    ///// Step 1. Creating an implementation of  the ISdmxObjectRetrievalManager interface
    public class DatabaseRetrievalManager :  ISdmxObjectRetrievalManager
    {
        #region Public Methods and Operators


        /// <summary>
        /// Returns the agency with the given Id, if the Agency is a child of another Agency (other then SDMX), then it should be a dot separated id, for 
        /// example DEMO.SUBDEMO
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns>agency, or null if none could be found with the supplied id</returns>
        public IAgency GetAgency(string id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the identifiable objects.
        /// </summary>
        /// <typeparam name="T">The type of the identifiable objects to return.</typeparam>
        /// <param name="structureReference">The structure reference.</param>
        /// <returns>Returns a set of identifiable objects that match the structure reference, which may be a full or partial reference to a maintainable or identifiable</returns>
        public ISet<T> GetIdentifiableObjects<T>(IStructureReference structureReference) where T : IIdentifiableObject
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Resolves an identifiable reference
        /// </summary>
        /// <param name="crossReference"> Cross-reference object
        /// </param>
        /// <returns>
        /// The <see cref="IIdentifiableObject"/> .
        /// </returns>
        /// <exception cref="CrossReferenceException">
        /// if the ICrossReference could not resolve to an IIdentifiableObject
        /// </exception>
        public IIdentifiableObject GetIdentifiableObject(ICrossReference crossReference)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Resolves an reference to a Object of type T, this will return the Object of the given type, throwing an exception if either the
        ///     Object can not be resolved or if it is not of type T
        /// </summary>
        /// <typeparam name="T"> Generic type parameter. </typeparam>
        /// <param name="crossReference">
        ///     Cross-reference object
        /// </param>
        /// <returns>
        /// The <see cref="T"/> .
        /// </returns>
        /// <exception cref="CrossReferenceException">
        /// if the ICrossReference could not resolve to an IIdentifiableObject
        /// </exception>
        public T GetIdentifiableObject<T>(ICrossReference crossReference)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Resolves an reference to a Object of type T, this will return the Object of the given type, throwing an exception if e
        ///     Object is not of type T
        /// </summary>
        /// <typeparam name="T">Generic type parameter.
        /// </typeparam>
        /// <param name="crossReference">Structure-reference object
        /// </param>
        /// <returns>
        /// The <see cref="T"/> .
        /// </returns>
        /// <exception cref="SdmxReferenceException">
        /// if the ICrossReference could not resolve to an IIdentifiableObject
        /// </exception>
        public T GetIdentifiableObject<T>(IStructureReference crossReference)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all the maintainable that match the <paramref name="restquery"/>
        /// </summary>
        /// <param name="restquery">The REST structure query.</param>
        /// <returns>the maintainable that match the <paramref name="restquery"/></returns>
        public ISdmxObjects GetMaintainables(IRestStructureQuery restquery)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <param name="structureReference">The <see cref="IStructureReference"/> which must not be null.</param>
        /// <param name="resolveCrossReferences">either 'do not resolve', 'resolve all' or 'resolve all excluding agencies'. If not set to 'do not resolve' then all the structures that are referenced by the resulting structures are also returned (and also their children).  This will be equivalent to descendants for a <c>RESTful</c> query..</param>
        /// <returns>Returns a <see cref="ISdmxObjects"/> container containing all the Maintainable Objects that match the query parameters as defined by the <paramref name="structureReference"/>.</returns>
        public ISdmxObjects GetSdmxObjects(IStructureReference structureReference, ResolveCrossReferences resolveCrossReferences)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a maintainable defined by the StructureQueryObject parameter.
        ///     <p/>
        ///     Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="structureReference">
        /// The reference object defining the search parameters, this is expected to uniquely identify one MaintainableObject
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableObject"/> .
        /// </returns>
        public IMaintainableObject GetMaintainableObject(IStructureReference structureReference)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a maintainable defined by the StructureQueryObject parameter.
        ///     <p/>
        ///     Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="structureReference">
        /// The reference object defining the search parameters, this is expected to uniquely identify one MaintainableObject
        /// </param>
        /// <param name="returnStub">
        /// If true then a stub object will be returned
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableObject"/> .
        /// </returns>
        public IMaintainableObject GetMaintainableObject(IStructureReference structureReference, bool returnStub, bool returnLatest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets a maintainable that is of the given type, determined by T, and matches the reference parameters in the IMaintainableRefObject.
        ///     <p/>
        ///     Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="maintainableReference">
        /// The reference object that must match on the returned structure. If version information is missing, then latest version is assumed
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableObject"/> .
        /// </returns>
        public T GetMaintainableObject<T>(IMaintainableRefObject maintainableReference) where T : IMaintainableObject
        {
            SdmxStructureType structureType = SdmxStructureType.ParseClass(typeof(T));
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.CodeList:
                    return (T)this.GetCodelist(maintainableReference);
                default:
                    throw new SdmxNotImplementedException(structureType.StructureType);
            } 
        }

        /// <summary>
        /// Gets a maintainable that is of the given type, determined by T, and matches the reference parameters in the IMaintainableRefObject.
        ///     <p/>
        ///     Expects only ONE maintainable to be returned from this query
        /// </summary>
        /// <param name="maintainableReference">
        /// The reference object that must match on the returned structure. If version information is missing, then latest version is assumed
        /// </param>
        /// <param name="returnStub">
        /// If true then a stub object will be returned
        /// </param>
        /// /// <param name="returnLatest">
        /// If true then the latest version is returned, regardless of whether version information is supplied
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableObject"/> .
        /// </returns>
        public T GetMaintainableObject<T>(IMaintainableRefObject maintainableReference, bool returnStub, bool returnLatest) where T : IMaintainableObject
        {
            SdmxStructureType structureType = SdmxStructureType.ParseClass(typeof(T));
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.CodeList:
                    return (T)this.GetCodelistObjects(maintainableReference, returnLatest, returnStub).FirstOrDefault();
                default:
                    throw new SdmxNotImplementedException(structureType.StructureType);
            } 
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T
        ///     <p/>
        ///     An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <returns>
        /// The set of <see cref="IMaintainableObject"/> .
        /// </returns>
        public ISet<T> GetMaintainableObjects<T>() where T : IMaintainableObject
        {
            return new HashSet<T>(this.GetCodelistObjects(new MaintainableRefObjectImpl()).Cast<T>());
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject argument.
        // <p/>
        ///     An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <param name="maintainableReference">
        /// Contains the identifiers of the structures to returns, can include widcarded values (null indicates a wildcard). 
        /// </param>
        /// <returns>
        /// The set of <see cref="IMaintainableObject"/> .
        /// </returns>
        public ISet<T> GetMaintainableObjects<T>(IMaintainableRefObject maintainableReference) where T : IMaintainableObject
        {
            SdmxStructureType structureType = SdmxStructureType.ParseClass(typeof(T));
            switch (structureType.EnumType)
            {
                   case SdmxStructureEnumType.CodeList:
                    return new HashSet<T>(this.GetCodelistObjects(maintainableReference).Cast<T>());
                default:
                    throw new SdmxNotImplementedException(structureType.StructureType);
            }
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject argument.
        // <p/>
        ///     An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <param name="maintainableReference">
        /// Contains the identifiers of the structures to returns, can include widcarded values (null indicates a wildcard). 
        /// </param>
        /// <param name="returnStub">
        /// If true then a stub object will be returned
        /// </param>
        /// /// <param name="returnLatest">
        /// If true then the latest version is returned, regardless of whether version information is supplied
        /// </param>
        /// <returns>
        /// The set of <see cref="IMaintainableObject"/> .
        /// </returns>
        public ISet<T> GetMaintainableObjects<T>(IMaintainableRefObject maintainableReference, bool returnStub, bool returnLatest) where T : IMaintainableObject
        {
            SdmxStructureType structureType = SdmxStructureType.ParseClass(typeof(T));
            switch (structureType.EnumType)
            {
                case SdmxStructureEnumType.CodeList:
                    return new HashSet<T>(this.GetCodelistObjects(maintainableReference, returnLatest, returnStub).Cast<T>());
                default:
                    throw new SdmxNotImplementedException(structureType.StructureType);
            }
        }

        /// <summary>
        /// Gets a set of all MaintainableObjects of type T that match the reference parameters in the IMaintainableRefObject argument.
        /// An empty Set will be returned if there are no matches to the query
        /// </summary>
        /// <typeparam name="T">The type of the maintainable. It is constraint  </typeparam>
        /// <param name="maintainableInterface">The maintainable interface.</param>
        /// <param name="maintainableReference">Contains the identifiers of the structures to returns, can include wild-carded values (null indicates a wild-card).</param>
        /// <returns>
        /// The set of <see cref="IMaintainableObject" /> .
        /// </returns>
        /// <remarks>This method exists only for compatibility reasons with Java implementation of this interface which uses raw types and unchecked generics.</remarks>
        public ISet<T> GetMaintainableObjects<T>(Type maintainableInterface, IMaintainableRefObject maintainableReference) where T : ISdmxObject
        {
            throw new NotImplementedException();
        }

        // DatabaseRetrievalManager: Step 2. Implementing ISdmxObjectRetrievalManager GetCodelist* methods.
        /// <summary>
        /// Gets the codelist.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The requested codelist; otherwise null.
        /// </returns>
        public ICodelistObject GetCodelist(IMaintainableRefObject query)
        {
            return this.GetCodelist(query, false);
        }

        /// <summary>
        /// Gets the codelist.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="returnStub">
        /// if set to <c>true</c> [return stub].
        /// </param>
        /// <returns>
        /// The requested codelist; otherwise null.
        /// </returns>
        public ICodelistObject GetCodelist(IMaintainableRefObject query, bool returnStub)
        {
            var returnLatest = query == null || query.HasVersion();
            return this.GetCodelistObjects(query, returnLatest, returnStub).FirstOrDefault();
        }

        /// <summary>
        /// Gets the codelist objects.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The set of requested code-lists; otherwise an empty set.
        /// </returns>
        public ISet<ICodelistObject> GetCodelistObjects(IMaintainableRefObject query)
        {
            return this.GetCodelistObjects(query, false, false);
        }

        /// <summary>
        /// Gets the codelist objects.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="returnLatest">
        /// if set to <c>true</c> [return latest].
        /// </param>
        /// <param name="returnStub">
        /// if set to <c>true</c> [return stub].
        /// </param>
        /// <returns>
        /// The set of requested code-lists; otherwise an empty set.
        /// </returns>
        public ISet<ICodelistObject> GetCodelistObjects(IMaintainableRefObject query, bool returnLatest, bool returnStub)
        {
            // Create the database connection string
            var builder = new SqlConnectionStringBuilder
                              {
                                  DataSource = ".\\SQLEXPRESS", 
                                  AttachDBFilename = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), @"DATABASE SCRIPTS\USECASE1.MDF"), 
                                  IntegratedSecurity = true, 
                                  UserInstance = true /* , UserID = "uc1user", Password = "123" */
                              };
            builder.Add("DATABASE", "USE_CASE1");

            // create a map that holds the primary keys of code-lists and the mutable codelist objects. 
            // We need that in order to store the code-lists but also retrieve the codes and annotations associated with each codelist.
            Dictionary<long, ICodelistMutableObject> codelistMap;

            // create a connection
            using (DbConnection connection = new SqlConnection())
            {
                // add the connection string
                connection.ConnectionString = builder.ConnectionString;

                // open the connection
                connection.Open();

                 // Step 3. Retrieving codelists from database. 
                codelistMap = RetrieveCodelistsFromDatabase(connection, query);

                if (!returnStub)
                {
                    // Step 4. Retrieving Codes from the database.
                    // Get the code from database and add them to the corresponding codelists
                    RetrieveCodesFromDatabase(connection, codelistMap);
                }
            }

            // create the set of immutable code lists
            ISet<ICodelistObject> codelistObjects = new HashSet<ICodelistObject>();
            foreach (var codelistMutableObject in codelistMap.Values)
            {
                var immutableInstance = codelistMutableObject.ImmutableInstance;

                // depending on the value of returnStub parameter add full or stub objects.
                codelistObjects.Add(returnStub ? immutableInstance.GetStub(new Uri("http://changeme.to"), false) : immutableInstance);
            }

            if (!returnLatest)
            {
                return codelistObjects;
            }

            // in case we should return latest versions of the code lists grouped by ID and AgencyID
            // ISdmxObjects implementation SdmxObjectsImpl will return the latest versions if version is null.
            var immutableObjects = new SdmxObjectsImpl(null, codelistObjects);
            return immutableObjects.GetCodelists(new MaintainableRefObjectImpl(query.AgencyId, query.MaintainableId, null));
        }


        #endregion

        #region Methods

        /// <summary>
        /// Populates the annotation.
        /// </summary>
        /// <typeparam name="T">
        /// The <see cref="IAnnotableMutableObject"/> based type
        /// </typeparam>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="annotableMap">
        /// The <c>Annotable</c> map.
        /// </param>
        /// <param name="commandText">
        /// The command text.
        /// </param>
        private static void PopulateAnnotation<T>(DbConnection connection, IDictionary<long, T> annotableMap, string commandText) where T : IAnnotableMutableObject
        {
            using (var command = connection.CreateCommand())
            {
                command.CommandText = commandText;
                using (var reader = command.ExecuteReader())
                {
                    long previousAnnotationID = long.MinValue;
                    IAnnotationMutableObject annotation = null;

                    while (reader.Read())
                    {
                        long codelistId = DatabaseHelper.GetInt64(reader, "SYSID");
                        long currentAnnotationID = DatabaseHelper.GetInt64(reader, "AN_ID");
                        if (previousAnnotationID != currentAnnotationID || annotation == null)
                        {
                            previousAnnotationID = currentAnnotationID;
                            annotation = new AnnotationMutableCore();
                            T mutable;
                            if (annotableMap.TryGetValue(codelistId, out mutable))
                            {
                                annotation.Id = DatabaseHelper.GetString(reader, "ID");
                                annotation.Title = DatabaseHelper.GetString(reader, "TITLE");
                                annotation.Type = DatabaseHelper.GetString(reader, "ATYPE");
                                annotation.Uri = DatabaseHelper.GetUri(reader, "URI");
                                mutable.AddAnnotation(annotation);
                            }
                        }

                        string lang = DatabaseHelper.GetString(reader, "LANGUAGE");
                        string text = DatabaseHelper.GetString(reader, "TEXT");
                        if (!string.IsNullOrWhiteSpace(lang) && !string.IsNullOrWhiteSpace(text))
                        {
                            annotation.AddText(lang, text);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Populates the name description.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="mutable">
        /// The mutable.
        /// </param>
        private static void PopulateNameDescription(IDataRecord reader, INameableMutableObject mutable)
        {
            string lang = DatabaseHelper.GetString(reader, "LANGUAGE");
            string text = DatabaseHelper.GetString(reader, "TEXT");
            if (DatabaseHelper.GetBoolean(reader, "IS_DESCRIPTION"))
            {
                mutable.AddDescription(lang, text);
            }
            else
            {
                mutable.AddName(lang, text);
            }
        }

        /// <summary>
        /// Retrieves the <c>Codelists</c> from the database as mutable objects.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="query">
        /// The query
        /// </param>
        /// <returns>
        /// A map between <c>Codelist</c> primary key value and the <c>ICodelistMutableObject</c>
        /// </returns>
        private static Dictionary<long, ICodelistMutableObject> RetrieveCodelistsFromDatabase(DbConnection connection, IMaintainableRefObject query)
        {
            // create a map that holds the primary keys of codelists and the mutable codelist objects. 
            // We need that in order to store the codelists but also retrieve the codes and annotations associated with each codelist.
            var codelistMap = new Dictionary<long, ICodelistMutableObject>();

            using (DbCommand command = connection.CreateCommand())
            {
                // query the codelist and the localised codelist and string tables. 
                // This SQL query will return one row for each name or description for each code list. 
                command.CommandText =
                    "select C.*, LC.IS_DESCRIPTION, LS.LANGUAGE, LS.TEXT FROM CODELIST C INNER JOIN LOCALISED_CODELIST LC ON C.CL_ID = LC.CL_ID INNER JOIN LOCALISED_STRING LS ON LC.LS_ID = LS.LS_ID WHERE (@agency is null or C.AGENCY=@agency) and (@id is null or C.ID = @id) and (@cversion is null or C.CVERSION = @cversion) ORDER BY C.CL_ID";

                // create the parameters.
                var agencyParameter = command.CreateParameter();
                agencyParameter.DbType = DbType.AnsiString;
                agencyParameter.Direction = ParameterDirection.Input;
                agencyParameter.ParameterName = "@agency";

                var idParameter = command.CreateParameter();
                idParameter.DbType = DbType.AnsiString;
                idParameter.Direction = ParameterDirection.Input;
                idParameter.ParameterName = "@id";

                var versionParameter = command.CreateParameter();
                versionParameter.DbType = DbType.AnsiString;
                versionParameter.Direction = ParameterDirection.Input;
                versionParameter.ParameterName = "@cversion";

                if (query != null)
                {
                    agencyParameter.Value = query.HasAgencyId() ? (object)query.AgencyId : DBNull.Value;
                    idParameter.Value = query.HasMaintainableId() ? (object)query.MaintainableId : DBNull.Value;
                    versionParameter.Value = query.HasVersion() ? (object)query.Version : DBNull.Value;
                }
                else
                {
                    agencyParameter.Value = DBNull.Value;
                    idParameter.Value = DBNull.Value;
                    versionParameter.Value = DBNull.Value;
                }

                command.Parameters.Add(agencyParameter);
                command.Parameters.Add(idParameter);
                command.Parameters.Add(versionParameter);

                using (var reader = command.ExecuteReader())
                {
                    long previousCodelistId = long.MinValue;
                    ICodelistMutableObject mutable = null;
                    while (reader.Read())
                    {
                        // This SQL query will return one row for each name or description for each code. 
                        // So we check first if we already handled this code.
                        var currentCodelistId = DatabaseHelper.GetInt64(reader, "CL_ID");
                        if (previousCodelistId != currentCodelistId || mutable == null)
                        {
                            previousCodelistId = currentCodelistId;

                            // Create a new mutable codelist Add ID, AGENCY, VERSION, FinalStructure, IsPartial.
                            mutable = new CodelistMutableCore
                                          {
                                              Id = DatabaseHelper.GetString(reader, "ID"), 
                                              AgencyId = DatabaseHelper.GetString(reader, "AGENCY"), 
                                              Version = DatabaseHelper.GetString(reader, "CVERSION"), 
                                              StartDate = DatabaseHelper.GetDateTime(reader, "VALID_FROM"), 
                                              EndDate = DatabaseHelper.GetDateTime(reader, "VALID_TO"), 
                                              FinalStructure = TertiaryBool.ParseBoolean(DatabaseHelper.GetBoolean(reader, "IS_FINAL")), 
                                              IsPartial = DatabaseHelper.GetBoolean(reader, "IS_PARTIAL"), 
                                              Uri = DatabaseHelper.GetUri(reader, "URI")
                                          };

                            codelistMap.Add(currentCodelistId, mutable);
                        }

                        PopulateNameDescription(reader, mutable);
                    }
                }
            }

            // retrieve the codelist annotations and populate the codelist annotations, if any,
            PopulateAnnotation(
                connection, 
                codelistMap, 
                "select LC.CL_ID as SYSID, AN.*, LS.LANGUAGE, LS.TEXT from ANNOTATION_CODELIST LC INNER JOIN ANNOTATION AN ON LC.AN_ID = AN.AN_ID INNER JOIN LOCALISED_ANNOTATION LA ON AN.AN_ID = LA.AN_ID INNER JOIN LOCALISED_STRING LS ON LS.LS_ID = LA.LS_ID");

            return codelistMap;
        }

        /// <summary>
        /// Retrieves the codes from database.
        /// </summary>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="codelistMap">
        /// The <c>Codelist</c> map.
        /// </param>
        private static void RetrieveCodesFromDatabase(DbConnection connection, IDictionary<long, ICodelistMutableObject> codelistMap)
        {
            var codeMap = new Dictionary<long, ICodeMutableObject>();
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText =
                    "select C.*, LC.IS_DESCRIPTION, LS.LANGUAGE, LS.TEXT FROM CODE C INNER JOIN LOCALISED_CODE LC ON C.C_ID = LC.C_ID INNER JOIN LOCALISED_STRING LS ON LC.LS_ID = LS.LS_ID ORDER BY C.C_ID";
                using (var reader = command.ExecuteReader())
                {
                    long previousCodeId = long.MinValue;
                    ICodeMutableObject mutable = null;
                    while (reader.Read())
                    {
                        // This SQL query will return one row for each name or description for each code. 
                        // So we check first if we already handled this code.
                        var currentCodeId = DatabaseHelper.GetInt64(reader, "C_ID");
                        if (currentCodeId != previousCodeId || mutable == null)
                        {
                            previousCodeId = currentCodeId;

                            // Create a new mutable code
                            mutable = new CodeMutableCore { Id = DatabaseHelper.GetString(reader, "ID"), ParentCode = DatabaseHelper.GetString(reader, "PARENT_CODE_ID"), Uri = DatabaseHelper.GetUri(reader, "URI") };

                            // Add the code to the corresponding codelist
                            var clid = DatabaseHelper.GetInt64(reader, "CL_ID");
                            ICodelistMutableObject codelist;
                            if (codelistMap.TryGetValue(clid, out codelist))
                            {
                                codeMap.Add(currentCodeId, mutable);
                                codelist.AddItem(mutable);
                            }
                        }

                        PopulateNameDescription(reader, mutable);
                    }
                }
            }

            // retrieve the code annotations and populate the code annotations, if any,
            PopulateAnnotation(
                connection, 
                codeMap, 
                "select LC.C_ID as SYSID, AN.* from ANNOTATION_CODE LC INNER JOIN ANNOTATION AN ON LC.AN_ID = AN.AN_ID INNER JOIN LOCALISED_ANNOTATION LA ON AN.AN_ID = LA.AN_ID INNER JOIN LOCALISED_STRING LS ON LS.LS_ID = LA.LS_ID");
        }

        #endregion

    }
}