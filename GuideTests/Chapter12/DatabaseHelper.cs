// -----------------------------------------------------------------------
// <copyright file="DatabaseHelper.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SkeletonApplication
{
    using System;
    using System.Data;

    /// <summary>
    /// The database helper.
    /// </summary>
    public static class DatabaseHelper
    {
        /// <summary>
        /// Gets the Boolean value for the specified <paramref name="field"/>.
        /// </summary>
        /// <param name="reader">
        /// The <see cref="IDataReader"/> reader.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The Boolean value.
        /// </returns>
        public static bool GetBoolean(IDataRecord reader, string field)
        {
            int ordinal = reader.GetOrdinal(field);
            if (reader.IsDBNull(ordinal))
            {
                return false;
            }

            return reader.GetBoolean(ordinal);
        }

        /// <summary>
        /// Gets the date time value from the database.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/> value if it exists; otherwise <c>null</c>.
        /// </returns>
        public static DateTime? GetDateTime(IDataRecord reader, string field)
        {
            int ordinal = reader.GetOrdinal(field);
            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return reader.GetDateTime(ordinal);
        }

        /// <summary>
        /// Gets the long integer value (64bit) from the specified <paramref name="field"/>
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The long value; otherwise -1
        /// </returns>
        public static long GetInt64(IDataRecord reader, string field)
        {
            int ordinal = reader.GetOrdinal(field);
            if (reader.IsDBNull(ordinal))
            {
                return -1;
            }

            return reader.GetInt64(ordinal);
        }

        /// <summary>
        /// Gets the string value from the specified <paramref name="field"/>.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The string value if it exists; otherwise <c>null</c>
        /// </returns>
        public static string GetString(IDataRecord reader, string field)
        {
            int ordinal = reader.GetOrdinal(field);
            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return reader.GetString(ordinal);
        }

        /// <summary>
        /// Gets the <see cref="Uri"/> value from the specified <paramref name="field"/>.
        /// </summary>
        /// <param name="reader">
        /// The reader.
        /// </param>
        /// <param name="field">
        /// The field.
        /// </param>
        /// <returns>
        /// The <see cref="Uri"/> value if it exists; otherwise <c>null</c>
        /// </returns>
        public static Uri GetUri(IDataRecord reader, string field)
        {
            int ordinal = reader.GetOrdinal(field);
            if (reader.IsDBNull(ordinal))
            {
                return null;
            }

            return new Uri(reader.GetString(ordinal));
        }
    }
}