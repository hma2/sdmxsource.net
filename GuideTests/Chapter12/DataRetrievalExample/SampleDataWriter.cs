﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataWriter.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter12.DataRetrievalExample
{
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    public class SampleDataWriter
    {
        public void writeSampleData(IDataStructureObject dsd, IDataflowObject dataflow, IDataWriterEngine dwe){
           // Step 1. Writing sample data. 
            // 1)	Start the dataset
            dwe.StartDataset(dataflow, dsd, null);

            // 2)	Start the dataset series
            dwe.StartSeries();

            // 3)	Write dimensions
            dwe.WriteSeriesKeyValue("FREQ", "Q");
            dwe.WriteSeriesKeyValue("REF_AREA", "IT");
            dwe.WriteSeriesKeyValue("ADJUSTMENT", "W");
            dwe.WriteSeriesKeyValue("STS_INDICATOR", "E_P");
            dwe.WriteSeriesKeyValue("STS_ACTIVITY", "NS0020");
            dwe.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            dwe.WriteSeriesKeyValue("STS_BASE_YEAR", "2000");

            // 4)	Write Attribute at Series Level
            dwe.WriteAttributeValue("TIME_FORMAT", "P3M");

            // 5)	Write the observations
            dwe.WriteObservation("2000-Q1", "18,22");
            dwe.WriteObservation("2000-Q2", "17,63");

            // 6)	Write Attribute at Observation Level
            dwe.WriteAttributeValue("OBS_STATUS", "C");

            // 7)	Repeat the steps from 2 to 6 to create another dataset series 
            dwe.StartSeries();
            dwe.WriteSeriesKeyValue("FREQ", "M");
            dwe.WriteSeriesKeyValue("REF_AREA", "SP");
            dwe.WriteSeriesKeyValue("ADJUSTMENT", "N");
            dwe.WriteSeriesKeyValue("STS_INDICATOR", "PROD");
            dwe.WriteSeriesKeyValue("STS_ACTIVITY", "NS0030");
            dwe.WriteSeriesKeyValue("STS_INSTITUTION", "1");
            dwe.WriteSeriesKeyValue("STS_BASE_YEAR", "2001");
            dwe.WriteAttributeValue("TIME_FORMAT", "P1M");
            dwe.WriteObservation("2000-01", "18,22");
            dwe.WriteObservation("2000-02", "17,63");
            dwe.WriteAttributeValue("OBS_STATUS", "S");

            // 8)	Close the dataset
            dwe.Close();

        }
    }
}
