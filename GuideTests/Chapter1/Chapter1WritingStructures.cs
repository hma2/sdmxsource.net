﻿// -----------------------------------------------------------------------
// <copyright file="Chapter1WritingStructures.cs" company="EUROSTAT">
//   Date Created : 2014-04-28
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter1
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;

    /// <summary>
    ///  Description of Chapter1WritingStructures.
    /// User: sli
    /// </summary>
    public class Chapter1WritingStructures
    {
        #region Public Methods and Operators

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            var fileWriter = new SdmxFileWriter();

            // create the IStructureFormat from a standard Sdmx StructureOutputFormat
            StructureOutputFormat soFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);
            IStructureFormat outputFormat = new SdmxStructureFormat(soFormat);

            // create the output file
            var outputStream = new FileStream("output/structures.xml", FileMode.Create);

            // write the the structures to the output file
            fileWriter.writeStructureToFile(outputFormat, outputStream);
        }

        #endregion
    }
}