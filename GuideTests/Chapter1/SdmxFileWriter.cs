﻿// -----------------------------------------------------------------------
// <copyright file="SdmxFileWriter.cs" company="EUROSTAT">
//   Date Created : 2014-04-28
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter1
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     Description of SdmxFileWriter.
    /// </summary>
    public class SdmxFileWriter
    {
        #region Fields

        /// <summary>
        /// The structures creator.
        /// </summary>
        private readonly StructuresCreator structuresCreator;

        /// <summary>
        /// The swm.
        /// </summary>
        private readonly IStructureWriterManager swm;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxFileWriter"/> class.
        /// </summary>
        public SdmxFileWriter()
        {
            this.structuresCreator = new StructuresCreator();

            this.swm = new StructureWriterManager();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Writes the structure to file.
        /// </summary>
        /// <param name="outputFormat">The output format.</param>
        /// <param name="outputStream">The output stream.</param>
        public void writeStructureToFile(IStructureFormat outputFormat, Stream outputStream)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();

            sdmxObjects.AddAgencyScheme(this.structuresCreator.BuildAgencyScheme());
            sdmxObjects.AddCodelist(this.structuresCreator.BuildCountryCodelist());
            sdmxObjects.AddCodelist(this.structuresCreator.BuildIndicatorCodelist());
            sdmxObjects.AddConceptScheme(this.structuresCreator.BuildConceptScheme());

            IDataStructureObject dsd = this.structuresCreator.BuildDataStructure();
            sdmxObjects.AddDataStructure(dsd);
            sdmxObjects.AddDataflow(this.structuresCreator.BuildDataflow("DF_WDI", "World Development Indicators", dsd));

            this.swm.WriteStructures(sdmxObjects, outputFormat, outputStream);
        }

        #endregion
    }
}