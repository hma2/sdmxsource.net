﻿// -----------------------------------------------------------------------
// <copyright file="Chapter4ReadingData.cs" company="EUROSTAT">
//   Date Created : 2014-07-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace GuideTests.Chapter4
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.DataParser.Transform;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     The chapter 4 reading data.
    /// </summary>
    public class Chapter4ReadingData
    {
        #region Fields

        /// <summary>
        ///     The _data reader manager.
        /// </summary>
        private readonly IDataReaderManager _dataReaderManager;

        /// <summary>
        ///     The  <see cref="IReadableDataLocationFactory" />.
        /// </summary>
        private readonly IReadableDataLocationFactory _rdlFactory;

        /// <summary>
        ///     The _structure parsing manager.
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager;

        private DataReaderWriterTransform _dataReaderWriterTransform;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Chapter4ReadingData"/> class.
        /// </summary>
        /// <param name="dataReaderManager">
        /// The data Reader Manager.
        /// </param>
        /// <param name="rdlFactory">
        /// The <see cref="IReadableDataLocationFactory"/>.
        /// </param>
        /// <param name="structureParsingManager">
        /// The structure Parsing Manager.
        /// </param>
        public Chapter4ReadingData(IDataReaderManager dataReaderManager, IReadableDataLocationFactory rdlFactory, IStructureParsingManager structureParsingManager)
        {
            this._dataReaderManager = dataReaderManager;
            this._rdlFactory = rdlFactory;
            this._structureParsingManager = structureParsingManager;
            _dataReaderWriterTransform = new DataReaderWriterTransform();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The main method.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            // Step 1 - Create a new instance of the main class
            var main = new Chapter4ReadingData(new DataReaderManager(), new ReadableDataLocationFactory(), new StructureParsingManager());

            // Step 3 - Create a Readable Data Location from the File
            var structureFile = new FileInfo("tests/structures/chapter2/structures_full.xml");
            var dataFile = new FileInfo("tests/data/chapter3/sample_data.xml");

            main.ReadData(structureFile, dataFile);
            main.Transform(structureFile,dataFile);
        }

        private void Transform(FileInfo structureFile,FileInfo dataFile)
        { 
            IStructureWorkspace workspace;
             using (IReadableDataLocation rdl = this._rdlFactory.GetReadableDataLocation(structureFile))
            {
                workspace = this._structureParsingManager.ParseStructures(rdl);
            }

            ISdmxObjects beans = workspace.GetStructureObjects(false);
            ISdmxObjectRetrievalManager retreivalManager = new InMemoryRetrievalManager(beans);
            var readableDataLocation = _rdlFactory.GetReadableDataLocation(dataFile);
            var dataReaderEngine = _dataReaderManager.GetDataReaderEngine(readableDataLocation, retreivalManager);

            string outfile = Path.GetTempFileName();
            using (Stream writer = File.Create(outfile))
            using (var dataWriter = new DataWriterManager().GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Generic21)), writer))
            {
                
                _dataReaderWriterTransform.CopyToWriter(dataReaderEngine, dataWriter, true, true);
            }
            var xElement = XDocument.Load(outfile).Descendants().First(x => x.Name.LocalName == "DataSet");
            Console.WriteLine("\n\n\n Transform Result :");
            Console.WriteLine(xElement.ToString());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reads the data.
        /// </summary>
        /// <param name="structureFile">The structure file.</param>
        /// <param name="dataFile">The data file.</param>
        private void ReadData(FileInfo structureFile, FileInfo dataFile)
        {
            // Parse Structures into ISdmxObjects and build a SdmxBeanRetrievalManager
            IStructureWorkspace workspace;
            using (IReadableDataLocation rdl = this._rdlFactory.GetReadableDataLocation(structureFile))
            {
                workspace = this._structureParsingManager.ParseStructures(rdl);
            }

            ISdmxObjects beans = workspace.GetStructureObjects(false);
            ISdmxObjectRetrievalManager retreivalManager = new InMemoryRetrievalManager(beans);

            // Get the DataLocation, and from this the DataReaderEngine
            using (IReadableDataLocation dataLocation = this._rdlFactory.GetReadableDataLocation(dataFile))
            using (IDataReaderEngine dre = this._dataReaderManager.GetDataReaderEngine(dataLocation, retreivalManager))
            {
                while (dre.MoveNextDataset())
                {
                    IDataStructureObject dsd = dre.DataStructure;
                    Console.WriteLine(dsd.Name);

                    while (dre.MoveNextKeyable())
                    {
                        IKeyable currentKey = dre.CurrentKey;
                        Console.WriteLine(currentKey);
                        while (dre.MoveNextObservation())
                        {
                            IObservation obs = dre.CurrentObservation;
                            Console.WriteLine(obs);
                        }
                    }
                }
            }
        }

        #endregion
    }
}