﻿// -----------------------------------------------------------------------
// <copyright file="HtmlStructureOutputFactory.cs" company="EUROSTAT">
//   Date Created : 2015-05-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests.Chapter8
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    public class HtmlStructureOutputFactory : IStructureWriterFactory
    {
        /// <summary>
        /// Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">An implementation of the StructureFormat to describe the output format for the structures (required)</param>
        /// <param name="streamWriter">The output stream to write to (can be null if it is not required)</param>
        /// <returns>Null if this factory is not capable of creating a data writer engine in the requested format</returns>
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            if (structureFormat is HtmlOutputFormat)
            {
                return new HtmlStructureWriterEngine(streamWriter);
            }

            return null;
        }
    }
}