﻿// -----------------------------------------------------------------------
// <copyright file="TestGuideSamples.cs" company="EUROSTAT">
//   Date Created : 2013-04-19
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace GuideTests
{
    using System.IO;
    using System.Web;

    using Chapter3WritingData;

    using GuideTests.Chapter1;
    using GuideTests.Chapter2;
    using GuideTests.Chapter4;

    using NUnit.Framework;

    /// <summary>
    /// Test unit for <see cref="ReadingStructures"/>
    /// </summary>
    [TestFixture]
    public class TestGuideSamples
    {
        private StructuresCreator _creator;

        public TestGuideSamples()
        {
            _creator = new StructuresCreator();
        }

        /// <summary>
        /// Test unit for <see cref="ReadingStructures.ReadStructures"/> 
        /// </summary>
        [Test]
        public void TestReadingStructuresMain()
        {
            ReadingStructures rs = new ReadingStructures();

            rs.ReadStructures(new FileInfo("tests/v21/structures.xml"));
        }

        /// <summary>
        /// Test unit for <see cref="ReadingStructures.ReadStructures"/> 
        /// </summary>
        [TestCase("tests/v21/codelists_concepts.xml", "tests/v21/dsd.xml")]
        [TestCase("tests/v21/Structures_codelists_concepts.xml", "tests/v21/Structures_dsd_dataflow.xml")]
        public void TestResolveStructuresMain(string dependencies, string structure)
        {
            ResolveStructures resolv = new ResolveStructures();
            resolv.Resolve(new FileInfo(dependencies), new FileInfo(structure));
        }

        [Test]
        public void TestBuildAgency()
        {
            var agencyScheme = this._creator.BuildAgencyScheme();
            Assert.NotNull(agencyScheme);
            Assert.IsNotEmpty(agencyScheme.Items);
        }

        [Test]
        public void TestBuildConceptScheme()
        {
            var artefact = this._creator.BuildConceptScheme();
            Assert.NotNull(artefact);
            Assert.IsNotEmpty(artefact.Items);
        }

        [Test]
        public void TestBuildCountryCodelist()
        {
            var artefact = this._creator.BuildCountryCodelist();
            Assert.NotNull(artefact);
            Assert.IsNotEmpty(artefact.Items);
        }

        [Test]
        public void BuildDataStructure()
        {
            var artefact = this._creator.BuildDataStructure();
            Assert.NotNull(artefact);
            Assert.IsNotEmpty(artefact.Components);
            Assert.IsNotEmpty(artefact.GetDimensions());
        }

        [Test]
        public void Chapter3Test()
        {
            WritingData.HasConsole = false;
            WritingData.Main(null);
        }

        [Test]
        public void Chapter4Test()
        {
            Chapter4ReadingData.Main(null);
        }
    }
}