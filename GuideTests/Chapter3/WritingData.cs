﻿// -----------------------------------------------------------------------
// <copyright file="WritingData.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.IO;

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Io;

namespace Chapter3WritingData
{
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Date;

    public class WritingData
	{
        private readonly IReadableDataLocationFactory _dataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        private readonly IDataWriterManager _dataWriterManager = new DataWriterManager();
        private readonly SampleDataWriter _sampleDataWriter = new SampleDataWriter();

        public static bool HasConsole { get; set; }

        static  WritingData()
        {
            HasConsole = true;
        }

		public void WriteData(FileInfo structureFile) {
            IDataFormat dataFormat = new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21));
		    using (Stream fileOutputStream = GetFileOutputStream())
		    using (IDataWriterEngine compactWriter = this._dataWriterManager.GetDataWriterEngine(dataFormat, fileOutputStream))
		    {
		        ISdmxObjectRetrievalManager retrievalManager;
		        using (IReadableDataLocation structureRdl = this._dataLocationFactory.GetReadableDataLocation(structureFile))
		        {
		            retrievalManager = new InMemoryRetrievalManager(structureRdl, this._parsingManager, null);
		        }

		        string agencyId = "SDMXSOURCE";
		        string version = MaintainableObject.DefaultVersion; // 1.0

		        IMaintainableRefObject dsdRef = new MaintainableRefObjectImpl(agencyId, "WDI", version);
		        IMaintainableRefObject flowRef = new MaintainableRefObjectImpl(agencyId, "DF_WDI", version);

		        IDataStructureObject dsd = retrievalManager.GetMaintainableObject<IDataStructureObject>(dsdRef);
		        IDataflowObject dataflow = retrievalManager.GetMaintainableObject<IDataflowObject>(flowRef);

		        this._sampleDataWriter.WriteSampleData(dsd, dataflow, compactWriter);
		    }
		}

        public void WriteDataXs(FileInfo structureFile)
        {
            ISdmxObjectRetrievalManager retrievalManager;
            using (IReadableDataLocation structureRdl = this._dataLocationFactory.GetReadableDataLocation(structureFile))
            {
                retrievalManager = new InMemoryRetrievalManager(structureRdl, this._parsingManager, null);
            }

            string agencyId = "SDMXSOURCE";
            string version = MaintainableObject.DefaultVersion; // 1.0

            IMaintainableRefObject dsdRef = new MaintainableRefObjectImpl(agencyId, "WDI", version);
            IMaintainableRefObject flowRef = new MaintainableRefObjectImpl(agencyId, "DF_WDI", version);

            IDataStructureObject dsd = retrievalManager.GetMaintainableObject<IDataStructureObject>(dsdRef);
            IDataflowObject dataflow = retrievalManager.GetMaintainableObject<IDataflowObject>(flowRef);
            IDataFormat dataFormat = new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21));
            using (Stream fileOutputStream = new FileStream("output/xs_data.xml", FileMode.Create))
            using (IDataWriterEngine compactWriter = this._dataWriterManager.GetDataWriterEngine(dataFormat, fileOutputStream))
            {
                CrossSectionalTest(compactWriter, dsd, dataflow);
            }
        }

        public void WriteDataFlat(FileInfo structureFile)
        {
            ISdmxObjectRetrievalManager retrievalManager;
            using (IReadableDataLocation structureRdl = this._dataLocationFactory.GetReadableDataLocation(structureFile))
            {
                retrievalManager = new InMemoryRetrievalManager(structureRdl, this._parsingManager, null);
            }

            string agencyId = "SDMXSOURCE";
            string version = MaintainableObject.DefaultVersion; // 1.0

            IMaintainableRefObject dsdRef = new MaintainableRefObjectImpl(agencyId, "WDI", version);
            IMaintainableRefObject flowRef = new MaintainableRefObjectImpl(agencyId, "DF_WDI", version);

            IDataStructureObject dsd = retrievalManager.GetMaintainableObject<IDataStructureObject>(dsdRef);
            IDataflowObject dataflow = retrievalManager.GetMaintainableObject<IDataflowObject>(flowRef);
            IDataFormat dataFormat = new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21));
            using (Stream fileOutputStream = new FileStream("output/flat_data.xml", FileMode.Create))
            using (IDataWriterEngine compactWriter = this._dataWriterManager.GetDataWriterEngine(dataFormat, fileOutputStream))
            {
                FlatTest(compactWriter, dsd, dataflow);
            }
        }


        private static void CrossSectionalTest(IDataWriterEngine writerEngine, IDataStructureObject dsd, IDataflowObject dataflowObject)
        {
            IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, "COUNTRY");
            IDatasetHeader header = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);
            writerEngine.StartDataset(dataflowObject, dsd, header);

            writerEngine.StartSeries();
            writerEngine.WriteSeriesKeyValue("INDICATOR", "E_P");
            writerEngine.WriteSeriesKeyValue(DimensionObject.TimeDimensionFixedId, "2000");
            writerEngine.WriteObservation("UK", "18.22");
            writerEngine.WriteObservation("FR", "22.22");

            writerEngine.StartSeries();
            writerEngine.WriteSeriesKeyValue("INDICATOR", "E_P");
            string date = DateUtil.FormatDate(DateTime.Now, TimeFormatEnumType.Week);
            writerEngine.WriteSeriesKeyValue(DimensionObject.TimeDimensionFixedId, date);
            writerEngine.WriteObservation("UK", "17.63");
            writerEngine.WriteObservation("FR", "15.53");
        }

        private static void FlatTest(IDataWriterEngine writerEngine, IDataStructureObject dsd, IDataflowObject dataflowObject)
        {
            IDatasetStructureReference dsRef = new DatasetStructureReferenceCore("Test", dsd.AsReference, null, null, DatasetStructureReference.AllDimensions);
            IDatasetHeader header = new DatasetHeaderCore("DS12345", DatasetActionEnumType.Information, dsRef);
            writerEngine.StartDataset(dataflowObject, dsd, header);

            writerEngine.StartSeries();
            writerEngine.WriteSeriesKeyValue("INDICATOR", "E_P");
            writerEngine.WriteSeriesKeyValue(DimensionObject.TimeDimensionFixedId, "2000");
            writerEngine.WriteObservation("COUNTRY", "UK", "18.22");
            writerEngine.WriteObservation("COUNTRY", "FR", "22.22");

            writerEngine.StartSeries();
            writerEngine.WriteSeriesKeyValue("INDICATOR", "E_P");
            string date = DateUtil.FormatDate(DateTime.Now, TimeFormatEnumType.Week);
            writerEngine.WriteSeriesKeyValue(DimensionObject.TimeDimensionFixedId, date);
            writerEngine.WriteObservation("COUNTRY", "UK", "17.63");
            writerEngine.WriteObservation("COUNTRY", "FR", "15.53");
        }

        private static Stream GetFileOutputStream()
        {
            return new FileStream("output/compact_data.xml", FileMode.Create);
        }

        public static void Main(string[] args)
		{
            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }

			WritingData wd = new WritingData();
            var structureFile = new FileInfo("tests/structures/chapter2/structures_dsd_dataflow.xml");
            wd.WriteData(structureFile);
			wd.WriteDataXs(structureFile);
			wd.WriteDataFlat(structureFile);

            if (HasConsole)
            {
                Console.Write("Press any key to continue . . . ");
                Console.ReadKey(true);
            }
		}
	}
}