﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataWriter.cs" company="EUROSTAT">
//   Date Created : 2015-04-08
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of GuideTests.
//     GuideTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     GuideTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with GuideTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System;

using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace Chapter3WritingData
{
	/// <summary>
	/// Description of SampleDataWriter.
	/// </summary>
	public class SampleDataWriter
	{
	    public void WriteSampleData(IDataStructureObject dsd, IDataflowObject dataflow, IDataWriterEngine dwe) {
			dwe.StartDataset(dataflow, dsd, null);
			
			dwe.StartSeries();
			dwe.WriteSeriesKeyValue("COUNTRY", "UK");
			dwe.WriteSeriesKeyValue("INDICATOR", "E_P");
			dwe.WriteObservation("2000", "18,22");
			dwe.WriteObservation("2000-02", "17,63");
			
			dwe.StartSeries();
			dwe.WriteSeriesKeyValue("COUNTRY", "FR");
			dwe.WriteSeriesKeyValue("INDICATOR", "E_P");
			dwe.WriteObservation("2000", "22,22");
			dwe.WriteObservation("2000-Q3", "15,63");
			
			dwe.Close();
		}
	}
}
