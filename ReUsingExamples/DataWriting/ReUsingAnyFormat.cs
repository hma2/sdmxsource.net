﻿// -----------------------------------------------------------------------
// <copyright file="ReUsingCompactWriter.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of ReUsingExamples.
//     ReUsingExamples is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     ReUsingExamples is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with ReUsingExamples.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace ReUsingExamples.DataWriting
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Estat.Sri.SdmxStructureMutableParser.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     The re using compact writer.
    /// </summary>
    public class ReUsingAnyuFormat
    {
        /// <summary>
        ///     The parsing manager.
        /// </summary>
        private static readonly IStructureParsingManager _parsingManager = new StructureMutableParsingManager();

        /// <summary>
        ///     The main.
        /// </summary>
        /// <param name="args">
        ///     The args.
        /// </param>
        public static void Main(string[] args)
        {
            IDataWriterManager manager = new DataWriterManager(/* data factories here*/);

            // 1. We need a IDataStructureObject. In this example we read it from a file. Alternative we could build it from a mutable object.
            var dataStructure = GetDataStructure();

            // 2. We get the data format
            var sdmxDataFormatCore = GetDataFormat(args);

            // 3. We get the writer
            using (var outputStream = File.Create(args[1]))
            using (var dataWriterEngine = manager.GetDataWriterEngine(sdmxDataFormatCore, outputStream))
            {
                // write header
                dataWriterEngine.WriteHeader(new HeaderImpl("ZZ9", "ZZ9"));


                // start dataset
                dataWriterEngine.StartDataset(null, dataStructure, null);

                foreach (var datasetPair in ExecuteGetDatasetAttributesQuery())
                {
                    // write dataset attributes
                    dataWriterEngine.WriteAttributeValue(datasetPair.Concept, datasetPair.Code);
                }

                // write group data
                foreach (var sdmxGroup in dataStructure.Groups)
                {
                    // first we write the group id
                    dataWriterEngine.StartGroup(sdmxGroup.Id);
                    foreach (var row in ExecuteGetGroupComponents(sdmxGroup.Id))
                    {
                        foreach (var keyValue in row)
                        {
                            var component = dataStructure.GetComponent(keyValue.Concept);
                            if (component.StructureType == SdmxStructureEnumType.Dimension)
                            {
                                // then we write the group dimensions
                                dataWriterEngine.WriteGroupKeyValue(keyValue.Concept, keyValue.Code);
                            }
                            else if (component.StructureType == SdmxStructureEnumType.DataAttribute)
                            {
                                // then we write the group attributes
                                dataWriterEngine.WriteAttributeValue(keyValue.Concept, keyValue.Code);
                            }
                        }
                    }
                }

                var dimensions = dataStructure.GetDimensions(SdmxStructureEnumType.Dimension);
                var seriesAttributes = GetSeriesAttributeIds();
                var observationAttributeIds = GetObsAttributeIds();

                string[] lastSeries = null;
                foreach (string[] row in ExecuteSeriesQuery())
                {
                    var currentKey = GetKey(dimensions, row);

                    // start a new series and writer the series key and attributes only if we have a new series key
                    if (lastSeries == null || !lastSeries.SequenceEqual(currentKey))
                    {
                        lastSeries = currentKey;
                        dataWriterEngine.StartSeries();
                        for (int index = 0; index < dimensions.Count; index++)
                        {
                            var dimension = dimensions[index];
                            var dimensionValue = currentKey[index];

                            // write series key
                            dataWriterEngine.WriteSeriesKeyValue(dimension.Id, dimensionValue);
                        }

                        for (int i = 0; i < seriesAttributes.Length; i++)
                        {
                            var attributeId = seriesAttributes[i];
                            var attributeValue = row[dimensions.Count + i];

                            // write series attributes
                            dataWriterEngine.WriteAttributeValue(attributeId, attributeValue);
                        }
                    }

                    // write observations
                    var observationRow = row.Skip(dimensions.Count + seriesAttributes.Length).ToArray();

                    var observationDimensionValue = observationRow[0];
                    var observationValue = observationRow[1];
                    dataWriterEngine.WriteObservation(observationDimensionValue, observationValue);

                    var observationAttributeValues = observationRow.Skip(2).ToArray();
                    for (int index = 0; index < observationAttributeIds.Length; index++)
                    {
                        var attributeId = observationAttributeIds[index];
                        var attributeValue = observationAttributeValues[index];

                        // write observation attributes
                        dataWriterEngine.WriteAttributeValue(attributeId, attributeValue);
                    }
                }
            }
        }


        private static string[] GetKey(IList<IDimension> dimensions, string[] row)
        {
            return row.Take(dimensions.Count).ToArray();
        }

        private static string[] GetSeriesAttributeIds()
        {
            return new[] { "TIME_FORMAT" };
        }
        private static string[] GetObsAttributeIds()
        {
            return new[] { "OBS_STATUS", "OBS_CONF" };
        }

        private static IEnumerable<string[]> ExecuteSeriesQuery()
        {
            yield return new[] { "A", "EL", "PROD", "NS0030", "1", "N", "P1Y", "2001", "1.23", "A", "F", };
            yield return new[] { "A", "EL", "PROD", "NS0030", "1", "N", "P1Y", "2002", "4.56", "A", "F", };
            yield return new[] { "A", "EL", "IND", "NS0030", "1", "N", "P1Y", "2001", "7.89", "A", "F", };
        }

        /// <summary>
        /// Gets the dataset attributes. A sample method emulating a DB query
        /// </summary>
        /// <returns>Attributes one time at a row</returns>
        private static IEnumerable<IKeyValue> ExecuteGetDatasetAttributesQuery()
        {
            yield return new KeyValueImpl("Any format code test", "TITLE");
        }


        private static IEnumerable<IList<IKeyValue>> ExecuteGetGroupComponents(string groupId)
        {
            yield return
                new IKeyValue[]
                    {
                        new KeyValueImpl("EL", "REF_AREA"), new KeyValueImpl("PROD", "STS_INDICATOR"), new KeyValueImpl("NS0030", "STS_ACTIVITY"), new KeyValueImpl("1", "STS_INSTITUTION"), new KeyValueImpl("N", "ADJUSTMENT"),
                        new KeyValueImpl("test", "COMPILATION")
                    };
            yield return new IKeyValue[]
            {
                new KeyValueImpl("EL", "REF_AREA"),
                new KeyValueImpl("IND", "STS_INDICATOR"),
                new KeyValueImpl("NS0030", "STS_ACTIVITY"),
                new KeyValueImpl("1", "STS_INSTITUTION"),
                new KeyValueImpl("N", "ADJUSTMENT"),
                new KeyValueImpl("test2", "COMPILATION")
            };

        }

        private static SdmxDataFormatCore GetDataFormat(string[] args)
        {
            DataEnumType format = (DataEnumType)Enum.Parse(typeof(DataEnumType), args[0], true);
            DataType dataType = DataType.GetFromEnum(format);
            var sdmxDataFormatCore = new SdmxDataFormatCore(dataType);
            return sdmxDataFormatCore;
        }

        private static IDataStructureObject GetDataStructure()
        {
            IDataStructureObject dataStructure;
            using (IReadableDataLocation readable = new FileReadableDataLocation("ESTAT+STS+2.0.xml"))
            {
                IStructureWorkspace structureWorkspace = _parsingManager.ParseStructures(readable);

                ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);
                dataStructure = structureBeans.DataStructures.FirstOrDefault();
            }

            if (dataStructure == null)
            {
                throw new InvalidOperationException("Could not build dataStructure object");
            }
            return dataStructure;
        }
    }
}