// -----------------------------------------------------------------------
// <copyright file="NameTableCache.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxMlConstants.
//     SdmxMlConstants is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxMlConstants is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstants.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxXmlConstants
{
    using System;

    /// <summary>
    ///     The name table cache.
    /// </summary>
    public sealed class NameTableCache
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly NameTableCache _instance = new NameTableCache();

        /// <summary>
        ///     An containing the <see cref="System.Xml.NameTable" /> string references and are indexed
        ///     by enumerations <see cref="ElementNameTable" /> and <see cref="AttributeNameTable" />
        /// </summary>
        private readonly object[] _elementNameCache;

        /// <summary>
        ///     A name table object that stores all the SDMX-ML elements
        /// </summary>
        private readonly ConcurrentNameTable _nameTable;

        /// <summary>
        ///     Prevents a default instance of the <see cref="NameTableCache" /> class from being created.
        ///     Initialize name table and element name cache object array both containing the same object references.
        ///     The name table would be used in XmlReaderSettings and the elementNameCache object array
        ///     would be used with <see cref="NameTableCache.IsElement" /> to atomically compare element local names.
        /// </summary>
        private NameTableCache()
        {
            this._nameTable = new ConcurrentNameTable();
            string[] elementNames = Enum.GetNames(typeof(ElementNameTable));
            string[] attributeNames = Enum.GetNames(typeof(AttributeNameTable));
            this._elementNameCache = new object[elementNames.Length + attributeNames.Length];

            for (int i = 0; i < elementNames.Length; i++)
            {
                this._elementNameCache[i] = this._nameTable.Add(elementNames[i]);
            }

            int count = elementNames.Length;

            for (int i = 0; i < attributeNames.Length; i++)
            {
                this._elementNameCache[count++] = this._nameTable.Add(attributeNames[i]);
            }
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static NameTableCache Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Gets the name table object that stores all the SDMX-ML elements
        /// </summary>
        public ConcurrentNameTable NameTable
        {
            get
            {
                return this._nameTable;
            }
        }

        /// <summary>
        ///     Gets the <paramref name="attributeName" /> name string
        /// </summary>
        /// <param name="attributeName">
        ///     The element name
        /// </param>
        /// <returns>
        ///     The atomized string of the <paramref name="attributeName" />
        /// </returns>
        public static string GetAttributeName(AttributeNameTable attributeName)
        {
            return (string)_instance._elementNameCache[(int)attributeName];
        }

        /// <summary>
        ///     Gets the <paramref name="elementName" /> name string
        /// </summary>
        /// <param name="elementName">
        ///     The element name
        /// </param>
        /// <returns>
        ///     The atomized string of the <paramref name="elementName" />
        /// </returns>
        public static string GetElementName(ElementNameTable elementName)
        {
            return (string)_instance._elementNameCache[(int)elementName];
        }

        /// <summary>
        ///     Checks if the given <paramref name="localName" /> equals to the given <paramref name="attributeName" />.
        /// </summary>
        /// <param name="localName">
        ///     Objectified string containing the current local name
        /// </param>
        /// <param name="attributeName">
        ///     The element name to check against
        /// </param>
        /// <returns>
        ///     True if the <paramref name="localName" /> is <paramref name="attributeName" />
        /// </returns>
        public static bool IsAttribute(object localName, AttributeNameTable attributeName)
        {
            return ReferenceEquals(localName, _instance._elementNameCache[(int)attributeName]);
        }

        /// <summary>
        ///     Checks if the given <paramref name="localName" /> equals to the given <paramref name="elementName" />.
        /// </summary>
        /// <param name="localName">
        ///     Objectified string containing the current local name
        /// </param>
        /// <param name="elementName">
        ///     The element name to check against
        /// </param>
        /// <returns>
        ///     True if the <paramref name="localName" /> is <paramref name="elementName" />
        /// </returns>
        public static bool IsElement(object localName, ElementNameTable elementName)
        {
            object objB = _instance._elementNameCache[(int)elementName];
            return ReferenceEquals(localName, objB);
        }
    }
}