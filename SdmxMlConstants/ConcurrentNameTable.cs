// -----------------------------------------------------------------------
// <copyright file="ConcurrentNameTable.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxMlConstants.
//     SdmxMlConstants is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxMlConstants is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxMlConstants.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.SdmxXmlConstants
{
    using System.Xml;

    /// <summary>
    ///     A concurrent version of NameTable. It locks Add methods
    /// </summary>
    /// <seealso cref="System.Xml.NameTable" />
    public class ConcurrentNameTable : NameTable
    {
        /// <summary>
        ///     This field is used to lock Add methods
        /// </summary>
        private readonly object _lockAdd;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConcurrentNameTable" /> class.
        /// </summary>
        public ConcurrentNameTable()
        {
            this._lockAdd = new object();
        }

        /// <summary>
        ///     Atomizes the specified string and adds it to the NameTable.
        /// </summary>
        /// <param name="key">
        ///     The string to add.
        /// </param>
        /// <returns>
        ///     The atomized string or the existing string if it already exists in the NameTable.
        /// </returns>
        public override string Add(string key)
        {
            lock (this._lockAdd)
            {
                return base.Add(key);
            }
        }

        /// <summary>
        ///     Atomizes the specified string and adds it to the NameTable.
        /// </summary>
        /// <param name="key">
        ///     The character array containing the string to add.
        /// </param>
        /// <param name="start">
        ///     The zero-based index into the array specifying the first character of the string.
        /// </param>
        /// <param name="len">
        ///     The number of characters in the string.
        /// </param>
        /// <returns>
        ///     The atomized string or the existing string if it already exists in the NameTable.
        /// </returns>
        public override string Add(char[] key, int start, int len)
        {
            lock (this._lockAdd)
            {
                return base.Add(key, start, len);
            }
        }
    }
}