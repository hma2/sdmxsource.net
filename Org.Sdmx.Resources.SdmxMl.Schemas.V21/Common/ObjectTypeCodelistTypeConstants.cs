﻿// -----------------------------------------------------------------------
// <copyright file="ObjectTypeCodelistTypeConstants.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common
{
    /// <summary>
    ///     The object type code list type constants class
    /// </summary>
    public static class ObjectTypeCodelistTypeConstants
    {
        /// <summary>
        ///     The agency
        /// </summary>
        public const string Agency = "Agency";

        /// <summary>
        ///     The agency scheme
        /// </summary>
        public const string AgencyScheme = "AgencyScheme";

        /// <summary>
        ///     Any type
        /// </summary>
        public const string Any = "Any";

        /// <summary>
        ///     The attachment constraint
        /// </summary>
        public const string AttachmentConstraint = "AttachmentConstraint";

        /// <summary>
        ///     The attribute
        /// </summary>
        public const string Attribute = "Attribute";

        /// <summary>
        ///     The attribute descriptor
        /// </summary>
        public const string AttributeDescriptor = "AttributeDescriptor";

        /// <summary>
        ///     The categorisation
        /// </summary>
        public const string Categorisation = "Categorisation";

        /// <summary>
        ///     The category
        /// </summary>
        public const string Category = "Category";

        /// <summary>
        ///     The category scheme
        /// </summary>
        public const string CategoryScheme = "CategoryScheme";

        /// <summary>
        ///     The category scheme map
        /// </summary>
        public const string CategorySchemeMap = "CategorySchemeMap";

        /// <summary>
        ///     The code
        /// </summary>
        public const string Code = "Code";

        /// <summary>
        ///     The code list
        /// </summary>
        public const string Codelist = "Codelist";

        /// <summary>
        ///     The codelist map
        /// </summary>
        public const string CodelistMap = "CodelistMap";

        /// <summary>
        ///     The code map
        /// </summary>
        public const string CodeMap = "CodeMap";

        /// <summary>
        ///     The component map
        /// </summary>
        public const string ComponentMap = "ComponentMap";

        /// <summary>
        ///     The concept
        /// </summary>
        public const string Concept = "Concept";

        /// <summary>
        ///     The concept map
        /// </summary>
        public const string ConceptMap = "ConceptMap";

        /// <summary>
        ///     The concept scheme
        /// </summary>
        public const string ConceptScheme = "ConceptScheme";

        /// <summary>
        ///     The concept scheme map
        /// </summary>
        public const string ConceptSchemeMap = "ConceptSchemeMap";

        /// <summary>
        ///     The constraint
        /// </summary>
        public const string Constraint = "Constraint";

        /// <summary>
        ///     The constraint target
        /// </summary>
        public const string ConstraintTarget = "ConstraintTarget";

        /// <summary>
        ///     The content constraint
        /// </summary>
        public const string ContentConstraint = "ContentConstraint";

        /// <summary>
        ///     The data consumer
        /// </summary>
        public const string DataConsumer = "DataConsumer";

        /// <summary>
        ///     The data consumer scheme
        /// </summary>
        public const string DataConsumerScheme = "DataConsumerScheme";

        /// <summary>
        ///     The data flow
        /// </summary>
        public const string Dataflow = "Dataflow";

        /// <summary>
        ///     The data provider
        /// </summary>
        public const string DataProvider = "DataProvider";

        /// <summary>
        ///     The data provider scheme
        /// </summary>
        public const string DataProviderScheme = "DataProviderScheme";

        /// <summary>
        ///     The data set target
        /// </summary>
        public const string DataSetTarget = "DataSetTarget";

        /// <summary>
        ///     The data structure
        /// </summary>
        public const string DataStructure = "DataStructure";

        /// <summary>
        ///     The dimension
        /// </summary>
        public const string Dimension = "Dimension";

        /// <summary>
        ///     The dimension descriptor
        /// </summary>
        public const string DimensionDescriptor = "DimensionDescriptor";

        /// <summary>
        ///     The dimension descriptor values target
        /// </summary>
        public const string DimensionDescriptorValuesTarget = "DimensionDescriptorValuesTarget";

        /// <summary>
        ///     The group dimension descriptor
        /// </summary>
        public const string GroupDimensionDescriptor = "GroupDimensionDescriptor";

        /// <summary>
        ///     The hierarchical code
        /// </summary>
        public const string HierarchicalCode = "HierarchicalCode";

        /// <summary>
        ///     The hierarchical codelist
        /// </summary>
        public const string HierarchicalCodelist = "HierarchicalCodelist";

        /// <summary>
        ///     The hierarchy
        /// </summary>
        public const string Hierarchy = "Hierarchy";

        /// <summary>
        ///     The hybrid codelist map
        /// </summary>
        public const string HybridCodelistMap = "HybridCodelistMap";

        /// <summary>
        ///     The hybrid code map
        /// </summary>
        public const string HybridCodeMap = "HybridCodeMap";

        /// <summary>
        ///     The identifiable object target
        /// </summary>
        public const string IdentifiableObjectTarget = "IdentifiableObjectTarget";

        /// <summary>
        ///     The level
        /// </summary>
        public const string Level = "Level";

        /// <summary>
        ///     The measure descriptor
        /// </summary>
        public const string MeasureDescriptor = "MeasureDescriptor";

        /// <summary>
        ///     The measure dimension
        /// </summary>
        public const string MeasureDimension = "MeasureDimension";

        /// <summary>
        ///     The metadata attribute
        /// </summary>
        public const string MetadataAttribute = "MetadataAttribute";

        /// <summary>
        ///     The metadataflow
        /// </summary>
        public const string Metadataflow = "Metadataflow";

        /// <summary>
        ///     The metadata set
        /// </summary>
        public const string MetadataSet = "MetadataSet";

        /// <summary>
        ///     The metadata structure
        /// </summary>
        public const string MetadataStructure = "MetadataStructure";

        /// <summary>
        ///     The metadata target
        /// </summary>
        public const string MetadataTarget = "MetadataTarget";

        /// <summary>
        ///     The organisation
        /// </summary>
        public const string Organisation = "Organisation";

        /// <summary>
        ///     The organisation map
        /// </summary>
        public const string OrganisationMap = "OrganisationMap";

        /// <summary>
        ///     The organisation scheme
        /// </summary>
        public const string OrganisationScheme = "OrganisationScheme";

        /// <summary>
        ///     The organisation scheme map
        /// </summary>
        public const string OrganisationSchemeMap = "OrganisationSchemeMap";

        /// <summary>
        ///     The organisation unit
        /// </summary>
        public const string OrganisationUnit = "OrganisationUnit";

        /// <summary>
        ///     The organisation unit scheme
        /// </summary>
        public const string OrganisationUnitScheme = "OrganisationUnitScheme";

        /// <summary>
        ///     The primary measure
        /// </summary>
        public const string PrimaryMeasure = "PrimaryMeasure";

        /// <summary>
        ///     The process
        /// </summary>
        public const string Process = "Process";

        /// <summary>
        ///     The process step
        /// </summary>
        public const string ProcessStep = "ProcessStep";

        /// <summary>
        ///     The provision agreement
        /// </summary>
        public const string ProvisionAgreement = "ProvisionAgreement";

        /// <summary>
        ///     The reporting category
        /// </summary>
        public const string ReportingCategory = "ReportingCategory";

        /// <summary>
        ///     The reporting category map
        /// </summary>
        public const string ReportingCategoryMap = "ReportingCategoryMap";

        /// <summary>
        ///     The reporting taxonomy
        /// </summary>
        public const string ReportingTaxonomy = "ReportingTaxonomy";

        /// <summary>
        ///     The reporting taxonomy map
        /// </summary>
        public const string ReportingTaxonomyMap = "ReportingTaxonomyMap";

        /// <summary>
        ///     The reporting year start day
        /// </summary>
        public const string ReportingYearStartDay = "ReportingYearStartDay";

        /// <summary>
        ///     The report period target
        /// </summary>
        public const string ReportPeriodTarget = "ReportPeriodTarget";

        /// <summary>
        ///     The report structure
        /// </summary>
        public const string ReportStructure = "ReportStructure";

        /// <summary>
        ///     The structure map
        /// </summary>
        public const string StructureMap = "StructureMap";

        /// <summary>
        ///     The structure set
        /// </summary>
        public const string StructureSet = "StructureSet";

        /// <summary>
        ///     The time dimension
        /// </summary>
        public const string TimeDimension = "TimeDimension";

        /// <summary>
        ///     The transition
        /// </summary>
        public const string Transition = "Transition";
    }
}