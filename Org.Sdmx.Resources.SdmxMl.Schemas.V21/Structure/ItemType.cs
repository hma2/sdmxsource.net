﻿// -----------------------------------------------------------------------
// <copyright file="ItemType.cs" company="EUROSTAT">
//   Date Created : 2014-04-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using System.Xml.Linq;

    using Common;

    using Message;

    using Xml.Schema.Linq;

    /// <summary>
    ///     The item type.
    /// </summary>
    public partial class ItemType
    {
        /// <summary>
        ///     The Parent element name
        /// </summary>
        private static readonly XName _parentName = XName.Get(
            "Parent", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure");

        /// <summary>
        ///     Gets the typed parent.
        /// </summary>
        /// <typeparam name="T">The type of the parent.</typeparam>
        /// <returns>The parent item of this item; otherwise null.</returns>
        public T GetTypedParent<T>() where T : LocalItemReferenceType
        {
            XElement x = this.GetElement(_parentName);
            return XTypedServices.ToXTypedElement<T>(x, LinqToXsdTypeManager.Instance);
        }

        /// <summary>
        ///     Sets the typed parent.
        /// </summary>
        /// <typeparam name="T">The type of the parent.</typeparam>
        /// <param name="value">The value.</param>
        public void SetTypedParent<T>(T value) where T : LocalItemReferenceType
        {
            this.SetElement(_parentName, value);
        }
    }
}