﻿// -----------------------------------------------------------------------
// <copyright file="ItemAssociation.cs" company="EUROSTAT">
//   Date Created : 2014-10-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure
{
    using Common;

    /// <summary>
    ///     The Item association class
    /// </summary>
    /// <seealso cref="Xml.Schema.Linq.XTypedElement" />
    public abstract partial class ItemAssociation
    {
        /// <summary>
        ///     Gets the typed source.
        /// </summary>
        /// <typeparam name="T">The <see cref="ItemSchemeReferenceBaseType" /> based type.</typeparam>
        /// <returns>he <see cref="ItemSchemeReferenceBaseType" /> based instance.</returns>
        public T GetTypedSource<T>() where T : LocalItemReferenceType
        {
            return this.ContentField.GetTypedSource<T>();
        }

        /// <summary>
        ///     Gets the typed source.
        /// </summary>
        /// <typeparam name="T">The <see cref="ItemSchemeReferenceBaseType" /> based type.</typeparam>
        /// <returns>he <see cref="ItemSchemeReferenceBaseType" /> based instance.</returns>
        public T GetTypedTarget<T>() where T : LocalItemReferenceType
        {
            return this.ContentField.GetTypedTarget<T>();
        }
    }
}