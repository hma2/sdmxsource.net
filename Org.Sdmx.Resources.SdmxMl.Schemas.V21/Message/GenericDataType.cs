// -----------------------------------------------------------------------
// <copyright file="GenericDataType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml.Linq;

    using Data.Generic;

    using Xml.Schema.Linq;

    /// <summary>
    ///     Additions to the auto-generated <see cref="GenericDataType" />
    /// </summary>
    public partial class GenericDataType
    {
        /// <summary>
        ///     The contents type.
        /// </summary>
        private static readonly Type _contentsType = typeof(DataSetType);

        /// <summary>
        ///     The contents element name
        /// </summary>
        private static readonly XName _contentsXName = XName.Get(
            "DataSet", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message");

        /// <summary>
        ///     The footer type.
        /// </summary>
        private static readonly Type _footerType = typeof(Footer.Footer);

        /// <summary>
        ///     The _footer element name.
        /// </summary>
        private static readonly XName _footerXName = XName.Get(
            "Footer", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer");

        /// <summary>
        ///     The header type.
        /// </summary>
        private static readonly Type _headerType = typeof(GenericDataHeaderType);

        /// <summary>
        ///     The _header element name.
        /// </summary>
        private static readonly XName _headerXName = XName.Get(
            "Header", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message");

        /// <summary>
        ///     The local element dictionary.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private static readonly Dictionary<XName, Type> _localElementDictionary = new Dictionary<XName, Type>();

        /// <summary>
        ///     The validation states.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private static FSM validationStates;

        /// <summary>
        ///     The DataSet List.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private XTypedList<DataSetType> _dataSetType;

        /// <summary>
        ///     Initializes static members of the <see cref="GenericDataType" /> class.
        /// </summary>
        static GenericDataType()
        {
            BuildElementDictionary();
            InitFsm();
        }

        /// <summary>
        ///     Gets or sets the DataSet structures.
        /// </summary>
        public IList<DataSetType> DataSet
        {
            get
            {
                return this._dataSetType
                       ?? (this._dataSetType =
                           new XTypedList<DataSetType>(this, LinqToXsdTypeManager.Instance, _contentsXName));
            }

            set
            {
                if (value == null)
                {
                    this._dataSetType = null;
                }
                else
                {
                    if (this._dataSetType == null)
                    {
                        this._dataSetType = XTypedList<DataSetType>.Initialize(
                            this, 
                            LinqToXsdTypeManager.Instance, 
                            value, 
                            _contentsXName);
                    }
                    else
                    {
                        XTypedServices.SetList(this._dataSetType, value);
                    }
                }
            }
        }

        /// <summary>
        ///     Gets or sets the header.
        /// </summary>
        public new GenericDataHeaderType Header
        {
            get
            {
                XElement x = this.GetElement(_headerXName);
                return (GenericDataHeaderType)x;
            }

            set
            {
                this.SetElement(_headerXName, value);
            }
        }

        /// <summary>
        ///     Gets the local elements dictionary.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        Dictionary<XName, Type> IXMetaData.LocalElementsDictionary
        {
            get { return _localElementDictionary; }
        }

        /// <summary>
        ///     Gets validation states.
        /// </summary>
        /// <returns>
        ///     The validation states.
        /// </returns>
        FSM IXMetaData.GetValidationStates()
        {
            return validationStates;
        }

        /// <summary>
        ///     Build element dictionary.
        /// </summary>
        private static void BuildElementDictionary()
        {
            _localElementDictionary.Add(_headerXName, _headerType);
            _localElementDictionary.Add(_contentsXName, _contentsType);
            _localElementDictionary.Add(_footerXName, _footerType);
        }

        /// <summary>
        ///     Initialize the <c>FSM</c>
        /// </summary>
        private static void InitFsm()
        {
            // TODO check if the transition numbers just need to be in order or need to have specific values ?!? 
            var transitions = new Dictionary<int, Transitions>
            {
                {
                    1, 
                    new Transitions(
                        new SingleTransition(_headerXName, 2))
                }, 
                {
                    2, 
                    new Transitions(
                        new SingleTransition(_contentsXName, 4))
                }, 
                {
                    4, 
                    new Transitions(
                        new SingleTransition(_footerXName, 6), 
                        new SingleTransition(
                            new WildCard(
                                "##targetNamespace", 
                                "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message"), 
                            2))
                }
            };
            validationStates = new FSM(1, new Set<int>(new[] { 2, 4, 6 }), transitions);
        }
    }
}