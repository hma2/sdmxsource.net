﻿// -----------------------------------------------------------------------
// <copyright file="Error.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message
{
    using System.Xml;

    using Xml.Schema.Linq;

    /// <summary>
    ///     <para>
    ///         ErrorType describes the structure of an error response.
    ///     </para>
    /// </summary>
    public partial class Error
    {
        /// <summary>
        ///     Load error message to a LINQ2XSD <see cref="Error" /> object
        /// </summary>
        /// <param name="xmlFile">The input file</param>
        /// <returns>a LINQ2XSD <see cref="Error" /> object</returns>
        public static Error Load(XmlReader xmlFile)
        {
            return XTypedServices.Load<Error, ErrorType>(xmlFile, LinqToXsdTypeManager.Instance);
        }
    }
}