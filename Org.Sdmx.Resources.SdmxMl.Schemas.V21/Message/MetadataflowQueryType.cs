// -----------------------------------------------------------------------
// <copyright file="MetadataflowQueryType.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of Org.Sdmx.Resources.SdmxMl.Schemas.V21.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     Org.Sdmx.Resources.SdmxMl.Schemas.V21 is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with Org.Sdmx.Resources.SdmxMl.Schemas.V21.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Xml.Linq;

    using Xml.Schema.Linq;

    /// <summary>
    ///     The metadataflow query type.
    /// </summary>
    public partial class MetadataflowQueryType
    {
        /// <summary>
        ///     The contents type.
        /// </summary>
        private static readonly Type _contentsType = typeof(Query.MetadataflowQueryType);

        /// <summary>
        ///     The contents element name
        /// </summary>
        private static readonly XName _contentsXName = XName.Get(
            "Query", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message");

        /// <summary>
        ///     The footer type.
        /// </summary>
        private static readonly Type _footerType = typeof(Footer.Footer);

        /// <summary>
        ///     The _footer element name.
        /// </summary>
        private static readonly XName _footerXName = XName.Get(
            "Footer", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer");

        /// <summary>
        ///     The header type.
        /// </summary>
        private static readonly Type _headerType = typeof(BasicHeaderType);

        /// <summary>
        ///     The _header element name.
        /// </summary>
        private static readonly XName _headerXName = XName.Get(
            "Header", 
            "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message");

        /// <summary>
        ///     The local element dictionary.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private static readonly Dictionary<XName, Type> _localElementDictionary = new Dictionary<XName, Type>();

        /// <summary>
        ///     The validation states.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)] private static FSM validationStates;

        /// <summary>
        ///     Initializes static members of the <see cref="MetadataflowQueryType" /> class.
        /// </summary>
        static MetadataflowQueryType()
        {
            BuildElementDictionary();
            InitFsm();
        }

        /// <summary>
        ///     Gets or sets the header.
        /// </summary>
        public new BasicHeaderType Header
        {
            get
            {
                XElement x = this.GetElement(_headerXName);
                return (BasicHeaderType)x;
            }

            set
            {
                this.SetElement(_headerXName, value);
            }
        }

        /// <summary>
        ///     Gets or sets the Query.
        /// </summary>
        /// <remarks>
        ///     It is named as <c>SdmxQuery</c> instead of <c>Query</c> to avoid name conflict with base <see cref="Query" />
        /// </remarks>
        public Query.MetadataflowQueryType SdmxQuery
        {
            get
            {
                XElement x = this.GetElement(_contentsXName);
                return (Query.MetadataflowQueryType)x;
            }

            set
            {
                this.SetElement(_contentsXName, value);
            }
        }

        /// <summary>
        ///     Gets the local elements dictionary.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        Dictionary<XName, Type> IXMetaData.LocalElementsDictionary
        {
            get { return _localElementDictionary; }
        }

        /// <summary>
        ///     Gets validation states.
        /// </summary>
        /// <returns>
        ///     The validation states.
        /// </returns>
        FSM IXMetaData.GetValidationStates()
        {
            return validationStates;
        }

        /// <summary>
        ///     Build element dictionary.
        /// </summary>
        private static void BuildElementDictionary()
        {
            _localElementDictionary.Add(_headerXName, _headerType);
            _localElementDictionary.Add(_contentsXName, _contentsType);
            _localElementDictionary.Add(_footerXName, _footerType);
        }

        /// <summary>
        ///     Initialize the <c>FSM</c>
        /// </summary>
        private static void InitFsm()
        {
            // TODO check if the transition numbers just need to be in order or need to have specific values ?!? 
            var transitions = new Dictionary<int, Transitions>
            {
                {
                    1, 
                    new Transitions(
                        new SingleTransition(_headerXName, 2))
                }, 
                {
                    2, 
                    new Transitions(
                        new SingleTransition(_contentsXName, 4))
                }, 
                {
                    4, 
                    new Transitions(
                        new SingleTransition(_footerXName, 6), 
                        new SingleTransition(
                            new WildCard(
                                "##targetNamespace", 
                                "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message"), 
                            2))
                }
            };
            validationStates = new FSM(1, new Set<int>(new[] { 2, 4, 6 }), transitions);
        }
    }
}