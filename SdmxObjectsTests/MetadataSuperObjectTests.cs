﻿// -----------------------------------------------------------------------
// <copyright file="MetadataSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-03-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.IO;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Metadata;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Extension;

namespace SdmxObjectsTests
{
    [TestFixture]
    internal class MetadataSuperObjectTests
    {
        [SetUp]
        public virtual void SetUp()
        {
            var sdmxObjects = new FileInfo("../../StructureReferences/MetadataStructures.xml").GetSdmxObjects(new StructureParsingManager());
            retMan = new InMemoryRetrievalManager(sdmxObjects);
        }

        private IdentifiableRetrievalManagerCore retMan;

        public MetadataSuperObject CreateSUT()
        {
            return new MetadataSuperObject(Utils.Metadata, retMan);
        }

        public class When_constructing_a_MetadataSuperObject : MetadataSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }
        }
    }
}