// -----------------------------------------------------------------------
// <copyright file="CategorisationSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-03-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.CategoryScheme;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Util.Io;

namespace SdmxObjectsTests
{
    [TestFixture]
    class CategorisationSuperObjectTests
    {
        private IIdentifiableRetrievalManager retMan;
        private ICategorisationObject categorisation;

        [SetUp]
        public virtual void SetUp()
        {
            retMan = new IdentifiableRetrievalManagerCore(null, new InMemoryRetrievalManager(Utils.Structures));
            categorisation = Utils.Categorisation;
        }

        /// <summary>
        /// Creates the sut.
        /// </summary>
        /// <returns></returns>
        public CategorisationSuperObject CreateSUT()
        {
            return new CategorisationSuperObject(categorisation, retMan);
        }

        public class When_constructing_a_CategorisationSuperObject : CategorisationSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }
        }
    }
}