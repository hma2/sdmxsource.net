﻿// -----------------------------------------------------------------------
// <copyright file="HierarchicalCodelistSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using FluentAssertions;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;

namespace SdmxObjectsTests
{
    [TestFixture]
    public class HierarchicalCodelistSuperObjectTests
    {
        private IHierarchicalCodelistObject hcl;
        private List<ICodelistObject> referencedCodelists;

        [SetUp]
        public virtual void SetUp()
        {
            hcl = Utils.HierarchicalCodelist;
            referencedCodelists = Utils.CodelistsForHierarchical;
        }

        public HierarchicalCodelistSuperObject CreateSUT()
        {
            return new HierarchicalCodelistSuperObject(hcl, referencedCodelists);
        }

        public class When_constructing_a_HierarchicalCodelistSuperObject : HierarchicalCodelistSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }

            [Test]
            public void Should_contain_the_original_HierarchicalCodelistObject()
            {
                var sut = CreateSUT();

                foreach (var item in hcl.Hierarchies)
                {
                    sut.Hierarchies.Should().Contain(x => x.Id == item.Id);
                }
            }
        }
    }
}