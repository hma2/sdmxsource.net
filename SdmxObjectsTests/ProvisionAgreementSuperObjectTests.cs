// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

using Rhino.Mocks;

namespace SdmxObjectsTests
{
    [TestFixture]
    class ProvisionAgreementSuperObjectTests
    {
        private IProvisionAgreementObject provisionAgreement;
        private IDataflowSuperObject dataflow;
        private IDataProvider dataProvider;

        [SetUp]
        public virtual void SetUp()
        {
            var codelist = new CodelistSuperObject(Utils.Codelist);
            var concept = new ConceptSuperObject(Utils.ConceptScheme.Items.FirstOrDefault(), codelist);
            var primaryMeasure = new PrimaryMeasureSuperObject(Utils.DataStructure.PrimaryMeasure, codelist, concept);
            var dimensions = new List<IDimensionSuperObject>(Utils.DataStructure.DimensionList.Dimensions.Select(x => new DimensionSuperObject(x, codelist, concept)));
            var attributes = new List<IAttributeSuperObject>(Utils.DataStructure.Attributes.Select(x => new AttributeSuperObject(x, codelist, concept)));
            var dataStructure = new DataStructureSuperObject(Utils.DataStructure, dimensions, attributes, primaryMeasure);
            dataflow = new DataflowSuperObject(Utils.DataFlow, dataStructure);
            dataProvider = Utils.DataProvider;
            provisionAgreement = Utils.ProvisionAgreement;
        }

        /// <summary>
        /// Creates the sut.
        /// </summary>
        /// <returns></returns>
        public ProvisionAgreementSuperObject CreateSUT()
        {
            return new ProvisionAgreementSuperObject(provisionAgreement, dataflow, dataProvider);
        }

        public class When_constructing_a_ProvisionAgreementSuperObject : ProvisionAgreementSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }
        }
    }
}