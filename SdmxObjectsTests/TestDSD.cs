﻿// -----------------------------------------------------------------------
// <copyright file="TestDSD.cs" company="EUROSTAT">
//   Date Created : 2014-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects
{
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    using TextType = Org.Sdmxsource.Sdmx.Api.Constants.TextType;

    /// <summary>
    /// Tests for DSD
    /// </summary>
    [TestFixture]
    public class TestDSD
    {
        /// <summary>
        /// Tests the get observations.
        /// </summary>
        [Test]
        public void TestGetObservations()
        {
            var structureFile = new FileInfo(@"tests\v21\demography.xml");
            var sdmxObjects = structureFile.GetSdmxObjects(new StructureParsingManager());
            var dataStructureObject = sdmxObjects.DataStructures.First();
            Assert.IsNotEmpty(dataStructureObject.GetObservationAttributes(null));
            Assert.AreEqual(1, dataStructureObject.GetObservationAttributes(null).Count);
            Assert.IsTrue(dataStructureObject.GetObservationAttributes(null).Any(o => o.Id.Equals("OBS_STATUS")));
        }

        /// <summary>
        /// Tests the get observations.
        /// </summary>
        [Test]
        public void TestGetObservationsWithDimensionAtObservation()
        {
            var structureFile = new FileInfo(@"tests\v21\demography.xml");
            var sdmxObjects = structureFile.GetSdmxObjects(new StructureParsingManager());
            var dataStructureObject = sdmxObjects.DataStructures.First();
            Assert.IsNotEmpty(dataStructureObject.GetObservationAttributes("DEMO"));
            Assert.AreEqual(4, dataStructureObject.GetObservationAttributes("DEMO").Count);
            Assert.IsTrue(dataStructureObject.GetObservationAttributes("DEMO").Any(o => o.Id.Equals("OBS_STATUS")));
            Assert.IsTrue(dataStructureObject.GetObservationAttributes("DEMO").Any(o => o.Id.Equals("DECIMALS")));
            Assert.IsTrue(dataStructureObject.GetObservationAttributes("DEMO").Any(o => o.Id.Equals("UNIT_MEASURE")));
            Assert.IsTrue(dataStructureObject.GetObservationAttributes("DEMO").Any(o => o.Id.Equals("UNIT_MULT")));
        }

        /// <summary>
        /// Tests the parsing component text format.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="componentId">The component identifier.</param>
        /// <param name="textType">Type of the text.</param>
        [TestCase("tests/v20/ESTAT_CPI_v1.0.xml", "OBS_PRE_BREAK", TextEnumType.String)]
        [TestCase("tests/v20/ESTAT_CPI_v1.0_fixed.xml", "OBS_PRE_BREAK", TextEnumType.String)]
        public void TestParsingComponentTextFormat(string file, string componentId, TextEnumType textType)
        {
            var sdmxObjects = new FileInfo(file).GetSdmxObjects(new StructureParsingManager());
            var dsd = sdmxObjects.DataStructures.First().MutableInstance.ImmutableInstance;
            var component = dsd.GetComponent(componentId);
            Assert.IsNotNull(component.Representation);
            Assert.IsNotNull(component.Representation.TextFormat);
            Assert.AreEqual(textType, component.Representation.TextFormat.TextType.EnumType);
        }

        /// <summary>
        /// Tests the freq with concept role.
        /// </summary>
        [Test]
        public void TestFreqWithConceptRole()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.Id = "FREQ";
            dimension.ConceptRole.Add(new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "FREQ"));
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "FREQ");
            var immutable = BuildDataStructureObject(dimension);
            Assert.NotNull(immutable.FrequencyDimension);
        }

        /// <summary>
        /// Tests the dimension position without position.
        /// </summary>
        [Test]
        public void TestDimensionPositionWithoutPosition()
        {
            var structureFile = new FileInfo(@"tests\v21\demography.xml");
            var sdmxObjects = structureFile.GetSdmxObjects(new StructureParsingManager());
            var dataStructureObject = sdmxObjects.DataStructures.First();
            Assert.AreEqual("FREQ", dataStructureObject.DimensionList.Dimensions[0].Id);
            Assert.AreEqual(1, dataStructureObject.DimensionList.Dimensions[0].Position);

            Assert.AreEqual("COUNTRY", dataStructureObject.DimensionList.Dimensions[1].Id);
            Assert.AreEqual(2, dataStructureObject.DimensionList.Dimensions[1].Position);

            Assert.AreEqual("SEX", dataStructureObject.DimensionList.Dimensions[2].Id);
            Assert.AreEqual(3, dataStructureObject.DimensionList.Dimensions[2].Position);

            Assert.AreEqual("DEMO", dataStructureObject.DimensionList.Dimensions[3].Id);
            Assert.AreEqual(4, dataStructureObject.DimensionList.Dimensions[3].Position);
        }

        /// <summary>
        /// Tests the dimension position with position.
        /// </summary>
        /// <remarks>This behavior is not correct. See SDMXCONV-125</remarks>
        [Test]
        public void TestDimensionPositionWithPosition()
        {
            // See SDMXCONV-125 - test was re-writen to make sure no sorting is done
            var structureFile = new FileInfo(@"tests\v21\demography-with-position.xml");
            var sdmxObjects = structureFile.GetSdmxObjects(new StructureParsingManager());
            var dataStructureObject = sdmxObjects.DataStructures.First();
            Assert.AreEqual("FREQ", dataStructureObject.DimensionList.Dimensions[0].Id);
            Assert.AreEqual(4, dataStructureObject.DimensionList.Dimensions[0].Position);

            Assert.AreEqual("COUNTRY", dataStructureObject.DimensionList.Dimensions[1].Id);
            Assert.AreEqual(3, dataStructureObject.DimensionList.Dimensions[1].Position);

            Assert.AreEqual("SEX", dataStructureObject.DimensionList.Dimensions[2].Id);
            Assert.AreEqual(2, dataStructureObject.DimensionList.Dimensions[2].Position);

            Assert.AreEqual("DEMO", dataStructureObject.DimensionList.Dimensions[3].Id);
            Assert.AreEqual(1, dataStructureObject.DimensionList.Dimensions[3].Position);
        }

        /// <summary>
        /// Tests the freq with freq concept.
        /// </summary>
        [Test]
        public void TestFreqWithFreqConcept()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "FREQ");
            var immutable = BuildDataStructureObject(dimension);
            Assert.NotNull(immutable.FrequencyDimension); 
        }

        /// <summary>
        /// Tests the freq with freq identifier.
        /// </summary>
        [Test]
        public void TestFreqWithFreqId()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.Id = "FREQ";
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "SOMETHING_ELSE");
            var immutable = BuildDataStructureObject(dimension);
            Assert.NotNull(immutable.FrequencyDimension);
        }

        /// <summary>
        /// Tests the coded time dimension.
        /// </summary>
        [Test]
        public void TestCodedTimeDimension()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.TimeDimension = true;
            dimension.Id = DimensionObject.TimeDimensionFixedId;
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD");
            dimension.Representation = new RepresentationMutableCore() {Representation = new StructureReferenceImpl("TEST_AGENCY", "CL_TIME_PERIOD", "1.0", SdmxStructureEnumType.CodeList) };
            var immutable = BuildDataStructureObject(dimension);
            var timeDimension = immutable.TimeDimension;
            Assert.NotNull(timeDimension);
            Assert.IsTrue(timeDimension.HasCodedRepresentation());
            var structureReference = dimension.Representation.Representation;
            Assert.AreEqual(timeDimension.Representation.Representation.AgencyId, structureReference.AgencyId);
            Assert.AreEqual(timeDimension.Representation.Representation.MaintainableId, structureReference.MaintainableId);
            Assert.AreEqual(timeDimension.Representation.Representation.Version, structureReference.Version);
        }

        /// <summary>
        /// Tests the not coded time dimension.
        /// </summary>
        [Test]
        public void TestUnCodeTimeDimension()
        {
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.TimeDimension = true;
            dimension.Id = DimensionObject.TimeDimensionFixedId;
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TIME_PERIOD");
            dimension.Representation = new RepresentationMutableCore() { TextFormat = new TextFormatMutableCore() { TextType = TextType.GetFromEnum(TextEnumType.TimePeriod) } };
            var immutable = BuildDataStructureObject(dimension);
            Assert.NotNull(immutable.TimeDimension);
            Assert.IsFalse(immutable.TimeDimension.HasCodedRepresentation());
            Assert.NotNull(immutable.TimeDimension.Representation);
            Assert.NotNull(immutable.TimeDimension.Representation.TextFormat);
        }

        /// <summary>
        /// Builds the data structure object.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <returns>
        /// The <see cref="IDataStructureObject"/>
        /// </returns>
        private static IDataStructureObject BuildDataStructureObject(IDimensionMutableObject dimension)
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore() { Id = "TEST_DSD", AgencyId = "TEST", Version = "1.0" };
            dsd.AddName("en", "TEST_DSD");
            dsd.AddPrimaryMeasure(new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));
            dsd.AddDimension(dimension);

            var immutable = dsd.ImmutableInstance;
            return immutable;
        }
    }
}