﻿// -----------------------------------------------------------------------
// <copyright file="DataflowMutableSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;

namespace SdmxObjectsTests
{
    [TestFixture]
    class DataflowMutableSuperObjectTests
    {
        private IPrimaryMeasureSuperObject primaryMeasure;
        private ICodelistSuperObject codelist;
        private IConceptSuperObject concept;
        private IDataStructureSuperObject dataStructure;
        private List<IDimensionSuperObject> dimensions;
        private List<IAttributeSuperObject> attributes;
        private IDataflowObject dataflow;

        [SetUp]
        public virtual void SetUp()
        {
            codelist = new CodelistSuperObject(Utils.Codelist);
            primaryMeasure = new PrimaryMeasureSuperObject(Utils.DataStructure.PrimaryMeasure, null, new ConceptSuperObject(Utils.ConceptScheme.GetItemById(Utils.DataStructure.PrimaryMeasure.ConceptRef.FullId), null));
            concept = new ConceptSuperObject(Utils.ConceptScheme.Items.FirstOrDefault(), codelist);
            dimensions = new List<IDimensionSuperObject>(Utils.DataStructure.DimensionList.Dimensions.Select(x => new DimensionSuperObject(x, codelist, concept)));
            attributes = new List<IAttributeSuperObject>(Utils.DataStructure.Attributes.Select(x => new AttributeSuperObject(x, codelist, concept)));
            dataStructure = new DataStructureSuperObject(Utils.DataStructure, dimensions, attributes, primaryMeasure);
            dataflow = Utils.DataFlow;
        }

        public DataflowMutableSuperObject CreateSUT()
        {
            return new DataflowMutableSuperObject(new DataflowSuperObject(dataflow, dataStructure));
        }

        public class When_constructing_a_DataflowSuperObject : DataflowSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }
        }
    }
}