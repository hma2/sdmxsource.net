﻿// -----------------------------------------------------------------------
// <copyright file="CompositesTest.cs" company="EUROSTAT">
//   Date Created : 2017-11-22
//   Copyright (c) 2017 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace SdmxObjectsTests
{

    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    class CompositesTest
    {
        [TestCase("tests/v20/ESTAT+DEMOGRAPHY+2.1.xml")]
        [TestCase("tests/v21/ESTAT_STS_3.0.xml")]
        public void TestDsd(string file)
        {
            var sdmxObjects = new FileInfo(file).GetSdmxObjects(new StructureParsingManager());
            var dsd = sdmxObjects.DataStructures.First();

            var composites = dsd.IdentifiableComposites;
            var components = dsd.Components;
            var groups = dsd.Groups;

            var expectedList = new List<IIdentifiableObject>();
            expectedList.AddRange(groups);
            expectedList.AddRange(components);
            expectedList.Add(dsd.DimensionList);
            expectedList.Add(dsd.AttributeList);
            expectedList.Add(dsd.MeasureList);

            Assert.That(composites.Count, Is.EqualTo(expectedList.Count));
            Assert.That(composites, Is.EquivalentTo(expectedList));
        }

        [TestCase("tests/v20/CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME_annotations.xml")]
        [TestCase("tests/v21/Structure/test-sdmxv2.1-CATEGORY_SCHEME_ESTAT_DATAFLOWS_SCHEME.xml")]
        public void TestCategoryScheme(string file)
        {
            var sdmxObjects = new FileInfo(file).GetSdmxObjects(new StructureParsingManager());
            var catScheme = sdmxObjects.CategorySchemes.First();

            var composites = catScheme.IdentifiableComposites;

            var expectedList = new List<IIdentifiableObject>();
            var categories = new Stack<ICategoryObject>(catScheme.Items);
            while(categories.Count > 0)
            {
                var current = categories.Pop();
                expectedList.Add(current);
                foreach(var item in current.Items)
                {
                    categories.Push(item);
                }
            }

            Assert.That(composites.Count, Is.EqualTo(expectedList.Count));
            Assert.That(composites, Is.EquivalentTo(expectedList));
        }
    }
}
