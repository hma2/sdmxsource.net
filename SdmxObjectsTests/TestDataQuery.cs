﻿// -----------------------------------------------------------------------
// <copyright file="TestDataQuery.cs" company="EUROSTAT">
//   Date Created : 2014-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System.Collections.Generic;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture]
    public class TestDataQuery
    {
        [Test]
        public void TestDimensionAtObservation()
        {
            IDataStructureMutableObject dsd = new DataStructureMutableCore() { Id = "TEST", AgencyId = "TEST_AGENCY", Version = "1.0" };
            dsd.AddName("en", "Test name");
            IDimensionMutableObject dimension = new DimensionMutableCore();
            dimension.ConceptRef = new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "TEST_DIM");
            dimension.Representation = new RepresentationMutableCore { Representation = new StructureReferenceImpl("TEST_AGENCY", "CL_TEST", "2.0", SdmxStructureEnumType.CodeList) };

            dsd.AddDimension(dimension);
            dsd.AddPrimaryMeasure(new StructureReferenceImpl("TEST_AGENCY", "TEST_CONCEPTS", "1.0", SdmxStructureEnumType.Concept, "OBS_VALUE"));

            var immutableDsd = dsd.ImmutableInstance;
            var dataflowMutable = new DataflowMutableCore { Id = "TEST_DF", AgencyId = "TEST_AGENCY", Version = "1.2" };
            dataflowMutable.AddName("en", "Test");
            dataflowMutable.DataStructureRef = immutableDsd.AsReference;
            var dataflow = dataflowMutable.ImmutableInstance;
           
           IDataQuery query = new DataQueryImpl(immutableDsd, null, DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full), null, null, null, dataflow, "AllDimensions", new HashSet<IDataQuerySelection>(), null, null);
           Assert.AreEqual("AllDimensions", query.DimensionAtObservation);
        }
    }
}