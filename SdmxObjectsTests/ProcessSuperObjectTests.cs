// -----------------------------------------------------------------------
// <copyright file="ProcessSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using FluentAssertions;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Process;

namespace SdmxObjectsTests
{
    [TestFixture]
    internal class ProcessSuperObjectTests
    {
        [SetUp]
        public virtual void SetUp()
        {
            process = Utils.Process;
            processSteps =
                Utils.ProcessSteps.Select(x => new ProcessStepSuperObject(x, new List<IInputOutputSuperObject>()
                    , new List<IInputOutputSuperObject>(), x.Computation, x.Transitions.ToList(), null));
        }

        private IEnumerable<ProcessStepSuperObject> processSteps;
        private IProcessObject process;

        /// <summary>
        ///     Creates the sut.
        /// </summary>
        /// <returns></returns>
        public ProcessSuperObject CreateSUT()
        {
            return new ProcessSuperObject(process, new List<IProcessStepSuperObject>(processSteps));
        }

        public class When_constructing_a_ProcessSuperObject : ProcessSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }

            [Test]
            public void Should_have_the_same_process_steps_as_the_original_ICategoryObject()
            {
                var sut = CreateSUT();

                sut.ProcessSteps.Should().OnlyContain(x => processSteps.ToList().Exists(y => x.Id == y.Id));
            }
        }
    }
}