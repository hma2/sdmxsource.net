﻿// -----------------------------------------------------------------------
// <copyright file="TestMutableImmutable.cs" company="EUROSTAT">
//   Date Created : 2017-04-12
//   Copyright (c) 2012, 2017 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Util.Extension;

    [TestFixture]
    public class TestMutableImmutable
    {
        private IMaintainableObject[] _maintainable = GetSdmxObjects().ToArray();

        [Test, TestCaseSource("_maintainable")]
        public void TestMutableToImmutableRoundTrip(IMaintainableObject artefact)
        {
            var maintainableMutableObject = artefact.MutableInstance;
            var newImmutableInstance = maintainableMutableObject.ImmutableInstance;

            Assert.That(artefact, Is.EqualTo(newImmutableInstance));
            Assert.That(artefact.DeepEquals(newImmutableInstance, false));
        }

        private static IList<IMaintainableObject> GetSdmxObjects()
        {
            IStructureParsingManager parsingManager = new StructureParsingManager();
            DirectoryInfo testDir = new DirectoryInfo("tests/v21/Structure");
            if (!testDir.Exists)
            {
                Trace.WriteLine(string.Format("Test directory '{0}' not found ", testDir.FullName));
            }

            var files = testDir.GetFiles("*.xml");
            List<IMaintainableObject> maintainableObjects = new List<IMaintainableObject>();
            foreach (var file in files)
            {
                ISdmxObjects sdmxObjects;
                try
                {
                    sdmxObjects = file.GetSdmxObjects(parsingManager);
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.ToString());
                    continue;
                }
                
                maintainableObjects.AddRange(sdmxObjects.GetAllMaintainables());
            }

            return maintainableObjects;
        }
    }
}