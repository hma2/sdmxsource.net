﻿// -----------------------------------------------------------------------
// <copyright file="TestTextTypeWrapper.cs" company="EUROSTAT">
//   Date Created : 2016-10-24
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    /// Tests the <see cref="TextTypeWrapperImpl"/>
    /// </summary>
    public class TestTextTypeWrapper
    {
        /// <summary>
        /// Tests the locale.
        /// </summary>
        /// <param name="locale">The locale.</param>
        [TestCase("en")]
        [TestCase("en-GB")]
        [TestCase("en-gb")]
        [TestCase("en-US")]
        [TestCase("lo")]
        [TestCase("lo-la")]
        [TestCase("lo-LA")]
        [TestCase("LO-LA")]
        [TestCase("el")]
        [TestCase("el-GR")]
        [TestCase("el-gr")]
        [TestCase("de")]
        [TestCase("de-DE")]
        [TestCase("cs")]
        [TestCase("hr")]
        [TestCase("sr-Latn-RS")]
        [TestCase("sr-latn-rs")]
        [TestCase("sr-Cyrl")]
        [TestCase("sr-CYRL")]
        [TestCase("sr-cyrl")]
        [TestCase("la", IgnoreReason = "Works only on Windows 10 / Server 2016 or later.")]
        [TestCase("zz", ExpectedException = typeof(SdmxSemmanticException))]
        public void TestLocale(string locale)
        {
            var text = new TextTypeWrapperImpl(locale, "Test value", null);
            Assert.That(text.Locale, Is.EqualTo(locale));
        }
    }
}