﻿// -----------------------------------------------------------------------
// <copyright file="TestContactObject.cs" company="EUROSTAT">
//   Date Created : 2016-08-01
//   Copyright (c) 2012, 2016 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxObjectsTests.
// 
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace SdmxObjectsTests
{
    using System.Linq;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    [TestFixture]
    public class TestContactObject
    {
        [Test]
        public void TestRoleBuiltFromMutable()
        {
            var contact = new ContactMutableObjectCore() { Id = "ZZ9" };
            contact.Names.Add(new TextTypeWrapperMutableCore("en", "Test Name"));
            contact.AddName(new TextTypeWrapperMutableCore("fr", "Test FR NAme"));
            const string TestRole = "Test Role";
            contact.AddRole(new TextTypeWrapperMutableCore("en", TestRole));
            var immutable = new ContactCore(contact);
            Assert.AreEqual(1, immutable.Role.Count);
            Assert.AreEqual(TestRole, immutable.Role[0].Value);
        }
    }
}