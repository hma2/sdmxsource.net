﻿// -----------------------------------------------------------------------
// <copyright file="CodelistSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-22
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

using FluentAssertions;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;

namespace SdmxObjectsTests
{
    [TestFixture]
    public class CodelistSuperObjectTests
    {
        [SetUp]
        public virtual void SetUp()
        {
            codelist = Utils.Codelist;
        }

        private ICodelistObject codelist;

        public CodelistSuperObject CreateSUT()
        {
            return new CodelistSuperObject(codelist);
        }

        public class When_building_the_CodelistSuperObject : CodelistSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_get_a_code_by_id()
            {
                var sut = CreateSUT();
                sut.GetCodeByValue(codelist.Items.First().Id).Should().NotBeNull();
            }

            [Test]
            public void Should_not_throw_an_exception_for_invalid_code_id()
            {
                var sut = CreateSUT();
                sut.GetCodeByValue("DOES_NOT_EXIST").Should().BeNull();
            }

            [Test]
            public void Should_have_the_same_number_of_codes_without_parent_as_the_initial_codelist()
            {
                var sut = CreateSUT();
                sut.Codes.Should().HaveCount(codelist.Items.Count(c => c.ParentCode == null));
            }
        }
    }
}