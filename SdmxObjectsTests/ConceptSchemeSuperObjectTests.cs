﻿// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme;

namespace SdmxObjectsTests
{
    [TestFixture]
    public class ConceptSchemeSuperObjectTests
    {
        private IConceptSchemeObject conceptSchemeType;
        private IDictionary<IConceptObject, ICodelistSuperObject> representations;

        [SetUp]
        public virtual void SetUp()
        {
            conceptSchemeType = Utils.ConceptScheme;
            representations = new Dictionary<IConceptObject, ICodelistSuperObject>();
            foreach (var conceptObject in conceptSchemeType.Items)
            {
                representations.Add(conceptObject, new CodelistSuperObject(Utils.Codelist));
            }
        }

        public ConceptSchemeSuperObject CreateSUT()
        {
            return new ConceptSchemeSuperObject(conceptSchemeType, representations);
        }


        public class When_constructing_a_ConceptSchemeSuperObject : ConceptSchemeSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }
        }
    }
}