// -----------------------------------------------------------------------
// <copyright file="CodeRefMutableSuperObjectTests.cs" company="EUROSTAT">
//   Date Created : 2016-02-24
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjectsTests.
//     SdmxObjectsTests is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjectsTests is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjectsTests.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

using FluentAssertions;

using NUnit.Framework;

using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Codelist;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

namespace SdmxObjectsTests
{
    [TestFixture]
    public class CodeRefMutableSuperObjectTests
    {
        private IHierarchicalCodelistObject hcl;
        private IHierarchy hierarchyBean;
        private List<ICodelistObject> codelists;
        private IHierarchicalCode _originalCode;

        [SetUp]
        public virtual void SetUp()
        {
            hcl = Utils.HierarchicalCodelist;
            codelists = Utils.CodelistsForHierarchical;
            hierarchyBean = hcl.Hierarchies.FirstOrDefault();
            _originalCode = hierarchyBean.HierarchicalCodeObjects.FirstOrDefault();

        }

        public CodeRefMutableSuperObject CreateSUT()
        {
            
            return new CodeRefMutableSuperObject(new CodeRefSuperObject(hierarchyBean, _originalCode, codelists));
        }

        public class When_constructing_a_CodeRefSuperObject : CodeRefMutableSuperObjectTests
        {
            [Test]
            public void Should_be_able_to_construct_object()
            {
                CreateSUT();
            }

            [Test]
            public void Should_have_code_with_id_the_same_as_original_HierarchicalCode()
            {
                var sut = CreateSUT();
                sut.Code.Id.Should().Be(_originalCode.Id);
            }

            [Test]
            public void Should_have_codeRefs_the_same_as_original_HierarchicalCode()
            {
                var sut = CreateSUT();
                foreach (var item in _originalCode.CodeRefs)
                {
                    sut.CodeRefs.Should().Contain(x => x.Id == item.Id);
                }

            }
        }
    }
}