// -----------------------------------------------------------------------
// <copyright file="DataStructureFromCrossBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-02-07
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Util
{
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;

    /// <summary>
    ///     The data structure from cross builder.
    /// </summary>
    public class DataStructureFromCrossBuilder : IDataStructureFromCrossBuilder
    {
        /// <summary>
        ///     Builds an object of type <see cref="IDataStructureMutableObject" /> from the specified
        ///     <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     An <see cref="ICrossSectionalDataStructureMutableObject" /> object to build the output object from
        /// </param>
        /// <returns>
        ///     Object of type <see cref="IDataStructureMutableObject" />
        /// </returns>
        public IDataStructureMutableObject Build(ICrossSectionalDataStructureMutableObject buildFrom)
        {
            var imutableDsd = new DataStructureObjectCore(buildFrom);
            return new DataStructureMutableCore(imutableDsd);
        }
    }
}