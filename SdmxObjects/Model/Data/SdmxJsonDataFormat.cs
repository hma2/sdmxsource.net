﻿// -----------------------------------------------------------------------
// <copyright file="SdmxDataFormatCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// The SDMX json data format.
    /// </summary>
    public class SdmxJsonDataFormat : SdmxDataFormatCore
    {
        /// <summary>
        /// The encoding
        /// </summary>
        private readonly Encoding _encoding;

        /// <summary>
        /// The SDMX super object retrieval manager
        /// </summary>
        private readonly ISdmxSuperObjectRetrievalManager _sdmxSuperObjectRetrievalManager;

        /// <summary>
        /// The translator
        /// </summary>
        private readonly ITranslator _translator;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxJsonDataFormat"/> class. 
        /// Initializes a new instance of the <see cref="SdmxDataFormatCore"/> class.
        /// </summary>
        /// <param name="sdmxSuperObjectRetrievalManager">
        /// The sdmxSuperObjectRetrievalManager.
        /// </param>
        /// <param name="dataType">
        /// The data type.
        /// </param>
        /// <param name="encoding">
        /// The encoding.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        public SdmxJsonDataFormat(ISdmxSuperObjectRetrievalManager sdmxSuperObjectRetrievalManager, DataType dataType, Encoding encoding, ITranslator translator)
            : base(dataType)
        {
            if (sdmxSuperObjectRetrievalManager == null)
            {
                throw new ArgumentNullException("sdmxSuperObjectRetrievalManager");
            }

            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }

            if (translator == null)
            {
                throw new ArgumentNullException("translator");
            }

            this._sdmxSuperObjectRetrievalManager = sdmxSuperObjectRetrievalManager;
            this._encoding = encoding;
            this._translator = translator;
        }

        /// <summary>
        /// Gets the encoding.
        /// </summary>
        public Encoding Encoding
        {
            get
            {
                return this._encoding;
            }
        }

        /// <summary>
        /// Gets the SdmxSuperObjectRetrievalManager.
        /// </summary>
        public ISdmxSuperObjectRetrievalManager SdmxSuperObjectRetrievalManager
        {
            get
            {
                return this._sdmxSuperObjectRetrievalManager;
            }
        }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        public ITranslator Translator
        {
            get
            {
                return this._translator;
            }
        }
    }
}