﻿// -----------------------------------------------------------------------
// <copyright file="FooterMessageCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The footer message core
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.IFooterMessage" />
    public class FooterMessageCore : IFooterMessage
    {
        /// <summary>
        ///     The _code
        /// </summary>
        private readonly string _code;

        /// <summary>
        ///     The _footer text
        /// </summary>
        private readonly IList<ITextTypeWrapper> _footerText;

        /// <summary>
        ///     The _severity
        /// </summary>
        private readonly Severity _severity;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FooterMessageCore" /> class.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="textType">Type of the text.</param>
        /// <exception cref="ArgumentException">
        ///     FooterMessage - Code is mandatory
        ///     or
        ///     FooterMessage - At least on e text is required
        /// </exception>
        public FooterMessageCore(string code, Severity severity, ITextTypeWrapper textType)
        {
            this._code = code;
            this._severity = severity;
            if (code == null)
            {
                throw new ArgumentException("FooterMessage - Code is mandatory");
            }

            if (textType == null)
            {
                throw new ArgumentException("FooterMessage - At least on e text is required");
            }

            this._footerText = new List<ITextTypeWrapper>();
            this._footerText.Add(textType);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FooterMessageCore" /> class.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="textType">Type of the text.</param>
        /// <exception cref="ArgumentException">
        ///     FooterMessage - Code is mandatory
        ///     or
        ///     FooterMessage - At least on e text is required
        /// </exception>
        public FooterMessageCore(string code, Severity severity, IList<ITextTypeWrapper> textType)
        {
            this._code = code;
            this._severity = severity;
            this._footerText = textType;
            if (code == null)
            {
                throw new ArgumentException("FooterMessage - Code is mandatory");
            }

            if (textType == null || textType.Count == 0)
            {
                throw new ArgumentException("FooterMessage - At least on e text is required");
            }
        }

        /// <summary>
        ///     Gets the code. Mandatory Field - use to describe the error/warning
        /// </summary>
        public string Code
        {
            get
            {
                return this._code;
            }
        }

        /// <summary>
        ///     Gets the footer text. Any text associated with the footer, there must be at least one text message
        /// </summary>
        public IList<ITextTypeWrapper> FooterText
        {
            get
            {
                return this._footerText;
            }
        }

        /// <summary>
        ///     Gets the severity. Optional - describes severity of problem
        /// </summary>
        public Severity Severity
        {
            get
            {
                return this._severity;
            }
        }
    }
}