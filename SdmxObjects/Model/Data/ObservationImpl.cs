// -----------------------------------------------------------------------
// <copyright file="ObservationImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The observation implementation.
    /// </summary>
    public class ObservationImpl : IObservation
    {
        /// <summary>
        ///     The _series key
        /// </summary>
        private readonly IKeyable _seriesKey;

        /// <summary>
        ///     The _annotations.
        /// </summary>
        private readonly IList<IAnnotation> _annotations = new List<IAnnotation>();

        /// <summary>
        ///     The attribute map.
        /// </summary>
        private readonly IDictionary<string, IKeyValue> _attributeMap =
            new Dictionary<string, IKeyValue>(StringComparer.Ordinal);

        /// <summary>
        ///     The attributes.
        /// </summary>
        private readonly IList<IKeyValue> _attributes = new List<IKeyValue>();

        /// <summary>
        ///     The cross section value.
        /// </summary>
        private readonly IKeyValue _crossSectionValue;

        /// <summary>
        ///     The is cross section.
        /// </summary>
        private readonly bool _isCrossSection;

        /// <summary>
        ///     The obs time.
        /// </summary>
        private readonly string _obsTime;

        /// <summary>
        ///     The obs value.
        /// </summary>
        private readonly string _obsValue;

        /// <summary>
        ///     The date.
        /// </summary>
        private DateTime? _date;

        /// <summary>
        ///     The time format.
        /// </summary>
        private TimeFormat _timeFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ObservationImpl" /> class.
        /// </summary>
        /// <param name="seriesKey">The series key.</param>
        /// <param name="obsTime">The obs time 0.</param>
        /// <param name="obsValue">The obs value 1.</param>
        /// <param name="attributes">The attributes 2.</param>
        /// <param name="crossSectionValue">The cross section value 3.</param>
        /// <param name="annotations">The annotations.</param>
        /// <exception cref="ArgumentException">Series Key can not be null</exception>
        public ObservationImpl(
            IKeyable seriesKey, 
            string obsTime, 
            string obsValue, 
            IList<IKeyValue> attributes, 
            IKeyValue crossSectionValue, 
            params IAnnotation[] annotations)
        {
            this._seriesKey = seriesKey;
            this._obsValue = obsValue;
            this._obsTime = obsTime;

            if (seriesKey == null)
            {
                throw new ArgumentException("Series Key can not be null");
            }

            if (attributes != null)
            {
                this._attributes = new List<IKeyValue>(attributes);

                foreach (IKeyValue currentKv in attributes)
                {
                    this._attributeMap.Add(currentKv.Concept, currentKv);
                }
            }

            if (annotations != null)
            {
                foreach (IAnnotation currentAnnotation in annotations)
                {
                    this._annotations.Add(currentAnnotation);
                }
            }

            this._crossSectionValue = crossSectionValue;
            this._isCrossSection = crossSectionValue != null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ObservationImpl" /> class.
        /// </summary>
        /// <param name="seriesKey">The series key.</param>
        /// <param name="obsTime">The obs time 0.</param>
        /// <param name="obsValue">The obs value 1.</param>
        /// <param name="attributes">The attributes 2.</param>
        /// <param name="annotations">The annotations.</param>
        /// <exception cref="ArgumentException">
        ///     Series Key can not be null
        ///     or
        ///     Observation for Key ' + seriesKey + ' does not specify the observation time
        ///     or
        ///     Observation for Key ' + seriesKey + ' does not specify the observation concept:
        ///     + seriesKey.CrossSectionConcept
        /// </exception>
        public ObservationImpl(
            IKeyable seriesKey, 
            string obsTime, 
            string obsValue, 
            IList<IKeyValue> attributes, 
            params IAnnotation[] annotations)
        {
            this._obsValue = obsValue;
            this._seriesKey = seriesKey;

            if (seriesKey == null)
            {
                throw new ArgumentException("Series Key can not be null");
            }

            if (!ObjectUtil.ValidString(obsTime))
            {
                if (seriesKey.TimeSeries)
                {
                    throw new ArgumentException(
                        "Observation for Key '" + seriesKey + "' does not specify the observation time");
                }

                throw new ArgumentException(
                    "Observation for Key '" + seriesKey + "' does not specify the observation concept: "
                    + seriesKey.CrossSectionConcept);
            }

            this._obsTime = obsTime;
            if (attributes != null)
            {
                this._attributes = new List<IKeyValue>(attributes);
                foreach (IKeyValue currentKv in attributes)
                {
                    this._attributeMap.Add(currentKv.Concept, currentKv);
                }
            }

            if (annotations != null)
            {
                foreach (IAnnotation currentAnnotation in annotations)
                {
                    this._annotations.Add(currentAnnotation);
                }
            }

            this._isCrossSection = false;
        }

        /// <summary>
        ///     Gets a value indicating annotations.
        /// </summary>
        public virtual IList<IAnnotation> Annotations
        {
            get
            {
                return new List<IAnnotation>(this._annotations);
            }
        }

        /// <summary>
        ///     Gets the attributes.
        /// </summary>
        public virtual IList<IKeyValue> Attributes
        {
            get
            {
                return new List<IKeyValue>(this._attributes);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether cross section.
        /// </summary>
        public virtual bool CrossSection
        {
            get
            {
                return this._isCrossSection;
            }
        }

        /// <summary>
        ///     Gets the cross sectional value.
        /// </summary>
        public virtual IKeyValue CrossSectionalValue
        {
            get
            {
                return this._crossSectionValue;
            }
        }

        /// <summary>
        ///     Gets the obs as time date.
        /// </summary>
        public virtual DateTime? ObsAsTimeDate
        {
            get
            {
                if (this._obsTime == null)
                {
                    return null;
                }

                return this._date ?? (this._date = DateUtil.FormatDate(this._obsTime, true)); // TODO: Copy paste
            }
        }

        /// <summary>
        ///     Gets the observation value.
        /// </summary>
        public virtual string ObservationValue
        {
            get
            {
                return this._obsValue;
            }
        }

        /// <summary>
        ///     Gets the obs time.
        /// </summary>
        public virtual string ObsTime
        {
            get
            {
                return this._obsTime;
            }
        }

        /// <summary>
        ///     Gets the obs time format.
        /// </summary>
        public virtual TimeFormat ObsTimeFormat
        {
            get
            {
                if (this._obsTime == null)
                {
                    return default(TimeFormat) /* was: null */;
                }

                return this._timeFormat ?? (this._timeFormat = DateUtil.GetTimeFormatOfDate(this._obsTime));
            }
        }

        /// <summary>
        ///     Gets the parent series key for this observation.  The returned object can not be null.
        /// </summary>
        public virtual IKeyable SeriesKey
        {
            get
            {
                return this._seriesKey;
            }
        }

        /// <summary>
        ///     The compare to.
        /// </summary>
        /// <param name="other">
        ///     The other.
        /// </param>
        /// <returns>
        ///     The <see cref="int" /> .
        /// </returns>
        public virtual int CompareTo(IObservation other)
        {
            if (other == null)
            {
                return 1;
            }

            if (this._obsTime == null)
            {
                if (other.ObsTime == null)
                {
                    return 0;
                }

                return -1;
            }

            if (other.ObsTime == null)
            {
                return 1;
            }

            if (this._obsTime.Length == other.ObsTime.Length)
            {
                return string.CompareOrdinal(this._obsTime, other.ObsTime);
            }

            DateTime? obsAsTimeDate = this.ObsAsTimeDate;
            if (obsAsTimeDate != null)
            {
                return obsAsTimeDate.Value.CompareTo(other.ObsAsTimeDate);
            }

            return -1;
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this))
            {
                return true;
            }

            if (obj == null)
            {
                return false;
            }

            return obj.ToString().Equals(this.ToString());
        }

        /// <summary>
        ///     The get attribute.
        /// </summary>
        /// <param name="concept">
        ///     The concept.
        /// </param>
        /// <returns>
        ///     The <see cref="IKeyValue" /> .
        /// </returns>
        public virtual IKeyValue GetAttribute(string concept)
        {
            IKeyValue ret;

            if (this._attributeMap.TryGetValue(concept, out ret))
            {
                return ret;
            }

            return null;
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            string concat = string.Empty;

            foreach (IKeyValue kv in this._attributes)
            {
                sb.Append(concat + kv.Concept + ":" + kv.Code);
                concat = ",";
            }

            if (this._isCrossSection)
            {
                return "Obs " + this._crossSectionValue.Concept + ":" + this._crossSectionValue.Code + " = "
                       + this._obsValue + " - Attributes : " + sb;
            }

            return "Obs " + this._obsTime + " = " + this._obsValue + " - Attributes : " + sb;
        }
    }
}