﻿// -----------------------------------------------------------------------
// <copyright file="SdmxCsvDataFormat.cs" company="EUROSTAT">
//   Date Created : 2018-01-09
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Translator;

    /// <summary>
    /// The SDMX json data format.
    /// </summary>
    public class SdmxCsvDataFormat : SdmxDataFormatCore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxJsonDataFormat"/> class. 
        /// </summary>
        /// <param name="csvOptions"></param>
        /// <param name="dataType"></param>
        public SdmxCsvDataFormat(ISdmxSuperObjectRetrievalManager sdmxSuperObjectRetrievalManager, SdmxCsvOptions csvOptions, ITranslator translator, DataType dataType) : base(dataType)
        {
            if (sdmxSuperObjectRetrievalManager == null)
            {
                throw new ArgumentNullException("sdmxSuperObjectRetrievalManager");
            }

            if (csvOptions == null)
            {
                throw new ArgumentNullException("csvOptions");
            }

            if (translator == null)
            {
                throw new ArgumentNullException("translator");
            }

            this.SdmxSuperObjectRetrievalManager = sdmxSuperObjectRetrievalManager;
            this.Options = csvOptions;
            this.Translator = translator;
        }

        /// <summary>
        /// Gets the SdmxSuperObjectRetrievalManager.
        /// </summary>
        public SdmxCsvOptions Options { get; private set; }

        /// <summary>
        /// Gets the SdmxSuperObjectRetrievalManager.
        /// </summary>
        public ISdmxSuperObjectRetrievalManager SdmxSuperObjectRetrievalManager { get; private set; }

        /// <summary>
        /// Gets the translator.
        /// </summary>
        public ITranslator Translator { get; private set; }
    }
}