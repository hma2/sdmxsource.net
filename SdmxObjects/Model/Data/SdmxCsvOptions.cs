﻿namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System.Text;

    /// <summary>
    /// Class for defining Sdmx CSV options
    /// </summary>
    public class SdmxCsvOptions
    {
        /// <summary>
        /// Use labels in headers, in dimension & attribute columns
        /// </summary>
        public bool UseLabels { get; set; }

        /// <summary>
        /// Use normalized time
        /// </summary>
        public bool UseNormalizedTime { get; set; }

        /// <summary>
        /// Csv column delimiter
        /// </summary>
        public char Delimiter { get; private set; }

        /// <summary>
        /// Csv encoding
        /// </summary>
        public Encoding Encoding { get; private set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="delimiter"></param>
        /// <param name="encoding"></param>
        public SdmxCsvOptions(char delimiter = ',', Encoding encoding = null)
        {
            this.Delimiter = delimiter;
            this.Encoding = encoding ?? Encoding.UTF8;
        }
    }
}
