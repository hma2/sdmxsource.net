// -----------------------------------------------------------------------
// <copyright file="KeyValueImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    ///     The key value impl.
    /// </summary>
    [Serializable]
    public class KeyValueImpl : IKeyValue
    {
        /// <summary>
        ///     The _code.
        /// </summary>
        private readonly string _code;

        /// <summary>
        ///     The _concept.
        /// </summary>
        private readonly string _concept;

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        public KeyValueImpl()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValueImpl" /> class.
        /// </summary>
        /// <param name="code0">
        ///     The code 0.
        /// </param>
        /// <param name="concept1">
        ///     The concept 1.
        /// </param>
        public KeyValueImpl(string code0, string concept1)
        {
            this._code = code0;
            this._concept = concept1;
        }

        /// <summary>
        ///     Gets the code.
        /// </summary>
        public virtual string Code
        {
            get
            {
                return this._code;
            }
        }

        /// <summary>
        ///     Gets the concept.
        /// </summary>
        public virtual string Concept
        {
            get
            {
                return this._concept;
            }
        }

        /// <summary>
        ///     The compare to.
        /// </summary>
        /// <param name="other">
        ///     Key value to compare to
        /// </param>
        /// <returns>
        ///     The <see cref="int" /> .
        /// </returns>
        public virtual int CompareTo(IKeyValue other)
        {
            if (other == null)
            {
                return 1;
            }

            if (this._concept.Equals(other.Concept))
            {
                return string.CompareOrdinal(this._code, other.Code);
            }

            return string.CompareOrdinal(this._concept, other.Concept);
        }

        /// <summary>
        ///     The equals.
        /// </summary>
        /// <param name="obj">
        ///     The obj.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool Equals(object obj)
        {
            var that = obj as IKeyValue;
            if (that != null)
            {
                return this.Concept.Equals(that.Concept) && this.Code.Equals(that.Code);
            }

            return base.Equals(obj);
        }

        /// <summary>
        ///     The get hash code.
        /// </summary>
        /// <returns> The <see cref="int" /> . </returns>
        public override int GetHashCode()
        {
            return (this._concept + this._code).GetHashCode();
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            return this._concept + ":" + this._code;
        }
    }
}