﻿// -----------------------------------------------------------------------
// <copyright file="BaseDataQuery.cs" company="EUROSTAT">
//   Date Created : 2013-06-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     The base data query class
    /// </summary>
    public abstract class BaseDataQuery
    {
        /// <summary>
        ///     The _dataflow
        /// </summary>
        private IDataflowObject _dataflow;

        /// <summary>
        ///     The _data structure
        /// </summary>
        private IDataStructureObject _dataStructure;

        /// <summary>
        ///     The _dimension at observation
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        ///     The _first n obs
        /// </summary>
        private int? _firstNObs;

        /// <summary>
        ///     The _last n obs
        /// </summary>
        private int? _lastNObs;

        /// <summary>
        ///     Gets or sets the dataflow.
        /// </summary>
        /// <value>
        ///     The dataflow.
        /// </value>
        public IDataflowObject Dataflow
        {
            get
            {
                return this._dataflow;
            }

            set
            {
                this._dataflow = value;
            }
        }

        /// <summary>
        ///     Gets or sets the data structure.
        /// </summary>
        /// <value>
        ///     The data structure.
        /// </value>
        public IDataStructureObject DataStructure
        {
            get
            {
                return this._dataStructure;
            }

            set
            {
                this._dataStructure = value;
            }
        }

        /// <summary>
        ///     Gets or sets the dimension at observation.
        /// </summary>
        /// <value>
        ///     The dimension at observation.
        /// </value>
        public string DimensionAtObservation
        {
            get
            {
                return this._dimensionAtObservation;
            }

            protected set
            {
                this._dimensionAtObservation = value;
            }
        }

        /// <summary>
        ///     Gets or sets the first n observations.
        /// </summary>
        /// <value>
        ///     The first n observations.
        /// </value>
        public int? FirstNObservations
        {
            get
            {
                return this._firstNObs;
            }

            set
            {
                this._firstNObs = value;
            }
        }

        /// <summary>
        ///     Gets or sets the last n observations.
        /// </summary>
        /// <value>
        ///     The last n observations.
        /// </value>
        public int? LastNObservations
        {
            get
            {
                return this._lastNObs;
            }

            set
            {
                this._lastNObs = value;
            }
        }

        /// <summary>
        ///     Gets a set of strings which represent the component ids that are being queried on
        /// </summary>
        /// <returns>The set of components</returns>
        protected abstract ISet<string> GetQueryComponentIds();

        /// <summary>
        ///     Validates the query.
        /// </summary>
        /// <exception cref="ArgumentException">
        ///     Can not create DataQuery, Dataflow is required
        ///     or
        ///     Can not create DataQuery, DataStructure is required
        /// </exception>
        protected void ValidateQuery()
        {
            if (this._dataflow == null)
            {
                throw new ArgumentException("Can not create DataQuery, Dataflow is required");
            }

            if (this._dataStructure == null)
            {
                throw new ArgumentException("Can not create DataQuery, DataStructure is required");
            }

            this.ValidateQueryComponents();
            this.ValidateDimensionAtObservation();
        }

        /// <summary>
        ///     Validates the dimension at observation.
        ///     If no dimension at observation is set, then the following rules apply in the order specified:
        ///     <li>Set to Time Dimension (if it exists)</li>
        ///     <li>Set to the first Measure Dimension (if one exists)</li>
        ///     <li>Set to AllDimensions</li>
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        ///     Can not create DataQuery, The dimension at observation ' + _dimensionAtObservation
        ///     + ' is not included in the Dimension list of the DSD.  Allowed values are  + sb
        /// </exception>
        private void ValidateDimensionAtObservation()
        {
            if (this._dimensionAtObservation == null)
            {
                if (this._dataStructure.TimeDimension != null)
                {
                    this._dimensionAtObservation = this._dataStructure.TimeDimension.Id;
                }
                else if (this._dataStructure.GetDimensions(SdmxStructureEnumType.MeasureDimension).Count > 0)
                {
                    this._dimensionAtObservation =
                        this._dataStructure.GetDimensions(SdmxStructureEnumType.MeasureDimension)[0].Id;
                }
                else
                {
                    this._dimensionAtObservation = "AllDimensions";
                }
            }
            else if (!this._dimensionAtObservation.Equals("AllDimensions"))
            {
                IDimension dimension = this._dataStructure.GetDimension(this._dimensionAtObservation);
                if (dimension == null)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (IDimension dim in
                        this._dataStructure.GetDimensions(
                            SdmxStructureEnumType.Dimension, 
                            SdmxStructureEnumType.MeasureDimension, 
                            SdmxStructureEnumType.TimeDimension))
                    {
                        sb.Append(dim.Id + "\r\n");
                    }

                    // Changed from ArgumentException in order for Web Service to produce the correct error (semantic).
                    // This is different than in SdmxSource Java where it throws the equivalent of ArgumentException. 
                    throw new SdmxSemmanticException(
                        "Can not create DataQuery, The dimension at observation '" + this._dimensionAtObservation
                        + "' is not included in the Dimension list of the DSD.  Allowed values are " + sb);
                }
            }
        }

        /// <summary>
        ///     Validates the query components (e.g. dimension/attributes) exist on the data structure
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        ///     Data Structure ' + _dataStructure.Urn + ' does not contain component with id:
        ///     + currentComponetId
        /// </exception>
        private void ValidateQueryComponents()
        {
            foreach (string currentComponetId in this.GetQueryComponentIds())
            {
                if (this._dataStructure.GetComponent(currentComponetId) == null)
                {
                    throw new SdmxSemmanticException(
                        "Data Structure '" + this._dataStructure.Urn + "' does not contain component with id: "
                        + currentComponetId);
                }
            }
        }
    }
}