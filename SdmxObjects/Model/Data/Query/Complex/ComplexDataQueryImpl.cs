﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQueryImpl.cs" company="EUROSTAT">
//   Date Created : 2013-06-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.Complex
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using ITimeRange = Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex.ITimeRange;

    /// <summary>
    ///     The complex data query class
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.BaseDataQuery" />
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex.IComplexDataQuery" />
    public class ComplexDataQueryImpl : BaseDataQuery, IComplexDataQuery
    {
        /// <summary>
        ///     The _all dimensions
        /// </summary>
        private static readonly string _allDimensions;

        /// <summary>
        ///     The _complex data query selection groups
        /// </summary>
        private readonly IList<IComplexDataQuerySelectionGroup> _complexDataQuerySelectionGroups =
            new List<IComplexDataQuerySelectionGroup>();

        /// <summary>
        ///     The _data providers
        /// </summary>
        private readonly ISet<IDataProvider> _dataProviders = new HashSet<IDataProvider>();

        /// <summary>
        ///     The _dataset identifier
        /// </summary>
        private readonly string _datasetId;

        /// <summary>
        ///     The _dataset identifier operator
        /// </summary>
        private readonly TextSearch _datasetIdOperator;

        /// <summary>
        ///     The _default limit
        /// </summary>
        private readonly int? _defaultLimit;

        /// <summary>
        ///     The _has explicit measures
        /// </summary>
        private readonly bool _hasExplicitMeasures;

        /// <summary>
        ///     The _last updated date
        /// </summary>
        private readonly IList<ITimeRange> _lastUpdatedDate = new List<ITimeRange>();

        /// <summary>
        ///     The _obs action
        /// </summary>
        private readonly ObservationAction _obsAction;

        /// <summary>
        ///     The _provision agreement
        /// </summary>
        private readonly IProvisionAgreementObject _provisionAgreement;

        /// <summary>
        ///     The _query detail
        /// </summary>
        private readonly DataQueryDetail _queryDetail;

        /// <summary>
        ///     Initializes static members of the <see cref="ComplexDataQueryImpl" /> class.
        /// </summary>
        static ComplexDataQueryImpl()
        {
            _allDimensions = Api.Constants.DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.All).Value;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQueryImpl" /> class.
        /// </summary>
        /// <param name="datasetId">The dataset identifier.</param>
        /// <param name="datasetIdOperator">The dataset identifier operator.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataFlow">The data flow.</param>
        /// <param name="provisionAgreement">The provision agreement.</param>
        /// <param name="lastUpdatedDate">The last updated date.</param>
        /// <param name="maxObs">The maximum obs.</param>
        /// <param name="defaultLimit">The default limit.</param>
        /// <param name="orderAsc">if set to <c>true</c> [order asc].</param>
        /// <param name="obsAction">The obs action.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="hasExplicitMeasures">if set to <c>true</c> [has explicit measures].</param>
        /// <param name="queryDetail">The query detail.</param>
        /// <param name="complexDataQuerySelectionGroup">The complex data query selection group.</param>
        public ComplexDataQueryImpl(
            string datasetId, 
            TextSearch datasetIdOperator, 
            ISet<IDataProvider> dataProviders, 
            IDataStructureObject dataStructure, 
            IDataflowObject dataFlow, 
            IProvisionAgreementObject provisionAgreement, 
            IList<ITimeRange> lastUpdatedDate, 
            int? maxObs, 
            int? defaultLimit, 
            bool orderAsc, 
            ObservationAction obsAction, 
            string dimensionAtObservation, 
            bool hasExplicitMeasures, 
            DataQueryDetail queryDetail, 
            ICollection<IComplexDataQuerySelectionGroup> complexDataQuerySelectionGroup)
        {
            this._datasetId = datasetId;

            if (datasetIdOperator != null)
            {
                this._datasetIdOperator = datasetIdOperator;
            }
            else
            {
                this._datasetIdOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.DataStructure = dataStructure;
            this.Dataflow = dataFlow;
            this._provisionAgreement = provisionAgreement;

            if (lastUpdatedDate != null)
            {
                this._lastUpdatedDate = new List<ITimeRange>(lastUpdatedDate);
            }

            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            this._defaultLimit = defaultLimit;

            if (obsAction != null)
            {
                this._obsAction = obsAction;
            }
            else
            {
                this._obsAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            }

            this.DimensionAtObservation = dimensionAtObservation;

            if (dimensionAtObservation != null)
            {
                // the values: 'AllDimensions' and 'TIME_PERIOD' are valid values.
                if (dimensionAtObservation.Equals(_allDimensions)
                    || dimensionAtObservation.Equals(DimensionAtObservationEnumType.Time.ToString()))
                {
                    this.DimensionAtObservation = dimensionAtObservation;
                }
                else
                {
                    // check if the value is a dimension Value
                    this.CheckDimensionExistence(dimensionAtObservation, dataStructure);
                }
            }
            else
            {
                this.DimensionAtObservation = this.GetDimensionAtObservationLevel(dataStructure);
            }

            this._hasExplicitMeasures = hasExplicitMeasures;

            if (queryDetail != null)
            {
                this._queryDetail = queryDetail;
            }
            else
            {
                this._queryDetail = DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            }

            if (complexDataQuerySelectionGroup != null)
            {
                foreach (IComplexDataQuerySelectionGroup cdqsg in complexDataQuerySelectionGroup)
                {
                    if (cdqsg != null)
                    {
                        this._complexDataQuerySelectionGroups.Add(cdqsg);
                    }
                }
            }

            // Perform validation
            this.ValidateQuery();
            this.ValidateProvisionAgreement();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQueryImpl" /> class.
        /// </summary>
        /// <param name="datasetId">The dataset identifier.</param>
        /// <param name="datasetIdOperator">The dataset identifier operator.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataFlow">The data flow.</param>
        /// <param name="provisionAgreement">The provision agreement.</param>
        /// <param name="lastUpdatedDate">The last updated date.</param>
        /// <param name="firstNObs">The first n obs.</param>
        /// <param name="lastNObs">The last n obs.</param>
        /// <param name="defaultLimit">The default limit.</param>
        /// <param name="obsAction">The obs action.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="hasExplicitMeasures">if set to <c>true</c> [has explicit measures].</param>
        /// <param name="queryDetail">The query detail.</param>
        /// <param name="complexDataQuerySelectionGroup">The complex data query selection group.</param>
        public ComplexDataQueryImpl(
            string datasetId, 
            TextSearch datasetIdOperator, 
            ISet<IDataProvider> dataProviders, 
            IDataStructureObject dataStructure, 
            IDataflowObject dataFlow, 
            IProvisionAgreementObject provisionAgreement, 
            IList<ITimeRange> lastUpdatedDate, 
            int? firstNObs, 
            int? lastNObs, 
            int? defaultLimit, 
            ObservationAction obsAction, 
            string dimensionAtObservation, 
            bool hasExplicitMeasures, 
            DataQueryDetail queryDetail, 
            ICollection<IComplexDataQuerySelectionGroup> complexDataQuerySelectionGroup)
        {
            this._datasetId = datasetId;
            if (datasetIdOperator != null)
            {
                this._datasetIdOperator = datasetIdOperator;
            }
            else
            {
                this._datasetIdOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            if (lastUpdatedDate != null)
            {
                this._lastUpdatedDate = new List<ITimeRange>(lastUpdatedDate);
            }

            this.DimensionAtObservation = dimensionAtObservation;
            this.DataStructure = dataStructure;
            this.Dataflow = dataFlow;
            this._provisionAgreement = provisionAgreement;
            this.FirstNObservations = firstNObs;
            this.LastNObservations = lastNObs;
            this._defaultLimit = defaultLimit;

            if (obsAction != null)
            {
                this._obsAction = obsAction;
            }
            else
            {
                this._obsAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            }

            if (dimensionAtObservation != null)
            {
                // the values: 'AllDimensions' and 'TIME_PERIOD' are valid values.
                if (dimensionAtObservation.Equals(_allDimensions)
                    || dimensionAtObservation.Equals(DimensionAtObservationEnumType.Time.ToString()))
                {
                    this.DimensionAtObservation = dimensionAtObservation;
                }
                else
                {
                    // check if the value is a dimension Value
                    this.CheckDimensionExistence(dimensionAtObservation, dataStructure);
                }
            }
            else
            {
                this.DimensionAtObservation = this.GetDimensionAtObservationLevel(dataStructure);
            }

            this._hasExplicitMeasures = hasExplicitMeasures;

            if (queryDetail != null)
            {
                this._queryDetail = queryDetail;
            }
            else
            {
                this._queryDetail = DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            }

            if (complexDataQuerySelectionGroup != null)
            {
                foreach (IComplexDataQuerySelectionGroup cdqsg in complexDataQuerySelectionGroup)
                {
                    if (cdqsg != null)
                    {
                        this._complexDataQuerySelectionGroups.Add(cdqsg);
                    }
                }
            }

            // perform validation
            this.ValidateQuery();
            this.ValidateProvisionAgreement();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQueryImpl" /> class.
        /// </summary>
        /// <param name="datasetId">The dataset identifier.</param>
        /// <param name="datasetIdOperator">The dataset identifier operator.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataFlow">The data flow.</param>
        /// <param name="provisionAgreement">The provision agreement.</param>
        /// <param name="lastUpdatedDate">The last updated date.</param>
        /// <param name="maxObs">The maximum obs.</param>
        /// <param name="orderAsc">if set to <c>true</c> [order asc].</param>
        /// <param name="defaultLimit">The default limit.</param>
        /// <param name="obsAction">The obs action.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="hasExplicitMeasures">if set to <c>true</c> [has explicit measures].</param>
        /// <param name="queryDetail">The query detail.</param>
        /// <param name="complexDataQuerySelectionGroup">The complex data query selection group.</param>
        public ComplexDataQueryImpl(
            string datasetId, 
            TextSearch datasetIdOperator, 
            ISet<IDataProvider> dataProviders, 
            IDataStructureObject dataStructure, 
            IDataflowObject dataFlow, 
            IProvisionAgreementObject provisionAgreement, 
            IList<ITimeRange> lastUpdatedDate, 
            int? maxObs, 
            bool orderAsc, 
            int? defaultLimit, 
            ObservationAction obsAction, 
            string dimensionAtObservation, 
            bool hasExplicitMeasures, 
            DataQueryDetail queryDetail, 
            IList<IComplexDataQuerySelectionGroup> complexDataQuerySelectionGroup)
        {
            this._datasetId = datasetId;
            if (datasetIdOperator != null)
            {
                this._datasetIdOperator = datasetIdOperator;
            }
            else
            {
                this._datasetIdOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            if (lastUpdatedDate != null)
            {
                this._lastUpdatedDate = new List<ITimeRange>(lastUpdatedDate);
            }

            this.DimensionAtObservation = dimensionAtObservation;
            this.DataStructure = dataStructure;
            this.Dataflow = dataFlow;
            this._provisionAgreement = provisionAgreement;
            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            this._defaultLimit = defaultLimit;

            if (obsAction != null)
            {
                this._obsAction = obsAction;
            }
            else
            {
                this._obsAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            }

            if (dimensionAtObservation != null)
            {
                // the values: 'AllDimensions' and 'TIME_PERIOD' are valid values.
                if (dimensionAtObservation.Equals(_allDimensions)
                    || dimensionAtObservation.Equals(DimensionAtObservationEnumType.Time.ToString()))
                {
                    this.DimensionAtObservation = dimensionAtObservation;
                }
                else
                {
                    // check if the value is a dimension Value
                    this.CheckDimensionExistence(dimensionAtObservation, dataStructure);
                }
            }
            else
            {
                this.DimensionAtObservation = this.GetDimensionAtObservationLevel(dataStructure);
            }

            this._hasExplicitMeasures = hasExplicitMeasures;

            if (queryDetail != null)
            {
                this._queryDetail = queryDetail;
            }
            else
            {
                this._queryDetail = DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            }

            if (complexDataQuerySelectionGroup != null)
            {
                foreach (IComplexDataQuerySelectionGroup cdqsg in complexDataQuerySelectionGroup)
                {
                    if (cdqsg != null)
                    {
                        this._complexDataQuerySelectionGroups.Add(cdqsg);
                    }
                }
            }

            // perform validation
            this.ValidateQuery();
            this.ValidateProvisionAgreement();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQueryImpl" /> class.
        /// </summary>
        /// <param name="datasetId">The dataset identifier.</param>
        /// <param name="datasetIdOperator">The dataset identifier operator.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataFlow">The data flow.</param>
        /// <param name="provisionAgreement">The provision agreement.</param>
        /// <param name="lastUpdatedDate">The last updated date.</param>
        /// <param name="maxObs">The maximum obs.</param>
        /// <param name="orderAsc">if set to <c>true</c> [order asc].</param>
        /// <param name="defaultLimit">The default limit.</param>
        /// <param name="obsAction">The obs action.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="hasExplicitMeasures">if set to <c>true</c> [has explicit measures].</param>
        /// <param name="queryDetail">The query detail.</param>
        /// <param name="complexSelections">The complex selections.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateFromOperator">The date from operator.</param>
        /// <param name="dateTo">The date to.</param>
        /// <param name="dateToOperator">The date to operator.</param>
        /// <param name="primaryMeasureValues">The primary measure values.</param>
        public ComplexDataQueryImpl(
            string datasetId, 
            TextSearch datasetIdOperator, 
            ISet<IDataProvider> dataProviders, 
            IDataStructureObject dataStructure, 
            IDataflowObject dataFlow, 
            IProvisionAgreementObject provisionAgreement, 
            IList<ITimeRange> lastUpdatedDate, 
            int? maxObs, 
            bool orderAsc, 
            int? defaultLimit, 
            ObservationAction obsAction, 
            string dimensionAtObservation, 
            bool hasExplicitMeasures, 
            DataQueryDetail queryDetail, 
            ISet<IComplexDataQuerySelection> complexSelections, 
            DateTime dateFrom, 
            OrderedOperator dateFromOperator, 
            DateTime dateTo, 
            OrderedOperator dateToOperator, 
            ISet<IComplexComponentValue> primaryMeasureValues)
        {
            this._datasetId = datasetId;
            if (datasetIdOperator != null)
            {
                this._datasetIdOperator = datasetIdOperator;
            }
            else
            {
                this._datasetIdOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            if (lastUpdatedDate != null)
            {
                this._lastUpdatedDate = new List<ITimeRange>(lastUpdatedDate);
            }

            this.DimensionAtObservation = dimensionAtObservation;
            this.DataStructure = dataStructure;
            this.Dataflow = dataFlow;
            this._provisionAgreement = provisionAgreement;

            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            this._defaultLimit = defaultLimit;

            if (obsAction != null)
            {
                this._obsAction = obsAction;
            }
            else
            {
                this._obsAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            }

            if (dimensionAtObservation != null)
            {
                // the values: 'AllDimensions' and 'TIME_PERIOD' are valid values.
                if (dimensionAtObservation.Equals(_allDimensions)
                    || dimensionAtObservation.Equals(DimensionAtObservationEnumType.Time.ToString()))
                {
                    this.DimensionAtObservation = dimensionAtObservation;
                }
                else
                {
                    // check if the value is a dimension Value
                    this.CheckDimensionExistence(dimensionAtObservation, dataStructure);
                }
            }
            else
            {
                this.DimensionAtObservation = this.GetDimensionAtObservationLevel(dataStructure);
            }

            this._hasExplicitMeasures = hasExplicitMeasures;
            if (queryDetail != null)
            {
                this._queryDetail = queryDetail;
            }
            else
            {
                this._queryDetail = DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            }

            if (ObjectUtil.ValidCollection(complexSelections) || dateFrom != null || dateTo != null)
            {
                ISdmxDate sdmxDateFrom = null;
                if (dateFrom != null)
                {
                    sdmxDateFrom = new SdmxDateCore(dateFrom, TimeFormatEnumType.Date);
                }

                ISdmxDate sdmxDateTo = null;
                if (dateFrom != null)
                {
                    sdmxDateTo = new SdmxDateCore(dateTo, TimeFormatEnumType.Date);
                }

                this._complexDataQuerySelectionGroups.Add(
                    new ComplexDataQuerySelectionGroupImpl(
                        complexSelections, 
                        sdmxDateFrom, 
                        dateFromOperator, 
                        sdmxDateTo, 
                        dateToOperator, 
                        primaryMeasureValues));
            }

            // perform validation
            this.ValidateQuery();
            this.ValidateProvisionAgreement();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQueryImpl" /> class.
        /// </summary>
        /// <param name="datasetId">The dataset identifier.</param>
        /// <param name="datasetIdOperator">The dataset identifier operator.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataFlow">The data flow.</param>
        /// <param name="provisionAgreement">The provision agreement.</param>
        /// <param name="lastUpdatedDate">The last updated date.</param>
        /// <param name="firstNObs">The first n obs.</param>
        /// <param name="lastNObs">The last n obs.</param>
        /// <param name="defaultLimit">The default limit.</param>
        /// <param name="obsAction">The obs action.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="hasExplicitMeasures">if set to <c>true</c> [has explicit measures].</param>
        /// <param name="queryDetail">The query detail.</param>
        /// <param name="complexSelections">The complex selections.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateFromOperator">The date from operator.</param>
        /// <param name="dateTo">The date to.</param>
        /// <param name="dateToOperator">The date to operator.</param>
        /// <param name="primaryMeasureValues">The primary measure values.</param>
        public ComplexDataQueryImpl(
            string datasetId, 
            TextSearch datasetIdOperator, 
            ISet<IDataProvider> dataProviders, 
            IDataStructureObject dataStructure, 
            IDataflowObject dataFlow, 
            IProvisionAgreementObject provisionAgreement, 
            IList<ITimeRange> lastUpdatedDate, 
            int? firstNObs, 
            int? lastNObs, 
            int? defaultLimit, 
            ObservationAction obsAction, 
            string dimensionAtObservation, 
            bool hasExplicitMeasures, 
            DataQueryDetail queryDetail, 
            ISet<IComplexDataQuerySelection> complexSelections, 
            DateTime dateFrom, 
            OrderedOperator dateFromOperator, 
            DateTime dateTo, 
            OrderedOperator dateToOperator, 
            ISet<IComplexComponentValue> primaryMeasureValues)
        {
            this._datasetId = datasetId;
            if (datasetIdOperator != null)
            {
                this._datasetIdOperator = datasetIdOperator;
            }
            else
            {
                this._datasetIdOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            }

            this._dataProviders = dataProviders;
            this.DataStructure = dataStructure;
            this.Dataflow = dataFlow;
            this._provisionAgreement = provisionAgreement;
            this._lastUpdatedDate = lastUpdatedDate;
            this.FirstNObservations = firstNObs;
            this.LastNObservations = lastNObs;
            this._defaultLimit = defaultLimit;

            if (obsAction != null)
            {
                this._obsAction = obsAction;
            }
            else
            {
                this._obsAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            }

            this.DimensionAtObservation = dimensionAtObservation;
            if (dimensionAtObservation != null)
            {
                // the values: 'AllDimensions' and 'TIME_PERIOD' are valid values.
                if (dimensionAtObservation.Equals(_allDimensions)
                    || dimensionAtObservation.Equals(DimensionAtObservationEnumType.Time.ToString()))
                {
                    this.DimensionAtObservation = dimensionAtObservation;
                }
                else
                {
                    // check if the value is a dimension Value
                    this.CheckDimensionExistence(dimensionAtObservation, dataStructure);
                }
            }
            else
            {
                this.DimensionAtObservation = this.GetDimensionAtObservationLevel(dataStructure);
            }

            this._hasExplicitMeasures = hasExplicitMeasures;
            if (queryDetail != null)
            {
                this._queryDetail = queryDetail;
            }
            else
            {
                this._queryDetail = DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            }

            if (ObjectUtil.ValidCollection(complexSelections) || dateFrom != null || dateTo != null)
            {
                ISdmxDate sdmxDateFrom = null;
                if (dateFrom != null)
                {
                    sdmxDateFrom = new SdmxDateCore(dateFrom, TimeFormatEnumType.Date);
                }

                ISdmxDate sdmxDateTo = null;
                if (dateFrom != null)
                {
                    sdmxDateTo = new SdmxDateCore(dateTo, TimeFormatEnumType.Date);
                }

                this._complexDataQuerySelectionGroups.Add(
                    new ComplexDataQuerySelectionGroupImpl(
                        complexSelections, 
                        sdmxDateFrom, 
                        dateFromOperator, 
                        sdmxDateTo, 
                        dateToOperator, 
                        primaryMeasureValues));
            }

            // perform validation
            this.ValidateQuery();
            this.ValidateProvisionAgreement();
        }

        /// <summary>
        ///     Gets the data provider(s) that the query is for, an empty list represents ALL
        /// </summary>
        public ISet<IDataProvider> DataProvider
        {
            get
            {
                return new HashSet<IDataProvider>(this._dataProviders);
            }
        }

        /// <summary>
        ///     Gets the detail of the query.  The detail specifies whether to return just the series keys, just the data or
        ///     everything.
        ///     <p />
        ///     Defaults to FULL (everything)
        /// </summary>
        public DataQueryDetail DataQueryDetail
        {
            get
            {
                return this._queryDetail;
            }
        }

        /// <summary>
        ///     Gets the id of the dataset from which data will be returned or null if unspecified
        /// </summary>
        public string DatasetId
        {
            get
            {
                return this._datasetId;
            }
        }

        /// <summary>
        ///     Gets theId.
        ///     The Id id of the dataset requested should satisfy the condition implied by the operator this method returns.
        ///     For instance if the operator is starts_with then data from a dataset with id that starts with the
        ///     getDatasetId() result
        ///     should be returned.
        ///     Defaults to EQUAL
        /// </summary>
        public TextSearch DatasetIdOperator
        {
            get
            {
                return this._datasetIdOperator;
            }
        }

        /// <summary>
        ///     Gets the suggested maximum response size or null if unspecified
        /// </summary>
        public int? DefaultLimit
        {
            get
            {
                return this._defaultLimit;
            }
        }

        /// <summary>
        ///     Gets the type of observations to be returned.
        ///     Defaults to ACTIVE
        /// </summary>
        public ObservationAction ObservationAction
        {
            get
            {
                return this._obsAction;
            }
        }

        /// <summary>
        ///     Gets the Provision Agreement that the query is returning data for or null if unspecified
        /// </summary>
        public IProvisionAgreementObject ProvisionAgreement
        {
            get
            {
                return this._provisionAgreement;
            }
        }

        /// <summary>
        ///     Gets a list of selection groups. The list is empty if no selection groups have been added.
        /// </summary>
        public IList<IComplexDataQuerySelectionGroup> SelectionGroups
        {
            get
            {
                return new List<IComplexDataQuerySelectionGroup>(this._complexDataQuerySelectionGroups);
            }
        }

        /// <summary>
        ///     Gets the last updated time range.
        ///     This method accomplishes the following criteria :match data based on when they were last updated
        ///     The date points to the start date or the range that the queried date must occur within and/ or to the end
        ///     period of the range
        ///     It returns null if unspecified.
        /// </summary>
        public IList<ITimeRange> LastUpdatedDateTimeRange
        {
            get
            {
                return new List<ITimeRange>(this._lastUpdatedDate);
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     Gets true or false depending on the existence of explicit measures
        ///     Defaults to false
        /// </summary>
        /// <returns>
        ///     The boolean
        /// </returns>
        public bool HasExplicitMeasures()
        {
            return this._hasExplicitMeasures;
        }

        /// <summary>
        ///     Gets a value indicating whether this query has one or more selection groups on it, false means the query is a query
        ///     for all.
        /// </summary>
        /// <returns>
        ///     The boolean
        /// </returns>
        public bool HasSelections()
        {
            return this._complexDataQuerySelectionGroups.Count > 0;
        }

        /// <summary>
        ///     Gets a set of strings which represent the component ids that are being queried on
        /// </summary>
        /// <returns>
        ///     The set of components
        /// </returns>
        protected override ISet<string> GetQueryComponentIds()
        {
            ISet<string> returnSet = new HashSet<string>();
            foreach (IComplexDataQuerySelectionGroup dqsg in this.SelectionGroups)
            {
                foreach (IComplexDataQuerySelection dqs in dqsg.Selections)
                {
                    returnSet.Add(dqs.ComponentId);
                }
            }

            return returnSet;
        }

        /// <summary>
        ///     Checks the dimension existence.
        ///     It checks for the existence of the provided dimensionAtObs in the data structure definition.
        ///     If it is not included an error is thrown.
        /// </summary>
        /// <param name="dimensionAtObs">The dimension at obs.</param>
        /// <param name="dataStructure">The data structure.</param>
        /// <exception cref="SdmxSemmanticException">
        ///     Can not create DataQuery, The dimension at observation is not included in the
        ///     Dimension list of the DSD
        /// </exception>
        private void CheckDimensionExistence(string dimensionAtObs, IDataStructureObject dataStructure)
        {
            IDimension dimension = dataStructure.GetDimension(dimensionAtObs);
            if (dimension == null)
            {
                // Changed from ArgumentException in order for Web Service to produce the correct error (semantic).
                // This is different than in SdmxSource Java where it throws the equivalent of ArgumentException. 
                throw new SdmxSemmanticException(
                    "Can not create DataQuery, The dimension at observation is not included in the Dimension list of the DSD ");
            }
        }

        /// <summary>
        ///     Gets the dimension at observation level.
        ///     It returns the default value for the dimension at observation in accordance with the DSD.
        ///     For a time series DSD, the return value Equals to the Time Dimension. For a crossX DSD it returns the measure
        ///     dimension value.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <returns>The dimension at observation level</returns>
        private string GetDimensionAtObservationLevel(IDataStructureObject dataStructure)
        {
            //// Change from the SdmxSource Java. 
            //// MAT-675 : Not all DSD have either time or measure dimension. The standard says in that case 
            //// use all (flat).
            if (dataStructure.TimeDimension != null)
            {
                return Api.Constants.DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.Time).Value;
            }

            var measureDimension = dataStructure.GetDimensions(SdmxStructureEnumType.MeasureDimension).FirstOrDefault();
            if (measureDimension != null)
            {
                return measureDimension.Id;
            }

            return Api.Constants.DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.All).Value;
        }

        /// <summary>
        ///     Validates the provision agreement.
        ///     It performs validation upon the provision agreement information. If the current dataflow is not referenced
        ///     by the provision agreement then an exception occurs.
        /// </summary>
        /// <exception cref="SdmxException">
        ///     Can not create DataQuery, Dataflow is required
        ///     or
        ///     Can not create DataQuery, Dataflow provided is not referenced by the Provision Agreement
        /// </exception>
        private void ValidateProvisionAgreement()
        {
            if (this._provisionAgreement != null)
            {
                // get the dataflow id, version and agency 
                ICrossReference dataflowReference = this._provisionAgreement.StructureUseage;
                if (dataflowReference == null)
                {
                    throw new SdmxException(
                        "Can not create DataQuery, Dataflow is required", 
                        SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError));
                }

                if (!dataflowReference.MaintainableReference.MaintainableId.Equals(this.Dataflow.Id)
                    || !dataflowReference.MaintainableReference.Version.Equals(this.Dataflow.Version)
                    || !dataflowReference.MaintainableReference.AgencyId.Equals(this.Dataflow.AgencyId))
                {
                    throw new SdmxException(
                        "Can not create DataQuery, Dataflow provided is not referenced by the Provision Agreement", 
                        SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError));
                }
            }
        }
    }
}