﻿// -----------------------------------------------------------------------
// <copyright file="ComplexDataQuerySelectionGroupImpl.cs" company="EUROSTAT">
//   Date Created : 2013-06-04
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query.Complex
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The complex data query selection group class
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex.IComplexDataQuerySelectionGroup" />
    public class ComplexDataQuerySelectionGroupImpl : IComplexDataQuerySelectionGroup
    {
        /// <summary>
        ///     The _complex selection for concept
        /// </summary>
        private readonly IDictionary<string, IComplexDataQuerySelection> _complexSelectionForConcept =
            new Dictionary<string, IComplexDataQuerySelection>();

        /// <summary>
        ///     The _complex selections
        /// </summary>
        private readonly ISet<IComplexDataQuerySelection> _complexSelections = new HashSet<IComplexDataQuerySelection>();

        /// <summary>
        ///     The _date from
        /// </summary>
        private readonly ISdmxDate _dateFrom;

        /// <summary>
        ///     The _date from operator
        /// </summary>
        private readonly OrderedOperator _dateFromOperator;

        /// <summary>
        ///     The _date to
        /// </summary>
        private readonly ISdmxDate _dateTo;

        /// <summary>
        ///     The _date to operator
        /// </summary>
        private readonly OrderedOperator _dateToOperator;

        /// <summary>
        ///     The _primary measure values
        /// </summary>
        private readonly ISet<IComplexComponentValue> _primaryMeasureValues = new HashSet<IComplexComponentValue>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexDataQuerySelectionGroupImpl" /> class.
        /// </summary>
        /// <param name="complexSelections">The complex selections.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateFromOperator">The date from operator.</param>
        /// <param name="dateTo">The date to.</param>
        /// <param name="dateToOperator">The date to operator.</param>
        /// <param name="primaryMeasureValues">The primary measure values.</param>
        /// <exception cref="SdmxSemmanticException">The SDMX semantic exception</exception>
        /// <exception cref="ArgumentException">Duplicate concept</exception>
        public ComplexDataQuerySelectionGroupImpl(
            ISet<IComplexDataQuerySelection> complexSelections, 
            ISdmxDate dateFrom, 
            OrderedOperator dateFromOperator, 
            ISdmxDate dateTo, 
            OrderedOperator dateToOperator, 
            ISet<IComplexComponentValue> primaryMeasureValues)
        {
            if (dateFromOperator == null)
            {
                dateFromOperator = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal);
            }

            if (dateToOperator == null)
            {
                dateToOperator = OrderedOperator.GetFromEnum(OrderedOperatorEnumType.Equal);
            }

            // check if the operator to be applied on the time has not the 'NOT_EQUAL' value
            if (dateFromOperator == OrderedOperatorEnumType.NotEqual
                || dateToOperator == OrderedOperatorEnumType.NotEqual)
            {
                throw new SdmxSemmanticException(ExceptionCode.QuerySelectionIllegalOperator);
            }

            if (complexSelections == null)
            {
                return;
            }

            this._dateFrom = dateFrom;
            this._dateFromOperator = dateFromOperator;
            this._dateTo = dateTo;
            this._dateToOperator = dateToOperator;
            this._complexSelections = complexSelections;
            this._primaryMeasureValues = primaryMeasureValues;

            // Add each of the Component Selections to the selection concept map. 
            foreach (IComplexDataQuerySelection compSel in this._complexSelections)
            {
                if (this._complexSelectionForConcept.ContainsKey(compSel.ComponentId))
                {
                    // TODO Does this require a exception, or can the code selections be merged?
                    throw new ArgumentException("Duplicate concept");
                }

                this._complexSelectionForConcept.Add(compSel.ComponentId, compSel);
            }
        }

        /// <summary>
        ///     Gets the "date from" in this selection group.
        /// </summary>
        public ISdmxDate DateFrom
        {
            get
            {
                return this._dateFrom;
            }
        }

        /// <summary>
        ///     Gets the operator for the dateFrom
        ///     The operator cannot take the 'NOT_EQUAL' value
        /// </summary>
        public OrderedOperator DateFromOperator
        {
            get
            {
                return this._dateFromOperator;
            }
        }

        /// <summary>
        ///     Gets the "date to" in this selection group.
        /// </summary>
        public ISdmxDate DateTo
        {
            get
            {
                return this._dateTo;
            }
        }

        /// <summary>
        ///     Gets the operator for the dateTo
        ///     The operator cannot take the 'NOT_EQUAL' value
        /// </summary>
        public OrderedOperator DateToOperator
        {
            get
            {
                return this._dateToOperator;
            }
        }

        /// <summary>
        ///     Gets the component value (s) for a primary measure value.
        /// </summary>
        public ISet<IComplexComponentValue> PrimaryMeasureValue
        {
            get
            {
                return this._primaryMeasureValues;
            }
        }

        /// <summary>
        ///     Gets the set of selections for this group. These DataQuerySelections are implicitly ANDED together.
        /// </summary>
        public ISet<IComplexDataQuerySelection> Selections
        {
            get
            {
                return this._complexSelections;
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     Returns the selection(s) for the given component id (dimension or attribute) or returns null if no selection exists
        ///     for the component id.
        /// </summary>
        /// <param name="componentId">The component id</param>
        /// <returns>
        ///     The selection(s)
        /// </returns>
        public IComplexDataQuerySelection GetSelectionsForConcept(string componentId)
        {
            return this._complexSelectionForConcept.GetOrDefault(componentId);
        }

        /// <summary>
        ///     Returns true if selections exist for this dimension Id.
        /// </summary>
        /// <param name="componentId">The dimension id</param>
        /// <returns>
        ///     The boolean
        /// </returns>
        public bool HasSelectionForConcept(string componentId)
        {
            return this._complexSelectionForConcept.ContainsKey(componentId);
        }
    }
}