﻿// -----------------------------------------------------------------------
// <copyright file="DataQueryFluentBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-03-28
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     Data query fluent builder class.
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Builder.IDataQueryFluentBuilder" />
    public class DataQueryFluentBuilder : IDataQueryFluentBuilder
    {
        /// <summary>
        ///     The _dataflow
        /// </summary>
        private IDataflowObject _dataflow;

        /// <summary>
        ///     The _data providers
        /// </summary>
        private ISet<IDataProvider> _dataProviders = new HashSet<IDataProvider>();

        /// <summary>
        ///     The _data query detail
        /// </summary>
        private DataQueryDetail _dataQueryDetail = DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);

        /// <summary>
        ///     The _data query selection group
        /// </summary>
        private ICollection<IDataQuerySelectionGroup> _dataQuerySelectionGroup;

        /// <summary>
        ///     The _data query selections
        /// </summary>
        private ISet<IDataQuerySelection> _dataQuerySelections;

        /// <summary>
        ///     The _data structure
        /// </summary>
        private IDataStructureObject _dataStructure;

        /// <summary>
        ///     The _date from
        /// </summary>
        private DateTime? _dateFrom; // value type

        /// <summary>
        ///     The _date to
        /// </summary>
        private DateTime? _dateTo;

        /// <summary>
        ///     The _dimension at observation
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        ///     The _first n obs
        /// </summary>
        private int? _firstNObs;

        /// <summary>
        ///     The _last n obs
        /// </summary>
        private int _lastNObs;

        /// <summary>
        ///     The _last updated
        /// </summary>
        private ISdmxDate _lastUpdated;

        /// <summary>
        ///     The _max obs
        /// </summary>
        private int? _maxObs;

        /// <summary>
        ///     The _order asc
        /// </summary>
        private bool _orderAsc;

        /// <summary>
        ///     Materialize the object construction
        /// </summary>
        /// <returns>
        ///     Returns a new instance of DataQuery
        /// </returns>
        public IDataQuery Build()
        {
            if (this._dataQuerySelectionGroup != null)
            {
                return new DataQueryImpl(
                    this._dataStructure, 
                    this._lastUpdated, 
                    this._dataQueryDetail, 
                    this._maxObs ?? default(int), 
                    this._orderAsc, 
                    this._dataProviders, 
                    this._dataflow, 
                    this._dimensionAtObservation, 
                    this._dataQuerySelectionGroup);
            }

            if (this._firstNObs.HasValue)
            {
                return new DataQueryImpl(
                    this._dataStructure, 
                    this._lastUpdated, 
                    this._dataQueryDetail, 
                    this._firstNObs.Value, 
                    this._lastNObs, 
                    this._dataProviders, 
                    this._dataflow, 
                    this._dimensionAtObservation, 
                    this._dataQuerySelections, 
                    this._dateFrom, 
                    this._dateTo);
            }

            if (this._maxObs.HasValue)
            {
                return new DataQueryImpl(
                    this._dataStructure, 
                    this._lastUpdated, 
                    this._dataQueryDetail, 
                    this._maxObs.Value, 
                    this._orderAsc, 
                    this._dataProviders, 
                    this._dataflow, 
                    this._dimensionAtObservation, 
                    this._dataQuerySelections, 
                    this._dateFrom, 
                    this._dateTo);
            }

            return DataQueryImpl.BuildEmptyQuery(this._dataStructure, this._dataflow, this._dataQueryDetail);
        }

        /// <summary>
        ///     Initialize the object to create a new DataQuery instance
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder Initialize(IDataStructureObject dataStructure, IDataflowObject dataflow)
        {
            this._dataStructure = dataStructure;
            this._dataflow = dataflow;
            return this;
        }

        /// <summary>
        ///     Add Data Providers
        /// </summary>
        /// <param name="dataProviders">The data providers.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDataProviders(ISet<IDataProvider> dataProviders)
        {
            this._dataProviders = dataProviders;
            return this;
        }

        /// <summary>
        ///     Add Data Query Detail
        /// </summary>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDataQueryDetail(DataQueryDetail dataQueryDetail)
        {
            if (dataQueryDetail != null)
            {
                this._dataQueryDetail = dataQueryDetail;
            }

            return this;
        }

        /// <summary>
        ///     Add Data Query Selection Group
        /// </summary>
        /// <param name="dataQuerySelectionGroups">The data query selection groups.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDataQuerySelectionGroup(
            ICollection<IDataQuerySelectionGroup> dataQuerySelectionGroups)
        {
            this._dataQuerySelectionGroup = dataQuerySelectionGroups;
            return this;
        }

        /// <summary>
        ///     Add Data Query Selections
        /// </summary>
        /// <param name="dataQuerySelections">The data query selections.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDataQuerySelections(ISet<IDataQuerySelection> dataQuerySelections)
        {
            this._dataQuerySelections = dataQuerySelections;
            return this;
        }

        /// <summary>
        ///     Add Date From
        /// </summary>
        /// <param name="dateFrom">The date from.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDateFrom(DateTime dateFrom)
        {
            this._dateFrom = dateFrom;
            return this;
        }

        /// <summary>
        ///     Add Date To
        /// </summary>
        /// <param name="dateTo">The date to.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDateTo(DateTime dateTo)
        {
            this._dateTo = dateTo;
            return this;
        }

        /// <summary>
        ///     Add Dimension Observation At
        /// </summary>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithDimensionAtObservation(string dimensionAtObservation)
        {
            this._dimensionAtObservation = dimensionAtObservation;
            return this;
        }

        /// <summary>
        ///     Add First N Observations
        /// </summary>
        /// <param name="firstNObs">The first n observations</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithFirstNObs(int firstNObs)
        {
            this._firstNObs = firstNObs;
            return this;
        }

        /// <summary>
        ///     Add Last N Observations
        /// </summary>
        /// <param name="lastNObs">The last n observations</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithLastNObs(int lastNObs)
        {
            this._lastNObs = lastNObs;
            return this;
        }

        /// <summary>
        ///     Add Last Updated parameter
        /// </summary>
        /// <param name="lastUpdated">The last updated.</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithLastUpdated(ISdmxDate lastUpdated)
        {
            this._lastUpdated = lastUpdated;
            return this;
        }

        /// <summary>
        ///     Add Max Observations
        /// </summary>
        /// <param name="maxObs">The maximum observations</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithMaxObservations(int maxObs)
        {
            this._maxObs = maxObs;
            return this;
        }

        /// <summary>
        ///     Add Order Ascending
        /// </summary>
        /// <param name="orderAsc">if set to <c>true</c> [order ascending].</param>
        /// <returns>
        ///     The data query fluent builder
        /// </returns>
        public IDataQueryFluentBuilder WithOrderAsc(bool orderAsc)
        {
            this._orderAsc = orderAsc;
            return this;
        }
    }
}