// -----------------------------------------------------------------------
// <copyright file="DataQueryImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The data query impl.
    /// </summary>
    public class DataQueryImpl : BaseDataQuery, IDataQuery
    {
        /// <summary>
        ///     The _data providers.
        /// </summary>
        private readonly ISet<IDataProvider> _dataProviders = new HashSet<IDataProvider>();

        /// <summary>
        ///     The _data query detail.
        /// </summary>
        private readonly DataQueryDetail _dataQueryDetail;

        /// <summary>
        ///     The _data query selection groups.
        /// </summary>
        private readonly IList<IDataQuerySelectionGroup> _dataQuerySelectionGroups =
            new List<IDataQuerySelectionGroup>();

        /// <summary>
        ///     The _last updated.
        /// </summary>
        private readonly ISdmxDate _lastUpdated;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="SdmxNoResultsException">
        ///     Data Flow could not be found for query :  + dataQuery.FlowRef
        ///     or
        ///     DSD could not be found for query :  + this.Dataflow.DataStructureRef
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Not enough key values in query, expecting
        ///     + this.DataStructure.GetDimensions(
        ///     SdmxStructureEnumType.Dimension,
        ///     SdmxStructureEnumType.MeasureDimension).Count +  got  + dataQuery.QueryList.Count
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="retrievalManager"/> is <see langword="null" />.</exception>
        public DataQueryImpl(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            this._lastUpdated = dataQuery.UpdatedAfter;
            this._dataQueryDetail = dataQuery.QueryDetail ?? DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);

            this.FirstNObservations = dataQuery.FirstNObservations;
            this.LastNObservations = dataQuery.LastNObsertations;
            if (ObjectUtil.ValidString(dataQuery.DimensionAtObservation))
            {
                this.DimensionAtObservation = dataQuery.DimensionAtObservation;
            }

            this.RetrieveDataflow(dataQuery, retrievalManager);

            this.RetrieveDsd(retrievalManager);

            RetrieveDataProviders(dataQuery, retrievalManager);

            this.BuildSelections(dataQuery);

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">
        ///     The data structure.
        /// </param>
        /// <param name="lastUpdated">
        ///     The last updated.
        /// </param>
        /// <param name="dataQueryDetail">
        ///     The data query detail.
        /// </param>
        /// <param name="firstNObs">
        ///     The first n obs.
        /// </param>
        /// <param name="lastNObs">
        ///     The last n obs.
        /// </param>
        /// <param name="dataProviders">
        ///     The data providers.
        /// </param>
        /// <param name="dataflow">
        ///     The dataflow.
        /// </param>
        /// <param name="dimensionAtObservation">
        ///     The dimension at observation.
        /// </param>
        /// <param name="selections">
        ///     The selections.
        /// </param>
        /// <param name="dateFrom">
        ///     The date from.
        /// </param>
        /// <param name="dateTo">
        ///     The date to.
        /// </param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int? firstNObs, 
            int? lastNObs, 
            ISet<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            ISet<IDataQuerySelection> selections, 
            DateTime? dateFrom, 
            DateTime? dateTo)
        {
            this.DataStructure = dataStructure;
            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            if (ObjectUtil.ValidCollection(selections) || dateFrom != null || dateTo != null)
            {
                ISdmxDate sdmxDateFrom = null;
                if (dateFrom != null)
                {
                    sdmxDateFrom = new SdmxDateCore(dateFrom, TimeFormatEnumType.Date);
                }

                ISdmxDate sdmxDateTo = null;
                if (dateTo != null)
                {
                    sdmxDateTo = new SdmxDateCore(dateTo, TimeFormatEnumType.Date);
                }

                this._dataQuerySelectionGroups.Add(
                    new DataQuerySelectionGroupImpl(selections, sdmxDateFrom, sdmxDateTo));
            }

            this.Dataflow = dataflow;
            this._lastUpdated = lastUpdated;
            this._dataQueryDetail = dataQueryDetail ?? DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            this.DimensionAtObservation = dimensionAtObservation;
            this.FirstNObservations = firstNObs;
            this.LastNObservations = lastNObs;
            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="lastUpdated">The last updated.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <param name="maxObs">The maximum obs.</param>
        /// <param name="orderAsc">if set to <c>true</c> [order asc].</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="selections">The selections.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int maxObs, 
            bool orderAsc, 
            ISet<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            ISet<IDataQuerySelection> selections, 
            DateTime? dateFrom, 
            DateTime? dateTo)
        {
            this.DataStructure = dataStructure;
            this._lastUpdated = lastUpdated;
            this._dataQueryDetail = dataQueryDetail ?? DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.Dataflow = dataflow;
            this.DimensionAtObservation = dimensionAtObservation;

            if (ObjectUtil.ValidCollection(selections) || dateFrom != null || dateTo != null)
            {
                ISdmxDate sdmxDateFrom = null;
                if (dateFrom != null)
                {
                    sdmxDateFrom = new SdmxDateCore(dateFrom, TimeFormatEnumType.Date);
                }

                ISdmxDate sdmxDateTo = null;
                if (dateTo != null)
                {
                    sdmxDateTo = new SdmxDateCore(dateTo, TimeFormatEnumType.Date);
                }

                // TODO: move to fluent interface
                this._dataQuerySelectionGroups.Add(
                    new DataQuerySelectionGroupImpl(selections, sdmxDateFrom, sdmxDateTo));
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="lastUpdated">The last updated.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <param name="firstNObs">The first asynchronous obs.</param>
        /// <param name="lastNObs">The last asynchronous obs.</param>
        /// <param name="dataProviders">The data providers.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="selectionGroup">The selection group.</param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int? firstNObs, 
            int? lastNObs, 
            IEnumerable<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            IEnumerable<IDataQuerySelectionGroup> selectionGroup)
        {
            this.DataStructure = dataStructure;
            this._lastUpdated = lastUpdated;
            this._dataQueryDetail = dataQueryDetail ?? DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            this.FirstNObservations = firstNObs;
            this.LastNObservations = lastNObs;
            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.Dataflow = dataflow;
            this.DimensionAtObservation = dimensionAtObservation;

            if (selectionGroup != null)
            {
                foreach (IDataQuerySelectionGroup dqsg in selectionGroup)
                {
                    if (dqsg != null)
                    {
                        this._dataQuerySelectionGroups.Add(dqsg);
                    }
                }
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">
        ///     The data structure.
        /// </param>
        /// <param name="lastUpdated">
        ///     The last updated.
        /// </param>
        /// <param name="dataQueryDetail">
        ///     The data query detail.
        /// </param>
        /// <param name="maxObs">
        ///     The max obs.
        /// </param>
        /// <param name="orderAsc">
        ///     If the order is ascending.
        /// </param>
        /// <param name="dataProviders">
        ///     The data providers.
        /// </param>
        /// <param name="dataflow">
        ///     The dataflow.
        /// </param>
        /// <param name="dimensionAtObservation">
        ///     The dimension at observation.
        /// </param>
        /// <param name="selectionGroup">
        ///     The selection group.
        /// </param>
        public DataQueryImpl(
            IDataStructureObject dataStructure, 
            ISdmxDate lastUpdated, 
            DataQueryDetail dataQueryDetail, 
            int maxObs, 
            bool orderAsc, 
            ISet<IDataProvider> dataProviders, 
            IDataflowObject dataflow, 
            string dimensionAtObservation, 
            ICollection<IDataQuerySelectionGroup> selectionGroup)
        {
            this.DataStructure = dataStructure;
            this._lastUpdated = lastUpdated;
            this._dataQueryDetail = dataQueryDetail ?? DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            if (orderAsc)
            {
                this.FirstNObservations = maxObs;
            }
            else
            {
                this.LastNObservations = maxObs;
            }

            if (dataProviders != null)
            {
                this._dataProviders = new HashSet<IDataProvider>(dataProviders);
            }

            this.Dataflow = dataflow;
            this.DimensionAtObservation = dimensionAtObservation;

            if (selectionGroup != null)
            {
                foreach (IDataQuerySelectionGroup dqsg in selectionGroup)
                {
                    if (dqsg != null)
                    {
                        this._dataQuerySelectionGroups.Add(dqsg);
                    }
                }
            }

            this.ValidateQuery();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryImpl" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        private DataQueryImpl(
            IDataStructureObject dataStructure, 
            IDataflowObject dataflow, 
            DataQueryDetail dataQueryDetail)
        {
            this.DataStructure = dataStructure;
            this._dataQueryDetail = dataQueryDetail ?? DataQueryDetail.GetFromEnum(DataQueryDetailEnumType.Full);
            this.Dataflow = dataflow;
            this.ValidateQuery();
        }

        /// <summary>
        ///     Gets the data provider.
        /// </summary>
        public virtual ISet<IDataProvider> DataProvider
        {
            get
            {
                return new HashSet<IDataProvider>(this._dataProviders);
            }
        }

        /// <summary>
        ///     Gets the data query detail.
        /// </summary>
        public virtual DataQueryDetail DataQueryDetail
        {
            get
            {
                return this._dataQueryDetail;
            }
        }

        /// <summary>
        ///     Gets the last updated date.
        /// </summary>
        public virtual ISdmxDate LastUpdatedDate
        {
            get
            {
                return this._lastUpdated;
            }
        }

        /// <summary>
        ///     Gets the selection groups.
        /// </summary>
        public virtual IList<IDataQuerySelectionGroup> SelectionGroups
        {
            get
            {
                return new List<IDataQuerySelectionGroup>(this._dataQuerySelectionGroups);
            }
        }

        /// <summary>
        ///     Builds the empty query.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="dataQueryDetail">The data query detail.</param>
        /// <returns>The data query.</returns>
        public static IDataQuery BuildEmptyQuery(
            IDataStructureObject dataStructure, 
            IDataflowObject dataflow, 
            DataQueryDetail dataQueryDetail)
        {
            return new DataQueryImpl(dataStructure, dataflow, dataQueryDetail);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     The has selections.
        /// </summary>
        /// <returns> The <see cref="bool" /> . </returns>
        public virtual bool HasSelections()
        {
            return this._dataQuerySelectionGroups.Count > 0;
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        public override string ToString()
        {
            var sb = new StringBuilder();

            string newLine = Environment.NewLine;
            if (this.DataStructure != null)
            {
                sb.Append(newLine).Append("Data Structure : ").Append(this.DataStructure.Urn);
            }

            if (this.Dataflow != null)
            {
                sb.Append(newLine).Append("Dataflow : ").Append(this.Dataflow.Urn);
            }

            if (this._dataProviders != null)
            {
                foreach (IDataProvider dataProvider in this._dataProviders)
                {
                    sb.Append(newLine).Append("Data Provider  : ").Append(dataProvider.Urn);
                }
            }

            // ADD SELECTION INFORMATION
            if (this.HasSelections())
            {
                string concat = string.Empty;

                foreach (IDataQuerySelectionGroup selectionGroup in this._dataQuerySelectionGroups)
                {
                    sb.Append(concat).Append("(").Append(selectionGroup).Append(")");
                    concat = "OR";
                }
            }

            return sb.ToString();
        }

        /// <summary>
        ///     Gets a set of strings which represent the component ids that are being queried on
        /// </summary>
        /// <returns>
        ///     The set of components
        /// </returns>
        protected override ISet<string> GetQueryComponentIds()
        {
            ISet<string> returnSet = new HashSet<string>();
            foreach (IDataQuerySelectionGroup dqsg in this.SelectionGroups)
            {
                foreach (IDataQuerySelection dqs in dqsg.Selections)
                {
                    returnSet.Add(dqs.ComponentId);
                }
            }

            return returnSet;
        }

        /// <summary>
        /// Retrieves the data providers.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        private static void RetrieveDataProviders(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            ISet<IDataProvider> dataProviders = new HashSet<IDataProvider>();
            if (dataQuery.ProviderRef != null)
            {
                ISet<IDataProviderScheme> dataProviderSchemes = retrievalManager.GetMaintainableObjects<IDataProviderScheme>(dataQuery.ProviderRef.MaintainableReference);
                foreach (IDataProviderScheme currentDpScheme in dataProviderSchemes)
                {
                    foreach (IDataProvider dataProvider in currentDpScheme.Items)
                    {
                        if (dataProvider.Id.Equals(dataQuery.ProviderRef.ChildReference.Id))
                        {
                            dataProviders.Add(dataProvider);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the selections.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <exception cref="SdmxSemmanticException">Not enough key values in query, expecting the number of Dimensions got dataQuery.QueryList.Count</exception>
        private void BuildSelections(IRestDataQuery dataQuery)
        {
            ISet<IDataQuerySelection> selections = new HashSet<IDataQuerySelection>();
            if (dataQuery.QueryList.Count > 0)
            {
                int i = 0;
                foreach (IDimension dimension in
                    this.DataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension).OrderBy(dimension => dimension.Position))
                {
                    if (dataQuery.QueryList.Count <= i)
                    {
                        throw new SdmxSemmanticException(
                            "Not enough key values in query, expecting " + this.DataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension).Count + " got "
                            + dataQuery.QueryList.Count);
                    }

                    ISet<string> queriesForDimension = dataQuery.QueryList[i];
                    if (queriesForDimension != null && queriesForDimension.Count > 0)
                    {
                        IDataQuerySelection selectionsForDimension = new DataQueryDimensionSelectionImpl(dimension.Id, new HashSet<string>(queriesForDimension));
                        selections.Add(selectionsForDimension);
                    }

                    i++;
                }
            }

            if (ObjectUtil.ValidCollection(selections) || dataQuery.StartPeriod != null || dataQuery.EndPeriod != null)
            {
                this._dataQuerySelectionGroups.Add(new DataQuerySelectionGroupImpl(selections, dataQuery.StartPeriod, dataQuery.EndPeriod));
            }
        }

        /// <summary>
        /// Retrieves the DSD.
        /// </summary>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="SdmxNoResultsException">DSD could not be found for query :  + this.Dataflow.DataStructureRef</exception>
        private void RetrieveDsd(ISdmxObjectRetrievalManager retrievalManager)
        {
            this.DataStructure = retrievalManager.GetMaintainableObject<IDataStructureObject>(this.Dataflow.DataStructureRef.MaintainableReference);
            if (this.DataStructure == null)
            {
                throw new SdmxNoResultsException("DSD could not be found for query : " + this.Dataflow.DataStructureRef);
            }
        }

        /// <summary>
        /// Retrieves the dataflow.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="SdmxNoResultsException">Data Flow could not be found for query :  + dataQuery.FlowRef</exception>
        private void RetrieveDataflow(IRestDataQuery dataQuery, ISdmxObjectRetrievalManager retrievalManager)
        {
            this.Dataflow = retrievalManager.GetMaintainableObject<IDataflowObject>(dataQuery.FlowRef.MaintainableReference);
            if (this.Dataflow == null)
            {
                throw new SdmxNoResultsException("Data Flow could not be found for query : " + dataQuery.FlowRef);
            }
        }
    }
}