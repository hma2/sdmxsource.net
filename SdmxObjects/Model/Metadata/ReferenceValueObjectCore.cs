// -----------------------------------------------------------------------
// <copyright file="ReferenceValueObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;

    using TargetType = Org.Sdmxsource.Sdmx.Api.Constants.TargetType;

    #endregion

    /// <summary>
    ///     The reference value object core.
    /// </summary>
    [Serializable]
    public class ReferenceValueObjectCore : SdmxObjectCore, IReferenceValue
    {
        /// <summary>
        ///     The constraint reference.
        /// </summary>
        private readonly ICrossReference _constraintReference;

        /// <summary>
        ///     The data keys.
        /// </summary>
        private readonly IList<IDataKey> _dataKeys;

        /// <summary>
        ///     The dataset id.
        /// </summary>
        private readonly string _datasetId;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The identifiable reference.
        /// </summary>
        private readonly ICrossReference _identifiableReference;

        /// <summary>
        ///     The report period
        /// </summary>
        private readonly ISdmxDate _reportPeriod;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReferenceValueObjectCore" /> class.
        /// </summary>
        /// <param name="parent"> The parent. </param>
        /// <param name="type"> The type. </param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public ReferenceValueObjectCore(ITarget parent, ReferenceValueType type)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataReferenceValue), parent)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this._dataKeys = new List<IDataKey>();
            this._id = type.id;
            if (type.ConstraintContentReference != null)
            {
                this._constraintReference = RefUtil.CreateReference(this, type.ConstraintContentReference);
            }

            if (type.ObjectReference != null)
            {
                this._identifiableReference = RefUtil.CreateReference(this, type.ObjectReference);
            }

            if (type.DataSetReference != null)
            {
                this._datasetId = type.DataSetReference.ID;
                this._identifiableReference = RefUtil.CreateReference(this, type.DataSetReference.DataProvider);
            }

            if (type.DataKey != null)
            {
                foreach (var cvst in type.DataKey.GetTypedKeyValue<DataKeyValueType>())
                {
                    this._dataKeys.Add(new DataKeyObjectCore(this, cvst));
                }
            }

            if (type.ReportPeriod != null)
            {
                this._reportPeriod = new SdmxDateCore(type.ReportPeriod.ToString());
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the content constraint reference.
        /// </summary>
        public ICrossReference ContentConstraintReference
        {
            get
            {
                return this._constraintReference;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether datakey reference.
        /// </summary>
        public bool DatakeyReference
        {
            get
            {
                return this._dataKeys.Count > 0;
            }
        }

        /// <summary>
        ///     Gets the data keys.
        /// </summary>
        public IList<IDataKey> DataKeys
        {
            get
            {
                return new List<IDataKey>(this._dataKeys);
            }
        }

        /// <summary>
        ///     Gets the dataset id.
        /// </summary>
        public string DatasetId
        {
            get
            {
                return this._datasetId;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether dataset reference.
        /// </summary>
        public bool DatasetReference
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this._datasetId);
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the identifiable reference.
        /// </summary>
        public ICrossReference IdentifiableReference
        {
            get
            {
                return this._identifiableReference;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether content constriant reference.
        /// </summary>
        public bool IsContentConstriantReference
        {
            get
            {
                return this._constraintReference != null;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether is identifiable reference.
        /// </summary>
        public bool IsIdentifiableReference
        {
            get
            {
                return !this.DatasetReference && this._identifiableReference != null;
            }
        }

        /// <summary>
        ///     Gets the date for which this report is relevant
        /// </summary>
        public ISdmxDate ReportPeriod
        {
            get
            {
                return this._reportPeriod;
            }
        }

        /// <summary>
        ///     Gets an enumeration defining what this reference value is referencing
        /// </summary>
        /// <value>
        ///     The type of the target.
        /// </value>
        /// <exception cref="InvalidOperationException" accessor="get">Reference value is not referencing anything</exception>
        public TargetType TargetType
        {
            get
            {
                if (this._constraintReference != null)
                {
                    return TargetType.Constraint;
                }

                if (ObjectUtil.ValidString(this._datasetId))
                {
                    return TargetType.Dataset;
                }

                if (this._dataKeys.Count > 0)
                {
                    return TargetType.DataKey;
                }

                if (this._reportPeriod != null)
                {
                    return TargetType.ReportPeriod;
                }

                if (this._identifiableReference != null)
                {
                    return TargetType.Identifiable;
                }

                // THIS POINT SHOULD NEVER BE REACHED AND PICKED UP BY THE VALIDATE METHOD
                throw new InvalidOperationException("Reference value is not referencing anything");
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                IReferenceValue that = (IReferenceValue)sdmxObject;
                if (!string.Equals(this._id, that.Id))
                {
                    return false;
                }

                if (!string.Equals(this._datasetId, that.DatasetId))
                {
                    return false;
                }

                if (!Equals(this._constraintReference, that.ContentConstraintReference))
                {
                    return false;
                }

                if (!Equals(this._identifiableReference, that.IdentifiableReference))
                {
                    return false;
                }

                if (!Equals(this._reportPeriod, that.ReportPeriod))
                {
                    return false;
                }

                if (!this.Equivalent(this._dataKeys, that.DataKeys, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsInternal(that);
            }

            return false;
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "Metadata Target Reference " + this.Id;
        }

        /// <summary>
        ///     Gets the internal composites.
        /// </summary>
        /// <returns>The set of SDMX objects</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._dataKeys, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (!ObjectUtil.ValidString(this._id))
            {
                throw new SdmxSemmanticException("Metadata Report must have an Id");
            }

            if (ObjectUtil.ValidString(this._datasetId))
            {
                this.ValidateDataSet();
            }
            else if (this._dataKeys.Count > 0)
            {
                this.ValidateDataKeys();
            }
            else if (this._identifiableReference != null)
            {
                this.ValidateIdentifableReference();
            }
            else if (this._reportPeriod != null)
            {
                this.ValidateReportingPeriod();
            }
            else if (this._constraintReference != null)
            {
                this.ValidateConstraintReference();
            }
            else
            {
                throw new SdmxSemmanticException(
                    "Metadata Reference Value must referenece either a datakey, dataset, report period, or an identifiable");
            }
        }

        /// <summary>
        /// Validates the constraint reference.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Constraint, and an Identifiable
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Constraint, and a dataset
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Constraint, and a DataKey
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Constraint, and a Report Period
        /// </exception>
        private void ValidateConstraintReference()
        {
            if (this._identifiableReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Constraint, and an Identifiable");
            }

            if (ObjectUtil.ValidString(this._datasetId))
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Constraint, and a dataset");
            }

            if (this._dataKeys.Count > 0)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Constraint, and a DataKey");
            }

            if (this._reportPeriod != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Constraint, and a Report Period");
            }
        }

        /// <summary>
        /// Validates the reporting period.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Report Period, and an Identifiable
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Report Period, and a DataKey
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Report Period, and a dataset
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a Report Period, and a Constraint
        /// </exception>
        private void ValidateReportingPeriod()
        {
            if (this._identifiableReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Report Period, and an Identifiable");
            }

            if (this._dataKeys.Count > 0)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Report Period, and a DataKey");
            }

            if (ObjectUtil.ValidString(this._datasetId))
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Report Period, and a dataset");
            }

            if (this._constraintReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a Report Period, and a Constraint");
            }
        }

        /// <summary>
        /// Validates the identifable reference.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both an Identifiable, and a DataKey
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both an Identifiable, and a dataset
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both an Identifiable, and a Report Period
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both an Identifiable, and a Constraint
        /// </exception>
        private void ValidateIdentifableReference()
        {
            if (this._dataKeys.Count > 0)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both an Identifiable, and a DataKey");
            }

            if (ObjectUtil.ValidString(this._datasetId))
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both an Identifiable, and a dataset");
            }

            if (this._reportPeriod != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both an Identifiable, and a Report Period");
            }

            if (this._constraintReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both an Identifiable, and a Constraint");
            }
        }

        /// <summary>
        /// Validates the data keys.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a DataKey, and an Identifiable
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a DataKey, and a dataset
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a DataKey, and a Report Period
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a DataKey, and a Constraint
        /// </exception>
        private void ValidateDataKeys()
        {
            if (this._identifiableReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a DataKey, and an Identifiable");
            }

            if (ObjectUtil.ValidString(this._datasetId))
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a DataKey, and a dataset");
            }

            if (this._reportPeriod != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a DataKey, and a Report Period");
            }

            if (this._constraintReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a DataKey, and a Constraint");
            }
        }

        /// <summary>
        /// Validates the data set.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a dataset, and a DataKey
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a dataset, and a Report Period
        /// or
        /// Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  ' + this._id + ' references both a dataset, and a Constraint
        /// </exception>
        private void ValidateDataSet()
        {
            if (this._dataKeys.Count > 0)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a dataset, and a DataKey");
            }

            if (this._reportPeriod != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a dataset, and a Report Period");
            }

            if (this._constraintReference != null)
            {
                throw new SdmxSemmanticException(
                    "Reference Value can only contain one target, a datakey, dataset, report period, or an identifiable.  '" + this._id + "' references both a dataset, and a Constraint");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}