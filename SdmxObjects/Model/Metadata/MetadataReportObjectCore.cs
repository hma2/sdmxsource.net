// -----------------------------------------------------------------------
// <copyright file="MetadataReportObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using TargetType = Org.Sdmxsource.Sdmx.Api.Constants.TargetType;

    #endregion

    /// <summary>
    ///     The metadata report dataStructureObject core.
    /// </summary>
    [Serializable]
    public class MetadataReportObjectCore : SdmxObjectCore, IMetadataReport
    {
        /// <summary>
        ///     The reported attributes.
        /// </summary>
        private readonly IList<IReportedAttributeObject> _reportedAttributes;

        /// <summary>
        ///     The _target.
        /// </summary>
        private readonly ITarget _target;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataReportObjectCore" /> class.
        /// </summary>
        /// <param name="parent"> The parent. </param>
        /// <param name="report"> The dataStructureObject. </param>
        /// <exception cref="ArgumentNullException"><paramref name="report"/> is <see langword="null" />.</exception>
        public MetadataReportObjectCore(IMetadataSet parent, ReportType report)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataReport), parent)
        {
            if (report == null)
            {
                throw new ArgumentNullException("report");
            }

            this._reportedAttributes = new List<IReportedAttributeObject>();
            this._id = report.id;
            if (report.Target != null)
            {
                this._target = new TargetObjectCore(this, report.Target);
            }

            if (report.AttributeSet != null)
            {
                if (ObjectUtil.ValidCollection(report.AttributeSet.ReportedAttribute))
                {
                    this._reportedAttributes.Clear();

                    foreach (ReportedAttributeType each in report.AttributeSet.ReportedAttribute)
                    {
                        this._reportedAttributes.Add(new ReportedAttributeObjectObjectCore(this, each));
                    }
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the reported attributes.
        /// </summary>
        public virtual IList<IReportedAttributeObject> ReportedAttributes
        {
            get
            {
                return new List<IReportedAttributeObject>(this._reportedAttributes);
            }
        }

        /// <summary>
        ///     Gets the _target.
        /// </summary>
        public virtual ITarget Target
        {
            get
            {
                return this._target;
            }
        }

        /// <summary>
        ///     Gets the reference to the content constraint, if there is one
        ///     This will search the Target.IReportedAttribute for any that contain a constraint reference, and will return null if
        ///     none do.
        /// </summary>
        public ICrossReference TargetContentConstraintReference
        {
            get
            {
                foreach (IReferenceValue rv in this._target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.Constraint)
                    {
                        return rv.ContentConstraintReference;
                    }
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets a list of data keys, will return an empty list if IsDatasetReference is false
        ///     This will search the Target.IReportedAttribute for any that contain data keys, and will return null if none do.
        /// </summary>
        public IList<IDataKey> TargetDataKeys
        {
            get
            {
                foreach (IReferenceValue rv in this._target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.DataKey)
                    {
                        return rv.DataKeys;
                    }
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets the id of the dataset this bean is referencing, returns null if this is not a dataset reference.
        ///     This will search the Target.ReportedAttribute for any that contain a dataset id, and will return null if none do.
        ///     <remarks>
        ///         This is to be used in conjunction with the IdentifiableReference which will return the data provider reference
        ///     </remarks>
        /// </summary>
        public string TargetDatasetId
        {
            get
            {
                foreach (IReferenceValue rv in this._target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.Dataset)
                    {
                        return rv.DatasetId;
                    }
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets null if there is no reference to an identifiable structure
        ///     This will search the Target.IReportedAttribute for any that contain a reportPeriod, and will return null if none
        ///     do.
        /// </summary>
        public ICrossReference TargetIdentifiableReference
        {
            get
            {
                foreach (IReferenceValue rv in this._target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.Identifiable)
                    {
                        return rv.IdentifiableReference;
                    }
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets the date for which this report is relevant
        ///     This will search the Target.IReportedAttribute for any that contain a report Period, and will return null if none
        ///     do.
        /// </summary>
        public ISdmxDate TargetReportPeriod
        {
            get
            {
                foreach (IReferenceValue rv in this._target.ReferenceValues)
                {
                    if (rv.TargetType == TargetType.ReportPeriod)
                    {
                        return rv.ReportPeriod;
                    }
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets the targets that exist in the target object
        /// </summary>
        public ISet<TargetType> Targets
        {
            get
            {
                ISet<TargetType> targets = new HashSet<TargetType>();
                foreach (IReferenceValue rv in this._target.ReferenceValues)
                {
                    targets.Add(rv.TargetType);
                }

                return targets;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                IMetadataReport that = (IMetadataReport)sdmxObject;
                if (!string.Equals(this._id, that.Id))
                {
                    return false;
                }

                if (!this.Equivalent(this._target, that.Target, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._reportedAttributes, that.ReportedAttributes, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsInternal(that);
            }

            return false;
        }

        /// <summary>
        ///     Gets the internal composites.
        /// </summary>
        /// <returns>The set of SDMX objects</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._target, composites);
            this.AddToCompositeSet(this._reportedAttributes, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._id))
            {
                throw new SdmxSemmanticException("Metadata Report must have an Id");
            }

            if (this._target == null)
            {
                throw new SdmxSemmanticException("Metadata Report must have a Target");
            }

            if (!ObjectUtil.ValidCollection(this._reportedAttributes))
            {
                throw new SdmxSemmanticException("Metadata Report must have at least one Reported Attribute");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}