// -----------------------------------------------------------------------
// <copyright file="TargetObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using TargetType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic.TargetType;

    /// <summary>
    ///     The target object core.
    /// </summary>
    [Serializable]
    public class TargetObjectCore : SdmxObjectCore, ITarget
    {
        /// <summary>
        ///     The id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The reference values.
        /// </summary>
        private readonly IList<IReferenceValue> _referenceValues;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TargetObjectCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public TargetObjectCore(IMetadataReport parent, TargetType type)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataReportTarget), parent)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this._referenceValues = new List<IReferenceValue>();
            this._id = type.id;
            if (ObjectUtil.ValidCollection(type.ReferenceValue))
            {
                foreach (ReferenceValueType refValue in type.ReferenceValue)
                {
                    this._referenceValues.Add(new ReferenceValueObjectCore(this, refValue));
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the reference values.
        /// </summary>
        public virtual IList<IReferenceValue> ReferenceValues
        {
            get
            {
                return new List<IReferenceValue>(this._referenceValues);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                ITarget that = (ITarget)sdmxObject;
                if (!string.Equals(this._id, that.Id))
                {
                    return false;
                }

                if (!this.Equivalent(this._referenceValues, that.ReferenceValues, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsInternal(that);
            }

            return false;
        }

        /// <summary>
        ///     Gets the internal composites.
        /// </summary>
        /// <returns>The set of SDMX objects</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._referenceValues, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._id))
            {
                throw new SdmxSemmanticException("Metadata Report must have an Id");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
    }
}