// -----------------------------------------------------------------------
// <copyright file="MetadataSetObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.MetaData.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The metadata set object core.
    /// </summary>
    [Serializable]
    public class MetadataSetObjectCore : SdmxObjectCore, IMetadataSet
    {
        /// <summary>
        ///     The data provider reference.
        /// </summary>
        private readonly ICrossReference _dataProviderReference;

        /// <summary>
        ///     The names
        /// </summary>
        private readonly IList<ITextTypeWrapper> _names = new List<ITextTypeWrapper>();

        /// <summary>
        ///     The publication period.
        /// </summary>
        private readonly object _publicationPeriod;

        /// <summary>
        ///     The publication year.
        /// </summary>
        private readonly ISdmxDate _publicationYear;

        /// <summary>
        ///     The reporting begin date.
        /// </summary>
        private readonly ISdmxDate _reportingBeginDate;

        /// <summary>
        ///     The reporting end date.
        /// </summary>
        private readonly ISdmxDate _reportingEndDate;

        /// <summary>
        ///     The reports.
        /// </summary>
        private readonly IList<IMetadataReport> _reports = new List<IMetadataReport>();

        /// <summary>
        ///     The set identifier
        /// </summary>
        private readonly string _setId;

        /// <summary>
        ///     The structure ref.
        /// </summary>
        private readonly ICrossReference _structureRef;

        /// <summary>
        ///     The valid from date.
        /// </summary>
        private readonly ISdmxDate _validFromDate;

        /// <summary>
        ///     The valid to date.
        /// </summary>
        private readonly ISdmxDate _validToDate;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataSetObjectCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// ///
        /// <exception cref="ArgumentNullException">
        ///     Throws ArgumentNullException.
        /// </exception>
        public MetadataSetObjectCore(IMetadata parent, MetadataSetType createdFrom)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataSet), null)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            this._setId = createdFrom.setID;

            foreach (IDatasetStructureReference structurereference in parent.Header.Structures)
            {
                if (structurereference.Id.Equals(createdFrom.structureRef))
                {
                    this._structureRef = new CrossReferenceImpl(this, structurereference.StructureReference);
                    break;
                }
            }

            if (createdFrom.Name != null)
            {
                this._names = TextTypeUtil.WrapTextTypeV21(createdFrom.Name, this);
            }

            var reportingDate = createdFrom.reportingBeginDate as DateTime?;

            if (reportingDate != null)
            {
                this._reportingBeginDate = new SdmxDateCore(reportingDate, TimeFormatEnumType.DateTime);
            }

            reportingDate = createdFrom.reportingEndDate as DateTime?;

            if (reportingDate != null)
            {
                this._reportingEndDate = new SdmxDateCore(reportingDate, TimeFormatEnumType.DateTime);
            }

            if (createdFrom.publicationYear != null)
            {
                this._publicationYear = new SdmxDateCore(createdFrom.publicationYear, TimeFormatEnumType.DateTime);
            }

            if (createdFrom.validFromDate != null)
            {
                this._validFromDate = new SdmxDateCore(createdFrom.validFromDate, TimeFormatEnumType.DateTime);
            }

            if (createdFrom.validToDate != null)
            {
                this._validToDate = new SdmxDateCore(createdFrom.validToDate, TimeFormatEnumType.DateTime);
            }

            // FUNC Publication Period
            this._publicationPeriod = createdFrom.publicationPeriod;

            if (createdFrom.DataProvider != null)
            {
                this._dataProviderReference = RefUtil.CreateReference(this, createdFrom.DataProvider);
            }

            if (ObjectUtil.ValidCollection(createdFrom.Report))
            {
                foreach (ReportType currentReport in createdFrom.Report)
                {
                    this._reports.Add(new MetadataReportObjectCore(this, currentReport));
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataSetObjectCore" /> class.
        /// </summary>
        /// <param name="metadataSet">The metadata set.</param>
        /// <param name="report">The report.</param>
        private MetadataSetObjectCore(MetadataSetObjectCore metadataSet, IMetadataReport report)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataSet), null)
        {
            this._setId = metadataSet._setId;
            this._structureRef = metadataSet._structureRef;
            this._reportingBeginDate = metadataSet._reportingBeginDate;
            this._reportingEndDate = metadataSet._reportingEndDate;
            this._publicationYear = metadataSet._publicationYear;
            this._validFromDate = metadataSet._validFromDate;
            this._validToDate = metadataSet._validToDate;
            this._publicationPeriod = metadataSet._publicationPeriod;
            this._dataProviderReference = metadataSet._dataProviderReference;
            this._names = metadataSet._names;
            this._reports.Add(report);
        }

        /// <summary>
        ///     Gets the data provider reference.
        /// </summary>
        public virtual ICrossReference DataProviderReference
        {
            get
            {
                return this._dataProviderReference;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets the msd reference.
        /// </summary>
        public virtual ICrossReference MsdReference
        {
            get
            {
                return this._structureRef;
            }
        }

        /// <summary>
        ///     Gets a list of names for this component - will return an empty list if no Names exist.
        ///     <remarks>
        ///         The list is a copy so modifying the returned list will not be reflected in the IdentifiableBean instance
        ///     </remarks>
        /// </summary>
        public IList<ITextTypeWrapper> Names
        {
            get
            {
                return this._names;
            }
        }

        /// <summary>
        ///     Gets the publication period.
        /// </summary>
        public virtual object PublicationPeriod
        {
            get
            {
                return this._publicationPeriod;
            }
        }

        /// <summary>
        ///     Gets the publication year.
        /// </summary>
        public virtual ISdmxDate PublicationYear
        {
            get
            {
                return this._publicationYear;
            }
        }

        /// <summary>
        ///     Gets the reporting begin date.
        /// </summary>
        public virtual ISdmxDate ReportingBeginDate
        {
            get
            {
                return this._reportingBeginDate;
            }
        }

        /// <summary>
        ///     Gets the reporting end date.
        /// </summary>
        public virtual ISdmxDate ReportingEndDate
        {
            get
            {
                return this._reportingEndDate;
            }
        }

        /// <summary>
        ///     Gets the reports.
        /// </summary>
        public virtual IList<IMetadataReport> Reports
        {
            get
            {
                return new List<IMetadataReport>(this._reports);
            }
        }

        /// <summary>
        ///     Gets an identification for the metadata set
        /// </summary>
        public string SetId
        {
            get
            {
                return this._setId;
            }
        }

        /// <summary>
        ///     Gets a list of metadata sets, built from this metadata set and metadata reports.  Each metadata set has one
        ///     metadata report in it.
        /// </summary>
        public IList<IMetadataSet> SplitReports
        {
            get
            {
                var returnList = new List<IMetadataSet>();
                foreach (IMetadataReport report in this._reports)
                {
                    returnList.Add(new MetadataSetObjectCore(this, report));
                }

                return returnList;
            }
        }

        /// <summary>
        ///     Gets the valid from date.
        /// </summary>
        public virtual ISdmxDate ValidFromDate
        {
            get
            {
                return this._validFromDate;
            }
        }

        /// <summary>
        ///     Gets the valid to date.
        /// </summary>
        public virtual ISdmxDate ValidToDate
        {
            get
            {
                return this._validToDate;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                IMetadataSet that = (IMetadataSet)sdmxObject;
                if (!string.Equals(this._setId, that.SetId))
                {
                    return false;
                }

                if (!this.Equivalent(this._structureRef, that.MsdReference))
                {
                    return false;
                }

                if (!Equals(this._reportingBeginDate, that.ReportingBeginDate))
                {
                    return false;
                }

                if (!Equals(this._reportingEndDate, that.ReportingEndDate))
                {
                    return false;
                }

                if (!Equals(this._publicationYear, that.PublicationYear))
                {
                    return false;
                }

                if (!Equals(this._validFromDate, that.ValidFromDate))
                {
                    return false;
                }

                if (!Equals(this._validToDate, that.ValidToDate))
                {
                    return false;
                }

                if (!Equals(this._publicationPeriod, (string)that.PublicationPeriod))
                {
                    return false;
                }

                if (!Equals(this._dataProviderReference, that.DataProviderReference))
                {
                    return false;
                }

                if (!this.Equivalent(this._names, that.Names, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._reports, that.Reports, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsInternal(that);
            }

            return false;
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var set = obj as IMetadataSet;
            if (set != null)
            {
                IMetadataSet that = set;
                return that.DeepEquals(this, true);
            }

            return false;
        }

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this._setId != null ? this._setId.GetHashCode() : 0;
                hashCode = (hashCode * 397)
                           ^ (this._dataProviderReference != null ? this._dataProviderReference.GetHashCode() : 0);
                hashCode = (hashCode * 397)
                           ^ (this._publicationPeriod != null ? this._publicationPeriod.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._publicationYear != null ? this._publicationYear.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._reports != null ? this._reports.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._structureRef != null ? this._structureRef.GetHashCode() : 0);
                hashCode = (hashCode * 397)
                           ^ (this._reportingBeginDate != null ? this._reportingBeginDate.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._reportingEndDate != null ? this._reportingEndDate.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._validFromDate != null ? this._validFromDate.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._validToDate != null ? this._validToDate.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._names != null ? this._names.GetHashCode() : 0);
                return hashCode;
            }
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this._setId;
        }

        /// <summary>
        ///     Equals the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>True if equals.</returns>
        protected bool Equals(MetadataSetObjectCore other)
        {
            if (other == null)
            {
                return false;
            }

            return string.Equals(this._setId, other._setId)
                   && Equals(this._dataProviderReference, other._dataProviderReference)
                   && Equals(this._publicationPeriod, other._publicationPeriod)
                   && Equals(this._publicationYear, other._publicationYear) && Equals(this._reports, other._reports)
                   && Equals(this._structureRef, other._structureRef)
                   && Equals(this._reportingBeginDate, other._reportingBeginDate)
                   && Equals(this._reportingEndDate, other._reportingEndDate)
                   && Equals(this._validFromDate, other._validFromDate) && Equals(this._validToDate, other._validToDate)
                   && Equals(this._names, other._names);
        }

        /// <summary>
        ///     Gets the internal composites.
        /// </summary>
        /// <returns>The set of SDMX objects</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._names, composites);
            this.AddToCompositeSet(this._reports, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            string setId = this._setId ?? string.Empty;

            if (!ObjectUtil.ValidCollection(this._reports))
            {
                throw new SdmxSemmanticException("Metadata Set " + setId + "requires at least one Report");
            }

            if (this._structureRef == null)
            {
                throw new SdmxSemmanticException("Metadata Set " + setId + "requires a reference to an MSD");
            }

            if (this._structureRef.TargetReference != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd))
            {
                throw new SdmxSemmanticException("Metadata Set " + setId + "reference must be a reference to an MSD");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}