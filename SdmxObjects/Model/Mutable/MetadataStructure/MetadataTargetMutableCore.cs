// -----------------------------------------------------------------------
// <copyright file="MetadataTargetMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The metadata target mutable core.
    /// </summary>
    [Serializable]
    public class MetadataTargetMutableCore : IdentifiableMutableCore, IMetadataTargetMutableObject
    {
        /// <summary>
        ///     The _identifiable target.
        /// </summary>
        private readonly IList<IIdentifiableTargetMutableObject> _identifiableTarget;

        /// <summary>
        ///     The _constraint content target.
        /// </summary>
        private IConstraintContentTargetMutableObject _constraintContentTarget;

        /// <summary>
        ///     The _data set target.
        /// </summary>
        private IDataSetTargetMutableObject _dataSetTarget;

        /// <summary>
        ///     The _key descriptor values target.
        /// </summary>
        private IKeyDescriptorValuesTargetMutableObject _keyDescriptorValuesTarget;

        /// <summary>
        ///     The _report period target.
        /// </summary>
        private IReportPeriodTargetMutableObject _reportPeriodTarget;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public MetadataTargetMutableCore(IMetadataTarget objTarget)
            : base(objTarget)
        {
            this._identifiableTarget = new List<IIdentifiableTargetMutableObject>();
            if (objTarget.KeyDescriptorValuesTarget != null)
            {
                this._keyDescriptorValuesTarget =
                    new KeyDescriptorValuesTargetMutableCore(objTarget.KeyDescriptorValuesTarget);
            }

            if (objTarget.DataSetTarget != null)
            {
                this._dataSetTarget = new DataSetTargetMutableCore(objTarget.DataSetTarget);
            }

            if (objTarget.ReportPeriodTarget != null)
            {
                this._reportPeriodTarget = new ReportPeriodTargetMutableCore(objTarget.ReportPeriodTarget);
            }

            if (objTarget.ConstraintContentTarget != null)
            {
                this._constraintContentTarget = new ConstraintContentTargetMutableCore(
                    objTarget.ConstraintContentTarget);
            }

            if (objTarget.IdentifiableTarget != null)
            {
                foreach (IIdentifiableTarget identifiableTarget in objTarget.IdentifiableTarget)
                {
                    this._identifiableTarget.Add(new IdentifiableTargetMutableCore(identifiableTarget));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetMutableCore" /> class.
        /// </summary>
        public MetadataTargetMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataTarget))
        {
            this._identifiableTarget = new List<IIdentifiableTargetMutableObject>();
        }

        /// <summary>
        ///     Gets or sets the constraint content target.
        /// </summary>
        public virtual IConstraintContentTargetMutableObject ConstraintContentTarget
        {
            get
            {
                return this._constraintContentTarget;
            }

            set
            {
                this._constraintContentTarget = value;
            }
        }

        /// <summary>
        ///     Gets or sets the data set target.
        /// </summary>
        public virtual IDataSetTargetMutableObject DataSetTarget
        {
            get
            {
                return this._dataSetTarget;
            }

            set
            {
                this._dataSetTarget = value;
            }
        }

        /// <summary>
        ///     Gets the identifiable target.
        /// </summary>
        public virtual IList<IIdentifiableTargetMutableObject> IdentifiableTarget
        {
            get
            {
                return this._identifiableTarget;
            }
        }

        /// <summary>
        ///     Gets or sets the key descriptor values target.
        /// </summary>
        public virtual IKeyDescriptorValuesTargetMutableObject KeyDescriptorValuesTarget
        {
            get
            {
                return this._keyDescriptorValuesTarget;
            }

            set
            {
                this._keyDescriptorValuesTarget = value;
            }
        }

        /// <summary>
        ///     Gets or sets the report period target.
        /// </summary>
        public virtual IReportPeriodTargetMutableObject ReportPeriodTarget
        {
            get
            {
                return this._reportPeriodTarget;
            }

            set
            {
                this._reportPeriodTarget = value;
            }
        }
    }
}