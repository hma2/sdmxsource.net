// -----------------------------------------------------------------------
// <copyright file="KeyDescriptorValuesTargetMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The key descriptor values target mutable core.
    /// </summary>
    public class KeyDescriptorValuesTargetMutableCore : IdentifiableMutableCore, IKeyDescriptorValuesTargetMutableObject
    {
        /// <summary>
        ///     The text type.
        /// </summary>
        private TextType _textType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyDescriptorValuesTargetMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public KeyDescriptorValuesTargetMutableCore(IKeyDescriptorValuesTarget objTarget)
            : base(objTarget)
        {
            this._textType = objTarget.TextType;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyDescriptorValuesTargetMutableCore" /> class.
        /// </summary>
        public KeyDescriptorValuesTargetMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptorValuesTarget))
        {
        }

        /// <summary>
        ///     Gets or sets the text type.
        /// </summary>
        public virtual TextType TextType
        {
            get
            {
                return this._textType;
            }

            set
            {
                this._textType = value;
            }
        }
    }
}