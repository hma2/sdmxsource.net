// -----------------------------------------------------------------------
// <copyright file="MetadataStructureDefinitionMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure;

    /// <summary>
    ///     The metadata structure definition mutable core.
    /// </summary>
    [Serializable]
    public class MetadataStructureDefinitionMutableCore : MaintainableMutableCore<IMetadataStructureDefinitionObject>, 
                                                          IMetadataStructureDefinitionMutableObject
    {
        /// <summary>
        ///     The metadata targets.
        /// </summary>
        private readonly IList<IMetadataTargetMutableObject> _metadataTargets;

        /// <summary>
        ///     The report structures.
        /// </summary>
        private readonly IList<IReportStructureMutableObject> _reportStructures;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataStructureDefinitionMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public MetadataStructureDefinitionMutableCore(IMetadataStructureDefinitionObject objTarget)
            : base(objTarget)
        {
            this._metadataTargets = new List<IMetadataTargetMutableObject>();
            this._reportStructures = new List<IReportStructureMutableObject>();
            if (objTarget.MetadataTargets != null)
            {
                foreach (IMetadataTarget currentMt in objTarget.MetadataTargets)
                {
                    this._metadataTargets.Add(new MetadataTargetMutableCore(currentMt));
                }
            }

            if (objTarget.ReportStructures != null)
            {
                foreach (IReportStructure currentReportStructure in objTarget.ReportStructures)
                {
                    this._reportStructures.Add(new ReportStructureMutableCore(currentReportStructure));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataStructureDefinitionMutableCore" /> class.
        /// </summary>
        public MetadataStructureDefinitionMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd))
        {
            this._metadataTargets = new List<IMetadataTargetMutableObject>();
            this._reportStructures = new List<IReportStructureMutableObject>();
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override IMetadataStructureDefinitionObject ImmutableInstance
        {
            get
            {
                return new MetadataStructureDefinitionObjectCore(this);
            }
        }

        /// <summary>
        ///     Gets the metadata targets.
        /// </summary>
        public virtual IList<IMetadataTargetMutableObject> MetadataTargets
        {
            get
            {
                return this._metadataTargets;
            }
        }

        /// <summary>
        ///     Gets the report structures.
        /// </summary>
        public virtual IList<IReportStructureMutableObject> ReportStructures
        {
            get
            {
                return this._reportStructures;
            }
        }
    }
}