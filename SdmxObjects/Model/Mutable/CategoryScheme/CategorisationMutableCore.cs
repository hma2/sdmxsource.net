// -----------------------------------------------------------------------
// <copyright file="CategorisationMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;

    /// <summary>
    ///     The categorisation mutable core.
    /// </summary>
    [Serializable]
    public class CategorisationMutableCore : MaintainableMutableCore<ICategorisationObject>, 
                                             ICategorisationMutableObject
    {
        /// <summary>
        ///     The icategory ref.
        /// </summary>
        private IStructureReference _categoryRef;

        /// <summary>
        ///     The structure reference.
        /// </summary>
        private IStructureReference _structureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationMutableCore" /> class.
        /// </summary>
        public CategorisationMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public CategorisationMutableCore(ICategorisationObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            if (objTarget.CategoryReference != null)
            {
                this._categoryRef = objTarget.CategoryReference.CreateMutableInstance();
            }

            if (objTarget.StructureReference != null)
            {
                this._structureReference = objTarget.StructureReference.CreateMutableInstance();
            }
        }

        /// <summary>
        ///     Gets or sets the category reference.
        /// </summary>
        public virtual IStructureReference CategoryReference
        {
            get
            {
                return this._categoryRef;
            }

            set
            {
                this._categoryRef = value;
            }
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override ICategorisationObject ImmutableInstance
        {
            get
            {
                return new CategorisationObjectCore(this);
            }
        }

        /// <summary>
        ///     Gets or sets the structure reference.
        /// </summary>
        public virtual IStructureReference StructureReference
        {
            get
            {
                return this._structureReference;
            }

            set
            {
                this._structureReference = value;
            }
        }
    }
}