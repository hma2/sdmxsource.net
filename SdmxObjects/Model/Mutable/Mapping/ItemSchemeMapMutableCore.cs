// -----------------------------------------------------------------------
// <copyright file="ItemSchemeMapMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;

    /// <summary>
    ///     The item scheme map mutable core.
    /// </summary>
    [Serializable]
    public abstract class ItemSchemeMapMutableCore : SchemeMapMutableCore
    {
        /// <summary>
        ///     The items.
        /// </summary>
        private readonly IList<IItemMapMutableObject> _items;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeMapMutableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        protected ItemSchemeMapMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
            this._items = new List<IItemMapMutableObject>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeMapMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        protected ItemSchemeMapMutableCore(IItemSchemeMapObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            this._items = new List<IItemMapMutableObject>();
            if (objTarget.Items != null)
            {
                foreach (IItemMap item in objTarget.Items)
                {
                    this._items.Add(new ItemMapMutableCore(item));
                }
            }
        }

        /// <summary>
        ///     Gets the items.
        /// </summary>
        public IList<IItemMapMutableObject> Items
        {
            get
            {
                return this._items;
            }
        }

        /// <summary>
        ///     The add item.
        /// </summary>
        /// <param name="item">
        ///     The item.
        /// </param>
        public void AddItem(IItemMapMutableObject item)
        {
            this._items.Add(item);
        }
    }
}