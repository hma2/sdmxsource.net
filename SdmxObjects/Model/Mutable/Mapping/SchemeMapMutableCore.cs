// -----------------------------------------------------------------------
// <copyright file="SchemeMapMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The scheme map mutable core.
    /// </summary>
    [Serializable]
    public abstract class SchemeMapMutableCore : NameableMutableCore, ISchemeMapMutableObject
    {
        /// <summary>
        ///     The source ref.
        /// </summary>
        private IStructureReference _sourceRef;

        /// <summary>
        ///     The target ref.
        /// </summary>
        private IStructureReference _targetRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemeMapMutableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        protected SchemeMapMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemeMapMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        protected SchemeMapMutableCore(ISchemeMapObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            if (objTarget.SourceRef != null)
            {
                this._sourceRef = objTarget.SourceRef.CreateMutableInstance();
            }

            if (objTarget.TargetRef != null)
            {
                this._targetRef = objTarget.TargetRef.CreateMutableInstance();
            }
        }

        /// <summary>
        ///     Gets or sets the source ref.
        /// </summary>
        public virtual IStructureReference SourceRef
        {
            get
            {
                return this._sourceRef;
            }

            set
            {
                this._sourceRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the target ref.
        /// </summary>
        public virtual IStructureReference TargetRef
        {
            get
            {
                return this._targetRef;
            }

            set
            {
                this._targetRef = value;
            }
        }
    }
}