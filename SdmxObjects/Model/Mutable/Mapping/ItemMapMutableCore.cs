// -----------------------------------------------------------------------
// <copyright file="ItemMapMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The item map mutable core.
    /// </summary>
    [Serializable]
    public class ItemMapMutableCore : MutableCore, IItemMapMutableObject
    {
        /// <summary>
        ///     The source id.
        /// </summary>
        private string _sourceId;

        /// <summary>
        ///     The target id.
        /// </summary>
        private string _targetId;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemMapMutableCore" /> class.
        /// </summary>
        public ItemMapMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ItemMap))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemMapMutableCore" /> class.
        /// </summary>
        /// <param name="itemMap">
        ///     The itemMap.
        /// </param>
        public ItemMapMutableCore(IItemMap itemMap)
            : base(itemMap)
        {
            this.SourceId = itemMap.SourceId;
            this.TargetId = itemMap.TargetId;
        }

        /// <summary>
        ///     Gets or sets the source id.
        /// </summary>
        public string SourceId
        {
            get
            {
                return this._sourceId;
            }

            set
            {
                this._sourceId = value;
            }
        }

        /// <summary>
        ///     Gets or sets the target id.
        /// </summary>
        public string TargetId
        {
            get
            {
                return this._targetId;
            }

            set
            {
                this._targetId = value;
            }
        }
    }
}