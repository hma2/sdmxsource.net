// -----------------------------------------------------------------------
// <copyright file="RelatedStructuresMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The related structures mutable core.
    /// </summary>
    [Serializable]
    public class RelatedStructuresMutableCore : MutableCore, IRelatedStructuresMutableObject
    {
        /// <summary>
        ///     The category scheme ref.
        /// </summary>
        private IList<IStructureReference> _categorySchemeRef;

        /// <summary>
        ///     The concept scheme ref.
        /// </summary>
        private IList<IStructureReference> _conceptSchemeRef;

        /// <summary>
        ///     The hier codelist ref.
        /// </summary>
        private IList<IStructureReference> _hierCodelistRef;

        /// <summary>
        ///     The key family ref.
        /// </summary>
        private IList<IStructureReference> _keyFamilyRef;

        /// <summary>
        ///     The metadata structure ref.
        /// </summary>
        private IList<IStructureReference> _metadataStructureRef;

        /// <summary>
        ///     The org scheme ref.
        /// </summary>
        private IList<IStructureReference> _orgSchemeRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RelatedStructuresMutableCore" /> class.
        /// </summary>
        public RelatedStructuresMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.RelatedStructures))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RelatedStructuresMutableCore" /> class.
        /// </summary>
        /// <param name="relStrucType">
        ///     The rel struc type.
        /// </param>
        public RelatedStructuresMutableCore(IRelatedStructures relStrucType)
            : base(relStrucType)
        {
            this._keyFamilyRef = ConvertList(relStrucType.DataStructureRef);
            this._metadataStructureRef = ConvertList(relStrucType.MetadataStructureRef);
            this._conceptSchemeRef = ConvertList(relStrucType.ConceptSchemeRef);
            this._categorySchemeRef = ConvertList(relStrucType.CategorySchemeRef);
            this._orgSchemeRef = ConvertList(relStrucType.OrgSchemeRef);
            this._hierCodelistRef = ConvertList(relStrucType.HierCodelistRef);
        }

        /// <summary>
        ///     Gets the category scheme ref.
        /// </summary>
        public virtual IList<IStructureReference> CategorySchemeRef
        {
            get
            {
                return this._categorySchemeRef;
            }
        }

        /// <summary>
        ///     Gets the concept scheme ref.
        /// </summary>
        public virtual IList<IStructureReference> ConceptSchemeRef
        {
            get
            {
                return this._conceptSchemeRef;
            }
        }

        /// <summary>
        ///     Gets the data structure ref.
        /// </summary>
        public virtual IList<IStructureReference> DataStructureRef
        {
            get
            {
                return this._keyFamilyRef;
            }
        }

        /// <summary>
        ///     Gets the hierarchical codelist reference.
        /// </summary>
        public virtual IList<IStructureReference> HierCodelistRef
        {
            get
            {
                return new ReadOnlyCollection<IStructureReference>(this._hierCodelistRef);
            }
        }

        /// <summary>
        ///     Gets the metadata structure ref.
        /// </summary>
        public virtual IList<IStructureReference> MetadataStructureRef
        {
            get
            {
                return new ReadOnlyCollection<IStructureReference>(this._metadataStructureRef);
            }
        }

        /// <summary>
        ///     Gets the org scheme ref.
        /// </summary>
        public virtual IList<IStructureReference> OrgSchemeRef
        {
            get
            {
                return new ReadOnlyCollection<IStructureReference>(this._orgSchemeRef);
            }
        }

        /// <summary>
        ///     The convert list.
        /// </summary>
        /// <param name="inputList">
        ///     The input list.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{IStructureReference}" /> .
        /// </returns>
        private static IList<IStructureReference> ConvertList(IEnumerable<ICrossReference> inputList)
        {
            IList<IStructureReference> returnList = new List<IStructureReference>();
            if (inputList != null)
            {
                foreach (ICrossReference currentCrossReference in inputList)
                {
                    returnList.Add(currentCrossReference.CreateMutableInstance());
                }
            }

            return returnList;
        }
    }
}