// -----------------------------------------------------------------------
// <copyright file="ComponentMapMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The component map mutable core.
    /// </summary>
    [Serializable]
    public class ComponentMapMutableCore : AnnotableMutableCore, IComponentMapMutableObject
    {
        /// <summary>
        ///     The map concept ref.
        /// </summary>
        private string _mapConceptRef;

        /// <summary>
        ///     The map target concept ref.
        /// </summary>
        private string _mapTargetConceptRef;

        /// <summary>
        ///     The rep map ref.
        /// </summary>
        private IRepresentationMapRefMutableObject _repMapRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMapMutableCore" /> class.
        /// </summary>
        public ComponentMapMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ComponentMap))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMapMutableCore" /> class.
        /// </summary>
        /// <param name="compMap">
        ///     The comp map.
        /// </param>
        public ComponentMapMutableCore(IComponentMapObject compMap)
            : base(compMap)
        {
            if (compMap.MapConceptRef != null)
            {
                this._mapConceptRef = compMap.MapConceptRef;
            }

            if (compMap.MapTargetConceptRef != null)
            {
                this._mapTargetConceptRef = compMap.MapTargetConceptRef;
            }

            if (compMap.RepMapRef != null)
            {
                this._repMapRef = new RepresentationMapRefMutableCore(compMap.RepMapRef);
            }
        }

        /// <summary>
        ///     Gets or sets the map concept ref.
        /// </summary>
        public virtual string MapConceptRef
        {
            get
            {
                return this._mapConceptRef;
            }

            set
            {
                this._mapConceptRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the map target concept ref.
        /// </summary>
        public virtual string MapTargetConceptRef
        {
            get
            {
                return this._mapTargetConceptRef;
            }

            set
            {
                this._mapTargetConceptRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the rep map ref.
        /// </summary>
        public virtual IRepresentationMapRefMutableObject RepMapRef
        {
            get
            {
                return this._repMapRef;
            }

            set
            {
                this._repMapRef = value;
            }
        }
    }
}