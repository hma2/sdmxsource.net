// -----------------------------------------------------------------------
// <copyright file="DataStructureMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;

    /// <summary>
    ///     The data structure mutable core.
    /// </summary>
    [Serializable]
    public class DataStructureMutableCore : MaintainableMutableCore<IDataStructureObject>, IDataStructureMutableObject
    {
        /// <summary>
        ///     The _groups.
        /// </summary>
        private readonly IList<IGroupMutableObject> _groups;

        /// <summary>
        ///     The _attribtue list.
        /// </summary>
        private IAttributeListMutableObject _attributeList;

        /// <summary>
        ///     The _dimension list.
        /// </summary>
        private IDimensionListMutableObject _dimensionList;

        /// <summary>
        ///     The _measure list.
        /// </summary>
        private IMeasureListMutableObject _measureList;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureMutableCore" /> class.
        /// </summary>
        public DataStructureMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd))
        {
            this._groups = new List<IGroupMutableObject>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The source immutable object
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        public DataStructureMutableCore(IDataStructureObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            this._groups = new List<IGroupMutableObject>();
            if (objTarget.Groups != null)
            {
                foreach (IGroup group in objTarget.Groups)
                {
                    this._groups.Add(new GroupMutableCore(group));
                }
            }

            if (objTarget.DimensionList != null)
            {
                this._dimensionList = new DimensionListMutableCore(objTarget.DimensionList);
            }

            if (objTarget.AttributeList != null)
            {
                this._attributeList = new AttributeListMutableCore(objTarget.AttributeList);
            }

            if (objTarget.MeasureList != null)
            {
                this._measureList = new MeasureListMutableCore(objTarget.MeasureList);
            }
        }

        /// <summary>
        ///     Gets or sets the attribute list.
        /// </summary>
        public virtual IAttributeListMutableObject AttributeList
        {
            get
            {
                return this._attributeList;
            }

            set
            {
                this._attributeList = value;
            }
        }

        /// <summary>
        ///     Gets the attributes.this may return null or an empty list if none exist
        /// </summary>
        /// <value>
        ///     The attributes.
        /// </value>
        public virtual IList<IAttributeMutableObject> Attributes
        {
            get
            {
                if (this._attributeList != null)
                {
                    return this._attributeList.Attributes;
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets or sets the dimension list.
        /// </summary>
        public virtual IDimensionListMutableObject DimensionList
        {
            get
            {
                return this._dimensionList;
            }

            set
            {
                this._dimensionList = value;
            }
        }

        /// <summary>
        ///     Gets the dimensions.
        /// </summary>
        public virtual IList<IDimensionMutableObject> Dimensions
        {
            get
            {
                if (this._dimensionList != null)
                {
                    return this._dimensionList.Dimensions;
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets the groups.
        /// </summary>
        public virtual IList<IGroupMutableObject> Groups
        {
            get
            {
                return this._groups;
            }
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override IDataStructureObject ImmutableInstance
        {
            get
            {
                return new DataStructureObjectCore(this);
            }
        }

        /// <summary>
        ///     Gets or sets the measure list.
        /// </summary>
        public virtual IMeasureListMutableObject MeasureList
        {
            get
            {
                return this._measureList;
            }

            set
            {
                this._measureList = value;
            }
        }

        /// <summary>
        ///     Gets or sets the primary measure.
        /// </summary>
        public virtual IPrimaryMeasureMutableObject PrimaryMeasure
        {
            get
            {
                var measureListMutableObject = this._measureList;
                if (measureListMutableObject != null)
                {
                    return measureListMutableObject.PrimaryMeasure;
                }

                return null;
            }

            set
            {
                if (this._measureList == null)
                {
                    this._measureList = new MeasureListMutableCore();
                }

                this._measureList.PrimaryMeasure = value;
            }
        }

        /// <summary>
        ///     The add attribute.
        /// </summary>
        /// <param name="attribute">
        ///     The attribute.
        /// </param>
        public virtual void AddAttribute(IAttributeMutableObject attribute)
        {
            if (this._attributeList == null)
            {
                this._attributeList = new AttributeListMutableCore();
            }

            this._attributeList.AddAttribute(attribute);
        }

        /// <summary>
        ///     Adds the attribute.
        /// </summary>
        /// <param name="conceptRef">The concept reference</param>
        /// <param name="codelistRef">The code list reference</param>
        /// <returns>
        ///     The attribute
        /// </returns>
        public IAttributeMutableObject AddAttribute(IStructureReference conceptRef, IStructureReference codelistRef)
        {
            IAttributeMutableObject newAttribute = new AttributeMutableCore();
            newAttribute.ConceptRef = conceptRef;
            if (codelistRef != null)
            {
                IRepresentationMutableObject representation = new RepresentationMutableCore();
                representation.Representation = codelistRef;
                newAttribute.Representation = representation;
            }

            this.AddAttribute(newAttribute);
            return newAttribute;
        }

        /// <summary>
        ///     The add dimension.
        /// </summary>
        /// <param name="dimension">
        ///     The dimension.
        /// </param>
        public virtual void AddDimension(IDimensionMutableObject dimension)
        {
            if (this._dimensionList == null)
            {
                this._dimensionList = new DimensionListMutableCore();
            }

            this._dimensionList.AddDimension(dimension);
        }

        /// <summary>
        ///     Adds the dimension.
        /// </summary>
        /// <param name="conceptRef">The concept preference.</param>
        /// <param name="codelistRef">The codelist preference.</param>
        /// <returns>
        ///     The dimension that was created.
        /// </returns>
        public IDimensionMutableObject AddDimension(IStructureReference conceptRef, IStructureReference codelistRef)
        {
            IDimensionMutableObject newDimension = new DimensionMutableCore();
            newDimension.ConceptRef = conceptRef;
            if (codelistRef != null)
            {
                IRepresentationMutableObject representation = new RepresentationMutableCore();
                representation.Representation = codelistRef;
                newDimension.Representation = representation;
            }

            this.AddDimension(newDimension);
            return newDimension;
        }

        /// <summary>
        ///     The add group.
        /// </summary>
        /// <param name="group">
        ///     The group.
        /// </param>
        public virtual void AddGroup(IGroupMutableObject group)
        {
            this._groups.Add(group);
        }

        /// <summary>
        ///     Adds the primary measure.
        /// </summary>
        /// <param name="conceptRef">The concept preference.</param>
        /// <returns>
        ///     The primary measure created.
        /// </returns>
        public IPrimaryMeasureMutableObject AddPrimaryMeasure(IStructureReference conceptRef)
        {
            IPrimaryMeasureMutableObject primaryMeasure = new PrimaryMeasureMutableCore();
            primaryMeasure.ConceptRef = conceptRef;
            this.PrimaryMeasure = primaryMeasure;
            return primaryMeasure;
        }

        /// <summary>
        ///     Gets the attribute.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns>
        ///     Returns the attribute with the given id, or null if there is no match.
        /// </returns>
        public virtual IAttributeMutableObject GetAttribute(string id)
        {
            return this.GetComponentById(this.Attributes, id);
        }

        /// <summary>
        ///     Gets the dimension.
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns>
        ///     Returns the dimension with the given id, or null if there is no match.
        /// </returns>
        public virtual IDimensionMutableObject GetDimension(string id)
        {
            return this.GetComponentById(this.Dimensions, id);
        }

        /// <summary>
        ///     Removes the component  (dimension or attribute) with the given id
        /// </summary>
        /// <param name="id">The unique identifier.</param>
        /// <returns>
        ///     this instance
        /// </returns>
        public virtual IDataStructureMutableObject RemoveComponent(string id)
        {
            this.RemoveComponent(this.Dimensions, id);
            this.RemoveComponent(this.Attributes, id);
            return this;
        }

        /// <summary>
        ///     Gets the component by identifier.
        /// </summary>
        /// <typeparam name="T">The generic type</typeparam>
        /// <param name="comps">The comps.</param>
        /// <param name="removeId">The remove identifier.</param>
        /// <returns>The component</returns>
        private T GetComponentById<T>(IList<T> comps, string removeId) where T : IComponentMutableObject
        {
            if (comps == null)
            {
                return default(T);
            }

            foreach (T currentComponent in comps)
            {
                if (currentComponent.Id != null && currentComponent.Id.Equals(removeId))
                {
                    return currentComponent;
                }
            }

            return default(T);
        }

        /// <summary>
        ///     Removes the component.
        /// </summary>
        /// <typeparam name="T">The generic type</typeparam>
        /// <param name="comps">The comps.</param>
        /// <param name="removeId">The remove identifier.</param>
        private void RemoveComponent<T>(IList<T> comps, string removeId) where T : IComponentMutableObject
        {
            if (comps == null)
            {
                return;
            }

            T toRemove = this.GetComponentById(comps, removeId);
            if (!EqualityComparer<T>.Default.Equals(toRemove, default(T)))
            {
                comps.Remove(toRemove);
            }
        }
    }
}