// -----------------------------------------------------------------------
// <copyright file="AttributeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The attribute mutable core.
    /// </summary>
    [Serializable]
    public class AttributeMutableCore : ComponentMutableCore, IAttributeMutableObject
    {
        /// <summary>
        ///     The concept roles.
        /// </summary>
        private readonly IList<IStructureReference> _conceptRoles;

        /// <summary>
        ///     The dimension reference.
        /// </summary>
        private readonly IList<string> _dimensionReferences;

        /// <summary>
        ///     The assignment status.
        /// </summary>
        private string _assignmentStatus;

        /// <summary>
        ///     The attachment group.
        /// </summary>
        private string _attachmentGroup;

        /// <summary>
        ///     The attachment level.
        /// </summary>
        private AttributeAttachmentLevel _attachmentLevel;

        /// <summary>
        ///     The primary measure reference.
        /// </summary>
        private string _primaryMeasureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeMutableCore" /> class.
        /// </summary>
        public AttributeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute))
        {
            this._dimensionReferences = new List<string>();
            this._conceptRoles = new List<IStructureReference>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The agencySchemeMutable target.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        public AttributeMutableCore(IAttributeObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            this._dimensionReferences = new List<string>();
            this._conceptRoles = new List<IStructureReference>();
            this._attachmentLevel = objTarget.AttachmentLevel;
            this._assignmentStatus = objTarget.AssignmentStatus;
            this._attachmentGroup = objTarget.AttachmentGroup;
            this._dimensionReferences = objTarget.DimensionReferences;
            this._primaryMeasureReference = objTarget.PrimaryMeasureReference;
            if (objTarget.ConceptRoles != null)
            {
                foreach (ICrossReference currentConceptRole in objTarget.ConceptRoles)
                {
                    this._conceptRoles.Add(currentConceptRole.CreateMutableInstance());
                }
            }
        }

        /// <summary>
        ///     Gets or sets the assignment status.
        /// </summary>
        public string AssignmentStatus
        {
            get
            {
                return this._assignmentStatus;
            }

            set
            {
                this._assignmentStatus = value;
            }
        }

        /// <summary>
        ///     Gets or sets the attachment group.
        /// </summary>
        public string AttachmentGroup
        {
            get
            {
                return this._attachmentGroup;
            }

            set
            {
                this._attachmentGroup = value;
            }
        }

        /// <summary>
        ///     Gets or sets the attachment level.
        /// </summary>
        public AttributeAttachmentLevel AttachmentLevel
        {
            get
            {
                return this._attachmentLevel;
            }

            set
            {
                this._attachmentLevel = value;
            }
        }

        /// <summary>
        ///     Gets the concept roles.
        /// </summary>
        public IList<IStructureReference> ConceptRoles
        {
            get
            {
                return this._conceptRoles;
            }
        }

        /// <summary>
        ///     Gets the dimension reference.
        /// </summary>
        public IList<string> DimensionReferences
        {
            get
            {
                return this._dimensionReferences;
            }
        }

        /// <summary>
        ///     Gets or sets the primary measure reference.
        /// </summary>
        public string PrimaryMeasureReference
        {
            get
            {
                return this._primaryMeasureReference;
            }

            set
            {
                this._primaryMeasureReference = value;
            }
        }

        /// <summary>
        ///     Adds a concept Role to the existing list of concept roles
        /// </summary>
        /// <param name="structureReference">The structure reference.</param>
        public void AddConceptRole(IStructureReference structureReference)
        {
            this._conceptRoles.Add(structureReference);
        }
    }
}