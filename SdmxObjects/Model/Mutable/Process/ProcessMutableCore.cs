// -----------------------------------------------------------------------
// <copyright file="ProcessMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process;

    /// <summary>
    ///     The process mutable core.
    /// </summary>
    [Serializable]
    public class ProcessMutableCore : MaintainableMutableCore<IProcessObject>, IProcessMutableObject
    {
        /// <summary>
        ///     The process steps.
        /// </summary>
        private readonly IList<IProcessStepMutableObject> _processSteps;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessMutableCore" /> class.
        /// </summary>
        public ProcessMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Process))
        {
            this._processSteps = new List<IProcessStepMutableObject>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public ProcessMutableCore(IProcessObject objTarget)
            : base(objTarget)
        {
            this._processSteps = new List<IProcessStepMutableObject>();

            // make into mutable objTarget list
            if (objTarget.ProcessSteps != null)
            {
                foreach (IProcessStepObject processStepObject in objTarget.ProcessSteps)
                {
                    this.AddProcessStep(new ProcessStepMutableCore(processStepObject));
                }
            }
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override IProcessObject ImmutableInstance
        {
            get
            {
                return new ProcessObjectCore(this);
            }
        }

        /// <summary>
        ///     Gets the process steps.
        /// </summary>
        public IList<IProcessStepMutableObject> ProcessSteps
        {
            get
            {
                return this._processSteps;
            }
        }

        /// <summary>
        ///     The add process step.
        /// </summary>
        /// <param name="processStep">
        ///     The process step.
        /// </param>
        public void AddProcessStep(IProcessStepMutableObject processStep)
        {
            this._processSteps.Add(processStep);
        }
    }
}