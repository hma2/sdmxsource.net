// -----------------------------------------------------------------------
// <copyright file="ProcessStepMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The process step mutable core.
    /// </summary>
    [Serializable]
    public class ProcessStepMutableCore : NameableMutableCore, IProcessStepMutableObject
    {
        /// <summary>
        ///     The _input.
        /// </summary>
        private readonly IList<IInputOutputMutableObject> _input;

        /// <summary>
        ///     The _output.
        /// </summary>
        private readonly IList<IInputOutputMutableObject> _output;

        /// <summary>
        ///     The _process steps.
        /// </summary>
        private readonly IList<IProcessStepMutableObject> _processSteps;

        /// <summary>
        ///     The _transitions.
        /// </summary>
        private readonly IList<ITransitionMutableObject> _transitions;

        /// <summary>
        ///     The _computation.
        /// </summary>
        private IComputationMutableObject _computation;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepMutableCore" /> class.
        /// </summary>
        public ProcessStepMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProcessStep))
        {
            this._input = new List<IInputOutputMutableObject>();
            this._output = new List<IInputOutputMutableObject>();
            this._transitions = new List<ITransitionMutableObject>();
            this._processSteps = new List<IProcessStepMutableObject>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepMutableCore" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        public ProcessStepMutableCore(IProcessStepObject objTarget)
            : base(objTarget)
        {
            this._input = new List<IInputOutputMutableObject>();
            this._output = new List<IInputOutputMutableObject>();
            this._transitions = new List<ITransitionMutableObject>();
            this._processSteps = new List<IProcessStepMutableObject>();

            if (objTarget.Input != null)
            {
                foreach (IInputOutputObject currentIo in objTarget.Input)
                {
                    this._input.Add(new InputOutputMutableCore(currentIo));
                }
            }

            if (objTarget.Output != null)
            {
                foreach (IInputOutputObject currentIo0 in objTarget.Output)
                {
                    this._output.Add(new InputOutputMutableCore(currentIo0));
                }
            }

            // make into mutable objTarget lists
            if (objTarget.ProcessSteps != null)
            {
                foreach (IProcessStepObject processStepObject in objTarget.ProcessSteps)
                {
                    this.AddProcessStep(new ProcessStepMutableCore(processStepObject));
                }
            }

            if (objTarget.Transitions != null)
            {
                foreach (ITransition t in objTarget.Transitions)
                {
                    this.AddTransition(new TransitionMutableCore(t));
                }
            }

            if (objTarget.Computation != null)
            {
                this._computation = new ComputationMutableCore(objTarget.Computation);
            }
        }

        /// <summary>
        ///     Gets or sets the computation.
        /// </summary>
        public IComputationMutableObject Computation
        {
            get
            {
                return this._computation;
            }

            set
            {
                this._computation = value;
            }
        }

        /// <summary>
        ///     Gets the input.
        /// </summary>
        public IList<IInputOutputMutableObject> Input
        {
            get
            {
                return this._input;
            }
        }

        /// <summary>
        ///     Gets the output.
        /// </summary>
        public IList<IInputOutputMutableObject> Output
        {
            get
            {
                return this._output;
            }
        }

        /// <summary>
        ///     Gets the process steps.
        /// </summary>
        public IList<IProcessStepMutableObject> ProcessSteps
        {
            get
            {
                return this._processSteps;
            }
        }

        /// <summary>
        ///     Gets the transitions.
        /// </summary>
        public IList<ITransitionMutableObject> Transitions
        {
            get
            {
                return this._transitions;
            }
        }

        /// <summary>
        ///     The add process step.
        /// </summary>
        /// <param name="processStep">
        ///     The process step.
        /// </param>
        public void AddProcessStep(IProcessStepMutableObject processStep)
        {
            this._processSteps.Add(processStep);
        }

        /// <summary>
        ///     The add transition.
        /// </summary>
        /// <param name="transition">
        ///     The transition.
        /// </param>
        public void AddTransition(ITransitionMutableObject transition)
        {
            this._transitions.Add(transition);
        }
    }
}