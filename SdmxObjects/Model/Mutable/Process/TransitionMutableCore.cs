// -----------------------------------------------------------------------
// <copyright file="TransitionMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The transition mutable core.
    /// </summary>
    [Serializable]
    public class TransitionMutableCore : IdentifiableMutableCore, ITransitionMutableObject
    {
        /// <summary>
        ///     The condition.
        /// </summary>
        private readonly IList<ITextTypeWrapperMutableObject> _conditions;

        /// <summary>
        ///     The local id.
        /// </summary>
        private string _localId;

        /// <summary>
        ///     The target step.
        /// </summary>
        private string _targetStep;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionMutableCore" /> class.
        /// </summary>
        public TransitionMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Transition))
        {
            this._conditions = new List<ITextTypeWrapperMutableObject>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionMutableCore" /> class.
        /// </summary>
        /// <param name="transitionType">
        ///     The transition type.
        /// </param>
        public TransitionMutableCore(ITransition transitionType)
            : base(transitionType)
        {
            this._conditions = new List<ITextTypeWrapperMutableObject>();
            this._targetStep = transitionType.TargetStep.Id;
            if (transitionType.Condition != null)
            {
                foreach (ITextTypeWrapper textTypeWrapper in transitionType.Condition)
                {
                    this._conditions.Add(new TextTypeWrapperMutableCore(textTypeWrapper));
                }
            }

            this._localId = transitionType.LocalId;
        }

        /// <summary>
        ///     Gets the condition.
        /// </summary>
        public virtual IList<ITextTypeWrapperMutableObject> Conditions
        {
            get
            {
                return this._conditions;
            }
        }

        /// <summary>
        ///     Gets or sets the local id.
        /// </summary>
        public virtual string LocalId
        {
            get
            {
                return this._localId;
            }

            set
            {
                this._localId = value;
            }
        }

        /// <summary>
        ///     Gets or sets the target step.
        /// </summary>
        public virtual string TargetStep
        {
            get
            {
                return this._targetStep;
            }

            set
            {
                this._targetStep = value;
            }
        }
    }
}