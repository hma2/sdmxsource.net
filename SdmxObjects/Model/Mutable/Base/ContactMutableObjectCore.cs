﻿// -----------------------------------------------------------------------
// <copyright file="ContactMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    #endregion

    /// <summary>
    ///     The contact mutable object class
    /// </summary>
    public class ContactMutableObjectCore : MutableCore, IContactMutableObject
    {
        /// <summary>
        ///     The _departments
        /// </summary>
        private readonly IList<ITextTypeWrapperMutableObject> _departments = new List<ITextTypeWrapperMutableObject>();

        /// <summary>
        ///     The _email
        /// </summary>
        private readonly IList<string> _email = new List<string>();

        /// <summary>
        ///     The _fax
        /// </summary>
        private readonly IList<string> _fax = new List<string>();

        /// <summary>
        ///     The _names
        /// </summary>
        private readonly IList<ITextTypeWrapperMutableObject> _names = new List<ITextTypeWrapperMutableObject>();

        /// <summary>
        ///     The _roles
        /// </summary>
        private readonly IList<ITextTypeWrapperMutableObject> _roles = new List<ITextTypeWrapperMutableObject>();

        /// <summary>
        ///     The _telephone
        /// </summary>
        private readonly IList<string> _telephone = new List<string>();

        /// <summary>
        ///     The _uri
        /// </summary>
        private readonly IList<string> _uri = new List<string>();

        /// <summary>
        ///     The X400
        /// </summary>
        private readonly IList<string> _x400 = new List<string>();

        /// <summary>
        ///     The _id
        /// </summary>
        private string _id;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactMutableObjectCore" /> class.
        /// </summary>
        public ContactMutableObjectCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Contact))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactMutableObjectCore" /> class.
        /// </summary>
        /// <param name="contact">The contact.</param>
        public ContactMutableObjectCore(IContact contact)
            : base(contact)
        {
            this._id = contact.Id;

            this.CopyTextTypes(contact.Name, this._names);
            this.CopyTextTypes(contact.Role, this._roles);
            this.CopyTextTypes(contact.Departments, this._departments);
            this._email = new List<string>(contact.Email);
            this._fax = new List<string>(contact.Fax);
            this._telephone = new List<string>(contact.Telephone);
            this._uri = new List<string>(contact.Uri);
            this._x400 = new List<string>(contact.X400);
        }

        /// <summary>
        ///     Gets the departments of the contact
        /// </summary>
        public IList<ITextTypeWrapperMutableObject> Departments
        {
            get
            {
                return this._departments;
            }
        }

        /// <summary>
        ///     Gets the email of the contact
        /// </summary>
        public IList<string> Email
        {
            get
            {
                return this._email;
            }
        }

        /// <summary>
        ///     Gets the fax of the contact
        /// </summary>
        public IList<string> Fax
        {
            get
            {
                return this._fax;
            }
        }

        /// <summary>
        ///     Gets or sets the id of the contact
        /// </summary>
        public string Id
        {
            get
            {
                return this._id;
            }

            set
            {
                this._id = value;
            }
        }

        /// <summary>
        ///     Gets the names of the contact.
        /// </summary>
        /// <value>
        ///     list of names, or an empty list if none exist
        /// </value>
        public virtual IList<ITextTypeWrapperMutableObject> Names
        {
            get
            {
                return this._names;
            }
        }

        /// <summary>
        ///     Gets the roles of the contact
        /// </summary>
        /// <value>
        ///     The roles.
        /// </value>
        public virtual IList<ITextTypeWrapperMutableObject> Roles
        {
            get
            {
                return this._roles;
            }
        }

        /// <summary>
        ///     Gets the telephone of the contact
        /// </summary>
        public IList<string> Telephone
        {
            get
            {
                return this._telephone;
            }
        }

        /// <summary>
        ///     Gets the uris of the contact = list of uris, or an empty list if none exist
        /// </summary>
        public IList<string> Uri
        {
            get
            {
                return this._uri;
            }
        }

        /// <summary>
        ///     Gets the X400 list
        /// </summary>
        public IList<string> X400
        {
            get
            {
                return this._x400;
            }
        }

        /// <summary>
        ///     Adds a new department to the list, creates a new list if it is null
        /// </summary>
        /// <param name="dept">The department.</param>
        public void AddDepartment(ITextTypeWrapperMutableObject dept)
        {
            this._departments.Add(dept);
        }

        /// <summary>
        ///     Adds a new email to the list, creates a new list if it is null
        /// </summary>
        /// <param name="email">The email.</param>
        public void AddEmail(string email)
        {
            this._email.Add(email);
        }

        /// <summary>
        ///     Adds a new fax to the list, creates a new list if it is null
        /// </summary>
        /// <param name="fax">The fax.</param>
        public void AddFax(string fax)
        {
            this._fax.Add(fax);
        }

        /// <summary>
        ///     Adds a new name to the list, creates a new list if it is null
        /// </summary>
        /// <param name="name">The name.</param>
        public void AddName(ITextTypeWrapperMutableObject name)
        {
            this._names.Add(name);
        }

        /// <summary>
        ///     Adds a new role to the list, creates a new list if it is null
        /// </summary>
        /// <param name="role">The role.</param>
        public void AddRole(ITextTypeWrapperMutableObject role)
        {
            this._roles.Add(role);
        }

        /// <summary>
        ///     Adds a new telephone to the list, creates a new list if it is null
        /// </summary>
        /// <param name="telephone">The telephone.</param>
        public void AddTelephone(string telephone)
        {
            this._telephone.Add(telephone);
        }

        /// <summary>
        ///     Adds a new uri to the list, creates a new list if it is null
        /// </summary>
        /// <param name="uri">The URI.</param>
        public void AddUri(string uri)
        {
            this._uri.Add(uri);
        }

        /// <summary>
        ///     Adds a new X400 to the list, creates a new list if it is null
        /// </summary>
        /// <param name="x400">The X400.</param>
        public void AddX400(string x400)
        {
            this._x400.Add(x400);
        }

        /// <summary>
        ///     Copies the text types.
        /// </summary>
        /// <param name="textType">Type of the text.</param>
        /// <param name="copyTo">The copy to.</param>
        private void CopyTextTypes(IList<ITextTypeWrapper> textType, IList<ITextTypeWrapperMutableObject> copyTo)
        {
            if (textType != null)
            {
                foreach (ITextTypeWrapper currentTextType in textType)
                {
                    copyTo.Add(new TextTypeWrapperMutableCore(currentTextType));
                }
            }
        }
    }
}