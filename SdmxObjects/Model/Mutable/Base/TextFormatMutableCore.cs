// -----------------------------------------------------------------------
// <copyright file="TextFormatMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The text format mutable core.
    /// </summary>
    public class TextFormatMutableCore : MutableCore, ITextFormatMutableObject
    {
        /// <summary>
        ///     The decimals.
        /// </summary>
        private long? _decimals;

        /// <summary>
        ///     The end value.
        /// </summary>
        private decimal? _endValue;

        /// <summary>
        ///     The interval.
        /// </summary>
        private decimal? _interval;

        /// <summary>
        ///     The is multi lingual.
        /// </summary>
        private TertiaryBool _isMultilingual = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

        /// <summary>
        ///     The is sequence.
        /// </summary>
        private TertiaryBool _isSequence = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

        /// <summary>
        ///     The max length.
        /// </summary>
        private long? _maxLength;

        /// <summary>
        ///     The max value.
        /// </summary>
        private decimal? _maxValue;

        /// <summary>
        ///     The min length.
        /// </summary>
        private long? _minLength;

        /// <summary>
        ///     The min value.
        /// </summary>
        private decimal? _minValue;

        /// <summary>
        ///     The pattern.
        /// </summary>
        private string _pattern;

        /// <summary>
        ///     The start value.
        /// </summary>
        private decimal? _startValue;

        /// <summary>
        ///     The text type.
        /// </summary>
        private TextType _textType;

        /// <summary>
        ///     The time interval.
        /// </summary>
        private string _timeInterval;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextFormatMutableCore" /> class.
        /// </summary>
        public TextFormatMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextFormat))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextFormatMutableCore" /> class.
        /// </summary>
        /// <param name="textFormat">
        ///     The itxt.
        /// </param>
        public TextFormatMutableCore(ITextFormat textFormat)
            : base(textFormat)
        {
            this._textType = textFormat.TextType;
            this._isSequence = textFormat.Sequence;
            this._maxLength = textFormat.MaxLength;
            this._minLength = textFormat.MinLength;
            this._startValue = textFormat.StartValue;
            this._endValue = textFormat.EndValue;
            this._interval = textFormat.Interval;
            this._maxValue = textFormat.MaxValue;
            this._minValue = textFormat.MinValue;
            this.StartTime = textFormat.StartTime;
            this.EndTime = textFormat.EndTime;
            this._isMultilingual = textFormat.Multilingual;
            if (textFormat.TimeInterval != null)
            {
                this._timeInterval = textFormat.TimeInterval;
            }

            this._decimals = textFormat.Decimals;
            this._pattern = textFormat.Pattern;
        }

        /// <summary>
        ///     Gets or sets the decimals.
        /// </summary>
        public virtual long? Decimals
        {
            get
            {
                return this._decimals;
            }

            set
            {
                this._decimals = value;
            }
        }

        /// <summary>
        ///     Gets or sets the latest end time the value can take -
        /// </summary>
        /// <value> null if not defined </value>
        public ISdmxDate EndTime { get; set; }

        /// <summary>
        ///     Gets or sets the end value.
        /// </summary>
        public virtual decimal? EndValue
        {
            get
            {
                return this._endValue;
            }

            set
            {
                this._endValue = value;
            }
        }

        /// <summary>
        ///     Gets or sets the interval.
        /// </summary>
        public virtual decimal? Interval
        {
            get
            {
                return this._interval;
            }

            set
            {
                this._interval = value;
            }
        }

        /// <summary>
        ///     Gets or sets the max length.
        /// </summary>
        public virtual long? MaxLength
        {
            get
            {
                return this._maxLength;
            }

            set
            {
                this._maxLength = value;
            }
        }

        /// <summary>
        ///     Gets or sets the max value.
        /// </summary>
        public virtual decimal? MaxValue
        {
            get
            {
                return this._maxValue;
            }

            set
            {
                this._maxValue = value;
            }
        }

        /// <summary>
        ///     Gets or sets the min length.
        /// </summary>
        public virtual long? MinLength
        {
            get
            {
                return this._minLength;
            }

            set
            {
                this._minLength = value;
            }
        }

        /// <summary>
        ///     Gets or sets the min value.
        /// </summary>
        public virtual decimal? MinValue
        {
            get
            {
                return this._minValue;
            }

            set
            {
                this._minValue = value;
            }
        }

        /// <summary>
        ///     Gets or sets the multi lingual.
        /// </summary>
        public virtual TertiaryBool Multilingual
        {
            get
            {
                return this._isMultilingual;
            }

            set
            {
                this._isMultilingual = value;
            }
        }

        /// <summary>
        ///     Gets or sets the pattern.
        /// </summary>
        public virtual string Pattern
        {
            get
            {
                return this._pattern;
            }

            set
            {
                this._pattern = value;
            }
        }

        /// <summary>
        ///     Gets or sets the sequence.
        /// </summary>
        public virtual TertiaryBool Sequence
        {
            get
            {
                return this._isSequence;
            }

            set
            {
                this._isSequence = value;
            }
        }

        /// <summary>
        ///     Gets or sets the earliest start time the value can take -
        /// </summary>
        /// <value> null if not defined </value>
        public ISdmxDate StartTime { get; set; }

        /// <summary>
        ///     Gets or sets the start value.
        /// </summary>
        public virtual decimal? StartValue
        {
            get
            {
                return this._startValue;
            }

            set
            {
                this._startValue = value;
            }
        }

        /// <summary>
        ///     Gets or sets the text type.
        /// </summary>
        public virtual TextType TextType
        {
            get
            {
                return this._textType;
            }

            set
            {
                this._textType = value;
            }
        }

        /// <summary>
        ///     Gets or sets the time interval.
        /// </summary>
        public virtual string TimeInterval
        {
            get
            {
                return this._timeInterval;
            }

            set
            {
                this._timeInterval = value;
            }
        }
    }
}