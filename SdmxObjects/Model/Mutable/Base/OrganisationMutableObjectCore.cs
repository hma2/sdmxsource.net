﻿// -----------------------------------------------------------------------
// <copyright file="OrganisationMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Attributes;

    #endregion

    /// <summary>
    ///     The organisation mutable object
    /// </summary>
    public abstract class OrganisationMutableObjectCore : ItemMutableCore, IOrganisationMutableObject
    {
        /// <summary>
        ///     The _contacts
        /// </summary>
        private readonly IList<IContactMutableObject> _contacts = new List<IContactMutableObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationMutableObjectCore" /> class.
        /// </summary>
        /// <param name="organisation">The organisation.</param>
        protected OrganisationMutableObjectCore([ValidatedNotNull]IOrganisation organisation)
            : base(organisation)
        {
            foreach (IContact contact in organisation.Contacts)
            {
                this.AddContact(new ContactMutableObjectCore(contact));
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationMutableObjectCore" /> class.
        /// </summary>
        /// <param name="structureType">The structure type.</param>
        protected OrganisationMutableObjectCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        ///     Gets all the contacts for this organisation, this might return empty collection.
        /// </summary>
        public IList<IContactMutableObject> Contacts
        {
            get
            {
                return this._contacts;
            }
        }

        /// <summary>
        ///     Add a new contact to the list
        /// </summary>
        /// <param name="contact">The contact.</param>
        public void AddContact(IContactMutableObject contact)
        {
            this._contacts.Add(contact);
        }
    }
}