// -----------------------------------------------------------------------
// <copyright file="TimeRangeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    ///     The time range mutable core.
    /// </summary>
    [Serializable]
    public class TimeRangeMutableCore : MutableCore, ITimeRangeMutableObject
    {
        /// <summary>
        ///     The is range.
        /// </summary>
        private bool _isIsRange;

        /// <summary>
        ///     The end date.
        /// </summary>
        private DateTime? _endDate;

        /// <summary>
        ///     The is end inclusive.
        /// </summary>
        private bool _isEndInclusive;

        /// <summary>
        ///     The is start inclusive.
        /// </summary>
        private bool _isStartInclusive;

        /// <summary>
        ///     The start date.
        /// </summary>
        private DateTime? _startDate;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeRangeMutableCore" /> class.
        /// </summary>
        public TimeRangeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TimeRange))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeRangeMutableCore" /> class.
        /// </summary>
        /// <param name="immutable">
        ///     The immutable.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="immutable"/> is <see langword="null" />.</exception>
        public TimeRangeMutableCore(ITimeRange immutable)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TimeRange))
        {
            if (immutable == null)
            {
                throw new ArgumentNullException("immutable");
            }

            if (immutable.StartDate != null)
            {
                this._startDate = immutable.StartDate.Date;
            }

            if (immutable.EndDate != null)
            {
                this._endDate = immutable.EndDate.Date;
            }

            this._isIsRange = immutable.Range;
            this._isStartInclusive = immutable.StartInclusive;
            this._isEndInclusive = immutable.EndInclusive;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TimeRangeMutableCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public TimeRangeMutableCore(TimeRangeValueType type)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TimeRange))
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (type.AfterPeriod != null)
            {
                this._isIsRange = false;

                // FUNC 2.1 ObservationalTimePeriodType - does this work?
                this._endDate = DateUtil.FormatDate(type.AfterPeriod.TypedValue, true);
                this._isEndInclusive = type.AfterPeriod.isInclusive;
            }

            if (type.BeforePeriod != null)
            {
                this._isIsRange = false;
                this._startDate = DateUtil.FormatDate(type.BeforePeriod.TypedValue, true);
                this._isStartInclusive = type.BeforePeriod.isInclusive;
            }

            if (type.StartPeriod != null)
            {
                this._isIsRange = true;
                this._startDate = DateUtil.FormatDate(type.StartPeriod.TypedValue, true);
                this._isStartInclusive = type.StartPeriod.isInclusive;
            }

            if (type.EndPeriod != null)
            {
                this._isIsRange = true;
                this._startDate = DateUtil.FormatDate(type.EndPeriod.TypedValue, true);
                this._isEndInclusive = type.EndPeriod.isInclusive;
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets or sets the end date.
        /// </summary>
        public virtual DateTime? EndDate
        {
            get
            {
                if (this._endDate != null)
                {
                    return new DateTime((this._endDate.Value.Ticks / 10000) * 10000);
                }

                return null;
            }

            set
            {
                this._endDate = value;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether is end inclusive.
        /// </summary>
        public virtual bool IsEndInclusive
        {
            get
            {
                return this._isEndInclusive;
            }

            set
            {
                this._isEndInclusive = value;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether range.
        /// </summary>
        public virtual bool IsRange
        {
            get
            {
                return this._isIsRange;
            }

            set
            {
                this._isIsRange = value;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether is start inclusive.
        /// </summary>
        public virtual bool IsStartInclusive
        {
            get
            {
                return this._isStartInclusive;
            }

            set
            {
                this._isStartInclusive = value;
            }
        }

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        public virtual DateTime? StartDate
        {
            get
            {
                if (this._startDate != null)
                {
                    return new DateTime((this._startDate.Value.Ticks / 10000) * 10000);
                }

                return null;
            }

            set
            {
                this._startDate = value;
            }
        }

        /// <summary>
        ///     The create immutable instance.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <returns>
        ///     The <see cref="ITimeRange" /> .
        /// </returns>
        public virtual ITimeRange CreateImmutableInstance(ISdmxStructure parent)
        {
            return new TimeRangeCore(this, parent);
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._startDate == null && this._endDate == null)
            {
                throw new SdmxSemmanticException("Time period must define at least one date");
            }

            if (this._isIsRange)
            {
                if (this._startDate == null || this._endDate == null)
                {
                    throw new SdmxSemmanticException("Time period with a range requires both a start and end period");
                }

                if ((this._startDate.Value.Ticks / 10000) > (this._endDate.Value.Ticks / 10000))
                {
                    throw new SdmxSemmanticException("Time range can not specify start period after end period");
                }
            }
            else
            {
                if (this._startDate != null && this._endDate != null)
                {
                    throw new SdmxSemmanticException("Time period can not define both a before period and after period");
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}