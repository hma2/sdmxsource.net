// -----------------------------------------------------------------------
// <copyright file="MaintainableMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The maintainable mutable core.
    /// </summary>
    /// <typeparam name="T">
    ///     Generic type param of IMaintainableObject type
    /// </typeparam>
    public abstract class MaintainableMutableCore<T> : NameableMutableCore, IMaintainableMutableObject
        where T : IMaintainableObject
    {
        /// <summary>
        ///     The _agency id.
        /// </summary>
        private string _agencyId;

        /// <summary>
        ///     The _end date.
        /// </summary>
        private DateTime? _endDate;

        /// <summary>
        ///     The _is external reference.
        /// </summary>
        private TertiaryBool _isExternalReference;

        /// <summary>
        ///     The _is final structure.
        /// </summary>
        private TertiaryBool _isFinalStructure;

        /// <summary>
        ///     The _is stub.
        /// </summary>
        private bool _isStub;

        /// <summary>
        ///     The _service url.
        /// </summary>
        private Uri _serviceUrl;

        /// <summary>
        ///     The _start date.
        /// </summary>
        private DateTime? _startDate;

        /// <summary>
        ///     The _structure url.
        /// </summary>
        private Uri _structureUrl;

        /// <summary>
        ///     The _version.
        /// </summary>
        private string _version;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableMutableCore{T}" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        protected MaintainableMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintainableMutableCore{T}" /> class.
        /// </summary>
        /// <param name="objTarget">
        ///     The obj target.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="objTarget"/> is <see langword="null" />.</exception>
        protected MaintainableMutableCore(IMaintainableObject objTarget)
            : base(objTarget)
        {
            if (objTarget == null)
            {
                throw new ArgumentNullException("objTarget");
            }

            this._agencyId = objTarget.AgencyId;
            if (objTarget.StartDate != null)
            {
                this._startDate = objTarget.StartDate.Date;
            }

            if (objTarget.EndDate != null)
            {
                this._endDate = objTarget.EndDate.Date;
            }

            if (objTarget.ServiceUrl != null)
            {
                this._serviceUrl = objTarget.ServiceUrl;
            }

            if (objTarget.StructureUrl != null)
            {
                this._structureUrl = objTarget.StructureUrl;
            }

            this._isFinalStructure = objTarget.IsFinal;
            this._version = objTarget.Version;
            this._isExternalReference = objTarget.IsExternalReference;
        }

        /// <summary>
        ///     Gets or sets the agency id.
        /// </summary>
        public virtual string AgencyId
        {
            get
            {
                return this._agencyId;
            }

            set
            {
                this._agencyId = value;
            }
        }

        /// <summary>
        ///     Gets or sets the end date.
        /// </summary>
        public virtual DateTime? EndDate
        {
            get
            {
                return this._endDate;
            }

            set
            {
                this._endDate = value;
            }
        }

        /// <summary>
        ///     Gets or sets the external reference.
        /// </summary>
        public virtual TertiaryBool ExternalReference
        {
            get
            {
                return this._isExternalReference;
            }

            set
            {
                this._isExternalReference = value;
            }
        }

        /// <summary>
        ///     Gets or sets the final structure.
        /// </summary>
        public virtual TertiaryBool FinalStructure
        {
            get
            {
                return this._isFinalStructure;
            }

            set
            {
                this._isFinalStructure = value;
            }
        }

        /// <summary>
        ///     Gets ImmutableInstance
        /// </summary>
        public abstract T ImmutableInstance { get; }

        /// <summary>
        ///     Gets or sets the service url.
        /// </summary>
        public virtual Uri ServiceURL
        {
            get
            {
                return this._serviceUrl;
            }

            set
            {
                this._serviceUrl = value;
            }
        }

        /// <summary>
        ///     Gets or sets the start date.
        /// </summary>
        public virtual DateTime? StartDate
        {
            get
            {
                return this._startDate;
            }

            set
            {
                this._startDate = value;
            }
        }

        /// <summary>
        ///     Gets or sets the structure url.
        /// </summary>
        public virtual Uri StructureURL
        {
            get
            {
                return this._structureUrl;
            }

            set
            {
                this._structureUrl = value;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether stub.
        /// </summary>
        public virtual bool Stub
        {
            get
            {
                return this._isStub;
            }

            set
            {
                this._isStub = value;
            }
        }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        /// ///
        /// <exception cref="ArgumentException">Throws ArgumentException.</exception>
        public virtual string Version
        {
            get
            {
                return this._version;
            }

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    value = "1.0";
                }

                if (!VersionableUtil.ValidVersion(value))
                {
                    throw new ArgumentException("Version invalid : " + value);
                }

                this._version = VersionableUtil.FormatVersion(value);
            }
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        IMaintainableObject IMaintainableMutableObject.ImmutableInstance
        {
            get
            {
                return this.ImmutableInstance;
            }
        }

        /// <summary>
        ///     The compare to.
        /// </summary>
        /// <param name="maintainableObject">
        ///     The maintainable dataStructureObject.
        /// </param>
        /// <returns>
        ///     The <see cref="int" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="maintainableObject"/> is <see langword="null" />.</exception>
        public int CompareTo(IMaintainableObject maintainableObject)
        {
            if (maintainableObject == null)
            {
                throw new ArgumentNullException("maintainableObject");
            }

            return VersionableUtil.IsHigherVersion(this._version, maintainableObject.Version) ? -1 : +1;
        }

        /// <summary>
        ///     The build maintainable attributes.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        protected internal void BuildMaintainableAttributes(ISdmxReader reader)
        {
            this.BuildIdentifiableAttributes(reader);
            IDictionary<string, string> attributes = reader.Attributes;
            this._version = attributes.GetOrDefault("version");
            this._agencyId = attributes.GetOrDefault("agencyID");

            string validFrom;
            if (attributes.TryGetValue("validFrom", out validFrom))
            {
                this._startDate = new SdmxDateCore(validFrom).Date;
            }

            string validTo;
            if (attributes.TryGetValue("validTo", out validTo))
            {
                this._endDate = new SdmxDateCore(validTo).Date;
            }

            string serviceUrl;
            if (attributes.TryGetValue("serviceURL", out serviceUrl))
            {
                this.ServiceURL = new Uri(serviceUrl);
            }

            string structureUrl;
            if (attributes.TryGetValue("structureURL", out structureUrl))
            {
                this.StructureURL = new Uri(structureUrl);
            }
        }

        // TODO Test this method orders a list index 0 higher version then a list with index 1
    }
}