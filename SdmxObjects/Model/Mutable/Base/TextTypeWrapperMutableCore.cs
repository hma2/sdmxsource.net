// -----------------------------------------------------------------------
// <copyright file="TextTypeWrapperMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The text type wrapper mutable core.
    /// </summary>
    [Serializable]
    public class TextTypeWrapperMutableCore : MutableCore, ITextTypeWrapperMutableObject
    {
        /// <summary>
        ///     The locale.
        /// </summary>
        private string _locale;

        /// <summary>
        ///     The valueren.
        /// </summary>
        private string _valueren;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperMutableCore" /> class.
        /// </summary>
        public TextTypeWrapperMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperMutableCore" /> class.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <param name="value">The value.</param>
        public TextTypeWrapperMutableCore(string locale, string value)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType))
        {
            this._locale = locale;
            this._valueren = value;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperMutableCore" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        public TextTypeWrapperMutableCore(ITextTypeWrapper textType)
            : base(textType)
        {
            this._locale = textType.Locale;
            this._valueren = textType.Value;
        }

        /// <summary>
        ///     Gets or sets the locale.
        /// </summary>
        public virtual string Locale
        {
            get
            {
                return this._locale;
            }

            set
            {
                this._locale = value;
            }
        }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public virtual string Value
        {
            get
            {
                return this._valueren;
            }

            set
            {
                this._valueren = value;
            }
        }
    }
}