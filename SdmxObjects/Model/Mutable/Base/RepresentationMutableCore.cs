// -----------------------------------------------------------------------
// <copyright file="RepresentationMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The representation mutable core.
    /// </summary>
    [Serializable]
    public class RepresentationMutableCore : MutableCore, IRepresentationMutableObject
    {
        /// <summary>
        ///     The representation ref.
        /// </summary>
        private IStructureReference _representationRef;

        /// <summary>
        ///     The text format.
        /// </summary>
        private ITextFormatMutableObject _textFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationMutableCore" /> class.
        /// </summary>
        public RepresentationMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.LocalRepresentation))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationMutableCore" /> class.
        /// </summary>
        /// <param name="representation">
        ///     The representation.
        /// </param>
        public RepresentationMutableCore(IRepresentation representation)
            : base(representation)
        {
            if (representation.TextFormat != null)
            {
                this._textFormat = new TextFormatMutableCore(representation.TextFormat);
            }

            if (representation.Representation != null)
            {
                this._representationRef = representation.Representation.CreateMutableInstance();
            }
        }

        /// <summary>
        ///     Gets or sets the representation.
        /// </summary>
        public virtual IStructureReference Representation
        {
            get
            {
                return this._representationRef;
            }

            set
            {
                this._representationRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the text format.
        /// </summary>
        public virtual ITextFormatMutableObject TextFormat
        {
            get
            {
                return this._textFormat;
            }

            set
            {
                this._textFormat = value;
            }
        }
    }
}