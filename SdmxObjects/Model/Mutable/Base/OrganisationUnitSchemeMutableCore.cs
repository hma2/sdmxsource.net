// -----------------------------------------------------------------------
// <copyright file="OrganisationUnitSchemeMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///     The organisation unit scheme mutable core.
    /// </summary>
    [Serializable]
    public class OrganisationUnitSchemeMutableCore :
        ItemSchemeMutableCore<IOrganisationUnitMutableObject, IOrganisationUnit, IOrganisationUnitSchemeObject>, 
        IOrganisationUnitSchemeMutableObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationUnitSchemeMutableCore" /> class.
        /// </summary>
        /// <param name="organisationUnitSchemeObject">
        ///     The organisationUnitSchemeObject.
        /// </param>
        public OrganisationUnitSchemeMutableCore(IOrganisationUnitSchemeObject organisationUnitSchemeObject)
            : base(organisationUnitSchemeObject)
        {
            foreach (IOrganisationUnit organisationUnit in organisationUnitSchemeObject.Items)
            {
                this.AddItem(new OrganisationUnitMutableCore(organisationUnit));

                // organisationUnits.add(new OrganisationUnitMutableCore(organisationUnit));
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationUnitSchemeMutableCore" /> class.
        /// </summary>
        public OrganisationUnitSchemeMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme))
        {
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override IOrganisationUnitSchemeObject ImmutableInstance
        {
            get
            {
                return new OrganisationUnitSchemeObjectCore(this);
            }
        }

        /// <summary>
        ///     Creates an item and adds it to the scheme
        /// </summary>
        /// <param name="id">The id to set</param>
        /// <param name="name">The name to set in the 'en' lang</param>
        /// <returns>
        ///     The created item
        /// </returns>
        public override IOrganisationUnitMutableObject CreateItem(string id, string name)
        {
            IOrganisationUnitMutableObject orgUnit = new OrganisationUnitMutableCore();
            orgUnit.Id = id;
            orgUnit.AddName("en", name);
            this.AddItem(orgUnit);
            return orgUnit;
        }
    }
}