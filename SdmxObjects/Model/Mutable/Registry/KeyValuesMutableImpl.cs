// -----------------------------------------------------------------------
// <copyright file="KeyValuesMutableImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The key values mutable impl.
    /// </summary>
    [Serializable]
    public class KeyValuesMutableImpl : MutableCore, IKeyValuesMutable
    {
        /// <summary>
        ///     The cascade list.
        /// </summary>
        private IList<string> _cascadeList;

        /// <summary>
        ///     The id.
        /// </summary>
        private string _id;

        /// <summary>
        ///     The itime range.
        /// </summary>
        private ITimeRangeMutableObject _timeRange;

        /// <summary>
        ///     The values.
        /// </summary>
        private IList<string> _values;

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValuesMutableImpl" /> class.
        /// </summary>
        public KeyValuesMutableImpl()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.KeyValues))
        {
            this._values = new List<string>();
            this._cascadeList = new List<string>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValuesMutableImpl" /> class.
        /// </summary>
        /// <param name="immutable">
        ///     The immutable.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="immutable"/> is <see langword="null" />.</exception>
        public KeyValuesMutableImpl(IKeyValues immutable)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.KeyValues))
        {
            if (immutable == null)
            {
                throw new ArgumentNullException("immutable");
            }

            this._values = new List<string>(immutable.Values);
            this._cascadeList = new List<string>();
            this._id = immutable.Id;

            foreach (string each in this._values)
            {
                if (immutable.IsCascadeValue(each))
                {
                    this._cascadeList.Add(each);
                }
            }

            if (immutable.TimeRange != null)
            {
                this._timeRange = new TimeRangeMutableCore(immutable.TimeRange);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValuesMutableImpl" /> class.
        /// </summary>
        /// <param name="keyValueType">
        ///     The key value type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="keyValueType"/> is <see langword="null" />.</exception>
        public KeyValuesMutableImpl(ComponentValueSetType keyValueType)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.KeyValues))
        {
            if (keyValueType == null)
            {
                throw new ArgumentNullException("keyValueType");
            }

            this._values = new List<string>();
            this._cascadeList = new List<string>();

            this._id = keyValueType.id;

            if (keyValueType.Value != null)
            {
                foreach (SimpleValueType dataKeyType in keyValueType.Value)
                {
                    this._values.Add(dataKeyType.TypedValue);
                    if (dataKeyType.cascadeValues)
                    {
                        this._cascadeList.Add(dataKeyType.TypedValue);
                    }
                }
            }

            if (keyValueType.TimeRange != null)
            {
                this._timeRange = new TimeRangeMutableCore(keyValueType.TimeRange);
            }
        }

        /// <summary>
        ///     Gets the cascade.
        /// </summary>
        public virtual IList<string> Cascade
        {
            get
            {
                return this._cascadeList;
            }
        }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }

            set
            {
                this._id = value;
            }
        }

        /// <summary>
        ///     Gets the key values.
        /// </summary>
        public virtual IList<string> KeyValues
        {
            get
            {
                return this._values;
            }
        }

        /// <summary>
        ///     Gets or sets the time range.
        /// </summary>
        public virtual ITimeRangeMutableObject TimeRange
        {
            get
            {
                return this._timeRange;
            }

            set
            {
                this._timeRange = value;
            }
        }

        /// <summary>
        ///     The add cascade.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        public virtual void AddCascade(string value)
        {
            if (this._cascadeList == null)
            {
                this._cascadeList = new List<string>();
            }

            this._cascadeList.Add(value);
        }

        /// <summary>
        ///     The add value.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        public virtual void AddValue(string value)
        {
            if (this._values == null)
            {
                this._values = new List<string>();
            }

            if (value != null)
            {
                this._values.Add(value);
            }
        }

        /// <summary>
        ///     The is cascade value.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public virtual bool IsCascadeValue(string value)
        {
            return this._cascadeList.Contains(value);
        }
    }
}