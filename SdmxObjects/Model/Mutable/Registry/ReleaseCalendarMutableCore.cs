// -----------------------------------------------------------------------
// <copyright file="ReleaseCalendarMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The release calendar mutable core.
    /// </summary>
    [Serializable]
    public class ReleaseCalendarMutableCore : MutableCore, IReleaseCalendarMutableObject
    {
        /// <summary>
        ///     The offset.
        /// </summary>
        private string _offset;

        /// <summary>
        ///     The periodicity.
        /// </summary>
        private string _periodicity;

        /// <summary>
        ///     The tolerance.
        /// </summary>
        private string _tolerance;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReleaseCalendarMutableCore" /> class.
        /// </summary>
        public ReleaseCalendarMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReleaseCalendar))
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM IMMUTABLE OBJECT                 //////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReleaseCalendarMutableCore" /> class.
        /// </summary>
        /// <param name="immutable">
        ///     The immutable.
        /// </param>
        public ReleaseCalendarMutableCore(IReleaseCalendar immutable)
            : base(immutable)
        {
            this._periodicity = immutable.Periodicity;
            this._offset = immutable.Offset;
            this._tolerance = immutable.Tolerance;
        }

        /// <summary>
        ///     Gets or sets the offset.
        /// </summary>
        public virtual string Offset
        {
            get
            {
                return this._offset;
            }

            set
            {
                this._offset = value;
            }
        }

        /// <summary>
        ///     Gets or sets the periodicity.
        /// </summary>
        public virtual string Periodicity
        {
            get
            {
                return this._periodicity;
            }

            set
            {
                this._periodicity = value;
            }
        }

        /// <summary>
        ///     Gets or sets the tolerance.
        /// </summary>
        public virtual string Tolerance
        {
            get
            {
                return this._tolerance;
            }

            set
            {
                this._tolerance = value;
            }
        }
    }
}