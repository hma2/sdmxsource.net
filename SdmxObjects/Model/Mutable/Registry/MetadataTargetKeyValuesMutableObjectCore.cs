﻿// -----------------------------------------------------------------------
// <copyright file="MetadataTargetKeyValuesMutableObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry
{
    #region Using directives

    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    #endregion

    /// <summary>
    ///     The MetadataTargetKeyValuesMutableObjectCore class.
    /// </summary>
    public class MetadataTargetKeyValuesMutableObjectCore : KeyValuesMutableImpl, IMetadataTargetKeyValuesMutable
    {
        /// <summary>
        ///     The dataset references
        /// </summary>
        private readonly IList<IDataSetReferenceMutableObject> _datasetReferences =
            new List<IDataSetReferenceMutableObject>();

        /// <summary>
        ///     The object references
        /// </summary>
        private readonly IList<IStructureReference> _objectReferences = new List<IStructureReference>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetKeyValuesMutableObjectCore" /> class.
        /// </summary>
        public MetadataTargetKeyValuesMutableObjectCore()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetKeyValuesMutableObjectCore" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        public MetadataTargetKeyValuesMutableObjectCore(IMetadataTargetKeyValues createdFrom)
            : base(createdFrom)
        {
            foreach (ICrossReference crossRef in createdFrom.ObjectReferences)
            {
                this._objectReferences.Add(new StructureReferenceImpl(crossRef.TargetUrn));
            }

            foreach (IDataSetReference dsref in createdFrom.DatasetReferences)
            {
                this._datasetReferences.Add(new DataSetReferenceMutableObjectCore(dsref));
            }
        }

        /// <summary>
        ///     Gets the dataset references.
        /// </summary>
        /// <value>
        ///     The dataset references.
        /// </value>
        public IList<IDataSetReferenceMutableObject> DatasetReferences
        {
            get
            {
                return this._datasetReferences;
            }
        }

        /// <summary>
        ///     Gets the object references.
        /// </summary>
        /// <value>
        ///     The object references.
        /// </value>
        public IList<IStructureReference> ObjectReferences
        {
            get
            {
                return this._objectReferences;
            }
        }

        /// <summary>
        ///     Adds the dataset reference.
        /// </summary>
        /// <param name="reference">The reference.</param>
        public void AddDatasetReference(IDataSetReferenceMutableObject reference)
        {
            this._datasetReferences.Add(reference);
        }

        /// <summary>
        ///     Adds the object reference.
        /// </summary>
        /// <param name="sref">The reference.</param>
        public void AddObjectReference(IStructureReference sref)
        {
            this._objectReferences.Add(sref);
        }
    }
}