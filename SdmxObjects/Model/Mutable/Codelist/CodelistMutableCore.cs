// -----------------------------------------------------------------------
// <copyright file="CodelistMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;

    /// <summary>
    ///     The codelist mutable core.
    /// </summary>
    [Serializable]
    public class CodelistMutableCore : ItemSchemeMutableCore<ICodeMutableObject, ICode, ICodelistObject>, 
                                       ICodelistMutableObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistMutableCore" /> class.
        /// </summary>
        public CodelistMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistMutableCore" /> class.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="reader"/> is <see langword="null" />.</exception>
        public CodelistMutableCore(ISdmxReader reader)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList))
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            ValidateRootElement(reader);
            this.BuildMaintainableAttributes(reader);

            reader.MoveNextElement();
            while (this.ProcessReader(reader))
            {
                reader.MoveNextElement();
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistMutableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        public CodelistMutableCore(SdmxStructureType structureType)
            : base(structureType)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistMutableCore" /> class.
        /// </summary>
        /// <param name="codelist">
        ///     The codelist.
        /// </param>
        public CodelistMutableCore(ICodelistObject codelist)
            : base(codelist)
        {
            // change Codelist beans in Mutable Codelist beans
            if (codelist.Items != null)
            {
                foreach (ICode code in codelist.Items)
                {
                    this.AddItem(new CodeMutableCore(code));
                }
            }
        }

        /// <summary>
        ///     Gets the immutable instance.
        /// </summary>
        public override ICodelistObject ImmutableInstance
        {
            get
            {
                return new CodelistObjectCore(this);
            }
        }

        /// <summary>
        ///     Creates an item and adds it to the scheme
        /// </summary>
        /// <param name="id">The id to set</param>
        /// <param name="name">The name to set in the 'en' lang</param>
        /// <returns>
        ///     The created item
        /// </returns>
        public override ICodeMutableObject CreateItem(string id, string name)
        {
            ICodeMutableObject code = new CodeMutableCore();
            code.Id = id;
            code.AddName("en", name);
            this.AddItem(code);
            return code;
        }

        /// <summary>
        ///     Returns the code with the given id, returns null if no such code exists
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///     The code mutable object
        /// </returns>
        public ICodeMutableObject GetCodeById(string id)
        {
            return this.Items.FirstOrDefault(currentCode => currentCode.Id.Equals(id));
        }

        /// <summary>
        ///     The process reader.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool ProcessReader(ISdmxReader reader)
        {
            if (this.ProcessReaderNameable(reader))
            {
                return true;
            }

            if (reader.CurrentElement.Equals("Code"))
            {
                this.ProcessCodes(reader);
            }

            return false;
        }

        /// <summary>
        ///     The validate root element.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private static void ValidateRootElement(ISdmxReader reader)
        {
            if (!reader.CurrentElement.Equals("Codelist"))
            {
                throw new SdmxSemmanticException(
                    "Can not construct codelist - expecting 'Codelist' Element in SDMX, actual element:"
                    + reader.CurrentElementValue);
            }
        }

        /// <summary>
        ///     The process codes.
        /// </summary>
        /// <param name="reader">
        ///     The reader.
        /// </param>
        private void ProcessCodes(ISdmxReader reader)
        {
            while (reader.CurrentElement.Equals("Code"))
            {
                ICodeMutableObject newCode = new CodeMutableCore(reader);

                ////if (newCode.Name == null || newCode.Name.Count == 0 || string.IsNullOrWhiteSpace(newCode.Name[0].Value))
                ////{
                ////    // Remove console output
                ////    ////Console.Out.WriteLine("HERE");
                ////}
                this.AddItem(newCode);
            }
        }
    }
}