// -----------------------------------------------------------------------
// <copyright file="LevelMutableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     The level mutable core.
    /// </summary>
    [Serializable]
    public class LevelMutableCore : NameableMutableCore, ILevelMutableObject
    {
        /// <summary>
        ///     The child level.
        /// </summary>
        private ILevelMutableObject _childLevel;

        /// <summary>
        ///     The coding format.
        /// </summary>
        private ITextFormatMutableObject _codingFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LevelMutableCore" /> class.
        /// </summary>
        public LevelMutableCore()
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Level))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="LevelMutableCore" /> class.
        /// </summary>
        /// <param name="level">
        ///     The level.
        /// </param>
        public LevelMutableCore(ILevelObject level)
            : base(level)
        {
            if (level.HasChild())
            {
                this._childLevel = new LevelMutableCore(level.ChildLevel);
            }

            if (level.CodingFormat != null)
            {
                this._codingFormat = new TextFormatMutableCore(level.CodingFormat);
            }
        }

        /// <summary>
        ///     Gets or sets the child level.
        /// </summary>
        public virtual ILevelMutableObject ChildLevel
        {
            get
            {
                return this._childLevel;
            }

            set
            {
                this._childLevel = value;
            }
        }

        /// <summary>
        ///     Gets or sets the coding format.
        /// </summary>
        public virtual ITextFormatMutableObject CodingFormat
        {
            get
            {
                return this._codingFormat;
            }

            set
            {
                this._codingFormat = value;
            }
        }
    }
}