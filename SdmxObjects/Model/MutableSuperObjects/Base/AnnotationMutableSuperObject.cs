// -----------------------------------------------------------------------
// <copyright file="AnnotationMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    /// <summary>
    ///     Annotation Mutable Super Object class
    /// </summary>
    public class AnnotationMutableSuperObject : IAnnotationMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationMutableSuperObject" /> class.
        /// </summary>
        /// <param name="annotation">The annotation.</param>
        /// <exception cref="ArgumentNullException"><paramref name="annotation"/> is <see langword="null" />.</exception>
        public AnnotationMutableSuperObject(IAnnotationSuperObject annotation) : this()
        {
            if (annotation == null)
            {
                throw new ArgumentNullException("annotation");
            }

            this.Title = annotation.Title;
            if (annotation.Url != null)
            {
                this.Url = annotation.Url;
            }

            this.Type = annotation.Type;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationMutableSuperObject" /> class.
        /// </summary>
        /// <param name="annotation">The annotation.</param>
        /// <exception cref="ArgumentNullException"><paramref name="annotation"/> is <see langword="null" />.</exception>
        public AnnotationMutableSuperObject(IAnnotation annotation) : this()
        {
            if (annotation == null)
            {
                throw new ArgumentNullException("annotation");
            }

            this.Title = annotation.Title;
            if (annotation.Uri != null)
            {
                this.Url = annotation.Uri;
            }

            this.Type = annotation.Type;
            if (annotation.Text != null)
            {
                foreach (var text in annotation.Text)
                {
                    this.Texts.Add(new TextTypeWrapperMutableCore(text));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationMutableSuperObject" /> class.
        /// </summary>
        public AnnotationMutableSuperObject()
        {
            this.Texts = new List<ITextTypeWrapperMutableObject>();
        }

        /// <summary>
        ///     Gets the texts.
        /// </summary>
        public IList<ITextTypeWrapperMutableObject> Texts { get; private set; }

        /// <summary>
        ///     Gets the title of the annotation
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        ///     Gets the type of the annotation
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        ///     Gets the Uri of the annotation
        /// </summary>
        public Uri Url { get; private set; }
    }
}