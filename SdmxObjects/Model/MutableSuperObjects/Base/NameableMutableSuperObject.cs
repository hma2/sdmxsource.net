// -----------------------------------------------------------------------
// <copyright file="NameableMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     NameableMutableSuperObject class
    /// </summary>
    public class NameableMutableSuperObject : IdentifiableMutableObject, INameableMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableMutableSuperObject" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <exception cref="ArgumentNullException"><paramref name="identifiable"/> is <see langword="null" />.</exception>
        public NameableMutableSuperObject(INameableSuperObject identifiable)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            // HACK This is a hack
            this.Descriptions = new List<ITextTypeWrapperMutableObject>();
            this.Names = new List<ITextTypeWrapperMutableObject>();
            if (identifiable.Descriptions != null)
            {
                foreach (var culture in identifiable.Descriptions.Keys)
                {
                    var mmean = new TextTypeWrapperMutableCore
                                    {
                                        Locale = culture.TwoLetterISOLanguageName, 
                                        Value = identifiable.Descriptions.GetOrDefault(culture)
                                    };
                    this.Descriptions.Add(mmean);
                }
            }

            if (identifiable.Names != null)
            {
                foreach (var cultureInfo in identifiable.Names.Keys)
                {
                    var mbean = new TextTypeWrapperMutableCore
                                    {
                                        Locale = cultureInfo.TwoLetterISOLanguageName, 
                                        Value = identifiable.Names.GetOrDefault(cultureInfo)
                                    };
                    this.Names.Add(mbean);
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableMutableSuperObject" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <exception cref="ArgumentNullException"><paramref name="identifiable"/> is <see langword="null" />.</exception>
        public NameableMutableSuperObject([ValidatedNotNull]INameableObject identifiable)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            if (identifiable.Description != null)
            {
                this.Descriptions = new List<ITextTypeWrapperMutableObject>();
                this.Names = new List<ITextTypeWrapperMutableObject>();

                foreach (var textTypeWrapper in identifiable.Descriptions)
                {
                    this.Descriptions.Add(new TextTypeWrapperMutableCore(textTypeWrapper));
                }
            }

            if (identifiable.Name != null)
            {
                foreach (var textTypeWrapper in identifiable.Names)
                {
                    this.Names.Add(new TextTypeWrapperMutableCore(textTypeWrapper));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableMutableSuperObject" /> class.
        /// </summary>
        public NameableMutableSuperObject()
        {
            this.Descriptions = new List<ITextTypeWrapperMutableObject>();
            this.Names = new List<ITextTypeWrapperMutableObject>();
        }

        /// <summary>
        ///     Gets the descriptions.
        /// </summary>
        public IList<ITextTypeWrapperMutableObject> Descriptions { get; private set; }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name
        {
            get
            {
                if (this.Names != null && this.Names.Count > 0)
                {
                    return this.Names[0].Value;
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets the names.
        /// </summary>
        public IList<ITextTypeWrapperMutableObject> Names { get; private set; }
    }
}