// -----------------------------------------------------------------------
// <copyright file="ItemMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     ItemMutableSuperObject class
    /// </summary>
    /// <typeparam name="T">The generic type</typeparam>
    /// <typeparam name="TV">The TV type</typeparam>
    public abstract class ItemMutableSuperObject<T, TV> : NameableMutableSuperObject
        where T : IMaintainableMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemMutableSuperObject{T, TV}" /> class.
        /// </summary>
        /// <param name="itemScheme">The item scheme.</param>
        /// <param name="item">The item.</param>
        /// <exception cref="ArgumentNullException"><paramref name="item"/> is <see langword="null" />.</exception>
        protected ItemMutableSuperObject(
            T itemScheme, Api.Model.SuperObjects.Base.IItemSuperObject<TV> item)
            : base(item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            this.ItemScheme = itemScheme;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemMutableSuperObject{T, TV}" /> class.
        /// </summary>
        protected ItemMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets or sets the item scheme.
        /// </summary>
        /// <value>
        ///     The item scheme.
        /// </value>
        public T ItemScheme { get; set; }
    }
}