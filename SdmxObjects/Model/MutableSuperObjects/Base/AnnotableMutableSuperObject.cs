// -----------------------------------------------------------------------
// <copyright file="AnnotableMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The AnnotableMutableSuperObject class
    /// </summary>
    public abstract class AnnotableMutableSuperObject : IAnnotableMutableSuperObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotableMutableSuperObject"/> class.
        /// </summary>
        /// <param name="superBean">The super bean.</param>
        /// <exception cref="ArgumentNullException"><paramref name="superBean"/> is <see langword="null" />.</exception>
        protected AnnotableMutableSuperObject(IAnnotableSuperObject superBean)
        {
            if (superBean == null)
            {
                throw new ArgumentNullException("superBean");
            }

            if (superBean.Annotations != null)
            {
                this.Annotations = new HashSet<IAnnotationMutableSuperObject>();
                foreach (var annotation in superBean.Annotations)
                {
                    this.Annotations.Add(new AnnotationMutableSuperObject(annotation));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableSuperObject" /> class.
        /// </summary>
        /// <param name="superBean">The super bean.</param>
        /// <exception cref="ArgumentNullException"><paramref name="superBean"/> is <see langword="null" />.</exception>
        protected AnnotableMutableSuperObject([ValidatedNotNull]IAnnotableObject superBean)
        {
            if (superBean == null)
            {
                throw new ArgumentNullException("superBean");
            }

            if (superBean.Annotations != null)
            {
                this.Annotations = new HashSet<IAnnotationMutableSuperObject>();
                foreach (var annotationSuperObject in superBean.Annotations)
                {
                    this.Annotations.Add(new AnnotationMutableSuperObject(annotationSuperObject));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableMutableSuperObject" /> class.
        /// </summary>
        protected AnnotableMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets a list of annotations that exist for this Annotable Object
        /// </summary>
        public ISet<IAnnotationMutableSuperObject> Annotations { get; private set; }
    }
}