// -----------------------------------------------------------------------
// <copyright file="GroupMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.DataStructure
{
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     GroupMutableSuperObject class
    /// </summary>
    public class GroupMutableSuperObject : IdentifiableMutableObject, IGroupMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupMutableSuperObject" /> class.
        /// </summary>
        /// <param name="group">The group.</param>
        public GroupMutableSuperObject(IGroupSuperObject group)
            : base(group)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupMutableSuperObject" /> class.
        /// </summary>
        public GroupMutableSuperObject()
        {
        }
    }
}