// -----------------------------------------------------------------------
// <copyright file="CodeMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Codelist
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     CodeMutableSuperObject class
    /// </summary>
    public class CodeMutableSuperObject : ItemMutableSuperObject<CodelistMutableSuperObject, ICodelistSuperObject>, 
                                          ICodeMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeMutableSuperObject" /> class.
        /// </summary>
        /// <param name="codelist">The codelist.</param>
        /// <param name="code">The code.</param>
        /// <param name="parent">The parent.</param>
        public CodeMutableSuperObject(
            CodelistMutableSuperObject codelist, 
            ICodeSuperObject code, 
            CodeMutableSuperObject parent)
            : base(codelist, code)
        {
            if (code.HasChildren())
            {
                this.Children = new List<ICodeMutableSuperObject>();

                foreach (var codeSuperObject in code.Children)
                {
                    this.Children.Add(new CodeMutableSuperObject(codelist, codeSuperObject, this));
                }
            }

            this.Parent = parent;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeMutableSuperObject" /> class.
        /// </summary>
        public CodeMutableSuperObject()
        {
        }

        /// <summary>
        ///     Gets the children.
        /// </summary>
        public IList<ICodeMutableSuperObject> Children { get; private set; }

        /// <summary>
        ///     Gets or sets the parent.
        /// </summary>
        public ICodeMutableSuperObject Parent { get; set; }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}