// -----------------------------------------------------------------------
// <copyright file="CodeRefMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Codelist
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     CodeRefMutableSuperObject class
    /// </summary>
    public class CodeRefMutableSuperObject : IdentifiableMutableObject, ICodeRefMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeRefMutableSuperObject" /> class.
        /// </summary>
        public CodeRefMutableSuperObject()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeRefMutableSuperObject" /> class.
        /// </summary>
        /// <param name="codeRefSuperBean">The code reference super bean.</param>
        /// <exception cref="ArgumentNullException"><paramref name="codeRefSuperBean"/> is <see langword="null" />.</exception>
        public CodeRefMutableSuperObject(IHierarchicalCodeSuperObject codeRefSuperBean)
            : base(codeRefSuperBean)
        {
            if (codeRefSuperBean == null)
            {
                throw new ArgumentNullException("codeRefSuperBean");
            }

            this.Code = new CodeMutableCore(codeRefSuperBean.Code);
            this.CodelistRef = codeRefSuperBean.Code.MaintainableParent.AsReference.MaintainableReference;
            this.CodeRefs = new List<ICodeRefMutableSuperObject>();
            foreach (var hierarchicalCodeSuperObject in codeRefSuperBean.CodeRefs)
            {
                this.CodeRefs.Add(new CodeRefMutableSuperObject(hierarchicalCodeSuperObject));
            }
        }

        /// <summary>
        ///     Gets or sets the code.
        /// </summary>
        public ICodeMutableObject Code { get; set; }

        /// <summary>
        ///     Gets or sets the codelist reference.
        /// </summary>
        /// <value>
        ///     The codelist reference.
        /// </value>
        public IMaintainableRefObject CodelistRef { get; set; }

        /// <summary>
        ///     Gets the code refs.
        /// </summary>
        public IList<ICodeRefMutableSuperObject> CodeRefs { get; private set; }
    }
}