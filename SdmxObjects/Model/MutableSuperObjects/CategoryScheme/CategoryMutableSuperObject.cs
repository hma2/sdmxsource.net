// -----------------------------------------------------------------------
// <copyright file="CategoryMutableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.CategoryScheme
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.MutableSuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.MutableSuperObjects.Base;

    /// <summary>
    ///     CategoryMutableSuperObject class
    /// </summary>
    public class CategoryMutableSuperObject :
        ItemMutableSuperObject<ICategorySchemeMutableSuperObject, ICategorySchemeSuperObject>, 
        ICategoryMutableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryMutableSuperObject" /> class.
        /// </summary>
        /// <param name="categopryScheme">The categopry scheme.</param>
        /// <param name="cat">The cat.</param>
        /// <param name="parent">The parent.</param>
        public CategoryMutableSuperObject(
            ICategorySchemeMutableSuperObject categopryScheme, 
            ICategorySuperObject cat, 
            ICategoryMutableSuperObject parent)
            : base(categopryScheme, cat)
        {
            this.Parent = parent;
            this.Children = new List<ICategoryMutableSuperObject>();
            if (cat.HasChildren())
            {
                foreach (var child in cat.Children)
                {
                    this.Children.Add(new CategoryMutableSuperObject(categopryScheme, child, this));
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryMutableSuperObject" /> class.
        /// </summary>
        public CategoryMutableSuperObject()
        {
            this.Children = new List<ICategoryMutableSuperObject>();
        }

        /// <summary>
        ///     Gets the children.
        /// </summary>
        public IList<ICategoryMutableSuperObject> Children { get; private set; }

        /// <summary>
        ///     Gets or sets the parent.
        /// </summary>
        public ICategoryMutableSuperObject Parent { get; set; }
    }
}