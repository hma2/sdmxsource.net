// -----------------------------------------------------------------------
// <copyright file="PartyCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The party core.
    /// </summary>
    [Serializable]
    public class PartyCore : IParty
    {
        /// <summary>
        ///     The _time zone regex
        /// </summary>
        private static readonly Regex _timeZoneRegex = new Regex(
            "^(\\+|\\-)(14:00|((0[0-9]|1[0-3]):[0-5][0-9]))$", 
            RegexOptions.Compiled);

        /// <summary>
        ///     The _name.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _name;

        /// <summary>
        ///     The contacts.
        /// </summary>
        private readonly IList<IContact> _contacts;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The time zone.
        /// </summary>
        private readonly string _timeZone;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartyCore" /> class.
        /// </summary>
        /// <param name="name">
        ///     The name 0.
        /// </param>
        /// <param name="id">
        ///     The id 1.
        /// </param>
        /// <param name="contacts">
        ///     The contacts 2.
        /// </param>
        /// <param name="timeZone">
        ///     The time zone 3.
        /// </param>
        public PartyCore(IList<ITextTypeWrapper> name, string id, IList<IContact> contacts, string timeZone)
            : this()
        {
            if (name != null)
            {
                this._name = new List<ITextTypeWrapper>(name);
            }

            this._id = id;
            if (contacts != null)
            {
                this._contacts = new List<IContact>(contacts);
            }

            this._timeZone = timeZone;
            this.Validate();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //////////BUILD FROM V2.1 SCHEMA        /////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartyCore" /> class.
        /// </summary>
        /// <param name="partyType">
        ///     The party type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="partyType"/> is <see langword="null" />.</exception>
        public PartyCore(PartyType partyType)
            : this()
        {
            if (partyType == null)
            {
                throw new ArgumentNullException("partyType");
            }

            this._id = partyType.id;

            if (ObjectUtil.ValidCollection(partyType.Name))
            {
                foreach (Name tt in partyType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt, null));
                }
            }

            if (ObjectUtil.ValidCollection(partyType.Contact))
            {
                foreach (ContactType contactType in partyType.Contact)
                {
                    this._contacts.Add(new ContactCore(contactType));
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartyCore" /> class.
        /// </summary>
        /// <param name="senderType">
        ///     The sender type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="senderType"/> is <see langword="null" />.</exception>
        public PartyCore(SenderType senderType)
            : this()
        {
            if (senderType == null)
            {
                throw new ArgumentNullException("senderType");
            }

            this._id = senderType.id;

            if (ObjectUtil.ValidCollection(senderType.Name))
            {
                foreach (Name tt in senderType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt, null));
                }
            }

            if (ObjectUtil.ValidCollection(senderType.Contact))
            {
                foreach (ContactType contactType in senderType.Contact)
                {
                    this._contacts.Add(new ContactCore(contactType));
                }
            }

            this._timeZone = senderType.Timezone;
            this.Validate();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //////////BUILD FROM V2.0 SCHEMA        /////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartyCore" /> class.
        /// </summary>
        /// <param name="partyType">
        ///     The party type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="partyType"/> is <see langword="null" />.</exception>
        public PartyCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.PartyType partyType)
            : this()
        {
            if (partyType == null)
            {
                throw new ArgumentNullException("partyType");
            }

            this._id = partyType.id;
            if (ObjectUtil.ValidCollection(partyType.Name))
            {
                foreach (TextType tt in partyType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt, null));
                }
            }

            if (ObjectUtil.ValidCollection(partyType.Contact))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.ContactType contactType in partyType.Contact)
                {
                    this._contacts.Add(new ContactCore(contactType));
                }
            }

            this.Validate();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //////////BUILD FROM V1.0 SCHEMA        /////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="PartyCore" /> class.
        /// </summary>
        /// <param name="partyType">
        ///     The party type.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="partyType"/> is <see langword="null" />.</exception>
        public PartyCore(Org.Sdmx.Resources.SdmxMl.Schemas.V10.message.PartyType partyType)
            : this()
        {
            if (partyType == null)
            {
                throw new ArgumentNullException("partyType");
            }

            this._id = partyType.id;
            if (ObjectUtil.ValidCollection(partyType.Name))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType tt in partyType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt, null));
                }
            }

            if (ObjectUtil.ValidCollection(partyType.Contact))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.message.ContactType contactType in partyType.Contact)
                {
                    this._contacts.Add(new ContactCore(contactType));
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Prevents a default instance of the <see cref="PartyCore" /> class from being created.
        /// </summary>
        private PartyCore()
        {
            this._name = new List<ITextTypeWrapper>();
            this._contacts = new List<IContact>();
        }

        /// <summary>
        ///     Gets the contacts.
        /// </summary>
        public virtual IList<IContact> Contacts
        {
            get
            {
                return new List<IContact>(this._contacts);
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public virtual IList<ITextTypeWrapper> Name
        {
            get
            {
                return new List<ITextTypeWrapper>(this._name);
            }
        }

        /// <summary>
        ///     Gets the time zone.
        /// </summary>
        public virtual string TimeZone
        {
            get
            {
                return this._timeZone;
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._id))
            {
                throw new SdmxSemmanticException("Party missing mandatory id");
            }

            if (this._timeZone != null)
            {
                // Pattern idPattern = ILOG.J2CsMapping.Text.Pattern.Compile("(\\+|\\-)(14:00|((0[0-9]|1[0-3]):[0-5][0-9]))");
                if (!_timeZoneRegex.IsMatch(this._timeZone))
                {
                    throw new SdmxSemmanticException(
                        "Time zone '" + this._timeZone
                        + "' is in an invalid format. please ensure the format matches the patttern (\\+|\\-)(14:00|((0[0-9]|1[0-3]):[0-5][0-9]) example +12:30");
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //////////VALIDATE                        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM ATTRIBUTES               ////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}