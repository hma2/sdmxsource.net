// -----------------------------------------------------------------------
// <copyright file="DatasetStructureReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     The dataset structure reference core.
    /// </summary>
    [Serializable]
    public class DatasetStructureReferenceCore : IDatasetStructureReference
    {
        /// <summary>
        ///     The service url.
        /// </summary>
        private readonly Uri _serviceUrl;

        /// <summary>
        ///     The structure reference.
        /// </summary>
        private readonly IStructureReference _structureReference;

        /// <summary>
        ///     The structure url.
        /// </summary>
        private readonly Uri _structureUrl;

        /// <summary>
        ///     The dimension at observation.
        /// </summary>
        private string _dimensionAtObservation;

        /// <summary>
        ///     The id.
        /// </summary>
        private string _id;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatasetStructureReferenceCore" /> class.
        ///     Minimal Constructor
        /// </summary>
        /// <param name="structureReference">
        ///     CategorisationStructure reference object
        /// </param>
        public DatasetStructureReferenceCore(IStructureReference structureReference)
        {
            this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;

            this._structureReference = structureReference;
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatasetStructureReferenceCore" /> class.
        /// </summary>
        /// <param name="id0">
        ///     The id 0.
        /// </param>
        /// <param name="structureReference1">
        ///     The structure reference 1.
        /// </param>
        /// <param name="serviceUrl2">
        ///     The service url 2.
        /// </param>
        /// <param name="structureUrl3">
        ///     The structure url 3.
        /// </param>
        /// <param name="dimensionAtObservation4">
        ///     The dimension at observation 4.
        /// </param>
        public DatasetStructureReferenceCore(
            string id0, 
            IStructureReference structureReference1, 
            Uri serviceUrl2, 
            Uri structureUrl3, 
            string dimensionAtObservation4)
        {
            this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            this._id = id0;
            this._structureReference = structureReference1;
            this._serviceUrl = serviceUrl2;
            this._structureUrl = structureUrl3;
            if (!string.IsNullOrWhiteSpace(dimensionAtObservation4))
            {
                this._dimensionAtObservation = dimensionAtObservation4;
            }

            this.Validate();
        }

        // ///////////////////////////////////////////////////////////////////////////////////////////////////
        // ////////////BUILD FROM V2.1 XMLStreamReader    ///////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////////////////////////////////////
        // public DatasetStructureReferenceCore(XMLStreamReader reader) {
        // }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA            ///////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatasetStructureReferenceCore" /> class.
        /// </summary>
        /// <param name="payloadSt">
        ///     The payload st.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="payloadSt"/> is <see langword="null" />.</exception>
        public DatasetStructureReferenceCore(PayloadStructureType payloadSt)
        {
            if (payloadSt == null)
            {
                throw new ArgumentNullException("payloadSt");
            }

            this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            if (payloadSt.ProvisionAgrement != null)
            {
                this._structureReference = RefUtil.CreateReference(payloadSt.ProvisionAgrement);
            }
            else if (payloadSt.StructureUsage != null)
            {
                this._structureReference = RefUtil.CreateReference(payloadSt.StructureUsage);
            }
            else if (payloadSt.Structure != null)
            {
                this._structureReference = RefUtil.CreateReference(payloadSt.Structure);
            }

            this._id = payloadSt.structureID;
            this._serviceUrl = payloadSt.serviceURL;
            this._structureUrl = payloadSt.structureURL;
            this._dimensionAtObservation = payloadSt.dimensionAtObservation.ToString();
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatasetStructureReferenceCore" /> class.
        /// </summary>
        /// <param name="payloadSt">
        ///     The payload st.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="payloadSt"/> is <see langword="null" />.</exception>
        public DatasetStructureReferenceCore(GenericMetadataStructureType payloadSt)
        {
            if (payloadSt == null)
            {
                throw new ArgumentNullException("payloadSt");
            }
            
            this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            var metadataStructureReferenceType =
                payloadSt.MetadataStructureReferenceType.GetTypedRef<MetadataStructureRefType>();
            if (payloadSt.ProvisionAgrement != null)
            {
                this._structureReference = new StructureReferenceImpl(
                    metadataStructureReferenceType.agencyID, 
                    metadataStructureReferenceType.id, 
                    metadataStructureReferenceType.version, 
                    SdmxStructureEnumType.ProvisionAgreement);
            }
            else if (payloadSt.StructureUsage != null)
            {
                this._structureReference = new StructureReferenceImpl(
                    metadataStructureReferenceType.agencyID, 
                    metadataStructureReferenceType.id, 
                    metadataStructureReferenceType.version, 
                    SdmxStructureEnumType.MetadataFlow);
            }
            else
            {
                if (metadataStructureReferenceType != null)
                {
                    this._structureReference = new StructureReferenceImpl(
                        metadataStructureReferenceType.agencyID, 
                        metadataStructureReferenceType.id, 
                        metadataStructureReferenceType.version, 
                        SdmxStructureEnumType.Msd);
                }
            }

            this._id = payloadSt.structureID;
            this._serviceUrl = payloadSt.serviceURL;
            this._structureUrl = payloadSt.structureURL;
            this.Validate();
        }

        /// <summary>
        ///     Gets the dimension at observation.
        /// </summary>
        public string DimensionAtObservation
        {
            get
            {
                return this._dimensionAtObservation;
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the service url.
        /// </summary>
        public Uri ServiceUrl
        {
            get
            {
                return this._serviceUrl;
            }
        }

        /// <summary>
        ///     Gets the structure reference.
        /// </summary>
        public IStructureReference StructureReference
        {
            get
            {
                return this._structureReference;
            }
        }

        /// <summary>
        ///     Gets the structure url.
        /// </summary>
        public Uri StructureUrl
        {
            get
            {
                return this._structureUrl;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether time series.
        /// </summary>
        public bool Timeseries
        {
            get
            {
                return this._dimensionAtObservation.Equals(DimensionObject.TimeDimensionFixedId);
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._id))
            {
                this._id = Guid.NewGuid().ToString();
            }

            if (this._structureReference == null)
            {
                throw new SdmxSemmanticException("Header 'CategorisationStructure' missing ObjectStructure Reference");
            }

            if (this._dimensionAtObservation == null)
            {
                this._dimensionAtObservation = DimensionObject.TimeDimensionFixedId;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //////////VALIDATE                        /////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////////////////
    }
}