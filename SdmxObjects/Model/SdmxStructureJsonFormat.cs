﻿// -----------------------------------------------------------------------
// <copyright file="SdmxStructureFormat.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model
{
    using System;
    using System.Text;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Translator;

    /// <summary>
    ///     SDMX Structure Format class for Json
    /// </summary>
    public class SdmxStructureJsonFormat : AbstractSdmxStructureFormat
    {
        /// <summary>
        /// Encoding used in the Json file
        /// </summary>
        private readonly Encoding _encoding;

        /// <summary>
        /// Encoding used in the Json file
        /// </summary>
        private readonly ITranslator _translator;

        /// <summary>
        /// The requested Artefact
        /// </summary>
        private readonly SdmxStructureEnumType _requestedStructureType;

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxStructureJsonFormat"/> class. 
        /// </summary>
        /// <param name="requestedStructureType">
        /// The requested Structure Type.
        /// </param>
        /// <param name="encoding">
        /// The encoding.
        /// </param>
        /// <param name="translator">
        /// The translator.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// encoding
        /// or
        /// translator
        /// </exception>
        /// <exception cref="ArgumentException">
        /// STRUCTURE_OUTPUT_FORMAT can not be null
        /// </exception>
        public SdmxStructureJsonFormat(SdmxStructureEnumType requestedStructureType, Encoding encoding, ITranslator translator)
            : base(StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.Json))
        {
            if (encoding == null)
            {
                throw new ArgumentNullException("encoding");
            }

            if (translator == null)
            {
                throw new ArgumentNullException("translator");
            }

            this._requestedStructureType = requestedStructureType;
            this._encoding = encoding;
            this._translator = translator;
        }

        /// <summary>
        /// Gets the requested Artefact
        /// </summary>
        public SdmxStructureEnumType RequestedStructureType
        {
            get
            {
                return this._requestedStructureType;
            }
        }

        /// <summary>
        /// Gets the Encoding used in the Json file
        /// </summary>
        public Encoding Encoding
        {
            get
            {
                return this._encoding;
            }
        }

        /// <summary>
        /// Gets the Translator
        /// </summary>
        public ITranslator Translator
        {
            get
            {
                return this._translator;
            }
        }
    }
}
