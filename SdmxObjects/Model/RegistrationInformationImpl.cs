// -----------------------------------------------------------------------
// <copyright file="RegistrationInformationImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    /// <summary>
    ///     The registration information impl.
    /// </summary>
    public class RegistrationInformationImpl : IRegistrationInformation
    {
        /// <summary>
        ///     The _action.
        /// </summary>
        private readonly DatasetAction _action;

        /// <summary>
        ///     The _registration.
        /// </summary>
        private readonly IRegistrationObject _registration;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RegistrationInformationImpl" /> class.
        /// </summary>
        /// <param name="action">
        ///     The action.
        /// </param>
        /// <param name="registration">
        ///     The registration codelistRef.
        /// </param>
        public RegistrationInformationImpl(DatasetAction action, IRegistrationObject registration)
        {
            this._action = action;
            this._registration = registration;
        }

        /// <summary>
        ///     Gets the registration.
        /// </summary>
        public virtual IRegistrationObject Registration
        {
            get
            {
                return this._registration;
            }
        }

        /// <summary>
        ///     Gets the registration action.
        /// </summary>
        public virtual DatasetAction RegistrationAction
        {
            get
            {
                return this._action;
            }
        }
    }
}