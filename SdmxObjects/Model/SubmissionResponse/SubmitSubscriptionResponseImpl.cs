// -----------------------------------------------------------------------
// <copyright file="SubmitSubscriptionResponseImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SubmissionResponse
{
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SubmissionResponse;

    /// <summary>
    ///     The submit subscription response impl.
    /// </summary>
    public class SubmitSubscriptionResponseImpl : SubmitStructureResponseImpl, ISubmitSubscriptionResponse
    {
        /// <summary>
        ///     The _subscriber id.
        /// </summary>
        private readonly string _subscriberId;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SubmitSubscriptionResponseImpl" /> class.
        /// </summary>
        /// <param name="structureReference">
        ///     The structure reference.
        /// </param>
        /// <param name="errorList">
        ///     The error list.
        /// </param>
        /// <param name="subscriberId">
        ///     The subscriber id.
        /// </param>
        public SubmitSubscriptionResponseImpl(
            IStructureReference structureReference, 
            IErrorList errorList, 
            string subscriberId)
            : base(structureReference, errorList)
        {
            this._subscriberId = subscriberId;
        }

        /// <summary>
        ///     Gets the subscriber assigned id.
        /// </summary>
        public virtual string SubscriberAssignedId
        {
            get
            {
                return this._subscriberId;
            }
        }
    }
}