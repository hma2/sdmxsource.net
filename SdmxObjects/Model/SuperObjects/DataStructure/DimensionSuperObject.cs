﻿// -----------------------------------------------------------------------
// <copyright file="DimensionSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     DimensionSuperObject class
    /// </summary>
    public class DimensionSuperObject : ComponentSuperObject, IDimensionSuperObject
    {
        /// <summary>
        ///     The dimension bean
        /// </summary>
        private readonly IDimension _dimensionBean;

        /// <summary>
        ///     The hierarchical code list
        /// </summary>
        private readonly ISet<IHierarchicalCodelistSuperObject> _hcls;

        /// <summary>
        ///     The is frequeny dimension
        /// </summary>
        private readonly bool _isFrequenyDimension;

        /// <summary>
        ///     The is measure dimension
        /// </summary>
        private readonly bool _isMeasureDimension;

        /// <summary>
        ///     The is time dimension
        /// </summary>
        private readonly bool _isTimeDimension;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionSuperObject" /> class.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <param name="codelist">The codelist.</param>
        /// <param name="concept">The concept.</param>
        public DimensionSuperObject(IDimension dimension, ICodelistSuperObject codelist, IConceptSuperObject concept)
            : this(dimension, codelist, concept, null)
        {
            this._dimensionBean = dimension;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionSuperObject" /> class.
        /// </summary>
        /// <param name="dimension">The dimension.</param>
        /// <param name="codelist">The codelist.</param>
        /// <param name="concept">The concept.</param>
        /// <param name="hcls">The HCLS.</param>
        /// <exception cref="SdmxSemmanticException">Semantic exception</exception>
        public DimensionSuperObject(
            IDimension dimension, 
            ICodelistSuperObject codelist, 
            IConceptSuperObject concept, 
            ISet<IHierarchicalCodelistSuperObject> hcls)
            : base(dimension, codelist, concept)
        {
            this._dimensionBean = dimension;
            this._isFrequenyDimension = dimension.FrequencyDimension;
            this._isMeasureDimension = dimension.MeasureDimension;
            this._isTimeDimension = dimension.TimeDimension;

            this._hcls = hcls;
            if (hcls == null)
            {
                this._hcls = new HashSet<IHierarchicalCodelistSuperObject>();
            }

            if (concept == null)
            {
                // JAVA_REQUIRED_OBJECT_NULL
                throw new SdmxSemmanticException(
                    ExceptionCode.JavaPropertyNotFound, 
                    typeof(IConceptSuperObject).FullName);
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IDimension BuiltFrom
        {
            get
            {
                return this._dimensionBean;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this <see cref="DimensionSuperObject" /> is frequency.
        /// </summary>
        /// <value>
        ///     <c>true</c> if frequency; otherwise, <c>false</c>.
        /// </value>
        public bool Frequency
        {
            get
            {
                return this._isFrequenyDimension;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the dimension is the frequency dimension, false otherwise.
        /// </summary>
        public bool FrequencyDimension
        {
            get
            {
                return this._isFrequenyDimension;
            }
        }

        /// <summary>
        ///     Gets the hierarchical codelists.
        /// </summary>
        /// <value>
        ///     The hierarchical codelists.
        /// </value>
        public ISet<IHierarchicalCodelistSuperObject> HierarchicalCodelists
        {
            get
            {
                return new HashSet<IHierarchicalCodelistSuperObject>(this._hcls);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the dimension is the measure dimension, false otherwise.
        /// </summary>
        public bool MeasureDimension
        {
            get
            {
                return this._isMeasureDimension;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether time dimension.
        /// </summary>
        public bool TimeDimension
        {
            get
            {
                return this._isTimeDimension;
            }
        }
    }
}