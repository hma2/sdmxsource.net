﻿// -----------------------------------------------------------------------
// <copyright file="DataStructureSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     DataStructureSuperObject class
    /// </summary>
    /// <seealso cref="MaintainableSuperObject" />
    /// <seealso cref="MaintainableSuperObject" />
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure.IDataStructureSuperObject" />
    public class DataStructureSuperObject : MaintainableSuperObject, IDataStructureSuperObject
    {
        /// <summary>
        ///     The attributes
        /// </summary>
        private readonly IList<IAttributeSuperObject> _attributes;

        /// <summary>
        ///     The concept codelist map
        /// </summary>
        private readonly IDictionary<string, ICodelistSuperObject> _conceptCodelistMap =
            new Dictionary<string, ICodelistSuperObject>();

        /// <summary>
        ///     The concept component map
        /// </summary>
        private readonly IDictionary<string, IComponentSuperObject> _conceptComponentMap =
            new Dictionary<string, IComponentSuperObject>();

        /// <summary>
        ///     The concept dimension map
        /// </summary>
        private readonly IDictionary<string, IDimensionSuperObject> _conceptDimensionMap;

        /// <summary>
        ///     The dimensions
        /// </summary>
        private readonly IList<IDimensionSuperObject> _dimensions;

        /// <summary>
        ///     The groups
        /// </summary>
        private readonly IList<IGroupSuperObject> _groups = new List<IGroupSuperObject>();

        /// <summary>
        ///     The key family
        /// </summary>
        private readonly IDataStructureObject _keyFamily;

        /// <summary>
        ///     The primary measure
        /// </summary>
        private readonly IPrimaryMeasureSuperObject _primaryMeasure;

        /// <summary>
        ///     The referenced codelists
        /// </summary>
        private readonly ISet<ICodelistSuperObject> _referencedCodelists = new HashSet<ICodelistSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureSuperObject" /> class.
        /// </summary>
        /// <param name="dataStructure">The data structure.</param>
        /// <param name="dimensions">The dimensions.</param>
        /// <param name="attributes">The attributes.</param>
        /// <param name="primaryMeasure">The primary measure.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dimensions"/> is <see langword="null" />.</exception>
        public DataStructureSuperObject(
            IDataStructureObject dataStructure, 
            IList<IDimensionSuperObject> dimensions, 
            IList<IAttributeSuperObject> attributes, 
            IPrimaryMeasureSuperObject primaryMeasure)
            : base(dataStructure)
        {
            if (dimensions == null)
            {
                throw new ArgumentNullException("dimensions");
            }

            if (attributes == null)
            {
                throw new ArgumentNullException("attributes");
            }

            if (primaryMeasure == null)
            {
                throw new ArgumentNullException("primaryMeasure");
            }

            this._keyFamily = dataStructure;
            this._dimensions = dimensions;
            this._attributes = attributes;
            this._primaryMeasure = primaryMeasure;

            // Create Mapping of Concept Id To Dimension that refers to that concept
            this._conceptDimensionMap = new Dictionary<string, IDimensionSuperObject>();

            foreach (var currentDimension in dimensions)
            {
                this._conceptDimensionMap.Add(currentDimension.Id, currentDimension);
                this._conceptComponentMap.Add(currentDimension.Id, currentDimension);
                if (currentDimension.GetCodelist(true) != null)
                {
                    this._conceptCodelistMap.Add(currentDimension.Id, currentDimension.GetCodelist(true));
                    this._referencedCodelists.Add(currentDimension.GetCodelist(true));
                }
            }

            if (attributes != null)
            {
                foreach (var currentBean in attributes)
                {
                    this._conceptComponentMap.Add(currentBean.Id, currentBean);
                    if (currentBean.GetCodelist(true) != null)
                    {
                        this._conceptCodelistMap.Add(currentBean.Id, currentBean.GetCodelist(true));
                        this._referencedCodelists.Add(currentBean.GetCodelist(true));
                    }
                }
            }

            if (primaryMeasure != null)
            {
                this._conceptComponentMap.Add(primaryMeasure.Id, primaryMeasure);
                if (primaryMeasure.GetCodelist(true) != null)
                {
                    this._conceptCodelistMap.Add(primaryMeasure.Id, primaryMeasure.GetCodelist(true));
                    this._referencedCodelists.Add(primaryMeasure.GetCodelist(true));
                }
            }

            foreach (var currentGroup in dataStructure.Groups)
            {
                this._groups.Add(new GroupSuperObject(currentGroup, this));
            }
        }

        /// <summary>
        ///     Gets a list of all the attributes in this DataStructure.
        ///     If there are no attributes then an empty list will be returned.
        /// </summary>
        /// <value>
        ///     list of attributes
        /// </value>
        public IList<IAttributeSuperObject> Attributes
        {
            get
            {
                return new List<IAttributeSuperObject>(this._attributes);
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IDataStructureObject BuiltFrom
        {
            get
            {
                return this._keyFamily;
            }
        }

        /// <summary>
        ///     Gets a set of all the components used within this DataStructure.
        /// </summary>
        public ISet<IComponentSuperObject> Components
        {
            get
            {
                return new HashSet<IComponentSuperObject>(this._conceptComponentMap.Values);
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this._keyFamily);

                foreach (var dim in this._dimensions)
                {
                    foreach (var dimOb in dim.CompositeObjects)
                    {
                        returnSet.Add(dimOb);
                    }
                }

                foreach (var attr in this._attributes)
                {
                    foreach (var attrOb in attr.CompositeObjects)
                    {
                        returnSet.Add(attrOb);
                    }
                }

                foreach (var grp in this._groups)
                {
                    foreach (var grpOb in grp.CompositeObjects)
                    {
                        returnSet.Add(grpOb);
                    }
                }

                foreach (var pmob in this._primaryMeasure.CompositeObjects)
                {
                    returnSet.Add(pmob);
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets a subset of the key family attributes.
        ///     Gets the attributes with Attachment Level of DataSet.
        ///     If no such attributes exist then an empty list will be returned.
        /// </summary>
        /// <value>
        ///     list of attributes
        /// </value>
        public IList<IAttributeSuperObject> DatasetAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.DataSet);
            }
        }

        /// <summary>
        ///     Gets the i data structure object.
        /// </summary>
        public IDataStructureObject DataStructureObject
        {
            get
            {
                return this._keyFamily;
            }
        }

        /// <summary>
        ///     Gets a list of all the dimensions in the DataStructure.
        ///     This does not include the primary measure dimension.
        ///     If there are no dimensions then an empty list will be returned.
        /// </summary>
        /// <value>
        ///     List of dimensions
        /// </value>
        public IList<IDimensionSuperObject> Dimensions
        {
            get
            {
                return new List<IDimensionSuperObject>(this._dimensions);
            }
        }

        /// <summary>
        ///     Gets a set of all the attributes attached to any group in the DataStructure.
        ///     If no such attributes exist then an empty list will be returned.
        /// </summary>
        /// <value>
        ///     list of attributes
        /// </value>
        public ISet<IAttributeSuperObject> GroupAttributes
        {
            get
            {
                var allGroupAttributes = this.GetAttribute(AttributeAttachmentLevel.Group);
                return new HashSet<IAttributeSuperObject>(allGroupAttributes);
            }
        }

        /// <summary>
        ///     Gets all the groups in the DataStructure.
        ///     If there are no groups an empty list will be returned.
        /// </summary>
        /// <value>
        ///     List of groups
        /// </value>
        public IList<IGroupSuperObject> Groups
        {
            get
            {
                return new List<IGroupSuperObject>(this._groups);
            }
        }

        /// <summary>
        ///     Gets a subset of the DataStructure attributes.
        ///     Gets the attributes with Attachment Level of Observation.
        ///     If no such attributes exist then an empty list will be returned.
        /// </summary>
        /// <value>
        ///     list of attributes
        /// </value>
        public IList<IAttributeSuperObject> ObservationAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.Observation);
            }
        }

        /// <summary>
        ///     Gets the primary measure for this DataStructure.
        ///     If there is no primary measure then null will be returned.
        /// </summary>
        /// <value>
        ///     list of IPrimaryMeasureSuperObject
        /// </value>
        public IPrimaryMeasureSuperObject PrimaryMeasure
        {
            get
            {
                return this._primaryMeasure;
            }
        }

        /// <summary>
        ///     Gets a set of all the codelists referenced within this DataStructure.
        /// </summary>
        public ISet<ICodelistSuperObject> ReferencedCodelists
        {
            get
            {
                return new HashSet<ICodelistSuperObject>(this._referencedCodelists);
            }
        }

        /// <summary>
        ///     Gets a set of all the concepts referenced within this DataStructure.
        /// </summary>
        public ISet<IConceptSuperObject> ReferencedConcepts
        {
            get
            {
                ISet<IConceptSuperObject> returnConcepts = new HashSet<IConceptSuperObject>();
                foreach (var currentComponent in this._conceptComponentMap.Values)
                {
                    returnConcepts.Add(currentComponent.Concept);
                }

                return returnConcepts;
            }
        }

        /// <summary>
        ///     Gets a subset of the DataStructure attributes.
        ///     Gets the attributes with Attachment Level of Series.
        /// </summary>
        /// <value>
        ///     list of attributes
        /// </value>
        public IList<IAttributeSuperObject> SeriesAttributes
        {
            get
            {
                return this.GetAttribute(AttributeAttachmentLevel.DimensionGroup);
            }
        }

        /// <summary>
        ///     Gets the time dimension from this DataStructure.
        /// </summary>
        /// <value>
        ///     The time dimension @object
        /// </value>
        public IDimensionSuperObject TimeDimension
        {
            get
            {
                foreach (var currentDimension in this._dimensions)
                {
                    if (currentDimension.TimeDimension)
                    {
                        return currentDimension;
                    }
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets a list of group identifiers, to which a group level attribute attaches.
        /// </summary>
        /// <param name="id">id of the attribute</param>
        /// <returns>
        ///     list of group identifiers
        /// </returns>
        public string GetAttributeAttachmentGroup(string id)
        {
            foreach (var a in this._attributes)
            {
                if (a.Id.Equals(id))
                {
                    return a.AttachmentGroup;
                }
            }

            return null;
        }

        /// <summary>
        ///     Gets a referenced codelist from a component with the given id.
        ///     If there are no components in the key family that have reference to the given concept
        ///     id or if the referenced component is uncoded then null will be returned.
        /// </summary>
        /// <param name="componentId">the id by which to refer to the codelist.</param>
        /// <returns>
        ///     the codelist referred to by the component id.
        /// </returns>
        public ICodelistSuperObject GetCodelistByComponentId(string componentId)
        {
            return this._conceptCodelistMap.GetOrDefault(componentId);
        }

        /// <summary>
        ///     Gets a component from an id.
        ///     If no such concept exists, null will be returned.
        /// </summary>
        /// <param name="componentId">the id by which to refer to the component.</param>
        /// <returns>
        ///     the component which references the specified id.
        /// </returns>
        public IComponentSuperObject GetComponentById(string componentId)
        {
            return this._conceptComponentMap.GetOrDefault(componentId);
        }

        /// <summary>
        ///     Gets a dimension, that is referenced by the specified id.
        ///     If no such dimension exists for this DataStructure then null will be returned.
        /// </summary>
        /// <param name="dimensionId">the id that the dimension has reference to</param>
        /// <returns>
        ///     the dimension which references the specified id.
        /// </returns>
        public IDimensionSuperObject GetDimensionById(string dimensionId)
        {
            return this._conceptDimensionMap.GetOrDefault(dimensionId);
        }

        /// <summary>
        ///     The get dimensions.
        /// </summary>
        /// <param name="include">The include.</param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IDimensionSuperObject> GetDimensions(params SdmxStructureType[] include)
        {
            if (this._dimensions != null)
            {
                IList<IDimensionSuperObject> returnList = new List<IDimensionSuperObject>();
                foreach (var dim in this._dimensions)
                {
                    if (include != null && include.Length > 0)
                    {
                        foreach (var currentType in include)
                        {
                            if (currentType == dim.BuiltFrom.StructureType)
                            {
                                returnList.Add(dim);
                            }
                        }
                    }
                    else
                    {
                        returnList.Add(dim);
                    }
                }

                return returnList;
            }

            return new List<IDimensionSuperObject>();
        }

        /// <summary>
        ///     Gets the group with the given id.
        ///     <p />
        ///     If no groups exist or no groups have the id, then null will be returned.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        ///     The group.
        /// </returns>
        public IGroupSuperObject GetGroup(string id)
        {
            if (this._groups != null)
            {
                foreach (var currentBean in this._groups)
                {
                    if (currentBean.Id.Equals(id))
                    {
                        return currentBean;
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     Gets a subset of the DataStructure attributes.
        ///     Gets the attributes with Attachment Level of Group, where the group id is the id given.
        ///     If no such attributes exist then an empty list will be returned.
        /// </summary>
        /// <param name="groupId">The group id of the group to return the attributes for</param>
        /// <param name="includeDimensionGroups">
        ///     If true, this will include attributes which are attached to a dimension group, which group the same dimensions as
        ///     the
        ///     group with the given id
        /// </param>
        /// <returns>
        ///     list of attributes
        /// </returns>
        public IList<IAttributeSuperObject> GetGroupAttributes(string groupId, bool includeDimensionGroups)
        {
            IList<IAttributeSuperObject> returnList = new List<IAttributeSuperObject>();
            foreach (var attr in this.BuiltFrom.GetGroupAttributes(groupId, includeDimensionGroups))
            {
                returnList.Add((IAttributeSuperObject)this.GetComponentById(attr.Id));
            }

            return returnList;
        }

        /// <summary>
        ///     Gets the group identifier that matches the dimensions set supplied .
        /// </summary>
        /// <param name="dimensions">a set containing the complete collection of dimensions to be matched with a group</param>
        /// <returns>
        ///     the identification of the group which has the same dimensions as the supplied set
        /// </returns>
        public string GetGroupId(ISet<string> dimensions)
        {
            foreach (var g in this._groups)
            {
                ISet<string> grpDims = new HashSet<string>();
                foreach (var d in g.Dimensions)
                {
                    grpDims.Add(d.Id);
                }

                if (grpDims.Equals(dimensions))
                {
                    return g.Id;
                }
            }

            return null;
        }

        /// <summary>
        ///     Gets the attribute.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The list of attribute super objects</returns>
        private IList<IAttributeSuperObject> GetAttribute(AttributeAttachmentLevel type)
        {
            return this._attributes.Where(currentAttribute => currentAttribute.AttachmentLevel.Equals(type)).ToList();
        }
    }
}