﻿// -----------------------------------------------------------------------
// <copyright file="DataflowSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     DataflowSuperObject class
    /// </summary>
    /// <seealso cref="MaintainableSuperObject" />
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure.IDataflowSuperObject" />
    public class DataflowSuperObject : MaintainableSuperObject, IDataflowSuperObject
    {
        /// <summary>
        ///     The dataflow bean
        /// </summary>
        private readonly IDataflowObject _dataflowBean;

        /// <summary>
        ///     The data structure
        /// </summary>
        private readonly IDataStructureSuperObject _dataStructure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataflowSuperObject" /> class.
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        /// <param name="keyFamily">The key family.</param>
        /// <exception cref="SdmxSemmanticException">DataflowSuperBeanImpl requires DataStructureSuperBean</exception>
        public DataflowSuperObject(IDataflowObject dataflow, IDataStructureSuperObject keyFamily)
            : base(dataflow)
        {
            if (keyFamily == null)
            {
                throw new SdmxSemmanticException("DataflowSuperBeanImpl requires DataStructureSuperBean");
            }

            this._dataStructure = keyFamily;
            this._dataflowBean = dataflow;
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IDataflowObject BuiltFrom
        {
            get
            {
                return this._dataflowBean;
            }
        }

        /// <summary>
        ///     Gets a Data Structure for this dataflow.
        /// </summary>
        public IDataStructureSuperObject DataStructure
        {
            get
            {
                return this._dataStructure;
            }
        }
    }
}