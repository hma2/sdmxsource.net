﻿// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     ConceptSchemeSuperObject class
    /// </summary>
    public class ConceptSchemeSuperObject : MaintainableSuperObject, IConceptSchemeSuperObject
    {
        /// <summary>
        ///     The concepts
        /// </summary>
        private readonly ISet<IConceptSuperObject> _concepts;

        /// <summary>
        ///     The concept scheme bean
        /// </summary>
        private readonly IConceptSchemeObject _conceptSchemeBean;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeSuperObject" /> class.
        /// </summary>
        /// <param name="conceptSchemeType">Type of the concept scheme.</param>
        /// <param name="representations">The representations.</param>
        /// <exception cref="SdmxSemmanticException">Semantic exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="conceptSchemeType"/> is <see langword="null" />.</exception>
        public ConceptSchemeSuperObject(
            IConceptSchemeObject conceptSchemeType, 
            IDictionary<IConceptObject, ICodelistSuperObject> representations)
            : base(conceptSchemeType)
        {
            if (conceptSchemeType == null)
            {
                throw new ArgumentNullException("conceptSchemeType");
            }

            if (representations == null)
            {
                throw new ArgumentNullException("representations");
            }

            this._conceptSchemeBean = conceptSchemeType;
            this._concepts = new HashSet<IConceptSuperObject>();
            if (conceptSchemeType.Items != null)
            {
                foreach (IConceptObject currentConcept in conceptSchemeType.Items)
                {
                    ICodelistSuperObject codelistSuperObject;
                    representations.TryGetValue(currentConcept, out codelistSuperObject);
                    IConceptSuperObject concept = new ConceptSuperObject(currentConcept, codelistSuperObject);
                    this._concepts.Add(concept);
                }
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IConceptSchemeObject BuiltFrom
        {
            get
            {
                return this._conceptSchemeBean;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                returnSet.Add(this._conceptSchemeBean);
                foreach (IConceptSuperObject concept in this._concepts)
                {
                    foreach (IMaintainableObject obconcept in concept.CompositeObjects)
                    {
                        returnSet.Add(obconcept);
                    }
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the concepts.
        /// </summary>
        /// <value>
        ///     The concepts.
        /// </value>
        public ISet<IConceptSuperObject> Concepts
        {
            get
            {
                return new HashSet<IConceptSuperObject>(this._concepts);
            }
        }
    }
}