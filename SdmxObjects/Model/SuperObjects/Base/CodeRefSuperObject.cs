// -----------------------------------------------------------------------
// <copyright file="CodeRefSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     CodeRefSuperObject class
    /// </summary>
    public class CodeRefSuperObject : IdentifiableSuperObject, IHierarchicalCodeSuperObject
    {
        /// <summary>
        ///     The _code bean
        /// </summary>
        private readonly ICode _codeBean;

        /// <summary>
        ///     The _code refs
        /// </summary>
        private readonly List<IHierarchicalCodeSuperObject> _codeRefs;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeRefSuperObject" /> class.
        /// </summary>
        /// <param name="hierarchyBean">The hierarchy bean.</param>
        /// <param name="codeRef">The code reference.</param>
        /// <param name="codelists">The codelists.</param>
        public CodeRefSuperObject(IHierarchy hierarchyBean, IHierarchicalCode codeRef, IList<ICodelistObject> codelists)
            : base(codeRef)
        {
            this._codeBean = this.GetCodeObject(hierarchyBean, codeRef, codelists);
            if (codeRef.CodeRefs != null)
            {
                this._codeRefs = new List<IHierarchicalCodeSuperObject>();
                foreach (var hierarchicalCode in codeRef.CodeRefs)
                {
                    this._codeRefs.Add(new CodeRefSuperObject(hierarchyBean, hierarchicalCode, codelists));
                }
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IHierarchicalCode BuiltFrom
        {
            get
            {
                return (IHierarchicalCode)base.BuiltFrom;
            }
        }

        /// <summary>
        ///     Gets the code.
        /// </summary>
        public ICode Code
        {
            get
            {
                return this._codeBean;
            }
        }

        /// <summary>
        ///     Gets the code refs.
        /// </summary>
        public IList<IHierarchicalCodeSuperObject> CodeRefs
        {
            get
            {
                if (this._codeRefs == null)
                {
                    return null;
                }

                return new List<IHierarchicalCodeSuperObject>(this._codeRefs);
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this._codeBean.MaintainableParent);
                if (this._codeRefs != null)
                {
                    foreach (var hierarchicalCodeSuperObject in this._codeRefs)
                    {
                        returnSet.AddAll(hierarchicalCodeSuperObject.CompositeObjects);
                    }
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the code.
        /// </summary>
        /// <param name="codelist">The codelist.</param>
        /// <param name="codeId">The code identifier.</param>
        /// <returns>The code.</returns>
        /// <exception cref="ArgumentException">Code  + codeId +  Not found in codelist :  + codelist.Urn</exception>
        private ICode GetCode(ICodelistObject codelist, string codeId)
        {
            foreach (var item in codelist.Items)
            {
                if (item.Id.Equals(codeId))
                {
                    return item;
                }
            }

            throw new ArgumentException("Code " + codeId + " Not found in codelist : " + codelist.Urn);
        }

        /// <summary>
        ///     Gets the codelist reference.
        /// </summary>
        /// <param name="codelistRefs">The codelist refs.</param>
        /// <param name="codelistaliasRef">The codelistalias reference.</param>
        /// <returns>The code list reference</returns>
        /// <exception cref="ArgumentException">Codelist Ref Not found with Alias :  + codelistaliasRef</exception>
        private ICodelistRef GetCodelistRef(IList<ICodelistRef> codelistRefs, string codelistaliasRef)
        {
            foreach (var codelistRef in codelistRefs)
            {
                if (codelistRef.Alias.Equals(codelistaliasRef))
                {
                    return codelistRef;
                }
            }

            throw new ArgumentException("Codelist Ref Not found with Alias : " + codelistaliasRef);
        }

        /// <summary>
        ///     Gets the code object.
        /// </summary>
        /// <param name="hierarchyBean">The hierarchy bean.</param>
        /// <param name="codeRef">The code reference.</param>
        /// <param name="codelists">The codelists.</param>
        /// <returns>The code object</returns>
        /// <exception cref="ArgumentException">Codelist  + sRef.MaintainableUrn +  Not found</exception>
        private ICode GetCodeObject(
            IHierarchy hierarchyBean, 
            IHierarchicalCode codeRef, 
            ICollection<ICodelistObject> codelists)
        {
            IStructureReference sref = null;
            if (codeRef.CodeReference != null)
            {
                sref = codeRef.CodeReference;
            }
            else
            {
                var hcl = hierarchyBean.MaintainableParent;
                var codelistRef = this.GetCodelistRef(hcl.CodelistRef, codeRef.CodelistAliasRef);
                sref = codelistRef.CodelistReference;
            }

            var codelist = (ICodelistObject)MaintainableUtil<ICodelistObject>.ResolveReference(codelists, sref);
            if (codelist == null)
            {
                throw new ArgumentException("Codelist " + sref.MaintainableUrn + " Not found");
            }

            return this.GetCode(codelist, sref.ChildReference.Id);
        }
    }
}