// -----------------------------------------------------------------------
// <copyright file="IdentifiableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    /// <summary>
    ///     IdentifiableSuperObject class
    /// </summary>
    public abstract class IdentifiableSuperObject : AnnotableSuperObject, IIdentifiableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="IdentifiableSuperObject" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        /// <exception cref="ArgumentNullException"><paramref name="identifiable"/> is <see langword="null" />.</exception>
        protected IdentifiableSuperObject(IIdentifiableObject identifiable)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            this.Id = identifiable.Id;
            this.Urn = identifiable.Urn;
        }

        /// <summary>
        ///     Gets the Id of the Identifiable Object
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        ///     Gets the URN of the Identifiable Object
        /// </summary>
        public Uri Urn { get; private set; }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var o = obj as IIdentifiableSuperObject;
            if (o != null)
            {
                var that = o;
                return this.Urn.Equals(that.Urn);
            }

            return false;
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return this.Urn.GetHashCode();
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.Urn.ToString();
        }
    }
}