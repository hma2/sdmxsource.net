// -----------------------------------------------------------------------
// <copyright file="ComponentSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     ComponentSuperObject class
    /// </summary>
    public abstract class ComponentSuperObject : IdentifiableSuperObject, IComponentSuperObject
    {
        /// <summary>
        ///     The _codelist bean
        /// </summary>
        private readonly ICodelistSuperObject _codelistBean;

        /// <summary>
        ///     The _component bean
        /// </summary>
        private readonly IComponent _componentBean;

        /// <summary>
        ///     The _concept super bean
        /// </summary>
        private readonly IConceptSuperObject _conceptSuperBean;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentSuperObject" /> class.
        /// </summary>
        /// <param name="componentBean">The component bean.</param>
        /// <param name="codelistBean">The codelist bean.</param>
        /// <param name="conceptSuperBean">The concept super bean.</param>
        /// <exception cref="ArgumentNullException"><paramref name="conceptSuperBean"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        protected ComponentSuperObject(
            [ValidatedNotNull]IComponent componentBean, 
            ICodelistSuperObject codelistBean, 
            IConceptSuperObject conceptSuperBean)
            : base(componentBean)
        {
            this._componentBean = componentBean;
            this._codelistBean = codelistBean;

            if (conceptSuperBean == null)
            {
                throw new ArgumentNullException("conceptSuperBean");
            }

            if (codelistBean == null)
            {
                this._codelistBean = conceptSuperBean.CoreRepresentation;
            }

            this._conceptSuperBean = conceptSuperBean;
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IComponent BuiltFrom
        {
            get
            {
                return this._componentBean;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this._componentBean.MaintainableParent);
                if (this._codelistBean != null)
                {
                    returnSet.AddAll(this._codelistBean.CompositeObjects);
                }

                if (this._conceptSuperBean != null)
                {
                    returnSet.AddAll(this._conceptSuperBean.CompositeObjects);
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the concept, this is mandatory and will always return a value
        /// </summary>
        public IConceptSuperObject Concept
        {
            get
            {
                return this._conceptSuperBean;
            }
        }

        /// <summary>
        ///     Gets the text format, this may be null if there is none
        /// </summary>
        public ITextFormat TextFormat
        {
            get
            {
                if (this._componentBean.Representation != null)
                {
                    return this._componentBean.Representation.TextFormat;
                }

                return null;
            }
        }

        /// <summary>
        ///     Gets the codelist, this may be null if there is none
        /// </summary>
        /// <param name="useConceptIfRequired">
        ///     if the representation is uncoded, but the concept has default representation, then the codelist for the concept
        ///     will be returned if this parameter is set to true
        /// </param>
        /// <returns>
        ///     The <see cref="ICodelistSuperObject" /> .
        /// </returns>
        public ICodelistSuperObject GetCodelist(bool useConceptIfRequired)
        {
            if (this._codelistBean == null)
            {
                return this._conceptSuperBean.CoreRepresentation;
            }

            return this._codelistBean;
        }

        /// <summary>
        ///     Gets the codelist.
        /// </summary>
        /// <returns>The code list super object</returns>
        public ICodelistSuperObject GetCodelist()
        {
            return this.GetCodelist(true);
        }
    }
}