// -----------------------------------------------------------------------
// <copyright file="ItemSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    /// <summary>
    ///     ItemSuperObject class
    /// </summary>
    /// <typeparam name="T">The generic type</typeparam>
    public abstract class ItemSuperObject<T> : NameableSuperObject, IItemSuperObject<T>
        where T : IMaintainableSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSuperObject{T}" /> class.
        /// </summary>
        /// <param name="identifiable">The identifiable.</param>
        /// <param name="itemScheme">The item scheme.</param>
        /// <exception cref="ArgumentNullException"><paramref name="identifiable"/> is <see langword="null" />.</exception>
        protected ItemSuperObject(IItemObject identifiable, T itemScheme)
            : base(identifiable)
        {
            if (identifiable == null)
            {
                throw new ArgumentNullException("identifiable");
            }

            this.ItemScheme = itemScheme;
        }

        /// <summary>
        ///     Gets the Item Scheme that this item lives inside
        /// </summary>
        public T ItemScheme { get; private set; }
    }
}