// -----------------------------------------------------------------------
// <copyright file="SuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     SuperObject class
    /// </summary>
    public abstract class SuperObject : ISuperObject
    {
        /// <summary>
        ///     The _built from
        /// </summary>
        private readonly ISdmxObject _builtFrom;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SuperObject" /> class.
        /// </summary>
        /// <param name="builtFrom">The built from.</param>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        protected SuperObject(ISdmxObject builtFrom)
        {
            if (builtFrom == null)
            {
                throw new SdmxSemmanticException("Super Bean missing the required SDMXBean that it is built from");
            }

            this._builtFrom = builtFrom;
        }

        /// <summary>
        ///     Gets the object that was used to build this object base.
        /// </summary>
        public ISdmxObject BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public virtual ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                return new HashSet<IMaintainableObject>();
            }
        }

        /// <summary>
        ///     Adds to set.
        /// </summary>
        /// <typeparam name="TV">The type of the v.</typeparam>
        /// <param name="returnSet">The return set.</param>
        /// <param name="superBeans">The super beans.</param>
        protected void AddToSet<TV>(ISet<IMaintainableObject> returnSet, IList<TV> superBeans) where TV : ISuperObject
        {
            if (superBeans != null)
            {
                foreach (var superBean in superBeans)
                {
                    returnSet.AddAll(superBean.CompositeObjects);
                }
            }
        }
    }
}