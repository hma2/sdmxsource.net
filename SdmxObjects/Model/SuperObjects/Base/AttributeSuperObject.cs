// -----------------------------------------------------------------------
// <copyright file="AttributeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;

    /// <summary>
    ///     AttributeSuperObject class
    /// </summary>
    public class AttributeSuperObject : ComponentSuperObject, IAttributeSuperObject
    {
        /// <summary>
        ///     The _attribute bean
        /// </summary>
        private readonly IAttributeObject _attributeBean;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeSuperObject" /> class.
        /// </summary>
        /// <param name="attributeBean">The attribute bean.</param>
        /// <param name="codelistBean">The codelist bean.</param>
        /// <param name="conceptSuperBean">The concept super bean.</param>
        /// <exception cref="ArgumentNullException"><paramref name="conceptSuperBean" /> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        public AttributeSuperObject(
            IAttributeObject attributeBean, 
            ICodelistSuperObject codelistBean, 
            IConceptSuperObject conceptSuperBean)
            : base(attributeBean, codelistBean, conceptSuperBean)
        {
            this._attributeBean = attributeBean;
        }

        /// <summary>
        ///     Gets the assignmentStatus attribute indicates whether a
        ///     value must be provided for the attribute when sending documentation along with the data.
        /// </summary>
        public string AssignmentStatus
        {
            get
            {
                return this._attributeBean.AssignmentStatus;
            }
        }

        /// <summary>
        ///     Gets the attachment group.
        /// </summary>
        public string AttachmentGroup
        {
            get
            {
                return this._attributeBean.AttachmentGroup;
            }
        }

        /// <summary>
        ///     Gets the ATTRIBUTE_ATTACHMENT_LEVEL attribute indicating the level to which the attribute is attached in
        ///     time-series formats
        ///     (generic, compact, utility data formats).
        ///     Attributes with an attachment level of Group are only available if the data is organized in groups,
        ///     and should be used appropriately, as the values may not be communicated if the data is not grouped.
        /// </summary>
        public AttributeAttachmentLevel AttachmentLevel
        {
            get
            {
                return this._attributeBean.AttachmentLevel;
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IAttributeObject BuiltFrom
        {
            get
            {
                return this._attributeBean;
            }
        }

        /// <summary>
        ///     Gets the dimension reference.
        /// </summary>
        public IList<string> DimensionReferences
        {
            get
            {
                return this._attributeBean.DimensionReferences;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the attribute is mandatory
        /// </summary>
        public bool Mandatory
        {
            get
            {
                return this._attributeBean.Mandatory;
            }
        }

        /// <summary>
        ///     Gets the primary measure reference.
        /// </summary>
        public string PrimaryMeasureReference
        {
            get
            {
                return this._attributeBean.PrimaryMeasureReference;
            }
        }
    }
}