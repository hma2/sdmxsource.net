// -----------------------------------------------------------------------
// <copyright file="AnnotableSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     AnnotableSuperObject class
    /// </summary>
    public abstract class AnnotableSuperObject : SuperObject, IAnnotableSuperObject
    {
        /// <summary>
        ///     The annotation by type
        /// </summary>
        private readonly IDictionary<string, IAnnotationSuperObject> _annotationByType =
            new Dictionary<string, IAnnotationSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotableSuperObject" /> class.
        /// </summary>
        /// <param name="annotableType">Type of the annotable.</param>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        protected AnnotableSuperObject(IAnnotableObject annotableType)
            : base(annotableType)
        {
            if (annotableType != null && annotableType.Annotations != null)
            {
                this.Annotations = new HashSet<IAnnotationSuperObject>();
                foreach (var item in annotableType.Annotations)
                {
                    var annotation = new AnnotationSuperObject(item);
                    this.Annotations.Add(annotation);
                    if (ObjectUtil.ValidString(annotation.Type))
                    {
                        // TODO FIXME
                        // it is possible for annotations to have the same type
                        // so the original line of code
                        // annotationByType.Add(annotation.Type, annotation);
                        // throws a  "An item with the same key has already been added."
                        // if there are annotations with the same type
                        this._annotationByType[annotation.Type] = annotation;
                    }
                }
            }
        }

        /// <summary>
        ///     Gets a list of annotations that exist for this Annotable Object
        /// </summary>
        public ISet<IAnnotationSuperObject> Annotations { get; private set; }

        /// <summary>
        ///     Gets an annotations with the given title, this returns null if no annotation exists with
        ///     the given type
        /// </summary>
        /// <param name="title">The title.</param>
        /// <returns>
        ///     The annotations with the given title, this returns null if no annotation exists with
        ///     the given type
        /// </returns>
        public ISet<IAnnotationSuperObject> GetAnnotationByTitle(string title)
        {
            ISet<IAnnotationSuperObject> returnSet = new HashSet<IAnnotationSuperObject>();
            if (this.HasAnnotations())
            {
                foreach (var annotation in this.Annotations)
                {
                    if (annotation.Title != null && annotation.Title.Equals(title))
                    {
                        returnSet.Add(annotation);
                    }
                }
            }

            return returnSet;
        }

        /// <summary>
        ///     Gets an annotation with the given type, this returns null if no annotation exists with
        ///     the given type
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///     The <see cref="IAnnotationSuperObject" /> .
        /// </returns>
        public IAnnotationSuperObject GetAnnotationByType(string type)
        {
            return this._annotationByType.GetOrDefault(type);
        }

        /// <summary>
        ///     Gets an annotations with the given url, this returns null if no annotation exists with
        ///     the given type
        /// </summary>
        /// <param name="url">The url.</param>
        /// <returns>
        ///     The annotations with the given url, this returns null if no annotation exists with
        ///     the given type
        /// </returns>
        public ISet<IAnnotationSuperObject> GetAnnotationByUrl(Uri url)
        {
            ISet<IAnnotationSuperObject> returnSet = new HashSet<IAnnotationSuperObject>();
            if (this.HasAnnotations())
            {
                foreach (var annotation in
                    this.Annotations.Where(
                        annotation => annotation.Url != null && annotation.Url.ToString().Equals(url.ToString())))
                {
                    returnSet.Add(annotation);
                }
            }

            return returnSet;
        }

        /// <summary>
        ///     Gets a value indicating whether the annotations exist for this Annotable Object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public bool HasAnnotations()
        {
            return this.Annotations != null && this.Annotations.Count > 0;
        }
    }
}