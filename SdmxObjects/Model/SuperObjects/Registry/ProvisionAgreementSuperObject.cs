// -----------------------------------------------------------------------
// <copyright file="ProvisionAgreementSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Registry
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     ProvisionAgreementSuperObject class
    /// </summary>
    public class ProvisionAgreementSuperObject : MaintainableSuperObject, IProvisionAgreementSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProvisionAgreementSuperObject" /> class.
        /// </summary>
        /// <param name="provisionAgreement">The provision agreement.</param>
        /// <param name="dataflowSuperBean">The dataflow super bean.</param>
        /// <param name="dataProviderBean">The data provider bean.</param>
        /// <exception cref="SdmxSemmanticException">Error creating provision agreement super bean :  + provisionAgreement.Urn</exception>
        public ProvisionAgreementSuperObject(
            IProvisionAgreementObject provisionAgreement, 
            IDataflowSuperObject dataflowSuperBean, 
            IDataProvider dataProviderBean)
            : base(provisionAgreement)
        {
            this.BuiltFrom = provisionAgreement;
            this.DataflowSuperObject = dataflowSuperBean;
            this.DataProvider = dataProviderBean;
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(
                    "Error creating provision agreement super bean : " + provisionAgreement.Urn, 
                    e);
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IProvisionAgreementObject BuiltFrom { get; private set; }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this.BuiltFrom);
                returnSet.AddAll(this.DataflowSuperObject.CompositeObjects);
                returnSet.Add(this.DataProvider.MaintainableParent);
                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the dataflow super @object that this provision agreement references
        /// </summary>
        public IDataflowSuperObject DataflowSuperObject { get; private set; }

        /// <summary>
        ///     Gets the data provider @object that this provision agreement references
        /// </summary>
        public IDataProvider DataProvider { get; private set; }

        /// <summary>
        ///     Validates this instance.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        ///     Madatory Dataflow not provided
        ///     or
        ///     Madatory Dataflow not provided
        /// </exception>
        private void Validate()
        {
            if (this.DataflowSuperObject == null)
            {
                throw new SdmxSemmanticException("Madatory Dataflow not provided");
            }

            if (this.DataProvider == null)
            {
                throw new SdmxSemmanticException("Madatory Dataflow not provided");
            }
        }
    }
}