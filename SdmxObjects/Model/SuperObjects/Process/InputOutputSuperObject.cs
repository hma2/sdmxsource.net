// -----------------------------------------------------------------------
// <copyright file="InputOutputSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Process
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     InputOutputSuperObject class
    /// </summary>
    public class InputOutputSuperObject : AnnotableSuperObject, IInputOutputSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InputOutputSuperObject" /> class.
        /// </summary>
        /// <param name="inputOutputBean">The input output bean.</param>
        /// <param name="structure">The structure.</param>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        /// <exception cref="ArgumentNullException"><paramref name="inputOutputBean"/> is <see langword="null" />.</exception>
        public InputOutputSuperObject(IInputOutputObject inputOutputBean, IIdentifiableObject structure)
            : base(inputOutputBean)
        {
            if (inputOutputBean == null)
            {
                throw new ArgumentNullException("inputOutputBean");
            }

            this.BuiltFrom = inputOutputBean;
            this.LocalId = inputOutputBean.LocalId;
            this.Structure = structure;
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IInputOutputObject BuiltFrom { get; private set; }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                if (this.Structure != null)
                {
                    returnSet.Add(this.Structure.MaintainableParent);
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the localID attribute is an optional identification for the input or output within the process.
        /// </summary>
        public string LocalId { get; private set; }

        /// <summary>
        ///     Gets the structure.
        /// </summary>
        public IIdentifiableObject Structure { get; private set; }
    }
}