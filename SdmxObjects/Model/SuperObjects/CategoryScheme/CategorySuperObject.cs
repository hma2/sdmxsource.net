// -----------------------------------------------------------------------
// <copyright file="CategorySuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.CategoryScheme
{
    using System.Collections.Generic;

    using Base;
    using Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Sdmxsource.Sdmx.Api.Model.SuperObjects.CategoryScheme;
    using Sdmxsource.Util;

    /// <summary>
    ///     CategorySuperObject class
    /// </summary>
    public class CategorySuperObject : ItemSuperObject<ICategorySchemeSuperObject>, ICategorySuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorySuperObject" /> class.
        /// </summary>
        /// <param name="catScheme">The cat scheme.</param>
        /// <param name="category">The category.</param>
        /// <param name="parent">The parent.</param>
        public CategorySuperObject(
            ICategorySchemeSuperObject catScheme, 
            ICategoryObject category, 
            ICategorySuperObject parent)
            : base(category, catScheme)
        {
            this.Children = new List<ICategorySuperObject>();
            this.Parent = parent;

            if (ObjectUtil.ValidCollection(category.Items))
            {
                foreach (var categoryObject in category.Items)
                {
                    var child = new CategorySuperObject(catScheme, categoryObject, this);
                    this.Children.Add(child);
                }
            }
        }

        /// <summary>
        ///     Gets the children.
        /// </summary>
        /// <value>
        ///     The children.
        /// </value>
        public IList<ICategorySuperObject> Children { get; private set; }

        /// <summary>
        ///     Gets the parent.
        /// </summary>
        /// <value>
        ///     The parent.
        /// </value>
        public ICategorySuperObject Parent { get; private set; }

        /// <summary>
        ///     Determines whether this instance has children.
        /// </summary>
        /// <returns>True if it has children</returns>
        public bool HasChildren()
        {
            return this.Children != null && this.Children.Count > 0;
        }

        /// <summary>
        ///     Determines whether this instance has parent.
        /// </summary>
        /// <returns>true if it has parents</returns>
        public bool HasParent()
        {
            return this.Parent != null;
        }
    }
}