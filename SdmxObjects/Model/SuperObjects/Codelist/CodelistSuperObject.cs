// -----------------------------------------------------------------------
// <copyright file="CodelistSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist
{
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     CodelistSuperObject class
    /// </summary>
    public class CodelistSuperObject : MaintainableSuperObject, ICodelistSuperObject
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(CodelistSuperObject));

        /// <summary>
        ///     The _code value map
        /// </summary>
        private readonly IDictionary<string, ICodeSuperObject> _codeValueMap =
            new Dictionary<string, ICodeSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistSuperObject" /> class.
        /// </summary>
        /// <param name="codelist">The codelist.</param>
        public CodelistSuperObject(ICodelistObject codelist)
            : base(codelist)
        {
            _log.Debug("Create Codelist Super Bean");
            this.BuiltFrom = codelist;
            this.Codes = new List<ICodeSuperObject>();
            var allCodes = codelist.Items;
            if (allCodes != null)
            {
                var codeChildMap = new Dictionary<ICode, IList<ICode>>();
                _log.Debug("Create code Child Map");
                foreach (var currentCode in allCodes)
                {
                    if (ObjectUtil.ValidString(currentCode.ParentCode))
                    {
                        var parent = codelist.GetCodeById(currentCode.ParentCode);

                        // Add the child to the parent to the key, and the child to the value
                        IList<ICode> children;
                        codeChildMap.TryGetValue(parent, out children);
                        if (children == null)
                        {
                            children = new List<ICode>();
                            codeChildMap.Add(parent, children);
                        }

                        children.Add(currentCode);
                    }
                }

                _log.Debug("Map Created");

                foreach (var currentCode in allCodes)
                {
                    if (!ObjectUtil.ValidString(currentCode.ParentCode))
                    {
                        _log.Debug("Create Top Level Code Super Bean: " + currentCode.Id);
                        var code = new CodeSuperObject(this, currentCode, codeChildMap, null);
                        _log.Debug("Create Top Level Code Super Bean Created: " + currentCode.Id);
                        this.Codes.Add(code);
                    }
                }
            }

            this.BuildCodeValueMap(this.Codes);
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new ICodelistObject BuiltFrom { get; private set; }

        /// <summary>
        ///     Gets the codes in this codelist. As codes are hierarchical only the top level codes, with no parents, will be
        ///     returned.
        /// </summary>
        /// <value>
        ///     the codes in this codelist.
        /// </value>
        public IList<ICodeSuperObject> Codes { get; private set; }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this.BuiltFrom);
                return returnSet;
            }
        }

        /// <summary>
        ///     Iterates through the code hierarchy and returns the ICodeSuperObject that has the same id as that supplied.
        /// </summary>
        /// <param name="id">the id of a ICodeSuperObject to search for.</param>
        /// <returns>
        ///     the matching ICodeSuperObject or null if there was no match.
        /// </returns>
        public ICodeSuperObject GetCodeByValue(string id)
        {
            ICodeSuperObject ret;

            if (!_codeValueMap.TryGetValue(id, out ret))
            {
                return null;
            }

            return ret;
        }

        /// <summary>
        ///     Builds the code value map.
        /// </summary>
        /// <param name="codes">The codes.</param>
        private void BuildCodeValueMap(IList<ICodeSuperObject> codes)
        {
            foreach (var currentCode in codes)
            {
                if (currentCode.HasChildren())
                {
                    this.BuildCodeValueMap(currentCode.Children);
                }

                this._codeValueMap.Add(currentCode.Id, currentCode);
            }
        }
    }
}