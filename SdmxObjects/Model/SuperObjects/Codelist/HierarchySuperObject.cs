// -----------------------------------------------------------------------
// <copyright file="HierarchySuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-16
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     HierarchySuperObject class
    /// </summary>
    public class HierarchySuperObject : ItemSuperObject<IHierarchicalCodelistSuperObject>, 
                                        IHierarchySuperObject<IHierarchicalCodelistSuperObject>
    {
        /// <summary>
        ///     The _code refs
        /// </summary>
        private readonly List<IHierarchicalCodeSuperObject> _codeRefs = new List<IHierarchicalCodeSuperObject>();

        /// <summary>
        ///     The _hierarchy bean
        /// </summary>
        private readonly IHierarchy _hierarchyBean;

        /// <summary>
        ///     Initializes a new instance of the <see cref="HierarchySuperObject" /> class.
        /// </summary>
        /// <param name="hierarchyBean">The hierarchy bean.</param>
        /// <param name="itemScheme">The item scheme.</param>
        /// <param name="codelists">The codelists.</param>
        public HierarchySuperObject(
            IHierarchy hierarchyBean, 
            IHierarchicalCodelistSuperObject itemScheme, 
            IList<ICodelistObject> codelists)
            : base(hierarchyBean, itemScheme)
        {
            this._hierarchyBean = hierarchyBean;
            foreach (var hierarchicalCodeObject in hierarchyBean.HierarchicalCodeObjects)
            {
                this._codeRefs.Add(new CodeRefSuperObject(hierarchyBean, hierarchicalCodeObject, codelists));
            }
        }

        /// <summary>
        ///     Gets the codes in this codelist
        /// </summary>
        public IList<IHierarchicalCodeSuperObject> Codes
        {
            get
            {
                return new List<IHierarchicalCodeSuperObject>(this._codeRefs);
            }
        }

        /// <summary>
        ///     Gets the composite objects that were used to build this object base.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public override ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                var returnSet = base.CompositeObjects;
                returnSet.Add(this._hierarchyBean.MaintainableParent);
                foreach (var item in this._codeRefs)
                {
                    returnSet.AddAll(item.CompositeObjects);
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the level.
        /// </summary>
        /// <value>
        ///     the level for this hierarchy, returns null if there is no level
        /// </value>
        public ILevelObject Level
        {
            get
            {
                return this._hierarchyBean.Level;
            }
        }

        /// <summary>
        ///     Gets the ILevelObject at the position indicated, by walking the ILevelObject hierarchy of this Hierarchy Object,
        ///     returns null if there is no level
        /// </summary>
        /// <param name="levelPos">The level position</param>
        /// <returns>
        ///     the level at the given hierarchical position (0 indexed) - returns null if there is no level at that position
        /// </returns>
        public ILevelObject GetLevelAtPosition(int levelPos)
        {
            return this._hierarchyBean.GetLevelAtPosition(levelPos);
        }

        /// <summary>
        ///     If true this indicates that the hierarchy has formal levels. In this case, every code should have a level
        ///     associated with it.
        ///     If false this does not have formal levels.  This hierarchy may still have levels, getLevel() may still return a
        ///     value, the levels are not formal and the call to a code for its level may return null.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public bool HasFormalLevels()
        {
            return this._hierarchyBean.HasFormalLevels();
        }
    }
}