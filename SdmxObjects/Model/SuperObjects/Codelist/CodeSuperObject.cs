// -----------------------------------------------------------------------
// <copyright file="CodeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     CodeSuperObject class
    /// </summary>
    public class CodeSuperObject : ItemSuperObject<ICodelistSuperObject>, ICodeSuperObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeSuperObject" /> class.
        /// </summary>
        /// <param name="codelist">The codelist.</param>
        /// <param name="code">The code.</param>
        /// <param name="codeChildMap">The code child map.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="ArgumentNullException"><paramref name="codeChildMap"/> is <see langword="null" />.</exception>
        public CodeSuperObject(
            ICodelistSuperObject codelist, 
            ICode code, 
            IDictionary<ICode, IList<ICode>> codeChildMap, 
            ICodeSuperObject parent)
            : base(code, codelist)
        {
            if (codeChildMap == null)
            {
                throw new ArgumentNullException("codeChildMap");
            }

            IList<ICode> childCodes;
            this.Children = new List<ICodeSuperObject>();
            if (codeChildMap.TryGetValue(code, out childCodes))
            {
                foreach (var childCode in childCodes)
                {
                    var child = new CodeSuperObject(codelist, childCode, codeChildMap, this);
                    this.Children.Add(child);
                }
            }

            this.Parent = parent;
        }

        /// <summary>
        ///     Gets the object that was used to build this object base.
        /// </summary>
        public new ICode BuiltFrom
        {
            get
            {
                return (ICode)base.BuiltFrom;
            }
        }

        /// <summary>
        ///     Gets any children of this object.  If there are no children
        ///     then a <c>null</c> will be returned.
        /// </summary>
        /// <value>
        ///     child categories
        /// </value>
        public IList<ICodeSuperObject> Children { get; private set; }

        /// <summary>
        ///     Gets the parent Object of this object, a null object reference will be
        ///     returned if there is no parent
        /// </summary>
        public ICodeSuperObject Parent { get; private set; }

        /// <summary>
        ///     Gets <c>true</c> if this Object contains children,
        ///     if this is the case the method call <c>getChildren()</c> is
        ///     guaranteed to return a Set with length greater then 0.
        /// </summary>
        /// <returns>
        ///     true if this IHierarchical Object contains children
        /// </returns>
        public bool HasChildren()
        {
            return this.Children != null && this.Children.Count > 0;
        }

        /// <summary>
        ///     Gets <c>true</c> if this Object has a parent, false otherwise.
        /// </summary>
        /// <returns>
        ///     true if this Object has a parent
        /// </returns>
        public bool HasParent()
        {
            return this.Parent != null;
        }
    }
}