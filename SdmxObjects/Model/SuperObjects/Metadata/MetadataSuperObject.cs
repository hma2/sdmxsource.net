﻿// -----------------------------------------------------------------------
// <copyright file="MetadataSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Metadata
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Metadata;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    ///     MetadataSuperObject class
    /// </summary>
    public class MetadataSuperObject : SuperObject, IMetadataSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly IMetadata _builtFrom;

        /// <summary>
        ///     The metadata set
        /// </summary>
        private readonly IList<IMetadataSetSuperObject> _metadataSet = new List<IMetadataSetSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataSuperObject" /> class.
        /// </summary>
        /// <param name="builtFrom">The built from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        public MetadataSuperObject(IMetadata builtFrom, IIdentifiableRetrievalManager retrievalManager)
            : base(builtFrom)
        {
            this._builtFrom = builtFrom;
            foreach (IMetadataSet currentMs in builtFrom.MetadataSet)
            {
                this._metadataSet.Add(new MetadataSetSuperObject(currentMs, retrievalManager));
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IMetadata BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                if (this._metadataSet != null)
                {
                    foreach (IMetadataSetSuperObject mds in this._metadataSet)
                    {
                        foreach (IMaintainableObject co in mds.CompositeObjects)
                        {
                            returnSet.Add(co);
                        }
                    }
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the metadata set.
        /// </summary>
        public IList<IMetadataSetSuperObject> MetadataSet
        {
            get
            {
                return new List<IMetadataSetSuperObject>(this._metadataSet);
            }
        }
    }
}