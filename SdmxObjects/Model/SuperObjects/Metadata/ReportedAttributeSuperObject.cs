﻿// -----------------------------------------------------------------------
// <copyright file="ReportedAttributeSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Metadata;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     ReportedAttributeSuperObject class
    /// </summary>
    public class ReportedAttributeSuperObject : SuperObject, IReportedAttributeSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly IReportedAttributeObject _builtFrom;

        /// <summary>
        ///     The codelist
        /// </summary>
        private readonly ICodelistObject _codelist;

        /// <summary>
        ///     The concept
        /// </summary>
        private readonly IConceptObject _concept;

        /// <summary>
        ///     The reported attributes
        /// </summary>
        private readonly IList<IReportedAttributeSuperObject> _reportedAttributes =
            new List<IReportedAttributeSuperObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportedAttributeSuperObject" /> class.
        /// </summary>
        /// <param name="correspondingMa">The corresponding ma.</param>
        /// <param name="builtFrom">The built from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="ArgumentNullException"><paramref name="retrievalManager"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Super Bean missing the required SDMXBean that it is built from</exception>
        public ReportedAttributeSuperObject(
            IMetadataAttributeObject correspondingMa, 
            IReportedAttributeObject builtFrom, 
            IIdentifiableRetrievalManager retrievalManager)
            : base(builtFrom)
        {
            if (correspondingMa == null)
            {
                throw new ArgumentNullException("correspondingMa");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            this._builtFrom = builtFrom;
            this._concept = retrievalManager.GetIdentifiableObject<IConceptObject>(correspondingMa.ConceptRef);
            if (correspondingMa.HasCodedRepresentation())
            {
                this._codelist =
                    retrievalManager.GetIdentifiableObject<ICodelistObject>(
                        correspondingMa.Representation.Representation);
            }

            if (builtFrom.ReportedAttributes != null)
            {
                foreach (IReportedAttributeObject currentRa in builtFrom.ReportedAttributes)
                {
                    IMetadataAttributeObject mabean = this.GetMetadataAttributeForReportedAttribute(
                        currentRa, 
                        correspondingMa.MetadataAttributes);
                    this._reportedAttributes.Add(new ReportedAttributeSuperObject(mabean, currentRa, retrievalManager));
                }
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IReportedAttributeObject BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets the codelist.
        /// </summary>
        public ICodelistObject Codelist
        {
            get
            {
                return this._codelist;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                if (this._concept != null)
                {
                    returnSet.Add(this._concept.MaintainableParent);
                }

                if (this._codelist != null)
                {
                    returnSet.Add(this._codelist);
                }

                if (this._reportedAttributes != null)
                {
                    foreach (IReportedAttributeSuperObject reportedAttribute in this._reportedAttributes)
                    {
                        foreach (IMaintainableObject co in reportedAttribute.CompositeObjects)
                        {
                            returnSet.Add(co);
                        }
                    }
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the concept.
        /// </summary>
        public IConceptObject Concept
        {
            get
            {
                return this._concept;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this is the coded representation.
        /// </summary>
        public bool HasCodedRepresentation
        {
            get
            {
                return this._codelist != null;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the IReportedAttributeObject has a simple value, in which case getSimpleValue will
        ///     return a not null value
        /// </summary>
        /// <value>
        ///     The &lt; see cref= " bool " / &gt; .
        /// </value>
        public bool HasSimpleValue
        {
            get
            {
                return this._builtFrom.HasSimpleValue();
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return this._builtFrom.Id;
            }
        }

        /// <summary>
        ///     Gets the metadata text.
        /// </summary>
        public IList<ITextTypeWrapper> MetadataText
        {
            get
            {
                return this._builtFrom.MetadataText;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the getMetadataText returns an empty list
        /// </summary>
        public bool Presentational
        {
            get
            {
                return this._builtFrom.Presentational;
            }
        }

        /// <summary>
        ///     Gets the reported attributes.
        /// </summary>
        public IList<IReportedAttributeSuperObject> ReportedAttributes
        {
            get
            {
                return new List<IReportedAttributeSuperObject>(this._reportedAttributes);
            }
        }

        /// <summary>
        ///     Gets a simple value for this attribute, returns null if there is no simple value
        /// </summary>
        public string SimpleValue
        {
            get
            {
                return this._builtFrom.SimpleValue;
            }
        }

        /// <summary>
        ///     Gets the metadata attribute for reported attribute.
        /// </summary>
        /// <param name="reportedAttribute">The reported attribute.</param>
        /// <param name="attributeBeans">The m attribute beans.</param>
        /// <returns>The attribute object</returns>
        /// <exception cref="SdmxReferenceException">SDMX Reference exception</exception>
        private IMetadataAttributeObject GetMetadataAttributeForReportedAttribute(
            IReportedAttributeObject reportedAttribute, 
            IList<IMetadataAttributeObject> attributeBeans)
        {
            foreach (IMetadataAttributeObject currentMAttribute in attributeBeans)
            {
                if (currentMAttribute.Id.Equals(reportedAttribute.Id))
                {
                    return currentMAttribute;
                }
            }

            IMaintainableObject maint = reportedAttribute.GetParent<IMaintainableObject>(false);

            IList<string> ids = new List<string>();
            ids.Add(reportedAttribute.Id);
            IStructureReference sref = new StructureReferenceImpl(
                maint, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttribute), 
                ids);
            throw new SdmxReferenceException(maint, sref);
        }
    }
}