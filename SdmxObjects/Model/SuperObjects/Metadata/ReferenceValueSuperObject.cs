﻿// -----------------------------------------------------------------------
// <copyright file="ReferenceValueSuperObject.cs" company="EUROSTAT">
//   Date Created : 2016-02-15
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Metadata
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Metadata;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Metadata;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Base;

    /// <summary>
    /// ReferenceValue contains a value for a target reference object reference.
    /// <p/>
    /// When this is taken with its sibling elements, they identify the object or
    /// objects to which the reported metadata apply.
    /// The content of this will either be a reference to an identifiable object, a
    /// data key, a reference to a data set, or a report period.
    /// </summary>
    public class ReferenceValueSuperObject : SuperObject, IReferenceValueSuperObject
    {
        /// <summary>
        ///     The built from
        /// </summary>
        private readonly IReferenceValue _builtFrom;

        /// <summary>
        ///     The content constraint reference
        /// </summary>
        private readonly IContentConstraintObject _contentConstraintReference;

        /// <summary>
        ///     The identifiable reference
        /// </summary>
        private readonly IIdentifiableObject _identifiableReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReferenceValueSuperObject" /> class.
        /// </summary>
        /// <param name="builtFrom">The built from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <exception cref="ArgumentNullException"><paramref name="retrievalManager"/> is <see langword="null" />.</exception>
        public ReferenceValueSuperObject(IReferenceValue builtFrom, IIdentifiableRetrievalManager retrievalManager)
            : base(builtFrom)
        {
            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            this._builtFrom = builtFrom;
            if (builtFrom.IdentifiableReference != null)
            {
                this._identifiableReference = retrievalManager.GetIdentifiableObject(builtFrom.IdentifiableReference);
            }

            if (builtFrom.ContentConstraintReference != null)
            {
                this._contentConstraintReference =
                    retrievalManager.GetIdentifiableObject<IContentConstraintObject>(
                        builtFrom.ContentConstraintReference);
            }
        }

        /// <summary>
        ///     Gets the built from.
        /// </summary>
        /// <value>
        ///     The built from.
        /// </value>
        public new IReferenceValue BuiltFrom
        {
            get
            {
                return this._builtFrom;
            }
        }

        /// <summary>
        ///     Gets the composite objects.
        /// </summary>
        /// <value>
        ///     The composite objects.
        /// </value>
        public new ISet<IMaintainableObject> CompositeObjects
        {
            get
            {
                ISet<IMaintainableObject> returnSet = base.CompositeObjects;
                if (this._identifiableReference != null)
                {
                    returnSet.Add(this._identifiableReference.MaintainableParent);
                }

                if (this._contentConstraintReference != null)
                {
                    returnSet.Add(this._contentConstraintReference);
                }

                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the content constraint that this structure references, returns null if there is no reference
        /// </summary>
        public IContentConstraintObject ContentConstraintReference
        {
            get
            {
                return this._contentConstraintReference;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the reference is a content constraint reference, if true
        ///     getContentConstraintReference() will return a not null value
        /// </summary>
        public bool ContentConstriantReference
        {
            get
            {
                return this._builtFrom.IsContentConstriantReference;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the the reference value is a datakey reference, if true getDataKeys() will return 1
        ///     or more items
        /// </summary>
        public bool DatakeyReference
        {
            get
            {
                return this._builtFrom.DatakeyReference;
            }
        }

        /// <summary>
        ///     Gets a list of data keys, will return an empty list if isDatasetReference() is false
        /// </summary>
        public IList<IDataKey> DataKeys
        {
            get
            {
                return this._builtFrom.DataKeys;
            }
        }

        /// <summary>
        ///     Gets the dataset id.
        /// </summary>
        public string DatasetId
        {
            get
            {
                return this._builtFrom.DatasetId;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the is a dataset reference, if true GetIdentifiableReference() AND getDatasetId()
        ///     will NOT be null
        /// </summary>
        public bool DatasetReference
        {
            get
            {
                return this._builtFrom.DatasetReference;
            }
        }

        /// <summary>
        ///     Gets the id of this reference value
        /// </summary>
        public string Id
        {
            get
            {
                return this._builtFrom.Id;
            }
        }

        /// <summary>
        ///     Gets identifiable reference.
        /// </summary>
        public IIdentifiableObject IdentifiableReference
        {
            get
            {
                return this._identifiableReference;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this is an identifiable structure reference, if true GetIdentifiableReference()
        ///     will NOT be null
        /// </summary>
        public bool IsIdentifiableReference
        {
            get
            {
                return this._builtFrom.IsIdentifiableReference;
            }
        }
    }
}