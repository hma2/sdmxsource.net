// -----------------------------------------------------------------------
// <copyright file="SdmxDateCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    ///     The sdmx sdmxDate core.
    /// </summary>
    public class SdmxDateCore : ISdmxDate
    {
        /// <summary>
        ///     The _date.
        /// </summary>
        private readonly DateTime _date;

        /// <summary>
        ///     The _date in sdmx.
        /// </summary>
        private readonly string _dateInSdmx;

        /// <summary>
        ///     The _time format.
        /// </summary>
        private readonly TimeFormat _timeFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxDateCore" /> class.
        /// </summary>
        /// <param name="date">
        ///     The sdmxDate.
        /// </param>
        /// <param name="timeFormat">
        ///     The time format.
        /// </param>
        /// ///
        /// <exception cref="ArgumentNullException">
        ///     Throws ArgumentNullException.
        /// </exception>
        public SdmxDateCore(DateTime? date, TimeFormatEnumType timeFormat)
        {
            if (date == null)
            {
                throw new ArgumentNullException("date");
            }

            this._date = date.Value;
            this._timeFormat = TimeFormat.GetFromEnum(timeFormat);
            this._dateInSdmx = DateUtil.FormatDate(date.Value, timeFormat);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxDateCore" /> class.
        /// </summary>
        /// <param name="dateInSdmx">
        ///     The sdmxDate in sdmx.
        /// </param>
        public SdmxDateCore(string dateInSdmx)
        {
            this._date = DateUtil.FormatDate(dateInSdmx, true);
            this._timeFormat = DateUtil.GetTimeFormatOfDate(dateInSdmx);
            this._dateInSdmx = dateInSdmx;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxDateCore" /> class.
        /// </summary>
        /// <param name="dateInSdmx">
        ///     The sdmxDate in sdmx.
        /// </param>
        public SdmxDateCore(object dateInSdmx)
        {
            if (dateInSdmx == null)
            {
                throw new ArgumentNullException("dateInSdmx");
            }

            this._date = DateUtil.FormatDate(dateInSdmx, true);

            var dateStr = dateInSdmx as string;
            if (dateStr != null)
            {
                this._timeFormat = DateUtil.GetTimeFormatOfDate(dateStr);
                this._dateInSdmx = dateStr;
            }
            else if (dateInSdmx is DateTime)
            {
                this._timeFormat = TimeFormat.GetFromEnum(TimeFormatEnumType.Hour);
                this._dateInSdmx = DateUtil.FormatDate((DateTime)dateInSdmx, TimeFormatEnumType.Hour);
            }
        }

        /// <summary>
        ///     Gets the sdmxDate.
        /// </summary>
        public virtual DateTime? Date
        {
            get
            {
                return this._date;
            }
        }

        /// <summary>
        ///     Gets the sdmxDate in sdmx format.
        /// </summary>
        public virtual string DateInSdmxFormat
        {
            get
            {
                return this._dateInSdmx;
            }
        }

        /// <summary>
        ///     Gets the time format of sdmxDate.
        /// </summary>
        public virtual TimeFormat TimeFormatOfDate
        {
            get
            {
                return this._timeFormat;
            }
        }

        /// <summary>
        ///     The equals.
        /// </summary>
        /// <param name="obj">
        ///     The agencySchemeMutable.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool Equals(object obj)
        {
            var that = obj as ISdmxDate;
            if (that != null)
            {
                return that.DateInSdmxFormat.Equals(this.DateInSdmxFormat);
            }

            return false;
        }

        /// <summary>
        ///     The get hash code.
        /// </summary>
        /// <returns> The <see cref="int" /> . </returns>
        public override int GetHashCode()
        {
            return this._dateInSdmx.GetHashCode();
        }

        /// <summary>
        ///     The is later.
        /// </summary>
        /// <param name="sdmxDate">
        ///     The sdmxDate.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public virtual bool IsLater(ISdmxDate sdmxDate)
        {
            DateTime? dateTime = this.Date;
            return sdmxDate != null && sdmxDate.Date != null && (dateTime != null && (dateTime.Value.Ticks / 10000) > (sdmxDate.Date.Value.Ticks / 10000));
        }
    }
}