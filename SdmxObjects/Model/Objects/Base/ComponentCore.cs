// -----------------------------------------------------------------------
// <copyright file="ComponentCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Xml.Serialization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Attributes;

    using TextFormatType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.TextFormatType;

    /// <summary>
    ///     The component core.
    /// </summary>
    [Serializable]
    public abstract class ComponentCore : IdentifiableCore, IComponent
    {
        /// <summary>
        ///     The _concept ref.
        /// </summary>
        private readonly ICrossReference _conceptRef;

        /// <summary>
        ///     The local representation.
        /// </summary>
        private IRepresentation _localRepresentation;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">The agencyScheme.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="SdmxException">Throws Validate exception.</exception>
        protected internal ComponentCore(IComponentMutableObject itemMutableObject, IIdentifiableObject parent)
            : this(itemMutableObject, parent, true)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">The agencyScheme.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="validateComponentAttributes">if set to <c>true</c> [validate component attributes].</param>
        /// <exception cref="SdmxException">Throws Validate exception.</exception>
        protected internal ComponentCore(IComponentMutableObject itemMutableObject, IIdentifiableObject parent, bool validateComponentAttributes)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }

            try
            {
                if (itemMutableObject.Representation != null)
                {
                    this.LocalRepresentation = new RepresentationCore(itemMutableObject.Representation, this);
                }

                if (itemMutableObject.ConceptRef != null)
                {
                    this._conceptRef = new CrossReferenceImpl(this, itemMutableObject.ConceptRef);
                }
            }
            catch (Exception th)
            {
                throw new SdmxException("IsError creating component: " + this, th);
            }

            if (validateComponentAttributes)
            {
                this.ValidateComponentAttributes();
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="representation"> The local representation of the component type</param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal ComponentCore([ValidatedNotNull] ComponentType createdFrom, RepresentationType representation, SdmxStructureType structureType, IIdentifiableObject parent)
            : base(createdFrom, structureType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            if (representation != null)
            {
                this.LocalRepresentation = new RepresentationCore(representation, this);
            }

            if (createdFrom.ConceptIdentity != null)
            {
                this._conceptRef = RefUtil.CreateReference(this, createdFrom.ConceptIdentity);
            }

            // FUNC 2.1 put in Concept Identifity = conceptRef
            this.ValidateComponentAttributes();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">The structure type.</param>
        /// <param name="parent">The parent.</param>
        protected internal ComponentCore([ValidatedNotNull] ComponentType createdFrom, SdmxStructureType structureType, IIdentifiableObject parent)
            : this(createdFrom, structureType, parent, true)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">The structure type.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="shouldValidate">if set to <c>true</c> [should validate].</param>
        protected internal ComponentCore(ComponentType createdFrom, SdmxStructureType structureType, IIdentifiableObject parent, bool shouldValidate)
            : base(createdFrom, structureType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            var simpleDataStructureRepresentationType = createdFrom.GetTypedLocalRepresentation<SimpleDataStructureRepresentationType>();
            if (simpleDataStructureRepresentationType != null)
            {
                this.LocalRepresentation = new RepresentationCore(simpleDataStructureRepresentationType, this);
            }

            if (createdFrom.ConceptIdentity != null)
            {
                this._conceptRef = RefUtil.CreateReference(this, createdFrom.ConceptIdentity);
            }

            // FUNC 2.1 put in Concept Identifity = conceptRef
            if (shouldValidate)
            {
                this.ValidateComponentAttributes();
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="annotationType">
        ///     The annotation type.
        /// </param>
        /// <param name="textFormat">
        ///     The text format.
        /// </param>
        /// <param name="codelistAgency">
        ///     The codelist agency.
        /// </param>
        /// <param name="codelistId">
        ///     The codelist id.
        /// </param>
        /// <param name="codelistVersion">
        ///     The codelist version.
        /// </param>
        /// <param name="conceptSchemeAgency">
        ///     The concept scheme agency.
        /// </param>
        /// <param name="conceptSchemeId">
        ///     The concept scheme id.
        /// </param>
        /// <param name="conceptSchemeVersion">
        ///     The concept scheme version.
        /// </param>
        /// <param name="conceptAgency">
        ///     The concept agency.
        /// </param>
        /// <param name="conceptId">
        ///     The concept id.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal ComponentCore(
            IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            AnnotationsType annotationType, 
            TextFormatType textFormat, 
            string codelistAgency, 
            string codelistId, 
            string codelistVersion, 
            string conceptSchemeAgency, 
            string conceptSchemeId, 
            string conceptSchemeVersion, 
            string conceptAgency, 
            string conceptId, 
            IIdentifiableObject parent)
            : base(structureType, conceptId, null, annotationType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            if (string.IsNullOrWhiteSpace(conceptAgency))
            {
                conceptAgency = this.MaintainableParent.AgencyId;
            }

            if (textFormat != null || ObjectUtil.ValidOneString(codelistAgency, codelistId, codelistVersion))
            {
                if (ObjectUtil.ValidOneString(codelistAgency, codelistId, codelistVersion))
                {
                    if (string.IsNullOrWhiteSpace(codelistAgency))
                    {
                        codelistAgency = this.MaintainableParent.AgencyId;
                    }
                }

                this.LocalRepresentation = new RepresentationCore(textFormat, codelistAgency, codelistId, codelistVersion, this);
            }

            this._conceptRef = ConceptRefUtil.BuildConceptRef(this, conceptSchemeAgency, conceptSchemeId, conceptSchemeVersion, conceptAgency, conceptId);
            this.ValidateComponentAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="annotationType">
        ///     The annotation type.
        /// </param>
        /// <param name="codelistId">
        ///     The codelist id.
        /// </param>
        /// <param name="conceptId">
        ///     The concept id.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal ComponentCore(
            [ValidatedNotNull] IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationsType annotationType, 
            string codelistId, 
            string conceptId, 
            ISdmxStructure parent)
            : base(structureType, conceptId, null, annotationType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            if (!string.IsNullOrWhiteSpace(codelistId))
            {
                this.LocalRepresentation = new RepresentationCore(codelistId, this);
            }

            this._conceptRef = new CrossReferenceImpl(
                this, 
                this.MaintainableParent.AgencyId, 
                ConceptSchemeObject.DefaultSchemeVersion, 
                ConceptSchemeObject.DefaultSchemeVersion, 
                SdmxStructureEnumType.Concept, 
                conceptId);
            this.ValidateComponentAttributes();
        }

        /// <summary>
        ///     Gets the concept ref.
        /// </summary>
        public virtual ICrossReference ConceptRef
        {
            get
            {
                return this._conceptRef;
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        [SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "It is OK. We are copying SdmxSource.Java behaviour.")]
        public override string Id
        {
            get
            {
                return this.BuildComponentId();
            }
        }

        /// <summary>
        ///     Gets or sets the local representation.
        /// </summary>
        public IRepresentation LocalRepresentation
        {
            get
            {
                return this._localRepresentation;
            }

            set
            {
                this._localRepresentation = value;
            }
        }

        /// <summary>
        ///     Gets the representation.
        /// </summary>
        public virtual IRepresentation Representation
        {
            get
            {
                return this.LocalRepresentation;
            }
        }

        /// <summary>
        ///     The has coded representation.
        /// </summary>
        /// <returns> The <see cref="bool" /> . </returns>
        public virtual bool HasCodedRepresentation()
        {
            if (this.LocalRepresentation != null)
            {
                return this.LocalRepresentation.Representation != null;
            }

            return false;
        }

        /// <summary>
        ///     The deep equals internal.
        /// </summary>
        /// <param name="component">The agencyScheme.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool DeepEqualsComponent(IComponent component, bool includeFinalProperties)
        {
            if (component == null)
            {
                return false;
            }

            if (!this.Equivalent(this._conceptRef, component.ConceptRef))
            {
                return false;
            }

            if (!this.Equivalent(this.LocalRepresentation, component.Representation, includeFinalProperties))
            {
                return false;
            }

            return this.DeepEqualsIdentifiable(component, includeFinalProperties);
        }

        /// <summary>
        ///     The validate component attributes.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void ValidateComponentAttributes()
        {
            this.ValidateComponetReference();
            base.ValidateId(false);
        }

        /// <summary>
        ///     The validate id.
        /// </summary>
        /// <param name="startWithIntAllowed">
        ///     The start with int allowed.
        /// </param>
        protected internal override void ValidateId(bool startWithIntAllowed)
        {
            // Do nothing yet, not yet fully built
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            var composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this.LocalRepresentation, composites);
            return composites;
        }

        /// <summary>
        ///     Validates the componet reference.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        ///     conceptRef
        ///     or
        ///     Component reference is invalid, expected
        ///     + SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept).GetType() +  reference, got
        ///     + this._conceptRef.TargetReference.GetType() +  reference
        /// </exception>
        protected void ValidateComponetReference()
        {
            if (this._conceptRef == null)
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectMissingRequiredAttribute, this.StructureType.GetType(), "conceptRef");
            }

            if (this._conceptRef.TargetReference != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept))
            {
                throw new SdmxSemmanticException(
                    "Component reference is invalid, expected " + SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept).GetType() + " reference, got "
                    + this._conceptRef.TargetReference.GetType() + " reference");
            }
        }

        /// <summary>
        /// Builds the component identifier.
        /// </summary>
        /// <returns>The component ID.</returns>
        /// <exception cref="SdmxSemmanticException">Id not set for component</exception>
        protected string BuildComponentId()
        {
            if (!string.IsNullOrWhiteSpace(base.Id))
            {
                return base.Id;
            }

            if (this._conceptRef != null)
            {
                return this._conceptRef.ChildReference.Id;
            }

            throw new SdmxSemmanticException("Id not set for component");
        }
    }
}