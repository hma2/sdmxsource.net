// -----------------------------------------------------------------------
// <copyright file="TextTypeWrapperImpl.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType;

    /// <summary>
    ///     The text type wrapper impl.
    /// </summary>
    [Serializable]
    public class TextTypeWrapperImpl : SdmxObjectCore, ITextTypeWrapper
    {
        /// <summary>
        ///     The is html text.
        /// </summary>
        private readonly bool _isHtmlText;

        /// <summary>
        ///     The locale.
        /// </summary>
        private string _locale;

        /// <summary>
        ///     The valueren.
        /// </summary>
        private string _valueren;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="locale0">
        ///     The locale 0.
        /// </param>
        /// <param name="valueren">
        ///     The valueren.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public TextTypeWrapperImpl(string locale0, string valueren, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            this.Locale = locale0;
            this.Value = valueren;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public TextTypeWrapperImpl(ITextTypeWrapperMutableObject textType, ISdmxObject parent)
            : base(textType, parent)
        {
            this.Locale = textType.Locale;
            this.Value = textType.Value;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textType"/> is <see langword="null" />.</exception>
        public TextTypeWrapperImpl(TextType textType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            if (textType == null)
            {
                throw new ArgumentNullException("textType");
            }

            this.Locale = textType.lang;
            this.Value = textType.TypedValue;
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textType"/> is <see langword="null" />.</exception>
        public TextTypeWrapperImpl(Name textType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            if (textType == null)
            {
                throw new ArgumentNullException("textType");
            }

            this.Locale = textType.lang;
            this.Value = textType.TypedValue;
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textType"/> is <see langword="null" />.</exception>
        public TextTypeWrapperImpl(Description textType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            if (textType == null)
            {
                throw new ArgumentNullException("textType");
            }

            this.Locale = textType.lang;
            this.Value = textType.TypedValue;
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textType"/> is <see langword="null" />.</exception>
        public TextTypeWrapperImpl(XHTMLType textType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            if (textType == null)
            {
                throw new ArgumentNullException("textType");
            }

            // XPathNavigator cursor = textType.;
            this.Locale = textType.lang;
            this.Value = textType.Untyped.Value; // ??? cursor?
            this._isHtmlText = true;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textType"/> is <see langword="null" />.</exception>
        public TextTypeWrapperImpl(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType textType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            if (textType == null)
            {
                throw new ArgumentNullException("textType");
            }

            this.Locale = textType.lang;
            this.Value = textType.TypedValue;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextTypeWrapperImpl" /> class.
        /// </summary>
        /// <param name="textType">
        ///     The text type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textType"/> is <see langword="null" />.</exception>
        public TextTypeWrapperImpl(Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType textType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextType), parent)
        {
            if (textType == null)
            {
                throw new ArgumentNullException("textType");
            }

            this.Locale = textType.lang;
            this.Value = textType.TypedValue;
            this.Validate();
        }

        /// <summary>
        ///     Gets a value indicating whether html.
        /// </summary>
        public virtual bool Html
        {
            get
            {
                return this._isHtmlText;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets or sets the locale.
        /// </summary>
        public string Locale
        {
            get
            {
                return this._locale;
            }

            set
            {
                if (!ObjectUtil.ValidString(this._locale))
                {
                    this._locale = "en";
                }

                // Bug fix, in XML Locale contains a '-' to be valid, in Java '_' is used
                this._locale = this._locale.Replace("_", "-");
                this._locale = value;
            }
        }

        /// <summary>
        ///     Gets or sets the value.
        /// </summary>
        public string Value
        {
            get
            {
                return this._valueren;
            }

            set
            {
                if (value != null)
                {
                    this._valueren = value.Trim();
                }
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ITextTypeWrapper)sdmxObject;
                if (!ObjectUtil.Equivalent(this._locale, that.Locale))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._valueren, that.Value))
                {
                    return false;
                }

                if (this._isHtmlText != that.Html)
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            return new HashSet<ISdmxObject>();
        }

        /// <summary>
        ///     The parse locale.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <returns>True if parse</returns>
        private bool ParseLocale(string locale)
        {
            try
            {
                var value = CultureInfo.GetCultureInfo(locale);

                // needed for Windows 10/Server 2016 or higher
                return !value.CultureTypes.HasFlag(CultureTypes.UserCustomCulture);
            }
            catch (CultureNotFoundException)
            {
                // Needed for Windows prior Windows 10/Server 2016
                return false;
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._locale))
            {
                // Default to english
                this._locale = CultureInfo.CreateSpecificCulture("en").TwoLetterISOLanguageName;
            }

            if (!this.ParseLocale(this._locale))
            {
                throw new SdmxSemmanticException("Illegal Locale: " + this._locale);
            }

            if (string.IsNullOrWhiteSpace(this._valueren))
            {
                throw new SdmxSemmanticException("Text Type can not have an empty string value");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM VALUES OBJECT                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}