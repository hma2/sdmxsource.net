// -----------------------------------------------------------------------
// <copyright file="ItemSchemeObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Attributes;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The item scheme dataStructureObject core.
    /// </summary>
    /// <typeparam name="TItem">
    ///     Generic type parm - IItemObject
    /// </typeparam>
    /// <typeparam name="TMaint">
    ///     Generic type parm -IMaintainableObject
    /// </typeparam>
    /// <typeparam name="TMaintMutable">
    ///     Generic type parm - IMaintainableMutableObject
    /// </typeparam>
    /// <typeparam name="TItemMutable">
    ///     Generic type parm - IItemMutableObject
    /// </typeparam>
    [Serializable]
    public abstract class ItemSchemeObjectCore<TItem, TMaint, TMaintMutable, TItemMutable> :
        MaintainableObjectCore<TMaint, TMaintMutable>, 
        IItemSchemeObject<TItem>
        where TItem : IItemObject
        where TMaint : IMaintainableObject
        where TMaintMutable : IMaintainableMutableObject
        where TItemMutable : IItemMutableObject
    {
        /// <summary>
        ///     The _is partial.
        /// </summary>
        private readonly bool _isPartial;

        /// <summary>
        ///     The items.
        /// </summary>
        private readonly IList<TItem> _items = new List<TItem>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSchemeObjectCore{TItem,TMaint,TMaintMutable,TItemMutable}" />
        /// class.
        /// </summary>
        /// <param name="agencyScheme">The dataStructureObject.</param>
        /// <param name="actualLocation">The actual location.</param>
        /// <param name="isServiceUrl">The is service url.</param>
        protected internal ItemSchemeObjectCore(
           [ValidatedNotNull]IItemSchemeObject<TItem> agencyScheme, 
            Uri actualLocation, 
            bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            this._isPartial = agencyScheme.Partial;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSchemeObjectCore{TItem,TMaint,TMaintMutable,TItemMutable}" />
        /// class.
        /// </summary>
        /// <param name="itemMutableObject">The dataStructureObject.</param>
        protected internal ItemSchemeObjectCore(IItemSchemeMutableObject<TItemMutable> itemMutableObject)
            : base(itemMutableObject)
        {
            this._isPartial = itemMutableObject.IsPartial;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeObjectCore{TItem,TMaint,TMaintMutable,TItemMutable}" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">The structure type.</param>
        protected internal ItemSchemeObjectCore(ItemSchemeType createdFrom, SdmxStructureType structureType)
            : base(createdFrom, structureType)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            this._isPartial = createdFrom.isPartial;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSchemeObjectCore{TItem,TMaint,TMaintMutable,TItemMutable}" />
        /// class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">The structure type.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="version">The version.</param>
        /// <param name="isFinal">The is final.</param>
        /// <param name="agencyId">The agency id.</param>
        /// <param name="id">The id.</param>
        /// <param name="uri">The uri.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="isExternalReference">The is external reference.</param>
        /// <param name="annotationsType">The annotations type.</param>
        protected internal ItemSchemeObjectCore(
            IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            object endDate, 
            object startDate, 
            string version, 
            TertiaryBool isFinal, 
            string agencyId, 
            string id, 
            Uri uri, 
            IList<TextType> name, 
            IList<TextType> description, 
            TertiaryBool isExternalReference, 
            AnnotationsType annotationsType)
            : base(
                structureType, 
                endDate, 
                startDate, 
                version, 
                isFinal, 
                agencyId, 
                id, 
                uri, 
                name, 
                description, 
                isExternalReference, 
                annotationsType)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            this._items = new List<TItem>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///  Initializes a new instance of the <see cref="ItemSchemeObjectCore{TItem,TMaint,TMaintMutable,TItemMutable}" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">The structure type.</param>
        /// <param name="version">The version.</param>
        /// <param name="agencyId">The agency id.</param>
        /// <param name="id">The id.</param>
        /// <param name="uri">The uri.</param>
        /// <param name="name">The name.</param>
        /// <param name="isExternalReference">The is external reference.</param>
        /// <param name="annotationsType">The annotations type.</param>
        protected internal ItemSchemeObjectCore(
            IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            string version, 
            string agencyId, 
            string id, 
            Uri uri, 
            IList<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType> name, 
            TertiaryBool isExternalReference, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationsType annotationsType)
            : base(structureType, version, agencyId, id, uri, name, isExternalReference, annotationsType)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            this._items = new List<TItem>();
        }

        /// <summary>
        ///  Initializes a new instance of the <see cref="ItemSchemeObjectCore{TItem,TMaint,TMaintMutable,TItemMutable}" /> class.
        /// </summary>
        /// <param name="structureType">The structure type.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="version">The version.</param>
        /// <param name="isFinal">The is final.</param>
        /// <param name="agencyId">The agency id.</param>
        /// <param name="id">The id.</param>
        /// <param name="uri">The uri.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="isExternalReference">The is external reference.</param>
        /// <param name="annotationsType">The annotations type.</param>
        protected internal ItemSchemeObjectCore(
            SdmxStructureType structureType, 
            object endDate, 
            object startDate, 
            string version, 
            TertiaryBool isFinal, 
            string agencyId, 
            string id, 
            Uri uri, 
            IList<TextType> name, 
            IList<TextType> description, 
            TertiaryBool isExternalReference, 
            AnnotationsType annotationsType)
            : base(
                structureType, 
                endDate, 
                startDate, 
                version, 
                isFinal, 
                agencyId, 
                id, 
                uri, 
                name, 
                description, 
                isExternalReference, 
                annotationsType)
        {
            this._items = new List<TItem>();
        }

        /// <summary>
        ///     Gets the items.
        /// </summary>
        public IList<TItem> Items
        {
            get
            {
                if (this._items == null)
                {
                    return new List<TItem>();
                }

                return new List<TItem>(this._items);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether partial.
        /// </summary>
        public virtual bool Partial
        {
            get
            {
                return this._isPartial;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        IMaintainableMutableObject IMaintainableObject.MutableInstance
        {
            get
            {
                return this.MutableInstance;
            }
        }

        /// <summary>
        ///     Filters the items.
        /// </summary>
        /// <param name="filterIds">The filter ids.</param>
        /// <param name="isKeepSet">if set to <c>true</c> [is keep set].</param>
        /// <returns>
        ///     A new <see cref="IItemSchemeObject{T}" /> with filtered items
        /// </returns>
        public virtual IItemSchemeObject<TItem> FilterItems(ICollection<string> filterIds, bool isKeepSet)
        {
            IItemSchemeMutableObject<IItemMutableObject> mutableScheme =
                (IItemSchemeMutableObject<IItemMutableObject>)this.MutableInstance;

            ICollection<string> removeSet = null;
            if (isKeepSet)
            {
                ISet<string> itemsInScheme = new HashSet<string>();
                foreach (TItem item in this.Items)
                {
                    itemsInScheme.Add(item.Id);
                }

                itemsInScheme.ExceptWith(filterIds);

                if (itemsInScheme.Count == 0)
                {
                    return this;
                }

                removeSet = itemsInScheme;
            }
            else
            {
                removeSet = filterIds;
            }

            foreach (string currentRemoveId in removeSet)
            {
                mutableScheme.RemoveItem(currentRemoveId);
            }

            return (IItemSchemeObject<TItem>)mutableScheme.ImmutableInstance;
        }

        /// <summary>
        ///     The deep equals internal.
        /// </summary>
        /// <param name="itemSchemeObject">The dataStructureObject.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool DeepEqualsInternal(
            IItemSchemeObject<TItem> itemSchemeObject, 
            bool includeFinalProperties)
        {
            if (itemSchemeObject == null)
            {
                return false;
            }

            if (!this.Equivalent(itemSchemeObject.Items, this.Items, includeFinalProperties))
            {
                return false;
            }

            if (this._isPartial != itemSchemeObject.Partial)
            {
                return false;
            }

            return this.DeepEqualsMaintainable(itemSchemeObject, includeFinalProperties);
        }

        /// <summary>
        ///     Add item.to <see cref="Items" />
        /// </summary>
        /// <param name="item">
        ///     The item.
        /// </param>
        protected void AddInternalItem(TItem item)
        {
            this._items.Add(item);
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>the composites</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._items, composites);
            return composites;
        }
    }
}