// -----------------------------------------------------------------------
// <copyright file="NameableCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The nameable core.
    /// </summary>
    [Serializable]
    public abstract class NameableCore : IdentifiableCore, INameableObject
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NameableCore));

        /// <summary>
        ///     The description.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _description;

        /// <summary>
        ///     The name.
        /// </summary>
        private IList<ITextTypeWrapper> _name = new List<ITextTypeWrapper>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The itemMutableObject.
        /// </param>
        protected internal NameableCore([ValidatedNotNull]INameableObject agencyScheme)
            : base(agencyScheme)
        {
            this._description = new List<ITextTypeWrapper>();
            this._name = agencyScheme.Names;
            this.ValidateNameableAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The itemMutableObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal NameableCore([ValidatedNotNull]INameableMutableObject itemMutableObject, IIdentifiableObject parent)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }

            this._description = new List<ITextTypeWrapper>();

            if (itemMutableObject.Names != null)
            {
                foreach (ITextTypeWrapperMutableObject mutable in itemMutableObject.Names)
                {
                    if (!string.IsNullOrWhiteSpace(mutable.Value))
                    {
                        this._name.Add(new TextTypeWrapperImpl(mutable, this));
                    }
                }
            }

            if (itemMutableObject.Descriptions != null)
            {
                foreach (ITextTypeWrapperMutableObject mutable0 in itemMutableObject.Descriptions)
                {
                    if (!string.IsNullOrWhiteSpace(mutable0.Value))
                    {
                        this._description.Add(new TextTypeWrapperImpl(mutable0, this));
                    }
                }
            }

            this.ValidateNameableAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM READER                    //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    
        // public NameableCore(SdmxStructureType structure, SdmxReader reader, IIdentifiableObject parent) {
        // super(structure, reader, parent);
        // string maintainableNode = reader.getCurrentElement();
        // while(reader.peek().equals("Name")) {
        // reader.moveNextElement();
        // string lang = reader.getAttributeValue("lang", false);
        // name.add(new TextTypeWrapperImpl(lang, reader.getCurrentElementValue(), this));
        // }
        // while(reader.peek().equals("Description")) {
        // reader.moveNextElement();
        // string lang = reader.getAttributeValue("lang", false);
        // description.add(new TextTypeWrapperImpl(lang, reader.getCurrentElementValue(), this));
        // }
        // if(!reader.getCurrentElement().equals(maintainableNode)) {
        // reader.moveBackToElement(maintainableNode);
        // }
        // validateNameableAttributes();
        // }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected NameableCore([ValidatedNotNull]NameableType createdFrom, SdmxStructureType structureType, IIdentifiableObject parent)
            : base(createdFrom, structureType, parent)
        {
            this._description = new List<ITextTypeWrapper>();
            this._name = TextTypeUtil.WrapTextTypeV21(createdFrom.Name, this);
            this._description = TextTypeUtil.WrapTextTypeV21(createdFrom.Description, this);
            this.ValidateNameableAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="uri">
        ///     The uri.
        /// </param>
        /// <param name="name0">
        ///     The name 0.
        /// </param>
        /// <param name="description1">
        ///     The description 1.
        /// </param>
        /// <param name="annotationsType">
        ///     The annotations type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected NameableCore(
            SdmxStructureType structureType, 
            string id, 
            Uri uri, 
            IList<TextType> name0, 
            IList<TextType> description1, 
            AnnotationsType annotationsType, 
            IIdentifiableObject parent)
            : base(structureType, id, uri, annotationsType, parent)
        {
            this._description = new List<ITextTypeWrapper>();
            this._name = TextTypeUtil.WrapTextTypeV2(name0, this);
            this._description = TextTypeUtil.WrapTextTypeV2(description1, this);
            this.ValidateNameableAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="NameableCore" /> class.
        /// </summary>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="uri">
        ///     The uri.
        /// </param>
        /// <param name="name0">
        ///     The name 0.
        /// </param>
        /// <param name="description1">
        ///     The description 1.
        /// </param>
        /// <param name="annotationsType">
        ///     The annotations type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected NameableCore(
            SdmxStructureType structureType, 
            string id, 
            Uri uri, 
            IList<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType> name0, 
            IList<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType> description1, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationsType annotationsType, 
            IIdentifiableObject parent)
            : base(structureType, id, uri, annotationsType, parent)
        {
            this._description = new List<ITextTypeWrapper>();
            this._name = TextTypeUtil.WrapTextTypeV1(name0, this);
            this._description = TextTypeUtil.WrapTextTypeV1(description1, this);
            this.ValidateNameableAttributes();
        }

        /// <summary>
        ///     Gets the all text types.
        /// </summary>
        public override IList<ITextTypeWrapper> AllTextTypes
        {
            get
            {
                IList<ITextTypeWrapper> returnList = base.AllTextTypes;
                this._name.AddAll(returnList);
                this._description.AddAll(returnList);
                return returnList;
            }
        }

        /// <summary>
        ///     Gets the description.
        /// </summary>
        public virtual string Description
        {
            get
            {
                // HACK This does not work properly
                ITextTypeWrapper ttw = TextTypeUtil.GetDefaultLocale(this._description);
                return (ttw == null) ? null : ttw.Value;
            }
        }

        /// <summary>
        ///     Gets the descriptions.
        /// </summary>
        public virtual IList<ITextTypeWrapper> Descriptions
        {
            get
            {
                return new List<ITextTypeWrapper>(this._description);
            }
        }

        /// <summary>
        ///     Gets the name.
        /// </summary>
        public virtual string Name
        {
            get
            {
                // HACK This does not work properly
                ITextTypeWrapper ttw = TextTypeUtil.GetDefaultLocale(this._name);
                return (ttw == null) ? null : ttw.Value;
            }
        }

        /// <summary>
        ///     Gets the names.
        /// </summary>
        public IList<ITextTypeWrapper> Names
        {
            get
            {
                return new ReadOnlyCollection<ITextTypeWrapper>(this._name);
            }
        }

        /// <summary>
        ///     Perform a deep equal comparison against <paramref name="nameableObject" />
        /// </summary>
        /// <param name="nameableObject">
        ///     The maintainable object to compare against
        /// </param>
        /// <param name="includeFinalProperties">
        ///     Set to true to compare final properties.
        ///     These are <see cref="_name" />, <see cref="_description" />. Otherwise those are ignored.
        /// </param>
        /// <returns>
        ///     True if the <paramref name="nameableObject" /> deep equals this instance; otherwise false
        /// </returns>
        protected internal bool DeepEqualsNameable(INameableObject nameableObject, bool includeFinalProperties)
        {
            if (nameableObject == null)
            {
                return false;
            }

            if (includeFinalProperties)
            {
                if (!this.Equivalent(this._name, nameableObject.Names, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._description, nameableObject.Descriptions, includeFinalProperties))
                {
                    return false;
                }
            }

            return this.DeepEqualsIdentifiable(nameableObject, includeFinalProperties);
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._name, composites);
            this.AddToCompositeSet(this._description, composites);
            return composites;
        }

        /// <summary>
        ///     The validate nameable attributes.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected virtual void ValidateNameableAttributes()
        {
            if (this.StructureType.EnumType != SdmxStructureEnumType.Subscription
                && this.StructureType.EnumType != SdmxStructureEnumType.Registration)
            {
                if (this._name == null || this._name.Count == 0)
                {
                    _log.WarnFormat("No names found for structure '{0}' with id '{1}'", this.StructureType, this.Id);
                    throw new SdmxSemmanticException(
                        ExceptionCode.StructureIdentifiableMissingName, 
                        this.StructureType + "  " + this.Id);
                }

                ValidationUtil.ValidateTextType(this._name, null);
                ValidationUtil.ValidateTextType(this._description, null);
            }
        }

        /// <summary>
        /// Adds the names.
        /// </summary>
        /// <param name="names">The names.</param>
        /// <exception cref="ArgumentNullException"><paramref name="names"/> is <see langword="null" />.</exception>
        protected void AddNames(IEnumerable<ITextTypeWrapper> names)
        {
            if (names == null)
            {
                throw new ArgumentNullException("names");
            }

            this._name.AddAll(names);
        }

        /// <summary>
        /// Adds the names.
        /// </summary>
        /// <param name="names">The names.</param>
        /// <exception cref="ArgumentNullException"><paramref name="names"/> is <see langword="null" />.</exception>
        protected void AddNames(params ITextTypeWrapper[] names)
        {
            if (names == null)
            {
                throw new ArgumentNullException("names");
            }

            this._name.AddAll(names);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}