// -----------------------------------------------------------------------
// <copyright file="SdmxObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Security;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects;
    using Org.Sdmxsource.Util.Attributes;
    using Org.Sdmxsource.Util.Extensions;
    using Org.Sdmxsource.Util.Reflect;

    /// <summary>
    ///     The sdmx object core.
    /// </summary>
    public abstract class SdmxObjectCore : ISdmxObject
    {
        /// <summary>
        ///     The _parent.
        /// </summary>
        private readonly ISdmxObject _parent;

        /// <summary>
        ///     The composites.
        /// </summary>
        private ISet<ISdmxObject> _composites;

        /// <summary>
        ///     The cross references.
        /// </summary>
        private ISet<ICrossReference> _crossReferences;

        /// <summary>
        ///     The structure type.
        /// </summary>
        private SdmxStructureType _structureType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxObjectCore" /> class.
        /// </summary>
        /// <param name="structureType0">
        ///     The structure type 0.
        /// </param>
        /// <param name="parent1">
        ///     The parent 1.
        /// </param>
        protected internal SdmxObjectCore(SdmxStructureType structureType0, ISdmxObject parent1)
        {
            this._structureType = structureType0;
            this._parent = parent1;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The sdmxObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected internal SdmxObjectCore([ValidatedNotNull]ISdmxObject agencyScheme)
        {
            if (agencyScheme == null)
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectNull, "object in constructor");
            }

            this._structureType = agencyScheme.StructureType;
            this._parent = agencyScheme.Parent;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxObjectCore" /> class.
        /// </summary>
        /// <param name="mutableObject">
        ///     The mutable object.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected SdmxObjectCore(IMutableObject mutableObject, ISdmxObject parent)
        {
            if (mutableObject == null)
            {
                throw new SdmxSemmanticException(ExceptionCode.ObjectNull, "object in constructor");
            }

            this._structureType = mutableObject.StructureType;
            this._parent = parent;
        }

        /// <summary>
        ///     Gets the composites.
        /// </summary>
        public virtual ISet<ISdmxObject> Composites
        {
            get
            {
                if (this._composites == null)
                {
                    this._composites = this.GetCompositesInternal();
                }

                return new HashSet<ISdmxObject>(this._composites);
            }
        }

        /// <summary>
        ///     Gets the cross references.
        /// </summary>
        public virtual ISet<ICrossReference> CrossReferences
        {
            get
            {
                if (this._crossReferences != null)
                {
                    return new HashSet<ICrossReference>(this._crossReferences);
                }

                var reflectCrossReference = new ReflectUtil<ICrossReference>();
                ISet<ICrossReference> returnSet = reflectCrossReference.GetCompositeObjects(this, this.GetType().GetProperty("CrossReferences"));
                returnSet.Remove(null);
                if (this.StructureType.IsMaintainable)
                {
                    ISet<ISdmxObject> compositSet = this.Composites;

                    foreach (ISdmxObject currentComposite in compositSet)
                    {
                        if (!ReferenceEquals(this, currentComposite))
                        {
                            returnSet.AddAll(currentComposite.CrossReferences);
                        }

                        ////currentComposite.CrossReferences.AddAll(returnSet);
                    }
                }

                this._crossReferences = returnSet;
                return returnSet;
            }
        }

        /// <summary>
        ///     Gets the parent.
        /// </summary>
        public virtual ISdmxObject Parent
        {
            get
            {
                return this._parent;
            }
        }

        /// <summary>
        ///     Gets or sets the structure type.
        /// </summary>
        public SdmxStructureType StructureType
        {
            get
            {
                return this._structureType;
            }

            set
            {
                this._structureType = value;
            }
        }

        /// <summary>
        ///     The create tertiary.
        /// </summary>
        /// <param name="isSet">
        ///     The is set.
        /// </param>
        /// <param name="valueren">
        ///     The valueren.
        /// </param>
        /// <returns>
        ///     The <see cref="TertiaryBool" /> .
        /// </returns>
        public static TertiaryBool CreateTertiary(bool isSet, bool valueren)
        {
            return SdmxObjectUtil.CreateTertiary(isSet, valueren);
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        /// ///
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        public virtual bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            throw new SdmxNotImplementedException("Deep Equals on " + this);
        }

        /// <summary>
        ///     The get composites.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <typeparam name="T">
        ///     Generic type parameter.
        /// </typeparam>
        /// <returns>
        ///     The <see cref="ISet{T}" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public virtual ISet<T> GetComposites<T>(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            ISet<T> returnSet = new HashSet<T>();
            if (this._parent != null)
            {
                foreach (ISdmxObject currentComposite in this.Composites)
                {
                    if (type.IsInstanceOfType(currentComposite))
                    {
                        returnSet.Add((T)currentComposite);
                    }
                }
            }

            return returnSet;
        }

        /// <summary>
        /// Visits all items up the parent hierarchy to return the first occurrence of parent of the given type that this
        /// SdmxObject belongs to
        /// <p />
        /// If a parent of the given type does not exist in the hierarchy, null will be returned
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="includeThisInSearch">if true then this type will be first checked to see if it is of the given type</param>
        /// <returns>The <typeparamref name="T"/>.</returns>
        public T GetParent<T>(bool includeThisInSearch) where T : class
        {
            Type type = typeof(T);
            if (this._parent != null)
            {
                if (type.IsInstanceOfType(this._parent))
                {
                    var returnObj = (T)this._parent;
                    return returnObj;
                }

                return this._parent.GetParent<T>(false);
            }

            if (type.IsInstanceOfType(this))
            {
                return this as T;
            }

            return null; // $$$default(T)/* was: null */;
        }

        /// <summary>
        ///     The create tertiary.
        /// </summary>
        /// <param name="valueren">
        ///     The valueren.
        /// </param>
        /// <returns>
        ///     The <see cref="TertiaryBool" /> .
        /// </returns>
        protected internal static TertiaryBool CreateTertiary(bool? valueren)
        {
            return SdmxObjectUtil.CreateTertiary(valueren);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     The equivalent.
        /// </summary>
        /// <typeparam name="T">Generic type param of type ISdmxObject</typeparam>
        /// <param name="list1">The list 1.</param>
        /// <param name="list2">The list 2.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool Equivalent<T>(IList<T> list1, IList<T> list2, bool includeFinalProperties)
            where T : ISdmxObject
        {
            if (ReferenceEquals(list1, list2))
            {
                return true;
            }

            if (list1 == null || list2 == null)
            {
                return false;
            }

            if (list1.Count != list2.Count)
            {
                return false;
            }

            if (list1.Count == 0)
            {
                return true;
            }

            for (int i = 0; i < list2.Count; i++)
            {
                ISdmxObject thisCurrentObj = list2[i];
                ISdmxObject thatCurrentObj = list1[i];

                if (!thisCurrentObj.DeepEquals(thatCurrentObj, includeFinalProperties))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     The equivalent.
        /// </summary>
        /// <param name="obj1">The obj 1.</param>
        /// <param name="obj2">The obj 2.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool Equivalent(ISdmxObject obj1, ISdmxObject obj2, bool includeFinalProperties)
        {
            if (obj1 == null)
            {
                return obj2 == null;
            }

            if (obj2 == null)
            {
                return false;
            }

            return obj1.DeepEquals(obj2, includeFinalProperties);
        }

        /// <summary>
        ///     The equivalent.
        /// </summary>
        /// <param name="crossRef1">
        ///     The cross ref 1.
        /// </param>
        /// <param name="crossRef2">
        ///     The cross ref 2.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        protected internal bool Equivalent(ICrossReference crossRef1, ICrossReference crossRef2)
        {
            if (crossRef1 == null)
            {
                return crossRef2 == null;
            }

            if (crossRef2 == null)
            {
                return false;
            }

            return crossRef2.TargetUrn.Equals(crossRef1.TargetUrn);
        }

        /// <summary>
        ///     The add to composite set.
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="comp">The comp.</param>
        /// <param name="compositesSet">The composites set.</param>
        /// <exception cref="ArgumentNullException"><paramref name="comp"/> is <see langword="null" />.</exception>
        protected void AddToCompositeSet<T>(ICollection<T> comp, ISet<ISdmxObject> compositesSet) where T : ISdmxObject
        {
            if (comp == null)
            {
                throw new ArgumentNullException("comp");
            }

            foreach (T composite in comp)
            {
                this.AddToCompositeSet(composite, compositesSet);
            }
        }

        /// <summary>
        ///     The add to composite set.
        /// </summary>
        /// <param name="composite">The composite.</param>
        /// <param name="composites">The composites.</param>
        /// <exception cref="ArgumentNullException"><paramref name="composites"/> is <see langword="null" />.</exception>
        protected void AddToCompositeSet(ISdmxObject composite, ISet<ISdmxObject> composites)
        {
            if (composites == null)
            {
                throw new ArgumentNullException("composites");
            }

            if (composite != null)
            {
                composites.Add(composite);
                ISet<ISdmxObject> getComposites = composite.Composites;
                if (getComposites != null)
                {
                    composites.AddAll(getComposites);
                }
            }
        }

        /// <summary>
        ///     Deeps the equals internal.
        /// </summary>
        /// <param name="bean">The bean.</param>
        /// <returns>True if equals</returns>
        protected bool DeepEqualsInternal(ISdmxObject bean)
        {
            if (bean != null && bean.StructureType == this.StructureType)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>The composites</returns>
        protected abstract ISet<ISdmxObject> GetCompositesInternal();
    }
}