// -----------------------------------------------------------------------
// <copyright file="AgencyCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using OrganisationType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.OrganisationType;

    /// <summary>
    ///     The agency core.
    /// </summary>
    [Serializable]
    public class AgencyCore : OrganisationCore<IAgency>, IAgency
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AgencyCore" /> class.
        /// </summary>
        /// <param name="agencyMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public AgencyCore(IAgencyMutableObject agencyMutableObject, IAgencyScheme parent)
            : base(agencyMutableObject, parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AgencyCore" /> class.
        /// </summary>
        /// <param name="agency">
        ///     The agency.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public AgencyCore(AgencyType agency, IAgencyScheme parent)
            : base(agency, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Agency), parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AgencyCore" /> class.
        /// </summary>
        /// <param name="organisationType">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public AgencyCore(OrganisationType organisationType, IAgencyScheme parent)
            : base(
                organisationType, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Agency), 
                organisationType.PassNoNull("organisationType").CollectorContact, 
                organisationType.PassNoNull("organisationType").id, 
                organisationType.PassNoNull("organisationType").uri, 
                organisationType.PassNoNull("organisationType").Name, 
                organisationType.PassNoNull("organisationType").Description, 
                organisationType.PassNoNull("organisationType").Annotations, 
                parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AgencyCore" /> class.
        /// </summary>
        /// <param name="agencyType">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public AgencyCore(Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AgencyType agencyType, IAgencyScheme parent)
            : base(
                agencyType, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Agency), 
                agencyType.PassNoNull("agencyType").CollectorContact, 
                agencyType.PassNoNull("agencyType").id, 
                agencyType.PassNoNull("agencyType").uri, 
                agencyType.PassNoNull("agencyType").Name, 
                null, 
                null, 
                parent)
        {
        }

        /// <summary>
        ///     Gets the full id.
        /// </summary>
        public virtual string FullId
        {
            get
            {
                var parent = this.MaintainableParent;
                if (parent.DefaultScheme)
                {
                    return this.Id;
                }

                return parent.AgencyId + "." + this.Id;
            }
        }

        /// <summary>
        ///     Gets the Maintainable Parent
        /// </summary>
        public IAgencyScheme GetMaintainableParent
        {
            get
            {
                return (IAgencyScheme)base.MaintainableParent;
            }
        }

        /// <summary>
        ///     Gets the maintainable parent.
        /// </summary>
        public new IAgencyScheme MaintainableParent
        {
            get
            {
                return (IAgencyScheme)base.MaintainableParent;
            }
        }

        /// <summary>
        ///     Gets the urn.
        /// </summary>
        public override Uri Urn
        {
            get
            {
                return new Uri(this.StructureType.UrnPrefix + this.FullId);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject != null && sdmxObject.StructureType.EnumType == SdmxStructureEnumType.Agency)
            {
                var that = (IAgency)sdmxObject;
                if (!ObjectUtil.Equivalent(this.FullId, that.FullId))
                {
                    return false;
                }

                return this.DeepEqualsInternal(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The validate id.
        /// </summary>
        /// <param name="startWithIntAllowed">
        ///     The start with int allowed.
        /// </param>
        protected internal override void ValidateId(bool startWithIntAllowed)
        {
            // Not allowed to start with an integer
            base.ValidateId(false);
        }
    }
}