﻿// -----------------------------------------------------------------------
// <copyright file="ContactCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    #region Using directives

    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common.TextType;

    #endregion

    /// <summary>
    ///     The SDMX Object core class
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base.SdmxObjectCore" />
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Objects.Base.IContact" />
    public class ContactCore : SdmxObjectCore, IContact
    {
        /// <summary>
        ///     The departments
        /// </summary>
        private readonly IList<ITextTypeWrapper> _departments = new List<ITextTypeWrapper>();

        /// <summary>
        ///     The email
        /// </summary>
        private readonly IList<string> _email = new List<string>();

        /// <summary>
        ///     The fax
        /// </summary>
        private readonly IList<string> _fax = new List<string>();

        /// <summary>
        ///     The identifier
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The name
        /// </summary>
        private readonly IList<ITextTypeWrapper> _name = new List<ITextTypeWrapper>();

        /// <summary>
        ///     The role
        /// </summary>
        private readonly IList<ITextTypeWrapper> _role = new List<ITextTypeWrapper>();

        /// <summary>
        ///     The telephone
        /// </summary>
        private readonly IList<string> _telephone = new List<string>();

        /// <summary>
        ///     The URI
        /// </summary>
        private readonly IList<string> _uri = new List<string>();

        /// <summary>
        ///     The X400
        /// </summary>
        private readonly IList<string> _x400 = new List<string>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="mutableBean">The mutable bean.</param>
        public ContactCore(IContactMutableObject mutableBean)
            : this(mutableBean, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="mutableBean">The mutable bean.</param>
        /// <param name="parent">The parent.</param>
        public ContactCore(IContactMutableObject mutableBean, ISdmxObject parent)
            : base(mutableBean, parent)
        {
            this._id = mutableBean.Id;
            this.CopyTextTypes(this._name, mutableBean.Names);
            this.CopyTextTypes(this._role, mutableBean.Roles);
            this.CopyTextTypes(this._departments, mutableBean.Departments);

            if (mutableBean.Email != null)
            {
                this._email = new List<string>(mutableBean.Email);
            }

            if (mutableBean.Telephone != null)
            {
                this._telephone = new List<string>(mutableBean.Telephone);
            }

            if (mutableBean.Fax != null)
            {
                this._fax = new List<string>(mutableBean.Fax);
            }

            if (mutableBean.Uri != null)
            {
                this._uri = new List<string>(mutableBean.Uri);
            }

            if (mutableBean.X400 != null)
            {
                this._x400 = new List<string>(mutableBean.X400);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="role">The role.</param>
        /// <param name="departments">The departments.</param>
        /// <param name="email">The email.</param>
        /// <param name="fax">The fax.</param>
        /// <param name="telephone">The telephone.</param>
        /// <param name="uri">The URI.</param>
        /// <param name="x400">The X400.</param>
        public ContactCore(
            IList<ITextTypeWrapper> name, 
            IList<ITextTypeWrapper> role, 
            IList<ITextTypeWrapper> departments, 
            IList<string> email, 
            IList<string> fax, 
            IList<string> telephone, 
            IList<string> uri, 
            IList<string> x400)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Contact), null)
        {
            if (name != null)
            {
                this._name = new List<ITextTypeWrapper>(name);
            }

            if (role != null)
            {
                this._role = new List<ITextTypeWrapper>(role);
            }

            if (departments != null)
            {
                this._departments = new List<ITextTypeWrapper>(departments);
            }

            if (email != null)
            {
                this._email = new List<string>(email);
            }

            if (fax != null)
            {
                this._fax = new List<string>(fax);
            }

            if (telephone != null)
            {
                this._telephone = new List<string>(telephone);
            }

            if (uri != null)
            {
                this._uri = new List<string>(uri);
            }

            if (x400 != null)
            {
                this._x400 = new List<string>(x400);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="contactType">Type of the contact.</param>
        /// <exception cref="ArgumentNullException"><paramref name="contactType"/> is <see langword="null" />.</exception>
        public ContactCore(ContactType contactType)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Contact), null)
        {
            if (contactType == null)
            {
                throw new ArgumentNullException("contactType");
            }

            if (ObjectUtil.ValidCollection(contactType.Department))
            {
                foreach (TextType tt in contactType.Department)
                {
                    this._departments.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Email))
            {
                this._email = new List<string>(contactType.Email);
            }

            if (ObjectUtil.ValidCollection(contactType.Fax))
            {
                this._fax = new List<string>(contactType.Fax);
            }

            if (ObjectUtil.ValidCollection(contactType.Name))
            {
                foreach (Name tt in contactType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Role))
            {
                foreach (TextType tt in contactType.Role)
                {
                    this._role.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Telephone))
            {
                this._telephone = new List<string>(contactType.Telephone);
            }

            if (ObjectUtil.ValidCollection(contactType.URI))
            {
                this._uri = new List<string>(contactType.URI.Select(x => x.ToString()).ToList());
            }

            if (ObjectUtil.ValidCollection(contactType.X400))
            {
                this._x400 = new List<string>(contactType.X400);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="contactType">Type of the contact.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="ArgumentNullException"><paramref name="contactType"/> is <see langword="null" />.</exception>
        public ContactCore(Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.ContactType contactType, ISdmxObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Contact), parent)
        {
            if (contactType == null)
            {
                throw new ArgumentNullException("contactType");
            }

            if (ObjectUtil.ValidCollection(contactType.Department))
            {
                foreach (TextType tt in contactType.Department)
                {
                    this._departments.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Email))
            {
                this._email = new List<string>(contactType.Email);
            }

            if (ObjectUtil.ValidCollection(contactType.Fax))
            {
                this._fax = new List<string>(contactType.Fax);
            }

            if (ObjectUtil.ValidCollection(contactType.Name))
            {
                foreach (Name tt in contactType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Role))
            {
                foreach (TextType tt in contactType.Role)
                {
                    this._role.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Telephone))
            {
                this._telephone = new List<string>(contactType.Telephone);
            }

            if (ObjectUtil.ValidCollection(contactType.URI))
            {
                this._uri = new List<string>(contactType.URI.Select(x => x.ToString()).ToList());
            }

            if (ObjectUtil.ValidCollection(contactType.X400))
            {
                this._x400 = new List<string>(contactType.X400);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="contactType">Type of the contact.</param>
        /// <exception cref="ArgumentNullException"><paramref name="contactType"/> is <see langword="null" />.</exception>
        public ContactCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.ContactType contactType)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Contact), null)
        {
            if (contactType == null)
            {
                throw new ArgumentNullException("contactType");
            }

            if (ObjectUtil.ValidCollection(contactType.Department))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType tt in contactType.Department)
                {
                    // Only add departments that are non-null
                    if (ObjectUtil.ValidString(tt.TypedValue))
                    {
                        this._departments.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                    }
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Email))
            {
                this._email = new List<string>(contactType.Email);
            }

            if (ObjectUtil.ValidCollection(contactType.Fax))
            {
                this._fax = new List<string>(contactType.Fax);
            }

            if (ObjectUtil.ValidCollection(contactType.Name))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType tt in contactType.Name)
                {
                    // Only add names that are non-null
                    if (ObjectUtil.ValidString(tt.TypedValue))
                    {
                        this._name.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                    }
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Role))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType tt in contactType.Role)
                {
                    // Only add roles that are non-null 
                    if (ObjectUtil.ValidString(tt.TypedValue))
                    {
                        this._role.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                    }
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Telephone))
            {
                this._telephone = new List<string>(contactType.Telephone);
            }

            if (ObjectUtil.ValidCollection(contactType.URI))
            {
                this._uri = new List<string>(contactType.URI.Select(x => x.ToString()).ToList());
            }

            if (ObjectUtil.ValidCollection(contactType.X400))
            {
                this._x400 = new List<string>(contactType.X400);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContactCore" /> class.
        /// </summary>
        /// <param name="contactType">Type of the contact.</param>
        /// <exception cref="ArgumentNullException"><paramref name="contactType"/> is <see langword="null" />.</exception>
        public ContactCore(Org.Sdmx.Resources.SdmxMl.Schemas.V10.message.ContactType contactType)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Contact), null)
        {
            if (contactType == null)
            {
                throw new ArgumentNullException("contactType");
            }

            if (ObjectUtil.ValidCollection(contactType.Department))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType tt in contactType.Department)
                {
                    this._departments.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Email))
            {
                this._email = new List<string>(contactType.Email);
            }

            if (ObjectUtil.ValidCollection(contactType.Fax))
            {
                this._fax = new List<string>(contactType.Fax);
            }

            if (ObjectUtil.ValidCollection(contactType.Name))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType tt in contactType.Name)
                {
                    this._name.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Role))
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType tt in contactType.Role)
                {
                    this._role.Add(new TextTypeWrapperImpl(tt.lang, tt.TypedValue, null));
                }
            }

            if (ObjectUtil.ValidCollection(contactType.Telephone))
            {
                this._telephone = new List<string>(contactType.Telephone);
            }

            if (ObjectUtil.ValidCollection(contactType.URI))
            {
                this._uri = new List<string>(contactType.URI.Select(x => x.ToString()).ToList());
            }

            if (ObjectUtil.ValidCollection(contactType.X400))
            {
                this._x400 = new List<string>(contactType.X400);
            }
        }

        /// <summary>
        ///     Gets the departments of the contact
        /// </summary>
        public IList<ITextTypeWrapper> Departments
        {
            get
            {
                return new List<ITextTypeWrapper>(this._departments);
            }
        }

        /// <summary>
        ///     Gets the email of the contact
        /// </summary>
        public IList<string> Email
        {
            get
            {
                return new List<string>(this._email);
            }
        }

        /// <summary>
        ///     Gets the fax of the contact
        /// </summary>
        public IList<string> Fax
        {
            get
            {
                return new List<string>(this._fax);
            }
        }

        /// <summary>
        ///     Gets the id of the contact the id, or null if there is no id
        /// </summary>
        public string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the names of the contact list of names, or an empty list if none exist
        /// </summary>
        public IList<ITextTypeWrapper> Name
        {
            get
            {
                return new List<ITextTypeWrapper>(this._name);
            }
        }

        /// <summary>
        ///     Gets the roles of the contact
        /// </summary>
        public IList<ITextTypeWrapper> Role
        {
            get
            {
                return new List<ITextTypeWrapper>(this._role);
            }
        }

        /// <summary>
        ///     Gets the telephone of the contact
        /// </summary>
        public IList<string> Telephone
        {
            get
            {
                return new List<string>(this._telephone);
            }
        }

        /// <summary>
        ///     Gets the uris of the contact
        /// </summary>
        public IList<string> Uri
        {
            get
            {
                return new List<string>(this._uri);
            }
        }

        /// <summary>
        ///     Gets X400
        /// </summary>
        public IList<string> X400
        {
            get
            {
                return new List<string>(this._x400);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> equals.
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            var contact = sdmxObject as IContact;
            if (contact != null)
            {
                if (!this.Equivalent(contact.Departments, this._departments, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(contact.Name, this._name, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(contact.Role, this._role, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(contact.Email, this._email))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(contact.Fax, this._fax))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(contact.Uri, this._uri))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(contact.X400, this._x400))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(contact.Telephone, this._telephone))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>A set of SDMX objects</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._name, composites);
            this.AddToCompositeSet(this._role, composites);
            this.AddToCompositeSet(this._departments, composites);
            return composites;
        }

        /// <summary>
        ///     Copies the text types.
        /// </summary>
        /// <param name="copyTo">The copy to.</param>
        /// <param name="mutable">The mutable.</param>
        private void CopyTextTypes(IList<ITextTypeWrapper> copyTo, IList<ITextTypeWrapperMutableObject> mutable)
        {
            if (mutable != null)
            {
                foreach (ITextTypeWrapperMutableObject currentTextType in mutable)
                {
                    copyTo.Add(new TextTypeWrapperImpl(currentTextType, this));
                }
            }
        }
    }
}