// -----------------------------------------------------------------------
// <copyright file="DataConsumerSchemeCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using OrganisationSchemeType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.OrganisationSchemeType;
    using OrganisationType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.OrganisationType;

    /// <summary>
    ///     The data consumer scheme object core.
    /// </summary>
    [Serializable]
    public sealed class DataConsumerSchemeCore :
        OrganisationSchemeCore<IDataConsumer, IDataConsumerScheme, IDataConsumerSchemeMutableObject, IDataConsumerMutableObject>,
        IDataConsumerScheme
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(DataConsumerSchemeCore));

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataConsumerSchemeCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The agencyScheme.
        /// </param>
        public DataConsumerSchemeCore(IDataConsumerSchemeMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            LOG.Debug("Building IDataConsumerScheme from Mutable Object");
            if (itemMutableObject.Items != null)
            {
                foreach (IDataConsumerMutableObject item in itemMutableObject.Items)
                {
                    this.AddInternalItem(new DataConsumerCore(item, this));
                }
            }

            if (LOG.IsDebugEnabled)
            {
                LOG.Debug("IDataConsumerScheme Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataConsumerSchemeCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        public DataConsumerSchemeCore(DataConsumerSchemeType type)
            : base(type, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme))
        {
            LOG.Debug("Building IDataConsumerScheme from 2.1");
            if (ObjectUtil.ValidCollection(type.Organisation))
            {
                foreach (DataConsumer currentDataConsumer in type.Organisation)
                {
                    this.AddInternalItem(new DataConsumerCore(currentDataConsumer.Content, this));
                }
            }

            if (LOG.IsDebugEnabled)
            {
                LOG.Debug("IDataConsumerScheme Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataConsumerSchemeCore" /> class.
        /// </summary>
        /// <param name="organisationScheme">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxException">
        ///     Throws SdmxException.
        /// </exception>
        public DataConsumerSchemeCore(OrganisationSchemeType organisationScheme)
            : base(
                organisationScheme, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme), 
                organisationScheme.PassNoNull("organisationScheme").validTo, 
                organisationScheme.PassNoNull("organisationScheme").validFrom, 
                DataConsumerScheme.FixedVersion, 
                TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False), 
                organisationScheme.PassNoNull("organisationScheme").agencyID, 
                DataConsumerScheme.FixedId, 
                organisationScheme.PassNoNull("organisationScheme").uri, 
                organisationScheme.PassNoNull("organisationScheme").Name, 
                organisationScheme.PassNoNull("organisationScheme").Description, 
                CreateTertiary(organisationScheme.PassNoNull("organisationScheme").isExternalReference), 
                organisationScheme.PassNoNull("organisationScheme").Annotations)
        {
            LOG.Debug("Building IDataConsumerScheme from 2.0");
            try
            {
                if (organisationScheme.DataProviders != null)
                {
                    foreach (DataConsumersType dataConsumersType in organisationScheme.DataConsumers)
                    {
                        foreach (OrganisationType dataConsumer in dataConsumersType.DataConsumer)
                        {
                            this.AddInternalItem(new DataConsumerCore(dataConsumer, this));
                        }
                    }
                }
            }
            catch (SdmxException ex)
            {
                throw new SdmxException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            if (LOG.IsDebugEnabled)
            {
                LOG.Debug("IDataConsumerScheme Built " + this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataConsumerSchemeCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private DataConsumerSchemeCore(IDataConsumerScheme agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            LOG.Debug("Stub IDataConsumerScheme Built");
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return DataConsumerScheme.FixedId;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IDataConsumerSchemeMutableObject MutableInstance
        {
            get
            {
                return new DataConsumerSchemeMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the version.
        /// </summary>
        public override string Version
        {
            get
            {
                return DataConsumerScheme.FixedVersion;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The agencyScheme.</param>
        /// <param name="includeFinalProperties">the include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject != null && sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsInternal((IDataConsumerScheme)sdmxObject, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IDataConsumerScheme" /> .
        /// </returns>
        public override IDataConsumerScheme GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new DataConsumerSchemeCore(this, actualLocation, isServiceUrl);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    
        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP VALIDATION                         //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}