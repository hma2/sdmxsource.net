// -----------------------------------------------------------------------
// <copyright file="AnnotationObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///     The annotation object core.
    /// </summary>
    [Serializable]
    public class AnnotationObjectCore : SdmxObjectCore, IAnnotation
    {
        /// <summary>
        ///     The _annotation type.
        /// </summary>
        private static readonly SdmxStructureType _annotationType =
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Annotation);

        /// <summary>
        ///     The _id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The _text.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _text;

        /// <summary>
        ///     The _title.
        /// </summary>
        private readonly string _title;

        /// <summary>
        ///     The _type.
        /// </summary>
        private readonly string _type;

        /// <summary>
        ///     The uri.
        /// </summary>
        private Uri _uri;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationObjectCore" /> class.
        /// </summary>
        /// <param name="annotationMutable">
        ///     The annotation mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxException">
        ///     Throws Validate exception.
        /// </exception>
        public AnnotationObjectCore(IAnnotationMutableObject annotationMutable, ISdmxObject parent)
            : base(annotationMutable, parent)
        {
            this._text = new List<ITextTypeWrapper>();
            this._id = annotationMutable.Id;
            this._title = annotationMutable.Title;
            this._type = annotationMutable.Type;
            if (annotationMutable.Text != null)
            {
                foreach (ITextTypeWrapperMutableObject mutable in annotationMutable.Text)
                {
                    if (!string.IsNullOrWhiteSpace(mutable.Value))
                    {
                        this._text.Add(new TextTypeWrapperImpl(mutable, this));
                    }
                }
            }

            this.Uri = annotationMutable.Uri;
            try
            {
                this.Validate();
            }
            catch (SdmxException ex)
            {
                throw new SdmxException("Annotation is not valid", ex);
            }
            catch (Exception th)
            {
                throw new SdmxException("Annotation is not valid", th);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM READER                    //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    
        // public AnnotationObjectCore(SdmxReader reader, ISdmxObject parent) {
        // super(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ANNOTATION), parent);
        // this.id = reader.getAttributeValue("id", false);
        // while(processNextElement(reader)) {
        // }
        // return;
        // }

        // private boolean processNextElement(SdmxReader reader) {
        // string nextEl = reader.peek();
        // if(nextEl.equals("AnnotationTitle")) {
        // reader.moveNextElement();
        // this.title = reader.getCurrentElementValue();
        // return true;
        // }
        // if(nextEl.equals("AnnotationType")) {
        // reader.moveNextElement();
        // this.type = reader.getCurrentElementValue();
        // return true;
        // } 
        // if(nextEl.equals("AnnotationURL")) {
        // reader.moveNextElement();
        // setURL(reader.getCurrentElementValue());
        // return true;
        // } 
        // if(nextEl.equals("AnnotationText")) {
        // reader.moveNextElement();
        // this.text.add(new TextTypeWrapperImpl(reader.getAttributeValue("lang", false), reader.getCurrentElementValue(), this));
        // return true;
        // } 
        // return false;
        // }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationObjectCore" /> class.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="annotation"/> is <see langword="null" />.</exception>
        public AnnotationObjectCore(AnnotationType annotation, ISdmxObject parent)
            : base(_annotationType, parent)
        {
            if (annotation == null)
            {
                throw new ArgumentNullException("annotation");
            }

            this._text = new List<ITextTypeWrapper>();
            this._title = annotation.AnnotationTitle;
            this._type = annotation.AnnotationType1;
            this._id = annotation.id;
            this._text = TextTypeUtil.WrapTextTypeV21(annotation.AnnotationText, this);
            this.Uri = annotation.AnnotationURL;
            try
            {
                this.Validate();
            }
            catch (SdmxException ex)
            {
                throw new SdmxException("Annotation is not valid", ex);
            }
            catch (Exception th)
            {
                throw new SdmxException("Annotation is not valid", th);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationObjectCore" /> class.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="annotation"/> is <see langword="null" />.</exception>
        public AnnotationObjectCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.AnnotationType annotation, 
            ISdmxObject parent)
            : base(_annotationType, parent)
        {
            if (annotation == null)
            {
                throw new ArgumentNullException("annotation");
            }

            this._text = new List<ITextTypeWrapper>();
            this._title = annotation.AnnotationTitle;
            this._type = annotation.AnnotationType1; // $$$ 1 ?
            this._text = TextTypeUtil.WrapTextTypeV2(annotation.AnnotationText, this);
            this.Uri = annotation.AnnotationURL;
            try
            {
                this.Validate();
            }
            catch (SdmxException ex)
            {
                throw new SdmxException("Annotation is not valid", ex);
            }
            catch (Exception th)
            {
                throw new SdmxException("Annotation is not valid", th);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     Initializes a new instance of the <see cref="AnnotationObjectCore" /> class.
        /// </summary>
        /// <param name="annotation">
        ///     The annotation.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="annotation"/> is <see langword="null" />.</exception>
        public AnnotationObjectCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationType annotation, 
            ISdmxObject parent)
            : base(_annotationType, parent)
        {
            if (annotation == null)
            {
                throw new ArgumentNullException("annotation");
            }

            this._text = new List<ITextTypeWrapper>();
            this._title = annotation.AnnotationTitle;
            this._type = annotation.AnnotationType1;
            this._text = TextTypeUtil.WrapTextTypeV1(annotation.AnnotationText, this);
            this.Uri = annotation.AnnotationURL;
            try
            {
                this.Validate();
            }
            catch (SdmxException ex)
            {
                throw new SdmxException("Annotation is not valid", ex);
            }
            catch (Exception th)
            {
                throw new SdmxException("Could not create Annotation as it did not validate", th);
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the text.
        /// </summary>
        public virtual IList<ITextTypeWrapper> Text
        {
            get
            {
                return new List<ITextTypeWrapper>(this._text);
            }
        }

        /// <summary>
        ///     Gets the title.
        /// </summary>
        public virtual string Title
        {
            get
            {
                return this._title;
            }
        }

        /// <summary>
        ///     Gets the type.
        /// </summary>
        public virtual string Type
        {
            get
            {
                return this._type;
            }
        }

        /// <summary>
        ///     Gets the url.
        /// </summary>
        /// <exception cref="SdmxException">Throws Validate exception.</exception>
        public Uri Uri
        {
            get
            {
                return this._uri;
            }

            private set
            {
                if (value == null)
                {
                    this._uri = null;
                    return;
                }

                try
                {
                    this._uri = value;
                }
                catch (SdmxException ex)
                {
                    throw new SdmxException("Could not create attribute 'annotationURL' with value '" + value + "'", ex);
                }
                catch (Exception th)
                {
                    throw new SdmxException("Could not create attribute 'annotationURL' with value '" + value + "'", th);
                }
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">the include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (!includeFinalProperties)
            {
                // If we don't care about the final properties, then don't check this
                return true;
            }

            return this.Equals(sdmxObject);
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((AnnotationObjectCore)obj);
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = this._id != null ? this._id.GetHashCode() : 0;
                foreach (var tt in this._text)
                {
                    hashCode = (hashCode * 397) ^ (tt.Locale != null ? tt.Locale.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (tt.Value != null ? tt.Value.GetHashCode() : 0);
                }

                hashCode = (hashCode * 397) ^ (this._title != null ? this._title.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._type != null ? this._type.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (this._uri != null ? this._uri.GetHashCode() : 0);
                return hashCode;
            }
        }

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Title:" + this.Title);
            sb.Append("Type:" + this.Type);
            sb.Append("URI:" + this.Uri);

            return sb.ToString();
        }

        /// <summary>
        ///     The equals.
        /// </summary>
        /// <param name="other">The annotationObject.</param>
        /// <returns>True if equals</returns>
        protected bool Equals(AnnotationObjectCore other)
        {
            if (other is AnnotationObjectCore)
            {
                AnnotationObjectCore annotation = other;
                if (!string.Equals(this._id, annotation.Id))
                {
                    return false;
                }

                if (!string.Equals(this._title, annotation.Title))
                {
                    return false;
                }

                if (!string.Equals(this._type, annotation.Type))
                {
                    return false;
                }

                if (!Equals(this._uri, annotation.Uri))
                {
                    return false;
                }

                if (!this.Equivalent(this._text, annotation.Text, true))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>set of composites</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = new HashSet<ISdmxObject>();
            this.AddToCompositeSet(this._text, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        private void Validate()
        {
            ValidationUtil.ValidateTextType(this._text, null);
        }
    }
}