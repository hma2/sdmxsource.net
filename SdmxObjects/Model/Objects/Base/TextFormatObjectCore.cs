// -----------------------------------------------------------------------
// <copyright file="TextFormatObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;

    using TextFormatType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.TextFormatType;

    /// <summary>
    ///     The text format dataStructureObject core.
    /// </summary>
    public class TextFormatObjectCore : SdmxObjectCore, ITextFormat
    {
        /// <summary>
        ///     The _text format type.
        /// </summary>
        private static readonly SdmxStructureType _textFormatType =
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TextFormat);

        /// <summary>
        ///     The _time interval regex.
        /// </summary>
        private static readonly Regex _timeIntervalRegex =
            new Regex("^P(([0-9]+Y)?([0-9]+M)?([0-9]+D)?)(T([0-9]+H)?([0-9]+M)?([0-9]+S)?)?$", RegexOptions.Compiled);

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(TextFormatObjectCore));

        /// <summary>
        ///     The _decimals.
        /// </summary>
        private readonly long? _decimals;

        /// <summary>
        ///     The _end time.
        /// </summary>
        private readonly ISdmxDate _endTime;

        /// <summary>
        ///     The _end value.
        /// </summary>
        private readonly decimal? _endValue;

        /// <summary>
        ///     The _interval.
        /// </summary>
        private readonly decimal? _interval;

        /// <summary>
        ///     The _is sequence.
        /// </summary>
        private readonly TertiaryBool _isSequence = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

        /// <summary>
        ///     The _max length.
        /// </summary>
        private readonly long? _maxLength;

        /// <summary>
        ///     The _max value.
        /// </summary>
        private readonly decimal? _maxValue;

        /// <summary>
        ///     The _min value.
        /// </summary>
        private readonly decimal? _minValue;

        /// <summary>
        ///     The _pattern.
        /// </summary>
        private readonly string _pattern;

        /// <summary>
        ///     The _start time.
        /// </summary>
        private readonly ISdmxDate _startTime;

        /// <summary>
        ///     The _start value.
        /// </summary>
        private readonly decimal? _startValue;

        /// <summary>
        ///     The _text type.
        /// </summary>
        private readonly TextType _textType;

        /// <summary>
        ///     The _time interval.
        /// </summary>
        private readonly string _timeInterval;

        /// <summary>
        ///     The _is multi lingual.
        /// </summary>
        private readonly TertiaryBool _multilingual = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

        /// <summary>
        ///     The _min length.
        /// </summary>
        private long? _minLength;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextFormatObjectCore" /> class.
        /// </summary>
        /// <param name="textFormatMutable">
        ///     The text format mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public TextFormatObjectCore(ITextFormatMutableObject textFormatMutable, ISdmxObject parent)
            : base(textFormatMutable, parent)
        {
            this._textType = textFormatMutable.TextType;
            if (textFormatMutable.Sequence != null)
            {
                this._isSequence = textFormatMutable.Sequence;
            }

            this._maxLength = textFormatMutable.MaxLength;
            this._minLength = textFormatMutable.MinLength;
            this._startValue = textFormatMutable.StartValue;
            this._endValue = textFormatMutable.EndValue;
            this._maxValue = textFormatMutable.MaxValue;
            this._minValue = textFormatMutable.MinValue;
            this._interval = textFormatMutable.Interval;
            this._timeInterval = textFormatMutable.TimeInterval;
            this._decimals = textFormatMutable.Decimals;
            this._pattern = textFormatMutable.Pattern;

            // This is correct. See SDMXRI-488
            this._multilingual = textFormatMutable.Multilingual;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextFormatObjectCore" /> class.
        /// </summary>
        /// <param name="textFormatType">
        ///     The text format type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textFormatType"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxNotImplementedException">
        ///     type is not supported.
        /// </exception>
        public TextFormatObjectCore(TextFormatType textFormatType, ISdmxObject parent)
            : base(_textFormatType, parent)
        {
            if (textFormatType == null)
            {
                throw new ArgumentNullException("textFormatType");
            }

            if (textFormatType.textType != null)
            {
                this._textType = TextTypeUtil.GetTextType(textFormatType.textType);
            }

            // This is correct as the default value is true. So only if it is false we need to set it
            // See SDMXRI-488 for more information
            if (!textFormatType.isMultiLingual)
            {
                this._multilingual = TertiaryBool.ParseBoolean(textFormatType.isMultiLingual);
            }

            if (textFormatType.isSequence != null && textFormatType.isSequence.Value)
            {
                this._isSequence = TertiaryBool.ParseBoolean(textFormatType.isSequence.Value);
            }

            if (textFormatType.maxLength.HasValue)
            {
                this._maxLength = (long?)textFormatType.maxLength;
            }

            if (textFormatType.minLength != null)
            {
                this._minLength = (long?)textFormatType.minLength;
            }

            if (textFormatType.startValue.HasValue)
            {
                this._startValue = textFormatType.startValue.Value;
            }

            if (textFormatType.endValue.HasValue)
            {
                this._endValue = textFormatType.endValue.Value;
            }

            if (textFormatType.maxValue.HasValue)
            {
                this._maxValue = textFormatType.maxValue.Value;
            }

            if (textFormatType.minValue.HasValue)
            {
                this._minValue = (long)textFormatType.minValue.Value;
            }

            if (textFormatType.interval.HasValue)
            {
                this._interval = textFormatType.interval.Value;
            }

            if (textFormatType.timeInterval != null)
            {
                this._timeInterval = textFormatType.timeInterval.ToString();
            }

            if (textFormatType.decimals.HasValue)
            {
                this._decimals = (long)textFormatType.decimals.Value;
            }

            if (!string.IsNullOrEmpty(textFormatType.pattern))
            {
                this._pattern = textFormatType.pattern;
            }

            if (textFormatType.endTime != null)
            {
                this._endTime = new SdmxDateCore(textFormatType.endTime.ToString());
            }

            if (textFormatType.startTime != null)
            {
                this._startTime = new SdmxDateCore(textFormatType.startTime.ToString());
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TextFormatObjectCore" /> class.
        /// </summary>
        /// <param name="textFormat">
        ///     The text format.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="textFormat"/> is <see langword="null" />.</exception>
        public TextFormatObjectCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.TextFormatType textFormat, 
            ISdmxObject parent)
            : base(_textFormatType, parent)
        {
            if (textFormat == null)
            {
                throw new ArgumentNullException("textFormat");
            }

            if (textFormat.textType != null)
            {
                string textType = textFormat.textType;

                this._textType = BuildTextType(textType);

                if (textFormat.isSequence.HasValue)
                {
                    this._isSequence = TertiaryBool.ParseBoolean(textFormat.isSequence);
                }

                if (textFormat.maxLength.HasValue)
                {
                    this._maxLength = (long?)textFormat.maxLength;
                }

                if (textFormat.minLength.HasValue)
                {
                    this._minLength = (long?)textFormat.minLength;
                }

                if (textFormat.startValue.HasValue)
                {
                    this._startValue = (decimal)textFormat.startValue.Value;
                }

                if (textFormat.endValue.HasValue)
                {
                    this._endValue = new decimal(textFormat.endValue.Value);
                }

                if (textFormat.interval.HasValue)
                {
                    this._interval = new decimal(textFormat.interval.Value);
                }

                if (textFormat.timeInterval != null)
                {
                    this._timeInterval = textFormat.timeInterval.ToString();
                }

                if (textFormat.decimals.HasValue)
                {
                    this._decimals = (long)textFormat.decimals.Value;
                }

                if (!string.IsNullOrEmpty(textFormat.pattern))
                {
                    this._pattern = textFormat.pattern;
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the decimals.
        /// </summary>
        public virtual long? Decimals
        {
            get
            {
                return this._decimals;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets the end time.
        /// </summary>
        public virtual ISdmxDate EndTime
        {
            get
            {
                return this._endTime;
            }
        }

        /// <summary>
        ///     Gets the end value.
        /// </summary>
        public decimal? EndValue
        {
            get
            {
                return this._endValue;
            }
        }

        /// <summary>
        ///     Gets the interval.
        /// </summary>
        public decimal? Interval
        {
            get
            {
                return this._interval;
            }
        }

        /// <summary>
        ///     Gets the max length.
        /// </summary>
        public long? MaxLength
        {
            get
            {
                return this._maxLength;
            }
        }

        /// <summary>
        ///     Gets the max value.
        /// </summary>
        public decimal? MaxValue
        {
            get
            {
                return this._maxValue;
            }
        }

        /// <summary>
        ///     Gets the min length.
        /// </summary>
        public long? MinLength
        {
            get
            {
                return this._minLength;
            }
        }

        /// <summary>
        ///     Gets the min value.
        /// </summary>
        public decimal? MinValue
        {
            get
            {
                return this._minValue;
            }
        }

        /// <summary>
        ///     Gets the is multi lingual.
        /// </summary>
        public TertiaryBool Multilingual
        {
            get
            {
                return this._multilingual;
            }
        }

        /// <summary>
        ///     Gets the pattern.
        /// </summary>
        public string Pattern
        {
            get
            {
                return this._pattern;
            }
        }

        /// <summary>
        ///     Gets whether the values are intended to be ordered, and it may work in combination with the interval attribute
        ///     If null, then interpret as N/A
        /// </summary>
        /// <value>
        ///     The sequence
        /// </value>
        public virtual TertiaryBool Sequence
        {
            get
            {
                return this._isSequence;
            }
        }

        /// <summary>
        ///     Gets the start time.
        /// </summary>
        public virtual ISdmxDate StartTime
        {
            get
            {
                return this._startTime;
            }
        }

        /// <summary>
        ///     Gets the start value.
        /// </summary>
        public decimal? StartValue
        {
            get
            {
                return this._startValue;
            }
        }

        /// <summary>
        ///     Gets the text type.
        /// </summary>
        public virtual TextType TextType
        {
            get
            {
                return this._textType;
            }
        }

        /// <summary>
        ///     Gets the time interval.
        /// </summary>
        public string TimeInterval
        {
            get
            {
                return this._timeInterval;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The dataStructureObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ITextFormat)sdmxObject;
                if (!ObjectUtil.Equivalent(this._textType, that.TextType))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._isSequence, that.Sequence))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._multilingual, that.Multilingual))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._minLength, that.MinLength))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._maxLength, that.MaxLength))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._minValue, that.MinValue))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._maxValue, that.MaxValue))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._startValue, that.StartValue))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._endValue, that.EndValue))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._interval, that.Interval))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._timeInterval, that.TimeInterval))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._decimals, that.Decimals))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._pattern, that.Pattern))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._startTime, that.StartTime))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._endTime, that.EndTime))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            return new HashSet<ISdmxObject>();
        }

        /// <summary>
        /// Builds the type of the text.
        /// </summary>
        /// <param name="textType">Type of the text.</param>
        /// <returns>The <see cref="TextType" />.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "It is OK. Big switch used.")]
        private static TextType BuildTextType(string textType)
        {
            TextType localTextType = null;
            switch (textType)
            {
                // TODO java 0.9.9 bug. No Integer case
                // http://www.metadatatechnology.com/mantis/view.php?id=1425
                case TextTypeTypeConstants.BigInteger:
                    localTextType = TextType.GetFromEnum(TextEnumType.BigInteger);
                    break;
                case TextTypeTypeConstants.Boolean:
                    localTextType = TextType.GetFromEnum(TextEnumType.Boolean);
                    break;
                case TextTypeTypeConstants.Count:
                    localTextType = TextType.GetFromEnum(TextEnumType.Count);
                    break;
                case TextTypeTypeConstants.Date:
                    localTextType = TextType.GetFromEnum(TextEnumType.Date);
                    break;
                case TextTypeTypeConstants.DateTime:
                    localTextType = TextType.GetFromEnum(TextEnumType.DateTime);
                    break;
                case TextTypeTypeConstants.Day:
                    localTextType = TextType.GetFromEnum(TextEnumType.Day);
                    break;
                case TextTypeTypeConstants.Decimal:
                    localTextType = TextType.GetFromEnum(TextEnumType.Decimal);
                    break;
                case TextTypeTypeConstants.Double:
                    localTextType = TextType.GetFromEnum(TextEnumType.Double);
                    break;
                case TextTypeTypeConstants.Duration:
                    localTextType = TextType.GetFromEnum(TextEnumType.Duration);
                    break;
                case TextTypeTypeConstants.ExclusiveValueRange:
                    localTextType = TextType.GetFromEnum(TextEnumType.ExclusiveValueRange);
                    break;
                case TextTypeTypeConstants.Float:
                    localTextType = TextType.GetFromEnum(TextEnumType.Float);
                    break;
                case TextTypeTypeConstants.InclusiveValueRange:
                    localTextType = TextType.GetFromEnum(TextEnumType.InclusiveValueRange);
                    break;
                case TextTypeTypeConstants.Incremental:
                    localTextType = TextType.GetFromEnum(TextEnumType.Incremental);
                    break;
                case TextTypeTypeConstants.Integer:
                    localTextType = TextType.GetFromEnum(TextEnumType.Integer);
                    break;
                case TextTypeTypeConstants.Long:
                    localTextType = TextType.GetFromEnum(TextEnumType.Long);
                    break;
                case TextTypeTypeConstants.Month:
                    localTextType = TextType.GetFromEnum(TextEnumType.Month);
                    break;
                case TextTypeTypeConstants.MonthDay:
                    localTextType = TextType.GetFromEnum(TextEnumType.MonthDay);
                    break;
                case TextTypeTypeConstants.ObservationalTimePeriod:
                    localTextType = TextType.GetFromEnum(TextEnumType.ObservationalTimePeriod);
                    break;
                case TextTypeTypeConstants.Short:
                    localTextType = TextType.GetFromEnum(TextEnumType.Short);
                    break;
                case TextTypeTypeConstants.String:
                    localTextType = TextType.GetFromEnum(TextEnumType.String);
                    break;
                case TextTypeTypeConstants.Time:
                    localTextType = TextType.GetFromEnum(TextEnumType.Time);
                    break;
                case TextTypeTypeConstants.Timespan:
                    localTextType = TextType.GetFromEnum(TextEnumType.Timespan);
                    break;
                case TextTypeTypeConstants.URI:
                    localTextType = TextType.GetFromEnum(TextEnumType.Uri);
                    break;
                case TextTypeTypeConstants.Year:
                    localTextType = TextType.GetFromEnum(TextEnumType.Year);
                    break;
                case TextTypeTypeConstants.YearMonth:
                    localTextType = TextType.GetFromEnum(TextEnumType.YearMonth);
                    break;
            }

            return localTextType;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._minLength != null)
            {
                if (this._minLength == 0)
                {
                    _log.Warn("Text format of 0 converted to 1");
                    this._minLength = 1;
                }
                else if (this._minLength < 0)
                {
                    throw new SdmxSemmanticException(
                        "Invalid Text Format, min length must be a positive integer - got "
                        + this._minLength);
                }
            }

            if (this._maxLength <= 0)
            {
                throw new SdmxSemmanticException(
                    "Invalid Text Format, max length must be a positive integer - got "
                    + this._maxLength);
            }

            if (this._minLength != null && this._maxLength != null)
            {
                if (this._minLength.Value > this._maxLength.Value)
                {
                    throw new SdmxSemmanticException(
                        "Invalid Text Format, min length can not be greater then max length");
                }
            }

            if (this._minValue != null && this._maxValue != null)
            {
                if (this._minValue.Value > this._maxValue)
                {
                    throw new SdmxSemmanticException("Invalid Text Format, min value can not be greater then max value");
                }
            }

            if (this._decimals <= 0)
            {
                throw new SdmxSemmanticException(
                    "Invalid Text Format, decimals must be a positive integer - got "
                    + this._decimals);
            }

            if (this._startTime != null && this._endTime != null)
            {
                if (this._startTime.IsLater(this._endTime))
                {
                    throw new SdmxSemmanticException("Invalid Text Format, start time can not be after end time");
                }
            }

            if (this._isSequence.IsTrue)
            {
                this.ValidateSequence();
            }
            else
            {
                this.ValidateNonSequence();
            }

            if (!string.IsNullOrWhiteSpace(this._timeInterval))
            {
                // Validate that the time interval matches the allowed xs:duration format PnYnMnDTnHnMnS - Use the RegEx, and make sure the string
                // is greater then length 1, as the regex does not ensure that there is any content after the P
                // The Regex ensures if there is anything after the P, it is of the valid type, example P5Y and P91DT12M are both valid formats
                // Pattern timeIntervalPattern = ILOG.J2CsMapping.Text.Pattern.Compile("P(([0-9]+Y)?([0-9]+M)?([0-9]+D)?)(T([0-9]+H)?([0-9]+M)?([0-9]+S)?)?");
                if (this._timeInterval.Length == 1 || !_timeIntervalRegex.IsMatch(this._timeInterval))
                {
                    throw new SdmxSemmanticException(
                        "Invalid time interval, pattern must be PnYnMnDTnHnMnS, where n=positive integer, and each section is optional after each n (example P5Y)");
                }
            }
        }

        /// <summary>
        /// Validates the non sequence.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Invalid Text Format, time interval can only be set if isSequence is set to true
        /// or
        /// Invalid Text Format, start time can only be set if isSequence is set to true
        /// or
        /// Invalid Text Format, interval can only be set if isSequence is set to true
        /// or
        /// Invalid Text Format, start value can only be set if isSequence is set to true
        /// </exception>
        private void ValidateNonSequence()
        {
            if (!string.IsNullOrWhiteSpace(this._timeInterval))
            {
                throw new SdmxSemmanticException("Invalid Text Format, time interval can only be set if isSequence is set to true");
            }

            if (this._startTime != null)
            {
                throw new SdmxSemmanticException("Invalid Text Format, start time can only be set if isSequence is set to true");
            }

            if (this._interval != null)
            {
                throw new SdmxSemmanticException("Invalid Text Format, interval can only be set if isSequence is set to true");
            }

            if (this._startValue != null)
            {
                throw new SdmxSemmanticException("Invalid Text Format, start value can only be set if isSequence is set to true");
            }
        }

        /// <summary>
        /// Validates the sequence.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        /// Invalid Text Format, time interval or interval must be set if isSequence is set to true
        /// or
        /// Invalid Text Format, start time must be set if time interval is set
        /// or
        /// Invalid Text Format, start value must be set if interval is set
        /// </exception>
        private void ValidateSequence()
        {
            if (this._timeInterval == null && this._interval == null)
            {
                throw new SdmxSemmanticException("Invalid Text Format, time interval or interval must be set if isSequence is set to true");
            }

            if (!string.IsNullOrWhiteSpace(this._timeInterval) && this._startTime == null)
            {
                throw new SdmxSemmanticException("Invalid Text Format, start time must be set if time interval is set");
            }

            if (this._interval != null && this._startValue == null)
            {
                throw new SdmxSemmanticException("Invalid Text Format, start value must be set if interval is set");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}