// -----------------------------------------------------------------------
// <copyright file="RepresentationCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    using TextFormatType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.TextFormatType;

    /// <summary>
    ///     The representation core.
    /// </summary>
    [Serializable]
    public class RepresentationCore : SdmxStructureCore, IRepresentation
    {
        /// <summary>
        ///     The _local representation.
        /// </summary>
        private static readonly SdmxStructureType _localRepresentation =
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.LocalRepresentation);

        /// <summary>
        ///     The representation ref.
        /// </summary>
        private readonly ICrossReference _representation;

        /// <summary>
        ///     The text format.
        /// </summary>
        private readonly ITextFormat _textFormat;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationCore" /> class.
        /// </summary>
        /// <param name="representationMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="representationMutableObject"/> is <see langword="null" />.</exception>
        public RepresentationCore(IRepresentationMutableObject representationMutableObject, IIdentifiableObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.LocalRepresentation), parent)
        {
            if (representationMutableObject == null)
            {
                throw new ArgumentNullException("representationMutableObject");
            }

            if (representationMutableObject.TextFormat != null)
            {
                this._textFormat = new TextFormatObjectCore(representationMutableObject.TextFormat, this);
            }

            if (representationMutableObject.Representation != null)
            {
                this._representation = new CrossReferenceImpl(this, representationMutableObject.Representation);
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationCore" /> class.
        /// </summary>
        /// <param name="conceptRepresentation">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="conceptRepresentation"/> is <see langword="null" />.</exception>
        public RepresentationCore(ConceptRepresentation conceptRepresentation, IIdentifiableObject parent)
            : base(_localRepresentation, parent)
        {
            if (conceptRepresentation == null)
            {
                throw new ArgumentNullException("conceptRepresentation");
            }

            if (conceptRepresentation.TextFormat != null)
            {
                this._textFormat = new TextFormatObjectCore(conceptRepresentation.TextFormat, this);
            }

            if (conceptRepresentation.Enumeration != null)
            {
                this._representation = RefUtil.CreateReference(this, conceptRepresentation.Enumeration);
                if (conceptRepresentation.EnumerationFormat != null)
                {
                    this._textFormat = new TextFormatObjectCore(conceptRepresentation.EnumerationFormat, this);
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationCore" /> class.
        /// </summary>
        /// <param name="representationType">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="representationType"/> is <see langword="null" />.</exception>
        public RepresentationCore(RepresentationType representationType, IIdentifiableObject parent)
            : base(_localRepresentation, parent)
        {
            if (representationType == null)
            {
                throw new ArgumentNullException("representationType");
            }

            if (representationType.TextFormat != null)
            {
                this._textFormat = new TextFormatObjectCore(representationType.TextFormat, this);
            }

            ItemSchemeReferenceBaseType reference;
            if (representationType.CodelistEnumeration != null)
            {
                reference = representationType.CodelistEnumeration;
            }
            else
            {
                reference = representationType.ConceptSchemeEnumeration;
            }

            if (reference != null)
            {
                this._representation = RefUtil.CreateReference(this, reference);
                if (representationType.EnumerationFormat != null)
                {
                    this._textFormat = new TextFormatObjectCore(representationType.EnumerationFormat, this);
                }
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationCore" /> class.
        /// </summary>
        /// <param name="textFormat0">
        ///     The text format 0.
        /// </param>
        /// <param name="codelistAgency">
        ///     The codelist agency.
        /// </param>
        /// <param name="codelistId">
        ///     The codelist id.
        /// </param>
        /// <param name="codelistVersion">
        ///     The codelist version.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="parent"/> is <see langword="null" />.</exception>
        public RepresentationCore(
            TextFormatType textFormat0, 
            string codelistAgency, 
            string codelistId, 
            string codelistVersion, 
            IIdentifiableObject parent)
            : base(_localRepresentation, parent)
        {
            if (textFormat0 != null)
            {
                this._textFormat = new TextFormatObjectCore(textFormat0, this);
            }

            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (ObjectUtil.ValidOneString(codelistAgency, codelistId, codelistVersion))
            {
                if (string.IsNullOrWhiteSpace(codelistAgency))
                {
                    codelistAgency = this.MaintainableParent.AgencyId;
                }

                SdmxStructureType structureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList);
                if (parent.MaintainableParent is ICrossSectionalDataStructureObject)
                {
                    var dimension = parent as IDimension;
                    if (dimension != null)
                    {
                        if (dimension.MeasureDimension)
                        {
                            structureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme);
                        }
                    }
                }

                this._representation = new CrossReferenceImpl(
                    this, 
                    codelistAgency, 
                    codelistId, 
                    codelistVersion, 
                    structureType);
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationCore" /> class.
        /// </summary>
        /// <param name="codelistId">
        ///     The codelist id.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal RepresentationCore(string codelistId, IIdentifiableObject parent)
            : base(_localRepresentation, parent)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (!string.IsNullOrWhiteSpace(codelistId))
            {
                this._representation = new CrossReferenceImpl(
                    this, 
                    parent.MaintainableParent.AgencyId, 
                    codelistId, 
                    null, 
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the representation ref.
        /// </summary>
        public virtual ICrossReference Representation
        {
            get
            {
                return this._representation;
            }
        }

        /// <summary>
        ///     Gets the text format.
        /// </summary>
        public virtual ITextFormat TextFormat
        {
            get
            {
                return this._textFormat;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IRepresentation)sdmxObject;

                if (!this.Equivalent(this._representation, that.Representation))
                {
                    return false;
                }

                if (!this.Equivalent(this._textFormat, that.TextFormat, includeFinalProperties))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>Composites set</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._textFormat, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._representation == null && this._textFormat == null)
            {
                throw new SdmxSemmanticException("Representation must have a codelist reference or text format");
            }
        }
    }
}