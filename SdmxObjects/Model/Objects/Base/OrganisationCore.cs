﻿// -----------------------------------------------------------------------
// <copyright file="OrganisationCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-11
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Attributes;

    using ContactType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ContactType;
    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     OrganisationCore class
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    /// <seealso cref="Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base.ItemCore" />
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.Objects.Base.IOrganisation" />
    public class OrganisationCore<TItem> : ItemCore, IOrganisation
        where TItem : IItemObject
    {
        /// <summary>
        ///     The contacts
        /// </summary>
        private readonly List<IContact> _contacts = new List<IContact>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationCore{TItem}" /> class.
        /// </summary>
        /// <param name="organisationMutableObject">The organisation mutable object.</param>
        /// <param name="parent">The parent.</param>
        public OrganisationCore([ValidatedNotNull]IOrganisationMutableObject organisationMutableObject, IItemSchemeObject<TItem> parent)
            : base(organisationMutableObject, parent)
        {
            if (organisationMutableObject == null)
            {
                throw new ArgumentNullException("organisationMutableObject");
            }

            foreach (IContactMutableObject currentContact in organisationMutableObject.Contacts)
            {
                this._contacts.Add(new ContactCore(currentContact, this));
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationCore{TItem}" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="structureType">Type of the structure.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public OrganisationCore(OrganisationType type, SdmxStructureType structureType, IItemSchemeObject<TItem> parent)
            : base(type, structureType, parent)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.ContactType contact in type.Contact)
            {
                this._contacts.Add(new ContactCore(contact, this));
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationCore{TItem}" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">Type of the structure.</param>
        /// <param name="contact">The contact.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="uri">The URI.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="annotationsType">Type of the annotations.</param>
        /// <param name="parent">The parent.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "contact", Justification = "Follows the SdmxSource Java version.")]
        public OrganisationCore(
            IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            ContactType contact, 
            string id, 
            Uri uri, 
            IList<TextType> name, 
            IList<TextType> description, 
            AnnotationsType annotationsType, 
            IIdentifiableObject parent)
            : base(createdFrom, structureType, id, uri, name, description, annotationsType, parent)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationCore{TItem}" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">Type of the structure.</param>
        /// <param name="contact">The contact.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="uri">The URI.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="annotationsType">Type of the annotations.</param>
        /// <param name="parent">The parent.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "contact", Justification = "Follows the SdmxSource Java version.")]
        public OrganisationCore(
            IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.ContactType contact, 
            string id, 
            Uri uri, 
            IList<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType> name, 
            IList<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.TextType> description, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationsType annotationsType, 
            IIdentifiableObject parent)
            : base(createdFrom, structureType, id, uri, name, description, annotationsType, parent)
        {
        }

        /// <summary>
        ///     Gets a list of contacts for the organisation
        /// </summary>
        public IList<IContact> Contacts
        {
            get
            {
                return new List<IContact>(this._contacts);
            }
        }

        /// <summary>
        ///     Deeps the equals internal.
        /// </summary>
        /// <param name="organisation">The organisation.</param>
        /// <param name="includeFinalProperties">if set to <c>true</c> [include final properties].</param>
        /// <returns>The internal equal</returns>
        public bool DeepEqualsInternal(IOrganisation organisation, bool includeFinalProperties)
        {
            if (organisation == null || !this.Equivalent(this._contacts, organisation.Contacts, includeFinalProperties))
            {
                return false;
            }

            return this.DeepEqualsNameable(organisation, includeFinalProperties);
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._contacts, composites);
            return composites;
        }
    }
}