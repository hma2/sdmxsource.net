// -----------------------------------------------------------------------
// <copyright file="OrganisationUnitSchemeObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base
{
    using System;
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The organisation unit scheme object core.
    /// </summary>
    [Serializable]
    public class OrganisationUnitSchemeObjectCore :
        ItemSchemeObjectCore<IOrganisationUnit, IOrganisationUnitSchemeObject, IOrganisationUnitSchemeMutableObject, IOrganisationUnitMutableObject>, 
        IOrganisationUnitSchemeObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(OrganisationUnitSchemeObjectCore));

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationUnitSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        public OrganisationUnitSchemeObjectCore(IOrganisationUnitSchemeMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            Log.Debug("Building IOrganisationUnitSchemeObject from Mutable Object");
            if (itemMutableObject.Items != null)
            {
                foreach (IOrganisationUnitMutableObject oumb in itemMutableObject.Items)
                {
                    this.AddInternalItem(new OrganisationUnitCore(oumb, this));
                }
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IOrganisationUnitSchemeObject Built " + base.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationUnitSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        public OrganisationUnitSchemeObjectCore(OrganisationUnitSchemeType type)
            : base(type, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme))
        {
            Log.Debug("Building IOrganisationUnitSchemeObject from 2.1 SDMX");
            if (ObjectUtil.ValidCollection(type.Organisation))
            {
                foreach (OrganisationUnit currentType in type.Organisation)
                {
                    this.AddInternalItem(new OrganisationUnitCore(currentType.Content, this));
                }
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IOrganisationUnitSchemeObject Built " + this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OrganisationUnitSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">The sdmxObject.</param>
        /// <param name="actualLocation">The actual location.</param>
        /// <param name="isServiceUrl">The is service url.</param>
        /// <exception cref="SdmxSemmanticException">The fail validation exception</exception>
        private OrganisationUnitSchemeObjectCore(
            IOrganisationUnitSchemeObject agencyScheme, 
            Uri actualLocation, 
            bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            Log.Debug("Stub IOrganisationUnitSchemeObject Built");
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IOrganisationUnitSchemeMutableObject MutableInstance
        {
            get
            {
                return new OrganisationUnitSchemeMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsInternal((IOrganisationUnitSchemeObject)sdmxObject, includeFinalProperties);
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS                                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IOrganisationUnitSchemeObject" /> .
        /// </returns>
        public override IOrganisationUnitSchemeObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new OrganisationUnitSchemeObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     Gets the unit.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The organisation unit</returns>
        /// <exception cref="SdmxSemmanticException">The Cannot resolve parent exception</exception>
        private IOrganisationUnit GetUnit(string id)
        {
            foreach (IOrganisationUnit organisationUnitBean in this.Items)
            {
                if (organisationUnitBean.Id.Equals(id))
                {
                    return organisationUnitBean;
                }
            }

            throw new SdmxSemmanticException(ExceptionCode.CannotResolveParent, id);
        }

        /// <summary>
        ///     Recurses the parent map.
        /// </summary>
        /// <param name="children">The children.</param>
        /// <param name="parentBean">The parent bean.</param>
        /// <param name="parentChildMap">The parent child map.</param>
        /// <exception cref="SdmxSemmanticException">The parent recursive loop exception</exception>
        private void RecurseParentMap(
            ISet<IOrganisationUnit> children, 
            IOrganisationUnit parentBean, 
            IDictionary<IOrganisationUnit, ISet<IOrganisationUnit>> parentChildMap)
        {
            // If the child is also a parent
            if (children != null)
            {
                if (children.Contains(parentBean))
                {
                    throw new SdmxSemmanticException(ExceptionCode.ParentRecursiveLoop, parentBean.Id);
                }

                foreach (IOrganisationUnit currentChild in children)
                {
                    ISet<IOrganisationUnit> currentChildren;
                    if (parentChildMap.TryGetValue(currentChild, out currentChildren))
                    {
                        this.RecurseParentMap(currentChildren, parentBean, parentChildMap);
                    }
                }
            }
        }

        /// <summary>
        ///     Validates this instance.
        /// </summary>
        private void Validate()
        {
            // CHECK FOR DUPLICATION OF URN & ILLEGAL PARENTING
            IDictionary<IOrganisationUnit, ISet<IOrganisationUnit>> parentChildMap =
                new Dictionary<IOrganisationUnit, ISet<IOrganisationUnit>>();

            /* foreach */
            foreach (IOrganisationUnit currentUnit in this.Items)
            {
                if (ObjectUtil.ValidString(currentUnit.ParentUnit))
                {
                    IOrganisationUnit parentUnit = this.GetUnit(currentUnit.ParentUnit);

                    ISet<IOrganisationUnit> children;
                    if (!parentChildMap.TryGetValue(parentUnit, out children))
                    {
                        children = new HashSet<IOrganisationUnit>();
                        parentChildMap.Add(parentUnit, children);
                    }

                    children.Add(currentUnit);

                    // Check that the parent code is not directly or indirectly a child of the code it is parenting
                    ISet<IOrganisationUnit> currentChildren;
                    if (parentChildMap.TryGetValue(currentUnit, out currentChildren))
                    {
                        this.RecurseParentMap(currentChildren, parentUnit, parentChildMap);
                    }
                }
            }
        }
    }
}