// -----------------------------------------------------------------------
// <copyright file="CodelistRefCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The codelist ref core.
    /// </summary>
    [Serializable]
    public class CodelistRefCore : SdmxStructureCore, ICodelistRef
    {
        /// <summary>
        ///     The alias.
        /// </summary>
        private readonly string _alias;

        /// <summary>
        ///     The codelist reference.
        /// </summary>
        private readonly ICrossReference _codelistReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistRefCore" /> class.
        /// </summary>
        /// <param name="codelistRefMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public CodelistRefCore(ICodelistRefMutableObject codelistRefMutableObject, ISdmxStructure parent)
            : base(codelistRefMutableObject, parent)
        {
            this._alias = codelistRefMutableObject.Alias;
            if (codelistRefMutableObject.CodelistReference != null)
            {
                this._codelistReference = new CrossReferenceImpl(this, codelistRefMutableObject.CodelistReference);
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistRefCore" /> class.
        /// </summary>
        /// <param name="agencyId">
        ///     The agency id.
        /// </param>
        /// <param name="maintainableId">
        ///     The maintainable id.
        /// </param>
        /// <param name="version">
        ///     The version.
        /// </param>
        /// <param name="alias0">
        ///     The alias 0.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public CodelistRefCore(
            string agencyId, 
            string maintainableId, 
            string version, 
            string alias0, 
            ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeListRef), parent)
        {
            this._alias = alias0;
            this._codelistReference = new CrossReferenceImpl(
                this, 
                agencyId, 
                maintainableId, 
                version, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistRefCore" /> class.
        /// </summary>
        /// <param name="urn">
        ///     The urn.
        /// </param>
        /// <param name="alias0">
        ///     The alias 0.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public CodelistRefCore(string urn, string alias0, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeListRef), parent)
        {
            this._alias = alias0;
            this._codelistReference = new CrossReferenceImpl(this, urn);
            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodelistRefCore" /> class.
        /// </summary>
        /// <param name="urn">
        ///     The urn.
        /// </param>
        /// <param name="alias0">
        ///     The alias 0.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public CodelistRefCore(Uri urn, string alias0, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeListRef), parent)
        {
            this._alias = alias0;
            this._codelistReference = new CrossReferenceImpl(this, urn);
            this.Validate();
        }

        /// <summary>
        ///     Gets the alias.
        /// </summary>
        public virtual string Alias
        {
            get
            {
                return this._alias;
            }
        }

        /// <summary>
        ///     Gets the codelist reference.
        /// </summary>
        public virtual ICrossReference CodelistReference
        {
            get
            {
                return this._codelistReference;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ICodelistRef)sdmxObject;
                if (!ObjectUtil.Equivalent(this._alias, that.Alias))
                {
                    return false;
                }

                if (!this.Equivalent(this._codelistReference, that.CodelistReference))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._codelistReference == null)
            {
                throw new SdmxSemmanticException("HierarchicalCodelist, Codelist Reference Missing a Reference");
            }

            if (this._codelistReference.TargetReference.EnumType != SdmxStructureEnumType.CodeList)
            {
                throw new SdmxSemmanticException(
                    "Referenced structure should be "
                    + SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList).StructureType
                    + " but is declared as " + this._codelistReference.TargetReference.GetType());
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP VALIDATION                         //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}