// -----------------------------------------------------------------------
// <copyright file="CodeCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The code core.
    /// </summary>
    [Serializable]
    public class CodeCore : ItemCore, ICode
    {
        /// <summary>
        ///     The _code type.
        /// </summary>
        private static readonly SdmxStructureType _codeType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code);

        /// <summary>
        ///     The parent code.
        /// </summary>
        private readonly string _parentCode;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        public CodeCore(ICodelistObject parent, ICodeMutableObject itemMutableObject)
            : base(itemMutableObject, parent)
        {
            this._parentCode = string.IsNullOrWhiteSpace(itemMutableObject.ParentCode)
                                  ? null
                                  : itemMutableObject.ParentCode;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM READER                    //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    
        // public CodeCore(ICodelistObject parent, SdmxReader reader) {
        // super(validateRootElement(reader), reader, parent);
        // if(reader.moveToElement("Parent", "Code")) {
        // reader.moveNextElement();  //Move to the Ref Node
        // if(!reader.getCurrentElement().equals("Ref")) {
        // throw new SdmxSemmanticException("Expecting 'Ref' element after 'Parent' element for Code");
        // }
        // this.parentCode = reader.getAttributeValue("id", true);
        // } 
        // }

        // private static SdmxStructureType validateRootElement(SdmxReader reader) {
        // if(!reader.getCurrentElement().equals("Code")) {
        // throw new SdmxSemmanticException("Can not construct code - expecting 'Code' Element in SDMX, actual element:" + reader.getCurrentElementValue());
        // }
        // return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CODE);
        // }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public CodeCore(ICodelistObject parent, CodeType code)
            : base(code, _codeType, parent)
        {
            var parentItem = code.GetTypedParent<LocalCodeReferenceType>();
            this._parentCode = (parentItem != null) ? parentItem.GetTypedRef<LocalCodeRefType>().id : null;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public CodeCore(ICodelistObject parent, Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CodeType code)
            : base(code, _codeType, code.PassNoNull("code").value, null, code.PassNoNull("code").Description, null, code.PassNoNull("code").Annotations, parent)
        {
            // In SDMX 2.0 it is perfectly valid for the XML to state a code has a parentCode which is blank. e.g.:
            // <str:Code value="aCode" parentCode="">
            // This can cause issues when manipulating the beans, so police the input by not setting the 
            // parentCode if it is an empty string.
            this._parentCode = string.IsNullOrWhiteSpace(code.parentCode) ? null : code.parentCode;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CodeCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="code">
        ///     The sdmxObject.
        /// </param>
        public CodeCore(ICodelistObject parent, Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.CodeType code)
            : base(code, _codeType, code.PassNoNull("code").value, null, code.PassNoNull("code").Description, null, code.PassNoNull("code").Annotations, parent)
        {
        }

        /// <summary>
        ///     Gets the parent code.
        /// </summary>
        public virtual string ParentCode
        {
            get
            {
                return this._parentCode;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ICode)sdmxObject;
                if (!ObjectUtil.Equivalent(this._parentCode, that.ParentCode))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
    }
}