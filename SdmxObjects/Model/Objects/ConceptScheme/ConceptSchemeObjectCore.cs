// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.ConceptScheme
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    using ConceptType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ConceptType;
    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The concept scheme object core.
    /// </summary>
    [Serializable]
    public sealed class ConceptSchemeObjectCore :
        ItemSchemeObjectCore<IConceptObject, IConceptSchemeObject, IConceptSchemeMutableObject, IConceptMutableObject>, 
        IConceptSchemeObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(IConceptSchemeObject));

        /// <summary>
        ///     The item by identifier
        /// </summary>
        [NonSerialized]
        private IDictionary<string, IConceptObject> _itemById = new Dictionary<string, IConceptObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="conceptScheme">
        ///     The concept scheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ConceptSchemeObjectCore(IConceptSchemeMutableObject conceptScheme)
            : base(conceptScheme)
        {
            Log.Debug("Building IConceptSchemeObject from Mutable Object");
            try
            {
                if (conceptScheme.Items != null)
                {
                    foreach (IConceptMutableObject concept in conceptScheme.Items)
                    {
                        this.AddInternalItem(new ConceptCore(this, concept));
                    }
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IConceptSchemeObject Built " + this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="conceptScheme">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ConceptSchemeObjectCore(ConceptSchemeType conceptScheme)
            : base(conceptScheme, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme))
        {
            Log.Debug("Building IConceptSchemeObject from 2.1 SDMX");
            try
            {
                foreach (Concept currentItem in conceptScheme.Item)
                {
                    this.AddInternalItem(new ConceptCore(this, currentItem.Content));
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IConceptSchemeObject Built " + this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="conceptScheme">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ConceptSchemeObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ConceptSchemeType conceptScheme)
            : base(
                conceptScheme, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme), 
                conceptScheme.PassNoNull("conceptScheme").validTo, 
                conceptScheme.PassNoNull("conceptScheme").validFrom, 
                conceptScheme.PassNoNull("conceptScheme").version, 
                CreateTertiary(conceptScheme.PassNoNull("conceptScheme").isFinal), 
                conceptScheme.PassNoNull("conceptScheme").agencyID, 
                conceptScheme.PassNoNull("conceptScheme").id, 
                conceptScheme.PassNoNull("conceptScheme").uri, 
                conceptScheme.PassNoNull("conceptScheme").Name, 
                conceptScheme.PassNoNull("conceptScheme").Description, 
                CreateTertiary(conceptScheme.PassNoNull("conceptScheme").isExternalReference), 
                conceptScheme.PassNoNull("conceptScheme").Annotations)
        {
            Log.Debug("Building IConceptSchemeObject from 2.0 SDMX");
            try
            {
                foreach (ConceptType currentItem in conceptScheme.Concept)
                {
                    this.AddInternalItem(new ConceptCore(this, currentItem));
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IConceptSchemeObject Built " + this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeObjectCore" /> class.
        ///     Default Scheme
        /// </summary>
        /// <param name="concepts">
        ///     The concepts.
        /// </param>
        /// <param name="agencyId">
        ///     The agency Id.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="concepts"/> is <see langword="null" />.</exception>
        public ConceptSchemeObjectCore(IList<ConceptType> concepts, string agencyId)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme), 
                null, 
                null, 
                ConceptSchemeObject.DefaultSchemeVersion, 
                TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False), 
                agencyId, 
                ConceptSchemeObject.DefaultSchemeId, 
                null, 
                DefaultName, 
                null, 
                TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False), 
                null)
        {
            if (concepts == null)
            {
                throw new ArgumentNullException("concepts");
            }

            Log.Debug("Building IConceptSchemeObject from Stand Alone 2.0 Concepts");
            try
            {
                foreach (ConceptType currentItem in concepts)
                {
                    if (!currentItem.agencyID.Equals(this.AgencyId))
                    {
                        var sb =
                            new StringBuilder(
                                "Attempting to create Default Concept Scheme from v1.0 List of concepts, and was provided with ");
                        sb.Append("a concept that reference different agency reference ('");
                        sb.Append(currentItem.agencyID);
                        sb.Append("') to the scheme agency ('");
                        sb.Append(this.AgencyId);
                        sb.Append("')");
                        throw new SdmxSemmanticException(ExceptionCode.FailValidation, sb.ToString());
                    }

                    this.AddInternalItem(new ConceptCore(this, currentItem));
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IConceptSchemeObject Built " + this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeObjectCore" /> class.
        ///     Default Scheme
        /// </summary>
        /// <param name="agencyId">
        ///     The agency Id.
        /// </param>
        /// <param name="concepts">
        ///     The concepts.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="concepts"/> is <see langword="null" />.</exception>
        public ConceptSchemeObjectCore(
            string agencyId, 
            IList<Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.ConceptType> concepts)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme), 
                null, 
                null, 
                ConceptSchemeObject.DefaultSchemeVersion, 
                TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False), 
                agencyId, 
                ConceptSchemeObject.DefaultSchemeId, 
                null, 
                DefaultName, 
                null, 
                TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False), 
                null)
        {
            if (concepts == null)
            {
                throw new ArgumentNullException("concepts");
            }

            Log.Debug("Building IConceptSchemeObject from 1.0 SDMX");
            try
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.ConceptType currentItem in concepts)
                {
                    if (!currentItem.agency.Equals(this.AgencyId))
                    {
                        var sb =
                            new StringBuilder(
                                "Attempting to create Default Concept Scheme from v1.0 List of concepts, and was provided with ");
                        sb.Append("a concept that reference different agency reference ('");
                        sb.Append("') to the scheme agency ('");
                        sb.Append(this.AgencyId);
                        sb.Append("')");

                        throw new SdmxSemmanticException(ExceptionCode.FailValidation, sb.ToString());
                    }

                    this.AddInternalItem(new ConceptCore(this, currentItem));
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
            catch (Exception ex)
            {
                throw new SdmxException(ex, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IConceptSchemeObject Built " + this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private ConceptSchemeObjectCore(IConceptSchemeObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            Log.Debug("Stub IConceptSchemeObject Built");
        }

        /// <summary>
        ///     Gets a value indicating whether default scheme.
        /// </summary>
        public bool DefaultScheme
        {
            get
            {
                return this.Id.Equals(ConceptSchemeObject.DefaultSchemeId);
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IConceptSchemeMutableObject MutableInstance
        {
            get
            {
                return new ConceptSchemeMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the default name.
        /// </summary>
        private static IList<TextType> DefaultName
        {
            get
            {
                IList<TextType> returnList = new List<TextType>();

                var tt = new TextType { TypedValue = ConceptSchemeObject.DefaultSchemeName };
                returnList.Add(tt);
                return returnList;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsInternal((IConceptSchemeObject)sdmxObject, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get item by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        ///     Returns the concept by id, or null if there is no concept with the given id
        /// </returns>
        public IConceptObject GetItemById(string id)
        {
            if (this._itemById == null || this._itemById.Count == 0)
            {
                this._itemById = new Dictionary<string, IConceptObject>();
                foreach (IConceptObject currentConcept in this.Items)
                {
                    this._itemById.Add(currentConcept.Id, currentConcept);
                }
            }

            return this._itemById.GetOrDefault(id);
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IConceptSchemeObject" /> .
        /// </returns>
        public override IConceptSchemeObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new ConceptSchemeObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     The validate id.
        /// </summary>
        /// <param name="startWithIntAllowed">
        ///     The start with int allowed.
        /// </param>
        protected internal override void ValidateId(bool startWithIntAllowed)
        {
            // Not allowed to start with an integer
            base.ValidateId(false);
        }

        /// <summary>
        ///     The i get concept.
        /// </summary>
        /// <param name="concepts">
        ///     The concepts.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="IConceptObject" /> .
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private IConceptObject GetConcept(IList<IConceptObject> concepts, string id)
        {
            foreach (IConceptObject current in concepts)
            {
                if (current.Id.Equals(id))
                {
                    return current;
                }
            }

            throw new SdmxSemmanticException(ExceptionCode.CannotResolveParent, id);
        }

        /// <summary>
        ///     Recurses the map checking the children of each child, if one of the children is the parent code, then an excetpion
        ///     is thrown
        /// </summary>
        /// <param name="children">
        ///     Set of children object.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="parentChildMap">Parent list. </param>
        private void RecurseParentMap(
            ISet<IConceptObject> children, 
            IConceptObject parent, 
            IDictionary<IConceptObject, ISet<IConceptObject>> parentChildMap)
        {
            // If the child is also a parent
            if (children != null)
            {
                if (children.Contains(parent))
                {
                    throw new SdmxSemmanticException(ExceptionCode.ParentRecursiveLoop, parent.Id);
                }

                foreach (IConceptObject currentChild in children)
                {
                    this.RecurseParentMap(parentChildMap.GetOrDefault(currentChild), parent, parentChildMap);
                }
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            var urns = new HashSet<Uri>();
            if (this.Id.Equals(ConceptSchemeObject.DefaultSchemeId))
            {
                if (!this.Version.Equals(ConceptSchemeObject.DefaultSchemeVersion))
                {
                    throw new SdmxSemmanticException(
                        ExceptionCode.FailValidation, 
                        ConceptSchemeObject.DefaultSchemeId + " can only be version " + ConceptSchemeObject.DefaultSchemeVersion);
                }

                if (this.IsFinal.IsTrue)
                {
                    throw new SdmxSemmanticException(
                        ExceptionCode.FailValidation, 
                        ConceptSchemeObject.DefaultSchemeId + " can not be made final");
                }
            }

            if (this.Items != null)
            {
                var parentChildMap = new Dictionary<IConceptObject, ISet<IConceptObject>>();

                foreach (IConceptObject concept in this.Items)
                {
                    if (urns.Contains(concept.Urn))
                    {
                        throw new SdmxSemmanticException(ExceptionCode.DuplicateUrn, concept.Urn);
                    }

                    urns.Add(concept.Urn);
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(concept.ParentConcept))
                        {
                            IConceptObject parent = this.GetConcept(this.Items, concept.ParentConcept);
                            ISet<IConceptObject> children;
                            if (!parentChildMap.TryGetValue(parent, out children))
                            {
                                children = new HashSet<IConceptObject>();
                                parentChildMap.Add(parent, children);
                            }

                            children.Add(concept);
                            ISet<IConceptObject> childSet = null;
                            if (parentChildMap.TryGetValue(concept, out childSet))
                            {
                                // Check that the parent code is not directly or indirectly a child of the code it is parenting
                                this.RecurseParentMap(childSet, parent, parentChildMap);
                            }
                        }
                    }
                    catch (SdmxSemmanticException ex)
                    {
                        throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
                    }
                    catch (Exception th)
                    {
                        throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS                                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP VALIDATION                         //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////
    }
}