﻿// -----------------------------------------------------------------------
// <copyright file="MetadataTargetRegionCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     MetadataTargetRegionCore class
    /// </summary>
    public class MetadataTargetRegionCore : SdmxStructureCore, IMetadataTargetRegion
    {
        /// <summary>
        ///     The _attributes
        /// </summary>
        private readonly IList<IKeyValues> _attributes;

        /// <summary>
        ///     The _is include
        /// </summary>
        private readonly bool _isInclude;

        /// <summary>
        ///     The _key
        /// </summary>
        private readonly IList<IMetadataTargetKeyValues> _key;

        /// <summary>
        ///     The _metadata target
        /// </summary>
        private readonly string _metadataTarget;

        /// <summary>
        ///     The _report
        /// </summary>
        private readonly string _report;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetRegionCore" /> class.
        /// </summary>
        /// <param name="mutable">The mutable.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="SdmxSemmanticException">Semantic exception</exception>
        public MetadataTargetRegionCore(IMetadataTargetRegionMutableObject mutable, IContentConstraintObject parent)
            : base(mutable, parent)
        {
            this._key = new List<IMetadataTargetKeyValues>();
            this._attributes = new List<IKeyValues>();

            this._report = mutable.Report;
            this._metadataTarget = mutable.MetadataTarget;
            if (mutable.Key != null)
            {
                foreach (IMetadataTargetKeyValuesMutable currentMetadataTarget in mutable.Key)
                {
                    this._key.Add(new MetadataTargetKeyValuesCore(currentMetadataTarget, this));
                }
            }

            if (mutable.Attributes != null)
            {
                foreach (IKeyValuesMutable currentKeyValue in mutable.Attributes)
                {
                    this._attributes.Add(new KeyValuesCore(currentKeyValue, this));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataTargetRegionCore" /> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="parent">The parent.</param>
        /// <exception cref="SdmxSemmanticException">Semantic exception</exception>
        public MetadataTargetRegionCore(MetadataTargetRegionType type, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataTargetRegion), parent)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this._key = new List<IMetadataTargetKeyValues>();
            this._attributes = new List<IKeyValues>();

            this._isInclude = type.include;
            this._report = type.Report;
            this._metadataTarget = type.MetadataTarget;
            var metadataTargetRegionKeyTypes = type.GetTypedKeyValue<MetadataTargetRegionKeyType>();
            if (metadataTargetRegionKeyTypes != null)
            {
                foreach (MetadataTargetRegionKeyType cv in metadataTargetRegionKeyTypes)
                {
                    this._key.Add(new MetadataTargetKeyValuesCore(cv, this));
                }
            }

            var metadataAttributeValueSetTypes = type.GetTypedAttribute<MetadataAttributeValueSetType>();
            if (metadataAttributeValueSetTypes != null)
            {
                foreach (var cv in metadataAttributeValueSetTypes)
                {
                    this._attributes.Add(new KeyValuesCore(cv, this));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the attributes restrictions for the metadata target region
        /// </summary>
        public IList<IKeyValues> Attributes
        {
            get
            {
                return new List<IKeyValues>(this._attributes);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the information reported is to be included or excluded
        /// </summary>
        public bool IsInclude
        {
            get
            {
                return this._isInclude;
            }
        }

        /// <summary>
        ///     Gets the key values restrictions for the metadata target region
        /// </summary>
        public IList<IMetadataTargetKeyValues> Key
        {
            get
            {
                return new List<IMetadataTargetKeyValues>(this._key);
            }
        }

        /// <summary>
        ///     Gets the metadata target
        /// </summary>
        public string MetadataTarget
        {
            get
            {
                return this._metadataTarget;
            }
        }

        /// <summary>
        ///     Gets the report
        /// </summary>
        public string Report
        {
            get
            {
                return this._report;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">The sdmxObject.</param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IMetadataTargetRegion)sdmxObject;

                if (!this.Equivalent(this._attributes, that.Attributes, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._key, that.Key, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._metadataTarget, that.MetadataTarget))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._report, that.Report))
                {
                    return false;
                }

                if (this._isInclude != that.IsInclude)
                {
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._key, composites);
            this.AddToCompositeSet(this._attributes, composites);
            return composites;
        }

        /// <summary>
        ///     Validates this instance.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">
        ///     Metadata Target Region missing mandatory 'report' identifier
        ///     or
        ///     Metadata Target Region missing mandatory 'metadata target' identifier
        /// </exception>
        private void Validate()
        {
            if (!ObjectUtil.ValidString(this._report))
            {
                throw new SdmxSemmanticException("Metadata Target Region missing mandatory 'report' identifier");
            }

            if (!ObjectUtil.ValidString(this._metadataTarget))
            {
                throw new SdmxSemmanticException(
                    "Metadata Target Region missing mandatory 'metadata target' identifier");
            }
        }
    }
}