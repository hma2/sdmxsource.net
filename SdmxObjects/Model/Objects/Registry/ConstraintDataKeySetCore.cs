// -----------------------------------------------------------------------
// <copyright file="ConstraintDataKeySetCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The constraint data key set core.
    /// </summary>
    [Serializable]
    public class ConstraintDataKeySetCore : SdmxStructureCore, IConstraintDataKeySet
    {
        /// <summary>
        ///     The contstrained keys.
        /// </summary>
        private readonly IList<IConstrainedDataKey> _contstrainedKeys;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintDataKeySetCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="mutable"/> is <see langword="null" />.</exception>
        public ConstraintDataKeySetCore(IConstraintDataKeySetMutableObject mutable, IConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKeyset), parent)
        {
            if (mutable == null)
            {
                throw new ArgumentNullException("mutable");
            }

            this._contstrainedKeys = new List<IConstrainedDataKey>();

            foreach (IConstrainedDataKeyMutableObject each in mutable.ConstrainedDataKeys)
            {
                IConstrainedDataKey cdk = new ConstrainedDataKeyCore(each, this);
                if (ObjectUtil.ValidCollection(cdk.KeyValues))
                {
                    this._contstrainedKeys.Add(cdk);
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintDataKeySetCore" /> class.
        /// </summary>
        /// <param name="dataKeySetType">
        ///     The data key set type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="dataKeySetType"/> is <see langword="null" />.</exception>
        public ConstraintDataKeySetCore(DataKeySetType dataKeySetType, IConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKeyset), parent)
        {
            if (dataKeySetType == null)
            {
                throw new ArgumentNullException("dataKeySetType");
            }

            this._contstrainedKeys = new List<IConstrainedDataKey>();
            var dataKeyTypes = dataKeySetType.Key;
            if (dataKeyTypes != null)
            {
                foreach (var currentKey in dataKeyTypes)
                {
                    IConstrainedDataKey cdk = new ConstrainedDataKeyCore(currentKey, this);
                    if (ObjectUtil.ValidCollection(cdk.KeyValues))
                    {
                        this._contstrainedKeys.Add(cdk);
                    }
                }
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintDataKeySetCore" /> class.
        /// </summary>
        /// <param name="keySetType">Type of the meta data key set.</param>
        /// <param name="parent">The parent.</param>
        public ConstraintDataKeySetCore(MetadataKeySetType keySetType, IConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKeyset), parent)
        {
            if (keySetType == null)
            {
                throw new ArgumentNullException("keySetType");
            }

            var metadataKeyTypes = keySetType.Key;
            if (metadataKeyTypes != null)
            {
                foreach (var currentKey in metadataKeyTypes)
                {
                    IConstrainedDataKey cdk = new ConstrainedDataKeyCore(currentKey, this);
                    if (ObjectUtil.ValidCollection(cdk.KeyValues))
                    {
                        this._contstrainedKeys.Add(cdk);
                    }
                }
            }
        }

        /// <summary>
        ///     Gets the constrained data keys.
        /// </summary>
        public virtual IList<IConstrainedDataKey> ConstrainedDataKeys
        {
            get
            {
                return new List<IConstrainedDataKey>(this._contstrainedKeys);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The dataStructureObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IConstraintDataKeySet)sdmxObject;
                if (!this.Equivalent(this._contstrainedKeys, that.ConstrainedDataKeys, includeFinalProperties))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._contstrainedKeys, composites);
            return composites;
        }
    }
}