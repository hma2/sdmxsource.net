// -----------------------------------------------------------------------
// <copyright file="ReleaseCalendarCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The release calendar core.
    /// </summary>
    [Serializable]
    public class ReleaseCalendarCore : SdmxStructureCore, IReleaseCalendar
    {
        /// <summary>
        ///     The offset.
        /// </summary>
        private readonly string _offset;

        /// <summary>
        ///     The periodicity.
        /// </summary>
        private readonly string _periodicity;

        /// <summary>
        ///     The tolerance.
        /// </summary>
        private readonly string _tolerance;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReleaseCalendarCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public ReleaseCalendarCore(IReleaseCalendarMutableObject mutable, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReleaseCalendar), parent)
        {
            if (mutable != null)
            {
                this._offset = mutable.Offset;
                this._periodicity = mutable.Periodicity;
                this._tolerance = mutable.Tolerance;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReleaseCalendarCore" /> class.
        /// </summary>
        /// <param name="releaseCalendarType">
        ///     The release calendar type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="releaseCalendarType"/> is <see langword="null" />.</exception>
        public ReleaseCalendarCore(ReleaseCalendarType releaseCalendarType, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReleaseCalendar), parent)
        {
            if (releaseCalendarType == null)
            {
                throw new ArgumentNullException("releaseCalendarType");
            }

            this._offset = releaseCalendarType.Offset;
            this._periodicity = releaseCalendarType.Periodicity;
            this._tolerance = releaseCalendarType.Tolerance;
        }

        /// <summary>
        ///     Gets the offset.
        /// </summary>
        public virtual string Offset
        {
            get
            {
                return this._offset;
            }
        }

        /// <summary>
        ///     Gets the periodicity.
        /// </summary>
        public virtual string Periodicity
        {
            get
            {
                return this._periodicity;
            }
        }

        /// <summary>
        ///     Gets the tolerance.
        /// </summary>
        public virtual string Tolerance
        {
            get
            {
                return this._tolerance;
            }
        }

        /// <summary>
        ///     The create mutable object.
        /// </summary>
        /// <returns> The <see cref="IReleaseCalendarMutableObject" /> . </returns>
        public virtual IReleaseCalendarMutableObject CreateMutableObject()
        {
            return new ReleaseCalendarMutableCore(this);
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IReleaseCalendar)sdmxObject;
                if (!ObjectUtil.Equivalent(this._offset, that.Offset))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._periodicity, that.Periodicity))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._tolerance, that.Tolerance))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}