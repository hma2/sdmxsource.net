// -----------------------------------------------------------------------
// <copyright file="ReferencePeriodCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The reference period core.
    /// </summary>
    [Serializable]
    public class ReferencePeriodCore : SdmxStructureCore, IReferencePeriod
    {
        /// <summary>
        ///     The end time.
        /// </summary>
        private readonly ISdmxDate _endTime;

        /// <summary>
        ///     The start time.
        /// </summary>
        private readonly ISdmxDate _startTime;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReferencePeriodCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="mutable"/> is <see langword="null" />.</exception>
        public ReferencePeriodCore(IReferencePeriodMutableObject mutable, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReferencePeriod), parent)
        {
            if (mutable == null)
            {
                throw new ArgumentNullException("mutable");
            }

            // These items are mandatory and thus should exist
            if (mutable.StartTime != null)
            {
                this._startTime = new SdmxDateCore(mutable.StartTime, TimeFormatEnumType.DateTime);
            }

            if (mutable.EndTime != null)
            {
                this._endTime = new SdmxDateCore(mutable.EndTime, TimeFormatEnumType.DateTime);
            }

            if (this._startTime == null)
            {
                throw new SdmxSemmanticException("ReferencePeriodCore - start time can not be null");
            }

            if (this._endTime == null)
            {
                throw new SdmxSemmanticException("ReferencePeriodCore - end time can not be null");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReferencePeriodCore" /> class.
        /// </summary>
        /// <param name="refPeriodType">
        ///     The ref period type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="refPeriodType"/> is <see langword="null" />.</exception>
        public ReferencePeriodCore(ReferencePeriodType refPeriodType, IContentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReferencePeriod), parent)
        {
            if (refPeriodType == null)
            {
                throw new ArgumentNullException("refPeriodType");
            }

            this._startTime = new SdmxDateCore(refPeriodType.startTime, TimeFormatEnumType.DateTime);
            this._endTime = new SdmxDateCore(refPeriodType.endTime, TimeFormatEnumType.DateTime);
            if (this._startTime == null)
            {
                throw new SdmxSemmanticException("ReferencePeriodCore - start time can not be null");
            }

            if (this._endTime == null)
            {
                throw new SdmxSemmanticException("ReferencePeriodCore - start time can not be null");
            }
        }

        /// <summary>
        ///     Gets the end time.
        /// </summary>
        public virtual ISdmxDate EndTime
        {
            get
            {
                return this._endTime;
            }
        }

        /// <summary>
        ///     Gets the start time.
        /// </summary>
        public virtual ISdmxDate StartTime
        {
            get
            {
                return this._startTime;
            }
        }

        /// <summary>
        ///     The create mutable object.
        /// </summary>
        /// <returns> The <see cref="IReferencePeriodMutableObject" /> . </returns>
        public virtual IReferencePeriodMutableObject CreateMutableObject()
        {
            return new ReferencePeriodMutableCore(this);
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IReferencePeriod)sdmxObject;
                if (!ObjectUtil.Equivalent(this._startTime, that.StartTime))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._endTime, that.EndTime))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
    }
}