// -----------------------------------------------------------------------
// <copyright file="AttachmentConstraintObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    /// <summary>
    ///     The attachment constraint object core.
    /// </summary>
    [Serializable]
    public class AttachmentConstraintObjectCore :
        ConstraintObjectCore<IAttachmentConstraintObject, IMaintainableMutableObject>, 
        IAttachmentConstraintObject
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="AttachmentConstraintObjectCore" /> class.
        /// </summary>
        /// <param name="attachment">The attachment.</param>
        public AttachmentConstraintObjectCore(IAttachmentConstraintMutableObject attachment)
            : base(attachment)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttachmentConstraintObjectCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        public AttachmentConstraintObjectCore(AttachmentConstraintType type)
            : base(
                type, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttachmentConstraint), 
                type != null ? type.GetConstraintAttachmentType<AttachmentConstraintAttachmentType>() : null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttachmentConstraintObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The sdmxObject.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private AttachmentConstraintObjectCore(
            IAttachmentConstraintObject agencyScheme, 
            Uri actualLocation, 
            bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        /// ///
        /// <exception cref="SdmxNotImplementedException">Throws SdmxNotImplementedException.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "It is OK. We are copying SdmxSource.Java behaviour.")]
        public override IMaintainableMutableObject MutableInstance
        {
            get
            {
                // FUNC 2.1 AttachmentConstraintObjectCore.getMutableInstance
                throw new SdmxNotImplementedException(
                    ExceptionCode.Unsupported, 
                    "AttachmentConstraint.getMutableInstance()");
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsConstraint((IAttachmentConstraintObject)sdmxObject, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IAttachmentConstraintObject" /> .
        /// </returns>
        public override IAttachmentConstraintObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new AttachmentConstraintObjectCore(this, actualLocation, isServiceUrl);
        }

        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS                                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}