// -----------------------------------------------------------------------
// <copyright file="ConstrainedDataKeyCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The constrained data key core.
    /// </summary>
    [Serializable]
    public class ConstrainedDataKeyCore : SdmxStructureCore, IConstrainedDataKey
    {
        /// <summary>
        ///     The _key values.
        /// </summary>
        private readonly IList<IKeyValue> _keyValues;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstrainedDataKeyCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="mutable"/> is <see langword="null" />.</exception>
        public ConstrainedDataKeyCore(IConstrainedDataKeyMutableObject mutable, IConstraintDataKeySet parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKey), parent)
        {
            if (mutable == null)
            {
                throw new ArgumentNullException("mutable");
            }

            this._keyValues = new List<IKeyValue>();

            foreach (IKeyValue each in mutable.KeyValues)
            {
                this._keyValues.Add(new KeyValueImpl(each.Code, each.Concept));
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstrainedDataKeyCore" /> class.
        /// </summary>
        /// <param name="dataKeyType">
        ///     The data key type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="dataKeyType"/> is <see langword="null" />.</exception>
        public ConstrainedDataKeyCore(DataKeyType dataKeyType, IConstraintDataKeySet parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKey), parent)
        {
            if (dataKeyType == null)
            {
                throw new ArgumentNullException("dataKeyType");
            }

            this._keyValues = new List<IKeyValue>();

            foreach (var componentValueSet in dataKeyType.GetTypedKeyValue<DataKeyValueType>())
            {
                string concept = componentValueSet.id;
                if (componentValueSet.Value == null || componentValueSet.Value.Count < 1
                    || componentValueSet.Value.Count > 1)
                {
                    throw new SdmxSemmanticException("KeyValue expected to contain a single value");
                }

                string valueren = componentValueSet.Value[0].TypedValue;
                this._keyValues.Add(new KeyValueImpl(valueren, concept));
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstrainedDataKeyCore" /> class.
        /// </summary>
        /// <param name="dataKeyType">
        ///     The data key type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="dataKeyType"/> is <see langword="null" />.</exception>
        public ConstrainedDataKeyCore(MetadataKeyType dataKeyType, IConstraintDataKeySet parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstrainedDataKey), parent)
        {
            if (dataKeyType == null)
            {
                throw new ArgumentNullException("dataKeyType");
            }

            this._keyValues = new List<IKeyValue>();

            foreach (var componentValueSet in dataKeyType.GetTypedKeyValue<MetadataKeyValueType>())
            {
                string concept = componentValueSet.id;
                if (componentValueSet.Value == null || componentValueSet.Value.Count < 1
                    || componentValueSet.Value.Count > 1)
                {
                    throw new SdmxSemmanticException("KeyValue expected to contain a single value");
                }

                string valueren = componentValueSet.Value[0].TypedValue;
                this._keyValues.Add(new KeyValueImpl(valueren, concept));
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the key values.
        /// </summary>
        public virtual IList<IKeyValue> KeyValues
        {
            get
            {
                return new List<IKeyValue>(this._keyValues);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencySchemeMutable.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IConstrainedDataKey)sdmxObject;
                if (!ObjectUtil.EquivalentCollection(this.KeyValues, that.KeyValues))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" /> is equal to the current
        ///     <see cref="ConstraintDataKeySetCore" />.
        /// </summary>
        /// <returns>
        ///     true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" /> ;
        ///     otherwise, false.
        /// </returns>
        /// <param name="obj">
        ///     The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" /> .
        /// </param>
        /// <filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this))
            {
                return true;
            }

            // Can match another of same type
            var key = obj as IConstrainedDataKey;
            if (key == null)
            {
                return false;
            }

            if (key.KeyValues.Count != this.KeyValues.Count)
            {
                return false;
            }

            foreach (IKeyValue otherKv in key.KeyValues)
            {
                bool found = false;

                foreach (IKeyValue thisKv in this.KeyValues)
                {
                    if (otherKv.Equals(thisKv))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     The get hash code.
        /// </summary>
        /// <returns> The <see cref="int" /> . </returns>
        public override int GetHashCode()
        {
            return this._keyValues != null ? this._keyValues.GetHashCode() : 0;
        }

        /// <summary>
        ///     Gets the key value.
        /// </summary>
        /// <param name="dimensionId">The dimension identifier.</param>
        /// <returns>The key value.</returns>
        public IKeyValue GetKeyValue(string dimensionId)
        {
            return this.KeyValues.FirstOrDefault(kv => kv.Concept.Equals(dimensionId));
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            ISet<string> idSet = new HashSet<string>();
            int count = 0;
            int coundWildCard = 0;

            foreach (IKeyValue kv in this._keyValues)
            {
                if (idSet.Contains(kv.Concept))
                {
                    throw new SdmxSemmanticException(
                        "Constrained data key contains more then one value for dimension id: " + kv.Concept);
                }

                idSet.Add(kv.Concept);
                if (!kv.Code.Equals(ContentConstraintObject.WildcardCode, StringComparison.OrdinalIgnoreCase))
                {
                    count++;
                }
                else
                {
                    coundWildCard++;
                }
            }

            if (count == 1 && coundWildCard == 1)
            {
                throw new SdmxSemmanticException(
                    "Can not define a datakey set with only one code.  Please use Cube Region instead to mark code for inclusion or exclusion");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}