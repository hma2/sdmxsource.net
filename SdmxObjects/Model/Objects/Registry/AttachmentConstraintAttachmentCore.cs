// -----------------------------------------------------------------------
// <copyright file="AttachmentConstraintAttachmentCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The attachment constraint attachment core.
    /// </summary>
    public class AttachmentConstraintAttachmentCore : SdmxStructureCore, IAttachmentConstraintAttachment
    {
        /// <summary>
        ///     The data or metadata set reference.
        /// </summary>
        private readonly IList<IDataAndMetadataSetReference> _dataOrMetadataSetReference;

        /// <summary>
        ///     The data sources.
        /// </summary>
        private readonly IList<IDataSource> _dataSources;

        /// <summary>
        ///     The structure references.
        /// </summary>
        private readonly IList<ICrossReference> _structureReferences;

        /// <summary>
        ///     The target structure.
        /// </summary>
        private SdmxStructureType _targetStructure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttachmentConstraintAttachmentCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public AttachmentConstraintAttachmentCore(ConstraintAttachmentType type, IAttachmentConstraintObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttachmentConstraintAttachment), parent)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this._dataOrMetadataSetReference = new List<IDataAndMetadataSetReference>();
            this._structureReferences = new List<ICrossReference>();
            this._dataSources = new List<IDataSource>();
            if (ObjectUtil.ValidCollection(type.DataSet))
            {
                this._targetStructure = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataset);
                foreach (SetReferenceType setReference in type.DataSet)
                {
                    ICrossReference dataProviderRef = RefUtil.CreateReference(this, setReference.DataProvider);
                    this._dataOrMetadataSetReference.Add(
                        new DataAndMetadataSetReferenceCore(dataProviderRef, setReference.ID, true));
                }
            }

            if (ObjectUtil.ValidCollection(type.MetadataSet))
            {
                this._targetStructure = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataSet);

                foreach (SetReferenceType setReference0 in type.MetadataSet)
                {
                    ICrossReference dataProviderRef1 = RefUtil.CreateReference(this, setReference0.DataProvider);
                    this._dataOrMetadataSetReference.Add(
                        new DataAndMetadataSetReferenceCore(dataProviderRef1, setReference0.ID, false));
                }
            }

            if (ObjectUtil.ValidCollection(type.SimpleDataSource))
            {
                this._targetStructure = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Datasource);

                foreach (Uri dataSource in type.SimpleDataSource)
                {
                    this._dataSources.Add(new DataSourceCore(dataSource.ToString(), this));
                }
            }

            foreach (MaintainableReferenceBaseType xref in type.DataStructure)
            {
                this.AddRef(xref);
            }

            foreach (MaintainableReferenceBaseType ref2 in type.MetadataStructure)
            {
                this.AddRef(ref2);
            }

            foreach (MaintainableReferenceBaseType ref3 in type.Dataflow)
            {
                this.AddRef(ref3);
            }

            foreach (MaintainableReferenceBaseType ref4 in type.Metadataflow)
            {
                this.AddRef(ref4);
            }

            foreach (MaintainableReferenceBaseType ref5 in type.ProvisionAgreement)
            {
                this.AddRef(ref5);
            }
        }

        /// <summary>
        ///     Gets the data or metadata set reference.
        /// </summary>
        public virtual IList<IDataAndMetadataSetReference> DataOrMetadataSetReference
        {
            get
            {
                return new List<IDataAndMetadataSetReference>(this._dataOrMetadataSetReference);
            }
        }

        /// <summary>
        ///     Gets the datasources.
        /// </summary>
        public virtual IList<IDataSource> Datasources
        {
            get
            {
                return new List<IDataSource>(this._dataSources);
            }
        }

        /// <summary>
        ///     Gets the structure references.
        /// </summary>
        public virtual IList<ICrossReference> StructureReferences
        {
            get
            {
                return new List<ICrossReference>(this._structureReferences);
            }
        }

        /// <summary>
        ///     Gets the target structure type.
        /// </summary>
        public virtual SdmxStructureType TargetStructureType
        {
            get
            {
                return this._targetStructure;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IAttachmentConstraintAttachment)sdmxObject;
                if (!ObjectUtil.EquivalentCollection(this._dataOrMetadataSetReference, that.DataOrMetadataSetReference))
                {
                    return false;
                }

                if (!this.Equivalent(this._dataSources, that.Datasources, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._structureReferences, that.StructureReferences))
                {
                    return false;
                }

                if (this._targetStructure != that.TargetStructureType)
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._dataSources, composites);
            return composites;
        }

        /// <summary>
        ///     The add ref.
        /// </summary>
        /// <param name="refType">
        ///     The ref type.
        /// </param>
        private void AddRef(MaintainableReferenceBaseType refType)
        {
            ICrossReference crossRef = RefUtil.CreateReference(this, refType);
            this._targetStructure = crossRef.TargetReference;
            this._structureReferences.Add(crossRef);
        }
    }
}