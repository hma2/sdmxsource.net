// -----------------------------------------------------------------------
// <copyright file="ConstraintObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     The constraint object core.
    /// </summary>
    /// <typeparam name="T"> Generic type param of type IMaintainableObject </typeparam>
    /// <typeparam name="TK"> Generic type param of type IMaintainableMutableObject </typeparam>
    [Serializable]
    public abstract class ConstraintObjectCore<T, TK> : MaintainableObjectCore<T, TK>, IConstraintObject
        where T : IMaintainableObject where TK : IMaintainableMutableObject
    {
        /// <summary>
        ///     The _constraint attachment.
        /// </summary>
        private readonly IConstraintAttachment _constraintAttachment;

        /// <summary>
        ///     The _excluded metadata keys
        /// </summary>
        private readonly IConstraintDataKeySet _excludedMetadataKeys;

        /// <summary>
        ///     The _included metadata keys
        /// </summary>
        private readonly IConstraintDataKeySet _includedMetadataKeys;

        /// <summary>
        ///     The _excluded series keys.
        /// </summary>
        private IConstraintDataKeySet _excludedSeriesKeys;

        /// <summary>
        ///     The _included series keys.
        /// </summary>
        private IConstraintDataKeySet _includedSeriesKeys;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintObjectCore{T, TK}" /> class.
        /// </summary>
        /// <param name="agencyScheme"> The agencySchemeMutable. </param>
        /// <param name="actualLocation"> The actual location. </param>
        /// <param name="isServiceUrl"> The is service url. </param>
        protected ConstraintObjectCore(IMaintainableObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintObjectCore{T,TK}" /> class.
        /// </summary>
        /// <param name="itemMutableObject"> The agencySchemeMutable. </param>
        /// <exception cref="ArgumentNullException"><paramref name="itemMutableObject"/> is <see langword="null" />.</exception>
        protected ConstraintObjectCore(IConstraintMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }

            if (this.MaintainableParent.IsExternalReference.IsTrue)
            {
                return;
            }

            if (itemMutableObject.IncludedSeriesKeys != null
                && itemMutableObject.IncludedSeriesKeys.ConstrainedDataKeys != null
                && itemMutableObject.IncludedSeriesKeys.ConstrainedDataKeys.Count > 0)
            {
                this._includedSeriesKeys = new ConstraintDataKeySetCore(itemMutableObject.IncludedSeriesKeys, this);
            }

            if (itemMutableObject.ExcludedSeriesKeys != null
                && itemMutableObject.ExcludedSeriesKeys.ConstrainedDataKeys != null
                && itemMutableObject.ExcludedSeriesKeys.ConstrainedDataKeys.Count > 0)
            {
                this._excludedSeriesKeys = new ConstraintDataKeySetCore(itemMutableObject.ExcludedSeriesKeys, this);
            }

            if (itemMutableObject.IncludedMetadataKeys != null
                && itemMutableObject.IncludedMetadataKeys.ConstrainedDataKeys != null
                && itemMutableObject.IncludedMetadataKeys.ConstrainedDataKeys.Count > 0)
            {
                this._includedMetadataKeys = new ConstraintDataKeySetCore(itemMutableObject.IncludedSeriesKeys, this);
            }

            if (itemMutableObject.ExcludedMetadataKeys != null
                && itemMutableObject.ExcludedMetadataKeys.ConstrainedDataKeys != null
                && itemMutableObject.ExcludedMetadataKeys.ConstrainedDataKeys.Count > 0)
            {
                this._excludedMetadataKeys = new ConstraintDataKeySetCore(itemMutableObject.ExcludedSeriesKeys, this);
            }

            if (itemMutableObject.ConstraintAttachment != null)
            {
                this._constraintAttachment = new ConstraintAttachmentCore(itemMutableObject.ConstraintAttachment, this);
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintObjectCore{T,TK}" /> class.
        /// </summary>
        /// <param name="createdFrom">The created from.</param>
        /// <param name="structureType">The structure type.</param>
        /// <param name="constraintAttachmentType">Type of the constraint attachment.</param>
        /// <exception cref="SdmxNotImplementedException">Throws Unsupported Exception.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="createdFrom"/> is <see langword="null" />.</exception>
        protected ConstraintObjectCore(
            ConstraintType createdFrom, 
            SdmxStructureType structureType, 
            ConstraintAttachmentType constraintAttachmentType)
            : base(createdFrom, structureType)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            if (ObjectUtil.ValidCollection(createdFrom.DataKeySet))
            {
                var includedKeySet = new DataKeySetType();
                var excludedKeySet = new DataKeySetType();

                this.PopulateKeySets(createdFrom.DataKeySet, includedKeySet, excludedKeySet);

                if (includedKeySet.Key.Count > 0)
                {
                    this._includedSeriesKeys = new ConstraintDataKeySetCore(includedKeySet, this);
                }

                if (excludedKeySet.Key.Count > 0)
                {
                    this._excludedSeriesKeys = new ConstraintDataKeySetCore(excludedKeySet, this);
                }
            }

            if (ObjectUtil.ValidCollection(createdFrom.MetadataKeySet))
            {
                var includedMetadataKeySet = new MetadataKeySetType();
                var excludedMetadataKeySet = new MetadataKeySetType();

                this.PopulateMetadataKeySets(createdFrom.MetadataKeySet, includedMetadataKeySet, excludedMetadataKeySet);

                if (includedMetadataKeySet.Key.Count > 0)
                {
                    this._includedMetadataKeys = new ConstraintDataKeySetCore(includedMetadataKeySet, this);
                }

                if (excludedMetadataKeySet.Key.Count > 0)
                {
                    this._excludedMetadataKeys = new ConstraintDataKeySetCore(excludedMetadataKeySet, this);
                }
            }

            if (constraintAttachmentType != null)
            {
                this._constraintAttachment = new ConstraintAttachmentCore(constraintAttachmentType, this);
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the constraint attachment.
        /// </summary>
        public virtual IConstraintAttachment ConstraintAttachment
        {
            get
            {
                return this._constraintAttachment;
            }
        }

        /// <summary>
        ///     Gets the Metadata keys that this constraint defines as ones that either
        ///     do not have data, or are not allowed to have data (depending on isDefiningActualDataPresent() value)
        /// </summary>
        public IConstraintDataKeySet ExcludedMetadataKeys
        {
            get
            {
                return this._excludedMetadataKeys;
            }
        }

        /// <summary>
        ///     Gets the excluded series keys.
        /// </summary>
        public virtual IConstraintDataKeySet ExcludedSeriesKeys
        {
            get
            {
                return this._excludedSeriesKeys;
            }
        }

        /// <summary>
        ///     Gets the Metadata keys that this constraint defines as ones that either
        ///     have data, or are allowed to have data (depending on isDefiningActualDataPresent() value)
        /// </summary>
        public IConstraintDataKeySet IncludedMetadataKeys
        {
            get
            {
                return this._includedMetadataKeys;
            }
        }

        /// <summary>
        ///     Gets the included series keys.
        /// </summary>
        public virtual IConstraintDataKeySet IncludedSeriesKeys
        {
            get
            {
                return this._includedSeriesKeys;
            }
        }

        /// <summary>
        ///     The deep equals internal.
        /// </summary>
        /// <param name="constraintObject"> The agencySchemeMutable. </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns> The <see cref="bool" /> . </returns>
        protected internal bool DeepEqualsConstraint(IConstraintObject constraintObject, bool includeFinalProperties)
        {
            if (constraintObject == null)
            {
                return false;
            }

            if (constraintObject.StructureType == this.StructureType)
            {
                IConstraintObject that = constraintObject;
                if (!this.Equivalent(this._includedSeriesKeys, that.IncludedSeriesKeys, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._excludedSeriesKeys, that.ExcludedSeriesKeys, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._constraintAttachment, that.ConstraintAttachment, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsMaintainable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._includedSeriesKeys, composites);
            this.AddToCompositeSet(this._excludedSeriesKeys, composites);
            this.AddToCompositeSet(this._includedMetadataKeys, composites);
            this.AddToCompositeSet(this._excludedMetadataKeys, composites);
            this.AddToCompositeSet(this._constraintAttachment, composites);
            return composites;
        }

        /// <summary>
        ///     Populates the key sets.
        /// </summary>
        /// <param name="allKeys">All keys.</param>
        /// <param name="includedKeySet">The included key set.</param>
        /// <param name="excludedKeySet">The excluded key set.</param>
        private void PopulateKeySets(
            IList<DataKeySetType> allKeys, 
            DataKeySetType includedKeySet, 
            DataKeySetType excludedKeySet)
        {
            foreach (DataKeySetType currentDataKeySet in allKeys)
            {
                if (currentDataKeySet.isIncluded)
                {
                    // INCLUDED
                    foreach (var currentKey in currentDataKeySet.Key)
                    {
                        if (!currentKey.include)
                        {
                            // EXCLUDED (isInclude=false)
                            excludedKeySet.Key.Add(currentKey);
                        }
                        else
                        {
                            // INCLUDED
                            includedKeySet.Key.Add(currentKey);
                        }
                    }
                }
                else
                {
                    // EXCLUDED
                    foreach (var currentKey in currentDataKeySet.Key)
                    {
                        if (!currentKey.include)
                        {
                            // INCLUDED (include=false on an already excluded list)
                            includedKeySet.Key.Add(currentKey);
                        }
                        else
                        {
                            // EXCLUDED
                            excludedKeySet.Key.Add(currentKey);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Populates the metadata key sets.
        /// </summary>
        /// <param name="allKeys">All keys.</param>
        /// <param name="includedKeySet">The included key set.</param>
        /// <param name="excludedKeySet">The excluded key set.</param>
        private void PopulateMetadataKeySets(
            IList<MetadataKeySetType> allKeys,
            MetadataKeySetType includedKeySet,
            MetadataKeySetType excludedKeySet)
        {
            foreach (MetadataKeySetType currentDataKeySet in allKeys)
            {
                if (currentDataKeySet.isIncluded)
                {
                    foreach (var currentKey in currentDataKeySet.Key)
                    {
                        if (!currentKey.include)
                        {
                            excludedKeySet.Key.Add(currentKey);
                        }
                        else
                        {
                            includedKeySet.Key.Add(currentKey);
                        }
                    }
                }
                else
                {
                    foreach (var currentKey in currentDataKeySet.Key)
                    {
                        if (!currentKey.include)
                        {
                            excludedKeySet.Key.Add(currentKey);
                        }
                        else
                        {
                            includedKeySet.Key.Add(currentKey);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Validates this instance.
        /// </summary>
        private void Validate()
        {
            if (this._includedSeriesKeys != null && !ObjectUtil.ValidCollection(this._includedSeriesKeys.ConstrainedDataKeys))
            {
                this._includedSeriesKeys = null;
            }

            if (this._excludedSeriesKeys != null && !ObjectUtil.ValidCollection(this._excludedSeriesKeys.ConstrainedDataKeys))
            {
                this._excludedSeriesKeys = null;
            }
        }
    }
}