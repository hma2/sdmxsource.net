// -----------------------------------------------------------------------
// <copyright file="ContentConstraintObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The content constraint object core.
    /// </summary>
    [Serializable]
    public class ContentConstraintObjectCore :
        ConstraintObjectCore<IContentConstraintObject, IContentConstraintMutableObject>, 
        IContentConstraintObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ContentConstraintObjectCore));

        /// <summary>
        ///     The _metadata target region bean
        /// </summary>
        private readonly IMetadataTargetRegion _metadataTargetRegionBean;

        /// <summary>
        ///     The is defining actual data present.
        /// </summary>
        private readonly bool _isIsDefiningActualDataPresent; // Default Value

        /// <summary>
        ///     The ireference period.
        /// </summary>
        private readonly IReferencePeriod _referencePeriod;

        /// <summary>
        ///     The irelease calendar.
        /// </summary>
        private readonly IReleaseCalendar _releaseCalendar;

        /// <summary>
        ///     The excluded cube region.
        /// </summary>
        private ICubeRegion _excludedCubeRegion;

        /// <summary>
        ///     The included cube region.
        /// </summary>
        private ICubeRegion _includedCubeRegion;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContentConstraintObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The sdmxObject.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        public ContentConstraintObjectCore(IContentConstraintObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            this._isIsDefiningActualDataPresent = true;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContentConstraintObjectCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ContentConstraintObjectCore(IContentConstraintMutableObject mutable)
            : base(mutable)
        {
            this._isIsDefiningActualDataPresent = true;
            if (mutable.IncludedCubeRegion != null
                && (ObjectUtil.ValidCollection(mutable.IncludedCubeRegion.KeyValues)
                    || ObjectUtil.ValidCollection(mutable.IncludedCubeRegion.AttributeValues)))
            {
                this._includedCubeRegion = new CubeRegionCore(mutable.IncludedCubeRegion, this);
            }

            if (mutable.ExcludedCubeRegion != null
                && (ObjectUtil.ValidCollection(mutable.ExcludedCubeRegion.KeyValues)
                    || ObjectUtil.ValidCollection(mutable.ExcludedCubeRegion.AttributeValues)))
            {
                this._excludedCubeRegion = new CubeRegionCore(mutable.ExcludedCubeRegion, this);
            }

            if (mutable.ReferencePeriod != null)
            {
                this._referencePeriod = new ReferencePeriodCore(mutable.ReferencePeriod, this);
            }

            if (mutable.ReleaseCalendar != null)
            {
                this._releaseCalendar = new ReleaseCalendarCore(mutable.ReleaseCalendar, this);
            }

            if (mutable.MetadataTargetRegion != null)
            {
                this._metadataTargetRegionBean = new MetadataTargetRegionCore(mutable.MetadataTargetRegion, this);
            }

            this._isIsDefiningActualDataPresent = mutable.IsDefiningActualDataPresent;

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ContentConstraintObjectCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// ///
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ContentConstraintObjectCore(ContentConstraintType type)
            : base(
                type, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint), 
                type.PassNoNull("type").GetConstraintAttachmentType<ContentConstraintAttachmentType>())
        {
            this._isIsDefiningActualDataPresent = true;
            if (type.type != null)
            {
                this._isIsDefiningActualDataPresent = type.type == ContentConstraintTypeCodeTypeConstants.Actual;
            }

            if (ObjectUtil.ValidCollection(type.MetadataKeySet))
            {
                // FUNC 2.1 MetadataKeySet
                throw new SdmxNotImplementedException(
                    ExceptionCode.Unsupported, 
                    "ContentConstraintObjectCore - MetadataKeySet");
            }

            if (ObjectUtil.ValidCollection(type.CubeRegion))
            {
                this.BuildCubeRegions(type.CubeRegion);
            }

            if (ObjectUtil.ValidCollection(type.MetadataTargetRegion))
            {
                // FUNC 2.1 MetadataTarget Region
                throw new SdmxNotImplementedException(
                    ExceptionCode.Unsupported, 
                    "ContentConstraintObjectCore - MetadataTargetRegionList");
            }

            if (type.ReferencePeriod != null)
            {
                this._referencePeriod = new ReferencePeriodCore(type.ReferencePeriod, this);
            }

            if (type.ReleaseCalendar != null)
            {
                this._releaseCalendar = new ReleaseCalendarCore(type.ReleaseCalendar, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the excluded cube region.
        /// </summary>
        public ICubeRegion ExcludedCubeRegion
        {
            get
            {
                return this._excludedCubeRegion;
            }
        }

        /// <summary>
        ///     Gets the included cube region.
        /// </summary>
        public ICubeRegion IncludedCubeRegion
        {
            get
            {
                return this._includedCubeRegion;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether defining actual data present.
        /// </summary>
        public bool IsDefiningActualDataPresent
        {
            get
            {
                return this._isIsDefiningActualDataPresent;
            }
        }

        /// <summary>
        ///     Gets the metadata target region, or null if this is undefined.
        /// </summary>
        public IMetadataTargetRegion MetadataTargetRegion
        {
            get
            {
                return this._metadataTargetRegionBean;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IContentConstraintMutableObject MutableInstance
        {
            get
            {
                return new ContentConstraintMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the reference period.
        /// </summary>
        public virtual IReferencePeriod ReferencePeriod
        {
            get
            {
                return this._referencePeriod;
            }
        }

        /// <summary>
        ///     Gets the release calendar.
        /// </summary>
        public virtual IReleaseCalendar ReleaseCalendar
        {
            get
            {
                return this._releaseCalendar;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IContentConstraintObject)sdmxObject;
                if (!this.Equivalent(this.IncludedCubeRegion, that.IncludedCubeRegion, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this.ExcludedCubeRegion, that.ExcludedCubeRegion, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._referencePeriod, that.ReferencePeriod, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._releaseCalendar, that.ReleaseCalendar, includeFinalProperties))
                {
                    return false;
                }

                if (this._isIsDefiningActualDataPresent != that.IsDefiningActualDataPresent)
                {
                    return false;
                }

                return this.DeepEqualsConstraint(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IContentConstraintObject" /> .
        /// </returns>
        public override IContentConstraintObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new ContentConstraintObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._includedCubeRegion, composites);
            this.AddToCompositeSet(this._excludedCubeRegion, composites);
            this.AddToCompositeSet(this._referencePeriod, composites);
            this.AddToCompositeSet(this._releaseCalendar, composites);
            this.AddToCompositeSet(this._metadataTargetRegionBean, composites);
            return composites;
        }

        /// <summary>
        ///     The build cube regions.
        /// </summary>
        /// <param name="cubeRegionsTypes">
        ///     The cube regions types.
        /// </param>
        private void BuildCubeRegions(IList<CubeRegionType> cubeRegionsTypes)
        {
            foreach (CubeRegionType cubeRegionType in cubeRegionsTypes)
            {
                bool isExcluded = false;
                bool negate = false;
                if (cubeRegionType.include)
                {
                    isExcluded = !cubeRegionType.include;
                }

                var attributeValueSetTypes = cubeRegionType.GetTypedAttribute<AttributeValueSetType>();
                if (attributeValueSetTypes != null)
                {
                    foreach (var currentKey in attributeValueSetTypes)
                    {
                        if (currentKey.include == false)
                        {
                            negate = true;
                            break;
                        }
                    }
                }

                if (!isExcluded)
                {
                    var cubeRegionKeyTypes = cubeRegionType.GetTypedKeyValue<CubeRegionKeyType>();
                    if (cubeRegionKeyTypes != null)
                    {
                        foreach (var currentKey0 in cubeRegionKeyTypes)
                        {
                            if (currentKey0.include == false)
                            {
                                negate = true;
                                break;
                            }
                        }
                    }
                }

                this.StoreCubeRegion(new CubeRegionCore(cubeRegionType, negate, this), isExcluded);
            }
        }

        /// <summary>
        ///     Determines whether the specified cube region contains key.
        /// </summary>
        /// <param name="cubeRegion">The cube region.</param>
        /// <param name="kv">The key value</param>
        /// <returns>true if contains the key.</returns>
        private bool ContainsKey(ICubeRegion cubeRegion, IKeyValue kv)
        {
            if (cubeRegion != null)
            {
                return cubeRegion.GetValues(kv.Concept).Contains(kv.Code);
            }

            return false;
        }

        /// <summary>
        ///     The get key values.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="kvs">
        ///     The kvs.
        /// </param>
        /// <returns>
        ///     The <see cref="IKeyValuesMutable" /> .
        /// </returns>
        private IKeyValuesMutable GetKeyValues(string id, IList<IKeyValuesMutable> kvs)
        {
            foreach (IKeyValuesMutable keyValuesMutable in kvs)
            {
                if (keyValuesMutable.Id.Equals(id))
                {
                    return keyValuesMutable;
                }
            }

            return null;
        }

        /// <summary>
        ///     The merge cube region.
        /// </summary>
        /// <param name="mergeIncluded">
        ///     The merge included.
        /// </param>
        /// <param name="toMerge">
        ///     The to merge.
        /// </param>
        private void MergeCubeRegion(bool mergeIncluded, ICubeRegion toMerge)
        {
            ICubeRegionMutableObject mutable = null;
            if (mergeIncluded)
            {
                mutable = new CubeRegionMutableCore(this._includedCubeRegion);
            }
            else
            {
                mutable = new CubeRegionMutableCore(this._excludedCubeRegion);
            }

            this.MergeKeyValues(mutable.KeyValues, toMerge.KeyValues);
            this.MergeKeyValues(mutable.AttributeValues, toMerge.AttributeValues);

            if (mergeIncluded)
            {
                this._includedCubeRegion = new CubeRegionCore(mutable, this);
            }
            else
            {
                this._excludedCubeRegion = new CubeRegionCore(mutable, this);
            }
        }

        /// <summary>
        ///     The merge key values.
        /// </summary>
        /// <param name="existingKeyValues">
        ///     The existing key values.
        /// </param>
        /// <param name="toMerge">
        ///     The to merge.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private void MergeKeyValues(IList<IKeyValuesMutable> existingKeyValues, IList<IKeyValues> toMerge)
        {
            foreach (IKeyValues currentKeyValues in toMerge)
            {
                IKeyValuesMutable existingMutable = this.GetKeyValues(currentKeyValues.Id, existingKeyValues);

                // Nothing exists yet, add this key values container in
                if (existingMutable == null)
                {
                    existingKeyValues.Add(new KeyValuesMutableImpl(currentKeyValues));
                    continue;
                }

                // We need to merge what we have with what we've got
                if (currentKeyValues.TimeRange != null)
                {
                    if (ObjectUtil.ValidCollection(existingMutable.KeyValues))
                    {
                        throw new SdmxSemmanticException(
                            "Can not create CubeRegion as it is defining both a TimeRange and a Set of allowed values for a Key Value with the same Id");
                    }

                    throw new SdmxSemmanticException(
                        "Can not create CubeRegion as it is a TimeRange twice for a Key Value with the same Id");
                }

                foreach (string currentValue in currentKeyValues.Values)
                {
                    if (existingMutable.KeyValues.Contains(currentValue))
                    {
                        if (existingMutable.IsCascadeValue(currentValue)
                            != currentKeyValues.IsCascadeValue(currentValue))
                        {
                            throw new SdmxSemmanticException(
                                "Can not create CubeRegion as it defines a Key/Value '" + currentKeyValues.Id
                                + "'/'+currentValue)+' twice, once with cascade values set to true, and once false");
                        }

                        LOG.Warn("Duplicate definition of KeyValue in 2 different Cube Regions");
                    }
                    else
                    {
                        existingMutable.AddValue(currentValue);
                    }

                    if (currentKeyValues.IsCascadeValue(currentValue))
                    {
                        existingMutable.AddCascade(currentValue);
                    }
                }
            }
        }

        /// <summary>
        ///     Takes a cube region, which is either a inclusive or exclusive, and if there is already the inclusive or exclusive
        ///     cube region
        ///     stored, then this one will merge into the stored one.  If there are any duplicate values, then an error will be
        ///     thrown.
        /// </summary>
        /// <param name="currentCubeRegion">
        ///     The current Cube Region.
        /// </param>
        /// <param name="isExcluded">
        ///     The is Excluded.
        /// </param>
        private void StoreCubeRegion(ICubeRegion currentCubeRegion, bool isExcluded)
        {
            if (!isExcluded)
            {
                if (this._includedCubeRegion == null)
                {
                    this._includedCubeRegion = currentCubeRegion;
                }
                else
                {
                    this.MergeCubeRegion(true, currentCubeRegion);
                }
            }
            else
            {
                if (this._excludedCubeRegion == null)
                {
                    this._excludedCubeRegion = currentCubeRegion;
                }
                else
                {
                    this.MergeCubeRegion(false, currentCubeRegion);
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     The validate.
        /// </summary>
        private void Validate()
        {
            this.ValidateMaintainableAttributes();

            IDictionary<string, ISet<string>> includedCodesForKey = new Dictionary<string, ISet<string>>();
            ISet<string> wildcardedConcepts = new HashSet<string>();

            if (this.IncludedSeriesKeys != null)
            {
                foreach (IConstrainedDataKey cdkb in this.IncludedSeriesKeys.ConstrainedDataKeys)
                {
                    foreach (IKeyValue kv in cdkb.KeyValues)
                    {
                        if (this.ContainsKey(this._excludedCubeRegion, kv))
                        {
                            throw new SdmxSemmanticException(
                                "Constraint is in conflict with itself.  Included series key contains component '"
                                + kv.Concept + "' with value '" + kv.Code
                                + "'.  This code has also been specified as excluded by the constraint's CubeRegion");
                        }

                        if (kv.Code.Equals(ContentConstraintObject.WildcardCode))
                        {
                            wildcardedConcepts.Add(kv.Concept);
                            includedCodesForKey.Remove(kv.Concept);
                        }
                        else if (!wildcardedConcepts.Contains(kv.Concept))
                        {
                            ISet<string> includedCodes;
                            if (!includedCodesForKey.TryGetValue(kv.Concept, out includedCodes))
                            {
                                includedCodes = new HashSet<string>();
                                includedCodesForKey.Add(kv.Concept, includedCodes);
                            }

                            includedCodes.Add(kv.Code);
                        }
                    }
                }
            }

            if (this.IncludedCubeRegion != null)
            {
                // Check we are not including any more / less then the series includes
                foreach (IKeyValues kvs in this.IncludedCubeRegion.KeyValues)
                {
                    ISet<string> allIncludes;
                    if (includedCodesForKey.TryGetValue(kvs.Id, out allIncludes))
                    {
                        if (!allIncludes.ContainsAll(kvs.Values) || !kvs.Values.ContainsAll(allIncludes))
                        {
                            throw new SdmxSemmanticException(
                                "Constraint is in conflict with itself. The constraint defines valid series, this can not be further restricted by the cube region.  The "
                                + "Cube Region has further restricted dimension '" + kvs.Id
                                + "' by not including all the codes defined by the keyset.");
                        }
                    }

                    if (this.ExcludedCubeRegion != null)
                    {
                        this.ValidateNoKeyValuesDuplicates(kvs, this.ExcludedCubeRegion.KeyValues);
                    }
                }
            }

            if (this.ExcludedCubeRegion != null)
            {
                foreach (IKeyValues kvs in this.ExcludedCubeRegion.KeyValues)
                {
                    ISet<string> allIncludes;
                    if (includedCodesForKey.TryGetValue(kvs.Id, out allIncludes))
                    {
                        throw new SdmxSemmanticException(
                            "Constraint is in conflict with itself. The constraint defines valid series, the dimension  '"
                            + kvs.Id + "' can not be further restriced by the cube region to "
                            + "exclude codes which are already marked for inclusion by the keyset");
                    }

                    if (this.IncludedCubeRegion != null)
                    {
                        this.ValidateNoKeyValuesDuplicates(kvs, this.IncludedCubeRegion.KeyValues);
                    }
                }
            }
        }

        /// <summary>
        ///     The validate no key values duplicates.
        /// </summary>
        /// <param name="kvs">
        ///     The kvs.
        /// </param>
        /// <param name="kvsList">
        ///     The kvs list.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private void ValidateNoKeyValuesDuplicates(IKeyValues kvs, IList<IKeyValues> kvsList)
        {
            foreach (IKeyValues currentKvs in kvsList)
            {
                if (currentKvs.Id.Equals(kvs.Id))
                {
                    foreach (string currentValue in currentKvs.Values)
                    {
                        if (kvs.Values.Contains(currentValue))
                        {
                            throw new SdmxSemmanticException(
                                "CubeRegion contains a Key Value that is both included and excluded Id='" + kvs.Id
                                + "' Value='" + currentValue + "'");
                        }
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS                                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}