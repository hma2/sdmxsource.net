// -----------------------------------------------------------------------
// <copyright file="KeyValuesCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The key values impl.
    /// </summary>
    [Serializable]
    public class KeyValuesCore : SdmxStructureCore, IKeyValues
    {
        /// <summary>
        ///     The case cade list.
        /// </summary>
        private readonly IList<string> _caseCadeList;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly string _id;

        /// <summary>
        ///     The itime range.
        /// </summary>
        private readonly ITimeRange _timeRange;

        /// <summary>
        ///     The values.
        /// </summary>
        private readonly List<string> _values;

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValuesCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public KeyValuesCore(IKeyValuesMutable mutable, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.KeyValues), parent)
        {
            if (mutable == null)
            {
                throw new ArgumentNullException("mutable");
            }

            this._values = new List<string>();
            this._caseCadeList = new List<string>();
            this._id = mutable.Id;
            this._values.AddRange(mutable.KeyValues);

            foreach (string value in this._values)
            {
                if (mutable.IsCascadeValue(value))
                {
                    this._caseCadeList.Add(value);
                }
            }

            if (mutable.TimeRange != null)
            {
                this._timeRange = new TimeRangeCore(mutable.TimeRange, this);
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="KeyValuesCore" /> class.
        /// </summary>
        /// <param name="keyValueType">
        ///     The key value type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="keyValueType"/> is <see langword="null" />.</exception>
        public KeyValuesCore(ComponentValueSetType keyValueType, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.KeyValues), parent)
        {
            if (keyValueType == null)
            {
                throw new ArgumentNullException("keyValueType");
            }

            this._values = new List<string>();
            this._caseCadeList = new List<string>();

            this._id = keyValueType.id;

            if (keyValueType.Value != null)
            {
                foreach (SimpleValueType dataKeyType in keyValueType.Value)
                {
                    this._values.Add(dataKeyType.TypedValue);
                    if (dataKeyType.cascadeValues)
                    {
                        this._caseCadeList.Add(dataKeyType.TypedValue);
                    }
                }
            }

            if (keyValueType.TimeRange != null)
            {
                this._timeRange = new TimeRangeCore(keyValueType.TimeRange, this);
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets cascade values.
        /// </summary>
        /// <value>
        ///     Returns a list of all the values which are cascade values
        /// </value>
        public virtual IList<string> CascadeValues
        {
            get
            {
                return new List<string>(this._caseCadeList);
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return this._id;
            }
        }

        /// <summary>
        ///     Gets the time range.
        /// </summary>
        public virtual ITimeRange TimeRange
        {
            get
            {
                return this._timeRange;
            }
        }

        /// <summary>
        ///     Gets the values.
        /// </summary>
        public virtual IList<string> Values
        {
            get
            {
                return new List<string>(this._values);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IKeyValues)sdmxObject;
                if (!ObjectUtil.EquivalentCollection(this._values, that.Values))
                {
                    return false;
                }

                foreach (string currentValue in this._values)
                {
                    if (that.IsCascadeValue(currentValue) != this.IsCascadeValue(currentValue))
                    {
                        return false;
                    }
                }

                if (!ObjectUtil.Equivalent(this._id, that.Id))
                {
                    return false;
                }

                if (!this.Equivalent(this._timeRange, that.TimeRange, includeFinalProperties))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The is cascade value.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public virtual bool IsCascadeValue(string value)
        {
            return this._caseCadeList.Contains(value);
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._timeRange, composites);
            return composites;
        }

        /// <summary>
        ///     Validates this instance.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">KeyValues requires an id</exception>
        private void Validate()
        {
            if (!ObjectUtil.ValidString(this._id))
            {
                throw new SdmxSemmanticException("KeyValues requires an id");
            }
        }
    }
}