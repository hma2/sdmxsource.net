// -----------------------------------------------------------------------
// <copyright file="ConstraintAttachmentCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The constraint attachment core.
    /// </summary>
    [Serializable]
    public class ConstraintAttachmentCore : SdmxStructureCore, IConstraintAttachment
    {
        /// <summary>
        ///     The cross reference.
        /// </summary>
        private readonly ISet<ICrossReference> _crossReference;

        /// <summary>
        ///     The data or metadata set reference.
        /// </summary>
        private readonly IDataAndMetadataSetReference _dataOrMetadataSetReference;

        /// <summary>
        ///     The data sources.
        /// </summary>
        private readonly IList<IDataSource> _dataSources;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintAttachmentCore" /> class.
        /// </summary>
        /// <param name="mutable">
        ///     The mutable.
        /// </param>
        /// <param name="constraint">
        ///     The constraint.
        /// </param>
        public ConstraintAttachmentCore(IConstraintAttachmentMutableObject mutable, IConstraintObject constraint)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraintAttachment), constraint)
        {
            if (mutable == null)
            {
                throw new ArgumentNullException("mutable");
            }

            this._crossReference = new HashSet<ICrossReference>();
            this._dataSources = new List<IDataSource>();
            if (mutable.DataOrMetadataSetReference != null)
            {
                this._dataOrMetadataSetReference = new DataAndMetadataSetReferenceCore(
                    mutable.DataOrMetadataSetReference);
            }

            if (mutable.StructureReference != null)
            {
                foreach (IStructureReference structureReference in mutable.StructureReference)
                {
                    this._crossReference.Add(new CrossReferenceImpl(this, structureReference));
                }
            }

            if (ObjectUtil.ValidCollection(mutable.DataSources))
            {
                foreach (IDataSourceMutableObject each in mutable.DataSources)
                {
                    this._dataSources.Add(new DataSourceCore(each, this));
                }
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConstraintAttachmentCore" /> class.
        /// </summary>
        /// <param name="type">
        ///     The type.
        /// </param>
        /// <param name="constraint">
        ///     The constraint.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="type"/> is <see langword="null" />.</exception>
        public ConstraintAttachmentCore(ConstraintAttachmentType type, IConstraintObject constraint)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraintAttachment), constraint)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            this._crossReference = new HashSet<ICrossReference>();
            this._dataSources = new List<IDataSource>();
            if (type.SimpleDataSource != null)
            {
                foreach (Uri dataSource in type.SimpleDataSource)
                {
                    this._dataSources.Add(new DataSourceCore(dataSource.ToString(), this));
                }
            }

            if (type.DataProvider != null)
            {
                this._crossReference.Add(RefUtil.CreateReference(this, type.DataProvider));
            }

            if (ObjectUtil.ValidCollection(type.DataSet))
            {
                // THERE IS ONLY ONE DATA SET
                SetReferenceType setRef = type.DataSet[0];
                ICrossReference xref = RefUtil.CreateReference(this, setRef.DataProvider);
                this._dataOrMetadataSetReference = new DataAndMetadataSetReferenceCore(xref, setRef.ID, true);
            }

            if (ObjectUtil.ValidCollection(type.MetadataSet))
            {
                // THERE IS ONLY ONE METADATA SET
                SetReferenceType setRef0 = type.MetadataSet[0];
                ICrossReference ref1 = RefUtil.CreateReference(this, setRef0.DataProvider);
                this._dataOrMetadataSetReference = new DataAndMetadataSetReferenceCore(ref1, setRef0.ID, false);
            }

            this.AddRef(type.DataStructure);
            this.AddRef(type.MetadataStructure);
            this.AddRef(type.Dataflow);
            this.AddRef(type.Metadataflow);
            this.AddRef(type.ProvisionAgreement);

            foreach (QueryableDataSourceType queryableDataSource in type.QueryableDataSource)
            {
                this._dataSources.Add(new DataSourceCore(queryableDataSource, this));
            }

            this.Validate();
        }

        /// <summary>
        ///     Gets the data or metadata set reference.
        /// </summary>
        public virtual IDataAndMetadataSetReference DataOrMetadataSetReference
        {
            get
            {
                return this._dataOrMetadataSetReference;
            }
        }

        /// <summary>
        ///     Gets the data sources.
        /// </summary>
        public virtual IList<IDataSource> DataSources
        {
            get
            {
                return new List<IDataSource>(this._dataSources);
            }
        }

        /// <summary>
        ///     Gets the structure reference.
        /// </summary>
        public virtual ISet<ICrossReference> StructureReference
        {
            get
            {
                return this._crossReference;
            }
        }

        /// <summary>
        ///     The create mutable instance.
        /// </summary>
        /// <returns> The <see cref="IConstraintAttachmentMutableObject" /> . </returns>
        public virtual IConstraintAttachmentMutableObject CreateMutableInstance()
        {
            return new ContentConstraintAttachmentMutableCore(this);
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IConstraintAttachment)sdmxObject;
                if (!ObjectUtil.Equivalent(this._dataOrMetadataSetReference, that.DataOrMetadataSetReference))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._crossReference, that.StructureReference))
                {
                    return false;
                }

                if (!this.Equivalent(this._dataSources, that.DataSources, includeFinalProperties))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._dataSources, composites);
            return composites;
        }

        /// <summary>
        ///     The add ref.
        /// </summary>
        /// <param name="refListType">
        ///     The ref list type.
        /// </param>
        /// <typeparam name="T">
        ///     Generic type param of type MaintainableReferenceBaseType
        /// </typeparam>
        private void AddRef<T>(IList<T> refListType) where T : MaintainableReferenceBaseType
        {
            if (ObjectUtil.ValidCollection(refListType))
            {
                foreach (T xref in refListType)
                {
                    ICrossReference crossRef = RefUtil.CreateReference(this, xref);
                    this._crossReference.Add(crossRef);
                }
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            SdmxStructureType constrainingType = default(SdmxStructureType) /* was: null */;

            // Checking that there is at least something in this Attachment
            if (this._dataOrMetadataSetReference == null && !ObjectUtil.ValidCollection(this._crossReference)
                && !ObjectUtil.ValidCollection(this._dataSources))
            {
                throw new SdmxSemmanticException("The ContentConstraint doesn't have a Constraint Attachment defined");
            }

            foreach (ICrossReference xsRef in this._crossReference)
            {
                if (constrainingType == null)
                {
                    constrainingType = xsRef.TargetReference;
                }
                else
                {
                    switch (xsRef.TargetReference.EnumType)
                    {
                        case SdmxStructureEnumType.Dsd:
                            if (constrainingType.EnumType != SdmxStructureEnumType.Dataflow
                                && constrainingType.EnumType != SdmxStructureEnumType.ProvisionAgreement)
                            {
                                constrainingType = xsRef.TargetReference;
                            }

                            break;
                        case SdmxStructureEnumType.Dataflow:
                            if (constrainingType.EnumType != SdmxStructureEnumType.ProvisionAgreement)
                            {
                                constrainingType = xsRef.TargetReference;
                            }

                            break;
                        case SdmxStructureEnumType.ProvisionAgreement:
                            constrainingType = xsRef.TargetReference;
                            break;
                    }

                    // if(constrainingType != xsRef.getTargetReference()) {
                    // throw new SdmxSemmanticException("ContentConstraint's ConstraintAttachment may only reference structures of the same type, got '"+constrainingType+"' and '"+xsRef.getTargetReference()+"'");
                    // }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        // FUNC Validation of ALL Constraints and This CrossRefence/DataSource Pair should be grouped

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////
    }
}