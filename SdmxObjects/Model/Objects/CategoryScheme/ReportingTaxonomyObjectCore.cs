// -----------------------------------------------------------------------
// <copyright file="ReportingTaxonomyObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme
{
    using System;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    using CategoryType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CategoryType;

    /// <summary>
    ///     The reporting taxonomy object core.
    /// </summary>
    [Serializable]
    public class ReportingTaxonomyObjectCore :
        ItemSchemeObjectCore<IReportingCategoryObject, IReportingTaxonomyObject, IReportingTaxonomyMutableObject, 
            IReportingCategoryMutableObject>, 
        IReportingTaxonomyObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ReportingTaxonomyObjectCore));

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingTaxonomyObjectCore" /> class.
        /// </summary>
        /// <param name="reportingTaxonomy">
        ///     The reporting taxonomy.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportingTaxonomyObjectCore(IReportingTaxonomyMutableObject reportingTaxonomy)
            : base(reportingTaxonomy)
        {
            LOG.Debug("Building IReportingTaxonomyObject from Mutable Object");
            try
            {
                if (reportingTaxonomy.Items != null)
                {
                    foreach (IReportingCategoryMutableObject currentcategory in reportingTaxonomy.Items)
                    {
                        this.AddInternalItem(new ReportingCategoryCore(this, currentcategory));
                    }
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (LOG.IsDebugEnabled)
            {
                LOG.Debug("IReportingTaxonomyObject Built " + this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingTaxonomyObjectCore" /> class.
        /// </summary>
        /// <param name="reportingTaxonomy">
        ///     The reporting taxonomy.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportingTaxonomyObjectCore(ReportingTaxonomyType reportingTaxonomy)
            : base(reportingTaxonomy, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingTaxonomy))
        {
            LOG.Debug("Building IReportingTaxonomyObject from 2.1 SDMX");
            try
            {
                if (reportingTaxonomy.Item != null)
                {
                    foreach (ReportingCategory currentcategory in reportingTaxonomy.Item)
                    {
                        this.AddInternalItem(new ReportingCategoryCore(this, currentcategory.Content));
                    }
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (LOG.IsDebugEnabled)
            {
                LOG.Debug("IReportingTaxonomyObject Built " + this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingTaxonomyObjectCore" /> class.
        /// </summary>
        /// <param name="reportingTaxonomy">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportingTaxonomyObjectCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ReportingTaxonomyType reportingTaxonomy)
            : base(
                reportingTaxonomy, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingTaxonomy), 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").validTo, 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").validFrom, 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").version, 
                CreateTertiary(reportingTaxonomy.PassNoNull("reportingTaxonomy").isFinal), 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").agencyID, 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").id, 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").uri, 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").Name, 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").Description, 
                CreateTertiary(reportingTaxonomy.PassNoNull("reportingTaxonomy").isExternalReference), 
                reportingTaxonomy.PassNoNull("reportingTaxonomy").Annotations)
        {
            LOG.Debug("Building IReportingTaxonomyObject from 2.0 SDMX");
            try
            {
                if (reportingTaxonomy.Category != null)
                {
                    foreach (CategoryType currentcategory in reportingTaxonomy.Category)
                    {
                        this.AddInternalItem(new ReportingCategoryCore(this, currentcategory));
                    }
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (LOG.IsDebugEnabled)
            {
                LOG.Debug("IReportingTaxonomyObject Built " + this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingTaxonomyObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private ReportingTaxonomyObjectCore(
            IReportingTaxonomyObject agencyScheme, 
            Uri actualLocation, 
            bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            LOG.Debug("Stub IReportingTaxonomyObject Built");
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IReportingTaxonomyMutableObject MutableInstance
        {
            get
            {
                return new ReportingTaxonomyMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                return this.DeepEqualsInternal((IReportingTaxonomyObject)sdmxObject, includeFinalProperties);
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS                                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IReportingTaxonomyObject" /> .
        /// </returns>
        public override IReportingTaxonomyObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new ReportingTaxonomyObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        private void Validate()
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    
        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP VALIDATION                         //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}