// -----------------------------------------------------------------------
// <copyright file="ReportingCategoryCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using CategoryType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CategoryType;
    using DataflowRefType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.DataflowRefType;
    using MetadataflowRefType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataflowRefType;
    using ReportingCategory = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.ReportingCategory;

    /// <summary>
    ///     The reporting category core.
    /// </summary>
    [Serializable]
    public class ReportingCategoryCore : ItemCore, IReportingCategoryObject
    {
        /// <summary>
        ///     The provisioning metadata.
        /// </summary>
        private readonly IList<ICrossReference> _provisioningMetadata;

        /// <summary>
        ///     The reporting categories.
        /// </summary>
        private readonly IList<IReportingCategoryObject> _reportingCategories;

        /// <summary>
        ///     The structural metadata.
        /// </summary>
        private readonly IList<ICrossReference> _structuralMetadata;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingCategoryCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="reportingCategory">
        ///     The reporting category.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportingCategoryCore(IIdentifiableObject parent, IReportingCategoryMutableObject reportingCategory)
            : base(reportingCategory, parent)
        {
            this._structuralMetadata = new List<ICrossReference>();
            this._provisioningMetadata = new List<ICrossReference>();
            this._reportingCategories = new List<IReportingCategoryObject>();

            if (reportingCategory.ProvisioningMetadata != null)
            {
                foreach (IStructureReference structureReference in reportingCategory.ProvisioningMetadata)
                {
                    this._provisioningMetadata.Add(new CrossReferenceImpl(this, structureReference));
                }
            }

            if (reportingCategory.StructuralMetadata != null)
            {
                foreach (IStructureReference structureReference in reportingCategory.StructuralMetadata)
                {
                    this._structuralMetadata.Add(new CrossReferenceImpl(this, structureReference));
                }
            }

            if (reportingCategory.Items != null)
            {
                foreach (IReportingCategoryMutableObject reportingCategoryMutableObject in reportingCategory.Items)
                {
                    this._reportingCategories.Add(new ReportingCategoryCore(this, reportingCategoryMutableObject));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingCategoryCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="category">
        ///     The category.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportingCategoryCore(IIdentifiableObject parent, ReportingCategoryType category)
            : base(category, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingCategory), parent)
        {
            this._structuralMetadata = new List<ICrossReference>();
            this._provisioningMetadata = new List<ICrossReference>();
            this._reportingCategories = new List<IReportingCategoryObject>();

            if (category.ProvisioningMetadata != null)
            {
                foreach (StructureUsageReferenceType structureUsageReferenceType in category.ProvisioningMetadata)
                {
                    this._provisioningMetadata.Add(RefUtil.CreateReference(this, structureUsageReferenceType));
                }
            }

            if (category.StructuralMetadata != null)
            {
                foreach (StructureReferenceType structureReferenceType in category.StructuralMetadata)
                {
                    this._structuralMetadata.Add(RefUtil.CreateReference(this, structureReferenceType));
                }
            }

            if (category.Item != null)
            {
                foreach (ReportingCategory childCategory in category.Item)
                {
                    this._reportingCategories.Add(new ReportingCategoryCore(this, childCategory.Content));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportingCategoryCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="category">
        ///     The category.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportingCategoryCore(IIdentifiableObject parent, CategoryType category)
            : base(
                category, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingCategory), 
                category.PassNoNull("category").id, 
                category.PassNoNull("category").uri, 
                category.PassNoNull("category").Name, 
                category.PassNoNull("category").Description, 
                category.PassNoNull("category").Annotations, 
                parent)
        {
            this._structuralMetadata = new List<ICrossReference>();
            this._provisioningMetadata = new List<ICrossReference>();
            this._reportingCategories = new List<IReportingCategoryObject>();

            if (category.DataflowRef != null)
            {
                foreach (DataflowRefType dataflowRefType in category.DataflowRef)
                {
                    if (dataflowRefType.URN != null)
                    {
                        this._provisioningMetadata.Add(new CrossReferenceImpl(this, dataflowRefType.URN));
                    }
                    else
                    {
                        this._provisioningMetadata.Add(
                            new CrossReferenceImpl(
                                this, 
                                dataflowRefType.AgencyID, 
                                dataflowRefType.DataflowID, 
                                dataflowRefType.Version, 
                                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)));
                    }
                }
            }

            if (category.MetadataflowRef != null)
            {
                foreach (MetadataflowRefType mdfRef in category.MetadataflowRef)
                {
                    if (mdfRef.URN != null)
                    {
                        this._provisioningMetadata.Add(new CrossReferenceImpl(this, mdfRef.URN));
                    }
                    else
                    {
                        this._provisioningMetadata.Add(
                            new CrossReferenceImpl(
                                this, 
                                mdfRef.AgencyID, 
                                mdfRef.MetadataflowID, 
                                mdfRef.Version, 
                                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow)));
                    }
                }
            }

            if (category.Category != null)
            {
                foreach (CategoryType childCategory in category.Category)
                {
                    this._reportingCategories.Add(new ReportingCategoryCore(this, childCategory));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        /// <summary>
        ///     Gets the items.
        /// </summary>
        public virtual IList<IReportingCategoryObject> Items
        {
            get
            {
                return new List<IReportingCategoryObject>(this._reportingCategories);
            }
        }

        /// <summary>
        ///     Gets the provisioning metadata.
        /// </summary>
        public virtual IList<ICrossReference> ProvisioningMetadata
        {
            get
            {
                return new List<ICrossReference>(this._provisioningMetadata);
            }
        }

        /// <summary>
        ///     Gets the structural metadata.
        /// </summary>
        public virtual IList<ICrossReference> StructuralMetadata
        {
            get
            {
                return new List<ICrossReference>(this._structuralMetadata);
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IReportingCategoryObject)sdmxObject;
                if (!ObjectUtil.Equivalent(this._structuralMetadata, that.StructuralMetadata))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._provisioningMetadata, that.ProvisioningMetadata))
                {
                    return false;
                }

                if (!this.Equivalent(this._reportingCategories, that.Items, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._reportingCategories, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (ObjectUtil.ValidCollection(this._provisioningMetadata)
                && ObjectUtil.ValidCollection(this._structuralMetadata))
            {
                throw new SdmxSemmanticException(
                    "Reporting Category can not have both structural metadata and provisioning metadata");
            }

            // Validate StructuralMetadata is either pointing to a DSD or MSD, and only contains the same type
            foreach (ICrossReference crossReference in this._structuralMetadata)
            {
                if (crossReference.TargetReference.EnumType != SdmxStructureEnumType.Dsd
                    && crossReference.TargetReference.EnumType != SdmxStructureEnumType.Dsd)
                {
                    throw new SdmxSemmanticException(
                        "Reporting Category 'Structural Metadata' must either reference DSDs or MSDs, not "
                        + crossReference.TargetReference.GetType());
                }
            }

            foreach (ICrossReference crossReference in this._provisioningMetadata)
            {
                if (crossReference.TargetReference.EnumType != SdmxStructureEnumType.Dataflow
                    && crossReference.TargetReference.EnumType != SdmxStructureEnumType.MetadataFlow)
                {
                    throw new SdmxSemmanticException(
                        "Reporting Category 'Provisioning Metadata' must either reference a Data Flow or Metadata Flow, not "
                        + crossReference.TargetReference.GetType());
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}