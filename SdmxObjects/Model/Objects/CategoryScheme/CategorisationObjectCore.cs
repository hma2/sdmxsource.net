// -----------------------------------------------------------------------
// <copyright file="CategorisationObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The categorisation object core.
    /// </summary>
    [Serializable]
    public class CategorisationObjectCore : MaintainableObjectCore<ICategorisationObject, ICategorisationMutableObject>, 
                                            ICategorisationObject
    {
        /// <summary>
        ///     The _sdmx structure.
        /// </summary>
        private static readonly SdmxStructureType _sdmxStructure =
            SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation);

        /// <summary>
        ///     The category reference.
        /// </summary>
        private readonly ICrossReference _categoryReference;

        /// <summary>
        ///     The structure reference.
        /// </summary>
        private readonly ICrossReference _structureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationObjectCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public CategorisationObjectCore(ICategorisationMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            try
            {
                if (itemMutableObject.CategoryReference != null)
                {
                    this._categoryReference = new CrossReferenceImpl(this, itemMutableObject.CategoryReference);
                }

                if (itemMutableObject.StructureReference != null)
                {
                    this._structureReference = new CrossReferenceImpl(this, itemMutableObject.StructureReference);
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex1)
            {
                throw new SdmxSemmanticException(ex1, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th1)
            {
                throw new SdmxException(th1, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationObjectCore" /> class.
        /// </summary>
        /// <param name="cs">
        ///     The cs.
        /// </param>
        public CategorisationObjectCore(CategorisationType cs)
            : base(cs, _sdmxStructure)
        {
            this._structureReference = RefUtil.CreateReference(this, cs.Source);
            this._categoryReference = RefUtil.CreateReference(this, cs.Target);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationObjectCore" /> class.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="mdf">The MDF.</param>
        /// <exception cref="SdmxSemmanticException">
        ///     SDMX Semantic exception
        /// </exception>
        /// <exception cref="SdmxException">
        ///     SDMX exception
        /// </exception>
        public CategorisationObjectCore(ICategoryObject category, MetadataflowRefType mdf)
            : base(
                _sdmxStructure, 
                null, 
                null, 
                null, 
                null, 
                category.PassNoNull("category").MaintainableParent.AgencyId, 
                null, 
                null, 
                null, 
                null, 
                null, 
                null)
        {
            if (category == null)
            {
                throw new ArgumentNullException("category");
            }

            if (mdf == null)
            {
                throw new ArgumentNullException("mdf");
            }

            try
            {
                this.AddNames(category.Names);
                if (mdf.URN != null)
                {
                    this._structureReference = new CrossReferenceImpl(this, mdf.URN);
                }
                else
                {
                    this._structureReference = new CrossReferenceImpl(
                        this, 
                        mdf.AgencyID, 
                        mdf.MetadataflowID, 
                        mdf.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow));
                }

                this._categoryReference = new CrossReferenceImpl(this, category.Urn);
                this.GenerateId();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationObjectCore" /> class.
        ///     Constructs a categorisation from a category that contains a dataflow ref
        /// </summary>
        /// <param name="category">
        ///     Category object
        /// </param>
        /// <param name="df">
        ///     DataFlow object
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="category"/> is <see langword="null" />.</exception>
        public CategorisationObjectCore(ICategoryObject category, DataflowRefType df)
            : base(
                _sdmxStructure, 
                null, 
                null, 
                null, 
                null, 
                category.PassNoNull("category").MaintainableParent.AgencyId, 
                null, 
                null, 
                null, 
                null, 
                null, 
                null)
        {
            if (category == null)
            {
                throw new ArgumentNullException("category");
            }

            if (df == null)
            {
                throw new ArgumentNullException("df");
            }

            try
            {
                this.AddNames(category.Names);
                if (df.URN != null)
                {
                    this._structureReference = new CrossReferenceImpl(this, df.URN);
                }
                else
                {
                    this._structureReference = new CrossReferenceImpl(
                        this, 
                        df.AgencyID, 
                        df.DataflowID, 
                        df.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow));
                }

                this._categoryReference = new CrossReferenceImpl(this, category.Urn);
                this.GenerateId();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationObjectCore" /> class.
        ///     Constructs a cateogrisation from a dataflow that contains a category ref
        /// </summary>
        /// <param name="referencedFrom">
        ///     Maintainable object
        /// </param>
        /// <param name="currentRef">
        ///     The current Ref.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="referencedFrom"/> is <see langword="null" />.</exception>
        public CategorisationObjectCore(IMaintainableObject referencedFrom, CategoryRefType currentRef)
            : base(
                _sdmxStructure, 
                null, 
                null, 
                null, 
                null, 
                referencedFrom.PassNoNull("referencedFrom").AgencyId, 
                null, 
                null, 
                null, 
                null, 
                null, 
                null)
        {
            if (referencedFrom == null)
            {
                throw new ArgumentNullException("referencedFrom");
            }

            try
            {
                this.AddNames(referencedFrom.Names);
                this._structureReference = new CrossReferenceImpl(this, referencedFrom.Urn);
                this._categoryReference = RefUtil.CreateCategoryRef(this, currentRef);
                this.GenerateId();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategorisationObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private CategorisationObjectCore(ICategorisationObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
        }

        /// <summary>
        ///     Gets the category reference.
        /// </summary>
        public virtual ICrossReference CategoryReference
        {
            get
            {
                return this._categoryReference;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override ICategorisationMutableObject MutableInstance
        {
            get
            {
                return new CategorisationMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the structure reference.
        /// </summary>
        public virtual ICrossReference StructureReference
        {
            get
            {
                return this._structureReference;
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ICategorisationObject)sdmxObject;
                if (!this.Equivalent(this._categoryReference, that.CategoryReference))
                {
                    return false;
                }

                if (!this.Equivalent(this._structureReference, that.StructureReference))
                {
                    return false;
                }

                return this.DeepEqualsMaintainable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="ICategorisationObject" /> .
        /// </returns>
        public override ICategorisationObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new CategorisationObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     The validate id.
        /// </summary>
        /// <param name="startWithIntAllowed">
        ///     The start with int allowed.
        /// </param>
        protected internal override void ValidateId(bool startWithIntAllowed)
        {
            // Do nothing yet, not yet fully built
        }

        /// <summary>
        ///     The validate nameable attributes.
        /// </summary>
        protected override void ValidateNameableAttributes()
        {
            // Do nothing yet, not yet fully built
        }

        /// <summary>
        ///     The generate id.
        /// </summary>
        private void GenerateId()
        {
            this.OriginalId = string.Format(
                CultureInfo.InvariantCulture, 
                "{0}_{1}", 
                this._categoryReference.GetHashCode(), 
                this._structureReference.GetHashCode());
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (!this.IsExternalReference.IsTrue)
            {
                if (this._structureReference == null)
                {
                    throw new SdmxSemmanticException(
                        ExceptionCode.ObjectMissingRequiredElement, 
                        this.StructureType, 
                        "StructureReference");
                }

                if (this._categoryReference == null)
                {
                    throw new SdmxSemmanticException(
                        ExceptionCode.ObjectMissingRequiredElement, 
                        this.StructureType, 
                        "CategoryReference");
                }
            }

            base.ValidateId(true);
            base.ValidateNameableAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT              //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}