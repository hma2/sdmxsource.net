// -----------------------------------------------------------------------
// <copyright file="DimensionListCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using DimensionList = Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant.DimensionList;
    using DimensionType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.DimensionType;

    /// <summary>
    ///     The dimension list core.
    /// </summary>
    [Serializable]
    public class DimensionListCore : IdentifiableCore, IDimensionList
    {
        /// <summary>
        ///     The dimensions.
        /// </summary>
        private readonly IList<IDimension> _dimensions;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionListCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public DimensionListCore(IDimensionListMutableObject itemMutableObject, IDataStructureObject parent)
            : base(itemMutableObject, parent)
        {
            this._dimensions = new List<IDimension>();
            if (itemMutableObject.Dimensions != null)
            {
                int pos = 1;

                foreach (IDimensionMutableObject currentDimension in itemMutableObject.Dimensions)
                {
                    this._dimensions.Add(new DimensionCore(currentDimension, pos, this));
                    pos++;
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionListCore" /> class.
        /// </summary>
        /// <param name="dimensionList">
        ///     The dimension list.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public DimensionListCore(DimensionListType dimensionList, IMaintainableObject parent)
            : base(dimensionList, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptor), parent)
        {
            this._dimensions = new List<IDimension>();
            int pos = 1;
            if (dimensionList.Dimension != null)
            {
                foreach (Dimension dimension in dimensionList.Dimension)
                {
                    this._dimensions.Add(new DimensionCore(dimension.Content, this, pos));
                    pos++;
                }
            }

            if (ObjectUtil.ValidCollection(dimensionList.MeasureDimension))
            {
                if (dimensionList.MeasureDimension.Count > 1)
                {
                    throw new SdmxSemmanticException("Can not have more then one measure dimension");
                }

                this._dimensions.Add(new DimensionCore(dimensionList.MeasureDimension[0].Content, this, pos));
                pos++;
            }

            if (ObjectUtil.ValidCollection(dimensionList.TimeDimension))
            {
                if (dimensionList.TimeDimension.Count > 1)
                {
                    throw new SdmxSemmanticException("Can not have more then one time dimension");
                }

                this._dimensions.Add(new DimensionCore(dimensionList.TimeDimension[0].Content, this, pos));
            }

            this.ValidateDimensionList();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionListCore" /> class.
        /// </summary>
        /// <param name="keyFamily">
        ///     The agencyScheme.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamily"/> is <see langword="null" />.</exception>
        public DimensionListCore(KeyFamilyType keyFamily, IMaintainableObject parent)
            : this(keyFamily, parent, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DimensionListCore"/> class.
        /// </summary>
        /// <param name="keyFamily">
        /// The agencyScheme.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="conceptRoleBuilder">
        /// The concept Role Builder.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        /// Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keyFamily"/> is <see langword="null"/>.
        /// </exception>
        public DimensionListCore(KeyFamilyType keyFamily, IMaintainableObject parent, IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
            : base(
                DimensionList.FixedId, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptor), 
                parent)
        {
            if (keyFamily == null)
            {
                throw new ArgumentNullException("keyFamily");
            }

            this._dimensions = new List<IDimension>();
            int pos = 1;
            ComponentsType components = keyFamily.Components;
            try
            {
                if (components != null)
                {
                    foreach (DimensionType currentDimension in components.Dimension)
                    {
                        this._dimensions.Add(new DimensionCore(currentDimension, this, pos, conceptRoleBuilder));
                        pos++;
                    }

                    if (components.TimeDimension != null)
                    {
                        this._dimensions.Add(new DimensionCore(components.TimeDimension, this, pos));
                    }
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            this.ValidateDimensionList();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="DimensionListCore" /> class.
        /// </summary>
        /// <param name="keyFamily">
        ///     The agencyScheme.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamily"/> is <see langword="null" />.</exception>
        public DimensionListCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.KeyFamilyType keyFamily, 
            IMaintainableObject parent)
            : base(
                DimensionList.FixedId, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptor), 
                parent)
        {
            if (keyFamily == null)
            {
                throw new ArgumentNullException("keyFamily");
            }

            this._dimensions = new List<IDimension>();
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.ComponentsType components = keyFamily.Components;
            int pos = 1;
            if (components != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.DimensionType currentDimension in
                    components.Dimension)
                {
                    this._dimensions.Add(new DimensionCore(currentDimension, this, pos));
                    pos++;
                }

                if (components.TimeDimension != null)
                {
                    this._dimensions.Add(new DimensionCore(components.TimeDimension, this, pos));
                }
            }

            this.ValidateDimensionList();
        }

        /// <summary>
        ///     Gets the dimensions.
        /// </summary>
        public virtual IList<IDimension> Dimensions
        {
            get
            {
                return new List<IDimension>(this._dimensions);
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return DimensionList.FixedId;
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IDimensionList)sdmxObject;
                if (!this.Equivalent(this.Dimensions, that.Dimensions, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._dimensions, composites);
            return composites;
        }

        /// <summary>
        ///     The validate dimension list.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void ValidateDimensionList()
        {
            // TODO The validation code doesn't work 
            IList<string> idList = new List<string>();

            ////((List<IDimension>)this._dimensions).Sort(); SDMXCONV-125 sorting changes the original position of Dimensions which is very important

            foreach (IDimension dimension in this._dimensions)
            {
                if (idList.Contains(dimension.Id))
                {
                    throw new SdmxSemmanticException("Duplicate Dimensions Id : " + dimension.Id);
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS              //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}