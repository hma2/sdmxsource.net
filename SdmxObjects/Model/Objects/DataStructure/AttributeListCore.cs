// -----------------------------------------------------------------------
// <copyright file="AttributeListCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    using Attribute = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.Attribute;
    using AttributeType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.AttributeType;

    /// <summary>
    ///     The attributeObject list core.
    /// </summary>
    [Serializable]
    public class AttributeListCore : IdentifiableCore, IAttributeList
    {
        /// <summary>
        ///     The attributes.
        /// </summary>
        private readonly IList<IAttributeObject> _attributes;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeListCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="itemMutableObject"/> is <see langword="null" />.</exception>
        public AttributeListCore(IAttributeListMutableObject itemMutableObject, IDataStructureObject parent)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }

            this._attributes = new List<IAttributeObject>();
            if (itemMutableObject.Attributes != null)
            {
                foreach (IAttributeMutableObject currentAttribute in itemMutableObject.Attributes)
                {
                    this._attributes.Add(new AttributeObjectCore(currentAttribute, this));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeListCore" /> class.
        /// </summary>
        /// <param name="attributeList">
        ///     The attributeObject list.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="attributeList"/> is <see langword="null" />.</exception>
        public AttributeListCore(AttributeListType attributeList, IMaintainableObject parent)
            : base(attributeList, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttributeDescriptor), parent)
        {
            if (attributeList == null)
            {
                throw new ArgumentNullException("attributeList");
            }

            this._attributes = new List<IAttributeObject>();
            if (attributeList.Attribute != null)
            {
                foreach (Attribute currentAttribute in attributeList.Attribute)
                {
                    this._attributes.Add(new AttributeObjectCore(currentAttribute.Content, this));
                }
            }
            if (attributeList.ReportingYearStartDay != null)
            {
                foreach (var reportingYearStartDay in attributeList.ReportingYearStartDay)
                {
                    this._attributes.Add(new AttributeObjectCore(reportingYearStartDay.Content, this));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.0 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeListCore" /> class.
        /// </summary>
        /// <param name="keyFamily">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamily"/> is <see langword="null" />.</exception>
        public AttributeListCore(KeyFamilyType keyFamily, IMaintainableObject parent)
            : this(keyFamily, parent, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeListCore"/> class.
        /// </summary>
        /// <param name="keyFamily">
        /// The sdmxObject.
        /// </param>
        /// <param name="parent">
        /// The parent.
        /// </param>
        /// <param name="conceptRoleBuilder">
        /// The concept Role Builder.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keyFamily"/> is <see langword="null"/>.
        /// </exception>
        public AttributeListCore(KeyFamilyType keyFamily, IMaintainableObject parent, IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
            : base(
                AttributeListObject.FixedId, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttributeDescriptor), 
                parent)
        {
            if (keyFamily == null)
            {
                throw new ArgumentNullException("keyFamily");
            }

            this._attributes = new List<IAttributeObject>();
            if (keyFamily.Components != null && keyFamily.Components.Attribute != null)
            {
                foreach (AttributeType currentAttribute in keyFamily.Components.Attribute)
                {
                    this._attributes.Add(new AttributeObjectCore(currentAttribute, this, conceptRoleBuilder));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1.0 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeListCore" /> class.
        /// </summary>
        /// <param name="keyFamily">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamily"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        public AttributeListCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.KeyFamilyType keyFamily, 
            IMaintainableObject parent)
            : base(
                AttributeListObject.FixedId, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttributeDescriptor), 
                parent)
        {
            if (keyFamily == null)
            {
                throw new ArgumentNullException("keyFamily");
            }

            this._attributes = new List<IAttributeObject>();
            if (keyFamily.Components != null && keyFamily.Components.Attribute != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AttributeType currentAttribute in
                    keyFamily.Components.Attribute)
                {
                    this._attributes.Add(new AttributeObjectCore(currentAttribute, this));
                }
            }
        }

        /// <summary>
        ///     Gets the attribtues.
        /// </summary>
        public virtual IList<IAttributeObject> Attributes
        {
            get
            {
                return new List<IAttributeObject>(this._attributes);
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return AttributeListObject.FixedId;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IAttributeList)sdmxObject;
                if (!this.Equivalent(this._attributes, that.Attributes, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._attributes, composites);
            return composites;
        }
    }
}