// -----------------------------------------------------------------------
// <copyright file="GroupCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The group core.
    /// </summary>
    [Serializable]
    public class GroupCore : IdentifiableCore, IGroup
    {
        /// <summary>
        ///     The attachment constraint ref.
        /// </summary>
        private readonly ICrossReference _attachmentConstraintRef;

        /// <summary>
        ///     The dimension ref.
        /// </summary>
        private readonly IList<string> _dimensionRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public GroupCore(IGroupMutableObject itemMutableObject, ISdmxStructure parent)
            : base(itemMutableObject, parent)
        {
            this._dimensionRef = new List<string>();
            if (itemMutableObject.AttachmentConstraintRef != null)
            {
                this._attachmentConstraintRef = new CrossReferenceImpl(this, itemMutableObject.AttachmentConstraintRef);
            }

            if (itemMutableObject.DimensionRef != null)
            {
                this._dimensionRef = new List<string>(itemMutableObject.DimensionRef);
            }

            this.ValidateGroupAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupCore" /> class.
        /// </summary>
        /// <param name="group">
        ///     The group.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public GroupCore(GroupType group, ISdmxStructure parent)
            : base(group, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Group), parent)
        {
            this._dimensionRef = new List<string>();
            if (group.AttachmentConstraint != null)
            {
                this._attachmentConstraintRef = RefUtil.CreateReference(this, group.AttachmentConstraint);
            }

            if (group.GroupDimension != null)
            {
                this._dimensionRef = new List<string>();
                foreach (GroupDimension each in group.GroupDimension)
                {
                    this._dimensionRef.Add(RefUtil.CreateLocalIdReference(each.DimensionReference));
                }
            }

            this.ValidateGroupAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupCore" /> class.
        /// </summary>
        /// <param name="group">
        ///     The group.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="group"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public GroupCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.GroupType group, ISdmxStructure parent)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Group), 
                group.PassNoNull("group").id, 
                null, 
                group.PassNoNull("group").Annotations, 
                parent)
        {
            if (@group == null)
            {
                throw new ArgumentNullException("group");
            }

            this._dimensionRef = new List<string>();

            /*
             * We do NOT support Attachment Constraint References in Version 2 since they are essentially useless.
             */
            if (group.DimensionRef != null)
            {
                this._dimensionRef = new List<string>(group.DimensionRef);
            }

            this.ValidateGroupAttributes();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="GroupCore" /> class.
        /// </summary>
        /// <param name="group">
        ///     The group.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="group"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        public GroupCore(Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.GroupType group, ISdmxStructure parent)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Group), 
                group.PassNoNull("group").id, 
                null, 
                group.PassNoNull("group").Annotations, 
                parent)
        {
            if (@group == null)
            {
                throw new ArgumentNullException("group");
            }

            this._dimensionRef = new List<string>();
            if (group.DimensionRef != null)
            {
                this._dimensionRef = new List<string>(group.DimensionRef);
            }

            this.ValidateGroupAttributes();
        }

        /// <summary>
        ///     Gets the attachment constraint ref.
        /// </summary>
        public virtual ICrossReference AttachmentConstraintRef
        {
            get
            {
                return this._attachmentConstraintRef;
            }
        }

        /// <summary>
        ///     Gets the dimension ref.
        /// </summary>
        public virtual IList<string> DimensionRefs
        {
            get
            {
                return new List<string>(this._dimensionRef);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IGroup)sdmxObject;
                if (!ObjectUtil.EquivalentCollection(this._dimensionRef, that.DimensionRefs))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._attachmentConstraintRef, that.AttachmentConstraintRef))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The validate group attributes.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void ValidateGroupAttributes()
        {
            if (this._attachmentConstraintRef == null && (this._dimensionRef == null || this._dimensionRef.Count == 0))
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredElement, 
                    "Group", 
                    "DimensionRefs/AttachmentConstraintRef");
            }

            if (this._attachmentConstraintRef != null && this._dimensionRef.Count > 0)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMutuallyExclusive, 
                    "DimensionRefs", 
                    "AttachmentConstraintRef", 
                    "Group");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECTS              //////////////////////////////////////////////////
    }
}