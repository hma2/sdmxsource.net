// -----------------------------------------------------------------------
// <copyright file="MeasureListCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    using MeasureList = Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant.MeasureList;
    using PrimaryMeasureType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.PrimaryMeasureType;

    /// <summary>
    ///     The measure list core.
    /// </summary>
    [Serializable]
    public class MeasureListCore : IdentifiableCore, IMeasureList
    {
        /// <summary>
        ///     The iprimary measure.
        /// </summary>
        private readonly IPrimaryMeasure _primaryMeasure;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="measureList">
        ///     The measure list.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(IMeasureListMutableObject measureList, IMaintainableObject parent)
            : base(measureList, parent)
        {
            if (measureList.PrimaryMeasure != null)
            {
                this._primaryMeasure = new PrimaryMeasureCore(measureList.PrimaryMeasure, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="measureList">
        ///     The measure list.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(MeasureListType measureList, IMaintainableObject parent)
            : base(measureList, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor), parent)
        {
            if (measureList.Component != null)
            {
                this._primaryMeasure =
                    new PrimaryMeasureCore(
                        (Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.PrimaryMeasureType)
                        measureList.Component[0].Content, 
                        this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(PrimaryMeasureType primaryMeasure, IMaintainableObject parent)
            : base(MeasureList.FixedId, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor), parent)
        {
            this._primaryMeasure = new PrimaryMeasureCore(primaryMeasure, this);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MeasureListCore" /> class.
        /// </summary>
        /// <param name="primaryMeasure">
        ///     The primary measure.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public MeasureListCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.PrimaryMeasureType primaryMeasure, 
            IMaintainableObject parent)
            : base(MeasureList.FixedId, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDescriptor), parent)
        {
            this._primaryMeasure = new PrimaryMeasureCore(primaryMeasure, this);
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return MeasureList.FixedId;
            }
        }

        /// <summary>
        ///     Gets the primary measure.
        /// </summary>
        public virtual IPrimaryMeasure PrimaryMeasure
        {
            get
            {
                return this._primaryMeasure;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IMeasureList)sdmxObject;
                if (!this.Equivalent(this._primaryMeasure, that.PrimaryMeasure, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._primaryMeasure, composites);
            return composites;
        }
    }
}