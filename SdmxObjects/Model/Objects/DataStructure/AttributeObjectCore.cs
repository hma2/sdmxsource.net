// -----------------------------------------------------------------------
// <copyright file="AttributeObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using AttributeType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.AttributeType;
    using PrimaryMeasure = Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant.PrimaryMeasure;

    /// <summary>
    ///     The attributeObject core.
    /// </summary>
    [Serializable]
    public class AttributeObjectCore : ComponentCore, IAttributeObject
    {
        /// <summary>
        ///     The assignment status.
        /// </summary>
        private readonly string _assignmentStatus;

        /// <summary>
        ///     The attachment group.
        /// </summary>
        private readonly string _attachmentGroup;

        /// <summary>
        ///     The concept roles.
        /// </summary>
        private readonly IList<ICrossReference> _conceptRoles = new List<ICrossReference>();

        /// <summary>
        ///     The dimension reference.
        /// </summary>
        private readonly IList<string> _dimensionReferences = new List<string>();

        /// <summary>
        ///     The attachment level.
        /// </summary>
        private AttributeAttachmentLevel _attachmentLevel;

        /// <summary>
        ///     The primary measure reference.
        /// </summary>
        private string _primaryMeasureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeObjectCore" /> class.
        /// </summary>
        /// <param name="attribute">
        ///     The attributeObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="attribute"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxException">Throws Validate exception.</exception>
        public AttributeObjectCore(IAttributeMutableObject attribute, IAttributeList parent)
            : base(attribute, parent)
        {
            if (attribute == null)
            {
                throw new ArgumentNullException("attribute");
            }

            try
            {
                this._attachmentLevel = attribute.AttachmentLevel;
                this._assignmentStatus = attribute.AssignmentStatus;
                this._attachmentGroup = attribute.AttachmentGroup;
                if (attribute.DimensionReferences != null)
                {
                    this._dimensionReferences = new List<string>(attribute.DimensionReferences);
                }

                if (attribute.ConceptRoles != null)
                {
                    foreach (IStructureReference currentConceptRole in attribute.ConceptRoles)
                    {
                        this._conceptRoles.Add(new CrossReferenceImpl(this, currentConceptRole));
                    }
                }

                this._primaryMeasureReference = attribute.PrimaryMeasureReference;
                this.ValidateAttributeAttributes();
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException("IsError creating structure: " + this, th);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeObjectCore" /> class.
        /// </summary>
        /// <param name="attribute">
        ///     The attributeObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="attribute"/> is <see langword="null" />.</exception>
        public AttributeObjectCore(AttributeType attribute, IAttributeList parent)
            : base(attribute, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute), parent)
        {
            if (attribute == null)
            {
                throw new ArgumentNullException("attribute");
            }

            if (attribute.AttributeRelationship != null)
            {
                AttributeRelationshipType attRelationShip = attribute.AttributeRelationship;
                if (attRelationShip.Group != null)
                {
                    // FUNC - this can also be attached to a dimension list
                    this._attachmentGroup = RefUtil.CreateLocalIdReference(attRelationShip.Group);
                    this._attachmentLevel = AttributeAttachmentLevel.Group;
                }
                else if (ObjectUtil.ValidCollection(attRelationShip.Dimension))
                {
                    // DEFAULT TO DIMENSION GROUP
                    this._attachmentLevel = AttributeAttachmentLevel.DimensionGroup;

                    foreach (LocalDimensionReferenceType dim in attRelationShip.Dimension)
                    {
                        this._dimensionReferences.Add(RefUtil.CreateLocalIdReference(dim));
                    }

                    var parentDsd = (IDataStructureObject)this.MaintainableParent;

                    foreach (IDimension currentDimension in parentDsd.GetDimensions())
                    {
                        if (this._dimensionReferences.Contains(currentDimension.Id))
                        {
                            if (currentDimension.TimeDimension)
                            {
                                // REFERENCING THE TIME DIMENSION THEREFOR OBSERVATION LEVEL
                                this._attachmentLevel = AttributeAttachmentLevel.Observation;
                                break;
                            }
                        }
                    }

                    if (ObjectUtil.ValidCollection(attRelationShip.AttachmentGroup))
                    {
                        this._attachmentGroup = RefUtil.CreateLocalIdReference(attRelationShip.AttachmentGroup.First());
                        this._attachmentLevel = AttributeAttachmentLevel.Group;
                    }
                }
                else if (attRelationShip.PrimaryMeasure != null)
                {
                    this._primaryMeasureReference = RefUtil.CreateLocalIdReference(attRelationShip.PrimaryMeasure);
                    this._attachmentLevel = AttributeAttachmentLevel.Observation;
                }
                else
                {
                    this._attachmentLevel = AttributeAttachmentLevel.DataSet;
                }
            }

            this._assignmentStatus = attribute.assignmentStatus;
            if (attribute.ConceptRole != null)
            {
                foreach (ConceptReferenceType conceptRef in attribute.ConceptRole)
                {
                    this._conceptRoles.Add(RefUtil.CreateReference(this, conceptRef));
                }
            }

            try
            {
                this.ValidateAttributeAttributes();
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException("IsError creating structure: " + this, th);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeObjectCore" /> class.
        /// </summary>
        /// <param name="attribute">
        ///     The attributeObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="attribute"/> is <see langword="null" />.</exception>
        public AttributeObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.AttributeType attribute, IAttributeList parent)
            : this(attribute, parent, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeObjectCore" /> class.
        /// </summary>
        /// <param name="attribute">
        ///     The attributeObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="conceptRoleBuilder">The concept role builder</param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="attribute"/> is <see langword="null" />.</exception>
        public AttributeObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.AttributeType attribute, IAttributeList parent, IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
            : base(
                attribute, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute), 
                attribute.PassNoNull("attribute").Annotations, 
                attribute.PassNoNull("attribute").TextFormat, 
                attribute.PassNoNull("attribute").codelistAgency, 
                attribute.PassNoNull("attribute").codelist, 
                attribute.PassNoNull("attribute").codelistVersion, 
                attribute.PassNoNull("attribute").conceptSchemeAgency, 
                attribute.PassNoNull("attribute").conceptSchemeRef, 
                GetConceptSchemeVersion(attribute), 
                attribute.PassNoNull("attribute").conceptAgency, 
                attribute.PassNoNull("attribute").conceptRef, 
                parent)
        {
            if (attribute == null)
            {
                throw new ArgumentNullException("attribute");
            }

            if (attribute.AttachmentGroup != null)
            {
                if (attribute.AttachmentGroup.Count > 1)
                {
                    throw new SdmxSemmanticException(
                        ExceptionCode.Unsupported, 
                        "Attribute with more then one group attachment");
                }

                if (attribute.AttachmentGroup.Count == 1)
                {
                    this._attachmentGroup = attribute.AttachmentGroup[0];
                }
            }

            AttributeAttachmentLevel attachmentLevel0 /* was: null */;
            var parentDsd = (IDataStructureObject)this.MaintainableParent;

            switch (attribute.attachmentLevel)
            {
                case AttachmentLevelTypeConstants.DataSet:
                    attachmentLevel0 = AttributeAttachmentLevel.DataSet;
                    break;
                case AttachmentLevelTypeConstants.Group:
                    attachmentLevel0 = AttributeAttachmentLevel.Group;
                    break;
                case AttachmentLevelTypeConstants.Observation:
                    attachmentLevel0 = AttributeAttachmentLevel.Observation;
                    this._primaryMeasureReference = parentDsd.PrimaryMeasure.Id;
                    break;
                case AttachmentLevelTypeConstants.Series:
                    attachmentLevel0 = AttributeAttachmentLevel.DimensionGroup;
                    {
                        foreach (IDimension currentDimension in
                            parentDsd.GetDimensions(
                                SdmxStructureEnumType.Dimension, 
                                SdmxStructureEnumType.MeasureDimension))
                        {
                            if (!this._dimensionReferences.Contains(currentDimension.Id))
                            {
                                this._dimensionReferences.Add(currentDimension.Id);
                            }
                        }
                    }

                    break;
                default:
                    throw new SdmxNotImplementedException(
                        ExceptionCode.Unsupported, 
                        "Attachment:" + attribute.attachmentLevel);
            }

            this._attachmentLevel = attachmentLevel0;
            this._assignmentStatus = attribute.assignmentStatus;

            this.BuildConceptRoles(attribute, conceptRoleBuilder);

            try
            {
                this.ValidateAttributeAttributes();
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException("IsError creating structure: " + this, th);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="AttributeObjectCore" /> class.
        /// </summary>
        /// <param name="attribute">
        ///     The attributeObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// ///
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        public AttributeObjectCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AttributeType attribute, 
            IAttributeList parent)
            : base(
                attribute, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataAttribute), 
                attribute.PassNoNull("attribute").Annotations, 
                attribute.PassNoNull("attribute").codelist, 
                attribute.PassNoNull("attribute").concept, 
                parent)
        {
            if (attribute.AttachmentGroup != null)
            {
                if (attribute.AttachmentGroup.Count > 1)
                {
                    throw new SdmxSemmanticException(
                        ExceptionCode.Unsupported, 
                        "Attribute with more then one group attachment");
                }

                if (attribute.AttachmentGroup.Count == 1)
                {
                    this._attachmentGroup = attribute.AttachmentGroup[0];
                }
            }

            AttributeAttachmentLevel attachmentLevel0;
            var parentDsd = (IDataStructureObject)this.MaintainableParent;

            switch (attribute.attachmentLevel)
            {
                case Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AttachmentLevelTypeConstants.DataSet:
                    attachmentLevel0 = AttributeAttachmentLevel.DataSet;
                    break;
                case Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AttachmentLevelTypeConstants.Group:
                    attachmentLevel0 = AttributeAttachmentLevel.Group;
                    break;
                case Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AttachmentLevelTypeConstants.Observation:
                    attachmentLevel0 = AttributeAttachmentLevel.Observation;
                    this._primaryMeasureReference = parentDsd.PrimaryMeasure.Id;
                    break;
                case Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure.AttachmentLevelTypeConstants.Series:
                    attachmentLevel0 = AttributeAttachmentLevel.DimensionGroup;
                    {
                        foreach (IDimension currentDimension in parentDsd.GetDimensions(SdmxStructureEnumType.Dimension))
                        {
                            if (!this._dimensionReferences.Contains(currentDimension.Id))
                            {
                                this._dimensionReferences.Add(currentDimension.Id);
                            }
                        }
                    }

                    break;
                default:
                    throw new SdmxNotImplementedException(
                        ExceptionCode.Unsupported, 
                        "Attachment:" + attribute.attachmentLevel);
            }

            this._attachmentLevel = attachmentLevel0;
            this._assignmentStatus = attribute.assignmentStatus;

            try
            {
                this.ValidateAttributeAttributes();
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException("IsError creating structure: " + this, th);
            }
        }

        /// <summary>
        ///     Gets the assignment status.
        /// </summary>
        public virtual string AssignmentStatus
        {
            get
            {
                return this._assignmentStatus;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets the attachment group.
        /// </summary>
        public virtual string AttachmentGroup
        {
            get
            {
                return this._attachmentGroup;
            }
        }

        /// <summary>
        ///     Gets the attachment level.
        /// </summary>
        public virtual AttributeAttachmentLevel AttachmentLevel
        {
            get
            {
                return this._attachmentLevel;
            }
        }

        /// <summary>
        ///     Gets the concept roles.
        /// </summary>
        public IList<ICrossReference> ConceptRoles
        {
            get
            {
                return new List<ICrossReference>(this._conceptRoles);
            }
        }

        /// <summary>
        ///     Gets the dimension reference.
        /// </summary>
        public IList<string> DimensionReferences
        {
            get
            {
                return this._dimensionReferences;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether mandatory.
        /// </summary>
        public bool Mandatory
        {
            get
            {
                return this._assignmentStatus.Equals("Mandatory");
            }
        }

        /// <summary>
        ///     Gets the primary measure reference.
        /// </summary>
        public string PrimaryMeasureReference
        {
            get
            {
                return this._primaryMeasureReference;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether time format.
        /// </summary>
        public bool TimeFormat
        {
            get
            {
                return this.Id.Equals("TIME_FORMAT");
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IAttributeObject)sdmxObject;
                if (!ObjectUtil.Equivalent(this._attachmentLevel, that.AttachmentLevel))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._assignmentStatus, that.AssignmentStatus))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._attachmentGroup, that.AttachmentGroup))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._dimensionReferences, that.DimensionReferences))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._conceptRoles, that.ConceptRoles))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._primaryMeasureReference, that.PrimaryMeasureReference))
                {
                    return false;
                }

                return this.DeepEqualsComponent(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get parent ids.
        /// </summary>
        /// <param name="includeDifferentTypes">
        ///     The include different types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        protected internal override IList<string> GetParentIds(bool includeDifferentTypes)
        {
            IList<string> returnList = new List<string>();
            returnList.Add(this.Id);
            return returnList;
        }

        /// <summary>
        ///     The validate attributeObject attributes.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void ValidateAttributeAttributes()
        {
            if (this._attachmentLevel == AttributeAttachmentLevel.Null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredAttribute, 
                    this.StructureType, 
                    "attachmentLevel");
            }

            if (this._attachmentLevel == AttributeAttachmentLevel.Observation)
            {
                if (!ObjectUtil.ValidCollection(this._dimensionReferences))
                {
                    // Ensure that the primary measure reference is set if the dimension references are empty
                    this._primaryMeasureReference = PrimaryMeasure.FixedId;
                }
                else
                {
                    var parentDsd = (IDataStructureObject)this.MaintainableParent;
                    IDimension timeDimension = parentDsd.TimeDimension;
                    if (timeDimension == null)
                    {
                        // This is not an observation attributeObject as it does not include the time dimension
                        this._attachmentLevel = AttributeAttachmentLevel.DimensionGroup;
                    }
                    else
                    {
                        string timeDimensionId = timeDimension.Id;
                        if (!this._dimensionReferences.Contains(timeDimensionId))
                        {
                            // This is not an observation attributeObject as it does not include the time dimension
                            this._attachmentLevel = AttributeAttachmentLevel.DimensionGroup;
                        }
                    }
                }
            }

            if (this._assignmentStatus == null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredAttribute, 
                    this.StructureType, 
                    "assignmentStatus");
            }

            // TODO: Check err messages
            if (string.IsNullOrWhiteSpace(this._attachmentGroup)
                && this._attachmentLevel == AttributeAttachmentLevel.Group)
            {
                throw new SdmxSemmanticException(
                    "Attribute has specified attachment level specified as Group, but specifies the attachment groups. Either change the attachment level to group, or remove the attachment groups off the attributeObject : "
                    + this.BuildComponentId());
            }

            if (!string.IsNullOrWhiteSpace(this._attachmentGroup)
                && this._attachmentLevel != AttributeAttachmentLevel.Group)
            {
                throw new SdmxSemmanticException(
                    "Attribute specifies an attachment group of '" + this._attachmentGroup
                    + "', but the the attachement level is not set to GROUP, it is set to  '" + this._attachmentLevel
                    + "'.  Either remove the attributeObject's reference to the '" + this._attachmentGroup
                    + "' or change the attributeObject's attachment level to GROUP");
            }

            if (this._attachmentLevel == AttributeAttachmentLevel.DimensionGroup)
            {
                if (!ObjectUtil.ValidCollection(this._dimensionReferences))
                {
                    throw new SdmxSemmanticException(
                        "Attribute specifies attachment level of 'Dimensions Group' but does not specify any dimensions");
                }
            }

            foreach (ICrossReference conceptRole in this._conceptRoles)
            {
                if (conceptRole.TargetReference != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept))
                {
                    throw new SdmxSemmanticException(
                        "Illegal concept role '" + conceptRole.TargetUrn + "'.  Concept Role must reference a concept.");
                }
            }
        }

        /// <summary>
        ///     Returns concept scheme version. It tries to detect various conventions
        /// </summary>
        /// <param name="attributeType">
        ///     The attribute Type.
        /// </param>
        /// <returns>
        ///     The concept scheme version; otherwise null
        /// </returns>
        private static string GetConceptSchemeVersion(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.AttributeType attributeType)
        {
            if (!string.IsNullOrWhiteSpace(attributeType.conceptVersion))
            {
                return attributeType.conceptVersion;
            }

            if (!string.IsNullOrWhiteSpace(attributeType.ConceptSchemeVersionEstat))
            {
                return attributeType.ConceptSchemeVersionEstat;
            }

            var extDimension = attributeType as Org.Sdmx.Resources.SdmxMl.Schemas.V20.extension.structure.AttributeType;
            if (extDimension != null && !string.IsNullOrWhiteSpace(extDimension.conceptSchemeVersion))
            {
                return extDimension.conceptSchemeVersion;
            }

            return null;
        }

        /// <summary>
        /// Builds the concept roles.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <param name="componentRoleBuilder">The component role builder.</param>
        private void BuildConceptRoles(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.AttributeType attribute, IBuilder<IStructureReference, ComponentRole> componentRoleBuilder)
        {
            if (componentRoleBuilder != null)
            {
                if (attribute.isCountAttribute)
                {
                    this._conceptRoles.Add(new CrossReferenceImpl(this, componentRoleBuilder.Build(ComponentRole.Count)));
                }

                if (attribute.isEntityAttribute)
                {
                    this._conceptRoles.Add(new CrossReferenceImpl(this, componentRoleBuilder.Build(ComponentRole.Entity)));
                }

                if (attribute.isIdentityAttribute)
                {
                    this._conceptRoles.Add(new CrossReferenceImpl(this, componentRoleBuilder.Build(ComponentRole.Identity)));
                }

                if (attribute.isNonObservationalTimeAttribute)
                {
                    this._conceptRoles.Add(new CrossReferenceImpl(this, componentRoleBuilder.Build(ComponentRole.NonObservationalTime)));
                }

                if (attribute.isFrequencyAttribute)
                {
                    this._conceptRoles.Add(new CrossReferenceImpl(this, componentRoleBuilder.Build(ComponentRole.Frequency)));
                }
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS              //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}