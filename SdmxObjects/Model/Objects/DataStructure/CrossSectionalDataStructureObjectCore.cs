// -----------------------------------------------------------------------
// <copyright file="CrossSectionalDataStructureObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The cross sectional data structure object core.
    /// </summary>
    [Serializable]
    public class CrossSectionalDataStructureObjectCore : DataStructureObjectCore, ICrossSectionalDataStructureObject
    {
        #region Fields

        /// <summary>
        ///     The _attribute to measures map.
        /// </summary>
        private readonly IDictionary<string, IList<ICrossSectionalMeasure>> _attributeToMeasuresMap = new Dictionary<string, IList<ICrossSectionalMeasure>>();

        /// <summary>
        ///     The codelist map.
        /// </summary>
        private readonly IDictionary<string, ICrossReference> _codelistMap = new Dictionary<string, ICrossReference>();

        /// <summary>
        ///     The _cross sectional attach data set.
        /// </summary>
        private readonly IList<IComponent> _crossSectionalAttachDataSet = new List<IComponent>();

        /// <summary>
        ///     The _cross sectional attach group.
        /// </summary>
        private readonly IList<IComponent> _crossSectionalAttachGroup = new List<IComponent>();

        /// <summary>
        ///     The _cross sectional attach observation.
        /// </summary>
        private readonly IList<IComponent> _crossSectionalAttachObservation = new List<IComponent>();

        /// <summary>
        ///     The _cross sectional attach section.
        /// </summary>
        private readonly IList<IComponent> _crossSectionalAttachSection = new List<IComponent>();

        /// <summary>
        ///     The _cross sectional measures.
        /// </summary>
        private readonly IList<ICrossSectionalMeasure> _crossSectionalMeasures = new List<ICrossSectionalMeasure>();

        /// <summary>
        ///     The measure dimensions.
        /// </summary>
        private readonly IList<string> _measureDimensions = new List<string>();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossSectionalDataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The itemMutableObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="SdmxException">Could not construct the object due to validation errors.</exception>
        public CrossSectionalDataStructureObjectCore(ICrossSectionalDataStructureMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            if (!itemMutableObject.Stub)
            {
                this.BuildXSMeasures(itemMutableObject);

                this.BuildMeasureDimCodelistMapping(itemMutableObject);

                this.BuildMeasureDimension(itemMutableObject);

                this.BuildCrossSectionalDatasetList(itemMutableObject);

                this.BuildCrossSectionalGroupList(itemMutableObject);

                this.BuildCrossSectionalSectionList(itemMutableObject);

                this.BuildCrossSectionalObsList(itemMutableObject);

                this.BuildAttributeMeasureMap(itemMutableObject);
            }
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.SafeGenerateUrn());
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.SafeGenerateUrn());
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossSectionalDataStructureObjectCore" /> class.
        /// </summary>
        /// <param name="keyFamilyType">
        ///     The itemMutableObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="keyFamilyType" /> is <see langword="null" />.</exception>
        /// <exception cref="SdmxException">Could not construct the object.</exception>
        public CrossSectionalDataStructureObjectCore(KeyFamilyType keyFamilyType)
            : this(keyFamilyType, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrossSectionalDataStructureObjectCore"/> class.
        /// </summary>
        /// <param name="keyFamilyType">
        /// The itemMutableObject.
        /// </param>
        /// <param name="conceptRoleBuilder">
        /// The concept Role Builder.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        /// Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keyFamilyType"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SdmxException">
        /// Could not construct the object.
        /// </exception>
        public CrossSectionalDataStructureObjectCore(KeyFamilyType keyFamilyType, IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
            : base(keyFamilyType, conceptRoleBuilder)
        {
            if (keyFamilyType == null)
            {
                throw new ArgumentNullException("keyFamilyType");
            }

            //// TODO java 0.9.9 bug. XS DSD may not have any XS Measures
            //// Reverted changes that were made during sync
            //// Please do not change unless you are sure.
            if (keyFamilyType.Components == null || !CrossSectionalUtil.IsCrossSectional(keyFamilyType))
            {
                throw new SdmxSemmanticException("Can not create ICrossSectionalDataStructureObject as there are no CrossSectional Measures defined");
            }

            this.BuildXSMeasures(keyFamilyType);

            this.BuildDimensions(keyFamilyType);

            this.BuildAttributes(keyFamilyType);

            this.BuildTimeDimension(keyFamilyType);

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the components.
        /// </summary>
        public override IList<IComponent> Components
        {
            get
            {
                IList<IComponent> components = base.Components;
                components.AddAll(this._crossSectionalMeasures);
                return components;
            }
        }

        /// <summary>
        ///     Gets the cross sectional measures.
        /// </summary>
        public IList<ICrossSectionalMeasure> CrossSectionalMeasures
        {
            get
            {
                return new List<ICrossSectionalMeasure>(this._crossSectionalMeasures);
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public new ICrossSectionalDataStructureMutableObject MutableInstance
        {
            get
            {
                return new CrossSectionalDataStructureMutableCore(this);
            }
        }

        #endregion

        #region Explicit Interface Properties

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        IMaintainableMutableObject IMaintainableObject.MutableInstance
        {
            get
            {
                return this.MutableInstance;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Get the cross sectional measures that the attribute is linked to, returns an empty list if there is no cross
        ///     sectional measures
        ///     defined by the attribute.
        /// </summary>
        /// <param name="attributeObject">
        ///     The attribute.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="attributeObject"/> is <see langword="null" />.</exception>
        public IList<ICrossSectionalMeasure> GetAttachmentMeasures(IAttributeObject attributeObject)
        {
            if (attributeObject == null)
            {
                throw new ArgumentNullException("attributeObject");
            }

            IList<ICrossSectionalMeasure> val;
            if (this._attributeToMeasuresMap.TryGetValue(attributeObject.Id, out val))
            {
                return val;
            }

            return new List<ICrossSectionalMeasure>();
        }

        /// <summary>
        ///     Gets the codelist reference for the dimension with the given id
        /// </summary>
        /// <param name="dimensionId">
        ///     The dimension Id.
        /// </param>
        /// <returns>
        ///     The <see cref="ICrossReference" /> .
        /// </returns>
        public ICrossReference GetCodelistForMeasureDimension(string dimensionId)
        {
            ICrossReference codelistRef;
            if (this._codelistMap.TryGetValue(dimensionId, out codelistRef))
            {
                return codelistRef;
            }

            return null;
        }

        /// <summary>
        ///     The get cross sectional attach data set.
        /// </summary>
        /// <param name="returnOnlyIfLowestLevel">
        ///     The return only if lowest level.
        /// </param>
        /// <param name="returnTypes">
        ///     The return enumeration types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IComponent> GetCrossSectionalAttachDataSet(bool returnOnlyIfLowestLevel, params SdmxStructureType[] returnTypes)
        {
            IList<IComponent> returnList = GetComponets(this._crossSectionalAttachDataSet, returnTypes);
            if (returnOnlyIfLowestLevel)
            {
                returnList.RemoveItemList(this._crossSectionalAttachGroup);
                returnList.RemoveItemList(this._crossSectionalAttachSection);
                returnList.RemoveItemList(this._crossSectionalAttachObservation);
            }

            return returnList;
        }

        /// <summary>
        ///     Returns the cross sectional attach group.
        /// </summary>
        /// <param name="returnOnlyIfLowestLevel">
        ///     The return only if lowest level.
        /// </param>
        /// <param name="returnTypes">
        ///     The return enumeration types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IComponent> GetCrossSectionalAttachGroup(bool returnOnlyIfLowestLevel, params SdmxStructureType[] returnTypes)
        {
            IList<IComponent> returnList = GetComponets(this._crossSectionalAttachGroup, returnTypes);
            if (returnOnlyIfLowestLevel)
            {
                returnList.RemoveItemList(this._crossSectionalAttachSection);
                returnList.RemoveItemList(this._crossSectionalAttachObservation);
            }

            return returnList;
        }

        /// <summary>
        ///     The get cross sectional attach observation.
        /// </summary>
        /// <param name="returnTypes">
        ///     The return types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IComponent> GetCrossSectionalAttachObservation(params SdmxStructureType[] returnTypes)
        {
            return GetComponets(this._crossSectionalAttachObservation, returnTypes);
        }

        /// <summary>
        ///     The get cross sectional attach section.
        /// </summary>
        /// <param name="returnOnlyIfLowestLevel">
        ///     The return only if lowest level.
        /// </param>
        /// <param name="returnTypes">
        ///     The return types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        public IList<IComponent> GetCrossSectionalAttachSection(bool returnOnlyIfLowestLevel, params SdmxStructureType[] returnTypes)
        {
            IList<IComponent> returnList = GetComponets(this._crossSectionalAttachSection, returnTypes);
            if (returnOnlyIfLowestLevel)
            {
                returnList.RemoveItemList(this._crossSectionalAttachObservation);
            }

            return returnList;
        }

        /// <summary>
        ///     The get cross sectional measure.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="ICrossSectionalMeasure" /> .
        /// </returns>
        public ICrossSectionalMeasure GetCrossSectionalMeasure(string id)
        {
            foreach (ICrossSectionalMeasure currentMeasure in this._crossSectionalMeasures)
            {
                if (currentMeasure.Id.Equals(id) || string.Equals(currentMeasure.Code, id))
                {
                    return currentMeasure;
                }
            }

            return null;
        }

        /// <summary>
        ///     The is measure dimension.
        /// </summary>
        /// <param name="dimension">
        ///     The dimension.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="dimension"/> is <see langword="null" />.</exception>
        public bool IsMeasureDimension(IDimension dimension)
        {
            if (dimension == null)
            {
                throw new ArgumentNullException("dimension");
            }

            return this._measureDimensions.Contains(dimension.Id);
        }

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableObject" /> .
        /// </returns>
        IMaintainableObject IMaintainableObject.GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return this.GetStub(actualLocation, isServiceUrl);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>the composites</returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._crossSectionalAttachDataSet, composites);
            this.AddToCompositeSet(this._crossSectionalAttachGroup, composites);
            this.AddToCompositeSet(this._crossSectionalAttachSection, composites);
            this.AddToCompositeSet(this._crossSectionalAttachObservation, composites);
            this.AddToCompositeSet(this._crossSectionalMeasures, composites);
            return composites;
        }

        /// <summary>
        ///     The get componets.
        /// </summary>
        /// <param name="listToGetFrom">
        ///     The list to get from.
        /// </param>
        /// <param name="returnTypes">
        ///     The return types.
        /// </param>
        /// <returns>
        ///     The <see cref="IList{T}" /> .
        /// </returns>
        private static IList<IComponent> GetComponets(IEnumerable<IComponent> listToGetFrom, params SdmxStructureType[] returnTypes)
        {
            IList<IComponent> returnList = new List<IComponent>();

            foreach (IComponent currentComponent in listToGetFrom)
            {
                if (IsValidReturnType(currentComponent, returnTypes))
                {
                    returnList.Add(currentComponent);
                }
            }

            return returnList;
        }

        /// <summary>
        ///     The is valid return type.
        /// </summary>
        /// <param name="component">
        ///     The component.
        /// </param>
        /// <param name="returnTypes">
        ///     The return types.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        private static bool IsValidReturnType(ISdmxObject component, params SdmxStructureType[] returnTypes)
        {
            if (returnTypes == null || returnTypes.Length == 0)
            {
                return true;
            }

            foreach (SdmxStructureType currentRetrunType in returnTypes)
            {
                if (component.StructureType == currentRetrunType)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     The add component to list.
        /// </summary>
        /// <param name="componentId">
        ///     The component id.
        /// </param>
        /// <param name="listToAddTo">
        ///     The list to add to.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private void AddComponentToList(string componentId, ICollection<IComponent> listToAddTo)
        {
            // TODO BUG in Java 0.9.4. It assumes that component id == concept ref always
            IComponent component = this.GetComponent(componentId);
            if (component == null)
            {
                throw new SdmxSemmanticException("Can not find referenced component with id " + componentId);
            }

            listToAddTo.Add(component);
        }

        /// <summary>
        ///     Builds the attribute measure map.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildAttributeMeasureMap(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.AttributeToMeasureMap != null)
            {
                foreach (KeyValuePair<string, IList<string>> pair in itemMutableObject.AttributeToMeasureMap)
                {
                    this.SetAttributeMeasures(pair.Key, pair.Value);
                }
            }
        }

        /// <summary>
        ///     Builds the attributes.
        /// </summary>
        /// <param name="keyFamilyType">Type of the key family.</param>
        private void BuildAttributes(KeyFamilyType keyFamilyType)
        {
            foreach (AttributeType currentComponent0 in keyFamilyType.Components.Attribute)
            {
                string componentId1 = currentComponent0.conceptRef;

                if (currentComponent0.crossSectionalAttachDataSet != null && currentComponent0.crossSectionalAttachDataSet.Value)
                {
                    this.AddComponentToList(componentId1, this._crossSectionalAttachDataSet);
                }

                if (currentComponent0.crossSectionalAttachGroup != null && currentComponent0.crossSectionalAttachGroup.Value)
                {
                    this.AddComponentToList(componentId1, this._crossSectionalAttachGroup);
                }

                if (currentComponent0.crossSectionalAttachObservation != null && currentComponent0.crossSectionalAttachObservation.Value)
                {
                    this.AddComponentToList(componentId1, this._crossSectionalAttachObservation);
                }

                if (currentComponent0.crossSectionalAttachSection != null && currentComponent0.crossSectionalAttachSection.Value)
                {
                    this.AddComponentToList(componentId1, this._crossSectionalAttachSection);
                }

                this.SetAttributeMeasures(componentId1, currentComponent0.AttachmentMeasure);
            }
        }

        /// <summary>
        ///     Builds the cross sectional dataset list.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildCrossSectionalDatasetList(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.CrossSectionalAttachDataSet != null)
            {
                foreach (string componentId in itemMutableObject.CrossSectionalAttachDataSet)
                {
                    this.AddComponentToList(componentId, this._crossSectionalAttachDataSet);
                }
            }
        }

        /// <summary>
        ///     Builds the cross sectional group list.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildCrossSectionalGroupList(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.CrossSectionalAttachGroup != null)
            {
                foreach (string componentId0 in itemMutableObject.CrossSectionalAttachGroup)
                {
                    this.AddComponentToList(componentId0, this._crossSectionalAttachGroup);
                }
            }
        }

        /// <summary>
        ///     Builds the cross sectional obs list.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildCrossSectionalObsList(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.CrossSectionalAttachObservation != null)
            {
                foreach (string componentId2 in itemMutableObject.CrossSectionalAttachObservation)
                {
                    this.AddComponentToList(componentId2, this._crossSectionalAttachObservation);
                }
            }
        }

        /// <summary>
        ///     Builds the cross sectional section list.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildCrossSectionalSectionList(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.CrossSectionalAttachSection != null)
            {
                foreach (string componentId1 in itemMutableObject.CrossSectionalAttachSection)
                {
                    this.AddComponentToList(componentId1, this._crossSectionalAttachSection);
                }
            }
        }

        /// <summary>
        ///     Builds the dimensions.
        /// </summary>
        /// <param name="keyFamilyType">Type of the key family.</param>
        private void BuildDimensions(KeyFamilyType keyFamilyType)
        {
            foreach (DimensionType currentComponent in keyFamilyType.Components.Dimension)
            {
                string componentId = currentComponent.conceptRef;
                if (currentComponent.isMeasureDimension)
                {
                    this._measureDimensions.Add(componentId);
                    string codelistAgency = currentComponent.codelistAgency;
                    if (string.IsNullOrWhiteSpace(codelistAgency))
                    {
                        codelistAgency = this.BaseAgencyId;
                    }

                    ICrossReference codelistRef = new CrossReferenceImpl(
                        this, 
                        codelistAgency, 
                        currentComponent.codelist, 
                        currentComponent.codelistVersion, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
                    this._codelistMap.Add(componentId, codelistRef);
                }

                if (currentComponent.crossSectionalAttachDataSet != null && currentComponent.crossSectionalAttachDataSet.Value)
                {
                    this.AddComponentToList(componentId, this._crossSectionalAttachDataSet);
                }

                if (currentComponent.crossSectionalAttachGroup != null && currentComponent.crossSectionalAttachGroup.Value)
                {
                    this.AddComponentToList(componentId, this._crossSectionalAttachGroup);
                }

                if (currentComponent.crossSectionalAttachObservation != null && currentComponent.crossSectionalAttachObservation.Value)
                {
                    this.AddComponentToList(componentId, this._crossSectionalAttachObservation);
                }

                if (currentComponent.crossSectionalAttachSection != null && currentComponent.crossSectionalAttachSection.Value)
                {
                    this.AddComponentToList(componentId, this._crossSectionalAttachSection);
                }
            }
        }

        /// <summary>
        ///     Builds the measure dimension codelist mapping.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildMeasureDimCodelistMapping(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.MeasureDimensionCodelistMapping != null)
            {
                foreach (KeyValuePair<string, IStructureReference> pair in itemMutableObject.MeasureDimensionCodelistMapping)
                {
                    this._codelistMap.Add(pair.Key, new CrossReferenceImpl(this, pair.Value));
                }
            }
        }

        /// <summary>
        ///     Builds the measure dimension.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildMeasureDimension(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            foreach (IDimensionMutableObject dim in itemMutableObject.Dimensions)
            {
                if (dim.MeasureDimension)
                {
                    this._measureDimensions.Add(dim.ConceptRef.IdentifiableIds[0]);
                }
            }
        }

        /// <summary>
        ///     Builds the time dimension.
        /// </summary>
        /// <param name="keyFamilyType">Type of the key family.</param>
        private void BuildTimeDimension(KeyFamilyType keyFamilyType)
        {
            if (keyFamilyType.Components.TimeDimension != null)
            {
                TimeDimensionType timeDimensionType = keyFamilyType.Components.TimeDimension;
                if (timeDimensionType.crossSectionalAttachDataSet != null && timeDimensionType.crossSectionalAttachDataSet.Value)
                {
                    this.AddComponentToList(DimensionObject.TimeDimensionFixedId, this._crossSectionalAttachDataSet);
                }

                if (timeDimensionType.crossSectionalAttachGroup != null && timeDimensionType.crossSectionalAttachGroup.Value)
                {
                    this.AddComponentToList(DimensionObject.TimeDimensionFixedId, this._crossSectionalAttachGroup);
                }

                if (timeDimensionType.crossSectionalAttachObservation != null && timeDimensionType.crossSectionalAttachObservation.Value)
                {
                    this.AddComponentToList(DimensionObject.TimeDimensionFixedId, this._crossSectionalAttachObservation);
                }

                if (timeDimensionType.crossSectionalAttachSection != null && timeDimensionType.crossSectionalAttachSection.Value)
                {
                    this.AddComponentToList(DimensionObject.TimeDimensionFixedId, this._crossSectionalAttachSection);
                }
            }
        }

        /// <summary>
        ///     Builds the cross-sectional measures.
        /// </summary>
        /// <param name="itemMutableObject">The item mutable object.</param>
        private void BuildXSMeasures(ICrossSectionalDataStructureMutableObject itemMutableObject)
        {
            if (itemMutableObject.CrossSectionalMeasures != null)
            {
                foreach (ICrossSectionalMeasureMutableObject currentMeasure in itemMutableObject.CrossSectionalMeasures)
                {
                    this._crossSectionalMeasures.Add(new CrossSectionalMeasureCore(currentMeasure, this));
                }
            }
        }

        /// <summary>
        ///     Builds the xs measures.
        /// </summary>
        /// <param name="keyFamilyType">Type of the key family.</param>
        private void BuildXSMeasures(KeyFamilyType keyFamilyType)
        {
            foreach (CrossSectionalMeasureType xsMeasureType in keyFamilyType.Components.CrossSectionalMeasure)
            {
                this._crossSectionalMeasures.Add(new CrossSectionalMeasureCore(xsMeasureType, this));
            }
        }

        /// <summary>
        ///     The set attributeObject measures.
        /// </summary>
        /// <param name="attributeId">
        ///     The attributeObject id.
        /// </param>
        /// <param name="measureIds">
        ///     The measure ids.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        private void SetAttributeMeasures(string attributeId, ICollection<string> measureIds)
        {
            if (!ObjectUtil.ValidCollection(measureIds))
            {
                return;
            }

            try
            {
                // TODO BUG in Java 0.9.4. It assumes that component id == concept ref always
                IComponent component = this.GetComponent(attributeId);
                if (component == null)
                {
                    throw new SdmxSemmanticException("Could not resolve reference to attributeObject with id '" + attributeId + "' " + "referenced from cross sectional data structure");
                }

                var attribute = component as IAttributeObject;
                if (attribute != null)
                {
                    IAttributeObject att = attribute;
                    if (att.AttachmentLevel != AttributeAttachmentLevel.Observation)
                    {
                        var sb = new StringBuilder();
                        sb.Append("Attribute '");
                        sb.Append(attributeId);
                        sb.Append("' is referencing cross sectional measure, the attributeObject ");
                        sb.Append("must have an attachment level of Observation, it is currently set to '");
                        sb.Append(att.AttachmentLevel);
                        sb.Append("'");

                        throw new SdmxSemmanticException(sb.ToString());
                    }
                }
                else
                {
                    throw new SdmxSemmanticException(
                        "Cross Sectional Measure attributeObject reference id '" + attributeId + "' " + "is referencing structure of type '" + component.StructureType.StructureType + "'");
                }

                IList<ICrossSectionalMeasure> measureList = new List<ICrossSectionalMeasure>();

                foreach (string measureId in measureIds)
                {
                    if (measureId == null)
                    {
                        continue;
                    }

                    ICrossSectionalMeasure crossSectionalMeasure = this.GetCrossSectionalMeasure(measureId);
                    if (crossSectionalMeasure == null)
                    {
                        throw new SdmxSemmanticException(
                            "Could not resolve reference to cross sectional measure with id '" + measureId + "' " + "referenced from attributeObject '" + attributeId + "'");
                    }

                    measureList.Add(crossSectionalMeasure);
                }

                this._attributeToMeasuresMap.Add(attributeId, measureList);
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                              //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     The validate.
        /// </summary>
        private void Validate()
        {
            if (this.IsExternalReference != null && this.IsExternalReference.IsTrue)
            {
                return;
            }

            //// Reverted changes that were made during sync
            //// Please do not change unless you are sure.
            ISet<IComponent> crossSectionalAttached = new HashSet<IComponent>(this._crossSectionalAttachDataSet);
            crossSectionalAttached.UnionWith(this._crossSectionalAttachGroup);
            crossSectionalAttached.UnionWith(this._crossSectionalAttachSection);
            crossSectionalAttached.UnionWith(this._crossSectionalAttachObservation);

            var dimensions = this.GetDimensions(SdmxStructureEnumType.Dimension);
            foreach (IDimension dimension in dimensions)
            {
                if (!dimension.FrequencyDimension && !crossSectionalAttached.Contains(dimension))
                {
                    throw new SdmxSemmanticException(
                        string.Format(CultureInfo.InvariantCulture, "Can not create Cross Sectional Data Structure, dimension '{0}' doesn't have cross sectional attachment level.", dimension.Id));
                }
            }

            var attributes = this.Attributes;
            foreach (IAttributeObject attribute in attributes)
            {
                if (!crossSectionalAttached.Contains(attribute))
                {
                    throw new SdmxSemmanticException(
                        string.Format(CultureInfo.InvariantCulture, "Can not create Cross Sectional Data Structure, attribute '{0}' doesn't have cross sectional attachment level.", attribute.Id));
                }
            }
        }

        #endregion
    }
}