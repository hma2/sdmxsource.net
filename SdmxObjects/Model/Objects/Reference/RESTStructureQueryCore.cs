﻿// -----------------------------------------------------------------------
// <copyright file="RESTStructureQueryCore.cs" company="EUROSTAT">
//   Date Created : 2013-06-03
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     The rest structure query core.
    /// </summary>
    [Serializable]
    public class RESTStructureQueryCore : IRestStructureQuery
    {
        /// <summary>
        ///     The structure query metadata.
        /// </summary>
        private readonly IStructureQueryMetadata _structureQueryMetadata =
            new StructureQueryMetadataCore(
                StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Null), 
                null, 
                null, 
                true);

        /// <summary>
        ///     The structure reference.
        /// </summary>
        private IStructureReference _structureReference;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        ///     Creation of a Structure Query for structures that match the given reference
        /// </summary>
        /// <param name="structureReference">
        ///     The structure reference.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="structureReference"/> is <see langword="null" />.</exception>
        public RESTStructureQueryCore(IStructureReference structureReference)
        {
            if (structureReference == null)
            {
                throw new ArgumentNullException("structureReference");
            }

            this._structureReference = structureReference;
            if (structureReference.Version != null)
            {
                this._structureQueryMetadata =
                    new StructureQueryMetadataCore(
                        StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Null), 
                        null, 
                        null, 
                        false);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        /// </summary>
        /// <param name="structureQueryDetail">
        ///     The structure query detail.
        /// </param>
        /// <param name="structureReferenceDetail">
        ///     The structure reference detail.
        /// </param>
        /// <param name="specificStructureReference">
        ///     The specific structure reference.
        /// </param>
        /// <param name="structureReference">
        ///     The structure reference.
        /// </param>
        /// <param name="returnLatest">
        ///     The return last.
        /// </param>
        public RESTStructureQueryCore(
            StructureQueryDetail structureQueryDetail, 
            StructureReferenceDetail structureReferenceDetail, 
            SdmxStructureType specificStructureReference, 
            IStructureReference structureReference, 
            bool returnLatest)
        {
            this._structureQueryMetadata = new StructureQueryMetadataCore(
                structureQueryDetail, 
                structureReferenceDetail, 
                specificStructureReference, 
                returnLatest);
            this._structureReference = structureReference;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        /// </summary>
        /// <param name="restString">
        ///     The rest string.
        /// </param>
        public RESTStructureQueryCore(string restString)
            : this(restString, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        ///     Constructs a REST query from the rest query string, example:
        ///     /dataflow/ALL/ALL/ALL
        /// </summary>
        /// <param name="queryString">
        ///     The rest query string.
        /// </param>
        /// <param name="queryParameters">
        ///     The parameters.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="queryString"/> is <see langword="null" />.</exception>
        public RESTStructureQueryCore(string queryString, IDictionary<string, string> queryParameters)
        {
            if (queryString == null)
            {
                throw new ArgumentNullException("queryString");
            }

            if (queryString.StartsWith("/", StringComparison.Ordinal))
            {
                queryString = queryString.Substring(1);
            }

            if (queryParameters == null)
            {
                queryParameters = new Dictionary<string, string>();
            }

            // Parse any additional parameters
            if (queryString.IndexOf("?", StringComparison.Ordinal) > 0)
            {
                string paramters = queryString.Substring(queryString.IndexOf("?", StringComparison.Ordinal) + 1);
                queryString = queryString.Substring(0, queryString.IndexOf("?", StringComparison.Ordinal));

                foreach (string currentParam in paramters.Split('&'))
                {
                    string[] param = currentParam.Split('=');
                    queryParameters.Add(param[0], param[1]);
                }
            }

            string[] args = queryString.Split('/');

            this.ParserQueryString(args);
            this._structureQueryMetadata = new StructureQueryMetadataCore(args, queryParameters);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTStructureQueryCore" /> class.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        /// <param name="queryParameters">
        ///     The query parameters.
        /// </param>
        public RESTStructureQueryCore(string[] queryString, IDictionary<string, string> queryParameters)
        {
            this.ParserQueryString(queryString);
            this._structureQueryMetadata = new StructureQueryMetadataCore(queryString, queryParameters);
        }

        /// <summary>
        ///     Gets the structure query metadata.
        /// </summary>
        public virtual IStructureQueryMetadata StructureQueryMetadata
        {
            get
            {
                return this._structureQueryMetadata;
            }
        }

        /// <summary>
        ///     Gets the structure reference.
        /// </summary>
        public virtual IStructureReference StructureReference
        {
            get
            {
                return this._structureReference;
            }
        }

        /// <summary>
        ///     The to string.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "ref: " + this._structureReference + "-detail:" + this._structureQueryMetadata.StructureQueryDetail
                   + "-references:" + this._structureReference + "-specific"
                   + this._structureQueryMetadata.SpecificStructureReference + "latest"
                   + this._structureQueryMetadata.IsReturnLatest;
        }

        /// <summary>
        ///     The get structure type.
        /// </summary>
        /// <param name="str">The structure.</param>
        /// <returns>the SDMX structure type</returns>
        private static SdmxStructureType GetStructureType(string str)
        {
            if (str.Equals("structure", StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }

            if (str.Equals("organisationscheme", StringComparison.OrdinalIgnoreCase))
            {
                return SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationScheme);
            }

            return SdmxStructureType.ParseClass(str);
        }

        /// <summary>
        ///     The parse query string.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The parsed query string</returns>
        private string ParseQueryString(string query)
        {
            if (!ObjectUtil.ValidString(query))
            {
                return null;
            }

            if (query.Equals("all", StringComparison.OrdinalIgnoreCase))
            {
                return "*";
            }

            if (query.Equals("latest", StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }

            return query;
        }

        /// <summary>
        ///     The parser query string.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        private void ParserQueryString(string[] queryString)
        {
            if (queryString.Length < 1)
            {
                throw new SdmxSemmanticException("Structure Query Expecting at least 1 parameter (structure type)");
            }

            SdmxStructureType structureType = GetStructureType(queryString[0]);
            string agencyId = null;
            string id = null;
            string version = null;
            if (queryString.Length >= 2)
            {
                agencyId = this.ParseQueryString(queryString[1]);
            }

            if (queryString.Length >= 3)
            {
                id = this.ParseQueryString(queryString[2]);
            }

            if (queryString.Length >= 4)
            {
                version = this.ParseQueryString(queryString[3]);
            }

            this._structureReference = new StructureReferenceImpl(agencyId, id, version, structureType);
        }
    }
}