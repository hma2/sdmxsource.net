﻿// -----------------------------------------------------------------------
// <copyright file="StructureQueryMetadataCore.cs" company="EUROSTAT">
//   Date Created : 2013-03-14
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    #endregion

    /// <summary>
    ///     StructureQueryMetadataCore class
    /// </summary>
    public class StructureQueryMetadataCore : IStructureQueryMetadata
    {
        /// <summary>
        ///     The is return latest
        /// </summary>
        private bool _isReturnLatest;

        /// <summary>
        ///     The specific structure reference
        /// </summary>
        private SdmxStructureType _specificStructureReference;

        /// <summary>
        ///     The structure query detail
        /// </summary>
        private StructureQueryDetail _structureQueryDetail =
            StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);

        /// <summary>
        ///     The structure reference detail
        /// </summary>
        private StructureReferenceDetail _structureReferenceDetail =
            StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureQueryMetadataCore" /> class.
        /// </summary>
        /// <param name="structureQueryDetail">The structure query detail.</param>
        /// <param name="structureReferenceDetail">The structure reference detail.</param>
        /// <param name="specificStructureReference">The specific structure reference.</param>
        /// <param name="isReturnLatest">if set to <c>true</c> [is return latest].</param>
        /// <exception cref="SdmxSemmanticException">
        ///     SpecificStructureReference is null and specific reference detail was requested
        ///     or
        ///     SpecificStructureReference is not maintainable
        /// </exception>
        public StructureQueryMetadataCore(
            StructureQueryDetail structureQueryDetail, 
            StructureReferenceDetail structureReferenceDetail, 
            SdmxStructureType specificStructureReference, 
            bool isReturnLatest)
        {
            if (structureReferenceDetail
                == StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Specific)
                && specificStructureReference == null)
            {
                throw new SdmxSemmanticException(
                    "SpecificStructureReference is null and specific reference detail was requested");
            }

            if (specificStructureReference != null && !specificStructureReference.IsMaintainable)
            {
                throw new SdmxSemmanticException("SpecificStructureReference is not maintainable");
            }

            if (structureQueryDetail != StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Null))
            {
                this._structureQueryDetail = structureQueryDetail;
            }

            if (structureReferenceDetail != null)
            {
                this._structureReferenceDetail = structureReferenceDetail;
            }

            if (specificStructureReference != null)
            {
                this._specificStructureReference = specificStructureReference;
            }

            this._isReturnLatest = isReturnLatest;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureQueryMetadataCore" /> class.
        /// </summary>
        /// <param name="querystring">The querystring.</param>
        /// <param name="queryParameters">The query parameters.</param>
        public StructureQueryMetadataCore(string[] querystring, IDictionary<string, string> queryParameters)
        {
            this.ParserQuerystring(querystring);
            this.ParserQueryParameters(queryParameters);
        }

        /// <summary>
        ///     Gets a value indicating whether this is a query for all of the latest versions of the given artefacts
        /// </summary>
        public bool IsReturnLatest
        {
            get
            {
                return this._isReturnLatest;
            }
        }

        /// <summary>
        ///     Gets the specific structure reference if STRUCTURE_REFERENCE_DETAIL == SPECIFIC, otherwise null
        /// </summary>
        public SdmxStructureType SpecificStructureReference
        {
            get
            {
                return this._specificStructureReference;
            }
        }

        /// <summary>
        ///     Gets the query detail for this structure query, can not be null
        /// </summary>
        public StructureQueryDetail StructureQueryDetail
        {
            get
            {
                return this._structureQueryDetail;
            }
        }

        /// <summary>
        ///     Gets the reference detail for this structure query, can not be null
        /// </summary>
        public StructureReferenceDetail StructureReferenceDetail
        {
            get
            {
                return this._structureReferenceDetail;
            }
        }

        /// <summary>
        ///     Parsers the query parameters.
        /// </summary>
        /// <param name="queryParameters">The query parameters.</param>
        /// <exception cref="SdmxSemmanticException">
        ///     unable to parse value for key  + key
        ///     or
        ///     unable to parse value for key  + key
        ///     or
        ///     Unknown query parameter :  + key
        /// </exception>
        private void ParserQueryParameters(IDictionary<string, string> queryParameters)
        {
            if (queryParameters != null)
            {
                foreach (var keyPair in queryParameters)
                {
                    string key = keyPair.Key;
                    string value = keyPair.Value;
                    if (key.Equals("detail", StringComparison.OrdinalIgnoreCase))
                    {
                        var structureQueryDetailEnum = StructureQueryDetailEnumType.Full;
                        if (!Enum.TryParse(value, true, out structureQueryDetailEnum))
                        {
                            this._structureQueryDetail = StructureQueryDetail.GetFromEnum(structureQueryDetailEnum);
                            throw new SdmxSemmanticException("unable to parse value for key " + key);
                        }

                        this._structureQueryDetail = StructureQueryDetail.GetFromEnum(structureQueryDetailEnum);
                    }
                    else if (key.Equals("references", StringComparison.OrdinalIgnoreCase))
                    {
                        try
                        {
                            this._structureReferenceDetail = StructureReferenceDetail.ParseString(value);

                            if (this._structureReferenceDetail.EnumType == StructureReferenceDetailEnumType.Specific)
                            {
                                this._specificStructureReference = SdmxStructureType.ParseClass(value);
                            }
                        }
                        catch (SdmxSemmanticException e)
                        {
                            throw new SdmxSemmanticException("unable to parse value for key " + key, e);
                        }
                    }
                    else
                    {
                        throw new SdmxSemmanticException("Unknown query parameter : " + key);
                    }
                }
            }
        }

        /// <summary>
        ///     Parsers the querystring.
        /// </summary>
        /// <param name="querystring">The querystring.</param>
        private void ParserQuerystring(string[] querystring)
        {
            if (querystring.Length >= 4)
            {
                if ("latest".Equals(querystring[3], StringComparison.OrdinalIgnoreCase))
                {
                    this._isReturnLatest = true;
                }
            }
            else
            {
                this._isReturnLatest = true;
            }
        }
    }
}