﻿// -----------------------------------------------------------------------
// <copyright file="RESTSchemaQueryCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    #region Using directives

    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    #endregion

    /// <summary>
    ///     The rest schema query core.
    /// </summary>
    [Serializable]
    public class RESTSchemaQueryCore : IRestSchemaQuery
    {
        /// <summary>
        ///     The agency id.
        /// </summary>
        private string _agencyId;

        /// <summary>
        ///     The context.
        /// </summary>
        private string _context;

        /// <summary>
        ///     The dim at obs.
        /// </summary>
        private string _dimAtObs = DimensionObject.TimeDimensionFixedId;

        /// <summary>
        ///     The explicit measure.
        /// </summary>
        private bool _explicitMeasure;

        /// <summary>
        ///     The id.
        /// </summary>
        private string _id;

        /// <summary>
        ///     The reference.
        /// </summary>
        private IStructureReference _reference;

        /// <summary>
        ///     The version.
        /// </summary>
        private string _version;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTSchemaQueryCore" /> class.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <param name="dimAtObs">The dim at obs.</param>
        public RESTSchemaQueryCore(IStructureReference reference, string dimAtObs)
            : this(reference, dimAtObs, true)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTSchemaQueryCore" /> class.
        /// </summary>
        /// <param name="reference">The reference.</param>
        /// <param name="dimAtObs">The dim at obs.</param>
        /// <param name="explicitMeasure">if set to <c>true</c> [explicit measure].</param>
        public RESTSchemaQueryCore(IStructureReference reference, string dimAtObs, bool explicitMeasure)
        {
            this._reference = reference;
            if (ObjectUtil.ValidString(dimAtObs))
            {
                this._dimAtObs = dimAtObs;
            }

            this._explicitMeasure = explicitMeasure;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTSchemaQueryCore" /> class.
        ///     Constructs a schema query from a full or partial REST URL.
        ///     The URL must start before the Schema segment and be complete, example input:
        ///     /schema/provision/IMF/PGI/1.0?dimensionAtObservation=freq
        /// </summary>
        /// <param name="restString">
        ///     REst Uri
        /// </param>
        public RESTSchemaQueryCore(string restString)
            : this(restString, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTSchemaQueryCore" /> class.
        ///     Constructs a schema query from a full or partial REST URL.
        ///     The URL must start before the Schema segment and be complete, example input:
        ///     /schema/provision/IMF/PGI/1.0?dimensionAtObservation=freq
        /// </summary>
        /// <param name="restString">REst Uri</param>
        /// <param name="queryParameters">The query parameters.</param>
        /// <exception cref="ArgumentNullException"><paramref name="restString"/> is <see langword="null" />.</exception>
        public RESTSchemaQueryCore(string restString, IDictionary<string, string> queryParameters)
        {
            if (restString == null)
            {
                throw new ArgumentNullException("restString");
            }

            // Construct a String[] for the queryString
            string queryString = restString.Substring(restString.IndexOf("schema/", StringComparison.OrdinalIgnoreCase));
            string[] queryStringArr = queryString.Split('/');

            if (queryParameters == null)
            {
                queryParameters = new Dictionary<string, string>();
            }

            if (queryString.IndexOf('?') > 0)
            {
                string parameters = queryString.Substring(queryString.IndexOf('?') + 1);
                queryString = queryString.Substring(0, queryString.IndexOf('?'));

                foreach (string currentParam in parameters.Split('&'))
                {
                    string[] param = currentParam.Split('=');
                    queryParameters.Add(param[0], param[1]);
                }
            }

            this.Evaluate(queryStringArr, queryParameters);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RESTSchemaQueryCore" /> class.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        /// <param name="queryParameters">
        ///     The query parameters.
        /// </param>
        public RESTSchemaQueryCore(string[] queryString, IDictionary<string, string> queryParameters)
        {
            this.Evaluate(queryString, queryParameters);
        }

        /// <summary>
        ///     Gets the dim at obs.
        /// </summary>
        public string DimAtObs
        {
            get
            {
                return this._dimAtObs;
            }
        }

        /// <summary>
        ///     Gets the reference.
        /// </summary>
        public IStructureReference Reference
        {
            get
            {
                return this._reference;
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     The is explicit measure.
        /// </summary>
        /// <returns>
        ///     The boolean
        /// </returns>
        public bool IsExplicitMeasure()
        {
            return this._explicitMeasure;
        }

        /// <summary>
        ///     The evaluate.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        /// <param name="queryParameters">
        ///     The query parameters.
        /// </param>
        private void Evaluate(string[] queryString, IDictionary<string, string> queryParameters)
        {
            this.ParseQueryString(queryString);
            this.ParseQueryParameters(queryParameters);

            SdmxStructureType referencedStructure = SdmxStructureType.ParseClass(this._context);

            if (referencedStructure != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)
                && referencedStructure != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)
                && referencedStructure != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProvisionAgreement)
                && referencedStructure != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow)
                && referencedStructure != SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd))
            {
                throw new SdmxSemmanticException("The referenced structure is not a legitimate type!");
            }

            IMaintainableRefObject maintainableRef = new MaintainableRefObjectImpl(
                this._agencyId, 
                this._id, 
                this._version);
            this._reference = new StructureReferenceImpl(maintainableRef, referencedStructure);
        }

        /// <summary>
        ///     The parse query parameters.
        /// </summary>
        /// <param name="parameters">
        ///     The parameters.
        /// </param>
        private void ParseQueryParameters(IDictionary<string, string> parameters)
        {
            if (parameters != null)
            {
                foreach (var keyPair in parameters)
                {
                    var key = keyPair.Key;
                    if (key.Equals("dimensionAtObservation", StringComparison.OrdinalIgnoreCase))
                    {
                        this._dimAtObs = keyPair.Value;
                    }
                    else if (key.Equals("explicitMeasure", StringComparison.OrdinalIgnoreCase))
                    {
                        string val = keyPair.Value;
                        this._explicitMeasure = bool.Parse(val);
                    }
                    else
                    {
                        throw new SdmxSemmanticException(
                            "Unknown query parameter : " + key
                            + " allowed parameters [dimensionAtObservation, explicitMeasure]");
                    }
                }
            }
        }

        /// <summary>
        ///     The parse query string.
        /// </summary>
        /// <param name="queryString">
        ///     The query string.
        /// </param>
        private void ParseQueryString(string[] queryString)
        {
            if (queryString.Length < 2)
            {
                throw new SdmxSemmanticException("Schema query expected to contain context as the second argument");
            }

            this._context = queryString[1];

            if (queryString.Length < 3)
            {
                throw new SdmxSemmanticException("Schema query expected to contain Agency ID as the third argument");
            }

            this._agencyId = queryString[2];

            if (queryString.Length < 4)
            {
                throw new SdmxSemmanticException("Schema query expected to contain Resource ID as the fourth argument");
            }

            this._id = queryString[3];

            if (queryString.Length > 4)
            {
                this._version = queryString[4];
                if (this._version.Equals("latest", StringComparison.OrdinalIgnoreCase))
                {
                    this._version = null;
                }
            }

            if (queryString.Length > 5)
            {
                throw new SdmxSemmanticException("Schema query has unexpected sixth argument");
            }
        }
    }
}