// -----------------------------------------------------------------------
// <copyright file="CrossReferencingTreeCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference
{
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The cross referencing tree impl.
    /// </summary>
    public class CrossReferencingTreeCore : ICrossReferencingTree
    {
        /// <summary>
        ///     The imaintianable.
        /// </summary>
        private readonly IMaintainableObject _maintainableObject;

        /// <summary>
        ///     The referencing objects.
        /// </summary>
        private readonly IList<ICrossReferencingTree> _referencingObjects;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CrossReferencingTreeCore" /> class.
        /// </summary>
        /// <param name="maintainable">
        ///     The maintainable.
        /// </param>
        /// <param name="referencingObjects">
        ///     The referencing objects
        /// </param>
        public CrossReferencingTreeCore(
            IMaintainableObject maintainable, 
            IList<ICrossReferencingTree> referencingObjects)
        {
            this._referencingObjects = new List<ICrossReferencingTree>();
            this._maintainableObject = maintainable;
            if (referencingObjects != null)
            {
                this._referencingObjects = referencingObjects;
            }
        }

        /// <summary>
        ///     Gets the maintainable.
        /// </summary>
        public virtual IMaintainableObject Maintainable
        {
            get
            {
                return this._maintainableObject;
            }
        }

        /// <summary>
        ///     Gets the referencing structures.
        /// </summary>
        public virtual IList<ICrossReferencingTree> ReferencingStructures
        {
            get
            {
                return new List<ICrossReferencingTree>(this._referencingObjects);
            }
        }
    }
}