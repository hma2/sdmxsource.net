﻿// -----------------------------------------------------------------------
// <copyright file="ComplexIdentifiableReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///     The complex identifiable reference.
    /// </summary>
    [Serializable]
    public class ComplexIdentifiableReferenceCore : ComplexNameableReferenceCore, IComplexIdentifiableReferenceObject
    {
        /// <summary>
        ///     The child reference.
        /// </summary>
        private readonly IComplexIdentifiableReferenceObject _childReference;

        /// <summary>
        ///     The id.
        /// </summary>
        private readonly IComplexTextReference _id;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexIdentifiableReferenceCore" /> class.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="annotationRef">
        ///     The annotation ref.
        /// </param>
        /// <param name="nameRef">
        ///     The name ref.
        /// </param>
        /// <param name="descriptionRef">
        ///     The description ref.
        /// </param>
        /// <param name="childReference">
        ///     The child reference.
        /// </param>
        public ComplexIdentifiableReferenceCore(
            IComplexTextReference id, 
            SdmxStructureType structureType, 
            IComplexAnnotationReference annotationRef, 
            IComplexTextReference nameRef, 
            IComplexTextReference descriptionRef, 
            IComplexIdentifiableReferenceObject childReference)
            : base(structureType, annotationRef, nameRef, descriptionRef)
        {
            this._id = id;
            this._childReference = childReference;
        }

        /// <summary>
        ///     Gets the child reference.
        /// </summary>
        public virtual IComplexIdentifiableReferenceObject ChildReference
        {
            get
            {
                return this._childReference;
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public virtual IComplexTextReference Id
        {
            get
            {
                return this._id;
            }
        }
    }
}