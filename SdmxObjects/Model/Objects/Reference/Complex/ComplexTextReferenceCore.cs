﻿// -----------------------------------------------------------------------
// <copyright file="ComplexTextReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///     The complex annotation reference.
    /// </summary>
    public class ComplexTextReferenceCore : IComplexTextReference
    {
        /// <summary>
        ///     The lang.
        /// </summary>
        private readonly string _lang;

        /// <summary>
        ///     The operator.
        /// </summary>
        private readonly TextSearch _operator;

        /// <summary>
        ///     The search param.
        /// </summary>
        private readonly string _searchParam;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexTextReferenceCore" /> class.
        /// </summary>
        /// <param name="lang">
        ///     The lang.
        /// </param>
        /// <param name="_operator">
        ///     The operator.
        /// </param>
        /// <param name="searchParam">
        ///     The search param.
        /// </param>
        public ComplexTextReferenceCore(string lang, TextSearch _operator, string searchParam)
        {
            this._lang = lang;

            if (_operator == null)
            {
                this._operator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            }
            else
            {
                this._operator = _operator;
            }

            if (string.IsNullOrEmpty(searchParam))
            {
                throw new SdmxSemmanticException("Not provided text to search for. It should not be null or empty.");
            }

            this._searchParam = searchParam;
        }

        /// <summary>
        ///     Gets the language.
        /// </summary>
        public virtual string Language
        {
            get
            {
                return this._lang;
            }
        }

        /// <summary>
        ///     Gets the operator.
        /// </summary>
        public virtual TextSearch Operator
        {
            get
            {
                return this._operator;
            }
        }

        /// <summary>
        ///     Gets the search parameter.
        /// </summary>
        public virtual string SearchParameter
        {
            get
            {
                return this._searchParam;
            }
        }
    }
}