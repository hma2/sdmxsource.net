﻿// -----------------------------------------------------------------------
// <copyright file="ComplexVersionReferenceCore.cs" company="EUROSTAT">
//   Date Created : 2013-05-31
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex
{
    #region Using directives

    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    #endregion

    /// <summary>
    ///     The complex annotation reference.
    /// </summary>
    public class ComplexVersionReferenceCore : IComplexVersionReference
    {
        /// <summary>
        ///     The return latest.
        /// </summary>
        private readonly TertiaryBool _returnLatest = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

        /// <summary>
        ///     The valid from.
        /// </summary>
        private readonly ITimeRange _validFrom;

        /// <summary>
        ///     The valid to.
        /// </summary>
        private readonly ITimeRange _validTo;

        /// <summary>
        ///     The version.
        /// </summary>
        private readonly string _version;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComplexVersionReferenceCore" /> class.
        /// </summary>
        /// <param name="returnLatest">
        ///     The return latest.
        /// </param>
        /// <param name="version">
        ///     The version.
        /// </param>
        /// <param name="validFrom">
        ///     The valid from.
        /// </param>
        /// <param name="validTo">
        ///     The valid to.
        /// </param>
        public ComplexVersionReferenceCore(
            TertiaryBool returnLatest, 
            string version, 
            ITimeRange validFrom, 
            ITimeRange validTo)
        {
            if (returnLatest != null)
            {
                this._returnLatest = returnLatest;
            }

            this._version = version;
            this._validFrom = validFrom;
            this._validTo = validTo;
        }

        /// <summary>
        ///     Gets the is return latest.
        /// </summary>
        public virtual TertiaryBool IsReturnLatest
        {
            get
            {
                return this._returnLatest;
            }
        }

        /// <summary>
        ///     Gets the version.
        /// </summary>
        public virtual string Version
        {
            get
            {
                return this._version;
            }
        }

        /// <summary>
        ///     Gets the version valid from.
        /// </summary>
        public virtual ITimeRange VersionValidFrom
        {
            get
            {
                return this._validFrom;
            }
        }

        /// <summary>
        ///     Gets the version valid to.
        /// </summary>
        public virtual ITimeRange VersionValidTo
        {
            get
            {
                return this._validTo;
            }
        }
    }
}