// -----------------------------------------------------------------------
// <copyright file="SchemeMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Attributes;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The scheme map core.
    /// </summary>
    [Serializable]
    public abstract class SchemeMapCore : NameableCore, ISchemeMapObject
    {
        /// <summary>
        ///     The source ref.
        /// </summary>
        private ICrossReference _sourceRef;

        /// <summary>
        ///     The target ref.
        /// </summary>
        private ICrossReference _targetRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemeMapCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        internal SchemeMapCore([ValidatedNotNull]ISchemeMapMutableObject itemMutableObject, IIdentifiableObject parent)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject.SourceRef != null)
            {
                this._sourceRef = new CrossReferenceImpl(this, itemMutableObject.SourceRef);
            }

            if (itemMutableObject.TargetRef != null)
            {
                this._targetRef = new CrossReferenceImpl(this, itemMutableObject.TargetRef);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemeMapCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        internal SchemeMapCore([ValidatedNotNull]NameableType createdFrom, SdmxStructureType structureType, IIdentifiableObject parent)
            : base(createdFrom, structureType, parent)
        {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="SchemeMapCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="uri">
        ///     The uri.
        /// </param>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <param name="description">
        ///     The description.
        /// </param>
        /// <param name="annotationsType">
        ///     The annotations type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="createdFrom"/> is <see langword="null" />.</exception>
        internal SchemeMapCore(
            [ValidatedNotNull]IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            string id, 
            Uri uri, 
            IList<TextType> name, 
            IList<TextType> description, 
            AnnotationsType annotationsType, 
            IIdentifiableObject parent)
            : base(structureType, id, uri, name, description, annotationsType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }
        }

        /// <summary>
        ///     Gets or sets the source ref.
        /// </summary>
        public ICrossReference SourceRef
        {
            get
            {
                return this._sourceRef;
            }

            set
            {
                this._sourceRef = value;
            }
        }

        /// <summary>
        ///     Gets or sets the target ref.
        /// </summary>
        public ICrossReference TargetRef
        {
            get
            {
                return this._targetRef;
            }

            set
            {
                this._targetRef = value;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ISchemeMapObject)sdmxObject;
                if (ObjectUtil.Equivalent(this._sourceRef, that.SourceRef))
                {
                    return false;
                }

                if (ObjectUtil.Equivalent(this._targetRef, that.TargetRef))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////
    }
}