// -----------------------------------------------------------------------
// <copyright file="ComponentMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The component map core.
    /// </summary>
    [Serializable]
    public class ComponentMapCore : AnnotableCore, IComponentMapObject
    {
        /// <summary>
        ///     The map concept ref.
        /// </summary>
        private readonly string _mapConceptRef;

        /// <summary>
        ///     The map target concept ref.
        /// </summary>
        private readonly string _mapTargetConceptRef;

        /// <summary>
        ///     The rep map ref.
        /// </summary>
        private readonly IRepresentationMapRef _repMapRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMapCore" /> class.
        /// </summary>
        /// <param name="compMap">
        ///     The comp map.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected internal ComponentMapCore(IComponentMapMutableObject compMap, ISdmxStructure parent)
            : base(compMap, parent)
        {
            if (compMap == null)
            {
                throw new ArgumentNullException("compMap");
            }

            if (compMap.MapConceptRef != null)
            {
                this._mapConceptRef = compMap.MapConceptRef;
            }

            if (compMap.MapTargetConceptRef != null)
            {
                this._mapTargetConceptRef = compMap.MapTargetConceptRef;
            }

            if (compMap.RepMapRef != null)
            {
                this._repMapRef = new RepresentationMapRefCore(compMap.RepMapRef, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMapCore" /> class.
        /// </summary>
        /// <param name="compMap">
        ///     The comp map.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected internal ComponentMapCore(ComponentMapType compMap, IStructureMapObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ComponentMap), parent)
        {
            if (compMap == null)
            {
                throw new ArgumentNullException("compMap");
            }

            this._mapConceptRef = compMap.Source.GetTypedRef<LocalComponentListComponentRefType>().id;
            this._mapTargetConceptRef = compMap.Target.GetTypedRef<LocalComponentListComponentRefType>().id;
            if (compMap.RepresentationMapping != null)
            {
                this._repMapRef = new RepresentationMapRefCore(compMap.RepresentationMapping, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        // private ICrossReference getCrossReferenceFromRef(ICrossReference dsdRef, RefBaseType ref) {
        // SdmxStructureType structureType = SdmxStructureType.parseClass(dsdRef.get().toString());
        // string agencyId = dsdRef.getMaintainableReference().getAgencyId();
        // string id = dsdRef.getMaintainableReference().getMaintainableId();
        // string version = dsdRef.getMaintainableReference().getVersion();
        // string componentId = ref.getId();
        // return  new CrossReferenceCore(this, agencyId, id, version, structureType, componentId);
        // }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComponentMapCore" /> class.
        /// </summary>
        /// <param name="compMap">
        ///     The comp map.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// ///
        /// <exception cref="SdmxNotImplementedException">
        ///     Throws Unsupported Exception.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "compMap", Justification = "Incomplete code. Waiting for SdmxSource Java to implement functionality.")]
        protected internal ComponentMapCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ComponentMapType compMap, 
            IStructureMapObject parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ComponentMap), parent)
        {
            throw new SdmxNotImplementedException("ComponentMapCore at version 2.0");

            // super(null, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryMap), parent);
            // this.mapConceptRef = getCrossReferenceFromV2Ref(parent.getSourceRef(), compMap.getMapConceptRef());
            // this.mapTargetConceptRef = getCrossReferenceFromV2Ref(parent.getTargetRef(), compMap.getMapTargetConceptRef());
            // if (compMap.getRepresentationMapRef() != null) {
            // this.repMapRef =  new RepresentationMapRefCore(compMap.getRepresentationMapRef(), this);
            // }
            // if(compMap.getToTextFormat() != null) {
            // this.repMapRef = new RepresentationMapRefCore(compMap.getToTextFormat(), compMap.getToValueType(), this);
            // }
            // try {
            // validate();
            // } catch(ValidationException e) {
            // throw new SdmxSemmanticException(e, ExceptionCode.FAIL_VALIDATION, this);
            // }
        }

        /// <summary>
        ///     Gets the map concept ref.
        /// </summary>
        public virtual string MapConceptRef
        {
            get
            {
                return this._mapConceptRef;
            }
        }

        /// <summary>
        ///     Gets the map target concept ref.
        /// </summary>
        public virtual string MapTargetConceptRef
        {
            get
            {
                return this._mapTargetConceptRef;
            }
        }

        /// <summary>
        ///     Gets the rep map ref.
        /// </summary>
        public virtual IRepresentationMapRef RepMapRef
        {
            get
            {
                return this._repMapRef;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IComponentMapObject)sdmxObject;
                if (!ObjectUtil.Equivalent(this._mapConceptRef, that.MapConceptRef))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._mapTargetConceptRef, that.MapTargetConceptRef))
                {
                    return false;
                }

                if (!this.Equivalent(this._repMapRef, that.RepMapRef, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsInternalAnnotable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._repMapRef, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (!ObjectUtil.ValidObject(this._mapConceptRef))
            {
                throw new SdmxSemmanticException("Component Map missing source component");
            }

            if (!ObjectUtil.ValidObject(this._mapTargetConceptRef))
            {
                throw new SdmxSemmanticException("Component Map missing target component");
            }

            ISet<SdmxStructureType> allowedTypes = new HashSet<SdmxStructureType>();
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AttributeDescriptor));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConstraintContentTarget));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DatasetTarget));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DimensionDescriptorValuesTarget));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.IdentifiableObjectTarget));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MeasureDimension));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttribute));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.PrimaryMeasure));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.TimeDimension));
            allowedTypes.Add(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportPeriodTarget));

            // verifyTypes(allowedTypes, mapConceptRef.getTargetReference());
            // verifyTypes(allowedTypes, mapTargetConceptRef.getTargetReference());
        }

        // }
        // throw new SdmxSemmanticException("Version 2.0 ComponentMap expecting conceptRef to be a reference to the component in the format 'Class@ComponentId'   Exmaple:Dimensions@FREQ");
        // if(split.length != 2) {

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        // //Dimensions@MT@DsdIn@10@A
        // private ICrossReference getCrossReferenceFromV2Ref(ICrossReference dsdRef, string ref) {
        // string[] split = ref.split("@");

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        // SdmxStructureType structureType = SdmxStructureType.parseClass(split[0]);
        // string agencyId = dsdRef.getMaintainableReference().getAgencyId();
        // string id = dsdRef.getMaintainableReference().getMaintainableId();
        // string version = dsdRef.getMaintainableReference().getVersion();
        // string componentId = split[1];
        // return  new CrossReferenceCore(this, agencyId, id, version, structureType, componentId);
        // }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        // private void verifyTypes(Set<SdmxStructureType> allowedTypes, SdmxStructureType actualType) {
        // if(!allowedTypes.contains(actualType)) {
        // string allowedTypesStr = "";
        // for(SdmxStructureType currentType : allowedTypes) {
        // allowedTypesStr+=currentType +",";
        // }
        // allowedTypesStr = allowedTypesStr.substring(0, allowedTypesStr.length()-2);
        // throw new SdmxSemmanticException("Disallowed concept map type '"+actualType+"' allowed types are '"+allowedTypesStr+"'");
        // }
        // }
    }
}