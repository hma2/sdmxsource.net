// -----------------------------------------------------------------------
// <copyright file="CategoryMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    using CategoryMapType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.CategoryMapType;

    /// <summary>
    ///     The category map core.
    /// </summary>
    [Serializable]
    public class CategoryMapCore : SdmxStructureCore, ICategoryMap
    {
        /// <summary>
        ///     The alias.
        /// </summary>
        private readonly string _alias;

        /// <summary>
        ///     The source id.
        /// </summary>
        private readonly IList<string> _sourceId = new List<string>();

        /// <summary>
        ///     The target id.
        /// </summary>
        private readonly IList<string> _targetId = new List<string>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryMapCore" /> class.
        /// </summary>
        /// <param name="categoryMapMutable">
        ///     Category Map object
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal CategoryMapCore(ICategoryMapMutableObject categoryMapMutable, ISdmxStructure parent)
            : base(categoryMapMutable, parent)
        {
            this._alias = categoryMapMutable.Alias;
            if (categoryMapMutable.SourceId != null)
            {
                this._sourceId = new List<string>(categoryMapMutable.SourceId);
            }

            if (categoryMapMutable.TargetId != null)
            {
                this._targetId = new List<string>(categoryMapMutable.TargetId);
            }

            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryMapCore" /> class.
        /// </summary>
        /// <param name="catType">
        ///     The cat type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "catType", Justification = "Incomplete code. Waiting for SdmxSource Java to implement functionality.")]
        protected internal CategoryMapCore(CategoryMapType catType, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryMap), parent)
        {
            // FUNC 2.1 - this referernce is wrong as it only allows for a single id
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryMapCore" /> class.
        /// </summary>
        /// <param name="catType">
        ///     The cat type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        protected internal CategoryMapCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.CategoryMapType catType, 
            ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryMap), parent)
        {
            if (catType == null)
            {
                throw new ArgumentNullException("catType");
            }

            this._alias = catType.categoryAlias;
            PopulateCategoryIdList(this._sourceId, catType.CategoryID);
            PopulateCategoryIdList(this._targetId, catType.TargetCategoryID);
            this.Validate();
        }

        /// <summary>
        ///     Gets the alias.
        /// </summary>
        public virtual string Alias
        {
            get
            {
                return this._alias;
            }
        }

        /// <summary>
        ///     Gets the source id.
        /// </summary>
        public virtual IList<string> SourceId
        {
            get
            {
                return new List<string>(this._sourceId);
            }
        }

        /// <summary>
        ///     Gets the target id.
        /// </summary>
        public virtual IList<string> TargetId
        {
            get
            {
                return new List<string>(this._targetId);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ICategoryMap)sdmxObject;
                if (!ObjectUtil.EquivalentCollection(this._sourceId, that.SourceId))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._targetId, that.TargetId))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._alias, that.Alias))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void Validate()
        {
            if (this._sourceId == null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredElement, 
                    "CategoryMap", 
                    "CategoryID");
            }

            if (this._targetId == null)
            {
                throw new SdmxSemmanticException(
                    ExceptionCode.ObjectMissingRequiredElement, 
                    "CategoryMap", 
                    "TargetCategoryID");
            }
        }

        /// <summary>
        ///     The populate category id list.
        /// </summary>
        /// <param name="catIdList">
        ///     The cat id list.
        /// </param>
        /// <param name="currentIdType">
        ///     The current id type.
        /// </param>
        private static void PopulateCategoryIdList(ICollection<string> catIdList, CategoryIDType currentIdType)
        {
            // TODO possibly use RefUtl.GetCateogryIds
            while (currentIdType != null)
            {
                catIdList.Add(currentIdType.ID);
                currentIdType = currentIdType.CategoryID;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}