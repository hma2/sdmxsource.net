// -----------------------------------------------------------------------
// <copyright file="ItemSchemeMapCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Util.Attributes;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The item scheme map core.
    /// </summary>
    [Serializable]
    public abstract class ItemSchemeMapCore : SchemeMapCore, IItemSchemeMapObject
    {
        /// <summary>
        ///     The items.
        /// </summary>
        private readonly IList<IItemMap> _items; // Package Protected

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeMapCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="itemMutableObject"/> is <see langword="null" />.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "structureType", Justification = "Follows the SdmxSource Java version.")]
        protected ItemSchemeMapCore(
            IItemSchemeMapMutableObject itemMutableObject, 
            SdmxStructureType structureType, 
            IIdentifiableObject parent)
            : base(itemMutableObject, parent)
        {
            if (itemMutableObject == null)
            {
                throw new ArgumentNullException("itemMutableObject");
            }

            this._items = new List<IItemMap>();
            if (itemMutableObject.Items != null)
            {
                foreach (IItemMapMutableObject item in itemMutableObject.Items)
                {
                    this._items.Add(new ItemMapCore(item, this));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeMapCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="createdFrom"/> is <see langword="null" />.</exception>
        protected ItemSchemeMapCore(
            NameableType createdFrom, 
            SdmxStructureType structureType, 
            IIdentifiableObject parent)
            : base(createdFrom, structureType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            this._items = new List<IItemMap>();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ItemSchemeMapCore" /> class.
        /// </summary>
        /// <param name="createdFrom">
        ///     The created from.
        /// </param>
        /// <param name="structureType">
        ///     The structure type.
        /// </param>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <param name="uri">
        ///     The uri.
        /// </param>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <param name="description">
        ///     The description.
        /// </param>
        /// <param name="annotationsType">
        ///     The annotations type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="createdFrom"/> is <see langword="null" />.</exception>
        protected ItemSchemeMapCore(
            [ValidatedNotNull]IXmlSerializable createdFrom, 
            SdmxStructureType structureType, 
            string id, 
            Uri uri, 
            IList<TextType> name, 
            IList<TextType> description, 
            AnnotationsType annotationsType, 
            IIdentifiableObject parent)
            : base(createdFrom, structureType, id, uri, name, description, annotationsType, parent)
        {
            if (createdFrom == null)
            {
                throw new ArgumentNullException("createdFrom");
            }

            this._items = new List<IItemMap>();
        }

        /// <summary>
        ///     Gets the items.
        /// </summary>
        public IList<IItemMap> Items
        {
            get
            {
                return new List<IItemMap>(this._items);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IItemSchemeMapObject)sdmxObject;
                if (!this.Equivalent(this._items, that.Items, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Add item.to <see cref="Items" />
        /// </summary>
        /// <param name="item">
        ///     The item.
        /// </param>
        protected void AddInternalItem(IItemMap item)
        {
            this._items.Add(item);
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._items, composites);
            return composites;
        }
    }
}