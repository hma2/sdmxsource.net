// -----------------------------------------------------------------------
// <copyright file="RepresentationMapRefCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Collections;

    using TextFormatType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.TextFormatType;
    using ToValueTypeTypeConstants = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.ToValueTypeTypeConstants;

    /// <summary>
    ///     The representation map ref core.
    /// </summary>
    [Serializable]
    public class RepresentationMapRefCore : SdmxStructureCore, IRepresentationMapRef
    {
        /// <summary>
        ///     The codelist map.
        /// </summary>
        private readonly ICrossReference _codelistMap;

        /// <summary>
        ///     The to text format.
        /// </summary>
        private readonly ITextFormat _toTextFormat;

        /// <summary>
        ///     The to value type.
        /// </summary>
        private readonly ToValue _toValueType;

        /// <summary>
        ///     The value mappings.
        /// </summary>
        private readonly IDictionaryOfSets<string, string> _valueMappings;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationMapRefCore" /> class.
        /// </summary>
        /// <param name="representationMapType">
        ///     The iref.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="representationMapType"/> is <see langword="null" />.</exception>
        public RepresentationMapRefCore(RepresentationMapType representationMapType, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.RepresentationMap), parent)
        {
            if (representationMapType == null)
            {
                throw new ArgumentNullException("representationMapType");
            }

            this._valueMappings = new DictionaryOfSets<string, string>();

            if (representationMapType.CodelistMap != null)
            {
                // Local Reference
                string agencyId = this.MaintainableParent.AgencyId;
                string maintainableId = this.MaintainableParent.Id;
                string version = this.MaintainableParent.Version;
                this._codelistMap = new CrossReferenceImpl(
                    this, 
                    agencyId, 
                    maintainableId, 
                    version, 
                    SdmxStructureEnumType.CodeListMap, 
                    representationMapType.CodelistMap.GetTypedRef<LocalCodelistMapRefType>().id);
            }

            if (representationMapType.ToTextFormat != null)
            {
                this._toTextFormat = new TextFormatObjectCore(representationMapType.ToTextFormat, this);
            }

            if (representationMapType.ToValueType != null)
            {
                switch (representationMapType.ToValueType)
                {
                    case ToValueTypeTypeConstants.Description:
                        this._toValueType = ToValue.Description;
                        break;
                    case ToValueTypeTypeConstants.Name:
                        this._toValueType = ToValue.Name;
                        break;
                    case ToValueTypeTypeConstants.Value:
                        this._toValueType = ToValue.Value;
                        break;
                }
            }

            if (representationMapType.ValueMap != null)
            {
                foreach (ValueMappingType vmt in representationMapType.ValueMap.ValueMapping)
                {
                    ISet<string> mappings;
                    if (!this._valueMappings.TryGetValue(vmt.source, out mappings))
                    {
                        mappings = new HashSet<string>();
                        this._valueMappings.Add(vmt.source, mappings);
                    }

                    mappings.Add(vmt.target);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationMapRefCore" /> class.
        /// </summary>
        /// <param name="textFormatType">
        ///     The text format type.
        /// </param>
        /// <param name="toValueType">
        ///     The to value type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public RepresentationMapRefCore(TextFormatType textFormatType, string toValueType, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.RepresentationMap), parent)
        {
            this._valueMappings = new DictionaryOfSets<string, string>();
            this._toTextFormat = new TextFormatObjectCore(textFormatType, this);
            if (toValueType != null)
            {
                switch (toValueType)
                {
                    case Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ToValueTypeTypeConstants.Description:
                        this._toValueType = ToValue.Description;
                        break;
                    case Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ToValueTypeTypeConstants.Name:
                        this._toValueType = ToValue.Name;
                        break;
                    case Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ToValueTypeTypeConstants.Value:
                        this._toValueType = ToValue.Value;
                        break;
                }
            }

            this.Validate();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationMapRefCore" /> class.
        /// </summary>
        /// <param name="representationMapRefType">
        ///     The iref.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="representationMapRefType"/> is <see langword="null" />.</exception>
        public RepresentationMapRefCore(RepresentationMapRefType representationMapRefType, ISdmxStructure parent)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.RepresentationMap), parent)
        {
            if (representationMapRefType == null)
            {
                throw new ArgumentNullException("representationMapRefType");
            }

            this._valueMappings = new DictionaryOfSets<string, string>();

            this._codelistMap = new CrossReferenceImpl(
                this, 
                representationMapRefType.RepresentationMapAgencyID, 
                representationMapRefType.RepresentationMapID, 
                MaintainableObject.DefaultVersion, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepresentationMapRefCore" /> class.
        /// </summary>
        /// <param name="xref">
        ///     The xref.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected internal RepresentationMapRefCore(IRepresentationMapRefMutableObject xref, ISdmxStructure parent)
            : base(xref, parent)
        {
            this._valueMappings = new DictionaryOfSets<string, string>();
            if (xref.CodelistMap != null)
            {
                this._codelistMap = new CrossReferenceImpl(this, xref.CodelistMap);
            }

            if (xref.ToTextFormat != null)
            {
                this._toTextFormat = new TextFormatObjectCore(xref.ToTextFormat, this);
            }

            if (xref.ValueMappings != null)
            {
                this._valueMappings = new DictionaryOfSets<string, string>(xref.ValueMappings);
            }

            this._toValueType = xref.ToValueType;
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }
        }

        /// <summary>
        ///     Gets the codelist map.
        /// </summary>
        public virtual ICrossReference CodelistMap
        {
            get
            {
                return this._codelistMap;
            }
        }

        /// <summary>
        ///     Gets the to text format.
        /// </summary>
        public virtual ITextFormat ToTextFormat
        {
            get
            {
                return this._toTextFormat;
            }
        }

        /// <summary>
        ///     Gets the to value type.
        /// </summary>
        public virtual ToValue ToValueType
        {
            get
            {
                return this._toValueType;
            }
        }

        /// <summary>
        ///     Gets the value mappings.
        /// </summary>
        public virtual IDictionaryOfSets<string, string> ValueMappings
        {
            get
            {
                return new DictionaryOfSets<string, string>(this._valueMappings);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IRepresentationMapRef)sdmxObject;
                if (!this.Equivalent(this._codelistMap, that.CodelistMap))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._toValueType, that.ToValueType))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._valueMappings.Keys, that.ValueMappings.Keys))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._valueMappings.Values, that.ValueMappings.Values))
                {
                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._toTextFormat, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._codelistMap == null && this._valueMappings.Count == 0 && this._toTextFormat == null)
            {
                throw new SdmxSemmanticException(
                    "RepresentationMapping requires either a codelistMap, ToTextFormat or ValueMap");
            }

            if (this._toTextFormat != null)
            {
                if (this._toValueType == ToValue.Null)
                {
                    throw new SdmxSemmanticException(
                        "For RepresentationMapping, if using ToTextFormat ToValueType is also required");
                }
            }
        }
    }
}