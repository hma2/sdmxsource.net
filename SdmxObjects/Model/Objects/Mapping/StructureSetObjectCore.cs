// -----------------------------------------------------------------------
// <copyright file="StructureSetObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping
{
    using System;
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The structure set object core.
    /// </summary>
    [Serializable]
    public class StructureSetObjectCore : MaintainableObjectCore<IStructureSetObject, IStructureSetMutableObject>, 
                                          IStructureSetObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(StructureSetObjectCore));

        /// <summary>
        ///     The category scheme map list.
        /// </summary>
        private readonly IList<ICategorySchemeMapObject> _categorySchemeMapList = new List<ICategorySchemeMapObject>();

        /// <summary>
        ///     The codelist map list.
        /// </summary>
        private readonly IList<ICodelistMapObject> _codelistMapList = new List<ICodelistMapObject>();

        /// <summary>
        ///     The concept scheme map list.
        /// </summary>
        private readonly IList<IConceptSchemeMapObject> _conceptSchemeMapList = new List<IConceptSchemeMapObject>();

        /// <summary>
        ///     The organisation scheme map list.
        /// </summary>
        private readonly IList<IOrganisationSchemeMapObject> _organisationSchemeMapList = new List<IOrganisationSchemeMapObject>();

        /// <summary>
        ///     The related structures.
        /// </summary>
        private readonly IRelatedStructures _relatedStructures;

        /// <summary>
        ///     The structure map list.
        /// </summary>
        private readonly IList<IStructureMapObject> _structureMapList = new List<IStructureMapObject>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetObjectCore" /> class.
        /// </summary>
        /// <param name="structureSet">
        ///     The structure set.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public StructureSetObjectCore(IStructureSetMutableObject structureSet)
            : base(structureSet)
        {
            _log.Debug("Building IStructureSetObject from Mutable Object");
            try
            {
                if (structureSet.RelatedStructures != null)
                {
                    this._relatedStructures = new RelatedStructuresCore(structureSet.RelatedStructures, this);
                }

                this.BuildLists(structureSet);
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("IStructureSetObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetObjectCore" /> class.
        /// </summary>
        /// <param name="structureSet">
        ///     The structure set.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public StructureSetObjectCore(StructureSetType structureSet)
            : base(structureSet, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet))
        {
            _log.Debug("Building IStructureSetObject from 2.1 SDMX");
            try
            {
                if (ObjectUtil.ValidCollection(structureSet.RelatedStructure))
                {
                    this._relatedStructures = new RelatedStructuresCore(structureSet.RelatedStructure, this);
                }

                this.BuildLists(structureSet);
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("IStructureSetObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetObjectCore" /> class.
        /// </summary>
        /// <param name="structureSet">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="structureSet"/> is <see langword="null" />.</exception>
        public StructureSetObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.StructureSetType structureSet)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet), 
                structureSet.PassNoNull("structureSet").validTo, 
                structureSet.PassNoNull("structureSet").validFrom, 
                structureSet.PassNoNull("structureSet").version, 
                CreateTertiary(structureSet.PassNoNull("structureSet").isFinal), 
                structureSet.PassNoNull("structureSet").agencyID, 
                structureSet.PassNoNull("structureSet").id, 
                structureSet.PassNoNull("structureSet").uri, 
                structureSet.PassNoNull("structureSet").Name, 
                structureSet.PassNoNull("structureSet").Description, 
                CreateTertiary(structureSet.PassNoNull("structureSet").isExternalReference), 
                structureSet.PassNoNull("structureSet").Annotations)
        {
            if (structureSet == null)
            {
                throw new ArgumentNullException("structureSet");
            }

            _log.Debug("Building IStructureSetObject from 2.0 SDMX");
            try
            {
                if (structureSet.RelatedStructures != null)
                {
                    this._relatedStructures = new RelatedStructuresCore(structureSet.RelatedStructures, this);
                }

                if (structureSet.StructureMap != null)
                {
                    this._structureMapList = new List<IStructureMapObject>();
                    this._structureMapList.Add(new StructureMapCore(structureSet.StructureMap, this));
                }

                if (structureSet.CodelistMap != null)
                {
                    this._codelistMapList = new List<ICodelistMapObject>();
                    this._codelistMapList.Add(new CodelistMapCore(structureSet.CodelistMap, this));
                }

                if (structureSet.CategorySchemeMap != null)
                {
                    this._categorySchemeMapList = new List<ICategorySchemeMapObject>();
                    this._categorySchemeMapList.Add(new CategorySchemeMapCore(structureSet.CategorySchemeMap, this));
                }

                if (structureSet.ConceptSchemeMap != null)
                {
                    this._conceptSchemeMapList = new List<IConceptSchemeMapObject>();
                    this._conceptSchemeMapList.Add(new ConceptSchemeMapCore(structureSet.ConceptSchemeMap, this));
                }

                if (structureSet.OrganisationSchemeMap != null)
                {
                    this._organisationSchemeMapList = new List<IOrganisationSchemeMapObject>();
                    this._organisationSchemeMapList.Add(new OrganisationSchemeMapCore(structureSet.OrganisationSchemeMap, this));
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (_log.IsDebugEnabled)
            {
                _log.Debug("IStructureSetObject Built " + this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureSetObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private StructureSetObjectCore(IStructureSetObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            _log.Debug("Stub IStructureSetObject Built");
        }

        /// <summary>
        ///     Gets the category scheme map list.
        /// </summary>
        public virtual IList<ICategorySchemeMapObject> CategorySchemeMapList
        {
            get
            {
                var list = new List<ICategorySchemeMapObject>();

                foreach (ICategorySchemeMapObject each in this._categorySchemeMapList)
                {
                    list.Add(each);
                }

                return list;
            }
        }

        /// <summary>
        ///     Gets the codelist map list.
        /// </summary>
        public virtual IList<ICodelistMapObject> CodelistMapList
        {
            get
            {
                var list = new List<ICodelistMapObject>();

                foreach (ICodelistMapObject each in this._codelistMapList)
                {
                    list.Add(each);
                }

                return list;
            }
        }

        /// <summary>
        ///     Gets the concept scheme map list.
        /// </summary>
        public virtual IList<IConceptSchemeMapObject> ConceptSchemeMapList
        {
            get
            {
                var list = new List<IConceptSchemeMapObject>();

                foreach (IConceptSchemeMapObject each in this._conceptSchemeMapList)
                {
                    list.Add(each);
                }

                return list;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IStructureSetMutableObject MutableInstance
        {
            get
            {
                return new StructureSetMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the organisation scheme map list.
        /// </summary>
        public virtual IList<IOrganisationSchemeMapObject> OrganisationSchemeMapList
        {
            get
            {
                return new List<IOrganisationSchemeMapObject>(this._organisationSchemeMapList);
            }
        }

        /// <summary>
        ///     Gets the related structures.
        /// </summary>
        public virtual IRelatedStructures RelatedStructures
        {
            get
            {
                return this._relatedStructures;
            }
        }

        /// <summary>
        ///     Gets the structure map list.
        /// </summary>
        public virtual IList<IStructureMapObject> StructureMapList
        {
            get
            {
                var list = new List<IStructureMapObject>();

                foreach (IStructureMapObject each in this._structureMapList)
                {
                    list.Add(each);
                }

                return list;
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IStructureSetObject)sdmxObject;
                if (!this.Equivalent(this._relatedStructures, that.RelatedStructures, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._structureMapList, that.StructureMapList, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._codelistMapList, that.CodelistMapList, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._categorySchemeMapList, that.CategorySchemeMapList, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._conceptSchemeMapList, that.ConceptSchemeMapList, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._organisationSchemeMapList, that.OrganisationSchemeMapList, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsMaintainable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IStructureSetObject" /> .
        /// </returns>
        public override IStructureSetObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new StructureSetObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._relatedStructures, composites);
            this.AddToCompositeSet(this._structureMapList, composites);
            this.AddToCompositeSet(this._codelistMapList, composites);
            this.AddToCompositeSet(this._categorySchemeMapList, composites);
            this.AddToCompositeSet(this._conceptSchemeMapList, composites);
            this.AddToCompositeSet(this._organisationSchemeMapList, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        private void Validate()
        {
        }

        /// <summary>
        /// Builds the lists.
        /// </summary>
        /// <param name="structureSet">The structure set.</param>
        private void BuildLists(IStructureSetMutableObject structureSet)
        {
            if (structureSet.StructureMapList != null)
            {
                foreach (IStructureMapMutableObject each in structureSet.StructureMapList)
                {
                    this._structureMapList.Add(new StructureMapCore(each, this));
                }
            }

            if (structureSet.CodelistMapList != null)
            {
                foreach (ICodelistMapMutableObject each0 in structureSet.CodelistMapList)
                {
                    this._codelistMapList.Add(new CodelistMapCore(each0, this));
                }
            }

            if (structureSet.CategorySchemeMapList != null)
            {
                foreach (ICategorySchemeMapMutableObject each1 in structureSet.CategorySchemeMapList)
                {
                    this._categorySchemeMapList.Add(new CategorySchemeMapCore(each1, this));
                }
            }

            if (structureSet.ConceptSchemeMapList != null)
            {
                foreach (IConceptSchemeMapMutableObject each2 in structureSet.ConceptSchemeMapList)
                {
                    this._conceptSchemeMapList.Add(new ConceptSchemeMapCore(each2, this));
                }
            }

            if (structureSet.OrganisationSchemeMapList != null)
            {
                foreach (IOrganisationSchemeMapMutableObject each3 in structureSet.OrganisationSchemeMapList)
                {
                    this._organisationSchemeMapList.Add(new OrganisationSchemeMapCore(each3, this));
                }
            }
        }

        /// <summary>
        /// Builds the lists.
        /// </summary>
        /// <param name="structureSet">The structure set.</param>
        private void BuildLists(StructureSetType structureSet)
        {
            if (structureSet.StructureMap != null)
            {
                foreach (StructureMapType each in structureSet.StructureMap)
                {
                    this._structureMapList.Add(new StructureMapCore(each, this));
                }
            }

            if (structureSet.CodelistMap != null)
            {
                foreach (CodelistMapType each0 in structureSet.CodelistMap)
                {
                    this._codelistMapList.Add(new CodelistMapCore(each0, this));
                }
            }

            if (structureSet.CategorySchemeMap != null)
            {
                foreach (CategorySchemeMapType each1 in structureSet.CategorySchemeMap)
                {
                    this._categorySchemeMapList.Add(new CategorySchemeMapCore(each1, this));
                }
            }

            if (structureSet.ConceptSchemeMap != null)
            {
                foreach (ConceptSchemeMapType each2 in structureSet.ConceptSchemeMap)
                {
                    this._conceptSchemeMapList.Add(new ConceptSchemeMapCore(each2, this));
                }
            }

            if (structureSet.OrganisationSchemeMap != null)
            {
                foreach (OrganisationSchemeMapType each3 in structureSet.OrganisationSchemeMap)
                {
                    this._organisationSchemeMapList.Add(new OrganisationSchemeMapCore(each3, this));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}