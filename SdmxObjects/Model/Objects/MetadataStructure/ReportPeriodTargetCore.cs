// -----------------------------------------------------------------------
// <copyright file="ReportPeriodTargetCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;

    using ReportPeriodTarget = Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant.ReportPeriodTarget;

    /// <summary>
    ///     The report period target core.
    /// </summary>
    public class ReportPeriodTargetCore : IdentifiableCore, IReportPeriodTarget
    {
        /// <summary>
        ///     The end time.
        /// </summary>
        private readonly ISdmxDate _endTime;

        /// <summary>
        ///     The start time.
        /// </summary>
        private readonly ISdmxDate _startTime;

        /// <summary>
        ///     The text type.
        /// </summary>
        private readonly TextType _textType;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportPeriodTargetCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportPeriodTargetCore(IIdentifiableObject parent, IReportPeriodTargetMutableObject itemMutableObject)
            : base(itemMutableObject, parent)
        {
            this._textType = TextType.GetFromEnum(TextEnumType.ObservationalTimePeriod);
            if (itemMutableObject.StartTime != null)
            {
                this._startTime = new SdmxDateCore(itemMutableObject.StartTime, TimeFormatEnumType.DateTime);
            }

            if (itemMutableObject.EndTime != null)
            {
                this._endTime = new SdmxDateCore(itemMutableObject.EndTime, TimeFormatEnumType.DateTime);
            }

            if (itemMutableObject.TextType != null)
            {
                this._textType = itemMutableObject.TextType;
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportPeriodTargetCore" /> class.
        /// </summary>
        /// <param name="reportPeriodTargetType">
        ///     The report period target type.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        protected internal ReportPeriodTargetCore(ReportPeriodTargetType reportPeriodTargetType, IMetadataTarget parent)
            : base(
                reportPeriodTargetType, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportPeriodTarget), 
                parent)
        {
            this._textType = TextType.GetFromEnum(TextEnumType.ObservationalTimePeriod);
            if (reportPeriodTargetType.LocalRepresentation != null)
            {
                RepresentationType repType = reportPeriodTargetType.LocalRepresentation;
                if (repType.TextFormat != null)
                {
                    if (repType.TextFormat.startTime != null)
                    {
                        this._startTime = new SdmxDateCore(repType.TextFormat.startTime.ToString());
                    }

                    if (repType.TextFormat.endTime != null)
                    {
                        this._endTime = new SdmxDateCore(repType.TextFormat.endTime.ToString());
                    }

                    if (repType.TextFormat.textType != null)
                    {
                        this._textType = TextTypeUtil.GetTextType(repType.TextFormat.textType);
                    }
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the end time.
        /// </summary>
        public virtual ISdmxDate EndTime
        {
            get
            {
                return this._endTime;
            }
        }

        /// <summary>
        ///     Gets the id.
        /// </summary>
        public override string Id
        {
            get
            {
                return ReportPeriodTarget.FixedId;
            }
        }

        /// <summary>
        ///     Gets the start time.
        /// </summary>
        public virtual ISdmxDate StartTime
        {
            get
            {
                return this._startTime;
            }
        }

        /// <summary>
        ///     Gets the text type.
        /// </summary>
        public virtual TextType TextType
        {
            get
            {
                return this._textType;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject != null && sdmxObject.StructureType == this.StructureType)
            {
                var that = (IReportPeriodTarget)sdmxObject;
                if (!ObjectUtil.Equivalent(this._startTime, that.StartTime))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._endTime, that.EndTime))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            this.OriginalId = ReportPeriodTarget.FixedId;
            if (this._startTime != null && this._endTime != null)
            {
                if (this._startTime.IsLater(this._endTime))
                {
                    throw new SdmxSemmanticException("Report Period Target - start time can not be later then end time");
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}