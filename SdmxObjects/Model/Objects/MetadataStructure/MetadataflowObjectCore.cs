// -----------------------------------------------------------------------
// <copyright file="MetadataflowObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure
{
    using System;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    using MetadataStructureRefType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataStructureRefType;

    /// <summary>
    ///     The metadataflow object core.
    /// </summary>
    [Serializable]
    public class MetadataflowObjectCore : MaintainableObjectCore<IMetadataFlow, IMetadataFlowMutableObject>, 
                                          IMetadataFlow
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(MetadataflowObjectCore));

        /// <summary>
        ///     The metadata structure ref.
        /// </summary>
        private readonly ICrossReference _metadataStructureRef;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataflowObjectCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        public MetadataflowObjectCore(IMetadataFlowMutableObject itemMutableObject)
            : base(itemMutableObject)
        {
            Log.Debug("Building IMetadataFlow from Mutable Object");
            if (itemMutableObject.MetadataStructureRef != null)
            {
                this._metadataStructureRef = new CrossReferenceImpl(this, itemMutableObject.MetadataStructureRef);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IMetadataFlow Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataflowObjectCore" /> class.
        /// </summary>
        /// <param name="metadataflow">
        ///     The sdmxObject.
        /// </param>
        public MetadataflowObjectCore(MetadataflowType metadataflow)
            : base(metadataflow, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow))
        {
            Log.Debug("Building IMetadataFlow from 2.1 SDMX");
            var dataStructureReferenceType = metadataflow.GetStructure<MetadataStructureReferenceType>();
            if (!metadataflow.isExternalReference)
            {
                if (dataStructureReferenceType != null)
                {
                    this._metadataStructureRef = RefUtil.CreateReference(this, dataStructureReferenceType);
                }
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IMetadataFlow Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataflowObjectCore" /> class.
        /// </summary>
        /// <param name="metadataflow">
        ///     The sdmxObject.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="metadataflow"/> is <see langword="null" />.</exception>
        public MetadataflowObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataflowType metadataflow)
            : base(
                SdmxStructureType.GetFromEnum(
                SdmxStructureEnumType.MetadataFlow), 
                metadataflow.PassNoNull("metadataflow").validTo, 
                metadataflow.PassNoNull("metadataflow").validFrom, 
                metadataflow.PassNoNull("metadataflow").version, 
                CreateTertiary(metadataflow.PassNoNull("metadataflow").isFinal), 
                metadataflow.PassNoNull("metadataflow").agencyID, 
                metadataflow.PassNoNull("metadataflow").id, 
                metadataflow.PassNoNull("metadataflow").uri, 
                metadataflow.PassNoNull("metadataflow").Name, 
                metadataflow.PassNoNull("metadataflow").Description, 
                CreateTertiary(metadataflow.PassNoNull("metadataflow").isExternalReference), 
                metadataflow.PassNoNull("metadataflow").Annotations)
        {
            if (metadataflow == null)
            {
                throw new ArgumentNullException("metadataflow");
            }

            Log.Debug("Building IMetadataFlow from 2.0 SDMX");
            if (metadataflow.MetadataStructureRef != null)
            {
                MetadataStructureRefType xref = metadataflow.MetadataStructureRef;
                if (xref.URN != null)
                {
                    this._metadataStructureRef = new CrossReferenceImpl(this, xref.URN);
                }
                else
                {
                    this._metadataStructureRef = new CrossReferenceImpl(
                        this, 
                        xref.MetadataStructureAgencyID, 
                        xref.MetadataStructureID, 
                        xref.Version, 
                        SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd));
                }
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IMetadataFlow Built " + this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataflowObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The sdmxObject.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private MetadataflowObjectCore(IMetadataFlow agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            Log.Debug("Stub IMetadataFlow Built");
        }

        /// <summary>
        ///     Gets the metadata structure ref.
        /// </summary>
        public virtual ICrossReference MetadataStructureRef
        {
            get
            {
                return this._metadataStructureRef;
            }
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IMetadataFlowMutableObject MutableInstance
        {
            get
            {
                return new MetadataflowMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject != null && sdmxObject.StructureType == this.StructureType)
            {
                var that = (IMetadataFlow)sdmxObject;
                if (!this.Equivalent(this._metadataStructureRef, that.MetadataStructureRef))
                {
                    return false;
                }

                return this.DeepEqualsMaintainable(that, includeFinalProperties);
            }

            return false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////GETTERS                                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IMetadataFlow" /> .
        /// </returns>
        public override IMetadataFlow GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new MetadataflowObjectCore(this, actualLocation, isServiceUrl);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////
    }
}