// -----------------------------------------------------------------------
// <copyright file="ReportStructureCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using MetadataAttribute = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.MetadataAttribute;
    using MetadataAttributeType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataAttributeType;

    /// <summary>
    ///     The report structure core.
    /// </summary>
    [Serializable]
    public class ReportStructureCore : IdentifiableCore, IReportStructure
    {
        /// <summary>
        ///     The metadata attributes.
        /// </summary>
        private readonly IList<IMetadataAttributeObject> _metadataAttributes;

        /// <summary>
        ///     The target metadatas.
        /// </summary>
        private readonly IList<string> _targetMetadatas;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportStructureCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="rs">
        ///     The rs.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportStructureCore(IMetadataStructureDefinitionObject parent, IReportStructureMutableObject rs)
            : base(rs, parent)
        {
            this._metadataAttributes = new List<IMetadataAttributeObject>();
            this._targetMetadatas = new List<string>();
            try
            {
                if (rs.MetadataAttributes != null)
                {
                    foreach (IMetadataAttributeMutableObject currentMa in rs.MetadataAttributes)
                    {
                        this._metadataAttributes.Add(new MetadataAttributeObjectCore(this, currentMa));
                    }
                }

                if (rs.TargetMetadatas != null)
                {
                    this._targetMetadatas = new List<string>(rs.TargetMetadatas);
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportStructureCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="rs">
        ///     The rs.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ReportStructureCore(IMetadataStructureDefinitionObject parent, ReportStructureType rs)
            : base(rs, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportStructure), parent)
        {
            this._metadataAttributes = new List<IMetadataAttributeObject>();
            this._targetMetadatas = new List<string>();
            try
            {
                foreach (MetadataAttribute currentMa in rs.Component)
                {
                    this._metadataAttributes.Add(new MetadataAttributeObjectCore(currentMa.Content, this));
                }

                if (rs.MetadataTarget != null)
                {
                    foreach (LocalMetadataTargetReferenceType localReference in rs.MetadataTarget)
                    {
                        this._targetMetadatas.Add(RefUtil.CreateLocalIdReference(localReference));
                    }
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ReportStructureCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="rs">
        ///     The rs.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="rs"/> is <see langword="null" />.</exception>
        public ReportStructureCore(
            IMetadataStructureDefinitionObject parent, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ReportStructureType rs)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportStructure), 
                rs.PassNoNull("rs").id, 
                rs.PassNoNull("rs").uri, 
                rs.PassNoNull("rs").Annotations, 
                parent)
        {
            if (rs == null)
            {
                throw new ArgumentNullException("rs");
            }

            this._metadataAttributes = new List<IMetadataAttributeObject>();
            this._targetMetadatas = new List<string>();
            try
            {
                foreach (MetadataAttributeType currentMa in rs.MetadataAttribute)
                {
                    this._metadataAttributes.Add(new MetadataAttributeObjectCore(this, currentMa));
                }

                if (!string.IsNullOrWhiteSpace(rs.target))
                {
                    this._targetMetadatas.Add(rs.target);
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the metadata attributes.
        /// </summary>
        public virtual IList<IMetadataAttributeObject> MetadataAttributes
        {
            get
            {
                return new List<IMetadataAttributeObject>(this._metadataAttributes);
            }
        }

        /// <summary>
        ///     Gets the target metadatas.
        /// </summary>
        public virtual IList<string> TargetMetadatas
        {
            get
            {
                return new List<string>(this._targetMetadatas);
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject != null && sdmxObject.StructureType == this.StructureType)
            {
                var that = (IReportStructure)sdmxObject;
                if (!this.Equivalent(this._metadataAttributes, that.MetadataAttributes, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.EquivalentCollection(this._targetMetadatas, that.TargetMetadatas))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._metadataAttributes, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (!ObjectUtil.ValidCollection(this._metadataAttributes))
            {
                throw new SdmxSemmanticException(
                    "Report CategorisationStructure requires at least one Metadata Attribute");
            }

            if (!ObjectUtil.ValidCollection(this._targetMetadatas))
            {
                throw new SdmxSemmanticException("Report CategorisationStructure requires at least one Metadata Target");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////
    }
}