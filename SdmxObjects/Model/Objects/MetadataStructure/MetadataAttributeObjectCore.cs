// -----------------------------------------------------------------------
// <copyright file="MetadataAttributeObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using UsageStatusTypeConstants = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.UsageStatusTypeConstants;

    /// <summary>
    ///     The metadata attributeObject core.
    /// </summary>
    public class MetadataAttributeObjectCore : ComponentCore, IMetadataAttributeObject
    {
        /// <summary>
        ///     The metadata attributes.
        /// </summary>
        private readonly IList<IMetadataAttributeObject> _metadataAttributes;

        /// <summary>
        ///     The presentational.
        /// </summary>
        private readonly TertiaryBool _presentational;

        /// <summary>
        ///     The max occurs.
        /// </summary>
        private int? _maxOccurs;

        /// <summary>
        ///     The min occurs.
        /// </summary>
        private int? _minOccurs;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataAttributeObjectCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public MetadataAttributeObjectCore(
            IIdentifiableObject parent, 
            IMetadataAttributeMutableObject itemMutableObject)
            : base(itemMutableObject, parent)
        {
            this._metadataAttributes = new List<IMetadataAttributeObject>();
            this._presentational = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
            try
            {
                if (itemMutableObject.MetadataAttributes != null)
                {
                    foreach (IMetadataAttributeMutableObject currentMa in itemMutableObject.MetadataAttributes)
                    {
                        this._metadataAttributes.Add(new MetadataAttributeObjectCore(this, currentMa));
                    }
                }

                if (itemMutableObject.MinOccurs != null)
                {
                    this._minOccurs = itemMutableObject.MinOccurs;
                }

                if (itemMutableObject.MaxOccurs != null)
                {
                    this._maxOccurs = itemMutableObject.MaxOccurs;
                }

                this._presentational = itemMutableObject.Presentational;
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException("IsError creating structure: " + this, th);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataAttributeObjectCore" /> class.
        /// </summary>
        /// <param name="metadataAttribute">
        ///     The metadata attributeObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public MetadataAttributeObjectCore(MetadataAttributeType metadataAttribute, IIdentifiableObject parent)
            : base(metadataAttribute, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttribute), parent)
        {
            this._metadataAttributes = new List<IMetadataAttributeObject>();
            this._presentational = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
            this._minOccurs = (metadataAttribute.minOccurs < int.MaxValue)
                                 ? decimal.ToInt32(metadataAttribute.minOccurs)
                                 : (int?)null;

            if (metadataAttribute.maxOccurs != null)
            {
                long res;
                if (long.TryParse(metadataAttribute.maxOccurs.ToString(), out res))
                {
                    this._maxOccurs = Convert.ToInt32(res);
                }
                else
                {
                    this._maxOccurs = null; // unbounded 
                }
            }

            if (metadataAttribute.isPresentational)
            {
                this._presentational = TertiaryBool.ParseBoolean(metadataAttribute.isPresentational);
            }

            if (metadataAttribute.MetadataAttribute != null)
            {
                foreach (MetadataAttribute currentMaType in metadataAttribute.MetadataAttribute)
                {
                    this._metadataAttributes.Add(new MetadataAttributeObjectCore(currentMaType.Content, this));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="MetadataAttributeObjectCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="metadataAttribute">
        ///     The metadata attributeObject.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public MetadataAttributeObjectCore(
            IIdentifiableObject parent, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataAttributeType metadataAttribute)
            : base(
                metadataAttribute, 
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataAttribute), 
                metadataAttribute.PassNoNull("metadataAttribute").Annotations, 
                metadataAttribute.PassNoNull("metadataAttribute").TextFormat, 
                metadataAttribute.PassNoNull("metadataAttribute").representationSchemeAgency, 
                metadataAttribute.PassNoNull("metadataAttribute").representationScheme, 
                metadataAttribute.PassNoNull("metadataAttribute").RepresentationSchemeVersionEstat, 
                metadataAttribute.PassNoNull("metadataAttribute").conceptSchemeAgency, 
                metadataAttribute.PassNoNull("metadataAttribute").conceptSchemeRef, 
                GetConceptSchemeVersion(metadataAttribute), 
                metadataAttribute.PassNoNull("metadataAttribute").conceptAgency, 
                metadataAttribute.PassNoNull("metadataAttribute").conceptRef, 
                parent)
        {
            this._metadataAttributes = new List<IMetadataAttributeObject>();
            this._presentational = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);

            if (metadataAttribute.usageStatus != null)
            {
                if (metadataAttribute.usageStatus == UsageStatusTypeConstants.Mandatory)
                {
                    this._minOccurs = 1;
                    this._maxOccurs = 1;
                }
                else
                {
                    this._minOccurs = 0;
                }
            }

            if (metadataAttribute.MetadataAttribute != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataAttributeType currentMa in
                    metadataAttribute.MetadataAttribute)
                {
                    this._metadataAttributes.Add(new MetadataAttributeObjectCore(this, currentMa));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the max occurs.
        /// </summary>
        public virtual int? MaxOccurs
        {
            get
            {
                return this._maxOccurs;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets the metadata attributes.
        /// </summary>
        public virtual IList<IMetadataAttributeObject> MetadataAttributes
        {
            get
            {
                return new List<IMetadataAttributeObject>(this._metadataAttributes);
            }
        }

        /// <summary>
        ///     Gets the min occurs.
        /// </summary>
        public virtual int? MinOccurs
        {
            get
            {
                if (this._minOccurs == null)
                {
                    return new int() /* was: null */;
                }

                return this._minOccurs;
            }
        }

        /// <summary>
        ///     Gets the presentational.
        /// </summary>
        public virtual TertiaryBool Presentational
        {
            get
            {
                return this._presentational;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject != null && sdmxObject.StructureType == this.StructureType)
            {
                var that = (IMetadataAttributeObject)sdmxObject;
                if (!this.Equivalent(this._metadataAttributes, that.MetadataAttributes, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._minOccurs, that.MinOccurs))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._maxOccurs, that.MaxOccurs))
                {
                    return false;
                }

                if (this._presentational != that.Presentational)
                {
                    return false;
                }

                return this.DeepEqualsComponent(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        protected internal void Validate()
        {
            if (this._metadataAttributes != null)
            {
                ISet<string> conceptIds = new HashSet<string>();

                foreach (IMetadataAttributeObject metadataAttribute in this._metadataAttributes)
                {
                    if (metadataAttribute.ConceptRef == null)
                    {
                        throw new SdmxSemmanticException("Metadata Attribute must reference a concept");
                    }

                    string ids = metadataAttribute.Id;
                    if (conceptIds.Contains(ids))
                    {
                        throw new SdmxSemmanticException(ExceptionCode.DuplicateConcept, metadataAttribute.ToString());
                    }

                    conceptIds.Add(ids);
                }
            }

            if (this._minOccurs != null && this._maxOccurs != null)
            {
                if (this._minOccurs.Value.CompareTo(this._maxOccurs.Value) > 0)
                {
                    throw new SdmxSemmanticException(
                        "Max Occurs '" + this._maxOccurs + "' can not be a lower value then Min Occurs '"
                        + this._minOccurs
                        + "'.  Please note the abscence of Max Occurs defaults to value of 1 - specify the value as 'unbounded' is this is not the case.");
                }
            }
        }

        /// <summary>
        ///     The get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._metadataAttributes, composites);
            return composites;
        }

        /// <summary>
        ///     Returns concept scheme version. It tries to detect various conventions
        /// </summary>
        /// <param name="attributeType">
        ///     The attribute Type.
        /// </param>
        /// <returns>
        ///     The concept scheme version; otherwise null
        /// </returns>
        private static string GetConceptSchemeVersion(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.MetadataAttributeType attributeType)
        {
            if (!string.IsNullOrWhiteSpace(attributeType.conceptVersion))
            {
                return attributeType.conceptVersion;
            }

            if (!string.IsNullOrWhiteSpace(attributeType.ConceptSchemeVersionEstat))
            {
                return attributeType.ConceptSchemeVersionEstat;
            }

            var extDimension =
                attributeType as Org.Sdmx.Resources.SdmxMl.Schemas.V20.extension.structure.MetadataAttributeType;
            if (extDimension != null && !string.IsNullOrWhiteSpace(extDimension.conceptSchemeVersion))
            {
                return extDimension.conceptSchemeVersion;
            }

            return null;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATION                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECT                 //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////    
    }
}