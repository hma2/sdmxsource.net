// -----------------------------------------------------------------------
// <copyright file="ProcessObjectCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process
{
    using System;
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The process object core.
    /// </summary>
    [Serializable]
    public class ProcessObjectCore : MaintainableObjectCore<IProcessObject, IProcessMutableObject>, IProcessObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(ProcessObjectCore));

        /// <summary>
        ///     The _process steps.
        /// </summary>
        private readonly IList<IProcessStepObject> _processSteps;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessObjectCore" /> class.
        /// </summary>
        /// <param name="processMutableObject">
        ///     The iprocess.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ProcessObjectCore(IProcessMutableObject processMutableObject)
            : base(processMutableObject)
        {
            this._processSteps = new List<IProcessStepObject>();
            Log.Debug("Building IProcessObject from Mutable Object");
            try
            {
                if (processMutableObject.ProcessSteps != null)
                {
                    foreach (IProcessStepMutableObject processStep in processMutableObject.ProcessSteps)
                    {
                        this._processSteps.Add(new ProcessStepCore(this, processStep));
                    }
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IProcessObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessObjectCore" /> class.
        /// </summary>
        /// <param name="process">
        ///     The process.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ProcessObjectCore(ProcessType process)
            : base(process, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Process))
        {
            this._processSteps = new List<IProcessStepObject>();
            Log.Debug("Building IProcessObject from 2.1 SDMX");
            try
            {
                if (process.ProcessStep != null)
                {
                    foreach (ProcessStepType processStep in process.ProcessStep)
                    {
                        this._processSteps.Add(new ProcessStepCore(this, processStep));
                    }
                }
            }
            catch (Exception th)
            {
                throw new SdmxSemmanticException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IProcessObject Built " + this.Urn);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessObjectCore" /> class.
        /// </summary>
        /// <param name="process">
        ///     The agencyScheme.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws SdmxSemmanticException.
        /// </exception>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="process"/> is <see langword="null" />.</exception>
        public ProcessObjectCore(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ProcessType process)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Process), 
                process.PassNoNull("process").validTo, 
                process.PassNoNull("process").validFrom, 
                process.PassNoNull("process").version, 
                CreateTertiary(process.PassNoNull("process").isFinal), 
                process.PassNoNull("process").agencyID, 
                process.PassNoNull("process").id, 
                process.PassNoNull("process").uri, 
                process.PassNoNull("process").Name, 
                process.PassNoNull("process").Description, 
                CreateTertiary(process.PassNoNull("process").isExternalReference), 
                process.PassNoNull("process").Annotations)
        {
            if (process == null)
            {
                throw new ArgumentNullException("process");
            }

            this._processSteps = new List<IProcessStepObject>();
            Log.Debug("Building IProcessObject from 2.0 SDMX");
            try
            {
                if (process.ProcessStep != null)
                {
                    foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ProcessStepType processStep in
                        process.ProcessStep)
                    {
                        this._processSteps.Add(new ProcessStepCore(this, processStep));
                    }
                }
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this.Urn);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }

            if (Log.IsDebugEnabled)
            {
                Log.Debug("IProcessObject Built " + this.Urn);
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessObjectCore" /> class.
        /// </summary>
        /// <param name="agencyScheme">
        ///     The agencyScheme.
        /// </param>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        private ProcessObjectCore(IProcessObject agencyScheme, Uri actualLocation, bool isServiceUrl)
            : base(agencyScheme, actualLocation, isServiceUrl)
        {
            this._processSteps = new List<IProcessStepObject>();
            Log.Debug("Stub IProcessObject Built");
        }

        /// <summary>
        ///     Gets the mutable instance.
        /// </summary>
        public override IProcessMutableObject MutableInstance
        {
            get
            {
                return new ProcessMutableCore(this);
            }
        }

        /// <summary>
        ///     Gets the process steps.
        /// </summary>
        public virtual IList<IProcessStepObject> ProcessSteps
        {
            get
            {
                return new List<IProcessStepObject>(this._processSteps);
            }
        }

        /// <summary>
        ///     Gets the Urn
        /// </summary>
        public override sealed Uri Urn
        {
            get
            {
                return base.Urn;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The agencyScheme.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IProcessObject)sdmxObject;
                if (!this.Equivalent(this._processSteps, that.ProcessSteps, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsMaintainable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     The get stub.
        /// </summary>
        /// <param name="actualLocation">
        ///     The actual location.
        /// </param>
        /// <param name="isServiceUrl">
        ///     The is service url.
        /// </param>
        /// <returns>
        ///     The <see cref="IProcessObject" /> .
        /// </returns>
        public override IProcessObject GetStub(Uri actualLocation, bool isServiceUrl)
        {
            return new ProcessObjectCore(this, actualLocation, isServiceUrl);
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._processSteps, composites);
            return composites;
        }

        /// <summary>
        ///     The populate process step sets.
        /// </summary>
        /// <param name="processSteps0">
        ///     The process steps 0.
        /// </param>
        /// <param name="processStepIds">
        ///     The process step ids.
        /// </param>
        /// <param name="processStepReferences">
        ///     The process step references.
        /// </param>
        private static void PopulateProcessStepSets(
            IList<IProcessStepObject> processSteps0, 
            ISet<string> processStepIds, 
            ISet<string> processStepReferences)
        {
            var stack = new Stack<IList<IProcessStepObject>>();
            stack.Push(processSteps0);
            while (stack.Count > 0)
            {
                processSteps0 = stack.Pop();
                foreach (IProcessStepObject processStep in processSteps0)
                {
                    foreach (ITransition transition in processStep.Transitions)
                    {
                        processStepReferences.Add(transition.TargetStep.Id);
                    }

                    processStepIds.Add(processStep.GetFullIdPath(false));
                    stack.Push(processStep.ProcessSteps);
                }
            }
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            ISet<string> processStepReferences = new HashSet<string>(); // Process Step References
            ISet<string> processStepIds = new HashSet<string>(); // Process Step Ids

            PopulateProcessStepSets(this._processSteps, processStepIds, processStepReferences);
            if (!processStepIds.IsSupersetOf(processStepReferences))
            {
                foreach (string currentReference in processStepReferences)
                {
                    if (!processStepIds.Contains(currentReference))
                    {
                        throw new SdmxSemmanticException(
                            "Transition references non existent process step '" + currentReference + "'");
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS             //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////    

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////BUILD FROM ITSELF, CREATES STUB OBJECT //////////////////////////////////////////////////
    }
}