// -----------------------------------------------------------------------
// <copyright file="ComputationCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;

    using ProcessStepType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ProcessStepType;

    /// <summary>
    ///     The computation core.
    /// </summary>
    [Serializable]
    public class ComputationCore : AnnotableCore, IComputationObject
    {
        /// <summary>
        ///     The description.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _description;

        /// <summary>
        ///     The local id.
        /// </summary>
        private readonly string _localId;

        /// <summary>
        ///     The software language.
        /// </summary>
        private readonly string _softwareLanguage;

        /// <summary>
        ///     The software package.
        /// </summary>
        private readonly string _softwarePackage;

        /// <summary>
        ///     The software version.
        /// </summary>
        private readonly string _softwareVersion;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComputationCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="mutableObject">
        ///     The mutable object.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="mutableObject"/> is <see langword="null" />.</exception>
        public ComputationCore(IIdentifiableObject parent, IComputationMutableObject mutableObject)
            : base(mutableObject, parent)
        {
            if (mutableObject == null)
            {
                throw new ArgumentNullException("mutableObject");
            }

            this._description = new List<ITextTypeWrapper>();
            this._localId = mutableObject.LocalId;
            this._softwareLanguage = mutableObject.SoftwareLanguage;
            this._softwarePackage = mutableObject.SoftwarePackage;
            this._softwareVersion = mutableObject.SoftwareVersion;
            if (mutableObject.Descriptions != null)
            {
                foreach (ITextTypeWrapperMutableObject currentTt in mutableObject.Descriptions)
                {
                    if (!string.IsNullOrWhiteSpace(currentTt.Value))
                    {
                        this._description.Add(new TextTypeWrapperImpl(currentTt, this));
                    }
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComputationCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="xmlType">
        ///     The xml type.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="xmlType"/> is <see langword="null" />.</exception>
        public ComputationCore(IIdentifiableObject parent, ComputationType xmlType)
            : base(xmlType, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Computation), parent)
        {
            if (xmlType == null)
            {
                throw new ArgumentNullException("xmlType");
            }

            this._description = new List<ITextTypeWrapper>();
            this._localId = xmlType.localID;
            this._softwareLanguage = xmlType.softwareLanguage;
            this._softwarePackage = xmlType.softwarePackage;
            this._softwareVersion = xmlType.softwareVersion;
            this._description = TextTypeUtil.WrapTextTypeV21(xmlType.Description, this);
            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.0 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ComputationCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="process">
        ///     The process.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="process"/> is <see langword="null" />.</exception>
        public ComputationCore(IIdentifiableObject parent, ProcessStepType process)
            : base(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Computation), parent)
        {
            if (process == null)
            {
                throw new ArgumentNullException("process");
            }

            this._description = new List<ITextTypeWrapper>();
            if (process.Computation != null)
            {
                this._description = TextTypeUtil.WrapTextTypeV2(process.Computation, this);
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException ex)
            {
                throw new SdmxSemmanticException(ex, ExceptionCode.ObjectStructureConstructionError, this);
            }
            catch (Exception th)
            {
                throw new SdmxException(th, ExceptionCode.ObjectStructureConstructionError, this);
            }
        }

        /// <summary>
        ///     Gets the description.
        /// </summary>
        public virtual IList<ITextTypeWrapper> Description
        {
            get
            {
                return new List<ITextTypeWrapper>(this._description);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Gets the local id.
        /// </summary>
        public virtual string LocalId
        {
            get
            {
                return this._localId;
            }
        }

        /// <summary>
        ///     Gets the software language.
        /// </summary>
        public virtual string SoftwareLanguage
        {
            get
            {
                return this._softwareLanguage;
            }
        }

        /// <summary>
        ///     Gets the software package.
        /// </summary>
        public virtual string SoftwarePackage
        {
            get
            {
                return this._softwarePackage;
            }
        }

        /// <summary>
        ///     Gets the software version.
        /// </summary>
        public virtual string SoftwareVersion
        {
            get
            {
                return this._softwareVersion;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IComputationObject)sdmxObject;
                if (!this.Equivalent(this._description, that.Description, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._localId, that.LocalId))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._softwarePackage, that.SoftwarePackage))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._softwareLanguage, that.SoftwareLanguage))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._softwareVersion, that.SoftwareVersion))
                {
                    return false;
                }

                return this.DeepEqualsInternalAnnotable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._description, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (this._description == null)
            {
                throw new SdmxSemmanticException("Computation missing mandatory field 'description'");
            }
        }
    }
}