// -----------------------------------------------------------------------
// <copyright file="ProcessStepCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process
{
    using System;
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The process step core.
    /// </summary>
    [Serializable]
    public class ProcessStepCore : NameableCore, IProcessStepObject
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog LOG = LogManager.GetLogger(typeof(ProcessStepCore));

        /// <summary>
        ///     The computation.
        /// </summary>
        private readonly IComputationObject _computation;

        /// <summary>
        ///     The input.
        /// </summary>
        private readonly IList<IInputOutputObject> _input;

        /// <summary>
        ///     The output.
        /// </summary>
        private readonly IList<IInputOutputObject> _output;

        /// <summary>
        ///     The process steps.
        /// </summary>
        private readonly IList<IProcessStepObject> _processSteps;

        /// <summary>
        ///     The transitions.
        /// </summary>
        private readonly IList<ITransition> _transitions;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="processStepMutableObject">
        ///     The iprocess.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ProcessStepCore(IIdentifiableObject parent, IProcessStepMutableObject processStepMutableObject)
            : base(processStepMutableObject, parent)
        {
            this._input = new List<IInputOutputObject>();
            this._output = new List<IInputOutputObject>();
            this._transitions = new List<ITransition>();
            this._processSteps = new List<IProcessStepObject>();
            if (processStepMutableObject.Input != null)
            {
                foreach (IInputOutputMutableObject currentIo in processStepMutableObject.Input)
                {
                    this._input.Add(new InputOutputCore(this, currentIo));
                }
            }

            if (processStepMutableObject.Output != null)
            {
                foreach (IInputOutputMutableObject currentIo0 in processStepMutableObject.Output)
                {
                    this._output.Add(new InputOutputCore(this, currentIo0));
                }
            }

            if (processStepMutableObject.Computation != null)
            {
                this._computation = new ComputationCore(this, processStepMutableObject.Computation);
            }

            if (processStepMutableObject.Transitions != null)
            {
                foreach (ITransitionMutableObject mutable in processStepMutableObject.Transitions)
                {
                    this._transitions.Add(new TransitionCore(mutable, this));
                }
            }

            if (processStepMutableObject.ProcessSteps != null)
            {
                foreach (IProcessStepMutableObject mutable1 in processStepMutableObject.ProcessSteps)
                {
                    this._processSteps.Add(new ProcessStepCore(this, mutable1));
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="process">
        ///     The process.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public ProcessStepCore(INameableObject parent, ProcessStepType process)
            : base(process, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProcessStep), parent)
        {
            this._input = new List<IInputOutputObject>();
            this._output = new List<IInputOutputObject>();
            this._transitions = new List<ITransition>();
            this._processSteps = new List<IProcessStepObject>();

            if (process.Input != null)
            {
                foreach (InputOutputType currentIo in process.Input)
                {
                    this._input.Add(new InputOutputCore(this, currentIo));
                }
            }

            if (process.Output != null)
            {
                foreach (InputOutputType currentIo0 in process.Output)
                {
                    this._output.Add(new InputOutputCore(this, currentIo0));
                }
            }

            if (process.Computation != null)
            {
                this._computation = new ComputationCore(this, process.Computation);
            }

            if (process.ProcessStep != null)
            {
                foreach (ProcessStepType processStep in process.ProcessStep)
                {
                    this._processSteps.Add(new ProcessStepCore(this, processStep));
                }
            }

            if (process.Transition != null)
            {
                foreach (TransitionType trans in process.Transition)
                {
                    ITransition transitionCore = new TransitionCore(trans, this);
                    this._transitions.Add(transitionCore);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessStepCore" /> class.
        /// </summary>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <param name="process">
        ///     The process.
        /// </param>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="process"/> is <see langword="null" />.</exception>
        public ProcessStepCore(
            INameableObject parent, 
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ProcessStepType process)
            : base(
                SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProcessStep), 
                process.PassNoNull("process").id, 
                default(Uri), 
                process.PassNoNull("process").Name, 
                process.PassNoNull("process").Description, 
                process.PassNoNull("process").Annotations, 
                parent)
        {
            if (process == null)
            {
                throw new ArgumentNullException("process");
            }

            this._input = new List<IInputOutputObject>();
            this._output = new List<IInputOutputObject>();
            this._transitions = new List<ITransition>();
            this._processSteps = new List<IProcessStepObject>();

            if (process.Input != null)
            {
                LOG.Warn("Input items not supported for SDMX V2.0. These items will be discarded");
            }

            if (process.Output != null)
            {
                LOG.Warn("Input items not supported for SDMX V2.0. These items will be discarded");
            }

            if (process.Computation != null)
            {
                this._computation = new ComputationCore(this, process);
            }

            if (process.ProcessStep != null)
            {
                foreach (
                    Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.ProcessStepType processStep in process.ProcessStep)
                {
                    this._processSteps.Add(new ProcessStepCore(this, processStep));
                }
            }

            if (process.Transition != null)
            {
                foreach (Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.TransitionType trans in process.Transition)
                {
                    ITransition transitionCore = new TransitionCore(trans, this);
                    this._transitions.Add(transitionCore);
                }
            }

            try
            {
                this.Validate();
            }
            catch (SdmxSemmanticException e)
            {
                throw new SdmxSemmanticException(e, ExceptionCode.FailValidation, this);
            }
        }

        /// <summary>
        ///     Gets the all text types.
        /// </summary>
        public override IList<ITextTypeWrapper> AllTextTypes
        {
            get
            {
                IList<ITextTypeWrapper> allTextTypes = base.AllTextTypes;
                if (this._computation != null)
                {
                    this._computation.Description.AddAll(allTextTypes);
                }

                return allTextTypes;
            }
        }

        /// <summary>
        ///     Gets the computation.
        /// </summary>
        public virtual IComputationObject Computation
        {
            get
            {
                return this._computation;
            }
        }

        /// <summary>
        ///     Gets the input.
        /// </summary>
        public virtual IList<IInputOutputObject> Input
        {
            get
            {
                return new List<IInputOutputObject>(this._input);
            }
        }

        /// <summary>
        ///     Gets the output.
        /// </summary>
        public virtual IList<IInputOutputObject> Output
        {
            get
            {
                return new List<IInputOutputObject>(this._output);
            }
        }

        /// <summary>
        ///     Gets the process steps.
        /// </summary>
        public virtual IList<IProcessStepObject> ProcessSteps
        {
            get
            {
                return new List<IProcessStepObject>(this._processSteps);
            }
        }

        /// <summary>
        ///     Gets the transitions.
        /// </summary>
        public virtual IList<ITransition> Transitions
        {
            get
            {
                return new List<ITransition>(this._transitions);
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (IProcessStepObject)sdmxObject;
                if (!this.Equivalent(this._input, that.Input, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._output, that.Output, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._transitions, that.Transitions, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._processSteps, that.ProcessSteps, includeFinalProperties))
                {
                    return false;
                }

                if (!this.Equivalent(this._computation, that.Computation, includeFinalProperties))
                {
                    return false;
                }

                return this.DeepEqualsNameable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._input, composites);
            this.AddToCompositeSet(this._output, composites);
            this.AddToCompositeSet(this._transitions, composites);
            this.AddToCompositeSet(this._processSteps, composites);
            this.AddToCompositeSet(this._computation, composites);
            return composites;
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        private void Validate()
        {
            // NO VALIDATION REQUIRED
        }
    }
}