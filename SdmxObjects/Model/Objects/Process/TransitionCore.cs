// -----------------------------------------------------------------------
// <copyright file="TransitionCore.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxObjects.
//     SdmxObjects is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxObjects is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxObjects.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The transition core.
    /// </summary>
    [Serializable]
    public class TransitionCore : IdentifiableCore, ITransition
    {
        /// <summary>
        ///     The condition.
        /// </summary>
        private readonly IList<ITextTypeWrapper> _condition;

        /// <summary>
        ///     The local id.
        /// </summary>
        private readonly string _localId;

        /// <summary>
        ///     The target step.
        /// </summary>
        private readonly string _targetStep;

        /// <summary>
        ///     The process step.
        /// </summary>
        private IProcessStepObject _processStep; // Referenced from targetStep

        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionCore" /> class.
        /// </summary>
        /// <param name="itemMutableObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public TransitionCore(ITransitionMutableObject itemMutableObject, ISdmxStructure parent)
            : base(itemMutableObject, parent)
        {
            this._condition = new List<ITextTypeWrapper>();
            this._targetStep = itemMutableObject.TargetStep;
            if (itemMutableObject.Conditions != null)
            {
                foreach (ITextTypeWrapperMutableObject textTypeWrapperMutableObject in itemMutableObject.Conditions)
                {
                    if (!string.IsNullOrWhiteSpace(textTypeWrapperMutableObject.Value))
                    {
                        this._condition.Add(new TextTypeWrapperImpl(textTypeWrapperMutableObject, this));
                    }
                }
            }

            this._localId = itemMutableObject.LocalId;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2.1 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionCore" /> class.
        /// </summary>
        /// <param name="transition">
        ///     The transition.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        public TransitionCore(TransitionType transition, ISdmxStructure parent)
            : base(transition, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Transition), parent)
        {
            this._condition = new List<ITextTypeWrapper>();
            if (transition.TargetStep != null)
            {
                this._targetStep = RefUtil.CreateLocalIdReference(transition.TargetStep);
            }

            if (transition.Condition != null)
            {
                this._condition = TextTypeUtil.WrapTextTypeV21(transition.Condition, this);
            }

            this._localId = transition.localID;
            this.Validate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM V2 SCHEMA                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionCore" /> class.
        /// </summary>
        /// <param name="trans">
        ///     The trans.
        /// </param>
        /// <param name="parent">
        ///     The parent.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="trans"/> is <see langword="null" />.</exception>
        public TransitionCore(
            Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure.TransitionType trans, 
            ISdmxStructure parent)
            : base(GenerateId(), SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Transition), parent)
        {
            if (trans == null)
            {
                throw new ArgumentNullException("trans");
            }

            this._condition = new List<ITextTypeWrapper>();
            this._targetStep = trans.TargetStep;
            if (trans.Condition != null)
            {
                this._condition.Add(new TextTypeWrapperImpl(trans.Condition, this));
            }
        }

        /// <summary>
        ///     Gets the all text types.
        /// </summary>
        public override IList<ITextTypeWrapper> AllTextTypes
        {
            get
            {
                IList<ITextTypeWrapper> returnList = base.AllTextTypes;
                this._condition.AddAll(returnList);
                return returnList;
            }
        }

        /// <summary>
        ///     Gets the condition.
        /// </summary>
        public virtual IList<ITextTypeWrapper> Condition
        {
            get
            {
                return new List<ITextTypeWrapper>(this._condition);
            }
        }

        /// <summary>
        ///     Gets the local id.
        /// </summary>
        public virtual string LocalId
        {
            get
            {
                return this._localId;
            }
        }

        /// <summary>
        ///     Gets the target step.
        /// </summary>
        public virtual IProcessStepObject TargetStep
        {
            get
            {
                if (this._targetStep != null && this._processStep == null)
                {
                    this.VerifyProcessSteps();
                }

                return this._processStep;
            }
        }

        /// <summary>
        ///     The deep equals.
        /// </summary>
        /// <param name="sdmxObject">
        ///     The sdmxObject.
        /// </param>
        /// <param name="includeFinalProperties">The include final properties flag.</param>
        /// <returns>
        ///     The <see cref="bool" /> .
        /// </returns>
        public override bool DeepEquals(ISdmxObject sdmxObject, bool includeFinalProperties)
        {
            if (sdmxObject == null)
            {
                return false;
            }

            if (sdmxObject.StructureType == this.StructureType)
            {
                var that = (ITransition)sdmxObject;
                if (!this.Equivalent(this._condition, that.Condition, includeFinalProperties))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this.TargetStep, that.TargetStep))
                {
                    return false;
                }

                if (!ObjectUtil.Equivalent(this._localId, that.LocalId))
                {
                    return false;
                }

                return this.DeepEqualsIdentifiable(that, includeFinalProperties);
            }

            return false;
        }

        /// <summary>
        ///     Method to be called after construction of Process, as a Transition may be referencing a Process Step that has not
        ///     yet been built due to nesting
        /// </summary>
        protected internal void VerifyProcessSteps()
        {
            var parentProcess = (IProcessObject)this.MaintainableParent;
            this.SetTargetStep(parentProcess.ProcessSteps, this._targetStep.Split('.'), 0);
        }

        /// <summary>
        ///     Get composites internal.
        /// </summary>
        /// <returns>
        ///     The composites
        /// </returns>
        protected override ISet<ISdmxObject> GetCompositesInternal()
        {
            ISet<ISdmxObject> composites = base.GetCompositesInternal();
            this.AddToCompositeSet(this._condition, composites);
            return composites;
        }

        /// <summary>
        ///     The generate id.
        /// </summary>
        /// <returns> The <see cref="string" /> . </returns>
        private static string GenerateId()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        ///     Walks through Processes and sub processes to set the IProcessStepObject referenced from the targetStep
        /// </summary>
        /// <param name="processSteps">
        ///     List of processes.
        /// </param>
        /// <param name="targetStepSplit">
        ///     The target Step Split.
        /// </param>
        /// <param name="currentPosition">
        ///     The current Position.
        /// </param>
        private void SetTargetStep(
            IList<IProcessStepObject> processSteps, 
            string[] targetStepSplit, 
            int currentPosition)
        {
            foreach (IProcessStepObject currentProcessStep in processSteps)
            {
                if (currentProcessStep.Id.Equals(targetStepSplit[currentPosition]))
                {
                    int nextPos = currentPosition + 1;
                    if (targetStepSplit.Length > nextPos)
                    {
                        this.SetTargetStep(currentProcessStep.ProcessSteps, targetStepSplit, nextPos);
                        return;
                    }

                    this._processStep = currentProcessStep;
                    return;
                }
            }

            throw new SdmxSemmanticException(
                "Can not resolve reference to ProcessStep with reference '" + this._targetStep + "'");
        }

        /// <summary>
        ///     The validate.
        /// </summary>
        /// <exception cref="SdmxSemmanticException">Throws Validate exception.</exception>
        private void Validate()
        {
            if (string.IsNullOrWhiteSpace(this._targetStep))
            {
                throw new SdmxSemmanticException("Transition is missing mandatory 'Target Step'");
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////DEEP EQUALS                             //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////VALIDATE                 //////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////BUILD FROM MUTABLE OBJECTS              //////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }
}