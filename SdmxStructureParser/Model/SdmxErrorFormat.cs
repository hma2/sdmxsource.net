﻿// -----------------------------------------------------------------------
// <copyright file="SdmxErrorFormat.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Model
{
    using Org.Sdmxsource.Sdmx.Api.Model;

    /// <summary>
    /// Defines an SDMX Error format, no methods required
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Model.IErrorFormat" />
    public sealed class SdmxErrorFormat : IErrorFormat
    {
        /// <summary>
        /// The instance
        /// </summary>
        private static readonly SdmxErrorFormat _instance = new SdmxErrorFormat();

        /// <summary>
        /// Prevents a default instance of the <see cref="SdmxErrorFormat"/> class from being created.
        /// </summary>
        private SdmxErrorFormat()
        {
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static SdmxErrorFormat Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}