﻿// -----------------------------------------------------------------------
// <copyright file="MetadataflowCrossReferenceUpdaterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The default implementation of the <see cref="IMetadataflowCrossReferenceUpdaterEngine"/>
    /// </summary>
    public class MetadataflowCrossReferenceUpdaterEngine : IMetadataflowCrossReferenceUpdaterEngine
    {
        /// <summary>
        /// Updates the references.
        /// </summary>
        /// <param name="maintainable">The maintainable.</param>
        /// <param name="updateReferences">The update references.</param>
        /// <param name="newVersionNumber">The new version number.</param>
        /// <returns>The <see cref="IMetadataFlow"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="maintainable"/> is <see langword="null" />.</exception>
        public IMetadataFlow UpdateReferences(
            IMetadataFlow maintainable, 
            IDictionary<IStructureReference, IStructureReference> updateReferences, 
            string newVersionNumber)
        {
            if (maintainable == null)
            {
                throw new ArgumentNullException("maintainable");
            }

            if (updateReferences == null)
            {
                throw new ArgumentNullException("updateReferences");
            }

            IMetadataFlowMutableObject mdf = maintainable.MutableInstance;
            mdf.Version = newVersionNumber;

            IStructureReference newTarget;
            if (updateReferences.TryGetValue(mdf.MetadataStructureRef, out newTarget))
            {
                mdf.MetadataStructureRef = newTarget;
            }

            return mdf.ImmutableInstance;
        }
    }
}