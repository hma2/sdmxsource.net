﻿// -----------------------------------------------------------------------
// <copyright file="StructureSetCrossReferenceUpdaterEngine.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Reversion
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The StructureSet CrossReference Updater Engine.
    /// </summary>
    public class StructureSetCrossReferenceUpdaterEngine : IStructureSetCrossReferenceUpdaterEngine
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Updates the references.
        /// </summary>
        /// <param name="maintainable">The maintainable.</param>
        /// <param name="updateReferences">The update references.</param>
        /// <param name="newVersionNumber">The new version number.</param>
        /// <returns>The <see cref="IStructureSetObject" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="maintainable"/> is <see langword="null" />.</exception>
        public IStructureSetObject UpdateReferences(IStructureSetObject maintainable, IDictionary<IStructureReference, IStructureReference> updateReferences, string newVersionNumber)
        {
            if (maintainable == null)
            {
                throw new ArgumentNullException("maintainable");
            }

            IStructureSetMutableObject ss = maintainable.MutableInstance;
            ss.Version = newVersionNumber;

            UpdateSchemeMap(ss.CategorySchemeMapList, updateReferences);
            UpdateSchemeMap(ss.CodelistMapList, updateReferences);
            UpdateSchemeMap(ss.ConceptSchemeMapList, updateReferences);
            UpdateSchemeMap(ss.OrganisationSchemeMapList, updateReferences);
            UpdateSchemeMap(ss.StructureMapList, updateReferences);

            if (ss.RelatedStructures != null)
            {
                IRelatedStructuresMutableObject relatedStructures = ss.RelatedStructures;
                UpdateRelatedStructures(relatedStructures.CategorySchemeRef, updateReferences);
                UpdateRelatedStructures(relatedStructures.ConceptSchemeRef, updateReferences);
                UpdateRelatedStructures(relatedStructures.HierCodelistRef, updateReferences);
                UpdateRelatedStructures(relatedStructures.DataStructureRef, updateReferences);
                UpdateRelatedStructures(relatedStructures.MetadataStructureRef, updateReferences);
                UpdateRelatedStructures(relatedStructures.OrgSchemeRef, updateReferences);
            }

            return ss.ImmutableInstance;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Updates the related structures.
        /// </summary>
        /// <param name="references">The reference list.</param>
        /// <param name="updateReferences">The update references.</param>
        private static void UpdateRelatedStructures(ICollection<IStructureReference> references, IDictionary<IStructureReference, IStructureReference> updateReferences)
        {
            IList<IStructureReference> newReferences = new List<IStructureReference>();
            if (references != null)
            {
                foreach (IStructureReference currentSRef in references)
                {
                    IStructureReference updatedRef;
                    if (updateReferences.TryGetValue(currentSRef, out updatedRef))
                    {
                        newReferences.Add(updatedRef);
                    }
                    else
                    {
                        newReferences.Add(currentSRef);
                    }
                }

                references.Clear();
                references.AddAll(newReferences);
            }
        }

        /// <summary>
        ///     Updates the scheme map.
        /// </summary>
        /// <typeparam name="T">The <see cref="ISchemeMapMutableObject" /> based class</typeparam>
        /// <param name="schemeMaps">The scheme maps.</param>
        /// <param name="updateReferences">The update references.</param>
        private static void UpdateSchemeMap<T>(IList<T> schemeMaps, IDictionary<IStructureReference, IStructureReference> updateReferences) where T : class, ISchemeMapMutableObject
        {
            if (schemeMaps != null)
            {
                foreach (T currentMap in schemeMaps)
                {
                    IStructureReference newTarget;
                    if (updateReferences.TryGetValue(currentMap.SourceRef, out newTarget))
                    {
                        currentMap.SourceRef = newTarget;
                    }

                    if (updateReferences.TryGetValue(currentMap.TargetRef, out newTarget))
                    {
                        currentMap.TargetRef = newTarget;
                    }
                }
            }
        }

        #endregion
    }
}