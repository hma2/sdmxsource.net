// -----------------------------------------------------------------------
// <copyright file="StructureWriterEngineJson.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Text;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Json;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.Util.Date;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The Json structure writing engine for SDMX.
    /// </summary>
    public class StructureWriterEngineJson : IStructureWriterEngine
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The jSON generator
        /// </summary>
        private readonly JsonGenerator _jsonGenerator;

        /// <summary>
        /// The Structure jSON Format
        /// </summary>
        private readonly SdmxStructureJsonFormat _sdmxStructureJsonFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineJson"/> class.
        /// </summary>
        /// <param name="outputStream">
        /// The output stream.
        /// </param>
        /// <param name="sdmxStructureJsonFormat">
        /// The SDMX structure JSON format.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// outputStream
        /// or
        /// translator
        /// or
        /// encoding
        /// </exception>
        public StructureWriterEngineJson(Stream outputStream, SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            if (sdmxStructureJsonFormat == null)
            {
                throw new ArgumentNullException("sdmxStructureJsonFormat");
            }

            this._jsonGenerator = new JsonGenerator(new StreamWriter(outputStream, Encoding.UTF8));
            this._jsonGenerator.WriteStartObject(); // The container for the message

            this._sdmxStructureJsonFormat = sdmxStructureJsonFormat;
        }

        /// <summary>
        /// Writes the @maintainableObject out to the output location in the format specified by the implementation
        /// </summary>
        /// <param name="maintainableObject">
        /// The maintainableObject.
        /// </param>
        public void WriteStructure(IMaintainableObject maintainableObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Write into Json format the sdmxObjects
        /// </summary>
        /// <param name="sdmxObjects">
        /// SDMX objects
        /// </param>
        public void WriteStructures(ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException("sdmxObjects");
            }

            if (sdmxObjects.Header != null)
            {
                this.WriteDocumentHeader(sdmxObjects.Header);
            }

            _log.Debug("{resources}");
            this._jsonGenerator.WriteArrayFieldStart("resources");

            switch (this._sdmxStructureJsonFormat.RequestedStructureType)
            {
                case SdmxStructureEnumType.Dataflow:
                    {
                        foreach (var df in sdmxObjects.Dataflows)
                        {
                            this.WriteDataflow(df, false);
                        }

                        break;
                    }

                case SdmxStructureEnumType.CategoryScheme:
                    {
                        foreach (var catSch in sdmxObjects.CategorySchemes)
                        {
                            this.WriteCategoryScheme(catSch, sdmxObjects.Categorisations, false);
                        }

                        break;
                    }

                case SdmxStructureEnumType.AgencyScheme:
                    {
                        foreach (var agSch in sdmxObjects.AgenciesSchemes)
                        {
                            this.WriteAgencyScheme(agSch, false);
                        }

                        break;
                    }
            }

            this._jsonGenerator.WriteEndArray();
            _log.Debug("{/resources}");

            _log.Debug("{references}");
            this._jsonGenerator.WriteObjectFieldStart("references");

            if (this._sdmxStructureJsonFormat.RequestedStructureType != SdmxStructureEnumType.Dataflow)
            {
                foreach (var df in sdmxObjects.Dataflows)
                {
                    this.WriteDataflow(df, true);
                }
            }

            if (this._sdmxStructureJsonFormat.RequestedStructureType != SdmxStructureEnumType.CategoryScheme)
            {
                foreach (var catSch in sdmxObjects.CategorySchemes)
                {
                    this.WriteCategoryScheme(catSch, sdmxObjects.Categorisations, true);
                }
            }

            // Categorisation has no meaning in the Data Discovery use case, SDMX-JSON doesn�t return that artefact but returns DataFlows directly 
            // as children of Categories, resolving the relationship dynamically
            /*foreach (ICategorisationObject cat in sdmxObjects.Categorisations)
                WriteCategorisation(cat);
             */
            if (this._sdmxStructureJsonFormat.RequestedStructureType != SdmxStructureEnumType.AgencyScheme)
            {
                foreach (var agSch in sdmxObjects.AgenciesSchemes)
                {
                    this.WriteAgencyScheme(agSch, true);
                }
            }

            this._jsonGenerator.WriteEndObject();
            _log.Debug("{/references}");

            this._jsonGenerator.Flush();
            this._jsonGenerator.Close(); // Todo: We should Close on Dispose?
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                if (this._jsonGenerator != null)
                {
                    this._jsonGenerator.Close();
                }
            }
        }

        /// <summary>
        /// Define Links inside Categories to Dataflow based on given Categorisation
        /// </summary>
        /// <param name="buildFrom">
        /// The source object (Category).
        /// </param>
        /// <param name="categorisations">
        /// The categorisations.
        /// </param>
        private void PopulatedCategorisations(
            IIdentifiableObject buildFrom,
            IEnumerable<ICategorisationObject> categorisations)
        {
            if (categorisations != null)
            {
                var dataflowRefTypes = new List<DataflowRefType>();

                foreach (var cat in categorisations)
                {
                    var catRef = cat.CategoryReference;
                    if (catRef.IsMatch(buildFrom))
                    {
                        switch (cat.StructureReference.TargetReference.EnumType)
                        {
                            case SdmxStructureEnumType.Dataflow:
                                {
                                    var dataflowRefType = new DataflowRefType();

                                    var maintainableReference = cat.StructureReference.MaintainableReference;
                                    var str1 = maintainableReference.AgencyId;
                                    if (!string.IsNullOrWhiteSpace(str1))
                                    {
                                        dataflowRefType.AgencyID = maintainableReference.AgencyId;
                                    }

                                    var str3 = maintainableReference.MaintainableId;
                                    if (!string.IsNullOrWhiteSpace(str3))
                                    {
                                        dataflowRefType.DataflowID = maintainableReference.MaintainableId;
                                    }

                                    var str4 = maintainableReference.Version;
                                    if (!string.IsNullOrWhiteSpace(str4))
                                    {
                                        dataflowRefType.Version = maintainableReference.Version;
                                    }

                                    dataflowRefType.URN = cat.StructureReference.TargetUrn;

                                    dataflowRefTypes.Add(dataflowRefType);
                                }

                                break;
                        }
                    }
                }

                if (dataflowRefTypes.Count > 0)
                {
                    // Todo: create a method for writting links !
                    this._jsonGenerator.WriteArrayFieldStart("links");

                    foreach (var drt in dataflowRefTypes)
                    {
                        this._jsonGenerator.WriteStartObject();

                        this._jsonGenerator.WriteStringField("href", drt.URN.ToString());
                        this._jsonGenerator.WriteStringField("rel", "dataflow");

                        this._jsonGenerator.WriteEndObject();
                    }

                    this._jsonGenerator.WriteEndArray();
                }
            }
        }

        /// <summary>
        /// Write an AgencyScheme.
        /// </summary>
        /// <param name="agencyScheme">
        /// The AgencyScheme to write.
        /// </param>
        /// <param name="isReference">
        /// If true then it is considered as reference and the URN is to identified the Json Object
        /// </param>
        private void WriteAgencyScheme(IAgencyScheme agencyScheme, bool isReference)
        {
            if (agencyScheme != null)
            {
                if (isReference)
                {
                    this._jsonGenerator.WriteObjectFieldStart(agencyScheme.Urn.ToString());
                }
                else
                {
                    this._jsonGenerator.WriteStartObject();
                }

                this.WriteIMaintainableObject(agencyScheme);

                if (agencyScheme.Items != null)
                {
                    this.WriteItems(agencyScheme.Items);
                }

                this._jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write an Annotation.
        /// </summary>
        /// <param name="annotation">
        /// The Annotation to write.
        /// </param>
        private void WriteAnnotation(IAnnotation annotation)
        {
            _log.Debug("{}");
            this._jsonGenerator.WriteStartObject();

            if (annotation.Id != null)
            {
                this._jsonGenerator.WriteStringField("id", annotation.Id);
            }

            if (annotation.Title != null)
            {
                this._jsonGenerator.WriteStringField("title", annotation.Title);
            }

            if (annotation.Type != null)
            {
                this._jsonGenerator.WriteStringField("type", annotation.Type);
            }

            if (annotation.Uri != null)
            {
                this._jsonGenerator.WriteStringField("uri", annotation.Uri.ToString());
            }

            if (annotation.Text.Count > 0)
            {
                this._jsonGenerator.WriteStringField(
                    "text", this._sdmxStructureJsonFormat.Translator.GetTranslation(annotation.Text));
            }

            _log.Debug("{/}");
            this._jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write a list of Annotations.
        /// </summary>
        /// <param name="annotations">
        /// The Annotations to write.
        /// </param>
        private void WriteAnnotations(IList<IAnnotation> annotations)
        {
            if (ObjectUtil.ValidCollection(annotations))
            {
                this._jsonGenerator.WriteArrayFieldStart("annotations");

                foreach (var annotation in annotations)
                {
                    this.WriteAnnotation(annotation);
                }

                this._jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a list of Categories.
        /// </summary>
        /// <param name="categories">
        /// The Categories to write.
        /// </param>
        /// <param name="categorisations">
        /// Categorisations will be inserted as Links into the Categories.
        /// </param>
        private void WriteCategories(IList<ICategoryObject> categories, ISet<ICategorisationObject> categorisations)
        {
            if (categories != null)
            {
                this._jsonGenerator.WriteArrayFieldStart("items");
                foreach (var cat in categories)
                {
                    this.WriteCategory(cat, categorisations);
                }

                this._jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a Category.
        /// </summary>
        /// <param name="category">
        /// The Category to write.
        /// </param>
        /// <param name="categorisations">
        /// Categorisations will be inserted as Links into the Categories.
        /// </param>
        private void WriteCategory(ICategoryObject category, ISet<ICategorisationObject> categorisations)
        {
            if (category != null)
            {
                this._jsonGenerator.WriteStartObject();

                if (category.Id != null)
                {
                    this._jsonGenerator.WriteStringField("id", category.Id);
                }

                if (category.Name != null)
                {
                    this._jsonGenerator.WriteStringField(
                        "name", this._sdmxStructureJsonFormat.Translator.GetTranslation(category.Names));
                }

                if (category.Description != null)
                {
                    this._jsonGenerator.WriteStringField(
                        "description", this._sdmxStructureJsonFormat.Translator.GetTranslation(category.Descriptions));
                }

                if (category.Urn != null)
                {
                    this._jsonGenerator.WriteStringField("urn", category.Urn.ToString());
                }

                if (category.Uri != null)
                {
                    this._jsonGenerator.WriteStringField("uri", category.Uri.ToString());
                }

                this.WriteAnnotations(category.Annotations);

                this.PopulatedCategorisations(category, categorisations);

                if (category.Items.Count > 0)
                {
                    this._jsonGenerator.WriteArrayFieldStart("items");

                    foreach (var cat in category.Items)
                    {
                        this.WriteCategory(cat, categorisations);
                    }

                    this._jsonGenerator.WriteEndArray();
                }

                this._jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a CategoryScheme.
        /// </summary>
        /// <param name="categoryScheme">
        /// The CategoryScheme to Write.
        /// </param>
        /// <param name="categorisations">
        /// Categorisations will be inserted as Links into the Categories.
        /// </param>
        /// <param name="isReference">
        /// If true then it is considered as reference and the URN is to identified the Json Object
        /// </param>
        private void WriteCategoryScheme(
            ICategorySchemeObject categoryScheme,
            ISet<ICategorisationObject> categorisations,
            bool isReference)
        {
            if (categoryScheme != null)
            {
                if (isReference)
                {
                    this._jsonGenerator.WriteObjectFieldStart(categoryScheme.Urn.ToString());
                }
                else
                {
                    this._jsonGenerator.WriteStartObject();
                }

                this.WriteIMaintainableObject(categoryScheme);
                this.WriteCategories(categoryScheme.Items, categorisations);

                this._jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a Contact.
        /// </summary>
        /// <param name="contact">
        /// The Contact to write.
        /// </param>
        private void WriteContact(IContact contact)
        {
            if (contact != null)
            {
                this._jsonGenerator.WriteStartObject();
                _log.Debug("{contact}");

                if (contact.Id != null)
                {
                    this._jsonGenerator.WriteStringField("id", contact.Id);
                }

                if (ObjectUtil.ValidCollection(contact.Name))
                {
                    this._jsonGenerator.WriteStringField("name", contact.Name[0].Value);
                }

                if (ObjectUtil.ValidCollection(contact.Departments))
                {
                    this._jsonGenerator.WriteStringField("department", contact.Departments[0].Value);
                }

                if (ObjectUtil.ValidCollection(contact.Role))
                {
                    this._jsonGenerator.WriteStringField("role", contact.Role[0].Value);
                }

                if (ObjectUtil.ValidCollection(contact.Email))
                {
                    this._jsonGenerator.WriteStringField("email", contact.Email[0]);
                }

                if (ObjectUtil.ValidCollection(contact.Fax))
                {
                    this._jsonGenerator.WriteStringField("fax", contact.Fax[0]);
                }

                if (ObjectUtil.ValidCollection(contact.Telephone))
                {
                    this._jsonGenerator.WriteStringField("telephone", contact.Telephone[0]);
                }

                if (ObjectUtil.ValidCollection(contact.Uri))
                {
                    this._jsonGenerator.WriteStringField("uri", contact.Uri[0]);
                }

                if (ObjectUtil.ValidCollection(contact.X400))
                {
                    this._jsonGenerator.WriteStringField("x400", contact.X400[0]);
                }

                _log.Debug("{/contact}");
                this._jsonGenerator.WriteEndObject();
            }
        }

        /// <summary>
        /// Write a dataflow
        /// </summary>
        /// <param name="dataflow">
        /// The object Dataflow
        /// </param>
        /// <param name="isReference">
        /// If true then it is considered as reference and the URN is to identified the Json Object
        /// </param>
        private void WriteDataflow(IDataflowObject dataflow, bool isReference)
        {
            if (isReference)
            {
                this._jsonGenerator.WriteObjectFieldStart(dataflow.Urn.ToString());
            }
            else
            {
                this._jsonGenerator.WriteStartObject();
            }

            this.WriteIMaintainableObject(dataflow);

            // Write DSD Ref
            this._jsonGenerator.WriteObjectFieldStart("structure");
            this._jsonGenerator.WriteStringField("urn", dataflow.DataStructureRef.TargetUrn.ToString());
            this._jsonGenerator.WriteEndObject();

            this._jsonGenerator.WriteEndObject();
        }

        /// <summary>
        /// Write the document header
        /// </summary>
        /// <param name="header">
        /// The Header to write.
        /// </param>
        private void WriteDocumentHeader(IHeader header)
        {
            string id;
            DateTime prepared;
            bool isTest;

            if (header != null)
            {
                id = header.Id;
                prepared = header.Prepared.GetValueOrDefault(DateTime.Now);

                // there is no option for null DateTime in C#
                isTest = header.Test;
            }
            else
            {
                id = Guid.NewGuid().ToString();
                prepared = DateTime.Now;
                isTest = false;
            }

            _log.Debug("{Header}");
            this._jsonGenerator.WriteObjectFieldStart("header");
            this._jsonGenerator.WriteStringField("id", id);
            this._jsonGenerator.WriteStringField("prepared", DateUtil.FormatDate(prepared, TimeFormatEnumType.DateTime));
            this._jsonGenerator.WriteBooleanField("test", isTest);

            if (header != null)
            {
                this.WriteSender(header.Sender);
                this.WriteReceivers(header.Receiver);
            }

            this._jsonGenerator.WriteEndObject();
            _log.Debug("{/Header}");
        }

        /// <summary>
        /// Write a MaintainableObject as base for all Artefact.
        /// </summary>
        /// <param name="maintainableObject">
        /// The MaintainableObject to write.
        /// </param>
        private void WriteIMaintainableObject(IMaintainableObject maintainableObject)
        {
            if (maintainableObject != null)
            {
                // todo: What about? -> ServiceUrl
                if (maintainableObject.Id != null)
                {
                    this._jsonGenerator.WriteStringField("id", maintainableObject.Id);
                }

                if (maintainableObject.Name != null)
                {
                    this._jsonGenerator.WriteStringField(
                        "name", this._sdmxStructureJsonFormat.Translator.GetTranslation(maintainableObject.Names));
                }

                if (maintainableObject.Description != null)
                {
                    this._jsonGenerator.WriteStringField(
                        "description", this._sdmxStructureJsonFormat.Translator.GetTranslation(maintainableObject.Descriptions));
                }

                if (maintainableObject.AgencyId != null)
                {
                    this._jsonGenerator.WriteStringField("agencyID", maintainableObject.AgencyId);
                }

                if (maintainableObject.Version != null)
                {
                    this._jsonGenerator.WriteStringField("version", maintainableObject.Version);
                }

                if (maintainableObject.IsFinal != null && maintainableObject.IsFinal.IsSet())
                {
                    this._jsonGenerator.WriteBooleanField("isFinal", maintainableObject.IsFinal.IsTrue);
                }

                if (maintainableObject.Urn != null)
                {
                    this._jsonGenerator.WriteStringField("urn", maintainableObject.Urn.ToString());
                }

                if (maintainableObject.Uri != null)
                {
                    this._jsonGenerator.WriteStringField("uri", maintainableObject.Uri.ToString());
                }

                this.WriteAnnotations(maintainableObject.Annotations);

                if (maintainableObject.StartDate != null && maintainableObject.StartDate.Date.HasValue)
                {
                    this._jsonGenerator.WriteStringField(
                        "validFrom",
                        DateUtil.FormatDate(maintainableObject.StartDate.Date.Value, TimeFormatEnumType.DateTime));
                }

                if (maintainableObject.EndDate != null && maintainableObject.EndDate.Date.HasValue)
                {
                    this._jsonGenerator.WriteStringField(
                        "validTo",
                        DateUtil.FormatDate(maintainableObject.EndDate.Date.Value, TimeFormatEnumType.DateTime));
                }

                if (maintainableObject.IsExternalReference != null && maintainableObject.IsExternalReference.IsSet())
                {
                    this._jsonGenerator.WriteBooleanField(
                        "IsExternalReference",
                        maintainableObject.IsExternalReference.IsTrue);
                }

                if (maintainableObject.StructureUrl != null)
                {
                    this._jsonGenerator.WriteStringField("structureUrl", maintainableObject.StructureUrl.ToString());
                }
            }
        }

        /// <summary>
        /// Write a list of Items.
        /// </summary>
        /// <typeparam name="T">
        /// Type of the Item.
        /// </typeparam>
        /// <param name="items">
        /// The Item to write.
        /// </param>
        private void WriteItems<T>(IList<T> items) where T : IItemObject
        {
            if (items.Count > 0)
            {
                this._jsonGenerator.WriteArrayFieldStart("items");

                foreach (var it in items)
                {
                    this._jsonGenerator.WriteStartObject();

                    if (it.Id != null)
                    {
                        this._jsonGenerator.WriteStringField("id", it.Id);
                    }

                    if (it.Urn != null)
                    {
                        this._jsonGenerator.WriteStringField("urn", it.Urn.ToString());
                    }

                    if (it.Name != null)
                    {
                        this._jsonGenerator.WriteStringField(
                            "name", this._sdmxStructureJsonFormat.Translator.GetTranslation(it.Names));
                    }

                    if (it.Description != null)
                    {
                        this._jsonGenerator.WriteStringField(
                            "description", this._sdmxStructureJsonFormat.Translator.GetTranslation(it.Descriptions));
                    }

                    this.WriteAnnotations(it.Annotations);

                    this._jsonGenerator.WriteEndObject();
                }

                this._jsonGenerator.WriteEndArray();
            }
        }

        /// <summary>
        /// Write a Party.
        /// </summary>
        /// <param name="partyObject">
        /// The Party to write.
        /// </param>
        private void WriteParty(IParty partyObject)
        {
            var senderId = "unknown";
            var senderName = "unknown";

            if (partyObject != null)
            {
                senderId = partyObject.Id;
                if (ObjectUtil.ValidCollection(partyObject.Name))
                {
                    senderName = partyObject.Name[0].Value; // Todo: Why the first one only?
                }

                if (ObjectUtil.ValidCollection(partyObject.Contacts))
                {
                    this._jsonGenerator.WriteArrayFieldStart("contacts");

                    foreach (var contact in partyObject.Contacts)
                    {
                        this.WriteContact(contact);
                    }

                    this._jsonGenerator.WriteEndArray();
                }
            }

            this._jsonGenerator.WriteStringField("id", senderId);
            this._jsonGenerator.WriteStringField("name", senderName);
        }

        /// <summary>
        /// Write the Receivers.
        /// </summary>
        /// <param name="receivers">
        /// The Receivers
        /// </param>
        private void WriteReceivers(IList<IParty> receivers)
        {
            if (receivers == null || !ObjectUtil.ValidCollection(receivers))
            {
                // Do not write receiver since it's empty
                return;
            }

            _log.Debug("{receiver}");

            this._jsonGenerator.WriteArrayFieldStart("receiver");

            foreach (var receiver in receivers)
            {
                this._jsonGenerator.WriteStartObject();
                this.WriteParty(receiver);
                this._jsonGenerator.WriteEndObject();
            }

            _log.Debug("{/receiver}");
            this._jsonGenerator.WriteEndArray();
        }

        /// <summary>
        /// Write the Sender.
        /// </summary>
        /// <param name="sender">
        /// The Sender to write.
        /// </param>
        private void WriteSender(IParty sender)
        {
            _log.Debug("{sender}");
            this._jsonGenerator.WriteObjectFieldStart("sender");

            this.WriteParty(sender);

            _log.Debug("{/sender}");
            this._jsonGenerator.WriteEndObject();
        }
    }
}