// -----------------------------------------------------------------------
// <copyright file="StructureWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;

    using Xml.Schema.Linq;

    /// <summary>
    ///     The structure writing engine v 2.
    /// </summary>
    public class StructureWriterEngineV2 : AbstractWritingEngine
    {
        /// <summary>
        ///     The structure xml bean builder bean.
        /// </summary>
        private readonly StructureXmlBuilder _structureXmlBuilderBean;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineV2" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="structureXmlBuilderBean">The structure XML builder bean.</param>
        public StructureWriterEngineV2(XmlWriter writer, StructureXmlBuilder structureXmlBuilderBean)
            : base(writer)
        {
            this._structureXmlBuilderBean = structureXmlBuilderBean;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineV2" /> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="prettyFy">controls if output should be pretty (indented and no duplicate namespaces)</param>
        /// <param name="structureXmlBuilderBean">The structure XML builder bean.</param>
        public StructureWriterEngineV2(Stream outputStream, bool prettyFy, StructureXmlBuilder structureXmlBuilderBean)
            : base(SdmxSchemaEnumType.VersionTwo, outputStream, prettyFy)
        {
            this._structureXmlBuilderBean = structureXmlBuilderBean;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineV2" /> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="structureXmlBuilderBean">The structure XML builder bean.</param>
        public StructureWriterEngineV2(Stream outputStream, StructureXmlBuilder structureXmlBuilderBean)
            : base(SdmxSchemaEnumType.VersionTwo, outputStream, true)
        {
            this._structureXmlBuilderBean = structureXmlBuilderBean;
        }

        /// <summary>
        ///     Build the XSD generated class objects from the specified <paramref name="beans" />
        /// </summary>
        /// <param name="beans">
        ///     The beans.
        /// </param>
        /// <returns>
        ///     the XSD generated class objects from the specified <paramref name="beans" />
        /// </returns>
        public override XNode Build(ISdmxObjects beans)
        {
            return this._structureXmlBuilderBean.Build(beans).Untyped;
        }
    }
}