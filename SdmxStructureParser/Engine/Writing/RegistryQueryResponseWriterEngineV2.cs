// -----------------------------------------------------------------------
// <copyright file="RegistryQueryResponseWriterEngineV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2;

    /// <summary>
    ///     The registry query response writing engine v 2.
    /// </summary>
    public class RegistryQueryResponseWriterEngineV2 : AbstractWritingEngine
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(RegistryQueryResponseWriterEngineV2));

        /// <summary>
        ///     The query structure response builder v 2.
        /// </summary>
        private readonly QueryStructureResponseBuilderV2 _queryStructureResponseBuilderV2;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistryQueryResponseWriterEngineV2" /> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="queryStructureResponseBuilderV2">The query structure response builder v2.</param>
        /// <exception cref="ArgumentNullException"><paramref name="writer" /> is null</exception>
        public RegistryQueryResponseWriterEngineV2(XmlWriter writer, QueryStructureResponseBuilderV2 queryStructureResponseBuilderV2)
            : base(writer)
        {
            this._queryStructureResponseBuilderV2 = queryStructureResponseBuilderV2;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistryQueryResponseWriterEngineV2" /> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="prettyFy">controls if output should be pretty (indented and no duplicate namespaces)</param>
        /// <param name="queryStructureResponseBuilderV2">The query structure response builder v2.</param>
        public RegistryQueryResponseWriterEngineV2(Stream outputStream, bool prettyFy, QueryStructureResponseBuilderV2 queryStructureResponseBuilderV2)
            : base(SdmxSchemaEnumType.VersionTwo, outputStream, prettyFy)
        {
            this._queryStructureResponseBuilderV2 = queryStructureResponseBuilderV2;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistryQueryResponseWriterEngineV2" /> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="queryStructureResponseBuilderV2">The query structure response builder v2.</param>
        public RegistryQueryResponseWriterEngineV2(Stream outputStream, QueryStructureResponseBuilderV2 queryStructureResponseBuilderV2)
            : base(SdmxSchemaEnumType.VersionTwo, outputStream, true)
        {
            this._queryStructureResponseBuilderV2 = queryStructureResponseBuilderV2;
        }

        /// <summary>
        ///     Build the XSD generated class objects from the specified <paramref name="beans" />
        /// </summary>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <returns>
        ///     the XSD generated class objects from the specified <paramref name="beans" />
        /// </returns>
        public override XNode Build(ISdmxObjects beans)
        {
            _log.Info("Write structures as a SDMX 2.0 Registry response message");
            return this._queryStructureResponseBuilderV2.BuildSuccessResponse(beans).Untyped;
        }
    }
}