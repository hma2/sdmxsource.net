﻿// -----------------------------------------------------------------------
// <copyright file="SchemaLocationWriter.cs" company="EUROSTAT">
//   Date Created : 2013-06-06
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    using Xml.Schema.Linq;

    /// <summary>
    /// The Schema location writer
    /// </summary>
    public class SchemaLocationWriter
    {
        /// <summary>
        /// The schema location map
        /// </summary>
        private readonly Dictionary<SdmxSchema, string> _schemaLocationMap = new Dictionary<SdmxSchema, string>();

        /// <summary>
        /// Gets the schema location.
        /// </summary>
        /// <param name="schemaVersion">The schema version.</param>
        /// <returns>The location string</returns>
        public string GetSchemaLocation(SdmxSchema schemaVersion)
        {
            string location;
            if (this._schemaLocationMap.TryGetValue(schemaVersion, out location))
            {
                return location;
            }

            return null;
        }

        /// <summary>
        /// Writes the schema location.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="namespaceUri">The namespace URI.</param>
        /// <exception cref="ArgumentNullException"><paramref name="doc"/> is <see langword="null" />.</exception>
        public void WriteSchemaLocation(XNode doc, string[] namespaceUri)
        {
            if (doc == null)
            {
                throw new ArgumentNullException("doc");
            }

            if (namespaceUri == null)
            {
                return;
            }

            StringBuilder schemaLocation = new StringBuilder();

            string concat = string.Empty;
            foreach (string currentNamespaceUri in namespaceUri)
            {
                var schemaVersion = SdmxConstants.GetSchemaVersion(currentNamespaceUri);

                // Base location of schema for version e.g. http://www.sss.sss/schema/
                string schemaBaseLocation = this.GetSchemaLocation(schemaVersion);
                string schemaName = SdmxConstants.GetSchemaName(currentNamespaceUri);
                schemaLocation.Append(concat + currentNamespaceUri + " " + concat + schemaBaseLocation + schemaName);
                concat = "\r\n";
            }

            doc.Document.Root.SetAttributeValue(
                XName.Get("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation"), 
                schemaLocation.ToString());
        }

        /// <summary>
        /// Adds the schema location.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="location">The location.</param>
        /// <exception cref="ArgumentNullException"><paramref name="sdmxSchema"/> is <see langword="null" />.</exception>
        public void AddSchemaLocation(SdmxSchema sdmxSchema, string location)
        {
            if (sdmxSchema == null)
            {
                throw new ArgumentNullException("sdmxSchema");
            }

            if (location == null)
            {
                throw new ArgumentNullException("location");
            }

            this._schemaLocationMap[sdmxSchema] = location;
        }
    }
}