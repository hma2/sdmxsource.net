// -----------------------------------------------------------------------
// <copyright file="UpdateNamespace.cs" company="EUROSTAT">
//   Date Created : 2018-01-31
//   Copyright (c) 2012, 2018 by the European Union. 
// 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
// 
//     This file is part of SdmxStructureParser.
// 
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
// 
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    using Api.Exception;

    internal class UpdateNamespace
    {
        private readonly List<XNamespace> _namespaces = new List<XNamespace>();

        public void For(XNode doc)
        {
            var root = doc as XElement;
            if (root == null)
            {
                var xDocument = doc as XDocument;
                if (xDocument != null)
                {
                    root = xDocument.Root;
                }
            }

            if (root == null)
            {
                throw new SdmxException("Could not find the root element of document");
            }
            
            //We traverse the root in order to find all namespaces used
            this.ExtractNamespaces(root);
            //We add the namespaces used as attributes to the root node.
            foreach (var ns in this._namespaces)
            {
                var prefix = ns.NamespaceName.Split('/').Last();
                if (root.Attributes().All(x => x.Value != ns.NamespaceName))
                {
                    root.Add(new XAttribute(XNamespace.Xmlns + prefix, ns.NamespaceName));
                }
            }
        }

        private void ExtractNamespaces(XElement e)
        {
            if (!string.IsNullOrWhiteSpace(e.Name.NamespaceName))
            {
                if (!this._namespaces.Contains(e.Name.NamespaceName))
                {
                    XNamespace xmlNamespace = e.Name.NamespaceName;
                    this._namespaces.Add(xmlNamespace);
                }
            }
            foreach (var node in e.Nodes())
            {
                var element = node as XElement;
                if (element != null)
                {
                    this.ExtractNamespaces(element);
                }
            }
        }
    }
}