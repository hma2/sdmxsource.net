// -----------------------------------------------------------------------
// <copyright file="StructureWriterEngineEdi.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing
{
    using System;
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.EdiParser.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     Not implemented.
    /// </summary>
    public class StructureWriterEngineEdi : IStructureWriterEngine
    {
        /// <summary>
        /// The _edi parse manager
        /// </summary>
        private readonly IEdiParseManager _ediParseManager;

        /// <summary>
        ///     The output stream.
        /// </summary>
        private readonly Stream _outputStream;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWriterEngineEdi" /> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="ediParseManager">The edi parse manager.</param>
        /// <exception cref="ArgumentNullException"><paramref name="ediParseManager"/> is <see langword="null" />.</exception>
        public StructureWriterEngineEdi(Stream outputStream, IEdiParseManager ediParseManager)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            if (ediParseManager == null)
            {
                throw new ArgumentNullException("ediParseManager");
            }

            this._outputStream = outputStream;
            this._ediParseManager = ediParseManager;
        }

        /// <summary>
        ///     The write structure.
        /// </summary>
        /// <param name="bean">
        ///     The maintainable object.
        /// </param>
        public void WriteStructure(IMaintainableObject bean)
        {
            ISdmxObjects beans = new SdmxObjectsImpl();
            beans.AddIdentifiable(bean);
            this.WriteStructures(beans);
        }

        /// <summary>
        ///     The write structures.
        /// </summary>
        /// <param name="beans">
        ///     The beans.
        /// </param>
        public void WriteStructures(ISdmxObjects beans)
        {
            this._ediParseManager.WriteToEdi(beans, this._outputStream);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
        }
    }
}