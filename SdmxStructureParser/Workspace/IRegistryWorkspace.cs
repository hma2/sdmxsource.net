// -----------------------------------------------------------------------
// <copyright file="IRegistryWorkspace.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Workspace
{
    using Org.Sdmxsource.Sdmx.Api.Model;

    /// <summary>
    ///     The structure workspace holds reference to a SubmitRegistryRequest document,
    ///     the contents of this document can be Structures, Registrations or Provisions
    /// </summary>
    public interface IRegistryWorkspace
    {
        /// <summary>
        ///     Gets the provision workspace for this workspace
        /// </summary>
        IProvisionWorkspace ProvisionWorkspace { get; }

        /// <summary>
        ///     Gets the registration workspace for this workspace
        /// </summary>
        IRegistrationWorkspace RegistrationWorkspace { get; }

        /// <summary>
        ///     Gets the structure workspace for this workspace
        /// </summary>
        IStructureWorkspace StructureWorkspace { get; }

        /// <summary>
        ///     Returns true if getProvisionWorkspace() returns a not null object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool HasProvisionWorkspace();

        /// <summary>
        ///     Returns true if getRegistrationWorkspace() returns a not null object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool HasRegitrationWorkspace();

        /// <summary>
        ///     Returns true if getStructureWorkspace() returns a not null object
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        bool HasStructureWorkspace();
    }
}