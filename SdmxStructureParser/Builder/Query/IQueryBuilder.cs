// -----------------------------------------------------------------------
// <copyright file="IQueryBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Query
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;

    using QueryRegistrationRequestType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry.QueryRegistrationRequestType;

    /// <summary>
    ///     Builds Query reference objects from SDMX query messages.
    ///     The SDMX queries can be structure queries, registration queries or provision queries
    /// </summary>
    public interface IQueryBuilder
    {
        /// <summary>
        ///     Builds a list of structure references from a version 2.0 registry query structure request message
        /// </summary>
        /// <param name="queryStructureRequests">
        ///     The Structure query
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        IList<IStructureReference> Build(QueryStructureRequestType queryStructureRequests);

        /// <summary>
        ///     Builds a list of provision references from a version 2.0 registry query registration request message
        /// </summary>
        /// <param name="queryRegistrationRequestType">
        ///     The query Registration Request Type.
        /// </param>
        /// <returns>
        ///     provision references
        /// </returns>
        IStructureReference Build(QueryRegistrationRequestType queryRegistrationRequestType);

        /// <summary>
        ///     Builds a list of provision references from a version 2.1 registry query registration request message
        /// </summary>
        /// <param name="queryRegistrationRequestType">
        ///     Query registration request type
        /// </param>
        /// <returns>
        ///     provision references
        /// </returns>
        IStructureReference Build(
            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.QueryRegistrationRequestType queryRegistrationRequestType);

        /// <summary>
        ///     Builds a list of provision references from a version 2.0 registry query provision request message
        /// </summary>
        /// <param name="queryProvisionRequestType">
        ///     The query for provision
        /// </param>
        /// <returns>
        ///     provision references
        /// </returns>
        IStructureReference Build(QueryProvisioningRequestType queryProvisionRequestType);

        /// <summary>
        ///     Builds a list of structure references from a version 1.0 query message
        /// </summary>
        /// <param name="queryMessage">
        ///     The query message
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        IList<IStructureReference> Build(QueryMessageType queryMessage);

        /// <summary>
        ///     Builds a list of structure references from a version 2.0 query message
        /// </summary>
        /// <param name="queryMessage">
        ///     The query message
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        IList<IStructureReference> Build(Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.QueryMessageType queryMessage);

        /// <summary>
        /// Builds the specified codelist query MSG.
        /// </summary>
        /// <param name="codelistQueryMsg">The codelist query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(CodelistQueryType codelistQueryMsg);

        /// <summary>
        /// Builds the specified dataflow query MSG.
        /// </summary>
        /// <param name="dataflowQueryMsg">The dataflow query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(DataflowQueryType dataflowQueryMsg);

        /// <summary>
        /// Builds the specified metadataflow query MSG.
        /// </summary>
        /// <param name="metadataflowQueryMsg">The metadataflow query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(MetadataflowQueryType metadataflowQueryMsg);

        /// <summary>
        /// Builds the specified data structure query MSG.
        /// </summary>
        /// <param name="dataStructureQueryMsg">The data structure query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(DataStructureQueryType dataStructureQueryMsg);

        /// <summary>
        /// Builds the specified metadata structure query MSG.
        /// </summary>
        /// <param name="metadataStructureQueryMsg">The metadata structure query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(MetadataStructureQueryType metadataStructureQueryMsg);

        /// <summary>
        /// Builds the specified category scheme query MSG.
        /// </summary>
        /// <param name="categorySchemeQueryMsg">The category scheme query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(CategorySchemeQueryType categorySchemeQueryMsg);

        /// <summary>
        /// Builds the specified concept scheme query MSG.
        /// </summary>
        /// <param name="conceptSchemeQueryMsg">The concept scheme query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(ConceptSchemeQueryType conceptSchemeQueryMsg);

        /// <summary>
        /// Builds the specified hierarchical codelist query MSG.
        /// </summary>
        /// <param name="hierarchicalCodelistQueryMsg">The hierarchical codelist query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(HierarchicalCodelistQueryType hierarchicalCodelistQueryMsg);

        /// <summary>
        /// Builds the specified organisation scheme query MSG.
        /// </summary>
        /// <param name="organisationSchemeQueryMsg">The organisation scheme query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(OrganisationSchemeQueryType organisationSchemeQueryMsg);

        /// <summary>
        /// Builds the specified reporting taxonomy query MSG.
        /// </summary>
        /// <param name="reportingTaxonomyQueryMsg">The reporting taxonomy query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(ReportingTaxonomyQueryType reportingTaxonomyQueryMsg);

        /// <summary>
        /// Builds the specified structure set query MSG.
        /// </summary>
        /// <param name="structureSetQueryMsg">The structure set query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(StructureSetQueryType structureSetQueryMsg);

        /// <summary>
        /// Builds the specified process query MSG.
        /// </summary>
        /// <param name="processQueryMsg">The process query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(ProcessQueryType processQueryMsg);

        /// <summary>
        /// Builds the specified categorisation query MSG.
        /// </summary>
        /// <param name="categorisationQueryMsg">The categorisation query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(CategorisationQueryType categorisationQueryMsg);

        /// <summary>
        /// Builds the specified provision agreement query MSG.
        /// </summary>
        /// <param name="provisionAgreementQueryMsg">The provision agreement query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(ProvisionAgreementQueryType provisionAgreementQueryMsg);

        /// <summary>
        /// Builds the specified constraint query MSG.
        /// </summary>
        /// <param name="constraintQueryMsg">The constraint query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(ConstraintQueryType constraintQueryMsg);

        /// <summary>
        /// Builds the specified structures query MSG.
        /// </summary>
        /// <param name="structuresQueryMsg">The structures query MSG.</param>
        /// <returns>The structure query</returns>
        IComplexStructureQuery Build(StructuresQueryType structuresQueryMsg);
    }
}