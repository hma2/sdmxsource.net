// -----------------------------------------------------------------------
// <copyright file="QueryBuilderV21.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Query
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    using CategorisationQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.CategorisationQueryType;
    using CategorySchemeQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.CategorySchemeQueryType;
    using CodelistQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.CodelistQueryType;
    using ConceptSchemeQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.ConceptSchemeQueryType;
    using ConstraintQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.ConstraintQueryType;
    using DataflowQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.DataflowQueryType;
    using DataStructureQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.DataStructureQueryType;
    using HierarchicalCodelistQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.HierarchicalCodelistQueryType;
    using MetadataflowQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.MetadataflowQueryType;
    using MetadataStructureQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.MetadataStructureQueryType;
    using OrganisationSchemeQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.OrganisationSchemeQueryType;
    using ProcessQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.ProcessQueryType;
    using ProvisionAgreementQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.ProvisionAgreementQueryType;
    using QueryRegistrationRequestType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.QueryRegistrationRequestType;
    using ReportingTaxonomyQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.ReportingTaxonomyQueryType;
    using StructureSetQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.StructureSetQueryType;
    using StructuresQueryType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.StructuresQueryType;
    using TimeRangeCore = Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex.TimeRangeCore;

    /// <summary>
    ///     The query bean builder v 21.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", 
        Justification = "It is OK. SDMX v2.0 uses different types for each artefact reference and we want to stay as close to the Java design.")]
    public class QueryBuilderV21
    {
        /// <summary>
        ///     Builds a list of provision references from a version 2.1 registry query registration request message
        /// </summary>
        /// <param name="queryRegistrationRequestType">
        ///     The query Registration Request Type.
        /// </param>
        /// <returns>
        ///     provision references
        /// </returns>
        /// <exception cref="SdmxSemmanticException">
        ///     Throws Validate exception.
        /// </exception>
        public IStructureReference Build(QueryRegistrationRequestType queryRegistrationRequestType)
        {
            if (queryRegistrationRequestType != null)
            {
                if (queryRegistrationRequestType.All != null)
                {
                    return new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ProvisionAgreement));
                }

                DataflowReferenceType dataflowRef = queryRegistrationRequestType.Dataflow;
                DataProviderReferenceType dataProviderRef = queryRegistrationRequestType.DataProvider;
                ProvisionAgreementReferenceType provRef = queryRegistrationRequestType.ProvisionAgreement;
                MetadataflowReferenceType mdfRef = queryRegistrationRequestType.Metadataflow;

                if (dataProviderRef != null)
                {
                    return RefUtil.CreateReference(dataProviderRef);
                }

                if (provRef != null)
                {
                    return RefUtil.CreateReference(provRef);
                }

                if (dataflowRef != null)
                {
                    return RefUtil.CreateReference(dataflowRef);
                }

                if (mdfRef != null)
                {
                    return RefUtil.CreateReference(mdfRef);
                }
            }

            return null;
        }

        /// <summary>
        /// Builds the specified dataflow query MSG.
        /// </summary>
        /// <param name="dataflowQueryMsg">The dataflow query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="System.ArgumentNullException">The <paramref name="dataflowQueryMsg"/>   is null.</exception>
        public IComplexStructureQuery Build(DataflowQueryType dataflowQueryMsg)
        {
            if (dataflowQueryMsg == null)
            {
                throw new ArgumentNullException("dataflowQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.DataflowQueryType query = dataflowQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified metadataflow query MSG.
        /// </summary>
        /// <param name="metadataflowQueryMsg">The metadataflow query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="metadataflowQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(MetadataflowQueryType metadataflowQueryMsg)
        {
            if (metadataflowQueryMsg == null)
            {
                throw new ArgumentNullException("metadataflowQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.MetadataflowQueryType query = metadataflowQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified data structure query MSG.
        /// </summary>
        /// <param name="dataStructureQueryMsg">The data structure query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="dataStructureQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(DataStructureQueryType dataStructureQueryMsg)
        {
            if (dataStructureQueryMsg == null)
            {
                throw new ArgumentNullException("dataStructureQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.DataStructureQueryType query = dataStructureQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified metadata structure query MSG.
        /// </summary>
        /// <param name="metadataStructureQueryMsg">The metadata structure query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="metadataStructureQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(MetadataStructureQueryType metadataStructureQueryMsg)
        {
            if (metadataStructureQueryMsg == null)
            {
                throw new ArgumentNullException("metadataStructureQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.MetadataStructureQueryType query = metadataStructureQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified category scheme query MSG.
        /// </summary>
        /// <param name="categorySchemeQueryMsg">The category scheme query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="categorySchemeQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(CategorySchemeQueryType categorySchemeQueryMsg)
        {
            if (categorySchemeQueryMsg == null)
            {
                throw new ArgumentNullException("categorySchemeQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.CategorySchemeQueryType query = categorySchemeQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process codelist specific clauses.
            CategorySchemeWhereType categorySchemeWhere = (CategorySchemeWhereType)query.StructuralMetadataWhere.Content;

            // .GetCategorySchemeWhere();
            IList<ItemWhere> itemWhereList = categorySchemeWhere.ItemWhere;
            IComplexIdentifiableReferenceObject childRef = null;
            if (itemWhereList != null && itemWhereList.Count > 0)
            {
                if (itemWhereList.Count > 1)
                {
                    // TODO warning or error that is not supported?????
                }

                childRef = this.BuildIdentifiableReference(itemWhereList[0].Content, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Category));
            }

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, childRef);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified concept scheme query MSG.
        /// </summary>
        /// <param name="conceptSchemeQueryMsg">The concept scheme query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="conceptSchemeQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(ConceptSchemeQueryType conceptSchemeQueryMsg)
        {
            if (conceptSchemeQueryMsg == null)
            {
                throw new ArgumentNullException("conceptSchemeQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.ConceptSchemeQueryType query = conceptSchemeQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process codelist specific clauses.
            ConceptSchemeWhereType conceptSchemeWhere = (ConceptSchemeWhereType)query.StructuralMetadataWhere.Content;

            // .getConceptSchemeWhere();
            IList<ItemWhere> itemWhereList = conceptSchemeWhere.ItemWhere;
            IComplexIdentifiableReferenceObject childRef = null;
            if (itemWhereList != null && itemWhereList.Count > 0)
            {
                if (itemWhereList.Count > 1)
                {
                    // TODO warning or error that is not supported?????
                }

                childRef = this.BuildIdentifiableReference(itemWhereList[0].Content, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept));
            }

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, childRef);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified codelist query MSG.
        /// </summary>
        /// <param name="codelistQueryMsg">The codelist query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="codelistQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(CodelistQueryType codelistQueryMsg)
        {
            if (codelistQueryMsg == null)
            {
                throw new ArgumentNullException("codelistQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.CodelistQueryType query = codelistQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process codelist specific clauses.
            CodelistWhereType codelistWhere = (CodelistWhereType)query.StructuralMetadataWhere.Content;

            // .getCodelistWhere();
            IList<ItemWhere> itemWhereList = codelistWhere.ItemWhere;
            IComplexIdentifiableReferenceObject childRef = null;
            if (itemWhereList != null && itemWhereList.Count > 0)
            {
                if (itemWhereList.Count > 1)
                {
                    // TODO warning or error that is not supported?????
                }

                childRef = this.BuildIdentifiableReference(itemWhereList[0].Content, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Code));
            }

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, childRef);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified hierarchical codelist query MSG.
        /// </summary>
        /// <param name="hierarchicalCodelistQueryMsg">The hierarchical codelist query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="hierarchicalCodelistQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(HierarchicalCodelistQueryType hierarchicalCodelistQueryMsg)
        {
            if (hierarchicalCodelistQueryMsg == null)
            {
                throw new ArgumentNullException("hierarchicalCodelistQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.HierarchicalCodelistQueryType query = hierarchicalCodelistQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified organisation scheme query MSG.
        /// </summary>
        /// <param name="organisationSchemeQueryMsg">The organisation scheme query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="System.ArgumentNullException">The <paramref name="organisationSchemeQueryMsg"/> is null.</exception>
        /// <exception cref="System.ArgumentException">An organisation scheme type expected instead of:  + orgSchemeType</exception>
        public IComplexStructureQuery Build(OrganisationSchemeQueryType organisationSchemeQueryMsg)
        {
            if (organisationSchemeQueryMsg == null)
            {
                throw new ArgumentNullException("organisationSchemeQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.OrganisationSchemeQueryType query = organisationSchemeQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process OrganisationScheme specific clauses.
            OrganisationSchemeWhereType organisationSchemeWhere = (OrganisationSchemeWhereType)query.StructuralMetadataWhere.Content; // .getOrganisationSchemeWhere();
            string orgSchemeStr = organisationSchemeWhere.type;
            SdmxStructureEnumType orgSchemeType;
            if (orgSchemeStr.Equals("OrganisationScheme"))
            {
                orgSchemeType = SdmxStructureEnumType.OrganisationScheme;
            }
            else
            {
                orgSchemeType = SdmxStructureType.ParseClass(orgSchemeStr).EnumType;
            }

            SdmxStructureType orgType;
            switch (orgSchemeType)
            {
                case SdmxStructureEnumType.AgencyScheme:
                    orgType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Agency);
                    break;
                case SdmxStructureEnumType.DataConsumerScheme:
                    orgType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumer);
                    break;
                case SdmxStructureEnumType.DataProviderScheme:
                    orgType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider);
                    break;
                case SdmxStructureEnumType.OrganisationUnitScheme:
                    orgType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnit);
                    break;
                case SdmxStructureEnumType.OrganisationScheme:
                    orgType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Organisation);
                    break;
                default:
                    throw new ArgumentException("An organisation scheme type expected instead of: " + orgSchemeType);
            }

            IList<ItemWhere> itemWhereList = organisationSchemeWhere.ItemWhere;
            IComplexIdentifiableReferenceObject childRef = null;
            if (itemWhereList != null && itemWhereList.Count > 0)
            {
                if (itemWhereList.Count > 1)
                {
                    // TODO warning or error that is not supported?????
                }

                childRef = this.BuildIdentifiableReference(itemWhereList[0].Content, orgType);
            }

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, childRef);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified reporting taxonomy query MSG.
        /// </summary>
        /// <param name="reportingTaxonomyQueryMsg">The reporting taxonomy query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="reportingTaxonomyQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(ReportingTaxonomyQueryType reportingTaxonomyQueryMsg)
        {
            if (reportingTaxonomyQueryMsg == null)
            {
                throw new ArgumentNullException("reportingTaxonomyQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.ReportingTaxonomyQueryType query = reportingTaxonomyQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process codelist specific clauses.
            ReportingTaxonomyWhereType reportingTaxonomyWhere = (ReportingTaxonomyWhereType)query.StructuralMetadataWhere.Content; // .getReportingTaxonomyWhere();
            IList<ItemWhere> itemWhereList = reportingTaxonomyWhere.ItemWhere;
            IComplexIdentifiableReferenceObject childRef = null;
            if (itemWhereList != null && itemWhereList.Count > 0)
            {
                if (itemWhereList.Count > 1)
                {
                    // TODO warning or error that is not supported?????
                }

                childRef = this.BuildIdentifiableReference(itemWhereList[0].Content, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingCategory));
            }

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, childRef);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified structure set query MSG.
        /// </summary>
        /// <param name="structureSetQueryMsg">The structure set query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="structureSetQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(StructureSetQueryType structureSetQueryMsg)
        {
            if (structureSetQueryMsg == null)
            {
                throw new ArgumentNullException("structureSetQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.StructureSetQueryType query = structureSetQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified process query MSG.
        /// </summary>
        /// <param name="processQueryMsg">The process query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="processQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(ProcessQueryType processQueryMsg)
        {
            if (processQueryMsg == null)
            {
                throw new ArgumentNullException("processQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.ProcessQueryType query = processQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified categorisation query MSG.
        /// </summary>
        /// <param name="categorisationQueryMsg">The categorisation query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="categorisationQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(CategorisationQueryType categorisationQueryMsg)
        {
            if (categorisationQueryMsg == null)
            {
                throw new ArgumentNullException("categorisationQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.CategorisationQueryType query = categorisationQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified provision agreement query MSG.
        /// </summary>
        /// <param name="provisionAgreementQueryMsg">The provision agreement query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="provisionAgreementQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(ProvisionAgreementQueryType provisionAgreementQueryMsg)
        {
            if (provisionAgreementQueryMsg == null)
            {
                throw new ArgumentNullException("provisionAgreementQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.ProvisionAgreementQueryType query = provisionAgreementQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified constraint query MSG.
        /// </summary>
        /// <param name="constraintQueryMsg">The constraint query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="constraintQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(ConstraintQueryType constraintQueryMsg)
        {
            if (constraintQueryMsg == null)
            {
                throw new ArgumentNullException("constraintQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.ConstraintQueryType query = constraintQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the specified structures query MSG.
        /// </summary>
        /// <param name="structuresQueryMsg">The structures query MSG.</param>
        /// <returns>The <see cref="IComplexStructureQuery"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="structuresQueryMsg"/> is <see langword="null" />.</exception>
        public IComplexStructureQuery Build(StructuresQueryType structuresQueryMsg)
        {
            if (structuresQueryMsg == null)
            {
                throw new ArgumentNullException("structuresQueryMsg");
            }

            Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query.StructuresQueryType query = structuresQueryMsg.SdmxQuery;

            // process details
            StructureReturnDetailsType returnDetails = query.ReturnDetails;
            IComplexStructureQueryMetadata queryMetadata = BuildQueryDetails(returnDetails);

            // process common clauses for all maintainables along with specific from child references.
            IComplexStructureReferenceObject structureRef = BuildMaintainableWhere(query.StructuralMetadataWhere.Content, null);
            return new ComplexStructureQueryCore(structureRef, queryMetadata);
        }

        /// <summary>
        /// Builds the annotation reference from XML beans values.
        /// </summary>
        /// <param name="annotationWhereType">Type of the annotation where.</param>
        /// <returns>The <see cref="IComplexAnnotationReference"/></returns>
        private static IComplexAnnotationReference BuildAnnotationReference(AnnotationWhereType annotationWhereType)
        {
            if (annotationWhereType == null)
            {
                return null;
            }

            QueryStringType type = annotationWhereType.Type;
            IComplexTextReference typeRef = null;
            if (type != null)
            {
                typeRef = BuildTextReference(null, type.@operator.ToString(), type.TypedValue);
            }

            QueryStringType title = annotationWhereType.Title;
            IComplexTextReference titleRef = null;
            if (title != null)
            {
                titleRef = BuildTextReference(null, title.@operator.ToString(), title.TypedValue);
            }

            QueryTextType text = annotationWhereType.Text;
            IComplexTextReference textRef = null;
            if (text != null)
            {
                textRef = BuildTextReference(text.lang, text.@operator.ToString(), text.TypedValue);
            }

            return new ComplexAnnotationReferenceCore(typeRef, titleRef, textRef);
        }

        /// <summary>
        /// Builds the maintainable where.
        /// </summary>
        /// <param name="maintainableWhere">The maintainable where.</param>
        /// <param name="childRef">The child reference.</param>
        /// <returns>The <see cref="IComplexStructureReferenceObject"/></returns>
        private static IComplexStructureReferenceObject BuildMaintainableWhere(MaintainableWhereType maintainableWhere, IComplexIdentifiableReferenceObject childRef)
        {
            QueryNestedIDType agencyIDType = maintainableWhere.AgencyID;
            IComplexTextReference agencyId = null;
            if (agencyIDType != null)
            {
                agencyId = BuildTextReference(null, agencyIDType.@operator.ToString(), agencyIDType.TypedValue);
            }

            QueryIDType queryIDType = maintainableWhere.ID;
            IComplexTextReference id = null;
            if (queryIDType != null)
            {
                id = BuildTextReference(null, queryIDType.@operator.ToString(), queryIDType.TypedValue);
            }

            IComplexVersionReference versionRef = BuildVersionReference(
                maintainableWhere.Version != null ? maintainableWhere.Version.ToString() : null, 
                maintainableWhere.VersionFrom, 
                maintainableWhere.VersionTo);

            SdmxStructureType structureType;
            if (maintainableWhere.type.Equals("OrganisationScheme"))
            {
                // hack. This checked are done since it is not identifiable no element name in enum and cannot use parseClass()
                structureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationScheme);
            }
            else if (maintainableWhere.type.Equals("Constraint"))
            {
                structureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ContentConstraint);
            }
            else if (maintainableWhere.type.Equals("Any"))
            {
                structureType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Any);
            }
            else
            {
                structureType = SdmxStructureType.ParseClass(maintainableWhere.type);
            }

            IComplexAnnotationReference annotationRef = BuildAnnotationReference(maintainableWhere.Annotation);

            QueryTextType nameType = maintainableWhere.Name;
            IComplexTextReference nameRef = null;
            if (nameType != null)
            {
                nameRef = BuildTextReference(nameType.lang, nameType.@operator.ToString(), nameType.TypedValue);
            }

            QueryTextType descriptionType = maintainableWhere.Description;
            IComplexTextReference descriptionRef = null;
            if (descriptionType != null)
            {
                descriptionRef = BuildTextReference(descriptionType.lang, descriptionType.@operator.ToString(), descriptionType.TypedValue);
            }

            return new ComplexStructureReferenceCore(agencyId, id, versionRef, structureType, annotationRef, nameRef, descriptionRef, childRef);
        }

        /// <summary>
        /// Builds the query details from the return detail part of the query message.
        /// </summary>
        /// <param name="returnDetails">The return details.</param>
        /// <returns>The <see cref="IComplexStructureQueryMetadata"/></returns>
        private static IComplexStructureQueryMetadata BuildQueryDetails(StructureReturnDetailsType returnDetails)
        {
            bool returnMatchedartefact = returnDetails.returnMatchedArtefact;

            ComplexStructureQueryDetail queryDetail = ComplexStructureQueryDetail.ParseString(returnDetails.detail);

            ReferencesType references = returnDetails.References;

            ComplexMaintainableQueryDetail referencesQueryDetail;
            if (references.detail != null)
            {
                referencesQueryDetail = ComplexMaintainableQueryDetail.ParseString(references.detail);
            }
            else
            {
                referencesQueryDetail = ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Full);
            }

            StructureReferenceDetail referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Null);
            List<SdmxStructureType> referenceSpecificStructures = null;

            if (references.All != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.All);
            }
            else if (references.Children != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Children);
            }
            else if (references.Descendants != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Descendants);
            }
            else if (references.None != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None);
            }
            else if (references.Parents != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Parents);
            }
            else if (references.ParentsAndSiblings != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.ParentsSiblings);
            }
            else if (references.SpecificObjects != null)
            {
                referenceDetail = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Specific);

                referenceSpecificStructures = new List<SdmxStructureType>();

                MaintainableObjectTypeListType specificObjects = references.SpecificObjects;

                // domNode = specificObjects.getDomNode();
                IEnumerable<XNode> childNodes = specificObjects.Untyped.DescendantNodes();
                foreach (var node in childNodes)
                {
                    var item = node as XElement;
                    if (item != null)
                    {
                        var tagName = item.Name.LocalName;
                        if (!string.IsNullOrWhiteSpace(tagName))
                        {
                            referenceSpecificStructures.Add(SdmxStructureType.ParseClass(tagName));
                        }
                    }
                }
            }

            return new ComplexStructureQueryMetadataCore(returnMatchedartefact, queryDetail, referencesQueryDetail, referenceDetail, referenceSpecificStructures);
        }

        /// <summary>
        /// Builds the text reference from values acquired from XML beans. Check if empty or unset to check default values according to XSD.
        /// </summary>
        /// <param name="lang">The language.</param>
        /// <param name="textOperator">The s operator.</param>
        /// <param name="value">The value.</param>
        /// <returns>The <see cref="IComplexTextReference"/></returns>
        private static IComplexTextReference BuildTextReference(string lang, string textOperator, string value)
        {
            string emptyCheckedLang = "en";
            if (lang != null && lang.Trim().Length > 1)
            {
                emptyCheckedLang = lang;
            }

            TextSearch defaultCheckedOperator = TextSearch.GetFromEnum(TextSearchEnumType.Equal);
            if (textOperator != null && textOperator.Trim().Length > 1)
            {
                defaultCheckedOperator = TextSearch.ParseString(textOperator);
            }

            return new ComplexTextReferenceCore(emptyCheckedLang, defaultCheckedOperator, value);
        }

        /// <summary>
        /// Builds the time range from time range value from XML.
        /// </summary>
        /// <param name="timeRangeValueType">Type of the time range value.</param>
        /// <returns>The <see cref="ITimeRange"/></returns>
        private static ITimeRange BuildTimeRange(TimeRangeValueType timeRangeValueType)
        {
            if (timeRangeValueType == null)
            {
                return null;
            }

            bool range = false;
            ISdmxDate startDate = null;
            ISdmxDate endDate = null;
            bool endInclusive = false;
            bool startInclusive = false;

            if (timeRangeValueType.AfterPeriod != null)
            {
                TimePeriodRangeType afterPeriod = timeRangeValueType.AfterPeriod;
                startDate = new SdmxDateCore(afterPeriod.TypedValue.ToString());
                startInclusive = afterPeriod.isInclusive;
            }
            else if (timeRangeValueType.BeforePeriod != null)
            {
                TimePeriodRangeType beforePeriod = timeRangeValueType.BeforePeriod;
                endDate = new SdmxDateCore(beforePeriod.TypedValue.ToString());
                endInclusive = beforePeriod.isInclusive;
            }
            else
            {
                // case that range is set
                range = true;
                TimePeriodRangeType startPeriod = timeRangeValueType.StartPeriod;
                startDate = new SdmxDateCore(startPeriod.TypedValue.ToString());
                startInclusive = startPeriod.isInclusive;

                TimePeriodRangeType endPeriod = timeRangeValueType.EndPeriod;
                endDate = new SdmxDateCore(endPeriod.TypedValue.ToString());
                endInclusive = endPeriod.isInclusive;
            }

            return new TimeRangeCore(range, startDate, endDate, startInclusive, endInclusive);
        }

        /// <summary>
        /// Builds the version reference.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <param name="versionFromType">Type of the version from.</param>
        /// <param name="versionToType">Type of the version to.</param>
        /// <returns>The <see cref="IComplexVersionReference"/></returns>
        private static IComplexVersionReference BuildVersionReference(string version, TimeRangeValueType versionFromType, TimeRangeValueType versionToType)
        {
            TertiaryBool returnLatest = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.Unset);
            string emptyCheckedString = null;
            if (!string.IsNullOrWhiteSpace(version))
            {
                if (version.Equals("*"))
                {
                    returnLatest = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.True);
                }
                else
                {
                    emptyCheckedString = version;
                    returnLatest = TertiaryBool.GetFromEnum(TertiaryBoolEnumType.False);
                }
            }

            ITimeRange validFrom = BuildTimeRange(versionFromType);
            ITimeRange validTo = BuildTimeRange(versionToType);

            return new ComplexVersionReferenceCore(returnLatest, emptyCheckedString, validFrom, validTo);
        }

        /// <summary>
        /// Builds the identifiable reference.
        /// </summary>
        /// <param name="itemWhereType">Type of the item where.</param>
        /// <param name="itemType">Type of the item.</param>
        /// <returns>The <see cref="IComplexIdentifiableReferenceObject"/></returns>
        private IComplexIdentifiableReferenceObject BuildIdentifiableReference(ItemWhereType itemWhereType, SdmxStructureType itemType)
        {
            if (itemWhereType == null)
            {
                return null;
            }

            QueryIDType idType = itemWhereType.ID;
            IComplexTextReference id = null;
            if (idType != null)
            {
                id = BuildTextReference(null, idType.@operator.ToString(), idType.ToString());
            }

            IComplexAnnotationReference annotationRef = BuildAnnotationReference(itemWhereType.Annotation);

            QueryTextType nameType = itemWhereType.Name;
            IComplexTextReference nameRef = null;
            if (nameType != null)
            {
                nameRef = BuildTextReference(nameType.lang, nameType.@operator.ToString(), nameType.ToString());
            }

            QueryTextType descriptionType = itemWhereType.Description;
            IComplexTextReference descriptionRef = null;
            if (descriptionType != null)
            {
                descriptionRef = BuildTextReference(descriptionType.lang, descriptionType.@operator.ToString(), descriptionType.ToString());
            }

            IComplexIdentifiableReferenceObject childRef = null;
            IList<ItemWhere> itemWhereList = itemWhereType.ItemWhere;
            if (itemWhereList != null && (itemWhereList.Count > 0))
            {
                // this should be the case only for the Categories, ReportingTaxonomies.
                if (itemWhereList.Count > 1)
                {
                    // TODO warning or error that is not supported?????
                }

                childRef = this.BuildIdentifiableReference(itemWhereList[0].Content, itemType);
            }

            return new ComplexIdentifiableReferenceCore(id, itemType, annotationRef, nameRef, descriptionRef, childRef);
        }
    }
}