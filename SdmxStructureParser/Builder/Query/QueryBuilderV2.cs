// -----------------------------------------------------------------------
// <copyright file="QueryBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.Query
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.query;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    using QueryMessageType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message.QueryMessageType;

    /// <summary>
    ///     Singleton factory pattern to build Version 2 reference beans from query types
    /// </summary>
    public class QueryBuilderV2
    {
        /// <summary>
        ///     Build a list of structure references
        /// </summary>
        /// <param name="queryStructureRequests">
        ///     given structure request query containing lists of references for each structure type.
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="queryStructureRequests"/> is <see langword="null" />.</exception>
        public IList<IStructureReference> Build(QueryStructureRequestType queryStructureRequests)
        {
            if (queryStructureRequests == null)
            {
                throw new ArgumentNullException("queryStructureRequests");
            }

            IList<IStructureReference> returnList = new List<IStructureReference>();
            AddAgencyRef(queryStructureRequests, returnList);
            AddCategorySchemeRef(queryStructureRequests, returnList);
            AddCodelistRef(queryStructureRequests, returnList);

            AddConceptSchemeRef(queryStructureRequests, returnList);
            this.AddDataflowRef(queryStructureRequests, returnList);

            AddDataProviderRef(queryStructureRequests, returnList);
            AddHerarchicalCodelistRef(queryStructureRequests, returnList);

            AddKeyfamilyRef(queryStructureRequests, returnList);
            AddMetadataflowRef(queryStructureRequests, returnList);
            AddMetadataStructureRef(queryStructureRequests, returnList);

            AddOrganisationSchemeRef(queryStructureRequests, returnList);

            AddProcessRef(queryStructureRequests, returnList);

            AddReportingTaxonomyRef(queryStructureRequests, returnList);
            AddStructureSetRef(queryStructureRequests, returnList);

            return returnList;
        }

        /// <summary>
        ///     Build a provision agreement reference bean
        ///     If only dataProviderRef is supplied then this is used and the flow type is assumed to be a dataflow
        /// </summary>
        /// <param name="queryRegistrationRequestType">
        ///     given registration request query containing references to its beans
        /// </param>
        /// <returns>
        ///     provision agreement reference bean of query reference, which ever of provision agreement, dataflow, data provider
        ///     or metadataflow is given, in that order.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="queryRegistrationRequestType"/> is <see langword="null" />.</exception>
        /// <exception cref="ArgumentException">Version 2.0 query for registration by provision agreement must use the provision URN</exception>
        public IStructureReference Build(QueryRegistrationRequestType queryRegistrationRequestType)
        {
            if (queryRegistrationRequestType == null)
            {
                throw new ArgumentNullException("queryRegistrationRequestType");
            }

            DataflowRefType dataflowRef = queryRegistrationRequestType.DataflowRef;
            DataProviderRefType dataProviderRef = queryRegistrationRequestType.DataProviderRef;
            MetadataflowRefType metadataflowRef = queryRegistrationRequestType.MetadataflowRef;
            ProvisionAgreementRefType provRef = queryRegistrationRequestType.ProvisionAgreementRef;

            if (dataProviderRef != null)
            {
                if (ObjectUtil.ValidString(dataProviderRef.URN))
                {
                    return new StructureReferenceImpl(dataProviderRef.URN);
                }

                string agencyId = dataProviderRef.OrganisationSchemeAgencyID;
                string maintId = dataProviderRef.OrganisationSchemeID;
                string version = dataProviderRef.Version;
                string id = dataProviderRef.DataProviderID;
                return new StructureReferenceImpl(agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider), id);
            }

            if (provRef != null)
            {
                if (ObjectUtil.ValidString(provRef.URN))
                {
                    return new StructureReferenceImpl(provRef.URN);
                }

                throw new ArgumentException("Version 2.0 query for registration by provision agreement must use the provision URN");
            }

            if (dataflowRef != null)
            {
                if (ObjectUtil.ValidString(dataflowRef.URN))
                {
                    return new StructureReferenceImpl(dataflowRef.URN);
                }

                string agencyId0 = dataflowRef.AgencyID;
                string maintId1 = dataflowRef.DataflowID;
                string version2 = dataflowRef.Version;
                return new StructureReferenceImpl(agencyId0, maintId1, version2, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow));
            }

            if (metadataflowRef != null)
            {
                if (ObjectUtil.ValidString(metadataflowRef.URN))
                {
                    return new StructureReferenceImpl(metadataflowRef.URN);
                }

                string agencyId3 = metadataflowRef.AgencyID;
                string maintId4 = metadataflowRef.MetadataflowID;
                string version5 = metadataflowRef.Version;
                return new StructureReferenceImpl(agencyId3, maintId4, version5, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow));
            }

            return null;
        }

        /// <summary>
        ///     Build a provision agreement reference bean
        /// </summary>
        /// <param name="queryProvisionRequestType">
        ///     given provision request query containing references to its beans
        /// </param>
        /// <returns>
        ///     provision agreement reference bean of query reference, which ever of provision agreement, dataflow, metadataflow
        ///     and data provider is given, in that order.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="queryProvisionRequestType"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxNotImplementedException">.At version 2.0 provisions can only be queries by Provision URN, Dataflow Ref, Data Provider Ref or Metadata Flow Ref</exception>
        public IStructureReference Build(QueryProvisioningRequestType queryProvisionRequestType)
        {
            if (queryProvisionRequestType == null)
            {
                throw new ArgumentNullException("queryProvisionRequestType");
            }

            DataflowRefType dataflowRef = queryProvisionRequestType.DataflowRef;
            DataProviderRefType dataProviderRef = queryProvisionRequestType.DataProviderRef;
            MetadataflowRefType metadataflowRef = queryProvisionRequestType.MetadataflowRef;
            ProvisionAgreementRefType provRef = queryProvisionRequestType.ProvisionAgreementRef;

            if (dataProviderRef != null)
            {
                if (ObjectUtil.ValidString(dataProviderRef.URN))
                {
                    return new StructureReferenceImpl(dataProviderRef.URN);
                }

                string agencyId = dataProviderRef.OrganisationSchemeAgencyID;
                string maintId = dataProviderRef.OrganisationSchemeID;
                string version = dataProviderRef.Version;
                string id = dataProviderRef.DataProviderID;
                return new StructureReferenceImpl(agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider), id);
            }

            if (provRef != null)
            {
                if (ObjectUtil.ValidString(provRef.URN))
                {
                    return new StructureReferenceImpl(provRef.URN);
                }
            }

            if (dataflowRef != null)
            {
                if (ObjectUtil.ValidString(dataflowRef.URN))
                {
                    return new StructureReferenceImpl(dataflowRef.URN);
                }

                string agencyId0 = dataflowRef.AgencyID;
                string maintId1 = dataflowRef.DataflowID;
                string version2 = dataflowRef.Version;
                return new StructureReferenceImpl(agencyId0, maintId1, version2, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow));
            }

            if (metadataflowRef != null)
            {
                if (ObjectUtil.ValidString(metadataflowRef.URN))
                {
                    return new StructureReferenceImpl(metadataflowRef.URN);
                }

                string agencyId3 = metadataflowRef.AgencyID;
                string maintId4 = metadataflowRef.MetadataflowID;
                string version5 = metadataflowRef.Version;
                return new StructureReferenceImpl(agencyId3, maintId4, version5, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow));
            }

            throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "At version 2.0 provisions can only be queries by Provision URN, Dataflow Ref, Data Provider Ref or Metadata Flow Ref");
        }

        /// <summary>
        ///     Build a list of structure references
        /// </summary>
        /// <param name="queryMessage">
        ///     given message query containing lists of references for each structure type.
        /// </param>
        /// <returns>
        ///     list of structure references
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="queryMessage"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxNotImplementedException">Unsupported structure.</exception>
        public IList<IStructureReference> Build(QueryMessageType queryMessage)
        {
            if (queryMessage == null)
            {
                throw new ArgumentNullException("queryMessage");
            }

            IList<IStructureReference> returnList = new List<IStructureReference>();

            if (queryMessage.Query == null)
            {
                return returnList;
            }

            QueryType queryType = queryMessage.Query;
            if (ObjectUtil.ValidCollection(queryMessage.Query.AgencyWhere))
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "AgencyWhere");
            }

            AddCategorySchemeWhere(queryType, returnList);
            AddCodelistWhere(queryType, returnList);
            AddConceptSchemeWhere(queryType, returnList);
            AddConceptWhere(queryType, returnList);
            AddDataflowWhere(queryType, returnList);

            if (queryType.DataProviderWhere != null && queryType.DataProviderWhere.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "DataProviderWhere");
            }

            if (queryType.DataWhere != null && queryType.DataWhere.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "DataWhere");
            }

            AddHierarchicalCodelistWhere(queryType, returnList);
            AddKeyFamilyWhere(queryType, returnList);
            AddMetadataflowWhere(queryType, returnList);

            if (queryType.MetadataWhere != null && queryType.MetadataWhere.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "DataWhere");
            }

            AddOrganisationSchemeWhere(queryType, returnList);
            AddProcessWhere(queryType, returnList);
            AddStructureSetWhere(queryType, returnList);
            AddReportingTaxonomyWhere(queryType, returnList);

            return returnList;
        }

        /// <summary>
        ///     Build dataflow query. Override to alter the default behavior
        /// </summary>
        /// <param name="refType">
        ///     The Dataflow reference (SDMX v2.0
        /// </param>
        /// <returns>
        ///     The <see cref="IStructureReference" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="refType"/> is <see langword="null" />.</exception>
        protected virtual IStructureReference BuildDataflowQuery(DataflowRefType refType)
        {
            if (refType == null)
            {
                throw new ArgumentNullException("refType");
            }

            var urn = refType.URN;

            StructureReferenceImpl structureReferenceImpl;

            if (ObjectUtil.ValidString(urn))
            {
                structureReferenceImpl = new StructureReferenceImpl(urn);
            }
            else
            {
                string agencyID = refType.AgencyID;
                string dataflowID = refType.DataflowID;
                string version = refType.Version;
                structureReferenceImpl = new StructureReferenceImpl(agencyID, dataflowID, version, SdmxStructureEnumType.Dataflow);
            }

            return structureReferenceImpl;
        }

        /// <summary>
        /// Adds the agency reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="structureReferenceList">The structure reference list.</param>
        private static void AddAgencyRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> structureReferenceList)
        {
            foreach (AgencyRefType agencyRefType in queryStructureRequests.AgencyRef)
            {
                var urn = agencyRefType.URN;
                string agencyId = agencyRefType.AgencyID;
                string maintId = agencyRefType.OrganisationSchemeID;
                string version = agencyRefType.OrganisationSchemeAgencyID;
                var targetStructureEnum = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Agency);
                AddStructureReferenceToList(urn, structureReferenceList, agencyId, maintId, version, targetStructureEnum);
            }
        }

        /// <summary>
        /// Adds the category scheme reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="structureReferenceList">The structure reference list.</param>
        private static void AddCategorySchemeRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> structureReferenceList)
        {
            foreach (CategorySchemeRefType refType in queryStructureRequests.CategorySchemeRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.CategorySchemeID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, structureReferenceList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme));
            }
        }

        /// <summary>
        /// Adds the category scheme where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddCategorySchemeWhere(QueryType queryType, IList<IStructureReference> returnList)
        {
            foreach (CategorySchemeWhereType structQuery in queryType.CategorySchemeWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the codelist reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="structureReferenceList">The structure reference list.</param>
        private static void AddCodelistRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> structureReferenceList)
        {
            foreach (CodelistRefType refType in queryStructureRequests.CodelistRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.CodelistID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, structureReferenceList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            }
        }

        /// <summary>
        /// Adds the codelist where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        /// <exception cref="SdmxNotImplementedException">
        /// CodelistWhere/Or
        /// or
        /// CodelistWhere/And
        /// </exception>
        private static void AddCodelistWhere(QueryType queryType, IList<IStructureReference> returnList)
        {
            foreach (CodelistWhereType structQuery in queryType.CodelistWhere)
            {
                string codelistId = null;
                if (structQuery.Codelist != null)
                {
                    codelistId = structQuery.Codelist.id;
                }

                string agencyId = structQuery.AgencyID;
                string maintId = codelistId;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
                if (structQuery.Or != null)
                {
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "CodelistWhere/Or");
                }

                if (structQuery.And != null)
                {
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "CodelistWhere/And");
                }
            }
        }

        /// <summary>
        /// Adds the concept scheme reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddConceptSchemeRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            if (queryStructureRequests.ConceptSchemeRef != null)
            {
                foreach (ConceptSchemeRefType refType in queryStructureRequests.ConceptSchemeRef)
                {
                    var urn = refType.URN;
                    string agencyId = refType.AgencyID;
                    string maintId = refType.ConceptSchemeID;
                    string version = refType.Version;
                    AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme));
                }
            }
        }

        /// <summary>
        /// Adds the concept scheme where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddConceptSchemeWhere(QueryType queryType, IList<IStructureReference> returnList)
        {
            foreach (ConceptSchemeWhereType structQuery in queryType.ConceptSchemeWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the concept where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        /// <exception cref="SdmxNotImplementedException">
        /// ConceptWhere/Or
        /// or
        /// ConceptWhere/And
        /// </exception>
        private static void AddConceptWhere(QueryType queryType, IList<IStructureReference> returnList)
        {
            foreach (ConceptWhereType structQuery in queryType.ConceptWhere)
            {
                string agencyId = structQuery.AgencyID;
                string conceptId = structQuery.Concept;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Concept);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, ConceptSchemeObject.DefaultSchemeId, ConceptSchemeObject.DefaultSchemeVersion, structType, conceptId);
                returnList.Add(refBean);
                if (structQuery.Or != null)
                {
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "ConceptWhere/Or");
                }

                if (structQuery.And != null)
                {
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "ConceptWhere/And");
                }
            }
        }

        /// <summary>
        /// Adds the dataflow where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddDataflowWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (DataflowWhereType structQuery in queryType.DataflowWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the data provider reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddDataProviderRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            if (queryStructureRequests.DataProviderRef != null)
            {
                foreach (DataProviderRefType refType1 in queryStructureRequests.DataProviderRef)
                {
                    var urn2 = refType1.URN;
                    if (ObjectUtil.ValidString(urn2))
                    {
                        returnList.Add(new StructureReferenceImpl(urn2));
                    }
                    else
                    {
                        string agencyId2 = refType1.OrganisationSchemeAgencyID;
                        string maintId2 = refType1.OrganisationSchemeAgencyID;
                        string version2 = refType1.Version;
                        string id = refType1.DataProviderID;
                        returnList.Add(new StructureReferenceImpl(agencyId2, maintId2, version2, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProvider), id));
                    }
                }
            }
        }

        /// <summary>
        /// Adds the hierarchical codelist reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddHerarchicalCodelistRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (HierarchicalCodelistRefType refType in queryStructureRequests.HierarchicalCodelistRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.HierarchicalCodelistID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchicalCodelist));
            }
        }

        /// <summary>
        /// Adds the hierarchical codelist where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddHierarchicalCodelistWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (HierarchicalCodelistWhereType structQuery in queryType.HierarchicalCodelistWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.HierarchicalCodelist);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the key family reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddKeyfamilyRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            if (queryStructureRequests.KeyFamilyRef != null)
            {
                foreach (KeyFamilyRefType refType in queryStructureRequests.KeyFamilyRef)
                {
                    var urn = refType.URN;
                    string agencyId = refType.AgencyID;
                    string maintId = refType.KeyFamilyID;
                    string version = refType.Version;
                    AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd));
                }
            }
        }

        /// <summary>
        /// Adds the key family where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddKeyFamilyWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (KeyFamilyWhereType structQuery in queryType.KeyFamilyWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.KeyFamily;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);

                ValidateKeyFamilyExtraCriteria(structQuery);
            }
        }

        /// <summary>
        /// Adds the meta dataflow reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddMetadataflowRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (MetadataflowRefType refType in queryStructureRequests.MetadataflowRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.MetadataflowID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow));
            }
        }

        /// <summary>
        /// Adds the meta dataflow where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddMetadataflowWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (MetadataflowWhereType structQuery in queryType.MetadataflowWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.MetadataFlow);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the metadata structure reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddMetadataStructureRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (MetadataStructureRefType refType in queryStructureRequests.MetadataStructureRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.MetadataStructureID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Msd));
            }
        }

        /// <summary>
        /// Adds the organisation scheme reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddOrganisationSchemeRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (OrganisationSchemeRefType refType in queryStructureRequests.OrganisationSchemeRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.OrganisationSchemeID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme));
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataConsumerScheme));
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.DataProviderScheme));
            }
        }

        /// <summary>
        /// Adds the organisation scheme where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddOrganisationSchemeWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (OrganisationSchemeWhereType structQuery in queryType.OrganisationSchemeWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.OrganisationUnitScheme);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the process reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddProcessRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (ProcessRefType refType in queryStructureRequests.ProcessRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.ProcessID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Process));
            }
        }

        /// <summary>
        /// Adds the process where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddProcessWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (ProcessWhereType structQuery in queryType.ProcessWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Process);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the reporting taxonomy reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddReportingTaxonomyRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (ReportingTaxonomyRefType refType in queryStructureRequests.ReportingTaxonomyRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.ReportingTaxonomyID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingTaxonomy));
            }
        }

        /// <summary>
        /// Adds the reporting taxonomy where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddReportingTaxonomyWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (ReportingTaxonomyWhereType structQuery in queryType.ReportingTaxonomyWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ReportingTaxonomy);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Adds the structure reference to list.
        /// </summary>
        /// <param name="urn">The urn.</param>
        /// <param name="returnList">The structure reference list.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="maintId">The maintainable identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="targetStructureEnum">The target structure .</param>
        private static void AddStructureReferenceToList(Uri urn, ICollection<IStructureReference> returnList, string agencyId, string maintId, string version, SdmxStructureType targetStructureEnum)
        {
            if (ObjectUtil.ValidString(urn))
            {
                returnList.Add(new StructureReferenceImpl(urn));
            }
            else
            {
                returnList.Add(new StructureReferenceImpl(agencyId, maintId, version, targetStructureEnum));
            }
        }

        /// <summary>
        /// Adds the structure set reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddStructureSetRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (StructureSetRefType refType in queryStructureRequests.StructureSetRef)
            {
                var urn = refType.URN;
                string agencyId = refType.AgencyID;
                string maintId = refType.StructureSetID;
                string version = refType.Version;
                AddStructureReferenceToList(urn, returnList, agencyId, maintId, version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet));
            }
        }

        /// <summary>
        /// Adds the structure set where.
        /// </summary>
        /// <param name="queryType">Type of the query.</param>
        /// <param name="returnList">The return list.</param>
        private static void AddStructureSetWhere(QueryType queryType, ICollection<IStructureReference> returnList)
        {
            foreach (StructureSetWhereType structQuery in queryType.StructureSetWhere)
            {
                string agencyId = structQuery.AgencyID;
                string maintId = structQuery.ID;
                string version = structQuery.Version;
                SdmxStructureType structType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.StructureSet);
                IStructureReference refBean = new StructureReferenceImpl(agencyId, maintId, version, structType);
                returnList.Add(refBean);
            }
        }

        /// <summary>
        /// Validates the key family extra criteria.
        /// </summary>
        /// <param name="structQuery">The structure query.</param>
        /// <exception cref="SdmxNotImplementedException">
        /// KeyFamilyWhere/Or
        /// or
        /// KeyFamilyWhere/And
        /// or
        /// KeyFamilyWhere/Dimensions
        /// or
        /// KeyFamilyWhere/Attribute
        /// or
        /// KeyFamilyWhere/Codelist
        /// or
        /// KeyFamilyWhere/Category
        /// or
        /// KeyFamilyWhere/Concept
        /// </exception>
        private static void ValidateKeyFamilyExtraCriteria(KeyFamilyWhereType structQuery)
        {
            if (structQuery.Or != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/Or");
            }

            if (structQuery.And != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/And");
            }

            if (structQuery.Dimension != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/Dimensions");
            }

            if (structQuery.Attribute != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/Attribute");
            }

            if (structQuery.Codelist != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/Codelist");
            }

            if (structQuery.Category != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/Category");
            }

            if (structQuery.Concept != null)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "KeyFamilyWhere/Concept");
            }
        }

        /// <summary>
        /// Adds the dataflow reference.
        /// </summary>
        /// <param name="queryStructureRequests">The query structure requests.</param>
        /// <param name="returnList">The return list.</param>
        private void AddDataflowRef(QueryStructureRequestType queryStructureRequests, IList<IStructureReference> returnList)
        {
            foreach (DataflowRefType refType1 in queryStructureRequests.DataflowRef)
            {
                var structureReferenceImpl = this.BuildDataflowQuery(refType1);
                returnList.Add(structureReferenceImpl);
            }
        }
    }
}