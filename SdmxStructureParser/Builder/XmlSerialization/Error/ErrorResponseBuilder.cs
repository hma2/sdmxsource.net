// -----------------------------------------------------------------------
// <copyright file="ErrorResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Error
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Util.Exception;

    using Xml.Schema.Linq;

    /// <summary>
    ///     The error response builder implementation
    /// </summary>
    public class ErrorResponseBuilder : XmlObjectBuilder, IErrorResponseBuilder
    {
        /// <summary>
        ///     Build error response.
        /// </summary>
        /// <param name="exception">
        ///     The exception.
        /// </param>
        /// <param name="exceptionCode">
        ///     The exception code.
        /// </param>
        /// <returns>
        ///     The <see cref="XTypedElement" />.
        /// </returns>
        public virtual XTypedElement BuildErrorResponse(Exception exception, string exceptionCode)
        {
            var errorDocument = new Error();

            var errorMessage = new CodedStatusMessageType();
            errorDocument.ErrorMessage.Add(errorMessage);

            errorMessage.code = exceptionCode;
            while (exception != null)
            {
                var text = new TextType();
                errorMessage.Text.Add(text);
                if (string.IsNullOrEmpty(exception.Message))
                {
                    if (exception.InnerException != null)
                    {
                        text.TypedValue = exception.InnerException.Message;
                    }
                }
                else
                {
                    text.TypedValue = exception.Message;
                }

                var validationException = exception as SchemaValidationException;
                if (validationException != null)
                {
                    this.ProcessSchemaValidationError(errorMessage, validationException);
                }
                else
                {
                    this.ProcessThrowable(errorMessage, exception);
                }

                exception = exception.InnerException;
            }

            return errorDocument;
        }

        /// <summary>
        /// Processes the schema validation error.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="e">The schema validation exception</param>
        private void ProcessSchemaValidationError(CodedStatusMessageType errorMessage, SchemaValidationException e)
        {
            foreach (string error in e.GetValidationErrors())
            {
                TextType text = new TextType();
                errorMessage.Text.Add(text);
                text.TypedValue = error;
            }
        }

        /// <summary>
        /// Processes the exception.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="th">The exception.</param>
        private void ProcessThrowable(CodedStatusMessageType errorMessage, Exception th)
        {
            TextType text = new TextType();
            errorMessage.Text.Add(text);

            if (string.IsNullOrWhiteSpace(th.Message))
            {
                if (th.InnerException != null)
                {
                    text.TypedValue = th.GetBaseException().Message;
                }
                else
                {
                    if (th is NullReferenceException)
                    {
                        text.TypedValue = "Null Reference Exception";
                    }
                    else
                    {
                        text.TypedValue = "No Error Message Provided";
                    }
                }
            }
            else
            {
                text.TypedValue = th.Message;
            }
        }
    }
}