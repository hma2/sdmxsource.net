// -----------------------------------------------------------------------
// <copyright file="DimensionXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V1
{
    using System;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects;

    /// <summary>
    ///     The dimension xml bean builder.
    /// </summary>
    public class DimensionXmlBuilder : AbstractBuilder, IBuilder<DimensionType, IDimension>
    {
        /// <summary>
        ///     Initializes static members of the <see cref="DimensionXmlBuilder" /> class.
        /// </summary>
        static DimensionXmlBuilder()
        {
            Log = LogManager.GetLogger(typeof(DimensionXmlBuilder));
        }

        /// <summary>
        ///     The build.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="DimensionType" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual DimensionType Build(IDimension buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new DimensionType();

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.HasCodedRepresentation())
            {
                builtObj.codelist = buildFrom.Representation.Representation.MaintainableReference.MaintainableId;
            }

            if (buildFrom.ConceptRef != null)
            {
                builtObj.concept = ConceptRefUtil.GetConceptId(buildFrom.ConceptRef);
            }

            if (buildFrom.FrequencyDimension)
            {
                builtObj.isFrequencyDimension = buildFrom.FrequencyDimension;
            }

            if (buildFrom.MeasureDimension)
            {
                builtObj.isMeasureDimension = buildFrom.MeasureDimension;
            }

            return builtObj;
        }
    }
}