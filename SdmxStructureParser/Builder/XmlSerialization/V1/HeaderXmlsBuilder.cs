// -----------------------------------------------------------------------
// <copyright file="HeaderXmlsBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V1
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.message;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;

    /// <summary>
    ///     The header xml beans builder.
    /// </summary>
    public class HeaderXmlsBuilder : AbstractBuilder, IBuilder<HeaderType, IHeader>
    {
        /// <summary>
        ///     The unassigned id.
        /// </summary>
        private const string UnassignedId = "unassigned";

        /// <summary>
        ///     The build.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="HeaderType" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual HeaderType Build(IHeader buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var headerType = new HeaderType();

            string value = buildFrom.Id;
            if (buildFrom != null && !string.IsNullOrWhiteSpace(value))
            {
                headerType.ID = buildFrom.Id;
            }
            else
            {
                headerType.ID = UnassignedId;
            }

            if (buildFrom != null && buildFrom.Prepared != null)
            {
                headerType.Prepared = buildFrom.Prepared;
            }
            else
            {
                headerType.Prepared = DateTime.Now;
            }

            if (buildFrom != null && buildFrom.Action != null)
            {
                switch (buildFrom.Action.EnumType)
                {
                    case DatasetActionEnumType.Append:
                        headerType.DataSetAction = ActionTypeConstants.Update;
                        break;
                    case DatasetActionEnumType.Replace:
                        headerType.DataSetAction = ActionTypeConstants.Update;
                        break;
                    case DatasetActionEnumType.Delete:
                        headerType.DataSetAction = ActionTypeConstants.Delete;
                        break;
                    default:
                        headerType.DataSetAction = ActionTypeConstants.Update;
                        break;
                }
            }
            else
            {
                headerType.DataSetAction = ActionTypeConstants.Update;
            }

            if (buildFrom != null && buildFrom.Sender != null)
            {
                SetSenderInfo(headerType, buildFrom.Sender);
            }
            else
            {
                var sender = new PartyType();
                headerType.Sender = sender;
                sender.id = "unknown";
            }

            return headerType;
        }

        /// <summary>
        ///     The set sender info.
        /// </summary>
        /// <param name="headerType">
        ///     The header type.
        /// </param>
        /// <param name="party">
        ///     The party.
        /// </param>
        private static void SetSenderInfo(HeaderType headerType, IParty party)
        {
            var sender = new PartyType();
            headerType.Sender = sender;

            string value = party.Id;
            if (!string.IsNullOrWhiteSpace(value))
            {
                sender.id = party.Id;
            }
        }
    }
}