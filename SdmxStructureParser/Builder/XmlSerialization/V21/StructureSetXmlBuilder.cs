// -----------------------------------------------------------------------
// <copyright file="StructureSetXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The structure set xml bean builder.
    /// </summary>
    public class StructureSetXmlBuilder : MaintainableAssembler, IBuilder<StructureSetType, IStructureSetObject>
    {
        /// <summary>
        ///     The category scheme map bean assembler bean.
        /// </summary>
        private readonly CategorySchemeMapAssembler _categorySchemeMapAssemblerBean = new CategorySchemeMapAssembler();

        /// <summary>
        ///     The codelist map bean assembler bean.
        /// </summary>
        private readonly CodelistMapAssembler _codelistMapAssemblerBean = new CodelistMapAssembler();

        /// <summary>
        ///     The concept scheme map bean assembler bean.
        /// </summary>
        private readonly ConceptSchemeMapAssembler _conceptSchemeMapAssemblerBean = new ConceptSchemeMapAssembler();

        /// <summary>
        ///     The organisation scheme map bean assembler bean.
        /// </summary>
        private readonly OrganisationSchemeMapAssembler _organisationSchemeMapAssemblerBean =
            new OrganisationSchemeMapAssembler();

        /// <summary>
        ///     The structure map bean assembler bean.
        /// </summary>
        private readonly StructureMapAssembler _structureMapAssemblerBean = new StructureMapAssembler();

        /// <summary>
        ///     Build a <see cref="StructureSetType" /> from <paramref name="buildFrom" /> and return it
        /// </summary>
        /// <param name="buildFrom">
        ///     The <see cref="IStructureSetObject" />
        /// </param>
        /// <returns>
        ///     The <see cref="StructureSetType" />.
        /// </returns>
        public virtual StructureSetType Build(IStructureSetObject buildFrom)
        {
            // Create outgoing build
            var builtObj = new StructureSetType();

            // Populate it from inherited super
            this.AssembleMaintainable(builtObj, buildFrom);

            // Populate it using this class's specifics
            foreach (IOrganisationSchemeMapObject eachOrganisationMapBean in buildFrom.OrganisationSchemeMapList)
            {
                var newOrganisationSchemeMapType = new OrganisationSchemeMapType();
                builtObj.OrganisationSchemeMap.Add(newOrganisationSchemeMapType);
                this._organisationSchemeMapAssemblerBean.Assemble(newOrganisationSchemeMapType, eachOrganisationMapBean);
            }

            foreach (ICategorySchemeMapObject eachCategoryMapBean in buildFrom.CategorySchemeMapList)
            {
                var newCategorySchemeMapType = new CategorySchemeMapType();
                builtObj.CategorySchemeMap.Add(newCategorySchemeMapType);
                this._categorySchemeMapAssemblerBean.Assemble(newCategorySchemeMapType, eachCategoryMapBean);
            }

            foreach (IConceptSchemeMapObject eachConceptMapBean in buildFrom.ConceptSchemeMapList)
            {
                var newConceptSchemeMapType = new ConceptSchemeMapType();
                builtObj.ConceptSchemeMap.Add(newConceptSchemeMapType);
                this._conceptSchemeMapAssemblerBean.Assemble(newConceptSchemeMapType, eachConceptMapBean);
            }

            // TODO RSG REPORTING TAXONOMY MAP CURRENTLY NOT YET IN MODEL ###
            foreach (ICodelistMapObject eachCodelistMapBean in buildFrom.CodelistMapList)
            {
                var newCodelistMapType = new CodelistMapType();
                builtObj.CodelistMap.Add(newCodelistMapType);
                this._codelistMapAssemblerBean.Assemble(newCodelistMapType, eachCodelistMapBean);
            }

            /* foreach */
            foreach (IStructureMapObject eachStructureMapBean in buildFrom.StructureMapList)
            {
                var newStructureMapType = new StructureMapType();
                builtObj.StructureMap.Add(newStructureMapType);
                this._structureMapAssemblerBean.Assemble(newStructureMapType, eachStructureMapBean);
            }

            return builtObj;
        }
    }
}