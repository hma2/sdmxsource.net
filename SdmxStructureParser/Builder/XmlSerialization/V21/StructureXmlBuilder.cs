// -----------------------------------------------------------------------
// <copyright file="StructureXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;

    using StructureType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message.StructureType;

    /// <summary>
    ///     Build a v2.1 SDMX Structure Document from SDMXObjects, by incorporating other builders for its parts
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Big number of SDMX v2.1 artefacts")]
    public class StructureXmlBuilder : IBuilder<Structure, ISdmxObjects>
    {
        /// <summary>
        ///     The agency scheme xml bean builder.
        /// </summary>
        private readonly AgencySchemeXmlBuilder _agencySchemeXmlBuilder = new AgencySchemeXmlBuilder();

        /// <summary>
        ///     The attachment constraint xml bean builder.
        /// </summary>
        private readonly AttachmentConstraintXmlBuilder _attachmentConstraintXmlBuilder = new AttachmentConstraintXmlBuilder();

        /// <summary>
        ///     The categorisation xml bean builder.
        /// </summary>
        private readonly CategorisationXmlBuilder _categorisationXmlBuilder = new CategorisationXmlBuilder();

        /// <summary>
        ///     The category scheme xml bean builder bean.
        /// </summary>
        private readonly CategorySchemeXmlBuilder _categorySchemeXmlBuilderBean = new CategorySchemeXmlBuilder();

        /// <summary>
        ///     The codelist xml bean builder bean.
        /// </summary>
        private readonly CodelistXmlBuilder _codelistXmlBuilderBean = new CodelistXmlBuilder();

        /// <summary>
        ///     The concept scheme xml bean builder bean.
        /// </summary>
        private readonly ConceptSchemeXmlBuilder _conceptSchemeXmlBuilderBean = new ConceptSchemeXmlBuilder();

        /// <summary>
        ///     The content constraint xml bean builder.
        /// </summary>
        private readonly ContentConstraintXmlBuilder _contentConstraintXmlBuilder = new ContentConstraintXmlBuilder();

        /// <summary>
        ///     The data consumer scheme xml bean builder.
        /// </summary>
        private readonly DataConsumerSchemeXmlBuilder _dataConsumerSchemeXmlBuilder = new DataConsumerSchemeXmlBuilder();

        /// <summary>
        ///     The dataflow xml bean builder bean.
        /// </summary>
        private readonly DataflowXmlBuilder _dataflowXmlBuilderBean = new DataflowXmlBuilder();

        /// <summary>
        ///     The data provider scheme xml bean builder.
        /// </summary>
        private readonly DataProviderSchemeXmlBuilder _dataProviderSchemeXmlBuilder = new DataProviderSchemeXmlBuilder();

        /// <summary>
        ///     The data structure xml bean builder bean.
        /// </summary>
        private readonly DataStructureXmlBuilder _dataStructureXmlBuilderBean = new DataStructureXmlBuilder();

        /// <summary>
        ///     The hierarchical codelist xml builder bean.
        /// </summary>
        private readonly HierarchicalCodelistXmlBuilder _hierarchicalCodelistXmlBuilderBean = new HierarchicalCodelistXmlBuilder();

        /// <summary>
        ///     The metadataflow xml bean builder bean.
        /// </summary>
        private readonly MetadataflowXmlBuilder _metadataflowXmlBuilderBean = new MetadataflowXmlBuilder();

        /// <summary>
        ///     The metadata structure xml bean builder bean.
        /// </summary>
        private readonly MetadataStructureXmlBuilder _metadataStructureXmlBuilderBean = new MetadataStructureXmlBuilder();

        /// <summary>
        ///     The organisation unit scheme xml bean builder.
        /// </summary>
        private readonly OrganisationUnitSchemeXmlBuilder _organisationUnitSchemeXmlBuilder = new OrganisationUnitSchemeXmlBuilder();

        /// <summary>
        ///     The process xml bean builder bean.
        /// </summary>
        private readonly ProcessXmlBuilder _processXmlBuilderBean = new ProcessXmlBuilder();

        /// <summary>
        ///     The provision agreement xml bean builder.
        /// </summary>
        private readonly ProvisionAgreementXmlBuilder _provisionAgreementXmlBuilder = new ProvisionAgreementXmlBuilder();

        /// <summary>
        ///     The reporting taxonomy xml bean builder bean.
        /// </summary>
        private readonly ReportingTaxonomyXmlBuilder _reportingTaxonomyXmlBuilderBean = new ReportingTaxonomyXmlBuilder();

        /// <summary>
        ///     The structure header xml bean builder.
        /// </summary>
        private readonly StructureHeaderXmlBuilder<StructureHeaderType> _structureHeaderXmlBuilder = new StructureHeaderXmlBuilder<StructureHeaderType>();

        /// <summary>
        ///     The structure set xml bean builder bean.
        /// </summary>
        private readonly StructureSetXmlBuilder _structureSetXmlBuilderBean = new StructureSetXmlBuilder();

        /// <summary>
        ///     Build a <see cref="Structure" /> from <paramref name="buildFrom" /> and return it
        /// </summary>
        /// <param name="buildFrom">
        ///     The <see cref="ISdmxObjects" />
        /// </param>
        /// <returns>
        ///     The <see cref="Structure" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual Structure Build(ISdmxObjects buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var doc = new Structure();
            StructureType structureType = doc.Content;

            // HEADER
            StructureHeaderType headerType;
            if (buildFrom.Header != null)
            {
                headerType = this._structureHeaderXmlBuilder.Build(buildFrom.Header);
                structureType.Header = headerType;
            }
            else
            {
                headerType = new StructureHeaderType();
                structureType.Header = headerType;
                V21Helper.SetHeader(headerType, buildFrom);
            }

            // TOP LEVEL STRUCTURES ELEMENT
            var structures = new StructuresType();
            structureType.Structures = structures;

            this.PopulateStructureType(buildFrom, structures);
            return doc;
        }

        /// <summary>
        ///     The populate structure type.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <param name="structures">
        ///     The structures.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public void PopulateStructureType(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (structures == null)
            {
                throw new ArgumentNullException("structures");
            }

            this.PopulateOrganisationTypes(buildFrom, structures);

            // CONSTRAINTS
            this.PopulateConstraints(buildFrom, structures);

            // DATAFLOWS
            this.PopulateDataflows(buildFrom, structures);

            // METADATAFLOWS
            this.PopulateMetaDataflows(buildFrom, structures);

            // CATEGORY SCHEMES
            this.PopulateCategorySchemes(buildFrom, structures);

            // CATEGORISATIONS
            this.PopulateCategorisations(buildFrom, structures);

            // CODELISTS
            this.PopulateCodelists(buildFrom, structures);

            // HIERARCHICAL CODELISTS
            this.PopulateHcl(buildFrom, structures);

            // CONCEPTS
            this.PopulateConceptSchemes(buildFrom, structures);

            // METADATA STRUCTURE
            this.PopulateMetadataStructures(buildFrom, structures);

            // DATA STRUCTURE
            this.PopulateDataStructures(buildFrom, structures);

            // STRUCTURE SETS
            this.PopulateStructureSets(buildFrom, structures);

            // REPORTING TAXONOMIES
            this.PopulateReportingTaxonomies(buildFrom, structures);

            // PROCESSES
            this.PopulateProcesses(buildFrom, structures);

            // PROVISION AGREEMENTS
            this.PopulateProvisionAgreements(buildFrom, structures);
        }

        /// <summary>
        ///     Add the organisation schemes type.
        /// </summary>
        /// <param name="structures">
        ///     The structures.
        /// </param>
        /// <returns>
        ///     The <see cref="OrganisationSchemesType" />.
        /// </returns>
        private static OrganisationSchemesType AddOrganisationSchemesType(StructuresType structures)
        {
            var orgType = new OrganisationSchemesType();
            structures.OrganisationSchemes = orgType;
            return orgType;
        }

        /// <summary>
        /// Populates the categorisations.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateCategorisations(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.Categorisations.Count > 0)
            {
                var categorisations = new CategorisationsType();
                structures.Categorisations = categorisations;

                foreach (ICategorisationObject categorisationBean in buildFrom.Categorisations)
                {
                    categorisations.Categorisation.Add(this._categorisationXmlBuilder.Build(categorisationBean));
                }
            }
        }

        /// <summary>
        /// Populates the category schemes.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateCategorySchemes(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.CategorySchemes.Count > 0)
            {
                var catSchemesType = new CategorySchemesType();
                structures.CategorySchemes = catSchemesType;

                foreach (ICategorySchemeObject categorySchemeBean in buildFrom.CategorySchemes)
                {
                    catSchemesType.CategoryScheme.Add(this._categorySchemeXmlBuilderBean.Build(categorySchemeBean));
                }
            }
        }

        /// <summary>
        /// Populates the codelists.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateCodelists(ISdmxObjects buildFrom, StructuresType structures)
        {
            ISet<ICodelistObject> codelists = buildFrom.Codelists;
            if (codelists.Count > 0)
            {
                var codelistsType = new CodelistsType();
                structures.Codelists = codelistsType;

                foreach (ICodelistObject codelistBean in codelists)
                {
                    codelistsType.Codelist.Add(this._codelistXmlBuilderBean.Build(codelistBean));
                }
            }
        }

        /// <summary>
        /// Populates the concept schemes.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateConceptSchemes(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.ConceptSchemes.Count > 0)
            {
                var conceptsType = new ConceptsType();
                structures.Concepts = conceptsType;

                foreach (IConceptSchemeObject conceptSchemeBean in buildFrom.ConceptSchemes)
                {
                    conceptsType.ConceptScheme.Add(this._conceptSchemeXmlBuilderBean.Build(conceptSchemeBean));
                }
            }
        }

        /// <summary>
        /// Populates the constraints.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateConstraints(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.AttachmentConstraints.Count > 0 || buildFrom.ContentConstraintObjects.Count > 0)
            {
                var constraintsType = new ConstraintsType();
                structures.Constraints = constraintsType;

                foreach (IAttachmentConstraintObject currentBean in buildFrom.AttachmentConstraints)
                {
                    constraintsType.AttachmentConstraint.Add(this._attachmentConstraintXmlBuilder.Build(currentBean));
                }

                foreach (IContentConstraintObject currentBean in buildFrom.ContentConstraintObjects)
                {
                    constraintsType.ContentConstraint.Add(this._contentConstraintXmlBuilder.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the dataflows.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateDataflows(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.Dataflows.Count > 0)
            {
                var dataflowsType = new DataflowsType();
                structures.Dataflows = dataflowsType;

                foreach (IDataflowObject currentBean in buildFrom.Dataflows)
                {
                    dataflowsType.Dataflow.Add(this._dataflowXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the data structures.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateDataStructures(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.DataStructures.Count > 0)
            {
                var dataStructuresType = new DataStructuresType();
                structures.DataStructures = dataStructuresType;

                foreach (IDataStructureObject currentBean in buildFrom.DataStructures)
                {
                    dataStructuresType.DataStructure.Add(this._dataStructureXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the HCL.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateHcl(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.HierarchicalCodelists.Count > 0)
            {
                var hierarchicalCodelistsType = new HierarchicalCodelistsType();
                structures.HierarchicalCodelists = hierarchicalCodelistsType;

                foreach (IHierarchicalCodelistObject currentBean in buildFrom.HierarchicalCodelists)
                {
                    hierarchicalCodelistsType.HierarchicalCodelist.Add(this._hierarchicalCodelistXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the meta dataflows.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateMetaDataflows(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.Metadataflows.Count > 0)
            {
                var metadataflowsType = new MetadataflowsType();
                structures.Metadataflows = metadataflowsType;

                foreach (IMetadataFlow currentBean in buildFrom.Metadataflows)
                {
                    metadataflowsType.Metadataflow.Add(this._metadataflowXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the metadata structures.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateMetadataStructures(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.MetadataStructures.Count > 0)
            {
                var msdsType = new MetadataStructuresType();
                structures.MetadataStructures = msdsType;

                foreach (IMetadataStructureDefinitionObject currentBean in buildFrom.MetadataStructures)
                {
                    msdsType.MetadataStructure.Add(this._metadataStructureXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the organisation types.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateOrganisationTypes(ISdmxObjects buildFrom, StructuresType structures)
        {
            OrganisationSchemesType orgType = null;
            if (buildFrom.OrganisationUnitSchemes.Count > 0)
            {
                orgType = AddOrganisationSchemesType(structures);

                foreach (IOrganisationUnitSchemeObject currentBean in buildFrom.OrganisationUnitSchemes)
                {
                    orgType.OrganisationUnitScheme.Add(this._organisationUnitSchemeXmlBuilder.Build(currentBean));
                }
            }

            if (buildFrom.DataConsumerSchemes.Count > 0)
            {
                if (orgType == null)
                {
                    orgType = AddOrganisationSchemesType(structures);
                }

                //// TODO check latest java code. Unused array in Java 0.9.1
                foreach (IDataConsumerScheme currentBean2 in buildFrom.DataConsumerSchemes)
                {
                    orgType.DataConsumerScheme.Add(this._dataConsumerSchemeXmlBuilder.Build(currentBean2));
                }
            }

            if (buildFrom.DataProviderSchemes.Count > 0)
            {
                if (orgType == null)
                {
                    orgType = AddOrganisationSchemesType(structures);
                }

                foreach (IDataProviderScheme currentBean3 in buildFrom.DataProviderSchemes)
                {
                    orgType.DataProviderScheme.Add(this._dataProviderSchemeXmlBuilder.Build(currentBean3));
                }
            }

            if (buildFrom.AgenciesSchemes.Count > 0)
            {
                if (orgType == null)
                {
                    orgType = AddOrganisationSchemesType(structures);
                }

                foreach (IAgencyScheme currentBean4 in buildFrom.AgenciesSchemes)
                {
                    orgType.AgencyScheme.Add(this._agencySchemeXmlBuilder.Build(currentBean4));
                }
            }
        }

        /// <summary>
        /// Populates the processes.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateProcesses(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.Processes.Count > 0)
            {
                var processesType = new ProcessesType();
                structures.Processes = processesType;

                foreach (IProcessObject currentBean in buildFrom.Processes)
                {
                    processesType.Process.Add(this._processXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the provision agreements.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateProvisionAgreements(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.ProvisionAgreements.Count > 0)
            {
                var provisionAgreementsType = new ProvisionAgreementsType();
                structures.ProvisionAgreements = provisionAgreementsType;

                foreach (IProvisionAgreementObject currentBean in buildFrom.ProvisionAgreements)
                {
                    provisionAgreementsType.ProvisionAgreement.Add(this._provisionAgreementXmlBuilder.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the reporting taxonomies.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateReportingTaxonomies(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.ReportingTaxonomys.Count > 0)
            {
                var reportingTaxonomiesType = new ReportingTaxonomiesType();
                structures.ReportingTaxonomies = reportingTaxonomiesType;

                foreach (IReportingTaxonomyObject currentBean in buildFrom.ReportingTaxonomys)
                {
                    reportingTaxonomiesType.ReportingTaxonomy.Add(this._reportingTaxonomyXmlBuilderBean.Build(currentBean));
                }
            }
        }

        /// <summary>
        /// Populates the structure sets.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="structures">The structures.</param>
        private void PopulateStructureSets(ISdmxObjects buildFrom, StructuresType structures)
        {
            if (buildFrom.StructureSets.Count > 0)
            {
                var structureSetsType = new StructureSetsType();
                structures.StructureSets = structureSetsType;

                foreach (IStructureSetObject currentBean in buildFrom.StructureSets)
                {
                    structureSetsType.StructureSet.Add(this._structureSetXmlBuilderBean.Build(currentBean));
                }
            }
        }
    }
}