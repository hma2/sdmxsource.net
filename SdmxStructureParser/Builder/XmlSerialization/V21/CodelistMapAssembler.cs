// -----------------------------------------------------------------------
// <copyright file="CodelistMapAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    using CodeMap = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure.CodeMap;

    /// <summary>
    ///     The codelist map bean assembler.
    /// </summary>
    public class CodelistMapAssembler : AbstractSchemeMapAssembler, IAssembler<CodelistMapType, ICodelistMapObject>
    {
        /// <summary>
        ///     The assemble.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from.
        /// </param>
        public virtual void Assemble(CodelistMapType assembleInto, ICodelistMapObject assembleFrom)
        {
            // Populate it from inherited super
            this.AssembleMap(assembleInto, assembleFrom);

            // Child maps
            foreach (IItemMap eachMapBean in assembleFrom.Items)
            {
                // Defer child creation to subclass
                var newMap = new CodeMap();
                assembleInto.ItemAssociation.Add(newMap);

                // Common source and target id allocation
                var sourceCodeRef = new LocalCodeReferenceType();
                sourceCodeRef.SetTypedRef(new LocalCodeRefType { id = eachMapBean.SourceId });
                newMap.Source = sourceCodeRef;
                var targetCodeRef = new LocalCodeReferenceType();
                targetCodeRef.SetTypedRef(new LocalCodeRefType { id = eachMapBean.TargetId });
                newMap.Target = targetCodeRef;
            }
        }

        /// <summary>
        ///     Create the ref. base type
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <returns>
        ///     The <see cref="ItemSchemeRefBaseType" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="assembleInto"/> is <see langword="null" />.</exception>
        protected override ItemSchemeRefBaseType CreateRef(ItemSchemeReferenceBaseType assembleInto)
        {
            if (assembleInto == null)
            {
                throw new ArgumentNullException("assembleInto");
            }

            var refType = new CodelistRefType();
            assembleInto.SetTypedRef(refType);
            return refType;
        }

        /// <summary>
        ///     Create the source reference.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <returns>
        ///     The <see cref="ItemSchemeReferenceBaseType" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="assembleInto"/> is <see langword="null" />.</exception>
        protected override ItemSchemeReferenceBaseType CreateSourceReference(ItemSchemeMapType assembleInto)
        {
            if (assembleInto == null)
            {
                throw new ArgumentNullException("assembleInto");
            }

            var source = new CodelistReferenceType();
            assembleInto.Source = source;

            return source;
        }

        /// <summary>
        ///     Create the target reference.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <returns>
        ///     The <see cref="ItemSchemeReferenceBaseType" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="assembleInto"/> is <see langword="null" />.</exception>
        protected override ItemSchemeReferenceBaseType CreateTargetReference(ItemSchemeMapType assembleInto)
        {
            if (assembleInto == null)
            {
                throw new ArgumentNullException("assembleInto");
            }

            var targetReference = new CodelistReferenceType();
            assembleInto.Target = targetReference;

            return targetReference;
        }
    }
}