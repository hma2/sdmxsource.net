// -----------------------------------------------------------------------
// <copyright file="SubmitRegistrationBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.RegistryRequest
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;

    using SubmitRegistrationsRequestType = Org.Sdmx.Resources.SdmxMl.Schemas.V21.Registry.SubmitRegistrationsRequestType;

    /// <summary>
    ///     The submit registration builder.
    /// </summary>
    public class SubmitRegistrationBuilder
    {
        /// <summary>
        ///     The registration xml bean builder.
        /// </summary>
        private readonly RegistrationXmlBuilder _registrationXmlBuilder = new RegistrationXmlBuilder();

        /// <summary>
        ///     Build the registry interface document.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <param name="action">
        ///     The action.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public RegistryInterface BuildRegistryInterfaceDocument(
            ICollection<IRegistrationObject> buildFrom, 
            DatasetActionEnumType action)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var registryInterface = new RegistryInterface();
            RegistryInterfaceType interfaceType = registryInterface.Content;
            V21Helper.SetHeader(interfaceType);
            var submitRegistrationRequest = new SubmitRegistrationsRequestType();
            interfaceType.SubmitRegistrationsRequest = submitRegistrationRequest;

            /* foreach */
            foreach (IRegistrationObject currentRegistration in buildFrom)
            {
                RegistrationType registrationType = this._registrationXmlBuilder.Build(currentRegistration);
                var registrationRequest = new RegistrationRequestType();
                submitRegistrationRequest.RegistrationRequest.Add(registrationRequest);
                registrationRequest.Registration = registrationType;
                switch (action)
                {
                    case DatasetActionEnumType.Append:
                        registrationRequest.action = ActionTypeConstants.Append;
                        break;
                    case DatasetActionEnumType.Replace:
                        registrationRequest.action = ActionTypeConstants.Replace;
                        break;
                    case DatasetActionEnumType.Delete:
                        registrationRequest.action = ActionTypeConstants.Delete;
                        break;
                    case DatasetActionEnumType.Information:
                        registrationRequest.action = ActionTypeConstants.Information;
                        break;
                }
            }

            return registryInterface;
        }
    }
}