// -----------------------------------------------------------------------
// <copyright file="CategorisationXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The categorisation xml bean builder.
    /// </summary>
    public class CategorisationXmlBuilder : MaintainableAssembler, IBuilder<CategorisationType, ICategorisationObject>
    {
        /// <summary>
        ///     The build.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="CategorisationType" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual CategorisationType Build(ICategorisationObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var returnType = new CategorisationType();

            this.AssembleMaintainable(returnType, buildFrom);
            if (buildFrom.StructureReference != null)
            {
                var objRefType = new ObjectReferenceType();
                returnType.Source = objRefType;
                var objectRefType = new ObjectRefType();
                objRefType.SetTypedRef(objectRefType);
                this.SetReference(objectRefType, buildFrom.StructureReference);
            }

            if (buildFrom.CategoryReference != null)
            {
                var catRefType = new CategoryReferenceType();
                returnType.Target = catRefType;
                var categoryRefType = new CategoryRefType();
                catRefType.SetTypedRef(categoryRefType);
                this.SetReference(categoryRefType, buildFrom.CategoryReference);
            }

            return returnType;
        }
    }
}