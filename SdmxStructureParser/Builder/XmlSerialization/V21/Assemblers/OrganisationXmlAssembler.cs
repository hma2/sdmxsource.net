﻿// -----------------------------------------------------------------------
// <copyright file="OrganisationXmlAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class OrganisationXmlAssembler : AbstractAssembler, IAssembler<Organisation, IOrganisation>
    {
        /// <summary>
        /// The assemble.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="assembleFrom"/> is <see langword="null" />.</exception>
        public virtual void Assemble(Organisation assembleInto, IOrganisation assembleFrom)
        {
            if (assembleInto == null)
            {
                throw new ArgumentNullException("assembleInto");
            }

            if (assembleFrom == null)
            {
                throw new ArgumentNullException("assembleFrom");
            }

            foreach (IContact currentContact in assembleFrom.Contacts)
            {
                var contact = new ContactType();
                assembleInto.Contact.Add(contact);

                if (ObjectUtil.ValidString(currentContact.Id))
                {
                    contact.id = currentContact.Id;
                }

                if (ObjectUtil.ValidCollection(currentContact.Departments))
                {
                    contact.Department = this.GetTextType(currentContact.Departments);
                }

                if (ObjectUtil.ValidCollection(currentContact.Name))
                {
                    if (contact.Name == null)
                    {
                        contact.Name = new List<Name>();
                    }

                    foreach (var name in currentContact.Name)
                    {
                        contact.Name.Add(new Name { TypedValue = name.Value, lang = name.Locale });
                    }
                }

                if (ObjectUtil.ValidCollection(currentContact.Role))
                {
                    contact.Role = this.GetTextType(currentContact.Role);
                }

                if (ObjectUtil.ValidCollection(currentContact.Telephone))
                {
                    contact.Telephone.AddAll(currentContact.Telephone);
                }

                if (ObjectUtil.ValidCollection(currentContact.Fax))
                {
                    contact.Fax.AddAll(currentContact.Fax);
                }

                if (ObjectUtil.ValidCollection(currentContact.Email))
                {
                    contact.Email.AddAll(currentContact.Email);
                }

                if (ObjectUtil.ValidCollection(currentContact.Uri))
                {
                    if (contact.URI == null)
                    {
                        contact.URI = new List<Uri>();
                    }

                    foreach (string uri in currentContact.Uri)
                    {
                        contact.URI.Add(new Uri(uri, UriKind.RelativeOrAbsolute)); // TODO: exception
                    }
                }

                if (ObjectUtil.ValidCollection(currentContact.X400))
                {
                    contact.X400.AddAll(currentContact.X400);
                }
            }
        }
    }
}