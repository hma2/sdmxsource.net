// -----------------------------------------------------------------------
// <copyright file="TextFormatAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System;
    using System.Globalization;

    using log4net;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The text format assembler.
    /// </summary>
    public class TextFormatAssembler : IAssembler<TextFormatType, ITextFormat>
    {
        /// <summary>
        ///     The data type builder.
        /// </summary>
        private readonly DataTypeBuilder _dataTypeBuilder = new DataTypeBuilder();

        /// <summary>
        ///     The log.
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(TextFormatAssembler));

        /// <summary>
        ///     The assemble.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="assembleInto"/> is <see langword="null" />.</exception>
        public virtual void Assemble(TextFormatType assembleInto, ITextFormat assembleFrom)
        {
            if (assembleInto == null)
            {
                throw new ArgumentNullException("assembleInto");
            }

            if (assembleFrom == null)
            {
                throw new ArgumentNullException("assembleFrom");
            }

            TextType textType = assembleFrom.TextType;
            if (textType != null)
            {
                assembleInto.textType = this._dataTypeBuilder.Build(textType);
            }

            if (assembleFrom.Sequence.IsSet())
            {
                assembleInto.isSequence = assembleFrom.Sequence.IsTrue;
            }

            if (assembleFrom.MinLength != null)
            {
                assembleInto.minLength = assembleFrom.MinLength;
            }

            if (assembleFrom.MaxLength != null)
            {
                assembleInto.maxLength = assembleFrom.MaxLength;
            }

            if (assembleFrom.MinValue != null)
            {
                assembleInto.minValue = assembleFrom.MinValue;
            }

            if (assembleFrom.MaxValue != null)
            {
                assembleInto.maxValue = assembleFrom.MaxValue;
            }

            if (assembleFrom.StartValue != null)
            {
                assembleInto.startValue = assembleFrom.StartValue;
            }

            if (assembleFrom.EndValue != null)
            {
                assembleInto.endValue = assembleFrom.EndValue;
            }

            if (assembleFrom.Interval != null)
            {
                assembleInto.interval = assembleFrom.Interval;
            }

            if (assembleFrom.TimeInterval != null)
            {
                TimeSpan result;
                if (TimeSpan.TryParse(assembleFrom.TimeInterval, CultureInfo.InvariantCulture, out result))
                {
                    assembleInto.timeInterval = result;
                }
                else
                {
                    assembleInto.timeInterval = null;
                    this._log.Warn("SDMX 2.1 - Unable to set TimeInterval on " + assembleFrom.Parent);
                }
            }

            if (assembleFrom.Decimals != null)
            {
                assembleInto.decimals = assembleFrom.Decimals;
            }

            if (assembleFrom.Pattern != null)
            {
                assembleInto.pattern = assembleFrom.Pattern;
            }

            if (assembleFrom.StartTime != null)
            {
                assembleInto.startTime = assembleFrom.StartTime.DateInSdmxFormat;
            }

            if (assembleFrom.EndTime != null)
            {
                assembleInto.endTime = assembleFrom.EndTime.DateInSdmxFormat;
            }
        }
    }
}