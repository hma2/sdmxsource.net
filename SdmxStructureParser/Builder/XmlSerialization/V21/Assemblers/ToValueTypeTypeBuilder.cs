// -----------------------------------------------------------------------
// <copyright file="ToValueTypeTypeBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The to value type type builder.
    /// </summary>
    public class ToValueTypeTypeBuilder : IBuilder<string, ToValue>
    {
        /// <summary>
        ///     Builds and return
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="Enum" />.
        /// </returns>
        public virtual string Build(ToValue buildFrom)
        {
            switch (buildFrom)
            {
                case ToValue.Value:
                    return ToValueTypeTypeConstants.Value;
                case ToValue.Name:
                    return ToValueTypeTypeConstants.Name;
                case ToValue.Description:
                    return ToValueTypeTypeConstants.Description;
            }

            return null;
        }
    }
}