// -----------------------------------------------------------------------
// <copyright file="NameableAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The nameable bean assembler.
    /// </summary>
    public class NameableAssembler : IdentifiableAssembler
    {
        /// <summary>
        ///     Assemble SDMX nameable artefact.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from.
        /// </param>
        public void AssembleNameable([ValidatedNotNull]NameableType assembleInto, [ValidatedNotNull]INameableObject assembleFrom)
        {
            this.AssembleIdentifiable(assembleInto, assembleFrom);
            IList<ITextTypeWrapper> names = assembleFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                this.GetTextType(names, assembleInto.Name);
            }

            IList<ITextTypeWrapper> descriptions = assembleFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                this.GetTextType(descriptions, assembleInto.Description);
            }
        }
    }
}