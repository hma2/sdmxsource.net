// -----------------------------------------------------------------------
// <copyright file="IdentifiableAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Util.Attributes;

    /// <summary>
    ///     The identifiable bean assembler.
    /// </summary>
    public abstract class IdentifiableAssembler : AbstractAssembler
    {
        /// <summary>
        ///     The assemble identifiable.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="assembleFrom"/> is <see langword="null" />.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="assembleInto"/> is <see langword="null" />.</exception>
        public void AssembleIdentifiable([ValidatedNotNull]IdentifiableType assembleInto, [ValidatedNotNull] IIdentifiableObject assembleFrom)
        {
            if (assembleInto == null)
            {
                throw new ArgumentNullException("assembleInto");
            }

            if (assembleFrom == null)
            {
                throw new ArgumentNullException("assembleFrom");
            }

            if (this.HasAnnotations(assembleFrom))
            {
                assembleInto.Annotations = new Annotations(this.GetAnnotationsType(assembleFrom));
            }

            string str0 = assembleFrom.Id;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                assembleInto.id = assembleFrom.Id;
            }

            if (assembleFrom.Uri != null)
            {
                assembleInto.uri = assembleFrom.Uri;
            }

            assembleInto.urn = assembleFrom.Urn;
        }
    }
}