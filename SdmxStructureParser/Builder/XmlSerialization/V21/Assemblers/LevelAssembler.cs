// -----------------------------------------------------------------------
// <copyright file="LevelAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;

    /// <summary>
    ///     The level assembler.
    /// </summary>
    public class LevelAssembler : NameableAssembler, IAssembler<LevelType, ILevelObject>
    {
        /// <summary>
        ///     The text format assembler.
        /// </summary>
        private readonly TextFormatAssembler _textFormatAssembler = new TextFormatAssembler();

        /// <summary>
        ///     The assemble method. It assembles from <paramref name="assembleFrom" /> into <paramref name="assembleInto" />
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into XSD object.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from SDMX Object.
        /// </param>
        public virtual void Assemble(LevelType assembleInto, ILevelObject assembleFrom)
        {
            var levels = new Stack<KeyValuePair<LevelType, ILevelObject>>();
            levels.Push(new KeyValuePair<LevelType, ILevelObject>(assembleInto, assembleFrom));

            while (levels.Count > 0)
            {
                KeyValuePair<LevelType, ILevelObject> pair = levels.Pop();
                assembleInto = pair.Key;
                assembleFrom = pair.Value;

                this.AssembleNameable(assembleInto, assembleFrom);
                if (assembleFrom.CodingFormat != null)
                {
                    var codingTextFormatType = new CodingTextFormatType();
                    assembleInto.CodingFormat = codingTextFormatType;
                    this._textFormatAssembler.Assemble(codingTextFormatType, assembleFrom.CodingFormat);
                }

                if (assembleFrom.ChildLevel != null)
                {
                    var levelType = new LevelType();
                    assembleInto.Level = levelType;
                    levels.Push(new KeyValuePair<LevelType, ILevelObject>(levelType, assembleFrom.ChildLevel));

                    ////this.Assemble(levelType, assembleFrom.ChildLevel);
                }
            }
        }
    }
}