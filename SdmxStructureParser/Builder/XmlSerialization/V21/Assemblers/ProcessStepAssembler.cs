// -----------------------------------------------------------------------
// <copyright file="ProcessStepAssembler.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers
{
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The process step bean assembler.
    /// </summary>
    public class ProcessStepAssembler : NameableAssembler, IAssembler<ProcessStepType, IProcessStepObject>
    {
        /// <summary>
        ///     The assemble.
        /// </summary>
        /// <param name="assembleInto">
        ///     The assemble into.
        /// </param>
        /// <param name="assembleFrom">
        ///     The assemble from.
        /// </param>
        public virtual void Assemble(ProcessStepType assembleInto, IProcessStepObject assembleFrom)
        {
            var processes = new Stack<KeyValuePair<ProcessStepType, IProcessStepObject>>();
            processes.Push(new KeyValuePair<ProcessStepType, IProcessStepObject>(assembleInto, assembleFrom));

            while (processes.Count > 0)
            {
                KeyValuePair<ProcessStepType, IProcessStepObject> pair = processes.Pop();
                assembleInto = pair.Key;
                assembleFrom = pair.Value;

                this.AssembleNameable(assembleInto, assembleFrom);

                /* foreach */
                foreach (IInputOutputObject currentInput in assembleFrom.Input)
                {
                    var inputOutputType = new InputOutputType();
                    assembleInto.Input.Add(inputOutputType);
                    this.AssembleInputOutput(currentInput, inputOutputType);
                }

                /* foreach */
                foreach (IInputOutputObject currentOutput in assembleFrom.Output)
                {
                    var inputOutputType = new InputOutputType();
                    assembleInto.Output.Add(inputOutputType);
                    this.AssembleInputOutput(currentOutput, inputOutputType);
                }

                if (assembleFrom.Computation != null)
                {
                    var computationType = new ComputationType();
                    assembleInto.Computation = computationType;
                    this.AssembleComputation(assembleFrom.Computation, computationType);
                }

                /* foreach */
                foreach (ITransition currentTranistion in assembleFrom.Transitions)
                {
                    var transitionType = new TransitionType();
                    assembleInto.Transition.Add(transitionType);
                    this.AssembleTransition(currentTranistion, transitionType);
                }

                /* foreach */
                foreach (IProcessStepObject eachProcessStepBean in assembleFrom.ProcessSteps)
                {
                    var eachProcessStep = new ProcessStepType();
                    assembleInto.ProcessStep.Add(eachProcessStep);
                    processes.Push(
                        new KeyValuePair<ProcessStepType, IProcessStepObject>(eachProcessStep, eachProcessStepBean));

                    ////this.Assemble(eachProcessStep, eachProcessStepBean);
                }
            }
        }

        /// <summary>
        ///     The assemble computation.
        /// </summary>
        /// <param name="computationBean">
        ///     The computation bean.
        /// </param>
        /// <param name="computationType">
        ///     The computation type.
        /// </param>
        private void AssembleComputation(IComputationObject computationBean, ComputationType computationType)
        {
            string value3 = computationBean.LocalId;
            if (!string.IsNullOrWhiteSpace(value3))
            {
                computationType.localID = computationBean.LocalId;
            }

            string value1 = computationBean.SoftwarePackage;
            if (!string.IsNullOrWhiteSpace(value1))
            {
                computationType.softwarePackage = computationBean.SoftwarePackage;
            }

            string value2 = computationBean.SoftwareLanguage;
            if (!string.IsNullOrWhiteSpace(value2))
            {
                computationType.softwareLanguage = computationBean.SoftwareLanguage;
            }

            string value = computationBean.SoftwareVersion;
            if (!string.IsNullOrWhiteSpace(value))
            {
                computationType.softwareVersion = computationBean.SoftwareVersion;
            }

            if (this.HasAnnotations(computationBean))
            {
                computationType.Annotations = new Annotations(this.GetAnnotationsType(computationBean));
            }

            this.GetTextType(computationBean.Description, computationType.Description);
        }

        /// <summary>
        ///     Assemble input output.
        /// </summary>
        /// <param name="currentInput">
        ///     The current input.
        /// </param>
        /// <param name="inputOutputType">
        ///     The input output type.
        /// </param>
        private void AssembleInputOutput(IInputOutputObject currentInput, InputOutputType inputOutputType)
        {
            string value = currentInput.LocalId;
            if (!string.IsNullOrWhiteSpace(value))
            {
                inputOutputType.localID = currentInput.LocalId;
            }

            if (this.HasAnnotations(currentInput))
            {
                inputOutputType.Annotations = new Annotations(this.GetAnnotationsType(currentInput));
            }

            if (currentInput.StructureReference != null)
            {
                var objectReferenceType = new ObjectReferenceType();
                inputOutputType.ObjectReference = objectReferenceType;
                var objectRefType = new ObjectRefType();
                objectReferenceType.SetTypedRef(objectRefType);
                this.SetReference(objectRefType, currentInput.StructureReference);
            }
        }

        /// <summary>
        ///     The assemble transition.
        /// </summary>
        /// <param name="transitionBean">
        ///     The transition bean.
        /// </param>
        /// <param name="transitionType">
        ///     The transition type.
        /// </param>
        private void AssembleTransition(ITransition transitionBean, TransitionType transitionType)
        {
            string value = transitionBean.LocalId;
            if (!string.IsNullOrWhiteSpace(value))
            {
                transitionType.localID = transitionBean.LocalId;
            }

            this.AssembleIdentifiable(transitionType, transitionBean);
            if (transitionBean.TargetStep != null)
            {
                var procRef = new LocalProcessStepReferenceType();
                transitionType.TargetStep = procRef;
                var xref = new LocalProcessStepRefType();
                procRef.Ref = xref;
                xref.id = transitionBean.TargetStep.GetFullIdPath(false);
            }

            if (ObjectUtil.ValidCollection(transitionBean.Condition))
            {
                transitionType.Condition = this.GetTextType(transitionBean.Condition);
            }
            else
            {
                transitionType.Condition.Add(new TextType());
            }
        }
    }
}