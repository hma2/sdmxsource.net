// -----------------------------------------------------------------------
// <copyright file="ReportingTaxonomyXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V21.Assemblers;

    /// <summary>
    ///     The reporting taxonomy xml bean builder.
    /// </summary>
    public class ReportingTaxonomyXmlBuilder : ItemSchemeAssembler, 
                                               IBuilder<ReportingTaxonomyType, IReportingTaxonomyObject>
    {
        /// <summary>
        ///     The reporting category bean assembler bean.
        /// </summary>
        private readonly ReportingCategoryAssembler _reportingCategoryAssemblerBean = new ReportingCategoryAssembler();

        /// <summary>
        ///     Build a <see cref="ReportingTaxonomyType" /> from <paramref name="buildFrom" /> and return it
        /// </summary>
        /// <param name="buildFrom">
        ///     The <see cref="IReportingTaxonomyObject" />
        /// </param>
        /// <returns>
        ///     The <see cref="ReportingTaxonomyType" />.
        /// </returns>
        public virtual ReportingTaxonomyType Build(IReportingTaxonomyObject buildFrom)
        {
            // Create outgoing build
            var builtObj = new ReportingTaxonomyType();

            // Populate it from inherited super
            this.AssembleItemScheme(builtObj, buildFrom);

            // Populate it using this class's specifics
            foreach (IReportingCategoryObject eachReportingCategoryBean in buildFrom.Items)
            {
                var newReportingCategory = new ReportingCategory();
                builtObj.Item.Add(newReportingCategory);
                this._reportingCategoryAssemblerBean.Assemble(newReportingCategory.Content, eachReportingCategoryBean);
            }

            return builtObj;
        }
    }
}