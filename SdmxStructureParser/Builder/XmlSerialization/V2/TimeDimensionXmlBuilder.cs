// -----------------------------------------------------------------------
// <copyright file="TimeDimensionXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The time dimension xml bean builder.
    /// </summary>
    public class TimeDimensionXmlBuilder : AbstractBuilder, IBuilder<TimeDimensionType, IDimension>
    {
        /// <summary>
        ///     Build <see cref="StructureSetType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The source SDMX Object.
        /// </param>
        /// <returns>
        ///     The <see cref="StructureSetType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        public virtual TimeDimensionType Build(IDimension buildFrom)
        {
            var builtObj = new TimeDimensionType();

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.HasCodedRepresentation())
            {
                IMaintainableRefObject maintRef = buildFrom.Representation.Representation.MaintainableReference;
                string value2 = maintRef.MaintainableId;
                if (!string.IsNullOrWhiteSpace(value2))
                {
                    builtObj.codelist = maintRef.MaintainableId;
                }

                string value1 = maintRef.AgencyId;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    builtObj.codelistAgency = maintRef.AgencyId;
                }

                string value = maintRef.Version;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    builtObj.codelistVersion = maintRef.Version;
                }
            }

            if (buildFrom.ConceptRef != null)
            {
                IMaintainableRefObject maintRef0 = buildFrom.ConceptRef.MaintainableReference;

                string value3 = maintRef0.AgencyId;
                if (!string.IsNullOrWhiteSpace(value3))
                {
                    builtObj.conceptSchemeAgency = maintRef0.AgencyId;
                }

                string value1 = maintRef0.MaintainableId;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    builtObj.conceptSchemeRef = maintRef0.MaintainableId;
                }

                string value = buildFrom.ConceptRef.ChildReference.Id;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    builtObj.conceptRef = buildFrom.ConceptRef.ChildReference.Id;
                }

                string value2 = maintRef0.Version;
                if (!string.IsNullOrWhiteSpace(value2))
                {
                    builtObj.conceptVersion = maintRef0.Version;
                }

                if (buildFrom.Representation != null && buildFrom.Representation.TextFormat != null)
                {
                    var textFormatType = new TextFormatType();
                    this.PopulateTextFormatType(textFormatType, buildFrom.Representation.TextFormat);
                    builtObj.TextFormat = textFormatType;
                }
            }

            return builtObj;
        }
    }
}