// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The concept scheme xml bean builder.
    /// </summary>
    public class ConceptSchemeXmlBuilder : AbstractBuilder, IBuilder<ConceptSchemeType, IConceptSchemeObject>
    {
        /// <summary>
        ///     The concept xml bean builder.
        /// </summary>
        private readonly IBuilder<ConceptType, IConceptObject> _conceptXmlBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConceptSchemeXmlBuilder"/> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="builderFactory" /> is null.</exception>
        /// <remarks>Used by <see cref="XmlBuilderFactoryV2"/></remarks>
        public ConceptSchemeXmlBuilder(IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            this._conceptXmlBuilder = builderFactory.GetBuilder<ConceptType, IConceptObject>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConceptSchemeXmlBuilder" /> class.
        /// </summary>
        /// <param name="conceptXmlBuilder">The concept XML builder.</param>
        public ConceptSchemeXmlBuilder(IBuilder<ConceptType, IConceptObject> conceptXmlBuilder)
        {
            this._conceptXmlBuilder = conceptXmlBuilder;
        }

        /// <summary>
        ///     Build <see cref="ConceptSchemeType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="ConceptSchemeType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual ConceptSchemeType Build(IConceptSchemeObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new ConceptSchemeType();
            string str0 = buildFrom.AgencyId;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                builtObj.agencyID = buildFrom.AgencyId;
            }

            string str1 = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(str1))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = builtObj.uri;
            }
            else if (buildFrom.StructureUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }
            else if (buildFrom.ServiceUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }

            builtObj.urn = buildFrom.Urn;

            string value = buildFrom.Version;
            if (!string.IsNullOrWhiteSpace(value))
            {
                builtObj.version = buildFrom.Version;
            }

            if (buildFrom.StartDate != null)
            {
                builtObj.validFrom = buildFrom.StartDate.Date;
            }

            if (buildFrom.EndDate != null)
            {
                builtObj.validTo = buildFrom.EndDate.Date;
            }

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.IsExternalReference.IsSet())
            {
                builtObj.isExternalReference = buildFrom.IsExternalReference.IsTrue;
            }

            if (buildFrom.IsFinal.IsSet())
            {
                builtObj.isFinal = buildFrom.IsFinal.IsTrue;
            }

            /* foreach */
            foreach (IConceptObject categoryBean in buildFrom.Items)
            {
                builtObj.Concept.Add(this._conceptXmlBuilder.Build(categoryBean));
            }

            return builtObj;
        }
    }
}