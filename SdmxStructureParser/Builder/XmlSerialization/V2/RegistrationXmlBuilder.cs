// -----------------------------------------------------------------------
// <copyright file="RegistrationXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The registration xml bean builder.
    /// </summary>
    public class RegistrationXmlBuilder : AbstractBuilder
    {
        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly RegistrationXmlBuilder _instance = new RegistrationXmlBuilder();

        /// <summary>
        ///     Prevents a default instance of the <see cref="RegistrationXmlBuilder" /> class from being created.
        /// </summary>
        private RegistrationXmlBuilder()
        {
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static RegistrationXmlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Build <see cref="RegistrationType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistrationType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public RegistrationType Build(IRegistrationObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new RegistrationType();
            if (buildFrom.LastUpdated != null)
            {
                builtObj.LastUpdated = buildFrom.LastUpdated.Date;
            }

            if (buildFrom.ValidFrom != null)
            {
                builtObj.ValidFrom = buildFrom.ValidFrom.Date;
            }

            if (buildFrom.ValidTo != null)
            {
                builtObj.ValidTo = buildFrom.ValidTo.Date;
            }

            if (buildFrom.ProvisionAgreementRef != null)
            {
                ICrossReference provRefBean = buildFrom.ProvisionAgreementRef;
                var provRefType = new ProvisionAgreementRefType();
                builtObj.ProvisionAgreementRef = provRefType;

                if (provRefBean.TargetReference.EnumType == SdmxStructureEnumType.ProvisionAgreement)
                {
                    if (ObjectUtil.ValidString(provRefBean.TargetUrn))
                    {
                        provRefType.URN = provRefBean.TargetUrn;
                    }
                }
            }

            if (buildFrom.DataSource != null)
            {
                IDataSource datasourceBean = buildFrom.DataSource;
                var datasourceType = new DatasourceType();
                builtObj.Datasource = datasourceType;
                if (datasourceBean.SimpleDatasource)
                {
                    datasourceType.SimpleDatasource = datasourceBean.DataUrl;
                }
                else
                {
                    var qdst = new QueryableDatasourceType();
                    datasourceType.QueryableDatasource = qdst;
                    qdst.isRESTDatasource = datasourceBean.RESTDatasource;
                    qdst.isWebServiceDatasource = datasourceBean.WebServiceDatasource;
                    qdst.DataUrl = datasourceBean.DataUrl;
                }
            }

            return builtObj;
        }

        // PRIVATE CONSTRUCTOR
    }
}