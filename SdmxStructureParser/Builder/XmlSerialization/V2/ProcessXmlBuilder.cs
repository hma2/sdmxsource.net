// -----------------------------------------------------------------------
// <copyright file="ProcessXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The process xml codelistRef builder.
    /// </summary>
    public class ProcessXmlBuilder : AbstractBuilder, IBuilder<ProcessType, IProcessObject>
    {
        /// <summary>
        ///     Build <see cref="ProcessType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="ProcessType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public ProcessType Build(IProcessObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new ProcessType();

            // MAINTAINABLE ATTRIBTUES
            string value = buildFrom.AgencyId;
            if (!string.IsNullOrWhiteSpace(value))
            {
                builtObj.agencyID = buildFrom.AgencyId;
            }

            string value1 = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(value1))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }
            else if (buildFrom.StructureUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }
            else if (buildFrom.ServiceUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }

            if (ObjectUtil.ValidString(buildFrom.Urn))
            {
                builtObj.urn = buildFrom.Urn;
            }

            string value2 = buildFrom.Version;
            if (!string.IsNullOrWhiteSpace(value2))
            {
                builtObj.version = buildFrom.Version;
            }

            if (buildFrom.StartDate != null)
            {
                builtObj.validFrom = buildFrom.StartDate;
            }

            if (buildFrom.EndDate != null)
            {
                builtObj.validTo = buildFrom.EndDate;
            }

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.IsExternalReference.IsSet())
            {
                builtObj.isExternalReference = buildFrom.IsExternalReference.IsTrue;
            }

            if (buildFrom.IsFinal.IsSet())
            {
                builtObj.isFinal = buildFrom.IsFinal.IsTrue;
            }

            /* foreach */
            foreach (IProcessStepObject processStep in buildFrom.ProcessSteps)
            {
                var processStepType = new ProcessStepType();
                builtObj.ProcessStep.Add(processStepType);
                this.ProcessProcessStep(processStep, processStepType);
            }

            return builtObj;
        }

        /// <summary>
        ///     Process <paramref name="buildFrom" />
        /// </summary>
        /// <param name="buildFrom">
        ///     The <see cref="IProcessStepObject" /> to build from
        /// </param>
        /// <param name="builtObj">
        ///     The output
        /// </param>
        private void ProcessProcessStep(IProcessStepObject buildFrom, ProcessStepType builtObj)
        {
            string value = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(value))
            {
                builtObj.id = buildFrom.Id;
            }

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            IList<IInputOutputObject> inputObjects = buildFrom.Input;
            if (ObjectUtil.ValidCollection(inputObjects))
            {
                /* foreach */
                foreach (IInputOutputObject input in inputObjects)
                {
                    if (input.StructureReference != null)
                    {
                        builtObj.Input.Add(
                            XmlobjectsEnumUtil.GetSdmxObjectIdType(input.StructureReference.TargetReference));
                    }
                }
            }

            IList<IInputOutputObject> outputObjects = buildFrom.Output;
            if (ObjectUtil.ValidCollection(outputObjects))
            {
                /* foreach */
                foreach (IInputOutputObject outputObject in outputObjects)
                {
                    if (outputObject.StructureReference != null)
                    {
                        builtObj.Input.Add(
                            XmlobjectsEnumUtil.GetSdmxObjectIdType(outputObject.StructureReference.TargetReference));
                    }
                }
            }

            if (buildFrom.Computation != null)
            {
                builtObj.Computation.AddAll(this.GetTextType(buildFrom.Computation.Description));
            }

            IList<ITransition> transitions = buildFrom.Transitions;
            if (ObjectUtil.ValidCollection(transitions))
            {
                /* foreach */
                foreach (ITransition transition in transitions)
                {
                    var transitionType = new TransitionType();
                    builtObj.Transition.Add(transitionType);
                    if (transition.TargetStep != null)
                    {
                        transitionType.TargetStep = transition.TargetStep.Id;
                    }

                    IList<ITextTypeWrapper> textTypeWrappers = transition.Condition;
                    if (ObjectUtil.ValidCollection(textTypeWrappers))
                    {
                        transitionType.Condition = this.GetTextType(textTypeWrappers[0]);
                    }
                }
            }

            IList<IProcessStepObject> processStepObjects = buildFrom.ProcessSteps;
            if (ObjectUtil.ValidCollection(processStepObjects))
            {
                /* foreach */
                foreach (IProcessStepObject processStep in processStepObjects)
                {
                    var newProcessStepType = new ProcessStepType();
                    builtObj.ProcessStep.Add(newProcessStepType);
                    this.ProcessProcessStep(processStep, newProcessStepType);
                }
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }
        }
    }
}