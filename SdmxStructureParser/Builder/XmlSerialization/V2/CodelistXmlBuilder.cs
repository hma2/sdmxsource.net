// -----------------------------------------------------------------------
// <copyright file="CodelistXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The codelist xml bean builder.
    /// </summary>
    public class CodelistXmlBuilder : AbstractBuilder, IBuilder<CodeListType, ICodelistObject>
    {
        /// <summary>
        ///     The code xml bean builder.
        /// </summary>
        private readonly IBuilder<CodeType, ICode> _codeXmlBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistXmlBuilder"/> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        /// <exception cref="System.ArgumentNullException">The <paramref name="builderFactory" /> is null.</exception>
        /// <remarks>Used by <see cref="XmlBuilderFactoryV2"/></remarks>
        public CodelistXmlBuilder(IBuilderFactory builderFactory)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            this._codeXmlBuilder = builderFactory.GetBuilder<CodeType, ICode>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CodelistXmlBuilder" /> class.
        /// </summary>
        /// <param name="codeXmlBuilder">The code XML builder.</param>
        public CodelistXmlBuilder(IBuilder<CodeType, ICode> codeXmlBuilder)
        {
            this._codeXmlBuilder = codeXmlBuilder;
        }

        /// <summary>
        ///     Build <see cref="CodeListType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="CodeListType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual CodeListType Build(ICodelistObject buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new CodeListType();
            string str0 = buildFrom.AgencyId;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                builtObj.agencyID = buildFrom.AgencyId;
            }

            string value = buildFrom.Id;
            if (!string.IsNullOrWhiteSpace(value))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }
            else if (buildFrom.StructureUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }
            else if (buildFrom.ServiceUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }

            if (ObjectUtil.ValidString(buildFrom.Urn))
            {
                builtObj.urn = buildFrom.Urn;
            }

            string value1 = buildFrom.Version;
            if (!string.IsNullOrWhiteSpace(value1))
            {
                builtObj.version = buildFrom.Version;
            }

            if (buildFrom.StartDate != null)
            {
                builtObj.validFrom = buildFrom.StartDate.Date;
            }

            if (buildFrom.EndDate != null)
            {
                builtObj.validTo = buildFrom.EndDate.Date;
            }

            IList<ITextTypeWrapper> names = buildFrom.Names;
            if (ObjectUtil.ValidCollection(names))
            {
                builtObj.Name = this.GetTextType(names);
            }

            IList<ITextTypeWrapper> descriptions = buildFrom.Descriptions;
            if (ObjectUtil.ValidCollection(descriptions))
            {
                builtObj.Description = this.GetTextType(descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.IsExternalReference.IsSet())
            {
                builtObj.isExternalReference = buildFrom.IsExternalReference.IsTrue;
            }

            if (buildFrom.IsFinal.IsSet())
            {
                builtObj.isFinal = buildFrom.IsFinal.IsTrue;
            }

            foreach (ICode code in buildFrom.Items)
            {
                builtObj.Code.Add(this._codeXmlBuilder.Build(code));
            }

            ////int i = 0;
            ////IList<ICode> codes = buildFrom.Items;
            ////var codeTypes = new CodeType[codes.Count];

            /////* foreach */
            ////foreach (ICode codeBean in codes)
            ////{
            ////    codeTypes[i] = this._codeXmlBuilder.Build(codeBean);
            ////    i++;
            ////}

            ////builtObj.Code = codeTypes;
            return builtObj;
        }
    }
}