// -----------------------------------------------------------------------
// <copyright file="AbstractBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util;

    using DataflowRefType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry.DataflowRefType;
    using DataProviderRefType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry.DataProviderRefType;
    using MetadataflowRefType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry.MetadataflowRefType;
    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The abstract builder.
    /// </summary>
    public class AbstractBuilder
    {
        /// <summary>
        ///     The default language.
        /// </summary>
        public const string DefaultLang = "en";

        /// <summary>
        ///     The default text value.
        /// </summary>
        public const string DefaultTextValue = "Undefined";

        /// <summary>
        ///     Gets annotations type.
        /// </summary>
        /// <param name="annotable">
        ///     The annotable.
        /// </param>
        /// <returns>
        ///     The <see cref="AnnotationsType" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="annotable"/> is <see langword="null" />.</exception>
        protected AnnotationsType GetAnnotationsType(IAnnotableObject annotable)
        {
            if (annotable == null)
            {
                throw new ArgumentNullException("annotable");
            }

            IList<IAnnotation> currentAnnotationBeans = annotable.Annotations;
            if (!ObjectUtil.ValidCollection(currentAnnotationBeans))
            {
                return null;
            }

            var returnType = new AnnotationsType();

            /* foreach */
            foreach (IAnnotation currentAnnotationBean in currentAnnotationBeans)
            {
                var annotation = new AnnotationType();
                returnType.Annotation.Add(annotation);
                IList<ITextTypeWrapper> text = currentAnnotationBean.Text;
                if (ObjectUtil.ValidCollection(text))
                {
                    annotation.AnnotationText = this.GetTextType(text);
                }

                string value = currentAnnotationBean.Title;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    annotation.AnnotationTitle = currentAnnotationBean.Title;
                }

                string value1 = currentAnnotationBean.Type;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    annotation.AnnotationType1 = currentAnnotationBean.Type;
                }

                if (currentAnnotationBean.Uri != null)
                {
                    annotation.AnnotationURL = currentAnnotationBean.Uri;
                }
            }

            return returnType;
        }

        /// <summary>
        ///     Gets text type.
        /// </summary>
        /// <param name="textTypeWrappers">
        ///     The text type wrappers.
        /// </param>
        /// <returns>
        ///     The array of <see cref="Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType" /> .
        /// </returns>
        protected TextType[] GetTextType(IList<ITextTypeWrapper> textTypeWrappers)
        {
            if (!ObjectUtil.ValidCollection(textTypeWrappers))
            {
                return null;
            }

            var textTypes = new TextType[textTypeWrappers.Count];
            for (int i = 0; i < textTypeWrappers.Count; i++)
            {
                TextType tt = this.GetTextType(textTypeWrappers[i]);
                textTypes[i] = tt;
            }

            return textTypes;
        }

        /// <summary>
        ///     Gets the text type from <paramref name="textTypeWrapper" />
        /// </summary>
        /// <param name="textTypeWrapper">
        ///     The text Type wrapper.
        /// </param>
        /// <returns>
        ///     The <see cref="TextType" /> from <paramref name="textTypeWrapper" /> .
        /// </returns>
        protected TextType GetTextType(ITextTypeWrapper textTypeWrapper)
        {
            if (textTypeWrapper == null)
            {
                throw new ArgumentNullException("textTypeWrapper");
            }

            var tt = new TextType { lang = textTypeWrapper.Locale, TypedValue = textTypeWrapper.Value };
            return tt;
        }

        /// <summary>
        ///     Returns true if <paramref name="annotable" /> has annotations.
        /// </summary>
        /// <param name="annotable">
        ///     The annotable.
        /// </param>
        /// <returns>
        ///     True if <paramref name="annotable" /> has annotations; otherwise false
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="annotable"/> is <see langword="null" />.</exception>
        protected bool HasAnnotations(IAnnotableObject annotable)
        {
            if (annotable == null)
            {
                throw new ArgumentNullException("annotable");
            }

            IList<IAnnotation> annotations = annotable.Annotations;
            if (ObjectUtil.ValidCollection(annotations))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Populate the <paramref name="dataflowRef" />  from <paramref name="crossReference" />
        /// </summary>
        /// <param name="crossReference">
        ///     The cross reference.
        /// </param>
        /// <param name="dataflowRef">
        ///     The reference to populate
        /// </param>
        protected void PopulateDataflowRef(ICrossReference crossReference, DataflowRefType dataflowRef)
        {
            if (crossReference == null)
            {
                throw new ArgumentNullException("crossReference");
            }

            if (dataflowRef == null)
            {
                throw new ArgumentNullException("dataflowRef");
            }

            IMaintainableRefObject maintRef = crossReference.MaintainableReference;
            dataflowRef.URN = crossReference.TargetUrn;

            string str1 = maintRef.AgencyId;
            if (!string.IsNullOrWhiteSpace(str1))
            {
                dataflowRef.AgencyID = maintRef.AgencyId;
            }

            string str2 = maintRef.MaintainableId;
            if (!string.IsNullOrWhiteSpace(str2))
            {
                dataflowRef.DataflowID = maintRef.MaintainableId;
            }

            string str3 = maintRef.Version;
            if (!string.IsNullOrWhiteSpace(str3))
            {
                dataflowRef.Version = maintRef.Version;
            }
        }

        /// <summary>
        ///     Populate the <paramref name="dataProviderRef" />  from <paramref name="crossReference" />
        /// </summary>
        /// <param name="crossReference">
        ///     The cross reference.
        /// </param>
        /// <param name="dataProviderRef">
        ///     The data Provider reference.
        /// </param>
        protected void PopulateDataproviderRef(ICrossReference crossReference, DataProviderRefType dataProviderRef)
        {
            if (crossReference == null)
            {
                throw new ArgumentNullException("crossReference");
            }

            if (dataProviderRef == null)
            {
                throw new ArgumentNullException("dataProviderRef");
            }

            IMaintainableRefObject maintRef = crossReference.MaintainableReference;
            if (crossReference.TargetUrn != null)
            {
                dataProviderRef.URN = crossReference.TargetUrn;
            }

            string str1 = maintRef.AgencyId;
            if (!string.IsNullOrWhiteSpace(str1))
            {
                dataProviderRef.OrganisationSchemeAgencyID = maintRef.AgencyId;
            }

            string str2 = maintRef.MaintainableId;
            if (!string.IsNullOrWhiteSpace(str2))
            {
                dataProviderRef.OrganisationSchemeID = maintRef.MaintainableId;
            }

            string str3 = maintRef.Version;
            if (!string.IsNullOrWhiteSpace(str3))
            {
                dataProviderRef.Version = maintRef.Version;
            }

            string str4 = crossReference.ChildReference.Id;
            if (!string.IsNullOrWhiteSpace(str4))
            {
                dataProviderRef.DataProviderID = crossReference.ChildReference.Id;
            }
        }

        /// <summary>
        ///     Populate the <paramref name="metadataflowRef" />  from <paramref name="crossReference" />
        /// </summary>
        /// <param name="crossReference">
        ///     The cross reference.
        /// </param>
        /// <param name="metadataflowRef">
        ///     The metadataflow reference.
        /// </param>
        protected void PopulateMetadataflowRef(ICrossReference crossReference, MetadataflowRefType metadataflowRef)
        {
            if (crossReference == null)
            {
                throw new ArgumentNullException("crossReference");
            }

            if (metadataflowRef == null)
            {
                throw new ArgumentNullException("metadataflowRef");
            }

            IMaintainableRefObject maintRef = crossReference.MaintainableReference;
            if (crossReference.TargetUrn != null)
            {
                metadataflowRef.URN = crossReference.TargetUrn;
            }

            string str1 = maintRef.AgencyId;
            if (!string.IsNullOrWhiteSpace(str1))
            {
                metadataflowRef.AgencyID = maintRef.AgencyId;
            }

            string str2 = maintRef.MaintainableId;
            if (!string.IsNullOrWhiteSpace(str2))
            {
                metadataflowRef.MetadataflowID = maintRef.MaintainableId;
            }

            string str3 = maintRef.Version;
            if (!string.IsNullOrWhiteSpace(str3))
            {
                metadataflowRef.Version = maintRef.Version;
            }
        }

        /// <summary>
        ///     The populate text format type.
        /// </summary>
        /// <param name="textFormatType">
        ///     The text format type.
        /// </param>
        /// <param name="textFormat">
        ///     The text format.
        /// </param>
        /// <exception cref="OverflowException">One of the values represents a number that is less than <see cref="F:System.Double.MinValue" /> or greater than <see cref="F:System.Double.MaxValue" />. </exception>
        /// <exception cref="InvalidCastException">One of the values does not implement the <see cref="T:System.IConvertible" /> interface. </exception>
        protected void PopulateTextFormatType(TextFormatType textFormatType, ITextFormat textFormat)
        {
            if (textFormatType == null)
            {
                throw new ArgumentNullException("textFormatType");
            }

            if (textFormat == null)
            {
                throw new ArgumentNullException("textFormat");
            }

            if (textFormat.TextType != null)
            {
                SetTextType(textFormatType, textFormat);
            }

            string str0 = textFormat.Pattern;
            if (!string.IsNullOrWhiteSpace(str0))
            {
                textFormatType.pattern = textFormat.Pattern;
            }

            TimeSpan result;
            string str1 = textFormat.TimeInterval;
            if (!string.IsNullOrWhiteSpace(str1) && TimeSpan.TryParse(textFormat.TimeInterval, out result))
            {
                textFormatType.timeInterval = result;
            }

            if (textFormat.Decimals != null)
            {
                textFormatType.decimals = textFormat.Decimals;
            }

            if (textFormat.EndValue != null)
            {
                textFormatType.endValue = Convert.ToDouble(textFormat.EndValue.Value, CultureInfo.InvariantCulture);
            }

            if (textFormat.Interval != null)
            {
                textFormatType.interval = Convert.ToDouble(textFormat.Interval.Value, CultureInfo.InvariantCulture);
            }

            if (textFormat.MaxLength != null)
            {
                textFormatType.maxLength = textFormat.MaxLength;
            }

            if (textFormat.MinLength != null)
            {
                textFormatType.minLength = textFormat.MinLength;
            }

            if (textFormat.StartValue != null)
            {
                textFormatType.startValue = Convert.ToDouble(textFormat.StartValue.Value, CultureInfo.InvariantCulture);
            }

            if (textFormat.Sequence.IsSet())
            {
                textFormatType.isSequence = textFormat.Sequence.IsTrue;
            }
        }

        /// <summary>
        /// Handles the maintainable information.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="builtObj">The built object.</param>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        protected void HandleMaintainableInformation(IMaintainableObject buildFrom, IMaintainableType builtObj)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (builtObj == null)
            {
                throw new ArgumentNullException("builtObj");
            }

            if (!string.IsNullOrWhiteSpace(buildFrom.AgencyId))
            {
                builtObj.agencyID = buildFrom.AgencyId;
            }

            if (!string.IsNullOrWhiteSpace(buildFrom.Id))
            {
                builtObj.id = buildFrom.Id;
            }

            if (buildFrom.Uri != null)
            {
                builtObj.uri = buildFrom.Uri;
            }
            else if (buildFrom.StructureUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }
            else if (buildFrom.ServiceUrl != null)
            {
                builtObj.uri = buildFrom.StructureUrl;
            }

            builtObj.urn = buildFrom.Urn;

            if (!string.IsNullOrWhiteSpace(buildFrom.Version))
            {
                builtObj.version = buildFrom.Version;
            }

            if (buildFrom.StartDate != null)
            {
                builtObj.validFrom = buildFrom.StartDate.Date;
            }

            if (buildFrom.EndDate != null)
            {
                builtObj.validTo = buildFrom.EndDate.Date;
            }

            if (ObjectUtil.ValidCollection(buildFrom.Names))
            {
                builtObj.Name = this.GetTextType(buildFrom.Names);
            }

            if (ObjectUtil.ValidCollection(buildFrom.Descriptions))
            {
                builtObj.Description = this.GetTextType(buildFrom.Descriptions);
            }

            if (this.HasAnnotations(buildFrom))
            {
                builtObj.Annotations = this.GetAnnotationsType(buildFrom);
            }

            if (buildFrom.IsExternalReference.IsSet())
            {
                builtObj.isExternalReference = buildFrom.IsExternalReference.IsTrue;
            }

            if (buildFrom.IsFinal.IsSet())
            {
                builtObj.isFinal = buildFrom.IsFinal.IsTrue;
            }
        }

        /// <summary>
        ///     Sets the default text.
        /// </summary>
        /// <param name="value">
        ///     The value.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null" />.</exception>
        protected void SetDefaultText(TextType value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            value.lang = DefaultLang;
            value.TypedValue = DefaultTextValue;
        }

        /// <summary>
        /// Sets the type of the text.
        /// </summary>
        /// <param name="textFormatType">Type of the text format.</param>
        /// <param name="textFormat">The text format.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "It is OK. Big switch statement.")]
        private static void SetTextType(TextFormatType textFormatType, ITextFormat textFormat)
        {
            switch (textFormat.TextType.EnumType)
            {
                case TextEnumType.BigInteger:
                    textFormatType.textType = TextTypeTypeConstants.BigInteger;
                    break;
                case TextEnumType.Boolean:
                    textFormatType.textType = TextTypeTypeConstants.Boolean;
                    break;
                case TextEnumType.Count:
                    textFormatType.textType = TextTypeTypeConstants.Count;
                    break;
                case TextEnumType.Date:
                    textFormatType.textType = TextTypeTypeConstants.Date;
                    break;
                case TextEnumType.DateTime:
                    textFormatType.textType = TextTypeTypeConstants.DateTime;
                    break;
                case TextEnumType.Day:
                    textFormatType.textType = TextTypeTypeConstants.Day;
                    break;
                case TextEnumType.Decimal:
                    textFormatType.textType = TextTypeTypeConstants.Decimal;
                    break;
                case TextEnumType.Double:
                    textFormatType.textType = TextTypeTypeConstants.Double;
                    break;
                case TextEnumType.Duration:
                    textFormatType.textType = TextTypeTypeConstants.Duration;
                    break;
                case TextEnumType.ExclusiveValueRange:
                    textFormatType.textType = TextTypeTypeConstants.ExclusiveValueRange;
                    break;
                case TextEnumType.Float:
                    textFormatType.textType = TextTypeTypeConstants.Float;
                    break;
                case TextEnumType.InclusiveValueRange:
                    textFormatType.textType = TextTypeTypeConstants.InclusiveValueRange;
                    break;
                case TextEnumType.Incremental:
                    textFormatType.textType = TextTypeTypeConstants.Incremental;
                    break;
                case TextEnumType.Integer:
                    textFormatType.textType = TextTypeTypeConstants.Integer;
                    break;
                case TextEnumType.Long:
                    textFormatType.textType = TextTypeTypeConstants.Long;
                    break;
                case TextEnumType.Month:
                    textFormatType.textType = TextTypeTypeConstants.Month;
                    break;
                case TextEnumType.MonthDay:
                    textFormatType.textType = TextTypeTypeConstants.MonthDay;
                    break;
                case TextEnumType.ObservationalTimePeriod:
                    textFormatType.textType = TextTypeTypeConstants.ObservationalTimePeriod;
                    break;
                case TextEnumType.Short:
                    textFormatType.textType = TextTypeTypeConstants.Short;
                    break;
                case TextEnumType.String:
                    textFormatType.textType = TextTypeTypeConstants.String;
                    break;
                case TextEnumType.Time:
                    textFormatType.textType = TextTypeTypeConstants.Time;
                    break;
                case TextEnumType.Timespan:
                    textFormatType.textType = TextTypeTypeConstants.Timespan;
                    break;
                case TextEnumType.Uri:
                    textFormatType.textType = TextTypeTypeConstants.URI;
                    break;
                case TextEnumType.Year:
                    textFormatType.textType = TextTypeTypeConstants.Year;
                    break;
                case TextEnumType.YearMonth:
                    textFormatType.textType = TextTypeTypeConstants.YearMonth;
                    break;
            }
        }
    }
}