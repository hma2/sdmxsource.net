// -----------------------------------------------------------------------
// <copyright file="CodelistRefXmlBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    ///     The codelist ref xml bean builder.
    /// </summary>
    public class CodelistRefXmlBuilder : AbstractBuilder, IBuilder<CodelistRefType, ICodelistRef>
    {
        /// <summary>
        ///     Build <see cref="CodelistRefType" /> from <paramref name="buildFrom" />.
        /// </summary>
        /// <param name="buildFrom">
        ///     The build from.
        /// </param>
        /// <returns>
        ///     The <see cref="CodelistRefType" /> from <paramref name="buildFrom" /> .
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public virtual CodelistRefType Build(ICodelistRef buildFrom)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var builtObj = new CodelistRefType();
            if (buildFrom.CodelistReference != null)
            {
                IMaintainableRefObject maintRef = buildFrom.CodelistReference.MaintainableReference;
                string str0 = maintRef.AgencyId;
                if (!string.IsNullOrWhiteSpace(str0))
                {
                    builtObj.AgencyID = maintRef.AgencyId;
                }

                string str1 = buildFrom.Alias;
                if (!string.IsNullOrWhiteSpace(str1))
                {
                    builtObj.Alias = buildFrom.Alias;
                }

                string str2 = maintRef.MaintainableId;
                if (!string.IsNullOrWhiteSpace(str2))
                {
                    builtObj.CodelistID = maintRef.MaintainableId;
                }

                builtObj.URN = buildFrom.CodelistReference.TargetUrn;

                string str4 = maintRef.Version;
                if (!string.IsNullOrWhiteSpace(str4))
                {
                    builtObj.Version = maintRef.Version;
                }
            }

            return builtObj;
        }
    }
}