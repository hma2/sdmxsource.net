// -----------------------------------------------------------------------
// <copyright file="QueryStructureResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V1;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;

    using Xml.Schema.Linq;

    /// <summary>
    ///     Builds error and success responses for querying structures.
    /// </summary>
    public class QueryStructureResponseBuilder : XmlObjectBuilder, IQueryStructureResponseBuilder
    {
        /// <summary>
        ///     The error response builder 21.
        /// </summary>
        private readonly ErrorResponseBuilder _errorResponseBuilder21 = new ErrorResponseBuilder();

        /// <summary>
        ///     The Query structure request v 2 builder.
        /// </summary>
        private readonly QueryStructureResponseBuilderV2 _queryStructureResponseBuilderV2;

        /// <summary>
        ///     The structure 1 builder.
        /// </summary>
        private readonly StructureXmlBuilder _structv1Builder;

        /// <summary>
        ///     The structure v 21 builder.
        /// </summary>
        private readonly XmlSerialization.V21.StructureXmlBuilder _structV21Builder;

        /// <summary>
        ///     The structure v2 builder.
        /// </summary>
        private readonly XmlSerialization.V2.StructureXmlBuilder _structv2Builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryStructureResponseBuilder"/> class.
        /// </summary>
        /// <param name="queryStructureResponseBuilderV2">The query structure response builder for SDMX v2.0.</param>
        /// <param name="structv1Builder">The structure SDMX v1.0 builder.</param>
        /// <param name="structV21Builder">The structure SDMX v2.1 builder.</param>
        /// <param name="structv2Builder">The structure SDMX v2.0 builder.</param>
        public QueryStructureResponseBuilder(QueryStructureResponseBuilderV2 queryStructureResponseBuilderV2, StructureXmlBuilder structv1Builder, XmlSerialization.V21.StructureXmlBuilder structV21Builder, XmlSerialization.V2.StructureXmlBuilder structv2Builder)
        {
            this._queryStructureResponseBuilderV2 = queryStructureResponseBuilderV2;
            this._structv1Builder = structv1Builder;
            this._structV21Builder = structV21Builder;
            this._structv2Builder = structv2Builder;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryStructureResponseBuilder"/> class.
        /// </summary>
        public QueryStructureResponseBuilder()
        {
            IBuilderFactory builderFactoryV20 = new XmlBuilderFactoryV2();
            this._structv2Builder = new XmlSerialization.V2.StructureXmlBuilder(builderFactoryV20);
            this._queryStructureResponseBuilderV2 = new QueryStructureResponseBuilderV2(builderFactoryV20, this._structv2Builder);
            this._structv1Builder = new StructureXmlBuilder();
            this._structV21Builder = new XmlSerialization.V21.StructureXmlBuilder();
        }

        /// <summary>
        ///     Returns an error response based on the exception
        /// </summary>
        /// <param name="exception">
        ///     - the error
        /// </param>
        /// <param name="schemaVersion">
        ///     - the version of the schema to output the response in
        /// </param>
        /// <returns>
        ///     The <see cref="XTypedElement" />.
        /// </returns>
        public virtual XTypedElement BuildErrorResponse(Exception exception, SdmxSchemaEnumType schemaVersion)
        {
            XTypedElement response = null;
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    response = this._errorResponseBuilder21.BuildErrorResponse(exception);
                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    response = this._queryStructureResponseBuilderV2.BuildErrorResponse(exception);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }

            this.WriteSchemaLocation(response, schemaVersion);
            return response;
        }

        /// <summary>
        ///     Builds a success response along with the query results
        /// </summary>
        /// <param name="beans">
        ///     - the beans that were successfully returned from the query
        /// </param>
        /// <param name="schemaVersion">
        ///     - the version of the schema to output the response in
        /// </param>
        /// <param name="returnAsStructureMessage">
        ///     returns a structure message if true, otherwise returns as a query structure response
        /// </param>
        /// <returns>
        ///     The <see cref="XTypedElement" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="beans"/> is <see langword="null" />.</exception>
        public virtual XTypedElement BuildSuccessResponse(
            ISdmxObjects beans, 
            SdmxSchemaEnumType schemaVersion, 
            bool returnAsStructureMessage)
        {
            if (beans == null)
            {
                throw new ArgumentNullException("beans");
            }

            XTypedElement response = null;
            switch (schemaVersion)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    if (beans.GetAllMaintainables().Count == 0)
                    {
                        response = this._errorResponseBuilder21.BuildErrorResponse(SdmxErrorCodeEnumType.NoResultsFound);
                    }
                    else
                    {
                        response = this._structV21Builder.Build(beans);
                    }

                    break;
                case SdmxSchemaEnumType.VersionTwo:
                    if (returnAsStructureMessage)
                    {
                        response = this._structv2Builder.Build(beans);
                    }
                    else
                    {
                        response = this._queryStructureResponseBuilderV2.BuildSuccessResponse(beans);
                    }

                    break;
                case SdmxSchemaEnumType.VersionOne:
                    response = this._structv1Builder.Build(beans);
                    break;
                default:
                    throw new SdmxNotImplementedException(ExceptionCode.Unsupported, schemaVersion);
            }

            this.WriteSchemaLocation(response, schemaVersion);
            return response;
        }
    }
}