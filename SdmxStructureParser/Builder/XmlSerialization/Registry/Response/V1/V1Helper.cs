// -----------------------------------------------------------------------
// <copyright file="V1Helper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V1
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V10.message;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    ///     The v 1 helper.
    /// </summary>
    public static class V1Helper
    {
        /// <summary>
        ///     The reference.
        /// </summary>
        private static int referenceNo = 1;

        /// <summary>
        ///     The set header.
        /// </summary>
        /// <param name="header">
        ///     The header.
        /// </param>
        /// <param name="sdmxObjects">
        ///     The sdmxObjects.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="header"/> is <see langword="null" />.</exception>
        public static void SetHeader(HeaderType header, ISdmxObjects sdmxObjects)
        {
            if (header == null)
            {
                throw new ArgumentNullException("header");
            }

            header.ID = "IDREF" + referenceNo;
            referenceNo++;
            header.Test = false;
            header.Prepared = DateTime.Now;

            string senderId;
            if (sdmxObjects != null && sdmxObjects.Header != null && sdmxObjects.Header.Sender != null)
            {
                // Get header information from the supplied sdmxObjects
                senderId = sdmxObjects.Header.Sender.Id;
            }
            else
            {
                // Get header info from HeaderHelper
                senderId = HeaderHelper.Instance.SenderId;
            }

            header.Sender.id = senderId;
        }
    }
}