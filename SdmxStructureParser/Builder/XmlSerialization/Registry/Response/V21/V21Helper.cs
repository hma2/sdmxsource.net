// -----------------------------------------------------------------------
// <copyright file="V21Helper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V21
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Message;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     v the 21 helper.
    /// </summary>
    public static class V21Helper
    {
        /// <summary>
        ///     The ref.
        /// </summary>
        private static int refNumber = 1;

        /// <summary>
        /// Sets the header.
        /// </summary>
        /// <param name="value">The value.</param>
        public static void SetHeader(RegistryInterfaceType value)
        {
            SetHeader(value, null, null);
        }

        /// <summary>
        ///     set the header.
        /// </summary>
        /// <param name="regInterface">
        ///     the registry interface.
        /// </param>
        /// <param name="beans">
        ///     The beans.
        /// </param>
        /// <param name="receivers">
        ///     The receivers.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="regInterface"/> is <see langword="null" />.</exception>
        public static void SetHeader(RegistryInterfaceType regInterface, ISdmxObjects beans, params string[] receivers)
        {
            if (regInterface == null)
            {
                throw new ArgumentNullException("regInterface");
            }

            var header = new BasicHeaderType();
            regInterface.Header = header;
            SetHeader(header, beans, receivers);
        }

        /// <summary>
        ///     set the header.
        /// </summary>
        /// <param name="header">
        ///     The header.
        /// </param>
        /// <param name="beans">
        ///     The beans.
        /// </param>
        /// <param name="receivers">
        ///     The receivers.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="header"/> is <see langword="null" />.</exception>
        public static void SetHeader(BaseHeaderType header, ISdmxObjects beans, params string[] receivers)
        {
            if (header == null)
            {
                throw new ArgumentNullException("header");
            }

            header.ID = "IDREF" + refNumber;
            refNumber++;
            header.Test = false;
            header.Prepared = DateTime.Now;
            var sender = new SenderType();
            header.Sender = sender;

            string senderId;
            if (beans != null && beans.Header != null && beans.Header.Sender != null)
            {
                // Get header information from the supplied beans
                senderId = beans.Header.Sender.Id;
            }
            else
            {
                // Get header info from HeaderHelper
                senderId = HeaderHelper.Instance.SenderId;
            }

            sender.id = senderId;

            if (ObjectUtil.ValidArray(receivers))
            {
                /* foreach */
                foreach (string currentReviever in receivers)
                {
                    var receiver = new PartyType();
                    header.Receiver.Add(receiver);
                    receiver.id = currentReviever;
                }
            }
            else
            {
                var receiver0 = new PartyType();
                header.Receiver.Add(receiver0);
                receiver0.id = HeaderHelper.Instance.ReceiverId;
            }
        }
    }
}