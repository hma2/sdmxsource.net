// -----------------------------------------------------------------------
// <copyright file="QueryRegistrationResponseBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The query registration response builder v 2.
    /// </summary>
    public class QueryRegistrationResponseBuilderV2 : AbstractResponseBuilder
    {
        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly QueryRegistrationResponseBuilderV2 _instance = new QueryRegistrationResponseBuilderV2();

        /// <summary>
        ///     Prevents a default instance of the <see cref="QueryRegistrationResponseBuilderV2" /> class from being created.
        /// </summary>
        private QueryRegistrationResponseBuilderV2()
        {
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static QueryRegistrationResponseBuilderV2 Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Builds error response.
        /// </summary>
        /// <param name="ex">
        ///     The exception.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        public RegistryInterface BuildErrorResponse(Exception ex)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryRegistrationResponseType();
            regInterface.QueryRegistrationResponse = returnType;
            V2Helper.SetHeader(regInterface);

            var queryResult = new QueryResultType();
            returnType.QueryResult.Add(queryResult);

            queryResult.timeSeriesMatch = false;
            var statusMessage = new StatusMessageType();
            queryResult.StatusMessage = statusMessage;

            this.AddStatus(statusMessage, ex);

            return responseType;
        }

        /// <summary>
        ///     The build success response.
        /// </summary>
        /// <param name="registrations">
        ///     The registrations.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        public RegistryInterface BuildSuccessResponse(ICollection<IRegistrationObject> registrations)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryRegistrationResponseType();
            regInterface.QueryRegistrationResponse = returnType;
            V2Helper.SetHeader(regInterface);

            if (!ObjectUtil.ValidCollection(registrations))
            {
                var queryResult = new QueryResultType();
                returnType.QueryResult.Add(queryResult);

                queryResult.timeSeriesMatch = false;
                var statusMessage = new StatusMessageType();
                queryResult.StatusMessage = statusMessage;

                statusMessage.status = StatusTypeConstants.Warning;
                var tt = new TextType();
                statusMessage.MessageText.Add(tt);

                tt.TypedValue = "No Registrations Match The Query Parameters";
            }
            else
            {
                /* foreach */
                foreach (IRegistrationObject currentRegistration in registrations)
                {
                    var queryResult0 = new QueryResultType();
                    returnType.QueryResult.Add(queryResult0);

                    var statusMessage1 = new StatusMessageType();
                    queryResult0.StatusMessage = statusMessage1;
                    this.AddStatus(statusMessage1, null);

                    queryResult0.timeSeriesMatch = false; // FUNC 1 - when is this true?  Also We need MetadataResult

                    var resultType = new ResultType();
                    queryResult0.DataResult = resultType;

                    if (currentRegistration.DataSource != null)
                    {
                        IDataSource datasourceBean = currentRegistration.DataSource;
                        var datasourceType = new DatasourceType();
                        resultType.Datasource = datasourceType;
                        if (datasourceBean.SimpleDatasource)
                        {
                            datasourceType.SimpleDatasource = datasourceBean.DataUrl;
                        }
                        else
                        {
                            var queryableDatasource = new QueryableDatasourceType();
                            datasourceType.QueryableDatasource = queryableDatasource;
                            queryableDatasource.isRESTDatasource = datasourceBean.RESTDatasource;
                            queryableDatasource.isWebServiceDatasource = datasourceBean.WebServiceDatasource;
                            queryableDatasource.DataUrl = datasourceBean.DataUrl;
                            if (datasourceBean.WsdlUrl != null)
                            {
                                queryableDatasource.WSDLUrl = datasourceBean.WsdlUrl;
                            }
                        }
                    }

                    if (currentRegistration.ProvisionAgreementRef != null)
                    {
                        WriteProvisionAgreementRef(currentRegistration.ProvisionAgreementRef, resultType);
                    }
                }
            }

            return responseType;
        }

        /// <summary>
        ///     Write provision agreement ref.
        /// </summary>
        /// <param name="provRefBean">
        ///     The Provision Agreement ref bean.
        /// </param>
        /// <param name="resultType">
        ///     The result type.
        /// </param>
        private static void WriteProvisionAgreementRef(IStructureReference provRefBean, ResultType resultType)
        {
            var provRef = new ProvisionAgreementRefType();
            resultType.ProvisionAgreementRef = provRef;

            if (provRefBean.TargetUrn != null)
            {
                provRef.URN = provRefBean.TargetUrn;
            }
        }

        // PRIVATE CONSTRUCTOR
    }
}