// -----------------------------------------------------------------------
// <copyright file="SubmitProvisionResponseBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The submit provision response builder v 2.
    /// </summary>
    public class SubmitProvisionResponseBuilderV2 : AbstractResponseBuilder
    {
        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly SubmitProvisionResponseBuilderV2 _instance = new SubmitProvisionResponseBuilderV2();

        /// <summary>
        ///     Prevents a default instance of the <see cref="SubmitProvisionResponseBuilderV2" /> class from being created.
        /// </summary>
        private SubmitProvisionResponseBuilderV2()
        {
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static SubmitProvisionResponseBuilderV2 Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     The build error response.
        /// </summary>
        /// <param name="exception">
        ///     The exception.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        public RegistryInterface BuildErrorResponse(Exception exception)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new SubmitProvisioningResponseType();
            regInterface.SubmitProvisioningResponse = returnType;
            V2Helper.SetHeader(regInterface);
            var statusType = new ProvisioningStatusType();
            returnType.ProvisioningStatus.Add(statusType);
            var statusMessage = new StatusMessageType();
            statusType.StatusMessage = statusMessage;
            this.AddStatus(statusMessage, exception);
            return responseType;
        }

        /// <summary>
        ///     The build response.
        /// </summary>
        /// <param name="response">
        ///     The response.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="response"/> is <see langword="null" />.</exception>
        public RegistryInterface BuildResponse(ICollection<IProvisionAgreementObject> response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new SubmitProvisioningResponseType();
            regInterface.SubmitProvisioningResponse = returnType;
            V2Helper.SetHeader(regInterface);

            foreach (IProvisionAgreementObject provisionAgreement in response)
            {
                this.ProcessResponse(returnType, provisionAgreement);
            }

            return responseType;
        }

        /// <summary>
        ///     The process response.
        /// </summary>
        /// <param name="returnType">
        ///     The return type.
        /// </param>
        /// <param name="provisionAgreement">
        ///     The provision agreement.
        /// </param>
        private void ProcessResponse(
            SubmitProvisioningResponseType returnType, 
            IProvisionAgreementObject provisionAgreement)
        {
            var statusType = new ProvisioningStatusType();
            returnType.ProvisioningStatus.Add(statusType);
            var statusMessage = new StatusMessageType();
            statusType.StatusMessage = statusMessage;
            this.AddStatus(statusMessage, null);

            var provRefType = new ProvisionAgreementRefType();
            statusType.ProvisionAgreementRef = provRefType;

            if (ObjectUtil.ValidString(provisionAgreement.Urn))
            {
                provRefType.URN = provisionAgreement.Urn;
            }

            if (provisionAgreement.DataproviderRef != null)
            {
                ICrossReference crossRef = provisionAgreement.DataproviderRef;
                IMaintainableRefObject maintRef = crossRef.MaintainableReference;
                string value = maintRef.AgencyId;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    provRefType.OrganisationSchemeAgencyID = maintRef.AgencyId;
                }

                string value1 = maintRef.MaintainableId;
                if (!string.IsNullOrWhiteSpace(value1))
                {
                    provRefType.OrganisationSchemeID = maintRef.MaintainableId;
                }

                string value2 = crossRef.ChildReference.Id;
                if (crossRef.ChildReference != null && !string.IsNullOrWhiteSpace(value2))
                {
                    provRefType.DataProviderID = crossRef.ChildReference.Id;
                }

                string value3 = maintRef.Version;
                if (!string.IsNullOrWhiteSpace(value3))
                {
                    provRefType.DataProviderVersion = maintRef.Version;
                }
            }

            if (provisionAgreement.StructureUseage != null)
            {
                ICrossReference structUseageCrossRef = provisionAgreement.StructureUseage;
                IMaintainableRefObject maintRef0 = structUseageCrossRef.MaintainableReference;
                if (structUseageCrossRef.TargetReference.EnumType == SdmxStructureEnumType.Dataflow)
                {
                    string value = maintRef0.AgencyId;
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        provRefType.DataflowAgencyID = maintRef0.AgencyId;
                    }

                    string value1 = maintRef0.MaintainableId;
                    if (!string.IsNullOrWhiteSpace(value1))
                    {
                        provRefType.DataflowID = maintRef0.MaintainableId;
                    }

                    string value2 = maintRef0.Version;
                    if (!string.IsNullOrWhiteSpace(value2))
                    {
                        provRefType.DataflowVersion = maintRef0.Version;
                    }
                }
            }
        }

        // PRIVATE CONSTRUCTOR
    }
}