// -----------------------------------------------------------------------
// <copyright file="SubmitRegistrationResponseBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    /// <summary>
    ///     The submit registration response builder v 2.
    /// </summary>
    public class SubmitRegistrationResponseBuilderV2 : AbstractResponseBuilder
    {
        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly SubmitRegistrationResponseBuilderV2 _instance =
            new SubmitRegistrationResponseBuilderV2();

        /// <summary>
        ///     Prevents a default instance of the <see cref="SubmitRegistrationResponseBuilderV2" /> class from being created.
        /// </summary>
        private SubmitRegistrationResponseBuilderV2()
        {
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static SubmitRegistrationResponseBuilderV2 Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Build error response.
        /// </summary>
        /// <param name="exception">
        ///     The exception.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        public RegistryInterface BuildErrorResponse(Exception exception)
        {
            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            V2Helper.SetHeader(regInterface);
            var returnType = new SubmitRegistrationResponseType();
            regInterface.SubmitRegistrationResponse = returnType;
            var registrationStatusType = new RegistrationStatusType();
            returnType.RegistrationStatus.Add(registrationStatusType);
            registrationStatusType.StatusMessage = new StatusMessageType();

            this.AddStatus(registrationStatusType.StatusMessage, exception);
            return responseType;
        }

        /// <summary>
        ///     The build response.
        /// </summary>
        /// <param name="response">
        ///     The response.
        /// </param>
        /// <returns>
        ///     The <see cref="RegistryInterface" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="response"/> is <see langword="null" />.</exception>
        public RegistryInterface BuildResponse(IDictionary<IRegistrationObject, Exception> response)
        {
            if (response == null)
            {
                throw new ArgumentNullException("response");
            }

            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new SubmitRegistrationResponseType();
            regInterface.SubmitRegistrationResponse = returnType;
            V2Helper.SetHeader(regInterface);

            /* foreach */
            foreach (KeyValuePair<IRegistrationObject, Exception> registration in response)
            {
                this.ProcessResponse(returnType, registration.Key, registration.Value);
            }

            return responseType;
        }

        /// <summary>
        ///     The process response.
        /// </summary>
        /// <param name="returnType">
        ///     The return type.
        /// </param>
        /// <param name="registration">
        ///     The registration.
        /// </param>
        /// <param name="exception">
        ///     The exception.
        /// </param>
        private void ProcessResponse(
            SubmitRegistrationResponseType returnType, 
            IRegistrationObject registration, 
            Exception exception)
        {
            var registrationStatusType = new RegistrationStatusType();
            returnType.RegistrationStatus.Add(registrationStatusType);
            registrationStatusType.StatusMessage = new StatusMessageType();
            this.AddStatus(registrationStatusType.StatusMessage, exception);
            if (registration.DataSource != null)
            {
                var datasourceType = new DatasourceType();
                registrationStatusType.Datasource = datasourceType;
                this.AddDatasource(registration.DataSource, datasourceType);
            }

            if (registration.ProvisionAgreementRef != null)
            {
                ICrossReference provRef = registration.ProvisionAgreementRef;
                var provRefType = new ProvisionAgreementRefType();
                registrationStatusType.ProvisionAgreementRef = provRefType;
                if (provRef.TargetUrn != null)
                {
                    provRefType.URN = provRef.TargetUrn;
                }
            }
        }

        // PRIVATE CONSTRUCTOR
    }
}