// -----------------------------------------------------------------------
// <copyright file="QueryStructureResponseBuilderV2.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Registry;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Util;

    using TextType = Org.Sdmx.Resources.SdmxMl.Schemas.V20.Common.TextType;

    /// <summary>
    ///     The query structure response builder v 2.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling",
        Justification = "It is OK. We need to handle a big number of SDMX v2.0 artefacts.")]
    public class QueryStructureResponseBuilderV2 : AbstractResponseBuilder
    {
        /// <summary>
        /// The _structure XML builder
        /// </summary>
        private readonly StructureXmlBuilder _structureXmlBuilder;

        /// <summary>
        /// The <c>StructureHeader</c> XML Builder
        /// </summary>
        private readonly IBuilder<HeaderType, IHeader> _headerXmlsBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryStructureResponseBuilderV2" /> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        /// <param name="structureXmlBuilder">The structure XML builder.</param>
        /// <exception cref="System.ArgumentNullException">
        /// builderFactory
        /// or
        /// structureXmlBuilder
        /// </exception>
        public QueryStructureResponseBuilderV2(IBuilderFactory builderFactory, StructureXmlBuilder structureXmlBuilder)
        {
            if (builderFactory == null)
            {
                throw new ArgumentNullException("builderFactory");
            }

            if (structureXmlBuilder == null)
            {
                throw new ArgumentNullException("structureXmlBuilder");
            }

            this._structureXmlBuilder = structureXmlBuilder;

            this._headerXmlsBuilder = builderFactory.GetBuilder<HeaderType, IHeader>();
        }

        /// <summary>
        /// The build error response.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="exception"/> is <see langword="null"/>.
        /// </exception>
        public RegistryInterface BuildErrorResponse(Exception exception)
        {
            if (exception == null)
            {
                throw new ArgumentNullException("exception");
            }

            var responseType = new RegistryInterface();
            RegistryInterfaceType regInterface = responseType.Content;
            var returnType = new QueryStructureResponseType();
            regInterface.QueryStructureResponse = returnType;
            V2Helper.SetHeader(regInterface);

            var statusMessage = new StatusMessageType();
            returnType.StatusMessage = statusMessage;

            statusMessage.status = StatusTypeConstants.Failure;

            var tt = new TextType();
            statusMessage.MessageText.Add(tt);

            var uncheckedException = exception as SdmxException;
            tt.TypedValue = uncheckedException != null ? uncheckedException.FullMessage : exception.Message;

            return responseType;
        }

        /// <summary>
        /// The build success response.
        /// </summary>
        /// <param name="beans">
        /// The beans.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">
        /// Unsupported artefact.
        /// </exception>
        public RegistryInterface BuildSuccessResponse(ISdmxObjects beans)
        {
            return this.BuildSuccessResponse(beans, null);
        }

        /// <summary>
        /// Build success response.
        /// </summary>
        /// <param name="buildFrom">
        /// The source sdmx objects.
        /// </param>
        /// <param name="warningMessage">
        /// The warning message.
        /// </param>
        /// <returns>
        /// The <see cref="RegistryInterface"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="buildFrom"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="SdmxNotImplementedException">
        /// Unsupported artefact.
        /// </exception>
        public RegistryInterface BuildSuccessResponse(ISdmxObjects buildFrom, string warningMessage)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            // PLEASE NOTE. The code here is slightly different than in Java.
            // That is because of the differences between Java XmlBeans and .NET Linq2Xsd generated classes.
            // Please consult GIT log before making any changes.
            var responseType = new RegistryInterface();

            RegistryInterfaceType regInterface = responseType.Content;
            HeaderType headerType;

            if (buildFrom.Header != null)
            {
                headerType = this._headerXmlsBuilder.Build(buildFrom.Header);
                regInterface.Header = headerType;
            }
            else
            {
                headerType = new HeaderType();
                regInterface.Header = headerType;
                V2Helper.SetHeader(headerType, buildFrom);
            }

            var returnType = new QueryStructureResponseType();
            regInterface.QueryStructureResponse = returnType;

            var statusMessage = new StatusMessageType();
            returnType.StatusMessage = statusMessage;

            if (!string.IsNullOrWhiteSpace(warningMessage)
                || !ObjectUtil.ValidCollection(buildFrom.GetAllMaintainables()))
            {
                statusMessage.status = StatusTypeConstants.Warning;
                var tt = new TextType();
                statusMessage.MessageText.Add(tt);
                tt.TypedValue = !string.IsNullOrWhiteSpace(warningMessage)
                                    ? warningMessage
                                    : "No Structures Match The Query Parameters";
            }
            else
            {
                statusMessage.status = StatusTypeConstants.Success;
            }

            this.BuildSupportedObjects(buildFrom, returnType);

            ISet<IAttachmentConstraintObject> attachmentConstraintObjects = buildFrom.AttachmentConstraints;
            if (attachmentConstraintObjects.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Attachment Constraint at SMDX v2.0 - please use SDMX v2.1");
            }

            ISet<IContentConstraintObject> contentConstraintObjects = buildFrom.ContentConstraintObjects;
            if (contentConstraintObjects.Count > 0)
            {
                throw new SdmxNotImplementedException(ExceptionCode.Unsupported, "Content Constraint at SMDX v2.0 - please use SDMX v2.1");
            }

            return responseType;
        }

        /// <summary>
        /// Builds the supported objects.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="returnType">Type of the return.</param>
        private void BuildSupportedObjects(ISdmxObjects buildFrom, QueryStructureResponseType returnType)
        {
            // TOP LEVEL STRUCTURES ELEMENT
            ISet<ICategorisationObject> categorisations = buildFrom.Categorisations;

            // GET CATEGORY SCHEMES
            var categorySchemeObjects = buildFrom.CategorySchemes;
            returnType.CategorySchemes = this._structureXmlBuilder.Build(categorySchemeObjects, categorisations);

            // GET CODELISTS
            var codelistObjects = buildFrom.Codelists;
            returnType.CodeLists = this._structureXmlBuilder.Build(codelistObjects);

            // CONCEPT SCHEMES
            ISet<IConceptSchemeObject> conceptSchemeObjects = buildFrom.ConceptSchemes;
            returnType.Concepts = this._structureXmlBuilder.Build(conceptSchemeObjects);

            // DATAFLOWS
            ISet<IDataflowObject> dataflowObjects = buildFrom.Dataflows;
            returnType.Dataflows = this._structureXmlBuilder.Build(dataflowObjects, categorisations);

            // HIERARCIC CODELIST
            ISet<IHierarchicalCodelistObject> hierarchicalCodelistObjects = buildFrom.HierarchicalCodelists;
            returnType.HierarchicalCodelists = this._structureXmlBuilder.Build(hierarchicalCodelistObjects);

            // KEY FAMILY
            ISet<IDataStructureObject> dataStructureObjects = buildFrom.DataStructures;
            returnType.KeyFamilies = this._structureXmlBuilder.Build(dataStructureObjects);

            // METADATA FLOW
            ISet<IMetadataFlow> metadataFlows = buildFrom.Metadataflows;
            returnType.Metadataflows = this._structureXmlBuilder.Build(metadataFlows, categorisations);

            // METADATA STRUCTURE
            ISet<IMetadataStructureDefinitionObject> metadataStructureDefinitionObjects = buildFrom.MetadataStructures;
            returnType.MetadataStructureDefinitions = this._structureXmlBuilder.Build(metadataStructureDefinitionObjects);

            // ORGAISATION SCHEME
            returnType.OrganisationSchemes = this._structureXmlBuilder.BuildOrganisationSchemes(buildFrom);

            // PROCESSES
            ISet<IProcessObject> processObjects = buildFrom.Processes;
            returnType.Processes = this._structureXmlBuilder.Build(processObjects);

            // STRUCTURE SETS
            ISet<IStructureSetObject> structureSetObjects = buildFrom.StructureSets;
            returnType.StructureSets = this._structureXmlBuilder.Build(structureSetObjects);

            // REPORTING TAXONOMIES
            ISet<IReportingTaxonomyObject> reportingTaxonomyObjects = buildFrom.ReportingTaxonomys;
            returnType.ReportingTaxonomies = this._structureXmlBuilder.Build(reportingTaxonomyObjects);
        }
    }
}
