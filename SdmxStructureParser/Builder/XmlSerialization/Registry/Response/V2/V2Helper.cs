// -----------------------------------------------------------------------
// <copyright file="V2Helper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2
{
    using System;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Message;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;

    /// <summary>
    ///     The v 2 helper.
    /// </summary>
    public static class V2Helper
    {
        /// <summary>
        ///     The ref.
        /// </summary>
        private static int refNumber = 1;

        /// <summary>
        /// Sets the header.
        /// </summary>
        /// <param name="value">The value.</param>
        public static void SetHeader(RegistryInterfaceType value)
        {
            SetHeader(value, null);
        }

        /// <summary>
        ///     Sets the header.
        /// </summary>
        /// <param name="regInterface">
        ///     The registry interface.
        /// </param>
        /// <param name="sdmxObjects">
        ///     The beans.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="regInterface"/> is <see langword="null" />.</exception>
        public static void SetHeader(RegistryInterfaceType regInterface, ISdmxObjects sdmxObjects)
        {
            if (regInterface == null)
            {
                throw new ArgumentNullException("regInterface");
            }

            var header = new HeaderType();
            regInterface.Header = header;
            SetHeader(header, sdmxObjects);
        }

        /// <summary>
        ///     Sets the header.
        /// </summary>
        /// <param name="header">
        ///     The header.
        /// </param>
        /// <param name="sdmxObjects">
        ///     The beans.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="header"/> is <see langword="null" />.</exception>
        public static void SetHeader(HeaderType header, ISdmxObjects sdmxObjects)
        {
            if (header == null)
            {
                throw new ArgumentNullException("header");
            }

            header.ID = "IDREF" + refNumber;
            refNumber++;
            header.Test = false;
            header.Prepared = DateTime.Now;
            var sender = new PartyType();
            header.Sender.Add(sender);

            string senderId;
            if (sdmxObjects != null && sdmxObjects.Header != null && sdmxObjects.Header.Sender != null)
            {
                // Get header information from the supplied beans
                senderId = sdmxObjects.Header.Sender.Id;
            }
            else
            {
                // Get header info from HeaderHelper
                senderId = HeaderHelper.Instance.SenderId;
            }

            sender.id = senderId;

            var receiver = new PartyType();
            header.Receiver.Add(receiver);
            receiver.id = HeaderHelper.Instance.ReceiverId;
        }
    }
}