// -----------------------------------------------------------------------
// <copyright file="ISubmitProvisionResponseBuilder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;

    using Xml.Schema.Linq;

    /// <summary>
    ///     A class supporting this interface can build error and success responses for submission of provisions.
    /// </summary>
    public interface ISubmitProvisionResponseBuilder
    {
        /// <summary>
        ///     Build error response for submission of provisions..
        /// </summary>
        /// <param name="exception">
        ///     The exception.
        /// </param>
        /// <param name="structureReference">
        ///     The structure Reference.
        /// </param>
        /// <param name="schemaVersion">
        ///     The schema version.
        /// </param>
        /// <returns>
        ///     The error response for submission of provisions
        /// </returns>
        XTypedElement BuildErrorResponse(
            Exception exception, 
            IStructureReference structureReference, 
            SdmxSchemaEnumType schemaVersion);

        /// <summary>
        ///     Returns an response based on the submitted provision, if there is a Exception against the provision
        ///     then the error will be documented, and a status of failure will be put against it.
        /// </summary>
        /// <param name="response">
        ///     - a map of provision, and a error message (if there is one)
        /// </param>
        /// <param name="schemaVersion">
        ///     - the version of the schema to output the response in
        /// </param>
        /// <returns>
        ///     The <see cref="XTypedElement" />.
        /// </returns>
        XTypedElement BuildSuccessResponse(
            ICollection<IProvisionAgreementObject> response, 
            SdmxSchemaEnumType schemaVersion);
    }
}