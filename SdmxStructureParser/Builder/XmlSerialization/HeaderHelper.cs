// -----------------------------------------------------------------------
// <copyright file="HeaderHelper.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization
{
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The header helper.
    /// </summary>
    public class HeaderHelper
    {
        /// <summary>
        ///     The Unknown id.
        /// </summary>
        private const string UnknownId = "Unknown";

        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly HeaderHelper _instance = new HeaderHelper();

        /// <summary>
        ///     The header manager. TODO
        /// </summary>
        private readonly IHeaderRetrievalManager _headerManager = null;

        /// <summary>
        ///     Prevents a default instance of the <see cref="HeaderHelper" /> class from being created.
        /// </summary>
        private HeaderHelper()
        {
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        public static HeaderHelper Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Gets the receiver Id for this message
        /// </summary>
        public string ReceiverId
        {
            get
            {
                if (this._headerManager != null && this._headerManager.Header != null
                    && ObjectUtil.ValidCollection(this._headerManager.Header.Receiver))
                {
                    return this._headerManager.Header.Receiver[0].Id;
                }

                return UnknownId;
            }
        }

        /// <summary>
        ///     Gets the sender Id for this message
        /// </summary>
        public string SenderId
        {
            get
            {
                if (this._headerManager != null && this._headerManager.Header != null)
                {
                    string retVal = this._headerManager.Header.Sender.Id;
                    if (retVal != null)
                    {
                        return retVal;
                    }
                }

                return UnknownId;
            }
        }
    }
}