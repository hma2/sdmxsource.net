// -----------------------------------------------------------------------
// <copyright file="AbstractSdmxObjectsV2Builder.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Runtime.CompilerServices;

    using Org.Sdmx.Resources.SdmxMl.Schemas.V20.Structure;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Mapping;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The abstract sdmx sdmxObjects v 2 builder.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", 
        Justification = "It is OK. It tries to handle all SDMX v2.0 artefacts. Splitting into different classes would make sync to Java more difficult.")]
    public abstract class AbstractSdmxObjectsV2Builder : AbstractSdmxObjectsBuilder
    {
        /// <summary>
        /// The _concept role builder
        /// </summary>
        private IBuilder<IStructureReference, ComponentRole> _conceptRoleBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractSdmxObjectsV2Builder"/> class.
        /// </summary>
        /// <param name="conceptRoleBuilder">The concept role builder.</param>
        protected AbstractSdmxObjectsV2Builder(IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
        {
            this._conceptRoleBuilder = conceptRoleBuilder;
        }

        /// <summary>
        ///     Create Categorisations from Version 2.0 categories, adds the categorisations to the sdmxObjects container
        /// </summary>
        /// <param name="beans">
        ///     container to add to
        /// </param>
        /// <param name="categoryTypes">
        ///     category types to filter on
        /// </param>
        /// <param name="categoryBeans">
        ///     category sdmxObjects to process
        /// </param>
        protected internal void ProcessCategory(ISdmxObjects beans, IList<CategoryType> categoryTypes, IList<ICategoryObject> categoryBeans)
        {
            if (categoryTypes == null)
            {
                return;
            }

            // Note converted from recursive in Java 0.9.4 to iterative
            var stack = new Stack<KeyValuePair<IList<CategoryType>, IList<ICategoryObject>>>();
            stack.Push(new KeyValuePair<IList<CategoryType>, IList<ICategoryObject>>(categoryTypes, categoryBeans));
            while (stack.Count > 0)
            {
                KeyValuePair<IList<CategoryType>, IList<ICategoryObject>> pair = stack.Pop();
                IList<CategoryType> currentTypes = pair.Key;
                foreach (CategoryType cat in currentTypes)
                {
                    ICategoryObject processingCatBean = null;

                    IList<ICategoryObject> currentObjects = pair.Value;
                    foreach (ICategoryObject currentCatBean in currentObjects)
                    {
                        if (currentCatBean.Id.Equals(cat.id))
                        {
                            processingCatBean = currentCatBean;
                            break;
                        }
                    }

                    // TODO this check doesn't exist in Java 0.9.4
                    if (processingCatBean != null)
                    {
                        stack.Push(new KeyValuePair<IList<CategoryType>, IList<ICategoryObject>>(cat.Category, processingCatBean.Items));
                        this.ProcessCategory(beans, cat, processingCatBean);
                    }
                }
            }
        }

        /// <summary>
        ///     Create Categorisations from Version 2.0 category, adds the categorisation to the sdmxObjects container
        /// </summary>
        /// <param name="beans">
        ///     container to add to
        /// </param>
        /// <param name="categoryType">
        ///     The category Type.
        /// </param>
        /// <param name="categoryBean">
        ///     The category Bean.
        /// </param>
        protected internal void ProcessCategory(ISdmxObjects beans, CategoryType categoryType, ICategoryObject categoryBean)
        {
            if (beans == null)
            {
                throw new ArgumentNullException("beans");
            }

            if (categoryType == null)
            {
                throw new ArgumentNullException("categoryType");
            }

            if (categoryType.DataflowRef != null)
            {
                foreach (DataflowRefType dataflowRefType in categoryType.DataflowRef)
                {
                    HandleDataflowCategorisation(beans, categoryBean, dataflowRefType);
                }
            }

            if (categoryType.MetadataflowRef != null)
            {
                foreach (var mdfRef in categoryType.MetadataflowRef)
                {
                    beans.AddCategorisation(new CategorisationObjectCore(categoryBean, mdfRef));
                }
            }
        }

        /// <summary>
        ///     Creates category schemes and categorisations based on the input category schemes
        /// </summary>
        /// <param name="catSchemes">
        ///     - if null will not add anything to the sdmxObjects container
        /// </param>
        /// <param name="beans">
        ///     - to add category schemes and categorisations to
        /// </param>
        protected internal void ProcessCategorySchemes(CategorySchemesType catSchemes, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (catSchemes != null && ObjectUtil.ValidCollection(catSchemes.CategoryScheme))
            {
                foreach (CategorySchemeType currentType in catSchemes.CategoryScheme)
                {
                    ICategorySchemeObject categorySchemeObject = new CategorySchemeObjectCore(currentType);
                    this.AddIfNotDuplicateURN(beans, urns, categorySchemeObject);
                    if (currentType.Category != null)
                    {
                        this.ProcessCategory(beans, currentType.Category, categorySchemeObject.Items);
                    }
                }
            }
        }

        /// <summary>
        ///     Creates dataflow and categorisations based on the input dataflow
        /// </summary>
        /// <param name="dataflowsType">
        ///     - if null will not add anything to the sdmxObjects container
        /// </param>
        /// <param name="beans">
        ///     - to add dataflow and categorisations to
        /// </param>
        protected internal void ProcessDataflows(DataflowsType dataflowsType, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (dataflowsType != null && ObjectUtil.ValidCollection(dataflowsType.Dataflow))
            {
                foreach (DataflowType currentType in dataflowsType.Dataflow)
                {
                    IDataflowObject currentDataflow = new DataflowObjectCore(currentType);
                    this.AddIfNotDuplicateURN(beans, urns, currentDataflow);

                    // CATEGORISATIONS FROM DATAFLOWS
                    ProcessDataflowCategorisations(beans, currentType, currentDataflow);
                }
            }
        }

        /// <summary>
        ///     Creates meta-dataflow and categorisations based on the input meta-dataflow
        /// </summary>
        /// <param name="metadataflowsType">
        ///     - if null will not add anything to the sdmxObjects container
        /// </param>
        /// <param name="beans">
        ///     - to add meta-dataflow and categorisations to
        /// </param>
        protected internal void ProcessMetadataFlows(MetadataflowsType metadataflowsType, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (metadataflowsType != null && ObjectUtil.ValidCollection(metadataflowsType.Metadataflow))
            {
                foreach (MetadataflowType currentType in metadataflowsType.Metadataflow)
                {
                    IMetadataFlow currentMetadataflow = new MetadataflowObjectCore(currentType);

                    this.AddIfNotDuplicateURN(beans, urns, currentMetadataflow);

                    // CATEGORISATIONS FROM METADATAFLOWS
                    if (currentType.CategoryRef != null)
                    {
                        foreach (CategoryRefType cateogryRefType in currentType.CategoryRef)
                        {
                            beans.AddCategorisation(new CategorisationObjectCore(currentMetadataflow, cateogryRefType));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Process the  metadata structure definitions.
        /// </summary>
        /// <param name="metadataStructureDefinitionsType">The metadata structure definitions type.</param>
        /// <param name="beans">The beans.</param>
        /// <param name="urns">The urns.</param>
        protected internal void ProcessMetadataStructureDefinitions(MetadataStructureDefinitionsType metadataStructureDefinitionsType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (metadataStructureDefinitionsType != null && metadataStructureDefinitionsType.MetadataStructureDefinition != null)
            {
                foreach (MetadataStructureDefinitionType currentType in
                    metadataStructureDefinitionsType.MetadataStructureDefinition)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new MetadataStructureDefinitionObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Process the codelist
        /// </summary>
        /// <param name="structures">
        ///     The structures.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessCodelists(CodeListsType structures, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (structures != null && structures.CodeList != null)
            {
                foreach (CodeListType currentType in structures.CodeList)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new CodelistObjectCore(currentType));
                }
            }
        }

        /// <summary>
        ///     Process the  concept schemes.
        /// </summary>
        /// <param name="structures">
        ///     The structures.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessConceptSchemes(ConceptsType structures, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (structures == null || structures.ConceptScheme == null)
            {
                return;
            }

            foreach (ConceptSchemeType currentType0 in structures.ConceptScheme)
            {
                this.AddIfNotDuplicateURN(beans, urns, new ConceptSchemeObjectCore(currentType0));
            }

            // CONCEPTS
            IDictionary<string, IList<ConceptType>> conceptAgencyMap = new Dictionary<string, IList<ConceptType>>(StringComparer.Ordinal);
            {
                foreach (ConceptType currentType2 in structures.Concept)
                {
                    IList<ConceptType> concepts;
                    if (!conceptAgencyMap.TryGetValue(currentType2.agencyID, out concepts))
                    {
                        concepts = new List<ConceptType>();
                        conceptAgencyMap.Add(currentType2.agencyID, concepts);
                    }

                    concepts.Add(currentType2);
                }
            }

            foreach (KeyValuePair<string, IList<ConceptType>> currentConceptAgency in conceptAgencyMap)
            {
                this.AddIfNotDuplicateURN(beans, urns, new ConceptSchemeObjectCore(currentConceptAgency.Value, currentConceptAgency.Key));
            }
        }

        /// <summary>
        ///     Process the hierarchical codelist.
        /// </summary>
        /// <param name="hierarchicalCodelistsType">
        ///     The hierarchical codelist type.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessHierarchicalCodelists(HierarchicalCodelistsType hierarchicalCodelistsType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (hierarchicalCodelistsType != null && hierarchicalCodelistsType.HierarchicalCodelist != null)
            {
                foreach (HierarchicalCodelistType currentType4 in hierarchicalCodelistsType.HierarchicalCodelist)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new HierarchicalCodelistObjectCore(currentType4));
                }
            }
        }

        /// <summary>
        ///     Process the  key families.
        /// </summary>
        /// <param name="keyFamiliesType">
        ///     The key families type.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessKeyFamilies(KeyFamiliesType keyFamiliesType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (keyFamiliesType != null && keyFamiliesType.KeyFamily != null)
            {
                foreach (KeyFamilyType currentType in keyFamiliesType.KeyFamily)
                {
                    //// Reverted changes that were made during sync
                    //// Please do not change unless you are sure.
                    if (CrossSectionalUtil.IsCrossSectional(currentType))
                    {
                        var mutable = BuildCrossSectionalDataStructure(currentType, this._conceptRoleBuilder);

                        this.AddIfNotDuplicateURN(beans, urns, mutable.ImmutableInstance);
                    }
                    else
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new DataStructureObjectCore(currentType, this._conceptRoleBuilder));
                    }
                }
            }
        }

        /// <summary>
        ///     Process the  organisation schemes.
        /// </summary>
        /// <param name="organisationSchemesType">
        ///     The organisation schemes type.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        /// <exception cref="SdmxSemmanticException">Invalid Organisation Scheme.</exception>
        protected void ProcessOrganisationSchemes(OrganisationSchemesType organisationSchemesType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (organisationSchemesType != null)
            {
                foreach (OrganisationSchemeType currentType10 in organisationSchemesType.OrganisationScheme)
                {
                    if (ObjectUtil.ValidCollection(currentType10.Agencies))
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new AgencySchemeCore(currentType10));
                    }

                    if (ObjectUtil.ValidCollection(currentType10.DataConsumers))
                    {
                        this.AddIfNotDuplicateURN(beans, urns, new DataConsumerSchemeCore(currentType10));
                    }

                    if (ObjectUtil.ValidCollection(currentType10.DataProviders))
                    {
                            this.AddIfNotDuplicateURN(beans, urns, new DataProviderSchemeCore(currentType10));
                    }

                    // If the organisation scheme contains no elements, then this is an error
                    if (currentType10.Agencies.Count == 0 && currentType10.DataConsumers.Count == 0 && currentType10.DataProviders.Count == 0)
                    {
                        throw new SdmxSemmanticException(ExceptionCode.StructureInvalidOrganisationSchemeNoContent, currentType10.agencyID, currentType10.id);
                    }
                }
            }
        }

        /// <summary>
        ///     Process the processes.
        /// </summary>
        /// <param name="processesType">
        ///     The processes type.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessProcesses(ProcessesType processesType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (processesType != null && processesType.Process != null)
            {
                foreach (ProcessType currentType14 in processesType.Process)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ProcessObjectCore(currentType14));
                }
            }
        }

        /// <summary>
        ///     Process the  reporting taxonomies.
        /// </summary>
        /// <param name="reportingTaxonomiesType">
        ///     The reporting taxonomies type.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessReportingTaxonomies(ReportingTaxonomiesType reportingTaxonomiesType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (reportingTaxonomiesType != null && reportingTaxonomiesType.ReportingTaxonomy != null)
            {
                foreach (ReportingTaxonomyType currentType16 in reportingTaxonomiesType.ReportingTaxonomy)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new ReportingTaxonomyObjectCore(currentType16));
                }
            }
        }

        /// <summary>
        ///     Process the  structure sets.
        /// </summary>
        /// <param name="structureSetsType">
        ///     The structure sets type.
        /// </param>
        /// <param name="beans">
        ///     The sdmxObjects.
        /// </param>
        /// <param name="urns">
        ///     The URN.
        /// </param>
        /// <exception cref="MaintainableObjectException">
        ///     Duplicate URN
        /// </exception>
        protected void ProcessStructureSets(StructureSetsType structureSetsType, ISdmxObjects beans, ISet<Uri> urns)
        {
            if (structureSetsType != null && structureSetsType.StructureSet != null)
            {
                foreach (StructureSetType currentType18 in structureSetsType.StructureSet)
                {
                    this.AddIfNotDuplicateURN(beans, urns, new StructureSetObjectCore(currentType18));
                }
            }
        }

        /// <summary>
        /// Builds the cross sectional data structure.
        /// </summary>
        /// <param name="currentType">Type of the current.</param>
        /// <param name="conceptRoleBuilder">The concept Role Builder.</param>
        /// <returns>
        /// The <see cref="ICrossSectionalDataStructureMutableObject" />
        /// </returns>
        private static ICrossSectionalDataStructureMutableObject BuildCrossSectionalDataStructure(KeyFamilyType currentType, IBuilder<IStructureReference, ComponentRole> conceptRoleBuilder)
        {
            ICrossSectionalDataStructureObject xsdBean = new CrossSectionalDataStructureObjectCore(currentType, conceptRoleBuilder);
            ICrossSectionalDataStructureMutableObject mutable = xsdBean.MutableInstance;
            if (currentType.Components.CrossSectionalMeasure.Count > 0)
            {
                // Set the measure dimensions references
                ICrossSectionalMeasureMutableObject xsMutable = mutable.CrossSectionalMeasures[0];
                IStructureReference conceptRef = xsMutable.ConceptRef;

                IDictionary<string, IStructureReference> mapping = mutable.MeasureDimensionCodelistMapping;
                IStructureReference cocneptSchemeRef = new StructureReferenceImpl(conceptRef.MaintainableReference, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.ConceptScheme));

                foreach (IDimensionMutableObject dim in mutable.Dimensions)
                {
                    if (dim.MeasureDimension)
                    {
                        if (!mapping.ContainsKey(dim.Id))
                        {
                            mapping.Add(dim.Id, dim.Representation.Representation);
                        }

                        dim.Representation.Representation = cocneptSchemeRef;
                    }
                }
            }

            return mutable;
        }

        /// <summary>
        /// Handles the dataflow categorisation.
        /// </summary>
        /// <param name="beans">The beans.</param>
        /// <param name="categoryBean">The category bean.</param>
        /// <param name="dataflowRefType">Type of the dataflow reference.</param>
        private static void HandleDataflowCategorisation(ISdmxObjects beans, ICategoryObject categoryBean, DataflowRefType dataflowRefType)
        {
            // use mutable for now until the following issue is fixed. 
            // http://www.metadatatechnology.com/mantis/view.php?id=1341
            ICategorisationMutableObject mutable = new CategorisationMutableCore();
            mutable.AgencyId = categoryBean.MaintainableParent.AgencyId;

            mutable.CategoryReference = categoryBean.AsReference;

            // TODO create specialized collections for TextTypeWrapperMutable and TextTypeWrapper 
            foreach (ITextTypeWrapper name in categoryBean.Names)
            {
                mutable.Names.Add(new TextTypeWrapperMutableCore(name));
            }

            mutable.StructureReference = dataflowRefType.URN != null
                                             ? new StructureReferenceImpl(dataflowRefType.URN)
                                             : new StructureReferenceImpl(dataflowRefType.AgencyID, dataflowRefType.DataflowID, dataflowRefType.Version, SdmxStructureEnumType.Dataflow);
            mutable.Id = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", mutable.CategoryReference.GetHashCode(), mutable.StructureReference.GetHashCode());

            // TODO use MT fix in java when is done. Mantis ticket:
            // http://www.metadatatechnology.com/mantis/view.php?id=1341
            // sdmxObjects.AddCategorisation(new CategorisationObjectCore(categoryBean, dataflowRefType));
            beans.AddCategorisation(new CategorisationObjectCore(mutable));
        }

        /// <summary>
        /// Processes the dataflow categorisations.
        /// </summary>
        /// <param name="beans">The beans.</param>
        /// <param name="currentType">Type of the current.</param>
        /// <param name="currentDataflow">The current dataflow.</param>
        private static void ProcessDataflowCategorisations(ISdmxObjects beans, DataflowType currentType, IDataflowObject currentDataflow)
        {
            if (currentType.CategoryRef != null)
            {
                foreach (CategoryRefType cateogryRefType in currentType.CategoryRef)
                {
                    // use mutable for now until the following issue is fixed. 
                    // http://www.metadatatechnology.com/mantis/view.php?id=1341
                    ICategorisationMutableObject mutable = new CategorisationMutableCore();
                    mutable.AgencyId = currentDataflow.AgencyId;
                    mutable.CategoryReference = RefUtil.CreateCategoryRef(cateogryRefType);

                    // TODO create specialized collections for TextTypeWrapperMutable and TextTypeWrapper 
                    foreach (ITextTypeWrapper name in currentDataflow.Names)
                    {
                        mutable.Names.Add(new TextTypeWrapperMutableCore(name));
                    }

                    mutable.StructureReference = currentDataflow.AsReference;
                    mutable.Id = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", mutable.CategoryReference.GetHashCode(), mutable.StructureReference.GetHashCode());

                    // TODO use MT fix in java when is done. Mantis ticket:
                    // http://www.metadatatechnology.com/mantis/view.php?id=1341
                    beans.AddCategorisation(new CategorisationObjectCore(mutable));

                    ////sdmxObjects.AddCategorisation(new CategorisationObjectCore(currentDataflow, cateogryRefType));
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        //////////            VERSION 2.0 METHODS FOR STRUCTURES          ///////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////////
    }
}