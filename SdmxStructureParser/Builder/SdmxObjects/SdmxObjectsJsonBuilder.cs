// -----------------------------------------------------------------------
// <copyright file="SdmxObjectsJsonBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Newtonsoft.Json.Linq;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     This is the Parser for Json Structure message.
    ///     For the time being it handles Dataflow, AgencyScheme, CategoryScheme requests
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "It is OK. Big number of SDMX v2.1 artefacts")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Reviewed. They are used in private classes and for de-serialization.")]
    public class SdmxObjectsJsonBuilder : AbstractSdmxObjectsBuilder, IBuilder<ISdmxObjects, JObject>
    {
        /// <summary>
        /// The agency scheme urn.
        /// </summary>
        private const string AgencySchemeUrn = "urn:sdmx:org.sdmx.infomodel.base.AgencyScheme=";

        /// <summary>
        /// The categorisation urn.
        /// </summary>
        private const string CategorisationUrn = "urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=";

        /// <summary>
        /// The category scheme urn.
        /// </summary>
        private const string CategorySchemeUrn = "urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=";

        /// <summary>
        /// The dataflow urn.
        /// </summary>
        private const string DataflowUrn = "urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=";

        /// <summary>
        /// The _sdmx structure json format.
        /// </summary>
        private readonly SdmxStructureJsonFormat _sdmxStructureJsonFormat;

        /// <summary>
        /// The locale.
        /// </summary>
        private readonly string _locale = "en";

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxObjectsJsonBuilder"/> class. 
        /// Main constructor based on a given SdmxStructureJsonFormat
        /// </summary>
        /// <param name="sdmxStructureJsonFormat">
        /// This parameter is used to get the default language and the Type of the Request
        /// </param>
        public SdmxObjectsJsonBuilder(SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            if (sdmxStructureJsonFormat == null)
            {
                throw new ArgumentNullException("sdmxStructureJsonFormat");
            }

            this._sdmxStructureJsonFormat = sdmxStructureJsonFormat;

            this._locale = this._sdmxStructureJsonFormat.Translator.SelectedLanguage.Name;
        }

        /// <summary>
        /// Build beans from Json Structure Document
        /// </summary>
        /// <param name="structuresDoc">
        /// The structures Doc.
        /// </param>
        /// <returns>
        /// The <see cref="ISdmxObjects"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="structuresDoc"/> is <see langword="null"/>.
        /// </exception>
        public ISdmxObjects Build(JObject structuresDoc)
        {
            if (structuresDoc == null)
            {
                throw new ArgumentNullException("structuresDoc");
            }

            JToken headerToken;
            if (structuresDoc.TryGetValue("header", StringComparison.OrdinalIgnoreCase, out headerToken))
            {
                return this.Build(structuresDoc, this.ProcessHeader(headerToken), null);
            }
            else
            {
                return new SdmxObjectsImpl();
            }
        }

        /// <summary>
        /// Build SDMX objects from v2.1 structures
        /// </summary>
        /// <param name="structures">
        /// The structures
        /// </param>
        /// <param name="header">
        /// The header.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <returns>
        /// the container of all beans built
        /// </returns>
        private ISdmxObjects Build(JObject structures, IHeader header, DatasetAction action)
        {
            var beans = new SdmxObjectsImpl(header, action);
            JArray ressources = (JArray)structures["resources"];

            if (ressources != null && ressources.Count > 0)
            {
                foreach (JObject currentType in ressources)
                {
                    switch (this._sdmxStructureJsonFormat.RequestedStructureType)
                    {
                        case SdmxStructureEnumType.Dataflow:
                            {
                                this.ProcessDataflow(currentType, beans);
                                break;
                            }

                        case SdmxStructureEnumType.CategoryScheme:
                            {
                                this.ProcessCategoryScheme(currentType, beans);
                                break;
                            }

                        case SdmxStructureEnumType.AgencyScheme:
                            {
                                this.ProcessAgencyScheme(currentType, beans);
                                break;
                            }
                    }
                }
            }

            foreach (JProperty jp in structures["references"])
            {
                if (jp.Name.StartsWith(AgencySchemeUrn, StringComparison.OrdinalIgnoreCase))
                {
                    this.ProcessAgencyScheme((JObject)jp.Value, beans);
                }

                if (jp.Name.StartsWith(CategorisationUrn, StringComparison.OrdinalIgnoreCase))
                {
                    this.ProcessCategorisations((JObject)jp.Value, beans);
                }

                if (jp.Name.StartsWith(CategorySchemeUrn, StringComparison.OrdinalIgnoreCase))
                {
                    this.ProcessCategoryScheme((JObject)jp.Value, beans);
                }

                if (jp.Name.StartsWith(DataflowUrn, StringComparison.OrdinalIgnoreCase))
                {
                    this.ProcessDataflow((JObject)jp.Value, beans);
                }
            }

            return beans;
        }

        /// <summary>
        /// Create Annotations based on the input Annotations
        /// </summary>
        /// <param name="jaAnnotations">
        /// JArray of Annotations
        /// </param>
        /// <returns>
        /// The <see cref="IList{IAnnotationMutableObject}"/>.
        /// </returns>
        private IList<IAnnotationMutableObject> GetAnnotations(JArray jaAnnotations)
        {
            if (jaAnnotations != null)
            {
                IList<IAnnotationMutableObject> annotationList = new List<IAnnotationMutableObject>();

                foreach (JToken annotation in jaAnnotations)
                {
                    JObject joAnnotation = (JObject)annotation;
                    IAnnotationMutableObject mutable = new AnnotationMutableCore();

                    mutable.Title = joAnnotation.Value<string>("title");

                    if (joAnnotation["text"] != null)
                    {
                        mutable.AddText(this._locale, joAnnotation.Value<string>("text"));
                    }

                    if (joAnnotation["type"] != null)
                    {
                        mutable.Type = joAnnotation.Value<string>("type");
                    }

                    if (joAnnotation["url"] != null)
                    {
                        mutable.Uri = new Uri(joAnnotation.Value<string>("url"));
                    }

                    annotationList.Add(mutable);
                }

                return annotationList;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates AgencyScheme based on the input Agency Scheme.
        /// </summary>
        /// <param name="agencyScheme">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add AgencyScheme to beans
        /// </param>
        private void ProcessAgencyScheme(JObject agencyScheme, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();

            AgencySchemeMutableCore asmc = new AgencySchemeMutableCore();
            Core core = this.ProcessMaintainableMutableObject(asmc, agencyScheme);

            if (agencyScheme["items"] != null)
            {
                foreach (JObject item in (JArray)agencyScheme["items"])
                {
                    NameableObject nameable = new NameableObject(item);
                    IAgencyMutableObject agency = new AgencyMutableCore();
                    agency.Id = nameable.id;
                    agency.AddName(this._locale, nameable.name);
                    agency.AddDescription(this._locale, nameable.description);
                    agency.Uri = nameable.uri;
                    asmc.AddItem(agency);
                }
            }

            try
            {
                this.AddIfNotDuplicateURN(beans, urns, new AgencySchemeCore(asmc));
            }
            catch (Exception th)
            {
                throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.AgencyScheme), core.agencyID, core.nameable.id, core.version);
            }
        }

        /// <summary>
        /// Creates categorisations based on the input categorisation schemes
        /// </summary>
        /// <param name="categorisation">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add categorisations to
        /// </param>
        private void ProcessCategorisations(JObject categorisation, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();

            if (categorisation != null)
            {
                CategorisationMutableCore cm = new CategorisationMutableCore();
                Core core = this.ProcessMaintainableMutableObject(cm, categorisation);

                if (categorisation["target"] != null)
                {
                    JObject joCatTarget = (JObject)categorisation["target"];

                    if (joCatTarget.HasValues && joCatTarget["urn"] != null)
                    {
                        cm.CategoryReference = new StructureReferenceImpl(joCatTarget.Value<string>("urn"));
                    }
                }

                if (categorisation["source"] != null)
                {
                    JObject joCatSource = (JObject)categorisation["source"];

                    if (joCatSource.HasValues && joCatSource["urn"] != null)
                    {
                        cm.StructureReference = new StructureReferenceImpl(joCatSource.Value<string>("urn"));
                    }
                }

                try
                {
                    this.AddIfNotDuplicateURN(beans, urns, new CategorisationObjectCore(cm));
                }
                catch (Exception th)
                {
                    throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation), core.agencyID, core.nameable.id, core.version);
                }
            }
        }

        /// <summary>
        /// Creates category based on the input category.
        /// </summary>
        /// <param name="cat">
        /// JSON Category - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <returns>
        /// The <see cref="CategoryMutableCore"/>.
        /// </returns>
        private CategoryMutableCore ProcessCategory(JObject cat, ISdmxObjects beans)
        {
            if (cat != null)
            {
                CategoryMutableCore cmc = new CategoryMutableCore();
                this.ProcessItemMutableCore(cmc, cat);

                JArray jaItems = (JArray)cat["items"];
                JArray jaLinks = (JArray)cat["links"];

                if (jaLinks != null)
                {
                    foreach (JObject link in jaLinks)
                    {
                        this.ProcessLink(cmc, link, beans);
                    }
                }

                if (jaItems != null)
                {
                    foreach (JObject subCat in jaItems)
                    {
                        cmc.AddItem(this.ProcessCategory(subCat, beans));
                    }
                }

                return cmc;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates category schemes based on the input category schemes.
        /// </summary>
        /// <param name="catScheme">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add category schemes to
        /// </param>
        private void ProcessCategoryScheme(JObject catScheme, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();
            if (catScheme != null)
            {
                CategorySchemeMutableCore csmc = new CategorySchemeMutableCore();
                Core core = this.ProcessMaintainableMutableObject(csmc, catScheme);

                if (catScheme["items"] != null)
                {
                    JArray categories = (JArray)catScheme["items"];
                    foreach (JObject category in categories)
                    {
                        csmc.AddItem(this.ProcessCategory(category, beans));
                    }
                }

                try
                {
                    this.AddIfNotDuplicateURN(beans, urns, new CategorySchemeObjectCore(csmc));
                }
                catch (Exception th)
                {
                    throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme), core.agencyID, core.nameable.id, core.version);
                }
            }
        }

        /// <summary>
        /// Creates dataflows based on the input dataflows.
        /// </summary>
        /// <param name="dataflow">
        /// - if null will not add anything to the beans container
        /// </param>
        /// <param name="beans">
        /// - to add dataflows to beans
        /// </param>
        private void ProcessDataflow(JObject dataflow, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();

            DataflowMutableCore dmc = new DataflowMutableCore();
            Core core = this.ProcessMaintainableMutableObject(dmc, dataflow);

            if (dataflow["structure"] != null)
            {
                JObject joDsd = (JObject)dataflow["structure"];

                if (joDsd.HasValues && joDsd["urn"] != null)
                {
                    dmc.DataStructureRef = new StructureReferenceImpl(joDsd.Value<string>("urn"));
                }
            }

            try
            {
                this.AddIfNotDuplicateURN(beans, urns, new DataflowObjectCore(dmc));
            }
            catch (Exception th)
            {
                throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow), core.agencyID, core.nameable.id, core.version);
            }
        }

        /// <summary>
        /// Process the header.
        /// </summary>
        /// <param name="baseHeaderType">
        /// The base header type.
        /// </param>
        /// <returns>
        /// The <see cref="IHeader"/>.
        /// </returns>
        private IHeader ProcessHeader(JToken baseHeaderType)
        {
            string id = null;
            DateTime? prepared = null;
            DateTime? reportingBegin = null;
            DateTime? reportingEnd = null;
            IList<IParty> receiver = null;
            IParty sender = null;
            bool isTest = false;

            IDictionary<string, string> additionalAttributes = null;
            IList<IDatasetStructureReference> structures = null;
            IStructureReference dataProviderReference = null;
            DatasetAction datasetAction = null;
            string datasetId = null;
            DateTime? embargoDate = null;
            DateTime? extracted = null;
            IList<ITextTypeWrapper> name = null;
            IList<ITextTypeWrapper> source = null;

            if (baseHeaderType["id"] != null)
            {
                id = baseHeaderType.Value<string>("id");
            }

            if (baseHeaderType["prepared"] != null)
            {
                prepared = baseHeaderType.Value<DateTime>("prepared");
            }

            if (baseHeaderType["receiver"] != null)
            {
                JArray jaReceiver = (JArray)baseHeaderType["receiver"];

                if (jaReceiver.HasValues)
                {
                    List<ITextTypeWrapper> receiverName = new List<ITextTypeWrapper>();
                    receiver = new List<IParty>();

                    foreach (JObject joReceiver in jaReceiver)
                    {
                        if (joReceiver["name"] != null)
                        {
                            receiverName.Add(new TextTypeWrapperImpl(this._locale, joReceiver.Value<string>("name"), null));
                        }

                        receiver.Add(new PartyCore(receiverName, joReceiver.Value<string>("id"), new List<IContact>(), null));
                    }
                }
            }

            if (baseHeaderType["sender"] != null)
            {
                JObject joSender = (JObject)baseHeaderType["sender"];

                if (joSender.HasValues)
                {
                    List<ITextTypeWrapper> senderName = null;
                    if (joSender["name"] != null)
                    {
                        senderName = new List<ITextTypeWrapper> { new TextTypeWrapperImpl(this._locale, joSender.Value<string>("name"), null) };
                    }

                    sender = new PartyCore(senderName, joSender.Value<string>("id"), null, null);
                }
            }

            if (baseHeaderType["reportingBegin"] != null)
            {
                reportingBegin = baseHeaderType.Value<DateTime>("reportingBegin");
            }

            if (baseHeaderType["reportingEnd"] != null)
            {
                reportingEnd = baseHeaderType.Value<DateTime>("reportingEnd");
            }

            if (baseHeaderType["test"] != null)
            {
                isTest = baseHeaderType.Value<bool>("test");
            }

            return new HeaderImpl(additionalAttributes, structures, dataProviderReference, datasetAction, id, datasetId, embargoDate, extracted, prepared, reportingBegin, reportingEnd, name, source, receiver, sender, isTest);
        }

        /// <summary>
        /// Process a base Item properties to be used by an ItemMutableCore such as a Category.
        /// </summary>
        /// <param name="itemMutableCore">
        /// Item to fill properties
        /// </param>
        /// <param name="item">
        /// Json Object data
        /// </param>
        private void ProcessItemMutableCore(ItemMutableCore itemMutableCore, JObject item)
        {
            NameableObject nameableObject = new NameableObject(item);
            if (item["annotations"] != null)
            {
                foreach (IAnnotationMutableObject annotation in this.GetAnnotations((JArray)item["annotations"]))
                {
                    itemMutableCore.AddAnnotation(annotation);
                }
            }

            itemMutableCore.Id = nameableObject.id;
            itemMutableCore.AddName(this._locale, nameableObject.name);
            itemMutableCore.AddDescription(this._locale, nameableObject.description);
            itemMutableCore.Uri = nameableObject.uri;
            itemMutableCore.Urn = nameableObject.urn;
        }

        /// <summary>
        /// Process the relationship link between a Category and a dataflow and create the Categorisation accordingly.
        /// </summary>
        /// <param name="targetCategory">
        /// The target Category
        /// </param>
        /// <param name="link">
        /// The link
        /// </param>
        /// <param name="beans">
        /// - if null will not add anything to the beans container
        /// </param>
        private void ProcessLink(CategoryMutableCore targetCategory, JObject link, ISdmxObjects beans)
        {
            var urns = new HashSet<Uri>();

            if (link != null && link.HasValues && link["href"] != null)
            {
                CategorisationMutableCore cm = new CategorisationMutableCore();

                cm.CategoryReference = new StructureReferenceImpl(targetCategory.Urn.ToString());
                cm.StructureReference = new StructureReferenceImpl(link.Value<string>("href"));
                cm.Id = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", cm.CategoryReference.GetHashCode(), cm.StructureReference.GetHashCode());
                cm.AgencyId = cm.StructureReference.AgencyId;

                string name = string.Format(CultureInfo.InvariantCulture, "Categorisation between: {0} and {1} {2}", cm.CategoryReference, cm.StructureReference.TargetReference.StructureType, cm.StructureReference.MaintainableReference);

                cm.AddName(this._locale, name);

                try
                {
                    this.AddIfNotDuplicateURN(beans, urns, new CategorisationObjectCore(cm));
                }
                catch (Exception th)
                {
                    throw new MaintainableObjectException(th, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation), cm.AgencyId, cm.Id, string.Empty);
                }
            }
        }

        /// <summary>
        /// Processing base object properties to be used by IMaintainableMutableObject such as Dataflow, Category Scheme, Agency Scheme, etc.
        /// </summary>
        /// <param name="mutableCore">
        /// IMaintainableMutableObject to fill the properties
        /// </param>
        /// <param name="data">
        /// Json Object data
        /// </param>
        /// <returns>
        /// The <see cref="Core"/>.
        /// </returns>
        private Core ProcessMaintainableMutableObject(IMaintainableMutableObject mutableCore, JObject data)
        {
            Core core = new Core(data);
            mutableCore.AgencyId = core.agencyID;

            if (data["annotations"] != null)
            {
                foreach (IAnnotationMutableObject annotation in this.GetAnnotations((JArray)data["annotations"]))
                {
                    mutableCore.AddAnnotation(annotation);
                }
            }

            mutableCore.FinalStructure = TertiaryBool.ParseBoolean(core.isFinal);
            mutableCore.Id = core.nameable.id;
            mutableCore.AddName(this._locale, core.nameable.name);
            mutableCore.AddDescription(this._locale, core.nameable.description);
            mutableCore.StartDate = core.startDate;
            mutableCore.EndDate = core.endDate;
            mutableCore.Uri = core.nameable.uri;
            mutableCore.Version = core.version;

            return core;
        }

        /// <summary>
        /// Internal class to get a core equivalent of an artefact.
        /// </summary>
        private class Core
        {
            /// <summary>
            /// The agency id.
            /// </summary>
            public string agencyID = null;

            /// <summary>
            /// The end date.
            /// </summary>
            public DateTime? endDate;

            /// <summary>
            /// The is final.
            /// </summary>
            public bool? isFinal = null;

            /// <summary>
            /// The nameable.
            /// </summary>
            public NameableObject nameable = null;

            /// <summary>
            /// The start date.
            /// </summary>
            public DateTime? startDate;

            /// <summary>
            /// The version.
            /// </summary>
            public string version = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="Core"/> class.
            /// </summary>
            /// <param name="jo">
            /// The jo.
            /// </param>
            public Core(JObject jo)
            {
                if (jo["agencyID"] != null)
                {
                    this.agencyID = jo.Value<string>("agencyID");
                }

                if (jo["isFinal"] != null)
                {
                    this.isFinal = jo.Value<bool>("isFinal");
                }

                if (jo["version"] != null)
                {
                    this.version = jo.Value<string>("version");
                }

                if (jo["validFrom"] != null)
                {
                    this.startDate = jo.Value<DateTime>("validFrom");
                }

                if (jo["validTo"] != null)
                {
                    this.endDate = jo.Value<DateTime>("validTo");
                }

                this.nameable = new NameableObject(jo);
            }
        }

        /// <summary>
        /// Internal Base class to get the Identifiable Object properties
        /// </summary>
        private class IdentifiableObject
        {
            /// <summary>
            /// The id.
            /// </summary>
            public string id = null;

            /// <summary>
            /// The uri.
            /// </summary>
            public Uri uri = null;

            /// <summary>
            /// The urn.
            /// </summary>
            public Uri urn = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="IdentifiableObject"/> class.
            /// </summary>
            /// <param name="jo">
            /// The jo.
            /// </param>
            public IdentifiableObject(JObject jo)
            {
                if (jo["id"] != null)
                {
                    this.id = jo.Value<string>("id");
                }

                if (jo["urn"] != null)
                {
                    this.urn = new Uri(jo.Value<string>("urn"));
                }

                if (jo["uri"] != null)
                {
                    this.uri = new Uri(jo.Value<string>("uri"));
                }
            }
        }

        /// <summary>
        /// Internal class to get the Nameable Object properties
        /// </summary>
        private class NameableObject : IdentifiableObject
        {
            /// <summary>
            /// The description.
            /// </summary>
            public string description = null;

            /// <summary>
            /// The name.
            /// </summary>
            public string name = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="NameableObject"/> class.
            /// </summary>
            /// <param name="jo">
            /// The jo.
            /// </param>
            public NameableObject(JObject jo)
                : base(jo)
            {
                if (jo["name"] != null)
                {
                    this.name = jo.Value<string>("name");
                }

                if (jo["description"] != null)
                {
                    this.description = jo.Value<string>("description");
                }
            }
        }
    }
}