// -----------------------------------------------------------------------
// <copyright file="SuperObjectsBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     Super Objects Builder
    /// </summary>
    public class SuperObjectsBuilder : ISuperObjectsBuilder
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(FileUtil));

        /// <summary>
        /// The _category scheme super bean builder
        /// </summary>
        private readonly CategorySchemeSuperBeanBuilder _categorySchemeSuperBeanBuilder;

        /// <summary>
        /// The _codelist super bean builder
        /// </summary>
        private readonly CodelistSuperBeanBuilder _codelistSuperBeanBuilder;

        /// <summary>
        /// The _concept scheme super object builder
        /// </summary>
        private readonly ConceptSchemeSuperObjectBuilder _conceptSchemeSuperObjectBuilder;

        /// <summary>
        /// The _dataflow super object builder
        /// </summary>
        private readonly DataflowSuperObjectBuilder _dataflowSuperObjectBuilder;

        /// <summary>
        /// The _data structure super object builder
        /// </summary>
        private readonly DataStructureSuperObjectBuilder _dataStructureSuperObjectBuilder;

        /// <summary>
        /// The _hierarchical codelist super object builder
        /// </summary>
        private readonly HierarchicalCodelistSuperObjectBuilder _hierarchicalCodelistSuperObjectBuilder;

        /// <summary>
        /// The _process super object builder
        /// </summary>
        private readonly ProcessSuperObjectBuilder _processSuperObjectBuilder;

        /// <summary>
        /// The _provision super object builder
        /// </summary>
        private readonly ProvisionSuperObjectBuilder _provisionSuperObjectBuilder;

        /// <summary>
        /// The _registration super object builder
        /// </summary>
        private readonly RegistrationSuperObjectBuilder _registrationSuperObjectBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="SuperObjectsBuilder" /> class.
        /// </summary>
        /// <param name="categorySchemeSuperBeanBuilder">The category scheme super bean builder.</param>
        /// <param name="codelistSuperBeanBuilder">The codelist super bean builder.</param>
        /// <param name="conceptSchemeSuperObjectBuilder">The concept scheme super object builder.</param>
        /// <param name="dataflowSuperObjectBuilder">The dataflow super object builder.</param>
        /// <param name="dataStructureSuperObjectBuilder">The data structure super object builder.</param>
        /// <param name="hierarchicalCodelistSuperObjectBuilder">The hierarchical codelist super object builder.</param>
        /// <param name="provisionSuperObjectBuilder">The provision super object builder.</param>
        /// <param name="processSuperObjectBuilder">The process super object builder.</param>
        /// <param name="registrationSuperObjectBuilder">The registration super object builder.</param>
        public SuperObjectsBuilder(
            CategorySchemeSuperBeanBuilder categorySchemeSuperBeanBuilder, 
            CodelistSuperBeanBuilder codelistSuperBeanBuilder, 
            ConceptSchemeSuperObjectBuilder conceptSchemeSuperObjectBuilder, 
            DataflowSuperObjectBuilder dataflowSuperObjectBuilder, 
            DataStructureSuperObjectBuilder dataStructureSuperObjectBuilder, 
            HierarchicalCodelistSuperObjectBuilder hierarchicalCodelistSuperObjectBuilder, 
            ProvisionSuperObjectBuilder provisionSuperObjectBuilder, 
            ProcessSuperObjectBuilder processSuperObjectBuilder, 
            RegistrationSuperObjectBuilder registrationSuperObjectBuilder)
        {
            this._categorySchemeSuperBeanBuilder = categorySchemeSuperBeanBuilder;
            this._codelistSuperBeanBuilder = codelistSuperBeanBuilder;
            this._conceptSchemeSuperObjectBuilder = conceptSchemeSuperObjectBuilder;
            this._dataflowSuperObjectBuilder = dataflowSuperObjectBuilder;
            this._dataStructureSuperObjectBuilder = dataStructureSuperObjectBuilder;
            this._hierarchicalCodelistSuperObjectBuilder = hierarchicalCodelistSuperObjectBuilder;
            this._provisionSuperObjectBuilder = provisionSuperObjectBuilder;
            this._processSuperObjectBuilder = processSuperObjectBuilder;
            this._registrationSuperObjectBuilder = registrationSuperObjectBuilder;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SuperObjectsBuilder" /> class.
        /// </summary>
        public SuperObjectsBuilder()
            : this(
                new CategorySchemeSuperBeanBuilder(), 
                new CodelistSuperBeanBuilder(), 
                new ConceptSchemeSuperObjectBuilder(), 
                new DataflowSuperObjectBuilder(), 
                new DataStructureSuperObjectBuilder(), 
                new HierarchicalCodelistSuperObjectBuilder(), 
                new ProvisionSuperObjectBuilder(), 
                new ProcessSuperObjectBuilder(), 
                new RegistrationSuperObjectBuilder())
        {
        }

        /// <summary>
        /// Builds an object of type <see cref="ISuperObjects" />
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>Object of type <see cref="ISuperObjects" /></returns>
        /// <exception cref="CrossReferenceException">If an artefact is not found.</exception>
        public ISuperObjects Build(ISdmxObjects buildFrom)
        {
            return this.Build(buildFrom, null, new InMemoryRetrievalManager(buildFrom));
        }

        /// <summary>
        /// Builds super objects from the input objects, obtains any additional required objects from the retrieval manager
        /// </summary>
        /// <param name="buildFrom">The source SDMX Object</param>
        /// <param name="existingObjects">if any super objects exist then they should be passed in here in order to reuse - new objects will be added to this
        /// container</param>
        /// <param name="retrievalManager">The Retrieval Manager</param>
        /// <returns>The <see cref="ISuperObjects" /> .</returns>
        /// <exception cref="System.ArgumentNullException">The <paramref name="buildFrom" />   is null.</exception>
        /// <exception cref="CrossReferenceException">If an artefact is not found.</exception>
        public ISuperObjects Build(
            ISdmxObjects buildFrom, 
            ISuperObjects existingObjects, 
            ISdmxObjectRetrievalManager retrievalManager)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            _log.Debug("Build ISuperObjects: Create LocalRetrievalManager");

            if (existingObjects == null)
            {
                existingObjects = new SuperObjects();
            }

            foreach (var categoryScheme in buildFrom.CategorySchemes)
            {
                _log.Debug("Build SuperBean: " + categoryScheme.Urn);
                existingObjects.AddCategoryScheme(this._categorySchemeSuperBeanBuilder.Build(categoryScheme));
            }

            foreach (var codelistObject in buildFrom.Codelists)
            {
                _log.Debug("Build SuperBean: " + codelistObject.Urn);
                existingObjects.AddCodelist(this._codelistSuperBeanBuilder.Build(codelistObject));
            }

            foreach (var conceptSchemeObject in buildFrom.ConceptSchemes)
            {
                _log.Debug("Build SuperBean: " + conceptSchemeObject.Urn);
                existingObjects.AddConceptScheme(this._conceptSchemeSuperObjectBuilder.Build(conceptSchemeObject, retrievalManager, existingObjects));
            }

            foreach (var dataflowObject in buildFrom.Dataflows)
            {
                _log.Debug("Build SuperBean: " + dataflowObject.Urn);
                existingObjects.AddDataflow(this._dataflowSuperObjectBuilder.Build(dataflowObject, retrievalManager, existingObjects));
            }

            foreach (var dataStructureObject in buildFrom.DataStructures)
            {
                _log.Debug("Build SuperBean: " + dataStructureObject.Urn);
                existingObjects.AddDataStructure(this._dataStructureSuperObjectBuilder.Build(dataStructureObject, retrievalManager, existingObjects));
            }

            foreach (var hierarchicalCodelistObject in buildFrom.HierarchicalCodelists)
            {
                _log.Debug("Build SuperBean: " + hierarchicalCodelistObject.Urn);
                existingObjects.AddHierarchicalCodelist(this._hierarchicalCodelistSuperObjectBuilder.Build(hierarchicalCodelistObject, retrievalManager));
            }

            foreach (var provisionAgreementObject in buildFrom.ProvisionAgreements)
            {
                _log.Debug("Build SuperBean: " + provisionAgreementObject.Urn);
                existingObjects.AddProvision(this._provisionSuperObjectBuilder.Build(provisionAgreementObject, retrievalManager, existingObjects));
            }

            foreach (var processObject in buildFrom.Processes)
            {
                _log.Debug("Build SuperBean: " + processObject.Urn);
                existingObjects.AddProcess(this._processSuperObjectBuilder.Build(processObject, retrievalManager));
            }

            foreach (var registration in buildFrom.Registrations)
            {
                _log.Debug("Build SuperBean: " + registration.Urn);
                existingObjects.AddRegistration(this._registrationSuperObjectBuilder.Build(registration, retrievalManager, existingObjects));
            }

            return existingObjects;
        }
    }
}