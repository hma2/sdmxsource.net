// -----------------------------------------------------------------------
// <copyright file="StructureBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;

    /// <summary>
    ///     This class provides access to the structuralMetadataManager allowing classes extending this class
    /// </summary>
    /// <typeparam name="TK">The type of the destination structure super object.</typeparam>
    /// <typeparam name="TV">The type of the corresponding source plain object .</typeparam>
    public abstract class StructureBuilder<TK, TV> : ISuperObjectBuilder<TK, TV>
        where TK : ISuperObject where TV : ISdmxObject
    {
        /// <summary>
        /// Build a super object from a SdmxObject and a corresponding SdmxObjectRetrievalManager (optional) to resolve any
        /// cross referenced artifacts
        /// </summary>
        /// <param name="buildFrom">- the SdmxObject to build from</param>
        /// <param name="objectRetrievalManager">- to resolve any cross referenced artifacts declared by V - the buildFrom argument</param>
        /// <param name="exitingObjects">- can be null if none already exit</param>
        /// <returns>K - the Sdmx super object representation of the SdmxObject argument</returns>
        public abstract TK Build(TV buildFrom, ISdmxObjectRetrievalManager objectRetrievalManager, ISuperObjects exitingObjects);
    }
}