// -----------------------------------------------------------------------
// <copyright file="ConceptSchemeSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     ConceptSchemeSuperObjectBuilder class
    /// </summary>
    public class ConceptSchemeSuperObjectBuilder : StructureBuilder<IConceptSchemeSuperObject, IConceptSchemeObject>
    {
        /// <summary>
        /// The _codelist super bean builder
        /// </summary>
        private CodelistSuperBeanBuilder _codelistSuperBeanBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="codelistSuperBeanBuilder">The codelist super bean builder.</param>
        public ConceptSchemeSuperObjectBuilder(CodelistSuperBeanBuilder codelistSuperBeanBuilder)
        {
            this._codelistSuperBeanBuilder = codelistSuperBeanBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSchemeSuperObjectBuilder" /> class.
        /// </summary>
        public ConceptSchemeSuperObjectBuilder()
            : this(new CodelistSuperBeanBuilder())
        {
        }

        /// <summary>
        ///     Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The super object</returns>
        /// <exception cref="CrossReferenceException">The exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public override IConceptSchemeSuperObject Build(
            IConceptSchemeObject buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            var codelistMap = new Dictionary<IConceptObject, ICodelistSuperObject>();
            foreach (var conceptObject in buildFrom.Items)
            {
                if (conceptObject.CoreRepresentation != null && conceptObject.CoreRepresentation.Representation != null)
                {
                    var codelistRef = conceptObject.CoreRepresentation.Representation;

                    var codelistSuperBean = superBeanRetrievalManager.GetCodelistSuperObject(codelistRef);
                    if (codelistSuperBean == null)
                    {
                        if (retrievalManager != null)
                        {
                            var codelist = retrievalManager.GetIdentifiableObject<ICodelistObject>(codelistRef);
                            codelistSuperBean = this._codelistSuperBeanBuilder.Build(codelist);
                        }

                        if (codelistSuperBean == null)
                        {
                            throw new CrossReferenceException(conceptObject.CoreRepresentation.Representation);
                        }

                        existingBeans.AddCodelist(codelistSuperBean);
                    }

                    codelistMap.Add(conceptObject, codelistSuperBean);
                }
            }

            return new ConceptSchemeSuperObject(buildFrom, codelistMap);
        }

        /// <summary>
        ///     Sets the codelist super bean builder.
        /// </summary>
        /// <param name="codelistSuperBeanBuilder">The codelist super bean builder.</param>
        public void SetCodelistSuperBeanBuilder(CodelistSuperBeanBuilder codelistSuperBeanBuilder)
        {
            this._codelistSuperBeanBuilder = codelistSuperBeanBuilder;
        }
    }
}