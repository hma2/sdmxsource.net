// -----------------------------------------------------------------------
// <copyright file="CodelistSuperBeanBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Codelist;

    /// <summary>
    ///   The  CodelistSuperBean Builder
    /// </summary>
    public class CodelistSuperBeanBuilder : IBuilder<ICodelistSuperObject, ICodelistObject>
    {
        /// <summary>
        /// Builds an object of type <see cref="ICodelistSuperObject"/>
        /// </summary>
        /// <param name="buildFrom">An Object to build the output object from</param>
        /// <returns>Object of type <see cref="ICodelistSuperObject"/></returns>
        public ICodelistSuperObject Build(ICodelistObject buildFrom)
        {
            return new CodelistSuperObject(buildFrom);
        }
    }
}