// -----------------------------------------------------------------------
// <copyright file="DataStructureSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;
    using System.Collections.Generic;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     DataStructureSuperObjectBuilder class
    /// </summary>
    public class DataStructureSuperObjectBuilder : StructureBuilder<IDataStructureSuperObject, IDataStructureObject>
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataStructureSuperObjectBuilder));

        /// <summary>
        /// The _attribute super object builder
        /// </summary>
        private readonly AttributeSuperObjectBuilder _attributeSuperObjectBuilder;

        /// <summary>
        /// The _dimension super object builder
        /// </summary>
        private readonly DimensionSuperObjectBuilder _dimensionSuperObjectBuilder;

        /// <summary>
        /// The _primary measure super object builder
        /// </summary>
        private readonly PrimaryMeasureSuperObjectBuilder _primaryMeasureSuperObjectBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="attributeSuperObjectBuilder">The attribute super object builder.</param>
        /// <param name="dimensionSuperObjectBuilder">The dimension super object builder.</param>
        /// <param name="primaryMeasureSuperObjectBuilder">The primary measure super object builder.</param>
        public DataStructureSuperObjectBuilder(
            AttributeSuperObjectBuilder attributeSuperObjectBuilder, 
            DimensionSuperObjectBuilder dimensionSuperObjectBuilder, 
            PrimaryMeasureSuperObjectBuilder primaryMeasureSuperObjectBuilder)
        {
            this._attributeSuperObjectBuilder = attributeSuperObjectBuilder;
            this._dimensionSuperObjectBuilder = dimensionSuperObjectBuilder;
            this._primaryMeasureSuperObjectBuilder = primaryMeasureSuperObjectBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataStructureSuperObjectBuilder" /> class.
        /// </summary>
        public DataStructureSuperObjectBuilder()
            : this(
                new AttributeSuperObjectBuilder(), 
                new DimensionSuperObjectBuilder(), 
                new PrimaryMeasureSuperObjectBuilder())
        {
        }

        /// <summary>
        /// Build the DataStructure from an KeyFamilyType XML Bean
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The <see cref="IDataStructureSuperObject"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public override IDataStructureSuperObject Build(
            IDataStructureObject buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (buildFrom.IsExternalReference != null && buildFrom.IsExternalReference.IsTrue)
            {
                throw new SdmxSemmanticException("Cannot create a super object from a stub.");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            _log.Debug("Build KeyFamilySuperBean SuperBean");
            var attributes = new List<IAttributeSuperObject>();
            var dimensions = new List<IDimensionSuperObject>();

            foreach (var dimension in buildFrom.GetDimensions())
            {
                _log.Debug("Build Dimension: " + dimension.Urn);
                dimensions.Add(this._dimensionSuperObjectBuilder.Build(dimension, retrievalManager, existingBeans));
            }

            foreach (var attribute in buildFrom.Attributes)
            {
                _log.Debug("Build Attribute: " + attribute.Urn);
                attributes.Add(this._attributeSuperObjectBuilder.Build(attribute, retrievalManager, existingBeans));
            }

            IPrimaryMeasureSuperObject primaryMeasure = null;
            if (buildFrom.PrimaryMeasure != null)
            {
                _log.Debug("Build Measure: " + buildFrom.PrimaryMeasure.Urn);
                primaryMeasure = this._primaryMeasureSuperObjectBuilder.Build(
                    buildFrom.PrimaryMeasure, 
                    retrievalManager, 
                    existingBeans);
            }

            return new DataStructureSuperObject(buildFrom, dimensions, attributes, primaryMeasure);
        }
    }
}