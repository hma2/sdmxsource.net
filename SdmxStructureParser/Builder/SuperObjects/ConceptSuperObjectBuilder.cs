// -----------------------------------------------------------------------
// <copyright file="ConceptSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     ConceptSuperObjectBuilder class
    /// </summary>
    public class ConceptSuperObjectBuilder : StructureBuilder<IConceptSuperObject, IConceptObject>
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ConceptSuperObjectBuilder));

        /// <summary>
        /// The _codelist super bean builder
        /// </summary>
        private CodelistSuperBeanBuilder _codelistSuperBeanBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="codelistSuperBeanBuilder">The codelist super bean builder.</param>
        public ConceptSuperObjectBuilder(CodelistSuperBeanBuilder codelistSuperBeanBuilder)
        {
            this._codelistSuperBeanBuilder = codelistSuperBeanBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConceptSuperObjectBuilder" /> class.
        /// </summary>
        public ConceptSuperObjectBuilder()
            : this(new CodelistSuperBeanBuilder())
        {
        }

        /// <summary>
        /// Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The super object</returns>
        /// <exception cref="CrossReferenceException">The exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom" /> is <see langword="null" />.</exception>
        public override IConceptSuperObject Build(
            IConceptObject buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            ICodelistSuperObject codelistSuperBean = null;
            if (buildFrom.CoreRepresentation != null && buildFrom.CoreRepresentation.Representation != null)
            {
                var codelistRef = buildFrom.CoreRepresentation.Representation;

                _log.Debug("get codelist super bean : " + codelistRef);

                codelistSuperBean = superBeanRetrievalManager.GetCodelistSuperObject(codelistRef);
                if (codelistSuperBean == null)
                {
                    _log.Debug("no existing super bean found build new : " + codelistRef);
                    ICodelistObject codelist = null;
                    if (retrievalManager != null)
                    {
                        codelist = retrievalManager.GetIdentifiableObject<ICodelistObject>(codelistRef);
                    }

                    if (codelist == null)
                    {
                        throw new CrossReferenceException(buildFrom.CoreRepresentation.Representation);
                    }

                    codelistSuperBean = this._codelistSuperBeanBuilder.Build(codelist);

                    _log.Debug("codelist super bean built");
                    existingBeans.AddCodelist(codelistSuperBean);
                }
            }

            return new ConceptSuperObject(buildFrom, codelistSuperBean);
        }

        /// <summary>
        ///     Sets the codelist super bean builder.
        /// </summary>
        /// <param name="codelistSuperBeanBuilder">The codelist super bean builder.</param>
        public void SetCodelistSuperBeanBuilder(CodelistSuperBeanBuilder codelistSuperBeanBuilder)
        {
            this._codelistSuperBeanBuilder = codelistSuperBeanBuilder;
        }
    }
}