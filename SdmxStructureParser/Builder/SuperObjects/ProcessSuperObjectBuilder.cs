// -----------------------------------------------------------------------
// <copyright file="ProcessSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Process;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Process;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.Process;

    /// <summary>
    ///     Process Super Object Builder
    /// </summary>
    public class ProcessSuperObjectBuilder
    {
        /// <summary>
        /// The _process step super bean builder
        /// </summary>
        private readonly ProcessStepSuperBeanBuilder _processStepSuperBeanBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="processStepSuperBeanBuilder">The process step super bean builder.</param>
        public ProcessSuperObjectBuilder(ProcessStepSuperBeanBuilder processStepSuperBeanBuilder)
        {
            this._processStepSuperBeanBuilder = processStepSuperBeanBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessSuperObjectBuilder" /> class.
        /// </summary>
        public ProcessSuperObjectBuilder()
            : this(new ProcessStepSuperBeanBuilder())
        {
        }

        /// <summary>
        /// Build the Process from a Process XML Bean
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <returns>The <see cref="IProcessSuperObject"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public IProcessSuperObject Build(IProcessObject buildFrom, IIdentifiableRetrievalManager retrievalManager)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            var steps =
                buildFrom.ProcessSteps.Select(
                    processStepSuperObject => this._processStepSuperBeanBuilder.Build(processStepSuperObject, retrievalManager)).ToList();

            return new ProcessSuperObject(buildFrom, steps);
        }
    }
}