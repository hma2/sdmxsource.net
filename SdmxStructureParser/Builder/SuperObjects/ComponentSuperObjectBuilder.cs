// -----------------------------------------------------------------------
// <copyright file="ComponentSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     ComponentSuperObjectBuilder class
    /// </summary>
    /// <typeparam name="TK">Generic type</typeparam>
    /// <typeparam name="TV">Second generic type</typeparam>
    public abstract class ComponentSuperObjectBuilder<TK, TV> : StructureBuilder<TK, TV>
        where TK : ISuperObject where TV : ISdmxObject
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ComponentSuperObjectBuilder<TK, TV>));

        /// <summary>
        /// The _codelist super bean builder
        /// </summary>
        private readonly CodelistSuperBeanBuilder _codelistSuperBeanBuilder;

        /// <summary>
        /// The _concept super object builder
        /// </summary>
        private readonly ConceptSuperObjectBuilder _conceptSuperObjectBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentSuperObjectBuilder{TK, TV}"/> class.
        /// </summary>
        /// <param name="conceptSuperObjectBuilder">The concept super object builder.</param>
        /// <param name="codelistSuperBeanBuilder">The codelist super bean builder.</param>
        protected ComponentSuperObjectBuilder(
            ConceptSuperObjectBuilder conceptSuperObjectBuilder, 
            CodelistSuperBeanBuilder codelistSuperBeanBuilder)
        {
            this._conceptSuperObjectBuilder = conceptSuperObjectBuilder;
            this._codelistSuperBeanBuilder = codelistSuperBeanBuilder;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentSuperObjectBuilder{TK, TV}"/> class.
        /// </summary>
        protected ComponentSuperObjectBuilder()
            : this(new ConceptSuperObjectBuilder(), new CodelistSuperBeanBuilder())
        {
        }

        /// <summary>
        /// Gets the codelist.
        /// </summary>
        /// <param name="componentBean">The component bean.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The super object</returns>
        /// <exception cref="CrossReferenceException">The exception</exception>
        internal ICodelistSuperObject GetCodelist(
            IComponent componentBean, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            if (componentBean.HasCodedRepresentation())
            {
                var codelistRef = componentBean.Representation.Representation;

                _log.Debug("get codelist super bean : " + codelistRef);

                var codelistSuperBean = superBeanRetrievalManager.GetCodelistSuperObject(codelistRef);
                if (codelistSuperBean == null)
                {
                    var codelistBean = retrievalManager.GetIdentifiableObject<ICodelistObject>(codelistRef);

                    if (codelistBean == null)
                    {
                        throw new CrossReferenceException(componentBean.Representation.Representation);
                    }

                    _log.Debug("no existing super bean found build new : " + codelistBean.Urn);
                    codelistSuperBean = this._codelistSuperBeanBuilder.Build(codelistBean);

                    existingBeans.AddCodelist(codelistSuperBean);
                }

                return codelistSuperBean;
            }

            _log.Debug("component is uncoded");
            return null;
        }

        /// <summary>
        /// Gets the concept.
        /// </summary>
        /// <param name="componentBean">The component bean.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The <see cref="IConceptSuperObject"/></returns>
        /// <exception cref="System.ArgumentNullException">The <paramref name="componentBean"/> or <paramref name="retrievalManager"/> is null.</exception>
        /// <exception cref="CrossReferenceException">Could not find concept bean to build concept super bean from</exception>
        internal IConceptSuperObject GetConcept(
            IComponent componentBean, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (componentBean == null)
            {
                throw new ArgumentNullException("componentBean");
            }

            if (retrievalManager == null)
            {
                throw new ArgumentNullException("retrievalManager");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            var conceptRef = componentBean.ConceptRef;

            _log.Debug("get concept super bean : " + conceptRef);

            var conceptId = conceptRef.ChildReference.Id;

            // Return an existing super bean if you have one
            var superBean = superBeanRetrievalManager.GetConceptSchemeSuperObject(conceptRef);
            if (superBean != null)
            {
                _log.Debug("check existing concept scheme super bean : " + superBean.Urn);
                foreach (var conceptSuperObject in superBean.Concepts)
                {
                    if (conceptSuperObject.Id.Equals(conceptId))
                    {
                        _log.Debug("existing concept super bean found");
                        return conceptSuperObject;
                    }
                }
            }

            _log.Debug("No existing concept super bean found, build new from concept scheme bean");

            // Could not find an existing one.
            var conceptBean = retrievalManager.GetIdentifiableObject<IConceptObject>(conceptRef);
            if (conceptBean == null)
            {
                _log.Error("Could not find concept bean to build concept super bean from : " + conceptRef);
                throw new CrossReferenceException(conceptRef);
            }

            return this._conceptSuperObjectBuilder.Build(conceptBean, retrievalManager, existingBeans);
        }
    }
}