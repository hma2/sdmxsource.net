// -----------------------------------------------------------------------
// <copyright file="DataflowSuperObjectBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-02-17
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects
{
    using System;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects;
    using Org.Sdmxsource.Sdmx.Api.Model.SuperObjects.DataStructure;  
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.SuperObjects.DataStructure;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     DataflowSuperObjectBuilder class
    /// </summary>
    public class DataflowSuperObjectBuilder : StructureBuilder<IDataflowSuperObject, IDataflowObject>
    {
        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DataflowSuperObjectBuilder));

        /// <summary>
        /// The _data structure super object builder
        /// </summary>
        private readonly DataStructureSuperObjectBuilder _dataStructureSuperObjectBuilder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataflowSuperObjectBuilder" /> class.
        /// </summary>
        /// <param name="_dataStructureSuperObjectBuilder">The data structure super bean builder.</param>
        public DataflowSuperObjectBuilder(DataStructureSuperObjectBuilder _dataStructureSuperObjectBuilder)
        {
            this._dataStructureSuperObjectBuilder = _dataStructureSuperObjectBuilder;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataflowSuperObjectBuilder" /> class.
        /// </summary>
        public DataflowSuperObjectBuilder()
            : this(new DataStructureSuperObjectBuilder())
        {
        }

        /// <summary>
        ///     Builds the specified build from.
        /// </summary>
        /// <param name="buildFrom">The build from.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="existingBeans">The existing beans.</param>
        /// <returns>The super object</returns>
        /// <exception cref="CrossReferenceException">The exception</exception>
        /// <exception cref="ArgumentNullException"><paramref name="buildFrom"/> is <see langword="null" />.</exception>
        public override IDataflowSuperObject Build(
            IDataflowObject buildFrom, 
            ISdmxObjectRetrievalManager retrievalManager, 
            ISuperObjects existingBeans)
        {
            if (buildFrom == null)
            {
                throw new ArgumentNullException("buildFrom");
            }

            if (existingBeans == null)
            {
                existingBeans = new SuperObjects();
            }

            var superBeanRetrievalManager = new InMemorySdmxSuperObjectRetrievalManager(existingBeans);

            var dsdRef = buildFrom.DataStructureRef;

            _log.Debug("Build Dataflow SuperBean. Get DSD Superbean :" + dsdRef);

            var superBean = superBeanRetrievalManager.GetDataStructureSuperObject(dsdRef);

            if (superBean == null)
            {
                _log.Debug("No existing dsd super bean found, build new");
                IDataStructureObject keyFamily = null;
                if (retrievalManager != null)
                {
                    keyFamily = retrievalManager.GetIdentifiableObject<IDataStructureObject>(dsdRef);
                }

                if (keyFamily != null)
                {
                    superBean = _dataStructureSuperObjectBuilder.Build(keyFamily, retrievalManager, existingBeans);
                    existingBeans.AddDataStructure(superBean);
                }
                else
                {
                    throw new CrossReferenceException(buildFrom.DataStructureRef);
                }
            }

            return new DataflowSuperObject(buildFrom, superBean);
        }
    }
}