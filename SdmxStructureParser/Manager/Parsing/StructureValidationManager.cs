// -----------------------------------------------------------------------
// <copyright file="StructureValidationManager.cs" company="EUROSTAT">
//   Date Created : 2013-01-29
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using System.IO;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    ///     The structure validation manager implementation
    /// </summary>
    public class StructureValidationManager : BaseParsingManager, IStructureValidationManager
    {
        /// <summary>
        ///     The output format.
        /// </summary>
        private static readonly StructureOutputFormat _outputFormat =
            StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);

        /// <summary>
        ///     The _readable data location factory
        /// </summary>
        private readonly IReadableDataLocationFactory _readableDataLocationFactory;

        /// <summary>
        ///     The structure parsing manager.
        /// </summary>
        private readonly IStructureParsingManager _structureParsingManager;

        /// <summary>
        ///     The structure writing manager.
        /// </summary>
        private readonly IStructureWriterManager _structureWritingManager;

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureValidationManager" /> class.
        /// </summary>
        /// <param name="structureParsingManager">
        ///     The structure parsing manager.
        /// </param>
        /// <param name="structureWritingManager">
        ///     The structure writing manager.
        /// </param>
        /// <param name="readableDataLocationFactory">
        ///     The readable data location factory.
        /// </param>
        public StructureValidationManager(
            IStructureParsingManager structureParsingManager, 
            IStructureWriterManager structureWritingManager, 
            IReadableDataLocationFactory readableDataLocationFactory)
        {
            this._structureParsingManager = structureParsingManager ?? new StructureParsingManager();
            this._structureWritingManager = structureWritingManager ?? new StructureWriterManager();
            this._readableDataLocationFactory = readableDataLocationFactory ?? new ReadableDataLocationFactory();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="StructureValidationManager" /> class.
        /// </summary>
        /// <param name="sdmxSchema">
        ///     The sdmx schema.
        /// </param>
        public StructureValidationManager(SdmxSchemaEnumType sdmxSchema)
            : base(sdmxSchema)
        {
            this._structureWritingManager = new StructureWriterManager();
            this._structureParsingManager = new StructureParsingManager(sdmxSchema);
        }

        /// <summary>
        ///     Validates a structure is compliant to the 2.1 SDMX schema, by writing it out as SDMX and validating against the
        ///     schema.
        ///     WARNING: This implementation writes temporary to memory.
        /// </summary>
        /// <param name="maintainableBean">
        ///     The maintainable object
        /// </param>
        public virtual void ValidateStructure(IMaintainableObject maintainableBean)
        {
            using (var memoryStream = new MemoryStream())
            {
                this._structureWritingManager.WriteStructure(
                    maintainableBean, 
                    null, 
                    new SdmxStructureFormat(_outputFormat), 
                    memoryStream);
                memoryStream.Position = 0;

                using (
                    IReadableDataLocation dataLocation =
                        this._readableDataLocationFactory.GetReadableDataLocation(memoryStream.ToArray()))
                {
                    this._structureParsingManager.ParseStructures(dataLocation);
                }
            }
        }
    }
}