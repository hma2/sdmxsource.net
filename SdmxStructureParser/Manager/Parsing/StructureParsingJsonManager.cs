// -----------------------------------------------------------------------
// <copyright file="StructureParsingJsonManager.cs" company="EUROSTAT">
//   Date Created : 2016-07-21
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing
{
    using System.Collections.Generic;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Util.Log;
    using SdmxObjects.Model;

    /// <summary>
    ///     The Json structure parsing manager implementation.
    /// </summary>
    public class StructureParsingJsonManager : StructureParsingManager
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(StructureParsingManager));

        /// <summary>
        ///     The structure parser factory
        /// </summary>
        private readonly IList<IStructureParserFactory> _structureParserFactories;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureParsingJsonManager" /> class.
        /// </summary>
        /// <param name="sdmxStructureJsonFormat">The SDMX schema.</param>
        public StructureParsingJsonManager(SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            this._structureParserFactories = new List<IStructureParserFactory>
                                                 {
                                                     new SdmxJsonStructureParserFactory(
                                                         null,
                                                         sdmxStructureJsonFormat)
                                                 };
        }

        /// <summary>
        /// Parses a Json structure document.
        /// </summary>
        /// <param name="dataLocation">the supplied structures</param>
        /// <param name="settings">The settings</param>
        /// <param name="retrievalManager">The retrieval Manager</param>
        /// <returns>
        /// StructureWorkspace - from this structures can be retrieved in any format required
        /// </returns>
        /// <remarks>
        /// Validates the SDMX-ML against the correct schema, also validates the structure according to the SDMX standards,
        /// using rules which can not be specified by schema.  Uses the supplied settings to perform any extra operations.  If
        /// resolve external references is set to true, then these structures will also be validated against the Schema and
        /// business logic.
        /// </remarks>
        public override IStructureWorkspace ParseStructures(
            IReadableDataLocation dataLocation,
            ResolutionSettings settings,
            ISdmxObjectRetrievalManager retrievalManager)
        {
            LoggingUtil.Debug(_log, "Parse Structure request, for xml at location: " + dataLocation);

            return this.BuildWorkspace(this.GetSdmxObjects(dataLocation), settings, retrievalManager);
        }

        /// <summary>
        ///     Gets the sdmx objects from the source data location..
        /// </summary>
        /// <param name="sourceData">
        ///     - the supplied structures
        /// </param>
        /// <returns>
        ///     The Sdmx objects
        /// </returns>
        private ISdmxObjects GetSdmxObjects(IReadableDataLocation sourceData)
        {
            foreach (var structureParserFactory in this._structureParserFactories)
            {
                ISdmxObjects beans = structureParserFactory.GetSdmxObjects(sourceData);
                if (beans != null)
                {
                    return beans;
                }
            }

            throw new SdmxNotImplementedException(
                "Can not parse structures.  Structure format is either not supported, or has an invalid syntax");
        }
    }
}