﻿// -----------------------------------------------------------------------
// <copyright file="SdmxJsonStructureParserFactory.cs" company="EUROSTAT">
//   Date Created : 2014-03-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Factory
{
    using System;
    using System.IO;
    using System.Security;
    using System.Xml;

    using log4net;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.SdmxObjects;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using SdmxObjects.Model;

    /// <summary>
    /// SdmxJsonStructureParserFactory class
    /// </summary>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Factory.IStructureParserFactory" />
    internal class SdmxJsonStructureParserFactory : IStructureParserFactory
    {
        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SdmxJsonStructureParserFactory));

        /// <summary>
        ///     The sdmx objects builder.
        /// </summary>
        private readonly SdmxObjectsJsonBuilder _sdmxBeansJsonBuilder;

        /// <summary>
        ///     If set to a value other than <see cref="SdmxSchemaEnumType.Null" />, this SDMX schema will be assumed for all input
        /// </summary>
        private readonly SdmxSchemaEnumType _sdmxSchema;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxJsonStructureParserFactory" /> class.
        /// </summary>
        public SdmxJsonStructureParserFactory()
            : this(null, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxJsonStructureParserFactory" /> class.
        /// </summary>
        /// <param name="sdmxBeansJsonBuilder">The SDMX beans builder.</param>
        public SdmxJsonStructureParserFactory(
            SdmxObjectsJsonBuilder sdmxBeansJsonBuilder)
            : this(sdmxBeansJsonBuilder, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxJsonStructureParserFactory" /> class.
        /// </summary>
        /// <param name="sdmxBeansJsonBuilder">The SDMX beans builder.</param>
        /// <param name="sdmxStructureJsonFormat">The SDMX Structure Json Format.</param>
        public SdmxJsonStructureParserFactory(
            SdmxObjectsJsonBuilder sdmxBeansJsonBuilder,
            SdmxStructureJsonFormat sdmxStructureJsonFormat)
        {
            this._sdmxSchema = SdmxSchemaEnumType.Json;
            this._sdmxBeansJsonBuilder = sdmxBeansJsonBuilder ?? new SdmxObjectsJsonBuilder(sdmxStructureJsonFormat);
        }

        /// <summary>
        /// Returns the <see cref="ISdmxObjects" />.
        /// </summary>
        /// <param name="dataLocation">The data location.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="SdmxException">
        /// Error while attempting to process SDMX-Json Structure file
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="dataLocation"/> is <see langword="null" />.</exception>
        /// <exception cref="SecurityException">The <see cref="T:System.Xml.XmlReader" /> does not have sufficient permissions to access the location of the XML data.</exception>
        /// <exception cref="SdmxSyntaxException">Can not get Scheme Version from SDMX message.  Unable to determine structure type from Namespaces- please ensure this is a valid SDMX document</exception>
        /// <exception cref="XmlException">An error occurred while parsing the XML. </exception>
        /// <exception cref="SdmxException">
        /// Error while attempting to process SDMX-Json Structure file
        /// or
        /// Error while attempting to process SDMX-Json Structure file
        /// </exception>
        public ISdmxObjects GetSdmxObjects(IReadableDataLocation dataLocation)
        {
            if (dataLocation == null)
            {
                throw new ArgumentNullException("dataLocation");
            }

            SdmxSchema schemaVersion =
                SdmxSchema.GetFromEnum(
                    this._sdmxSchema == SdmxSchemaEnumType.Null
                        ? SdmxMessageUtil.GetSchemaVersion(dataLocation)
                        : this._sdmxSchema);

            _log.Debug("Schema Version : " + schemaVersion.EnumType);

            if (schemaVersion.EnumType == SdmxSchemaEnumType.Edi)
            {
                return null;
            }

            if (schemaVersion.EnumType == SdmxSchemaEnumType.Json)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(dataLocation.InputStream))
                    using (JsonTextReader reader = new JsonTextReader(sr))
                    {
                        return this.ParseSdmxJsonStructureMessage((JObject)JToken.ReadFrom(reader));
                    }
                }
                catch (JsonException e)
                {
                    throw new SdmxException("Error while attempting to process SDMX-Json Structure file", e);
                }
                catch (IOException e)
                {
                    throw new SdmxException("Error while attempting to process SDMX-Json Structure file", e);
                }
            }

            return null;
        }

        /// <summary>
        /// Parses the SDMX json structure message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>The <see cref="ISdmxObjects"/></returns>
        private ISdmxObjects ParseSdmxJsonStructureMessage(JObject message)
        {
            return this._sdmxBeansJsonBuilder.Build(message);
        }
    }
}