﻿// -----------------------------------------------------------------------
// <copyright file="SdmxStructureWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2013-03-20
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxStructureParser.
//     SdmxStructureParser is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxStructureParser is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxStructureParser.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------
namespace Org.Sdmxsource.Sdmx.Structureparser.Factory
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.EdiParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.Registry.Response.V2;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.XmlSerialization.V2;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class SdmxStructureWriterFactory : IStructureWriterFactory
    {
        /// <summary>
        /// The _structure XML builder
        /// </summary>
        private readonly StructureXmlBuilder _structureXmlBuilderBean;

        /// <summary>
        /// The _query structure response builder v2
        /// </summary>
        private readonly QueryStructureResponseBuilderV2 _queryStructureResponseBuilderV2;

        /// <summary>
        ///     Initializes a new instance of the <see cref="SdmxStructureWriterFactory" /> class.
        /// </summary>
        public SdmxStructureWriterFactory()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SdmxStructureWriterFactory" /> class.
        /// </summary>
        /// <param name="builderFactory">The builder factory.</param>
        public SdmxStructureWriterFactory(IBuilderFactory builderFactory)
        {
            builderFactory = builderFactory ?? new XmlBuilderFactoryV2();
            this._structureXmlBuilderBean = new StructureXmlBuilder(builderFactory);
            this._queryStructureResponseBuilderV2 = new QueryStructureResponseBuilderV2(builderFactory, this._structureXmlBuilderBean);
        }

        /// <summary>
        ///     Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">
        ///     An implementation of the StructureFormat to describe the output format for the structures
        ///     (required)
        /// </param>
        /// <param name="streamWriter">The output stream to write to (can be null if it is not required)</param>
        /// <returns>Null if this factory is not capable of creating a data writer engine in the requested format</returns>
        /// <exception cref="ArgumentNullException"><paramref name="structureFormat"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxNotImplementedException">
        ///     The SDMX version is not supported.
        /// </exception>
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            if (structureFormat == null)
            {
                throw new ArgumentNullException("structureFormat");
            }

            if (structureFormat.SdmxOutputFormat == null)
            {
                return null;
            }

            var outputFormat = structureFormat.SdmxOutputFormat;
            SdmxSchema schemaVersion = outputFormat.OutputVersion;

            SdmxStructureFormat sdmxStructureFormat = structureFormat as SdmxStructureFormat;
            if (sdmxStructureFormat != null)
            {
                if (!outputFormat.IsQueryResponse && !outputFormat.IsRegistryDocument)
                {
                    return this.GetObjectEngine(schemaVersion, streamWriter, sdmxStructureFormat.OutputEncoding);
                }

                if (outputFormat.EnumType == StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)
                {
                    return new RegistryQueryResponseWriterEngineV2(streamWriter, this._queryStructureResponseBuilderV2) { Encoding = sdmxStructureFormat.OutputEncoding };
                }
            }

            SdmxXmlStructureFormat sdmxXmlStructureFormat = structureFormat as SdmxXmlStructureFormat;
            if (sdmxXmlStructureFormat != null)
            {
                if (!outputFormat.IsQueryResponse && !outputFormat.IsRegistryDocument)
                {
                    return this.GetObjectEngine(schemaVersion, sdmxXmlStructureFormat.Writer);
                }

                if (outputFormat.EnumType == StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument)
                {
                    return new RegistryQueryResponseWriterEngineV2(sdmxXmlStructureFormat.Writer, this._queryStructureResponseBuilderV2);
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the <see cref="IStructureWriterEngine" /> engine.
        /// </summary>
        /// <param name="schemaVersion">The schema version.</param>
        /// <param name="writer">The writer.</param>
        /// <returns>
        /// The <see cref="IStructureWriterEngine" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">The specified value at <paramref name="schemaVersion" /> is not supported</exception>
        private IStructureWriterEngine GetObjectEngine(SdmxSchema schemaVersion, XmlWriter writer)
        {
            switch (schemaVersion.EnumType)
            {
                case SdmxSchemaEnumType.VersionOne:
                    return new StructureWriterEngineV1(writer);
                case SdmxSchemaEnumType.VersionTwo:
                    return new StructureWriterEngineV2(writer, this._structureXmlBuilderBean);
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    return new StructureWriterEngineV21(writer);
                default:
                    throw new SdmxNotImplementedException(
                        ExceptionCode.Unsupported,
                        schemaVersion + " - StructureWritingManagerImpl.writeStructure");
            }
        }

        /// <summary>
        /// Returns the <see cref="IStructureWriterEngine" /> engine.
        /// </summary>
        /// <param name="schemaVersion">The schema version.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <returns>
        /// The <see cref="IStructureWriterEngine" />.
        /// </returns>
        /// <exception cref="SdmxNotImplementedException">The specified value at <paramref name="schemaVersion" /> is not supported</exception>
        private IStructureWriterEngine GetObjectEngine(SdmxSchema schemaVersion, Stream stream, Encoding encoding)
        {
            switch (schemaVersion.EnumType)
            {
                case SdmxSchemaEnumType.VersionOne:
                    return new StructureWriterEngineV1(stream) { Encoding = encoding };
                case SdmxSchemaEnumType.VersionTwo:
                    return new StructureWriterEngineV2(stream, this._structureXmlBuilderBean) { Encoding = encoding };
                case SdmxSchemaEnumType.Edi:
                    return new StructureWriterEngineEdi(stream, new EdiParseManager());
                case SdmxSchemaEnumType.VersionTwoPointOne:
                    return new StructureWriterEngineV21(stream) { Encoding = encoding };
                default:
                    throw new SdmxNotImplementedException(
                        ExceptionCode.Unsupported,
                        schemaVersion + " - StructureWritingManagerImpl.writeStructure");
            }
        }
    }
}